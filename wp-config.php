<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'devprj02_dom');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'his70autoapp');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_MEMORY_LIMIT', '64M');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-hV8Ea96s|g`{*,x13nP=7{ZF*b)LiZ}+CEiJ^&/Z)el)bA??V/)~=0_a0X.Vzlu');
define('SECURE_AUTH_KEY',  '|5vvF-sWjt`Wa^zx/.(DRBcq.)qQgv#&*6}_rQ!N%b|a(llk.n.?Ma>`iEi!GGv>');
define('LOGGED_IN_KEY',    'GAL#1SHc|wf*!Fz-5!m^E$#VgaBiaXMz0dU6)`3F|lE=hs~|Shv{h4BN81-M^hi3');
define('NONCE_KEY',        ',bP/$!R[>lGMtW`n{<%^D^O|-x<2Qltl>P5m`oo290|pdcW2f$o|ob|5QwIB$1m$');
define('AUTH_SALT',        'L4-X6_/$*n2:)#fBw5{^TCYQnc=oha$hZTkTrDSd}[-=s3 f7+$AK.~~/ZX-,-H ');
define('SECURE_AUTH_SALT', '$FwOsP9L2R{f}:9Za,0WH+:|LG=VfD4>;|k|l+pog6p!g8e[[{!/jzqeZj*}#C+;');
define('LOGGED_IN_SALT',   ':l+%ZDi;3CS/wx7T C&+mCSRY[P(0[cmlbGb;Q+Ws/n|{6-:t=]D,?L,38pu)R3=');
define('NONCE_SALT',       'Eh E/Pz<z2p7z#w`Brn]K7p>_,g6)(elW_hT9An{OtBJ/k+u+i1p=)4sg#@hwFjH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('FS_METHOD', 'direct');
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
