# Online Insuranec Forms application


## About the application

Main intention of this application is related to insurance.

There are 3 type of users in this application. Here is over all functionality of the application for each user:

1. Super admin: 

   1.1  Super admin maintains the details about the clients. Clients are the intermediaries who sell policies to the policy holders. 

   1.2 We maintain details of forms in super admin. Forms will contain questions that needs to be answered by the polocy holder when he/she makes a claim. 

   1.3 We also maintain the email templates etc from the application.
   
   Super admin can be invoked from /super_admin
   
   Super admin creates questions that will be asnwered by the policy holders.

2. Insurance Intermediary that will have several admin

    Insurance intermediary is the persom from where all the revenue will be comming. He would pay us. Each insurance intermediary will have several admin users who will be able to log-in to our system and will see the forms filled up by the policy holder and then further process it. The intermediary will also be able to import the forms (from word templates) generated from our application.

    Note that we do not generate the word files they come to some directory from other application (not ours).

    Once the intermediary gets registered; one or more admins will be generated for him. The admins will perform tasks for insurance intermediary. 

    Accesing / of your application invokes the admin login. /admin also brings the same login page.

    Admin can see the answered filled up by the policy holders ( the questions created by the super admin will be answered by the policy holder )

3. Policy holder

    Policy holder is the person who will fill out the form imported.

    Policy holder receives a link in email from which he can use the application without loggin in to the system. Policy holder answers the questions and confirms etc. Policy holder can login also if he/she decides to.


## Technical Details

### Models 

1. "We have one model for each table. "
     
2. Each Model has a Save method. Which if called would insert the record in the database table.

3. Each Model has a get where method which returns an array ob objects of same class. 
    Get where method takes an array of arguments according to which we get the results. 
    The array needs to be in the form as specified in the documentation at http://ellislab.com/codeigniter/user-guide/database/active_record.html.

4. If you want some conversion to be done before save or after save for data in the model you can write the code for it in the before-save() and after_save() method of each model.

5. Calling update method on the object of model will cause the record to be updated in the database.

6. Each model has a table name attribute which once supplied in the constructor is applied to all the queries in the model. All the queries are of type active record so the "table_name does not need to be prefixed". The prefiex from the database.php will automatically be applied. 

7. If you want to change nullability of the field while saving in the datbase it can be done from the model itself. You might have to change the before save, after save and from object methods in the model.

#### Here is details of each model and their descrption

(table names in this document are not prefixed)

1. admin\_model : 

   Maps to table: admin. 

   Descreption: Each insurance intermediary will have several admins 
   fo him who will be able to perform tasks for him. 
   This model represents the same admin from the database table.

2. admin\_subscription\_kinds\_model :

   Maps to table: subscription_kinds. 

   Descreption: There are subscription kinds that the admin can maintain. 

   This class represents table for maintaining subscription kinds.

   Subscription kindss can also be called subscriptio type.

3. admin\_texts\_model : 

   Maps to table: texts. 

   Descreption: Represents text that can be maintained. 
   
   Texts are associated with admins at the moment.

4. claims\_files\_model : 

   Maps to table: claim_files. 

   Descreption: Represents the attachments attached by the policy holder for the claim. 

5. claims\_model :

   Maps to table: claims. 

   Descreption: For each policy holder there would be claim. This model represents the details of that calim. 

6. client\_email\_template\_model : 

   Maps to table: client\_email\_templates. 

   Descreption: a model that represents the client's (client=intermediary) email template from the database.

7. client\_subscription\_model : 

   Maps to table: client\_subscription. 

   Descreption: This model represents the details about which client has taken which subscription.

8. clients\_model : 

   Maps to table: clients. 

   Descreption: ach intermediary is known as client.
   This model represents the table and mechanism 
   for accesing it from the database.

9. clients\_model : 

   Maps to table: clients. 

   Descreption: ach intermediary is known as client.
 
10. forms\_answers\_details\_model : 

   Maps to table: forms\_answers\_details. 

   Descreption: This model represents the table from database that stores the actuals answers given by the s.
 
11. forms\_answers\_model : 

   Maps to table: forms\_answers. 

   Descreption: Super admin defines questions and possible answers (if any).
   
   Each question will have many possible answers.

   This model represents the possbile answers that the policy holder can select.

   New Answers CRUD for answer can be done by admin/super admin.
 
12. forms\_categories\_model : 

   Maps to table: forms\_categories. 

   Descreption: Each form will have many categories. This model represents the categories from the database.

13. forms\_categories_question\_answer\_model : 

   Maps to table: forms\_categories_answer. 

   Descreption: This model represents the answer that belong to a question (if the questio is of type single selection/multiple selection).

14. forms\_categories\_question_model

   Maps to table: forms\_categories_question. 

   Descreption: This model represents the table that is used to store the questions that belng to a category.

15. forms\_model :
  
   Maps to table: forms. 

   Descreption: This class represents the form from the database table.

   Each form will have many categories and which in turn have many questions.
    
16. mail\_template_model :
  
   Maps to table: admin\_email_templates. 

   Descreption: This class represents the mail templates of super admin.

17. policy\_holder_model :
  
   Maps to table: policy\_holder. 

   Descreption: Represents policy holder who maked claim. Each policyy holder can have many claims as of now.

18. policy\_holder\_model :
  
   Maps to table: policy\_holder. 

   Descreption: Represents policy holder who maked claim. Each policyy holder can have many claims as of now.

19. super\_admin_authenticate :
  
   Maps to table: super\_admin. 

   Descreption: A class that represents the super admin. Also used for authentication.

20. users\_model :
  
   Maps to table: users. 

   Descreption: Represents the users table. User can be one of these types: policy holder/admin.

21. invoices:
   
   At the moment the invoices are generated when a subscription is added.


### Other Technical Details

1. We have used hooks for checking the login etc.

   login\_check_hook is used for checking login for user, admin as well as super admin.

   claims\_check_hook makes sure that the policy holder is not accesing wrong claims.

2. Hidden firleds are used for many question and answer related pages. 

3. To find out the controller from the URL, first guess the controller name from the URL. 

   If you can not find the appropriate controller then see the routes.
   
   Many Controllers do not have routes and are accessed directly.

4. Each controller has afield named default\_template_name in the controller. This field is initialized in the constructor. The actual value comes from a helper. If you want to changed the default template then change the name of view in the helper.

5. In the controller we load the main template (which is actually a view). And that template in-turn loads the actual page. The name of actual page is passed in the array with name "page\_name". Controller renders the template page and the template page actually renders the page intended (the view gets the name of the page from variable page\_name)

6. Update the helper in order to change the logo on admin side.

7. The go to question and go to category (in admin preview, super admin preview and policy holder's answer page) uses hidden fields for determingin the previous question or category. Hidden fields are updated as and when we go to next category and valud of hidden field gets updated when we click back. C:74 in the hidden field says that the category id is 74 where we need to go. Q:58 says that we need to go to question with id 58. All the values are comma separated.

## Deployment

* The application can not be deployed directly. In order to put somethings on the server you will have to commit and snc with the git repository. After syncinc with bitbucket you will have to login to the server thru ssh and then execute pull command thru the commandline of ssh. This is the only way to deploy at the server. Onr order to change a single line you will have to commit it and sync it and then have to pull at the server.

* Here are the commands that you can execute in order to deploy to the server.

IT could be:

     ssh root@5.157.80.123
      
     # enter password of ssh here 
      
     cd /var/www/online-insurance-forms

     git pull origin master
     
     # enter password of git repositpry here 


* If the deployment fails because of a file changed at the server, you should see the error message and checkout the file so that it goes to its last committed state and the deployment can be done afterwards.

     git checkout folder/file_name.extension

## Translations

Schade nummer adviseur = damage number

Polisnummer ( s ) = policy number

Verzekeringsnemer = Policyholder

Adres = Address policy holder

Postcode en woonplaats = zip code city policy holder

Telefoon private = private phone number policy holder

Telefoon werk = business phone number policy holder

Telefoon mobiel = mobile phone number policy holder

Contact me = Neem contact op

Contact Person = Contactpersoon

First name = Voornaam

Last name* = Achternaam

Mail address* = E-mailadres

Company Details = Bedrijfsgegevens

Company name* = Bedrijfsnaam

Street* = Adres

Zip code* = Postcode	

City* = Stad

Mail Address = E-mailadres

Web-site = Website

Fields marked with * are mandatory = Velden met een '*' zijn verplicht om in te vullen

Contact me (button) = Verstuur

List insurances page = Formulieren

Import Date = Datum

Form Number = Nummer

Kind = Soort

Policy Numer = Polisnummer

Mail Send= E-mail verstuurd

Logout = Uitloggen

Mail Address = E-mailadres

Password = Wachtwoord

Password forgot? = Wachtwoord vergeten?

Contact Me = Contact

Login = Inloggen

Subscription Information = Abonnementen

About Us = Over ons

Completed = Afgerond

Search = Zoeken

Filter = Filter

List of insurance forms = Overzicht formulieren

Policy holder = Polishouder

import=importeren

select=Selecteer

List All Forms = Overzicht formulieren

Show entries = Toon rijen

Name = Formuliernaam

Form Code = Code

Categories = Categorieen

Forgot Password = Wachtwoord vergeten

Send password = Verstuur

///

Mail Templates = E-mailsjabloon

Email type = Naam

Customer = Klant

Subject = Onderwerp	

Attachment = Bijlage

No attachments yet = Geen bijlage	

Message = Tekst

BCC to administrator = BCC naar beheerder

Save = Opslaan

Cancel = Annuleer

///

List News = Overzicht nieuws

Add News = Toevoegen nieuwsbericht

Title = Titel

Publication Date = Publicatiedatum

Expire Date = Vervaldatum

News Text = Tekst

Current Logo = Huidige logo	

New Logo = Logo	

Email = E-mailadres

Welcome Text = Welkomstekst

List All Users = Overzicht gebruikers

Name = Naam

Register Date = Registratiedatum

Delete = Verwijderen

Detail of User = Details gebruikers

Active = Actief

Middle Name = Tussenvoegsel

Password = Wachtwoord

Password Confirmation = Wachtwoord (bevestiging)

Handler = Behandelaar

Fields marked with * are mandatory = Velden die gemarkeerd zijn met een * zijn verplicht

Home = Home
	
Forms = Formulieren

News = Nieuws

Users = Gebruikers

Settings = Instellingen

Mailtemplates = E-mailsjablonen

Add New News = Toevoegen nieuwsber

Publication Date = Publicatedatum	

Edit Forms = Details formulier
 		
Form Tag = Code	
	
Introduction Text = Introductietekst 
	
Closure Text = Afsluitende tekst

Show All questions at once = Toon alle vragen in een keer

List All Categories of schades Form = Overzicht categorien 

Sequence = Volgorde

Category Name = Categorienaam

Question = Vragen

Click Here = Klik hier

Add new category = Toevoegen categorie

Back = Terug

Policy Holder Address = Adres

Zip Code = Postcode

Mail Address = E-mailadres	

Private Number = Telefoon

Mobile Number = Mobiele telefoon

Uploaded Files By Policy Holder = Toegevoegde bestanden
	
Return = Terug

List All Question of categorycde Category in asdf Form = Overzicht vragen van de category <categoryname> 

Add New Questions = Toevoegen vraag

Question ID = Vraagnummer

Question = Vraag

Answers = Antwoorden


### In edit/add answer-choice page of super admin and admin:
  Add Answer = Toevoegen antwoord 

  Answer * = Antwoord

  Question/Category no to go to = Ga naar vraag / categorie	

  None = Geen

  To Question = Naar vraag

 To Category = Naar categorie
 
lient Subscription = Abonnement

Kind Of Subscription = Soort abonnement

Start Date = Ingangsdatum

End Date = Einddatum

Subscription Fee = Tarief/maand

#Forms = Aantal formulieren

Invoices = Facturen

Invoice Number = Nummer

Subject = Onderwerp

Invoice Amount = Bedrag

Invoice Date = Datum


