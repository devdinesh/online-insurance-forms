-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: devrepublic_insur
-- ------------------------------------------------------
-- Server version	5.5.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ins_admin`
--

DROP TABLE IF EXISTS `ins_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) DEFAULT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `password_reset_random_string` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`admin_id`),
  KEY `client_id_fk` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_admin`
--

LOCK TABLES `ins_admin` WRITE;
/*!40000 ALTER TABLE `ins_admin` DISABLE KEYS */;
INSERT INTO `ins_admin` VALUES (1,'umang@devrepublic.nl','Umang','Bhatt','21232f297a57a5a743894a0e4a801fc3',1,'');
/*!40000 ALTER TABLE `ins_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_admin_email_templates`
--

DROP TABLE IF EXISTS `ins_admin_email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_admin_email_templates` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_type` varchar(200) NOT NULL,
  `email_subject` text NOT NULL,
  `email_message` text NOT NULL,
  `email_attachment` varchar(200) NOT NULL,
  `email_format_info` text NOT NULL,
  `email_bcc_admin` tinyint(4) NOT NULL,
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_admin_email_templates`
--

LOCK TABLES `ins_admin_email_templates` WRITE;
/*!40000 ALTER TABLE `ins_admin_email_templates` DISABLE KEYS */;
INSERT INTO `ins_admin_email_templates` VALUES (1,'contact  me','text message','<p>Dear Edward,</p>\n<p>&nbsp;An customer with name &lt;name&gt; wants to contact you, His details are:</p>\n<p><strong>&nbsp;Person Detail</strong></p>\n<p>Name: &lt;name&gt;&nbsp; &lt;last&gt;</p>\n<p>Mail Address: &lt;address&gt;</p>\n<p>&nbsp;<strong>Company Detail</strong></p>\n<p>Company Name:&lt;company name&gt;</p>\n<p>Street:&lt;street&gt;</p>\n<p>Zip Code:&lt;zip code&gt;</p>\n<p>City:&lt;city&gt;</p>\n<p>&nbsp;</p>','dump.sql','ertw',1),(2,'reset_password','Reset Password of Online Insurance Forms','<p>Dear user,&nbsp;</p>\n<p>&nbsp;</p>\n<p>We have received your request for reset of password. In order to reset your password we need you to click on the link below:</p>\n<p>&nbsp;</p>\n<p>&lt;link&gt;</p>\n<p>&nbsp;</p>\n<p>If you face any problems with resetting the password then please contact the admin theru email&nbsp;</p>\n<p>admin@onlineinsuranceforms</p>\n<p>&nbsp;</p>\n<p>Thanks.</p>','','',0),(3,'damage form','damage information..','<p><span style=\"font-size: medium;\"><strong>Damage form</strong></span></p>\r\n<p>&nbsp;</p>\r\n<p>Damage:</p>\r\n<p>Handler:</p>\r\n<p>tussenpersoonnummer:</p>\r\n<p>Verzekerd bij:</p>\r\n<p>Is deze schade al gemend?</p>\r\n<p>Zo ja,wanneer en aan wie?</p>\r\n<p>Bent u elders tegen deze schade verzekerd?</p>\r\n<p>Maatschappij:</p>\r\n<p>Schadedatum:</p>\r\n<p>Uploaded files by policy holder:</p>','','dfgdfgfdg',0),(4,'Contact (client)','welcome cutomer','<p>Dear Customer, we have received following details of yours:</p>\n<p><strong>&nbsp;Person Detail</strong></p>\n<p>Name: &lt;name&gt; &lt;middle&gt; &lt;last&gt;</p>\n<p>Mail Address: &lt;address&gt;</p>\n<p>&nbsp;<strong>Company Detail</strong></p>\n<p>Company Name:&lt;company name&gt;</p>\n<p>Street:&lt;street&gt;</p>\n<p>Zip Code:&lt;zip code&gt;</p>\n<p>City:&lt;city&gt;</p>\n<p>Mail Address: &lt;mail address&gt;</p>\n<p>Website:&lt;website&gt;</p>','dump.sql','ertw',1),(5,'New user','asd','<p>Dear &lt;name&gt;,</p>\r\n<p>your email : &lt;email&gt; asdf&nbsp;</p>\r\n<p>your password:&nbsp;&lt;password&gt;</p>','','',0);
/*!40000 ALTER TABLE `ins_admin_email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_admin_setting`
--

DROP TABLE IF EXISTS `ins_admin_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_admin_setting` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_logo` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_admin_setting`
--

LOCK TABLES `ins_admin_setting` WRITE;
/*!40000 ALTER TABLE `ins_admin_setting` DISABLE KEYS */;
INSERT INTO `ins_admin_setting` VALUES (1,'');
/*!40000 ALTER TABLE `ins_admin_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_banners`
--

DROP TABLE IF EXISTS `ins_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_banners` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_name` varchar(65) NOT NULL,
  `banner_image` varchar(65) DEFAULT NULL,
  `banner_url` varchar(65) NOT NULL,
  `banner_sequence` int(2) NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_banners`
--

LOCK TABLES `ins_banners` WRITE;
/*!40000 ALTER TABLE `ins_banners` DISABLE KEYS */;
INSERT INTO `ins_banners` VALUES (20,'slider1','d2d2dd70e4c6cd02fc8ffde96f536d58.png','',1),(21,'slider2','d2d2dd70e4c6cd02fc8ffde96f536d58.png','qwe',2),(22,'slider3','cac84d4601a1c00d39dcf1c8280ab9d6.png','',3);
/*!40000 ALTER TABLE `ins_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_claim_files`
--

DROP TABLE IF EXISTS `ins_claim_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_claim_files` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) NOT NULL,
  `claim_id` int(11) NOT NULL,
  PRIMARY KEY (`file_id`),
  KEY `fk_ins_claim_files_claim_id_constraint` (`claim_id`),
  CONSTRAINT `fk_ins_claim_files_claim_id_constraint` FOREIGN KEY (`claim_id`) REFERENCES `ins_claims` (`claim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_claim_files`
--

LOCK TABLES `ins_claim_files` WRITE;
/*!40000 ALTER TABLE `ins_claim_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `ins_claim_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_claims`
--

DROP TABLE IF EXISTS `ins_claims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_claims` (
  `claim_id` int(11) NOT NULL AUTO_INCREMENT,
  `import_date` date NOT NULL,
  `kind` varchar(50) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `handler` varchar(255) DEFAULT NULL,
  `schadenummer` text,
  `form_id` int(11) NOT NULL,
  `mail_send_date` date DEFAULT NULL,
  `status` enum('Nieuw','open','Afgerond') NOT NULL,
  `policy_holder_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `conform_date` date DEFAULT NULL,
  `is_delete` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`claim_id`),
  KEY `fk_ins_claims_policy_holder_fk` (`policy_holder_id`),
  KEY `fk_ins_claims_client_fk` (`client_id`),
  CONSTRAINT `fk_ins_claims_client_fk` FOREIGN KEY (`client_id`) REFERENCES `ins_clients` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ins_claims_policy_holder_fk` FOREIGN KEY (`policy_holder_id`) REFERENCES `ins_policy_holder` (`policy_holder_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_claims`
--

LOCK TABLES `ins_claims` WRITE;
/*!40000 ALTER TABLE `ins_claims` DISABLE KEYS */;
INSERT INTO `ins_claims` VALUES (30,'2013-09-19',NULL,1,'Uiterwijk Winkel Verzekeringen','Uiterwijk Winkel Verzekeringen',50,NULL,'Nieuw',15,1,NULL,1),(31,'2013-09-19',NULL,9,'Mike van Schravendijk','tum formulier: 15-08-2013',61,NULL,'Nieuw',9,4,NULL,1),(32,'2013-09-19',NULL,1,NULL,'TIME \\@ \"d MMMM yyyy\" 6 juni 2013',13,NULL,'Nieuw',4,1,NULL,1),(33,'2013-09-19',NULL,9,'Mike van Schravendijk',NULL,61,'2013-09-19','Nieuw',5,4,'2013-09-19',1),(36,'2013-09-19',NULL,1,NULL,'TIME \\@ \"d MMMM yyyy\" 6 juni 2013',13,NULL,'Nieuw',4,1,NULL,1),(38,'2013-09-19',NULL,9,'Mike van Schravendijk',NULL,61,NULL,'Nieuw',9,4,NULL,1),(39,'2013-09-19',NULL,9,'Mike van Schravendijk',NULL,61,'2013-09-19','Nieuw',5,4,NULL,1),(40,'2013-09-19',NULL,9,'Mike van Schravendijk',NULL,61,'2013-09-19','Nieuw',5,4,'2013-09-19',1),(41,'2013-09-19',NULL,9,'Mike van Schravendijk',NULL,61,'2013-09-19','Nieuw',5,4,'2013-09-19',1),(42,'2013-09-19',NULL,9,'Mike van Schravendijk',NULL,61,'2013-09-19','Nieuw',5,4,'2013-09-19',1),(43,'2013-09-19',NULL,9,NULL,NULL,54,'2013-09-19','Nieuw',5,4,NULL,1),(44,'2013-09-26',NULL,27,'rzekeraar SB EP: UW Assuradeuren','rzekeraar SB EP: UW Assuradeuren',24,NULL,'Nieuw',16,4,NULL,1),(45,'2013-09-26',NULL,27,'',NULL,24,'2013-09-26','Nieuw',16,4,'2013-09-26',1),(46,'2013-09-26',NULL,9,'',NULL,24,'2013-09-26','Nieuw',17,4,NULL,1),(47,'2013-09-26',NULL,9,NULL,'Uiterwijk Winkel Verzekeringen',24,NULL,'Nieuw',17,4,NULL,1),(48,'2013-09-26',NULL,27,'',NULL,24,'2013-09-26','Nieuw',18,4,NULL,1),(49,'2013-09-26',NULL,27,'',NULL,24,'2013-09-26','Nieuw',11,4,NULL,1),(50,'2013-09-26',NULL,27,'',NULL,24,'2013-09-26','Nieuw',18,4,NULL,1),(51,'2013-09-26',NULL,27,'',NULL,24,'2013-09-26','Nieuw',11,4,NULL,1),(52,'2013-09-27',NULL,27,'M.H. van Schravendijk',NULL,61,'2013-09-27','Nieuw',2,4,'2013-09-27',1),(53,'2013-09-27',NULL,27,'M.H. van Schravendijk',NULL,54,'2013-09-27','Nieuw',2,4,'2013-09-27',1),(54,'2013-10-03',NULL,9,'',NULL,61,'2013-10-03','Nieuw',19,4,NULL,1),(55,'2013-10-03',NULL,9,'',NULL,61,'2013-10-03','Nieuw',19,4,NULL,1),(56,'2013-10-03',NULL,9,'',NULL,61,'2013-10-03','Nieuw',5,4,NULL,1),(57,'2013-10-04',NULL,27,'Hans van der Starren',NULL,24,'2013-10-04','Nieuw',16,4,NULL,1),(58,'2013-10-04',NULL,27,'Hans van der Starren',NULL,24,'2013-10-04','Nieuw',16,4,NULL,1),(59,'2013-10-09',NULL,9,'Mike van Schravendijk',NULL,61,'2013-10-15','open',5,4,'2013-10-11',0),(62,'2013-10-15',NULL,9,'Mike van Schravendijk','tum formulier: 15-08-2013',61,'2013-10-15','Afgerond',5,4,'2013-10-15',0),(67,'2013-11-11',NULL,1,'',NULL,13,'2013-11-11','Nieuw',4,1,NULL,1),(69,'2013-11-25','',0,'','201300000',13,'2014-01-29','open',21,1,'2013-12-16',0),(70,'2013-11-25','',0,'','201300000',13,'2014-01-28','Afgerond',21,1,'2014-01-29',0);
/*!40000 ALTER TABLE `ins_claims` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_client_email_templates`
--

DROP TABLE IF EXISTS `ins_client_email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_client_email_templates` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_type` varchar(200) CHARACTER SET utf8 NOT NULL,
  `email_subject` text CHARACTER SET utf8,
  `email_message` text CHARACTER SET utf8,
  `email_attachment` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `email_format_info` text CHARACTER SET utf8,
  `email_bcc_admin` tinyint(1) NOT NULL DEFAULT '0',
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`email_id`),
  KEY `email_id` (`email_id`),
  KEY `email_id_2` (`email_id`),
  KEY `email_id_3` (`email_id`),
  KEY `email_id_4` (`email_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_client_email_templates`
--

LOCK TABLES `ins_client_email_templates` WRITE;
/*!40000 ALTER TABLE `ins_client_email_templates` DISABLE KEYS */;
INSERT INTO `ins_client_email_templates` VALUES (1,'new_registration','New Registration','<p>asdf asdf &nbsp;asdf asdf</p>\r\n<p>&lt;link&gt;</p>\r\n<p>&lt;logo&gt;</p>\r\n<p>&nbsp;</p>',NULL,NULL,1,1),(3,'form link','Form link','<p>This is a form&nbsp;</p>\r\n<p>&lt;link&gt;</p>\r\n<p>&lt;logo&gt;</p>\r\n<p>&nbsp;email generated from &nbsp;application. Please do not reply to this email.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>',NULL,NULL,1,1),(7,'Form Completed (insurance)','Test: Form completed','<p>Dear Intermediary,</p>\r\n<p>&nbsp;</p>\r\n<p>Some one just filled in and confirmed details for policy number: &lt;policy_number&gt;</p>\r\n<p>and &lt;form_number&gt;</p>\r\n<p>&lt;logo&gt;</p>\r\n<p>System generated email, Please do not reply to this email</p>',NULL,NULL,1,1),(8,'Form Completed (policy)','Test: Form completed','<p>Dear Policy holder,</p>\r\n<p>&nbsp;</p>\r\n<p>You just filled in and confirmed details for policy number: &lt;policy_number&gt;</p>\r\n<p>and &lt;form_number&gt;</p>\r\n<p>&nbsp;&lt;logo&gt;</p>\r\n<p>System generated email, Please do not reply to this email</p>',NULL,NULL,1,1),(9,'form link','Mail aan relatie met formulierenlink','<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Geachte relatie,</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Dit emailbericht is automatisch door ons aangemaakt. Door op onderstaande link te klikken opent u een formulier. Wij verzoeken u&nbsp;deze per omgaande in te vullen en op de in het formulier aangegeven wijze aan ons te retourneren. </span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Klik op &lt;link&gt;&nbsp;om het formulier te openen</span></p>\r\n<address><span style=\"font-size: small;\">&nbsp;</span></address><address><span style=\"font-size: small;\">Met vriendelijke groet,</span></address><address><span style=\"font-size: small;\">&nbsp;</span></address><address><span style=\"font-size: small;\">Postbus 141, 7900 AC Hoogeveen</span></address><address><span style=\"font-size: small;\">&nbsp;</span></address><address><span style=\"font-size: small;\">Telnr. 0528-280551</span></address><address><span style=\"font-size: small;\">Faxnr. 0528-280505</span></address><address><span style=\"font-size: small;\">AFM verg.nr. 12007086</span></address><address><span style=\"font-size: small;\">KVK 04050872 te Meppel</span></address><address><span style=\"font-size: small;\">&nbsp;</span></address><address><span style=\"font-size: small;\">Op al onze werkzaamheden zijn de Algemene Voorwaarden van Uiterwijk Winkel Verzekeringen van toepassing.</span></address><address><span style=\"font-size: small;\">&nbsp;</span></address><address><span style=\"font-size: small;\">De informatie in dit e-mail-bericht is uitsluitend bestemd voor de geadresseerde. Verstrekking aan en gebruik door anderen is niet toegestaan.Uiterwijk Winkel Verzekeringen staat niet in voor de juiste en volledige overbrenging van een verzonden e-mail-bericht.</span></address><address><span style=\"font-size: small;\">&lt;logo&gt;</span></address>',NULL,NULL,1,4),(10,'Form Completed (insurance)','Bevestiging afgerond formulier door klant','<p><span style=\"font-size: small;\">Het formulier is door de klant ingevuld.</span></p>\r\n<p><span style=\"font-size: small;\">&lt;logo&gt;</span></p>',NULL,NULL,1,4),(11,'Form Completed (policy)','Bevestiging afgerond formulier aan klant','<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Geachte relatie,</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">U heeft zojuist met succes uw ingevulde&nbsp;formulier retour gestuurd naar Uiterwijk Winkel&nbsp;Verzekeringen.</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Uw formuliernummer is &lt;form_number&gt;. Onder dit kenmerk kunt u uw formulieren terugvinden, openen en nog eens nalezen. U kunt het formulier niet meer bewerken.</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">&lt;logo&gt;</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Heeft u nog vragen, belt u ons gerust.</span>&nbsp;</p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\"><img src=\"p:|assuconsultancy\\digitaal%20op%20maat\\logo.jpg\" alt=\"\" /></span></p>',NULL,NULL,1,4),(12,'new_registration','Registratie nieuwe klant','<p><span style=\"font-size: small;\">Geachte klant,</span></p>\r\n<p><span style=\"font-size: small;\">Uiterwijk Winkel Verzekeringen heeft u een link gestuurd zodat u kunt inloggen op uw persoonlijke formulierenomgeving.</span></p>\r\n<p><span style=\"font-size: small;\">Klik op de link hieronder om uw deze persoonlijke omgeving te activeren.</span></p>\r\n<p><span style=\"font-size: small;\">&lt;link&gt;</span></p>\r\n<p><span style=\"font-size: small;\">&lt;logo&gt;</span></p>\r\n<p><span style=\"font-size: small;\"><strong>Dit mailbericht is automatisch aangemaakt; u kunt hierop niet reageren.</strong></span></p>',NULL,NULL,1,4),(13,'forgot_password','Forgot Password','<p>Dear Policy holder,</p>\r\n<p>You have requested for reset password.</p>\r\n<p>&nbsp;Please click the below link for completing the Reset Password Process.</p>\r\n<p>&lt;link&gt;</p>\r\n<p>System generated email, Please do not reply to this email</p>',NULL,NULL,0,1),(14,'forgot_password','Wachtwoord opnieuw instellen aan klant','<p><span style=\"font-size: small;\">Geachte klant,</span></p>\n<p><span style=\"font-size: small;\">U heeft om een nieuw wachtwoord verzocht voor uw persoonlijke formulierenomgeving van Uiterwijk Winkel Verzekeringen.</span></p>\n<p><span style=\"font-size: small;\">Klik op de onderstaande link om verder te gaan; hiermee wordt uw wachtwoord opnieuw ingesteld.</span></p>\n<p><span style=\"font-size: small;\">&lt;link&gt;</span></p>\n<p><span style=\"font-size: small;\"><strong>Dit is een automatisch mailbericht, u kunt hier niet op reageren.</strong></span></p>',NULL,NULL,1,4),(15,'invoice','Digital op Maat Factuur','<p>Dear &lt;name&gt;,</p>\r\n<p>There is an atachment which has invoice detail.</p>\r\n<p>System generated email, Please do not reply to this email</p>',NULL,NULL,0,1),(16,'invoice','Digitaal op Maat Factuur','<p>Dear &lt;name&gt;,</p>\n<p>There is an atachment which has invoice detail.</p>\n<p>System generated email, Please do not reply to this email</p>',NULL,NULL,1,4);
/*!40000 ALTER TABLE `ins_client_email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_client_setting`
--

DROP TABLE IF EXISTS `ins_client_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_client_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_logo` varchar(60) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `welcome_text` text NOT NULL,
  `delete_term` int(11) DEFAULT NULL,
  `about_us` text NOT NULL,
  `contact` text NOT NULL,
  `registration_text` text NOT NULL,
  `password` varchar(65) NOT NULL,
  `smtp_server` varchar(100) DEFAULT NULL,
  `smtp_port` int(11) NOT NULL,
  `default_rows` int(11) NOT NULL,
  `pdf_template` varchar(255) DEFAULT NULL,
  `top_margin` int(11) DEFAULT NULL,
  `bottom_margin` int(11) DEFAULT NULL,
  `left_margin` int(11) DEFAULT NULL,
  `right_margin` int(11) DEFAULT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `client_id_idx` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_client_setting`
--

LOCK TABLES `ins_client_setting` WRITE;
/*!40000 ALTER TABLE `ins_client_setting` DISABLE KEYS */;
INSERT INTO `ins_client_setting` VALUES (1,'logo2.jpg',1,'edward@devrepublic.nl','<p>Welcome text from admin</p>',6,'<p>Abuout us text from admin</p>','<p>Contact text from admin</p>','<p>Registaration Text</p>','inderimboe','ssl://smtp.gmail.com',465,50,'home_page_background_pcc_r2_c1.jpg',50,50,70,70),(2,'logo3.jpg',4,'noreply@uiterwijkwinkel.nl','<p class=\"left\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Geachte relatie,</span></p>\r\n<p class=\"left\"><span style=\"font-size: small;\">W</span><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">elkom op de Digitaal Op Maat pagina. Hier vindt u alle berichten die wij aan u hebben verzonden.</span></p>\r\n<p class=\"left\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Sommige documenten zijn formulieren waarop wij graag een reactie van u verwachten.</span></p>\r\n<p class=\"left\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">U kunt deze documenten openen en de vragen beantwoorden. Daarna kunt u voor opslaan en verzenden kiezen.</span></p>\r\n<p class=\"left\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Uw ingevulde formulier wordt dan naar ons retour gezonden.</span></p>\r\n<p class=\"left\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Een eenmaal verzonden formulier is nog wel door u in te zien, maar niet meer te wijzigen.</span></p>',NULL,'<div class=\"left\">\r\n<div id=\"page_content\" class=\"content\">\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Uiterwijk Winkel Verzekeringen is een onafhankelijke tussenpersoon en sinds 1929 gevestigd in Hoogeveen.&nbsp;</span><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Relaties krijgen van ons, op alle verzekeringsgebieden, een deskundig&nbsp;en&nbsp;persoonlijk&nbsp;advies.</span></p>\r\n<div id=\"page_content\" class=\"content\">\r\n<h2>Persoonlijk</h2>\r\n</div>\r\n<div id=\"page_content\" class=\"content\">\r\n<div id=\"page_content\" class=\"content\">\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Voor Uiterwijk Winkel Verzekeringen is een persoonlijke benadering de beste manier om goed advies te geven. Door samen met&nbsp;onze klanten op te trekken, wordt er&nbsp;een passende risico-analyse gemaakt.&nbsp;Voor nu en&nbsp;voor later.</span></p>\r\n</div>\r\n</div>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Een vast aanspreekpunt maakt het daarbij voor&nbsp;de klant&nbsp;laagdrempelig en vertrouwd. De adviseurs van Uiterwijk Winkel Verzekeringen&nbsp;staan voor u klaar!</span></p>\r\n<h2><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Onafhankelijk en zelfstandig</span></h2>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Bij een onafhankelijk tussenpersoon als Uiterwijk Winkel Verzekeringen&nbsp;staat uw belang voorop en vindt u dit ook terug in&nbsp;ons advies. Dat betekent dat wij,&nbsp;met de best bij u passende&nbsp;partij&nbsp;zaken doen. Bij het opstellen van uw advies spelen onze&nbsp;ervaring met de verschillende aanbieders en hun producten&nbsp;een belangrijke rol. </span><br /><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Bovendien bieden wij kwalitatief goede producten aan onder ons eigen label <a href=\"http://www.uwassuradeuren.nl/\" target=\"_blank\">UWAssuradeuren</a>.&nbsp;Wij beschikken over een volmacht van een aantal grote verzekeraars. Door zelf&nbsp;polissen op te&nbsp;maken en schades af te wikkelen,&nbsp;helpen wij u nog sneller!</span></p>\r\n<h2><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Betrouwbaar</span></h2>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Doordat u weet met wie u als persoon te maken heeft, weet u ook dat uw zaken betrouwbaar&nbsp;behandeld worden. Wij doen wat we zeggen, afspraak is afspraak.&nbsp;</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">U krijgt een hoog kwalitatief&nbsp;advies. Onze vakdiploma\'s worden keer op keer gewaarborgd. </span><br /><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Wij worden gecontroleerd door de <a href=\"http://www.afm.nl/\" target=\"_blank\">Autoriteit Financi&euml;le Markten</a> en zijn lid van de branchevereniging <a href=\"http://www.adfiz.nl/overadfiz/consument/\" target=\"_blank\">Adfiz</a>.</span></p>\r\n<h2><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Ervaren</span></h2>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Uiterwijk Winkel Verzekeringen bestaat al sinds 1929. De in ruim tachtig jaar verzamelde&nbsp;kennis blijven wij delen en up-to-date houden.&nbsp;Voor veel van onze&nbsp;klanten is dat al van grote waarde gebleken.&nbsp;</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">De administratie is&nbsp;gestroomlijnd en modern. Dit resulteert in&nbsp;snelle behandeling van schades en vlotte beantwoording van door u gestelde vragen.&nbsp;</span></p>\r\n</div>\r\n</div>','<p><span style=\"font-family: arial,helvetica,sans-serif; font-size: small;\">Indien u nog vragen heeft kunt u contact opnemen met de in het formulier vermelde behandelaar.&nbsp;Zijn of haar rechtstreekse telefoonnummer is in het betreffende formulier vermeld.</span></p>','<p><span style=\"font-family: arial,helvetica,sans-serif; font-size: small;\">Om gebruik te kunnen maken van deze applicatie dient u eenmalig een wachtwoord in te stellen. Met dit wachtwoord kunt u ten allen tijde weer de aan u verzonden formulieren bekijken en eventueel beantwoorden.</span></p>','inderimboe','localhost',25,-1,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ins_client_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_client_subscription`
--

DROP TABLE IF EXISTS `ins_client_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_client_subscription` (
  `client_subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `subscription_kinds_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `monthly_fee` decimal(5,2) NOT NULL,
  PRIMARY KEY (`client_subscription_id`),
  KEY `client_id` (`client_id`,`subscription_kinds_id`),
  KEY `subscription_id` (`subscription_kinds_id`),
  KEY `client_subscription_client_id_constraint` (`client_id`),
  CONSTRAINT `ins_client_subscription_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `ins_clients` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_client_subscription`
--

LOCK TABLES `ins_client_subscription` WRITE;
/*!40000 ALTER TABLE `ins_client_subscription` DISABLE KEYS */;
INSERT INTO `ins_client_subscription` VALUES (1,3,1,'2013-05-08','2013-05-10',250.00),(5,3,1,'2013-05-25','2013-05-28',260.00),(6,4,1,'2013-05-14','2013-08-14',260.00),(7,3,1,'2013-05-24','2013-05-30',260.00),(8,3,1,'2013-07-18','2013-07-26',260.00),(9,3,1,'2013-07-11',NULL,260.00),(10,1,1,'2013-07-01',NULL,260.00),(11,4,1,'2013-08-14','2013-10-14',260.00),(12,1,1,'2013-05-01','2013-07-31',14.00),(13,4,4,'2013-09-03',NULL,70.00);
/*!40000 ALTER TABLE `ins_client_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_clients`
--

DROP TABLE IF EXISTS `ins_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_clients` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(65) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zip_code` varchar(6) NOT NULL,
  `city` varchar(60) NOT NULL,
  `registration_date` date NOT NULL,
  `logo` varchar(65) DEFAULT NULL,
  `title_application` varchar(255) NOT NULL,
  `path_to_import` varchar(65) NOT NULL,
  `current_url` varchar(65) NOT NULL,
  `first_remind_mail` int(3) NOT NULL,
  `second_remind_mail` int(3) NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_clients`
--

LOCK TABLES `ins_clients` WRITE;
/*!40000 ALTER TABLE `ins_clients` DISABLE KEYS */;
INSERT INTO `ins_clients` VALUES (1,'Soyab Rana','ASR Klachtenservice,','2072','baroda','2013-02-12','UW_logo_blok_wit1.jpg','ASR Nederland','a12','a1',1,1),(3,'namevval','address','390016','Baroda','2013-03-26','1.png','asdf','asdf','aaa',1,3),(4,'Uiterwijk Winkel','Hoofdstraat','1111','Hoogeveen','2013-04-15','UW_logo1.png','Test','newtest','www.his-automatisering.nl',10,20);
/*!40000 ALTER TABLE `ins_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_contact_message`
--

DROP TABLE IF EXISTS `ins_contact_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_contact_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date` date NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_contact_message`
--

LOCK TABLES `ins_contact_message` WRITE;
/*!40000 ALTER TABLE `ins_contact_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `ins_contact_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_departments`
--

DROP TABLE IF EXISTS `ins_departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_departments` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_departments`
--

LOCK TABLES `ins_departments` WRITE;
/*!40000 ALTER TABLE `ins_departments` DISABLE KEYS */;
/*!40000 ALTER TABLE `ins_departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_feature`
--

DROP TABLE IF EXISTS `ins_feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_feature` (
  `feature_id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_label` varchar(255) NOT NULL,
  `feature_image` varchar(65) DEFAULT NULL,
  `feature_text` text,
  PRIMARY KEY (`feature_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_feature`
--

LOCK TABLES `ins_feature` WRITE;
/*!40000 ALTER TABLE `ins_feature` DISABLE KEYS */;
INSERT INTO `ins_feature` VALUES (2,'Digitaal Op Maat1',NULL,'<p>\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>'),(3,'Digitaal Op Maat2','no_image1.png','<p>\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>'),(4,'Digitaal Op Maat',NULL,'<p>\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>'),(6,'Digitaal of maap4','default_logo.jpg','<p>\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>');
/*!40000 ALTER TABLE `ins_feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_feedback_category`
--

DROP TABLE IF EXISTS `ins_feedback_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_feedback_category` (
  `feed_cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_cat_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `feed_cat_status` tinyint(1) NOT NULL DEFAULT '1',
  `feed_cat_img` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`feed_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_feedback_category`
--

LOCK TABLES `ins_feedback_category` WRITE;
/*!40000 ALTER TABLE `ins_feedback_category` DISABLE KEYS */;
INSERT INTO `ins_feedback_category` VALUES (1,'Vraag',1,'icon_vraag.png'),(2,'Compliment',1,'icon_compliment.png'),(3,'Suggestie',1,'icon_suggestie.png'),(4,'Klacht',1,'icon_klacht.png'),(5,'Fout',1,'icon_fout.png'),(6,'Anders',1,'icon_anders.png');
/*!40000 ALTER TABLE `ins_feedback_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_feedback_record`
--

DROP TABLE IF EXISTS `ins_feedback_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_feedback_record` (
  `feed_id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_user_id` int(11) NOT NULL DEFAULT '0',
  `feed_username` varchar(200) CHARACTER SET utf8 NOT NULL,
  `feed_email` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `feed_cat_id` int(11) NOT NULL DEFAULT '0',
  `feed_rating` int(4) NOT NULL DEFAULT '0',
  `feed_remark` text CHARACTER SET utf8,
  `feed_created` datetime NOT NULL,
  PRIMARY KEY (`feed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_feedback_record`
--

LOCK TABLES `ins_feedback_record` WRITE;
/*!40000 ALTER TABLE `ins_feedback_record` DISABLE KEYS */;
INSERT INTO `ins_feedback_record` VALUES (1,0,'Guest','manish@devrepublic.nl',3,5,'Test suggetie','2013-10-26 00:00:00'),(2,1,'Soyab Rana','soyab@devrepublic.nl',1,0,'Test Vraag','2013-10-26 00:00:00'),(3,0,'Guest','dinesh@devrepublic.nl',2,5,'AS','2013-11-13 00:00:00'),(4,0,'Guest','dinesh@devrepublic.nl',2,5,'ZX','2013-11-13 00:00:00'),(5,0,'Guest','dinesh@devrepublic.nl',2,5,'sd','2013-11-13 00:00:00'),(6,0,'Guest','dinesh@devrepublic.nl',2,4,'sd','2013-11-13 00:00:00'),(7,0,'Guest','dinesh@devrepublic.nl',2,3,'asd','2013-11-13 00:00:00'),(8,0,'Guest','dinesh@devrepublic.nl',2,4,'zxc','2013-11-13 00:00:00'),(9,0,'Guest','dinesh@devrepublic.nl',2,5,'as','2013-11-13 00:00:00'),(10,0,'Guest','dinesh@devrepublic.nl',2,5,'dsdf','2013-11-13 00:00:00'),(11,0,'Guest','dinesh@devrepublic.nl',2,5,'testyi','2013-11-13 00:00:00'),(12,0,'Guest','dinesh@devrepublic.nl',2,5,'xc','2013-11-13 00:00:00'),(13,0,'Guest','dinesh@devrepublic.nl',2,5,'d','2013-11-13 00:00:00'),(14,0,'Guest','dinesh@devrepublic.nl',2,6,'sd','2013-11-13 00:00:00'),(15,0,'Guest','dinesh@devrepublic.nl',2,5,'sd','2013-11-13 00:00:00'),(16,0,'Guest','dinesh@devrepublic.nl',2,0,'te','2013-11-13 00:00:00'),(17,1,'Soyab Rana','dinesh@devrepublic.nl',2,4,'sa','2013-11-13 00:00:00'),(18,1,'Soyab Rana','dinesh@devrepublic.nl',2,5,'xc','2013-11-13 00:00:00'),(19,1,'Soyab Rana','dinesh@devrepublic.nl',2,6,'df','2013-11-13 00:00:00'),(20,1,'Soyab Rana','dinesh@devrepublic.nl',2,4,'test','2013-11-13 00:00:00'),(21,1,'Soyab Rana','dinesh@devrepublic.nl',5,5,'c','2013-11-13 00:00:00'),(22,1,'Soyab Rana','dinesh@devrepublic.nl',2,4,'xcv','2013-11-13 00:00:00'),(23,1,'Soyab Rana','dinesh@devrepublic.nl',2,5,'test','2013-11-13 00:00:00'),(24,1,'Soyab Rana','dinesh@devrepublic.nl',2,5,'sdfg','2013-11-13 00:00:00'),(25,1,'Soyab Rana','dinesh@devrepublic.nl',2,4,'asd','2013-11-13 00:00:00'),(26,1,'Soyab Rana','dinesh@devrepublic.nl',3,5,'df','2013-11-13 00:00:00'),(27,1,'Soyab Rana','dinesh@devrepublic.nl',2,5,'sd','2013-11-13 00:00:00'),(28,1,'Soyab Rana','dinesh@devrepublic.nl',2,6,'sdf','2013-11-13 00:00:00'),(29,1,'Soyab Rana','dinesh@devrepublic.nl',3,6,'dfg','2013-11-13 00:00:00'),(30,1,'Soyab Rana','dinesh@devrepublic.nl',2,6,'fdg','2013-11-13 00:00:00'),(31,1,'Soyab Rana','dinesh@devrepublic.nl',2,0,'dfg','2013-11-13 00:00:00'),(32,1,'Soyab Rana','dinesh@devrepublic.nl',2,0,'dfsg','2013-11-13 00:00:00'),(33,1,'Soyab Rana','dinesh@devrepublic.nl',2,0,'sdf','2013-11-13 00:00:00'),(34,1,'Soyab Rana','dinesh@devrepublic.nl',2,0,'df','2013-11-13 00:00:00'),(35,0,'Guest','dinesh@devrepublic.nl',2,4,'we','2013-11-13 00:00:00'),(36,0,'Guest','dinesh@devrepublic.nl',2,0,'sdf','2013-11-13 00:00:00'),(37,0,'Guest','dinesh@devrepublic.nl',1,0,'df','2013-11-13 00:00:00'),(38,0,'Guest','dinesh@devrepublic.nl',2,0,'sdf','2013-11-13 00:00:00'),(39,0,'Guest','dinesh@devrepublic.nl',2,5,'gfjhgf','2013-11-13 00:00:00'),(40,0,'Guest','dinesh@devrepublic.nl',2,0,'ers','2013-11-13 00:00:00'),(41,0,'Guest','dinesh@devrepublic.nl',2,0,'sdfg','2013-11-13 00:00:00'),(42,0,'Guest','dinesh@devrepublic.nl',2,5,'sdf','2013-11-13 00:00:00'),(43,0,'Guest','dinesh@devrepublic.nl',2,5,'zx','2013-11-13 00:00:00'),(44,0,'Guest','dinesh@devrepublic.nl',2,6,'test','2013-11-13 00:00:00'),(45,0,'Guest','dinesh@devrepublic.nl',2,4,'sd','2013-11-13 00:00:00'),(46,0,'Guest','dinesh@devrepublic.nl',2,6,'dfg','2013-11-13 00:00:00'),(47,0,'Guest','dinesh@devrepublic.nl',2,6,'zx','2013-11-13 00:00:00'),(48,0,'Guest','dinesh@devrepublic.nl',2,4,'testing','2013-11-13 00:00:00'),(49,0,'Guest','dinesh@devrepublic.nl',2,7,'ghj','2013-11-13 00:00:00'),(50,0,'Guest','dinesh@devrepublic.nl',2,3,'zx','2013-11-13 00:00:00'),(51,0,'Guest','dinesh@devrepublic.nl',2,6,'ds','2013-11-13 00:00:00'),(52,0,'Guest','dinesh@devrepublic.nl',2,6,'sa','2013-11-13 00:00:00'),(53,0,'Guest','dinesh@devrepublic.nl',2,4,'ds','2013-11-13 00:00:00'),(54,0,'Guest','dinesh@devrepublic.nl',2,6,'tes','2013-11-13 00:00:00'),(55,0,'Guest','dinesh@devrepublic.nl',2,3,'test','2013-11-13 00:00:00'),(56,0,'Guest','dinesh@devrepublic.nl',2,5,'tets','2013-11-13 00:00:00'),(57,0,'Guest','dinesh@devrepublic.nl',2,5,'xc','2013-11-13 00:00:00'),(58,1,'Soyab Rana','dinesh@devrepublic.nl',2,4,'xc','2013-11-13 00:00:00');
/*!40000 ALTER TABLE `ins_feedback_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_form_fields`
--

DROP TABLE IF EXISTS `ins_form_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_form_fields` (
  `field_id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(45) DEFAULT NULL,
  `name_on_form` varchar(45) DEFAULT NULL,
  `field_type` enum('P','B','E','N','S') NOT NULL DEFAULT 'N',
  `form_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`field_id`),
  KEY `form_id_idx` (`form_id`),
  CONSTRAINT `form_id` FOREIGN KEY (`form_id`) REFERENCES `ins_forms` (`form_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=402 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_form_fields`
--

LOCK TABLES `ins_form_fields` WRITE;
/*!40000 ALTER TABLE `ins_form_fields` DISABLE KEYS */;
INSERT INTO `ins_form_fields` VALUES (6,'Email','Email','E',13),(10,'Naam','Naam','N',13),(11,'Name:','Name:','N',18),(12,'Address:','Address:','N',18),(13,'Policy Number:','Policy Number:','N',18),(14,'Verzekeraar SB EP','Verzekeraar','N',14),(15,'Schadenummer verzekeraar SB EP','Schadenummer','N',14),(16,'Schadenummer SB EP','Schadenummer 2','N',14),(17,'Naam SB EP','Naam verzekeraar','N',14),(20,'Plaats SB EP','Woonplaats','N',14),(21,'Schadedatum SB EP','Schadedatum','N',14),(22,'Soort verzekering SB EP','Soort verzekering','N',14),(29,'Verzekeraar SB EP','Verzekeraar','N',24),(30,'Schadenummer verzekeraar SB EP','Schadenummer verzekeraar','N',24),(31,'Schadenummer SB EP','Schadenummer','',24),(32,'Soort verzekering SB EP','Soort verzekering','N',24),(33,'Polisnummer SB EP','Polisnummer','P',24),(34,'Naam SB EP','Naam','N',24),(36,'Naam SB EP','Naam','N',24),(37,'Postadres SB EP','Adres','N',24),(38,'Postcode SB EP','Postcode','N',24),(39,'Plaats SB EP','Plaats','N',24),(40,'Land SB EP','Land','N',24),(41,'Telefoon priv relatie','Telefoon priv','N',24),(42,'Telefoon zakelijk relatie','Telefoon zakelijk','N',24),(43,'Telefoon mobiel relatie','Telefoon mobiel','N',24),(45,'Bankrekening relatie','Bankrekening','N',24),(46,'Schadedatum SB EP','Schadedatum','N',24),(47,'Schade gemeld op SB EP','Schade gemeld op','N',24),(48,'Locatie schade SB EP','Locatie schade','N',24),(52,'Titulatuur persoon','Titulatuur','N',53),(53,'Titel(s) persoon','Titel(s)','N',53),(54,'Naam persoon','Naam','N',53),(55,'Postadres persoon','Postadres','N',53),(56,'Postcode postadres persoon','Postcode','N',53),(57,'Plaats postadres persoon','Plaats','N',53),(58,'Land postadres persoon','Land','N',53),(59,'Bezoekadres persoon','Bezoekadres','N',53),(60,'Postcode bezoekadres persoon','Postcode','N',53),(61,'Plaats bezoekadres persoon','Plaats','N',53),(62,'Land bezoekadres persoon','Land','N',53),(63,'Geboortedatum persoon','Geboortedatum','N',53),(64,'Geboorteplaats persoon','Geboorteplaats','N',53),(65,'Geboorteland persoon','Geboorteland','N',53),(66,'Nationaliteit persoon','Nationaliteit','N',53),(67,'Geslacht persoon','Geslacht','N',53),(68,'Burger Service Nummer persoon','Burger Service Nummer','N',53),(69,'Burgerlijke staat persoon','Burgerlijke staat','N',53),(70,'Beroep soort persoon','Beroep soort','N',53),(71,'Beroep omschrijving persoon','Beroep omschrijving','N',53),(72,'Huisvesting persoon','Huisvesting','N',53),(73,'Instantie instantie','Instantie','N',53),(74,'KvK nummer instantie','KvK nummer','N',53),(75,'KvK plaats instantie','KvK plaats','N',53),(76,'Rechtsvorm instantie','Rechtsvorm','N',53),(77,'Oprichtingsdatum instantie','Oprichtingsdatum','N',53),(78,'Hoedanigheid instantie','Hoedanigheid','N',53),(79,'Huisvesting instantie','Huisvesting','N',53),(80,'BTW nummer instantie','BTW nummer','N',53),(81,'BIK nummer instantie','BIK nummer','N',53),(82,'SBI code hoofdactiviteit instantie','SBI code hoofdactiviteit','N',53),(83,'SBI code nevenactiviteit instantie','SBI code nevenactiviteit','N',53),(84,'Titulatuur contactpersoon instantie','Titulatuur','N',53),(85,'Titel(s) contactpersoon instantie','Titel(s)','N',53),(86,'Naam contactpersoon instantie','Naam','N',53),(87,'Geslacht contactpersoon instantie','Geslacht contactpersoon','N',53),(88,'Functie contactpersoon instantie','Functie','N',53),(90,'Toelichting algemeen','Toelichting','N',53),(91,'Website relatie',' relatie','N',53),(92,'Telefoon prive relatie','Telefoon prive','N',53),(93,'Omschrijving telefoon prive relatie','Omschrijving telefoon prive','N',53),(94,'Telefoon zakelijk relatie','Telefoon zakelijk','N',53),(95,'Omschrijving telefoon zakelijk relatie','Omschrijving telefoon zakelijk','N',53),(96,'Telefoon fax relatie','Telefoon fax','N',53),(97,'Omschrijving telefoon fax relatie','Omschrijving telefoon fax','N',53),(98,'Telefoon mobiel relatie','Telefoon mobiel','N',53),(99,'Omschrijving telefoon mobiel relatie','Omschrijving telefoon mobiel','N',53),(101,'Correspondentie via email relatie','Correspondentie via email','N',53),(102,'Bankrekening relatie','Bankrekening','N',53),(103,'IBAN nummer relatie','IBAN nummer','N',53),(104,'Tenaamstelling bankrekening relatie','Tenaamstelling','N',53),(105,'Gebruiken voor bankrekening relatie','Gebruiken voor bankrekening','N',53),(106,'Titulatuur verzekerde','Titulatuur','N',53),(107,'Titel(s) verzekerde','Titel(s)','N',53),(108,'Naam verzekerde','Naam','N',53),(109,'Postadres verzekerde','Postadres','N',53),(110,'Postcode verzekerde','Postcode','N',53),(111,'Plaats verzekerde','Plaats','N',53),(112,'Land verzekerde','Land','N',53),(114,'Titulatuur contactpersoon verzekerde','Titulatuur','N',53),(115,'Titel(s) contactpersoon verzekerde','Titel(s)','N',53),(116,'Naam contactpersoon verzekerde','Naam','N',53),(117,'Functie contactpersoon verzekerde','Functie','N',53),(119,'Type contract assu','Type contract','N',53),(120,'Omschrijving contract assu','Omschrijving','N',53),(121,'Ingangsdatum contract assu','Ingangsdatum','N',53),(122,'Verzekeraar contract assu','Verzekeraar','N',53),(123,'Pakketnummer','Pakketnummer','N',53),(124,'Soort pakket','Soort pakket','N',53),(125,'Premei pakket','Premie pakket','N',53),(126,'Betaaltermijn pakket','Betaaltermijn','N',53),(127,'Collectief contractnummer','Collectief contractnummer','N',53),(128,'Soort collectief contract','Soort collectief contract','N',53),(129,'Hoofdvervaldatum collectief contract','Hoofdvervaldatum','N',53),(130,'Polisnummer assu','Polisnummer','N',53),(131,'Soort polis assu','Soort polis','N',53),(132,'Branche','Branche','N',53),(133,'Premie polis','Premie polis','N',53),(134,'Betaaltermijn polis','Betaaltermijn','N',53),(135,'Hoofdvervaldatum polis','Hoofdvervaldatum','N',53),(136,'Wijzigingsdatum polis','Wijzigingsdatum','N',53),(137,'Verzekerd object polis','Verzekerd object','N',53),(138,'Omschrijving object polis','Omschrijving','N',53),(139,'Kenteken object polis','Kenteken object','N',53),(140,'Meldcode object polis','Meldcode','N',53),(141,'Dekking polis','Dekking','N',53),(142,'Omschrijving dekking polis','Omschrijving','N',53),(143,'Verzekerd bedrag dekking polis','Verzekerd bedrag','N',53),(144,'Kortingstrede NC/BM polis','Kortingstrede NC/BM','N',53),(145,'Percentage NC/BM polis','Percentage NC/BM','N',53),(146,'Partij polis','Partij','N',53),(147,'Omschrijving partij polis','Omschrijving','N',53),(148,'Titulatuur rekeninghouder','Titulatuur','N',53),(149,'Titel(s) rekeninghouder','Titel(s)','N',53),(150,'Naam rekeninghouder','Naam','N',53),(151,'Adres rekeninghouder','Adres','N',53),(152,'Postcode rekeninghouder','Postcode','N',53),(153,'Plaats rekeninghouder','Plaats','N',53),(154,'Land rekeninghouder','Land','N',53),(156,'Titulatuur contactpersoon rekeninghouder','Titulatuur','N',53),(157,'Titel(s) contactpersoon rekeninghouder','Titel(s)','N',53),(158,'Naam contactpersoon rekeninghouder','Naam','N',53),(159,'Functie contactpersoon rekeninghouder','Functie','N',53),(161,'Type contract bancair','Type contract','N',53),(162,'Omschrijving contract bancair','Omschrijving','N',53),(163,'Ingangsdatum contract bancair','Ingangsdatum','N',53),(164,'Verstrekker contract bancair','Verstrekker','N',53),(165,'Bancair contractnummer contract bancair','Bancair contractnummer','N',53),(166,'Soort contract bancair','Soort contract','N',53),(167,'Betaaltermijn contract bancair','Betaaltermijn','N',53),(168,'Titulatuur debiteur assu','Titulatuur','N',53),(169,'Titel(s) debiteur assu','Titel(s)','N',53),(170,'Naam debiteur assu','Naam','N',53),(171,'Postadres debiteur assu','Adres','N',53),(172,'Postcode debiteur assu','Postcode','N',53),(173,'Plaats debiteur assu','Plaats','N',53),(174,'Land debiteur assu','Land','N',53),(175,'Titulatuur contactpersoon debiteur assu','Titulatuur','N',53),(176,'Titel(s) contactpersoon debiteur assu','Titel(s)','N',53),(177,'Naam contactpersoon debiteur assu','Naam','N',53),(178,'Functie contactpersoon debiteur assu','Functie','N',53),(179,'Omschrijving contract dienst','Omschrijving','N',53),(180,'Ingangsdatum contract dienst','Ingangsdatum','N',53),(181,'Dienstnummer contract dienst','Dienstnummer','N',53),(182,'Type dienst contract dienst','Type dienst','N',53),(183,'Soort dienst contract dienst','Soort dienst','N',53),(184,'Betaaltermijn contract dienst','Betaaltermijn','N',53),(185,'Termijnbedrag contract dienst','Termijnbedrag','N',53),(186,'Titulatuur SB EP','Titulatuur','N',53),(187,'Titel(s) SB EP','Titel(s)','N',53),(188,'Naam SB EP','Naam','N',53),(189,'Adres SB EP','Adres','N',53),(190,'Postcode SB EP','Postcode','N',53),(191,'Plaats SB EP','Plaats','N',53),(192,'Land SB EP','Land','N',53),(193,'Titulatuur contactpersoon SB EP','Titulatuur','N',53),(194,'Titel(s) contactpersoon SB EP','Titel(s)','N',53),(195,'Naam contactpersoon SB EP','Naam','N',53),(196,'Functie contactpersoon SB EP','Functie','N',53),(197,'Polisnummer SB EP','Polisnummer','N',53),(198,'Soort verzekering SB EP','Soort verzekering','N',53),(199,'Schadenummer SB EP','Schadenummer','N',53),(200,'Schadedatum SB EP','Schadedatum','N',53),(201,'Schade gemeld op SB EP','Schade gemeld op','N',53),(202,'Locatie SB EP','Locatie','N',53),(203,'Soort schade SB EP','Soort schade','N',53),(204,'Omschrijving SB EP','Omschrijving','N',53),(205,'Geschat bedrag SB EP','Geschat bedrag','N',53),(206,'Verzekeraar SB EP','Verzekeraar','N',53),(207,'Omschrijving object SB EP','Omschrijving','N',53),(208,'Omschrijving schadeobject SB EP','Omschrijving schadeobject','N',53),(209,'Kenteken SB EP','Kenteken','N',53),(210,'Titulatuur SB TP','Titulatuur','N',53),(211,'Titel(s) SB TP','Titel(s)','N',53),(212,'Naam SB TP','Naam','N',53),(213,'Adres SB TP','Adres','N',53),(214,'Postcode SB TP','Postcode','N',53),(215,'Plaats SB TP','Plaats','N',53),(216,'Land SB TP','Land','N',53),(217,'Titulatuur contactpersoon SB TP','Titulatuur','N',53),(218,'Titel(s) contactpersoon SB TP','Titel(s)','N',53),(219,'Naam contactpersoon SB TP','Naam','N',53),(220,'Functie contactpersoon SB TP','Functie','N',53),(221,'Schadenummer SB TP','Schadenummer','N',53),(222,'Schadedatum SB TP','Schadedatum','N',53),(223,'Locatie SB TP','Locatie','N',53),(224,'Soort schade SB TP','Soort schade','N',53),(225,'Omschrijving SB TP','Omschrijving','N',53),(226,'Geschat bedrag SB TP','Geschat bedrag','N',53),(227,'Verzekeraar SB TP','Verzekeraar','N',53),(228,'Omschrijving object SB TP','Omschrijving','N',53),(229,'Omschrijving schadeobject SB TP','Omschrijving schadeobject','N',53),(230,'Kenteken SB TP','Kenteken','N',53),(231,'Titulatuur SU EP','Titulatuur','N',53),(232,'Titel(s) SU EP','Titel(s)','N',53),(233,'Naam SU EP','Naam','N',53),(234,'Adres SU EP','Adres','N',53),(235,'Postcode SU EP','Postcode','N',53),(236,'Plaats SU EP','Plaats','N',53),(237,'Land SU EP','Land','N',53),(238,'Titulatuur contactpersoon SU EP','Titulatuur','N',53),(239,'Titel(s) contactpersoon SU EP','Titel(s)','N',53),(240,'Naam contactpersoon SU EP','Naam','N',53),(241,'Functie contactpersoon SU EP','Functie','N',53),(242,'Polisnummer SU EP','Polisnummer','N',53),(243,'Soort verzekering SU EP','Soort verzekering','N',53),(244,'Schadenummer SU EP','Schadenummer','N',53),(245,'Schadedatum SU EP','Schadedatum','N',53),(246,'Soort schade SU EP','Soort schade','N',53),(247,'Omschrijving SU EP','Omschrijving','N',53),(248,'Schadebedrag SU EP','Schadebedrag','N',53),(249,'Eigen risico SU EP','Eigen risico','N',53),(250,'Verzekeraar SU EP','Verzekeraar','N',53),(251,'Titulatuur SU TP','Titulatuur','N',53),(252,'Titel(s) SU TP','Titel(s)','N',53),(253,'Naam SU TP','Naam','N',53),(254,'Adres SU TP','Adres','N',53),(255,'Postcode SU TP','Postcode','N',53),(256,'Plaats SU TP','Plaats','N',53),(257,'Land SU TP','Land','N',53),(258,'Titulatuur contactpersoon SU TP','Titulatuur','N',53),(259,'Titel(s) contactpersoon SU TP','Titel(s)','N',53),(260,'Naam contactpersoon SU TP','Naam','N',53),(261,'Functie contactpersoon SU TP','Functie','N',53),(262,'Schadenummer SU TP','Schadenummer','N',53),(263,'Schadenummer SU TP','Schadenummer','N',53),(264,'Schadedatum SU TP','Schadedatum','N',53),(265,'Omschrijving SU TP','Omschrijving','N',53),(266,'Schadebedrag SU TP','Schadebedrag','N',53),(267,'Verzekeraar SU TP','Verzekeraar','N',53),(268,'Naam verzekerde','Naam','N',54),(269,'Polisnummer assu','Polisnummer','P',54),(270,'Verzekeraar contract assu','Verzekeraar','N',54),(271,'Kenteken object polis','Kenteken','N',54),(272,'Meldcode object polis','Meldcode','N',54),(273,'Behandelaar','Behandelaar','B',54),(277,'Email','Email','E',53),(279,'Email','Email','E',24),(281,'Schadenummer SB EP','Schadenummer','N',59),(282,'Polisnummer SB EP','Polisnummer','N',59),(283,'Naam SB EP','Naam','N',59),(284,'Geboortedatum persoon','Geboortedatum','N',59),(285,'Geslacht persoon','Geslacht','N',59),(286,'Beroep soort persoon','Beroep soort','N',59),(287,'Beroep omschrijving persoon','Beroep omschrijving','N',59),(288,'Postadres persoon','Postadres','N',59),(289,'Postcode postadres persoon','Postcode','N',59),(290,'Plaats postadres persoon','Plaats','N',59),(291,'Telefoon prive relatie','Telefoon prive','N',59),(292,'Bankrekening relatie','Bankrekening','N',59),(293,'Email','Email','E',59),(294,'Email','Email','E',54),(295,'Datum ingang verzekering','Datum ingang verzekering','N',13),(296,'Polisnummer assu','Polisnummer assu','P',13),(297,'Verzekeraar contract assu','Verzekeraar contract assu','N',13),(298,'Kenteken object polis','Kenteken object polis','N',13),(299,'Meldcode object polis','Meldcode object polis','N',13),(300,'Behandelaar','Behandelaar','B',13),(301,'Datum formulier','Datum formulier','N',13),(304,'email','fghfghf','E',14),(311,'Datum formulier','Datum formulier','N',61),(312,'Betreft','Betreft','N',61),(313,'Datum ingang verzekering','Datum ingang verzekering','N',61),(314,'Titulatuur persoon','Titulatuur persoon','N',61),(315,'Naam persoon','Naam','N',61),(316,'Geboortedatum persoon','Geboortedatum','N',61),(317,'Postadres persoon','Adres','N',61),(318,'Postcode postadres persoon','Postcode','N',61),(319,'Plaats postadres persoon','Plaats','N',61),(320,'Email','Email','E',61),(321,'Polisnummer assu','Polisnummer','P',61),(322,'Behandelaar','Behandelaar','B',61),(326,'Telefoon behandelaar','Telefoon behandelaar','N',61),(327,'Behandelaarmail','Email behandelaar','N',61),(329,'Behandelaar','Schadebehandelaar','B',24),(330,'Verzekeraar SB EP','Verzekeraar','N',66),(331,'Schadenummer verzekeraar SB EP','Schadenummer verzekeraar','N',66),(332,'Schadenummer SB EP','Schadenummer','',66),(333,'Soort verzekering SB EP','Soort verzekering','N',66),(334,'Polisnummer SB EP','Polisnummer','P',66),(335,'Naam SB EP','Naam','N',66),(336,'Postadres SB EP','Adres','N',66),(337,'Postcode SB EP','Postcode','N',66),(338,'Plaats SB EP','Plaats','N',66),(339,'Land SB EP','Land','N',66),(340,'Telefoon prive relatie','Telefoon prive','N',66),(341,'Telefoon zakelijk relatie','Telefoon zakelijk','N',66),(342,'Telefoon mobiel relatie','Telefoon mobiel','N',66),(343,'Email','Email','E',66),(344,'Bankrekening relatie','Bankrekening','N',66),(345,'Schadedatum SB EP','Schadedatum','N',66),(346,'Schade gemeld op SB EP','Schade gemeld op','N',66),(347,'Locatie schade SB EP','Locatie schade','N',66),(348,'Behandelaar','Behandelaar','B',66),(349,'Behandelaar telefoon','Behandelaar telefoon','N',66),(350,'Behandelaar email','Behandelaar email','N',66),(351,'Verzekeraar SB EP','Verzekeraar','N',67),(352,'Schadenummer verzekeraar SB EP','Schadenummer verzekeraar','N',67),(353,'Soort verzekering SB EP','Soort verzekering','N',67),(354,'Polisnummer SB EP','Polisnummer','P',67),(355,'Schadenummer SB EP','Schadenummer','',67),(356,'Naam SB EP','Naam','N',67),(357,'Postadres SB EP','Adres','N',67),(358,'Postcode SB EP','Postcode','N',67),(359,'Plaats SB EP','Plaats','N',67),(360,'Land SB EP','Land','N',67),(361,'Telefoon prive relatie','Telefoon prive','N',67),(362,'Telefoon zakelijk relatie','Telefoon zakelijk','N',67),(363,'Telefoon mobiel relatie','Telefoon mobiel','N',67),(364,'Email','Email','E',67),(365,'Bankrekening relatie','Bankrekening','N',67),(366,'Schadedatum SB EP','Schadedatum','N',67),(367,'Schade gemeld op SB EP','Schade gemeld op','N',67),(368,'Locatie schade SB EP','Locatie schade','N',67),(369,'Behandelaar','Behandelaar','B',67),(370,'Behandelaar telefoon','Behandelaar telefoon','N',67),(371,'Behandelaar email','Behandelaar email','N',67),(372,'Verzekeraar SB EP','Verzekeraar','N',68),(373,'Schadenummer verzekeraar SB EP','Schadenummer verzekeraar','N',68),(374,'Schadenummer SB EP','Schadenummer','',68),(375,'Soort verzekering SB EP','Soort verzekering','N',68),(376,'Polisnummer SB EP','Polisnummer','P',68),(377,'Naam SB EP','Naam','N',68),(378,'Postadres SB EP','Adres','N',68),(379,'Postcode SB EP','Postcode','N',68),(380,'Plaats SB EP','Plaats','N',68),(381,'Land SB EP','Land','N',68),(382,'Telefoon prive relatie','Telefoon prive','N',68),(383,'Telefoon zakelijk relatie','Telefoon zakelijk','N',68),(384,'Telefoon mobiel relatie','Telefoon mobiel','N',68),(385,'Email','Email','E',68),(386,'Bankrekening relatie','Bankrekening','N',68),(387,'Schadedatum SB EP','Schadedatum','N',68),(388,'Schade gemeld op SB EP','Schade gemeld op','N',68),(389,'Locatie schade SB EP','Locatie schade','N',68),(390,'Behandelaar','Behandelaar','B',68),(391,'Behandelaar telefoon','Behandelaar telefoon','N',68),(392,'Behandelaar email','Behandelaar email','N',68),(395,'test1 normal','test1 normal','N',15),(396,'test2 Email','test2 Ema','E',15),(397,'test3 Schadenummer ','test3 Schadenummer ','S',15),(398,'test4 Behandelaar ','test4 Behandelaar ','B',15),(399,'test5 Polisnummer ','test5 Polisnummer ','N',15),(400,'Email','Email','E',92),(401,'Email','Email','E',93);
/*!40000 ALTER TABLE `ins_form_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_form_fields_values`
--

DROP TABLE IF EXISTS `ins_form_fields_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_form_fields_values` (
  `filed_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(65) NOT NULL,
  `name_at_form` varchar(65) DEFAULT NULL,
  `filed_value` varchar(65) DEFAULT NULL,
  `claim_id` int(11) NOT NULL,
  `old_field_value` varchar(65) DEFAULT NULL,
  `is_change` int(11) DEFAULT NULL,
  PRIMARY KEY (`filed_value_id`),
  KEY `claim_id` (`claim_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_form_fields_values`
--

LOCK TABLES `ins_form_fields_values` WRITE;
/*!40000 ALTER TABLE `ins_form_fields_values` DISABLE KEYS */;
INSERT INTO `ins_form_fields_values` VALUES (1,'Email','Email','dinesh@devrepublic.nl',1,NULL,NULL),(2,'Naam','Naam',NULL,1,NULL,NULL),(3,'Datum ingang verzekering','Datum ingang verzekering','Conform bijgaande offerte',1,NULL,NULL),(4,'Polisnummer assu','Polisnummer assu','2000',1,NULL,NULL),(5,'Verzekeraar contract assu','Verzekeraar contract assu',NULL,1,NULL,NULL),(6,'Kenteken object polis','Kenteken object polis',NULL,1,NULL,NULL),(7,'Meldcode object polis','Meldcode object polis',NULL,1,NULL,NULL),(8,'Behandelaar','Behandelaar',NULL,1,NULL,NULL),(9,'Datum formulier','Datum formulier','TIME \\@ \"d MMMM yyyy\" 6 juni 2013',1,NULL,NULL),(10,'Email','Email','dinesh@devrepublic.nl',65,'manish@devrepublic.nl',1),(11,'Naam','Naam',NULL,65,NULL,NULL),(12,'Datum ingang verzekering','Datum ingang verzekering','Conform bijgaande offerte',65,NULL,NULL),(13,'Polisnummer assu','Polisnummer assu','2000',65,NULL,NULL),(14,'Verzekeraar contract assu','Verzekeraar contract assu',NULL,65,NULL,NULL),(15,'Kenteken object polis','Kenteken object polis',NULL,65,NULL,NULL),(16,'Meldcode object polis','Meldcode object polis',NULL,65,NULL,NULL),(17,'Behandelaar','Behandelaar',NULL,65,NULL,NULL),(18,'Datum formulier','Datum formulier','6 juni 2013',65,NULL,NULL),(19,'Email','Email','dinesh@devrepublic.nl',66,NULL,NULL),(20,'Naam','Naam',NULL,66,NULL,NULL),(21,'Datum ingang verzekering','Datum ingang verzekering','Conform bijgaande offerte',66,NULL,NULL),(22,'Polisnummer assu','Polisnummer assu','2000123',66,'2000',1),(23,'Verzekeraar contract assu','Verzekeraar contract assu',NULL,66,NULL,NULL),(24,'Kenteken object polis','Kenteken object polis',NULL,66,NULL,NULL),(25,'Meldcode object polis','Meldcode object polis',NULL,66,NULL,NULL),(26,'Behandelaar','Behandelaar',NULL,66,NULL,NULL),(27,'Datum formulier','Datum formulier','11/11/2013',66,'TIME \\@ ',1),(28,'Email','Email','soyab@devrepublic.nl',67,NULL,NULL),(29,'Naam','Naam',NULL,67,NULL,NULL),(30,'Datum ingang verzekering','Datum ingang verzekering','Conform bijgaande offerte',67,NULL,NULL),(31,'Polisnummer assu','Polisnummer assu','2000',67,NULL,NULL),(32,'Verzekeraar contract assu','Verzekeraar contract assu',NULL,67,NULL,NULL),(33,'Kenteken object polis','Kenteken object polis',NULL,67,NULL,NULL),(34,'Meldcode object polis','Meldcode object polis',NULL,67,NULL,NULL),(35,'Behandelaar','Behandelaar',NULL,67,NULL,NULL),(36,'Datum formulier','Datum formulier','TIME \\@ ',67,'TIME \\@ \"d MMMM yyyy\" 6 juni 2013',1),(37,'Email','Email','jayesh@devrepublic.nl',71,NULL,1),(38,'Email','Email','dinesh@devrepublic.nl',72,NULL,NULL),(39,'Email','Email',NULL,73,NULL,NULL),(40,'Email','Email','dinesh@devrepublic.nl',74,NULL,NULL),(41,'Email','Email','dinesh@devrepublic.nl',75,NULL,NULL),(42,'Email','Email','dinesh@devrepublic.nl',76,NULL,NULL),(43,'Email','Email','dinesh@devrepublic.nl',77,NULL,NULL),(44,'Email','Email','dinesh@devrepublic.nl',78,NULL,NULL),(45,'Email','Email','jayesh@devrepublic.nl',71,NULL,1),(46,'Naam','Naam',NULL,71,NULL,NULL),(47,'Datum ingang verzekering','Datum ingang verzekering','Conform bijgaande offerte',71,NULL,NULL),(48,'Polisnummer assu','Polisnummer assu','2000',71,NULL,NULL),(49,'Verzekeraar contract assu','Verzekeraar contract assu',NULL,71,NULL,NULL),(50,'Kenteken object polis','Kenteken object polis',NULL,71,NULL,NULL),(51,'Meldcode object polis','Meldcode object polis',NULL,71,NULL,NULL),(52,'Behandelaar','Behandelaar',NULL,71,NULL,NULL),(53,'Datum formulier','Datum formulier','TIME \\@ ',71,'TIME \\@ \"d MMMM yyyy\" 6 juni 2013',1),(54,'Email','Email','jayesh@devrepublic.nl',71,NULL,NULL),(55,'Naam','Naam',NULL,71,NULL,NULL),(56,'Datum ingang verzekering','Datum ingang verzekering','Conform bijgaande offerte',71,NULL,NULL),(57,'Polisnummer assu','Polisnummer assu','2000',71,NULL,NULL),(58,'Verzekeraar contract assu','Verzekeraar contract assu',NULL,71,NULL,NULL),(59,'Kenteken object polis','Kenteken object polis',NULL,71,NULL,NULL),(60,'Meldcode object polis','Meldcode object polis',NULL,71,NULL,NULL),(61,'Behandelaar','Behandelaar',NULL,71,NULL,NULL),(62,'Datum formulier','Datum formulier','TIME \\@ \"d MMMM yyyy\" 6 juni 2013',71,NULL,NULL),(63,'Email','Email','jayesh@devrepublic.nl',72,NULL,NULL),(64,'Naam','Naam',NULL,72,NULL,NULL),(65,'Datum ingang verzekering','Datum ingang verzekering','Conform bijgaande offerte',72,NULL,NULL),(66,'Polisnummer assu','Polisnummer assu','2000',72,NULL,NULL),(67,'Verzekeraar contract assu','Verzekeraar contract assu',NULL,72,NULL,NULL),(68,'Kenteken object polis','Kenteken object polis',NULL,72,NULL,NULL),(69,'Meldcode object polis','Meldcode object polis',NULL,72,NULL,NULL),(70,'Behandelaar','Behandelaar',NULL,72,NULL,NULL),(71,'Datum formulier','Datum formulier','TIME \\@ \"d MMMM yyyy\" 6 juni 2013',72,NULL,NULL),(72,'Email','Email','jayesh@devrepublic.nl',73,NULL,NULL),(73,'Naam','Naam',NULL,73,NULL,NULL),(74,'Datum ingang verzekering','Datum ingang verzekering','Conform bijgaande offerte',73,NULL,NULL),(75,'Polisnummer assu','Polisnummer assu','2000',73,NULL,NULL),(76,'Verzekeraar contract assu','Verzekeraar contract assu',NULL,73,NULL,NULL),(77,'Kenteken object polis','Kenteken object polis',NULL,73,NULL,NULL),(78,'Meldcode object polis','Meldcode object polis',NULL,73,NULL,NULL),(79,'Behandelaar','Behandelaar',NULL,73,NULL,NULL),(80,'Datum formulier','Datum formulier','TIME \\@ \"d MMMM yyyy\" 6 juni 2013',73,NULL,NULL);
/*!40000 ALTER TABLE `ins_form_fields_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_forms`
--

DROP TABLE IF EXISTS `ins_forms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_forms` (
  `form_id` int(11) NOT NULL AUTO_INCREMENT,
  `form_code` varchar(11) DEFAULT NULL,
  `fill_in_needed` int(2) NOT NULL,
  `client_id` int(11) NOT NULL,
  `form_name` varchar(65) NOT NULL,
  `form_tag` varchar(65) NOT NULL,
  `first_remind` int(11) DEFAULT NULL,
  `second_remind` int(11) DEFAULT NULL,
  `select_for_policyholder` int(1) NOT NULL DEFAULT '0',
  `header_text` text,
  `introduction_text` text NOT NULL,
  `closure_text` text NOT NULL,
  `show_all_question_at_once` int(1) NOT NULL DEFAULT '0',
  `current_date` date NOT NULL,
  PRIMARY KEY (`form_id`),
  KEY `client_id` (`client_id`),
  CONSTRAINT `ins_forms_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `ins_clients` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_forms`
--

LOCK TABLES `ins_forms` WRITE;
/*!40000 ALTER TABLE `ins_forms` DISABLE KEYS */;
INSERT INTO `ins_forms` VALUES (13,NULL,1,1,'Bike Insurance Form','#bike_01',1,2,1,'<p>Hi</p>\r\n<p>Name : &lt;Naam&gt;<br /><br />Email : &lt;Email&gt;</p>\r\n<p>Datum formulier : &lt;Datum formulier&gt;</p>\r\n<p><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</span></p>','<p>&nbsp;<span>Etiam sdfdsf sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu.</span></p>','<p>Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.</p>',2,'0000-00-00'),(14,NULL,1,4,'test for template','Verwijderentest',0,0,0,'<p>Naam: &lt;Verzekeraar SB EP&gt;</p>\r\n<p>Woonplaats: &lt;Plaats SB EP&gt;</p>\r\n<p>Schadenummer: &lt;Schadenummer verzekeraar SB EP&gt;</p>\r\n<p>Vul hieronder de rest van het formulier verder in. Na het invullen kunt u het formulier verzenden en opslaan.</p>','','',1,'0000-00-00'),(15,NULL,1,1,'Test Jeroen','frrdererer',0,0,0,'','','',0,'0000-00-00'),(18,NULL,1,1,'for category go to','#tag_1',NULL,NULL,0,'<p>eee</p>','<p>Introductuiob text</p>','<p>closure text</p>',0,'0000-00-00'),(24,NULL,1,4,'Schadeaangifteformulier algemeen','#SBE3001',21,42,0,'<address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\"><strong>Schade-aangifteformulier algemeen</strong></span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Algemene gegevens</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Tussenpersoon: Uiterwijk Winkel Verzekeringen</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Verzekerd bij: &lt;Verzekeraar SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadenummer verzekeraar: &lt;Schadenummer verzekeraar SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadenummer: &lt;Schadenummer SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Soort verzekering: &lt;Soort verzekering SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Polisnummer: &lt;Polisnummer SB EP&gt;</span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Verzekerde</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Naam: &lt;Naam SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Postadres: &lt;Postadres SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Postcode: &lt;Postcode SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Plaats: &lt;Plaats SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Land: &lt;Land SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon prive: &lt;Telefoon priv&eacute; relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon zakelijk: &lt;Telefoon zakelijk relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon mobiel: &lt;Telefoon mobiel relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Emailadres: &lt;Email&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Bankrekening: &lt;Bankrekening relatie&gt;</span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Schadegegevens</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadedatum: &lt;Schadedatum SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schade gemeld op: &lt;Schade gemeld op SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Locatie schade: &lt;Locatie schade SB EP&gt;</span></address>','<p><span style=\"font-size: small;\">Hieronder volgt een aantal vragen met betrekking tot uw schade.</span></p>','<p><span style=\"font-size: small;\">Verzekerde verklaart vorenstaande vragen en opgaven naar beste weten, juist en overeenkomstig de waarheid te hebben beantwoord en geen bijzonderheden met betrekking tot deze schade te hebben verzwegen. Verzekerde verklaart tevens van de inhoud van dit formulier kennis te hebben genomen.</span></p>\n<p><span style=\"font-size: small;\">De op dit formulier ingevulde en eventueel nader over te leggen persoonsgegevens kunnen worden opgenomen in de cli&euml;ntenregistratie gevoerd door de maatschappij waarbij u verzekerd bent. Deze registratie is aangemeld bij de Registratiekamer.&nbsp; Een afschrift van het aanmeldingsformulier ligt ter inzage bij deze maatschappij. De verstrekte gegevens kunnen ook worden verwerkt in het Centraal Informatie Systeem van de in Nederland werkzame verzekeringsmaatschappijen. Aanmelding van en omvang van de schade deze registratie bij de Registratiekamer is gedaan op 23.04.1990. Een afschrift van het formulier van aanmelding ligt voor een ieder ter inzage bij Stichting CIS, Crabethpark 23, 2801 AP Gouda.</span></p>',2,'0000-00-00'),(53,NULL,1,4,'Alle Dias rubrieken','#ZDOM-REL',1,2,0,'<p>Basisformulier vanuit relatiegegevens</p>','','',0,'0000-00-00'),(54,NULL,0,4,'Dekkingsbevestiging Motorrijtuigen','#PVR2002',0,0,0,'<p><span style=\"font-size: small;\">Geachte relatie, </span><br /><br /><span style=\"font-size: small;\">Hierbij delen wij u mede, dat wij per heden de ondervermelde auto in dekking hebben genomen: </span><br /><span style=\"font-size: small;\">Naam: &lt;Naam verzekerde&gt; </span><br /><span style=\"font-size: small;\">Email: &lt;Email&gt; </span><br /><span style=\"font-size: small;\">Polisnummer: &lt;Polisnummer assu&gt; </span><br /><span style=\"font-size: small;\">Verzekeraar: &lt;Verzekeraar contract assu&gt; </span><br /><span style=\"font-size: small;\">Kenteken: &lt;Kenteken object polis&gt; </span><br /><span style=\"font-size: small;\">Meldcode: &lt;Meldcode object polis&gt; </span><br /><span style=\"font-size: small;\">Behandelaar: &lt;Behandelaar&gt; </span><br /><br /><span style=\"font-size: small;\">Wilt u de gegevens controleren en onjuistheden direct aan ons melden? </span><br /><br /><span style=\"font-size: small;\">Met vriendelijke groet, </span><br /><span style=\"font-size: small;\">UW Assuradeuren</span></p>','<p><span style=\"font-size: small;\">Dekkingsbevestiging motorrijtuigen</span></p>','<p><span style=\"font-size: small;\">Afsluitende tekst dekkingsbevestiging motorrijtuigen</span></p>',1,'0000-00-00'),(59,NULL,1,4,'Schadeformulier Reis en Annuleringsverzekering','#SBE3002-01',0,0,0,'<address><span style=\"font-size: small;\"><strong>Schadeformulier Reis- en Annuleringsverzekering</strong></span></address><address><span style=\"font-size: small;\">&nbsp;</span></address><address><span style=\"font-size: small;\">Het schadeformulier dient zo volledig mogelijk te worden beantwoord. Het door ons gevraagde houdt geen vergoedingsverplichting in.</span></address><address><span style=\"font-size: small;\">&nbsp;</span></address><address><span style=\"font-size: small;\">Schadenummer: &lt;Schadenummer SB EP&gt;</span></address><address><span style=\"font-size: small;\">Polisnummer: &lt;Polisnummer SB EP&gt;</span></address><address><span style=\"font-size: small;\">Naam: &lt;Naam SB EP&gt;</span></address><address><span style=\"font-size: small;\">Geslacht: &lt;Geslacht persoon&gt;</span></address><address><span style=\"font-size: small;\">Geboortedatum: &lt;Geboortedatum persoon&gt;</span></address><address><span style=\"font-size: small;\">Beroep soort: &lt;Beroep soort persoon&gt;</span></address><address><span style=\"font-size: small;\">Beroep omschrijving: &lt;Beroep omschrijving persoon&gt;</span></address><address><span style=\"font-size: small;\">Postadres: &lt;Postadres persoon&gt;</span></address><address><span style=\"font-size: small;\">Postcode: &lt;Postcode postadres persoon&gt;</span></address><address><span style=\"font-size: small;\">Plaats: &lt;plaats postadres persoon&gt;</span></address><address><span style=\"font-size: small;\">Telefoon priv&eacute;: &lt;Telefoon prive relatie&gt;</span></address><address><span style=\"font-size: small;\">Bankrekening: &lt;Bankrekening relatie&gt;</span></address><address><span style=\"font-size: small;\">Email: &lt;Email&gt;</span></address>','<p><span>Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu.</span></p>','<p>Wilt u eventueel van toepassing zijnde documenten zoals aankoopnota\'s, schadebegroting, foto\'s en dergelijke meesturen? U kunt deze op de laatste pagina van het vragenformulier als bijlagen toevoegen.</p>\r\n<p><span><br />Klik op bladeren, kies een bestand en kies daarna voor Toevoegen.</span></p>',0,'0000-00-00'),(61,NULL,1,4,'VZA','#PVR2001-01',0,0,0,'<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Datum ingang verzekering: &lt;Ingangsdatum&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Betreft: &lt;Betreft&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Titulatuur: &lt;Titulatuur persoon&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Naam: &lt;Naam persoon&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Geboortedatum: &lt;Geboortedatum persoon&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Email: &lt;Email&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Postadres: &lt;Postadres persoon&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Postcode: &lt;Postcode postadres persoon&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Plaats: &lt;Plaats postadres persoon&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Polisnummer: &lt;polisnummer assu&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Behandelaar: &lt;behandelaar&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon behandelaar: &lt;Telefoon behandelaar&gt;</span></p>\r\n<p class=\"full\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Email behandelaar: &lt;Behandelaarmail&gt;</span></p>','<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">U kunt het formulier openen door op de datum te klikken. Nadat u het formulier heeft ingevuld kunt u dit als concept opslaan om op een later moment aanpassingen te doen of een bijlage toe te voegen.</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Op het moment, dat u de knop bevestig en verstuur heeft aangeklikt kan het formulier niet meer worden bewerkt, maar nog wel worden ingezien.</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">U krijgt hiervan een bevestiging via email.</span></p>','<p class=\"left\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Wij verzoeken ou om uw&nbsp;gegevens te controleren voordat u het formulier opslaat en verstuurd.</span></p>',0,'0000-00-00'),(66,NULL,1,4,'Saf Aansprakelijkheid','#SBE3003',21,42,0,'<address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\"><strong>Schade-aangifteformulier algemeen</strong></span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Algemene gegevens</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Tussenpersoon: Uiterwijk Winkel Verzekeringen</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Verzekerd bij: &lt;Verzekeraar SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadenummer verzekeraar: &lt;Schadenummer verzekeraar SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadenummer: &lt;Schadenummer SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Soort verzekering: &lt;Soort verzekering SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Polisnummer: &lt;Polisnummer SB EP&gt;</span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Verzekerde</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Naam: &lt;Naam SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Postadres: &lt;Postadres SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Postcode: &lt;Postcode SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Plaats: &lt;Plaats SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Land: &lt;Land SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon prive: &lt;Telefoon priv&eacute; relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon zakelijk: &lt;Telefoon zakelijk relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon mobiel: &lt;Telefoon mobiel relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Emailadres: &lt;Email&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Bankrekening: &lt;Bankrekening relatie&gt;</span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Schadegegevens</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadedatum: &lt;Schadedatum SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schade gemeld op: &lt;Schade gemeld op SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Locatie schade: &lt;Locatie schade SB EP&gt;</span></address><address><span style=\"font-family: arial,helvetica,sans-serif; font-size: small;\"><strong>Behandelaar</strong><br />Behandelaar: &lt;&lt;Behandelaar&gt;<br />Behandelaar telefoon: &lt;Behandelaar telefoon&gt;<br />Behandelaar email: &lt;Behandelaar email&gt;</span></address>','','<p>Wilt u eventueel van toepassing zijnde documenten zoals aankoopnota\'s, schadebegroting, foto\'s en dergelijke meesturen? U kunt deze op de laatste pagina van het vragenformulier als bijlagen toevoegen.</p>',2,'0000-00-00'),(67,NULL,1,4,'Saf Mobiele apparatuur','#SBE3004',21,42,0,'<address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\"><strong>Schade-aangifteformulier algemeen</strong></span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Algemene gegevens</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Tussenpersoon: Uiterwijk Winkel Verzekeringen</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Verzekerd bij: &lt;Verzekeraar SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadenummer verzekeraar: &lt;Schadenummer verzekeraar SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadenummer: &lt;Schadenummer SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Soort verzekering: &lt;Soort verzekering SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Polisnummer: &lt;Polisnummer SB EP&gt;</span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Verzekerde</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Naam: &lt;Naam SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Postadres: &lt;Postadres SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Postcode: &lt;Postcode SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Plaats: &lt;Plaats SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Land: &lt;Land SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon prive: &lt;Telefoon priv&eacute; relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon zakelijk: &lt;Telefoon zakelijk relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon mobiel: &lt;Telefoon mobiel relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Emailadres: &lt;Email&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Bankrekening: &lt;Bankrekening relatie&gt;</span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Schadegegevens</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadedatum: &lt;Schadedatum SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schade gemeld op: &lt;Schade gemeld op SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Locatie schade: &lt;Locatie schade SB EP&gt;</span></address><address><span style=\"font-family: arial,helvetica,sans-serif; font-size: small;\"><strong>Behandelaar</strong><br />Behandelaar: &lt;&lt;Behandelaar&gt;<br />Behandelaar telefoon: &lt;Behandelaar telefoon&gt;<br />Behandelaar email: &lt;Behandelaar email&gt;</span></address>','','',2,'0000-00-00'),(68,NULL,1,4,'Saf Waterschade','#3005',21,42,0,'<address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\"><strong>Schade-aangifteformulier algemeen</strong></span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Algemene gegevens</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Tussenpersoon: Uiterwijk Winkel Verzekeringen</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Verzekerd bij: &lt;Verzekeraar SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadenummer verzekeraar: &lt;Schadenummer verzekeraar SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadenummer: &lt;Schadenummer SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Soort verzekering: &lt;Soort verzekering SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Polisnummer: &lt;Polisnummer SB EP&gt;</span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Verzekerde</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Naam: &lt;Naam SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Postadres: &lt;Postadres SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Postcode: &lt;Postcode SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Plaats: &lt;Plaats SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Land: &lt;Land SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon prive: &lt;Telefoon priv&eacute; relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon zakelijk: &lt;Telefoon zakelijk relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Telefoon mobiel: &lt;Telefoon mobiel relatie&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Emailadres: &lt;Email&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Bankrekening: &lt;Bankrekening relatie&gt;</span></address><address><span style=\"font-size: small;\"><strong><span style=\"font-family: arial, helvetica, sans-serif;\">Schadegegevens</span></strong></span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schadedatum: &lt;Schadedatum SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Schade gemeld op: &lt;Schade gemeld op SB EP&gt;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Locatie schade: &lt;Locatie schade SB EP&gt;</span></address><address><span style=\"font-family: arial,helvetica,sans-serif; font-size: small;\"><strong>Behandelaar</strong><br />Behandelaar: &lt;&lt;Behandelaar&gt;<br />Behandelaar telefoon: &lt;Behandelaar telefoon&gt;<br />Behandelaar email: &lt;Behandelaar email&gt;</span></address>','','',2,'0000-00-00'),(92,NULL,1,1,'Health Insurance','#007',1,2,0,'','','',0,'0000-00-00'),(93,'hArRn',1,4,'Health Insurance','#007',0,0,0,'','','',0,'2013-12-13'),(94,'xCYdi',1,1,'test form','#0006',1,2,0,'<p>test text</p>','<p>test text</p>','<p>test text</p>',1,'2014-01-28');
/*!40000 ALTER TABLE `ins_forms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_forms_answers`
--

DROP TABLE IF EXISTS `ins_forms_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_forms_answers` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_text` text,
  `answer_type` varchar(20) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `prev_question` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`),
  KEY `claim_id_idx` (`claim_id`),
  KEY `cat_id_idx` (`cat_id`),
  KEY `fk_question_id` (`question_id`),
  CONSTRAINT `ins_forms_answers_ibfk_1` FOREIGN KEY (`claim_id`) REFERENCES `ins_claims` (`claim_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ins_forms_answers_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `ins_forms_categories_question` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ins_forms_answers_ibfk_3` FOREIGN KEY (`cat_id`) REFERENCES `ins_forms_categories` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_forms_answers`
--

LOCK TABLES `ins_forms_answers` WRITE;
/*!40000 ALTER TABLE `ins_forms_answers` DISABLE KEYS */;
INSERT INTO `ins_forms_answers` VALUES (48,70,128,'zxc','text',66,0),(49,70,566,'459','radio',66,128),(50,70,132,NULL,'checkbox',66,566),(51,70,829,'09-12-2013','date',66,132),(52,70,830,'123','number',66,829),(53,70,831,'asdf','text',66,830),(54,70,861,'756','radio',66,831),(55,70,102,NULL,'checkbox',52,861),(56,70,860,'09-12-2013','date',52,102),(57,70,115,'we','textarea',26,860),(58,70,97,'qwe','text',26,115),(59,70,616,'534','radio',26,97),(60,70,96,'qwe','text',26,616),(61,70,152,'70','radio',26,96),(62,70,615,'532','radio',26,152),(63,70,614,'530','radio',26,615),(64,69,128,'zxc','text',66,0),(65,69,566,'459','radio',66,128),(66,69,132,NULL,'checkbox',66,566),(67,69,829,'09-12-2013','date',66,132),(68,69,830,'1,12','number',66,829),(69,69,831,'asdf','text',66,830),(70,69,861,'756','radio',66,831),(71,69,102,NULL,'checkbox',52,861),(72,69,860,'08-12-2013','date',52,102),(73,69,115,'dfg','textarea',26,860),(74,69,97,'sdg','text',26,115),(75,69,616,'534','radio',26,97),(76,69,96,'sdfg','text',26,616),(77,69,152,'70','radio',26,96),(78,69,615,'532','radio',26,152),(79,69,614,'530','radio',26,615);
/*!40000 ALTER TABLE `ins_forms_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_forms_answers_details`
--

DROP TABLE IF EXISTS `ins_forms_answers_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_forms_answers_details` (
  `answer_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_answer_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  PRIMARY KEY (`answer_detail_id`),
  KEY `user_answer_id_idx` (`user_answer_id`),
  KEY `answer_id_idx` (`answer_id`),
  CONSTRAINT `ins_forms_answers_details_ibfk_1` FOREIGN KEY (`user_answer_id`) REFERENCES `ins_forms_answers` (`answer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ins_forms_answers_details_ibfk_2` FOREIGN KEY (`answer_id`) REFERENCES `ins_forms_categories_answer` (`answer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_forms_answers_details`
--

LOCK TABLES `ins_forms_answers_details` WRITE;
/*!40000 ALTER TABLE `ins_forms_answers_details` DISABLE KEYS */;
INSERT INTO `ins_forms_answers_details` VALUES (82,66,46),(83,71,6),(84,50,47),(85,55,4),(86,55,5);
/*!40000 ALTER TABLE `ins_forms_answers_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_forms_categories`
--

DROP TABLE IF EXISTS `ins_forms_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_forms_categories` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `cat_name` varchar(65) NOT NULL,
  `sequence` int(11) NOT NULL,
  `introduction_text` text NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `form_id` (`form_id`),
  CONSTRAINT `ins_forms_categories_ibfk_1` FOREIGN KEY (`form_id`) REFERENCES `ins_forms` (`form_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_forms_categories`
--

LOCK TABLES `ins_forms_categories` WRITE;
/*!40000 ALTER TABLE `ins_forms_categories` DISABLE KEYS */;
INSERT INTO `ins_forms_categories` VALUES (26,13,'Category 03',3,'<p><span>Hi</span></p>\r\n<p>Email: &lt;Email&gt;</p>\r\n<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p>'),(52,13,'Category 02',2,'<p><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</span></p>'),(66,13,'Category 01',1,'<p><span>Email: &lt;Email&gt;<br /><span><br /><span>Datum Formulier : &lt;Datum formulier&gt;</span></span><br /><br />Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</span></p>'),(67,14,'dsfdsfds',1,''),(68,14,'Derde in het spel',2,''),(69,14,'andere categorie',3,''),(70,15,'testing',2,'<p>testq</p>'),(75,18,'Cat One',1,'<p>Intro for cat 1</p>'),(76,18,'Cat two',2,'<p>Intro text for cat 2</p>'),(77,18,'cat three',3,'<p>intro for cat three</p>'),(78,18,'cat four',4,'<p>intro fot cat 4</p>'),(89,24,'Algemeen',1,'<p>Hier volgt een aantal algemene vragen met betrekking tot uw schade.</p>'),(156,54,'Algemeen',1,'<p>Blablabla</p>'),(163,59,'Algemeen',1,'<p><span style=\"font-size: small;\">Hieronder a.u.b. aankruisen welke schade is geleden.</span></p>'),(164,59,'Annulering',2,'<address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Bij annulering wegens ziekte of ongeval dient de maatschappij terstond rechtstreeks telefonisch in kennis te worden gesteld. Telefoonnummer: 0528-280500.</span></address><address><span style=\"font-size: small;\">&nbsp;</span></address><address><span style=\"font-family: arial, helvetica, sans-serif; font-size: small;\">Het boekingsformulier, de originele annuleringskostennota en andere bewijsstukken (bijvoorbeeld een uittreksel uit het register van overlijden of een overlijdenskaart) bijvoegen.</span></address>'),(165,59,'Bagage',3,'<p><span style=\"font-size: small;\">Het bezit en de waarde van de beschadigde, vermiste en/of verloren goederen dient te worden aangetoond d.m.v. originele nota\'s, duplicaatnota\'s, garantiebewijzen, bank- of giroafschriften, foto\'s etc.</span></p>\n<p><span style=\"font-size: small;\">Indien goederen te repareren/reinigen zijn, de originele reparatie/reinigingsnota\'s bijvoegen.</span></p>\n<p><span style=\"font-size: small;\">Indien goederen niet meer te repareren/reinigen zijn, dient dit aangetoond te worden d.m.v. originele deskundigenverklaringen.</span></p>'),(166,59,'Ongeval en ziekte',5,'<p><span style=\"font-size: small;\">De originele nota\'s/tickets van de extra reis-, verblijf- en telefoonkosten alsmede een specificatie bijvoegen! Beantwoord eerst, voor zover van toepassing, de volgende vragen.</span></p>'),(169,59,'Schade logiesverblijven',6,'<p><span style=\"font-size: small;\">Deze vragen gaan over logiesverblijven</span></p>'),(171,59,'Buitengewone kosten',4,'<p><span style=\"font-size: small;\">Deze vragen gaan over buitengewone kosten</span></p>'),(172,24,'Schade',2,'<p>Hier volgt een aantal specifieke&nbsp;vragen met betrekking tot de schade.</p>'),(173,24,'Pleziervaartuigen',3,'<p>Hier volgen vragen over pleziervaartuigen</p>'),(174,24,'Schadeoorzaak',4,'<p>Hier volgt een aantal&nbsp;vragen over de schadeoorzaak.</p>'),(175,24,'Schade aan anderen',5,'<address>Hier volgen vragen over schade aan anderen (aansprakelijkheid).</address><address><strong>Overlegging van ontvangen brieven, nota\\\'s en dergelijke is absoluut noodzakelijk.</strong></address>'),(182,61,'Algemeen',1,'<p><span style=\"font-family: arial,helvetica,sans-serif; font-size: small;\">Introductietekst algemeen</span></p>'),(195,66,'Algemeen',1,'<p>Hier volgt een aantal algemen vragen.</p>'),(196,66,'Schadegegevens',2,'<p>Deze vragen gaan over de schade.</p>'),(197,66,'Aansprakelijkheid',3,'<p>Deze vragen gaan over uw aansprakelijkheid voor de schade.</p>'),(201,67,'Algemeen',1,'<p>Hier volgt een aantal algemen vragen.</p>'),(202,67,'Schadegegevens',2,'<p>Deze vragen gaan over de schadegegevens.</p>'),(203,67,'Object',3,'<p>Deze vragen gaan over het object.</p>'),(204,67,'Soort schade',4,'<p>Hier kunt u de soort schade aangeven.</p>'),(205,67,'Beschadiging',5,'<p>Deze vragen gaan over de beschadiging.</p>'),(206,67,'Diefstal',6,'<p>Deze vragen gaan over diefstal of vermissing.</p>'),(207,68,'Algemeen',1,'<p>Hier volgt een aantal algemen vragen.</p>'),(208,68,'Schadegegevens',2,'<p>De vragen gaan over de schadegegevens.</p>'),(209,68,'Beschadiging',3,'<p>De vragen gaan over de beschadiging.</p>');
/*!40000 ALTER TABLE `ins_forms_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_forms_categories_answer`
--

DROP TABLE IF EXISTS `ins_forms_categories_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_forms_categories_answer` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `skip_to_questions` int(11) DEFAULT NULL,
  `skip_to_category` int(11) DEFAULT NULL,
  PRIMARY KEY (`answer_id`),
  KEY `question_id` (`question_id`),
  KEY `fk_ins_forms_categories_answer_1` (`question_id`),
  CONSTRAINT `fk_ins_forms_categories_answer_1` FOREIGN KEY (`question_id`) REFERENCES `ins_forms_categories_question` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=758 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_forms_categories_answer`
--

LOCK TABLES `ins_forms_categories_answer` WRITE;
/*!40000 ALTER TABLE `ins_forms_categories_answer` DISABLE KEYS */;
INSERT INTO `ins_forms_categories_answer` VALUES (4,102,'Only Me',NULL,NULL),(5,102,'Me and other 1',NULL,NULL),(6,102,'No One Injured',NULL,NULL),(33,115,'test1',NULL,NULL),(46,132,'Yes',NULL,NULL),(47,132,'No',NULL,NULL),(48,133,'sdfs',NULL,NULL),(49,133,'sdfsdf',NULL,NULL),(50,137,'Answer 1',NULL,NULL),(51,137,'answer 2',NULL,NULL),(70,152,'Opt 1',615,NULL),(71,152,'Opt 2',NULL,NULL),(80,164,'ja',NULL,NULL),(81,164,'nee',NULL,NULL),(82,165,'df',NULL,NULL),(83,165,'dfd',NULL,NULL),(84,166,'dg',NULL,NULL),(85,166,'dfg',NULL,NULL),(321,353,'Ja',NULL,NULL),(322,353,'Nee',NULL,NULL),(323,354,'Ja',NULL,NULL),(324,354,'Nee',NULL,NULL),(325,355,'Ja',NULL,NULL),(326,355,'Nee',NULL,NULL),(335,370,'Ja',NULL,NULL),(336,370,'Nee',NULL,NULL),(337,380,'Ja',NULL,NULL),(338,380,'Nee',NULL,NULL),(339,382,'Ja',NULL,NULL),(340,382,'Nee',NULL,NULL),(341,384,'Ja',NULL,NULL),(342,384,'Nee',NULL,NULL),(343,389,'Ja',NULL,NULL),(344,389,'Nee',NULL,NULL),(345,394,'Ja',NULL,NULL),(346,394,'Nee',NULL,NULL),(347,397,'Ja',NULL,NULL),(348,397,'Nee',NULL,NULL),(353,407,'Ja',NULL,NULL),(354,407,'Nee',NULL,NULL),(355,409,'Ja',NULL,NULL),(356,409,'Nee',NULL,NULL),(357,412,'Ja',NULL,NULL),(358,412,'Nee',NULL,NULL),(359,415,'Ja',NULL,NULL),(360,415,'Nee',NULL,NULL),(361,416,'Ja',NULL,NULL),(362,416,'Nee',NULL,NULL),(363,420,'Ja',NULL,NULL),(364,420,'Nee',NULL,NULL),(365,421,'Ja',NULL,NULL),(366,421,'Nee',NULL,NULL),(367,431,'Ja',NULL,NULL),(368,431,'Nee',NULL,NULL),(369,431,'n.v.t.',NULL,NULL),(370,434,'Ja',NULL,NULL),(371,434,'Nee',NULL,NULL),(376,448,'Ja',NULL,NULL),(377,448,'Nee',NULL,NULL),(378,452,'Ja',NULL,NULL),(379,452,'Nee',NULL,NULL),(380,457,'Ja',NULL,NULL),(381,457,'Nee',NULL,NULL),(382,460,'Ja',NULL,NULL),(383,460,'Nee',NULL,NULL),(384,462,'Ja',NULL,NULL),(385,462,'Nee',NULL,NULL),(394,481,'Ja',NULL,NULL),(395,481,'Nee',NULL,NULL),(396,483,'Ja',NULL,NULL),(397,483,'Nee',NULL,NULL),(398,484,'Annulering',NULL,NULL),(399,484,'Bagage',NULL,NULL),(400,484,'Buitengewone kosten',NULL,NULL),(401,484,'Ongeval en ziekte',NULL,NULL),(402,484,'Schade logiesverblijven',NULL,NULL),(403,485,'Ja',NULL,NULL),(404,485,'Nee',NULL,NULL),(405,370,'n.v.t.',NULL,NULL),(406,490,'Ja',NULL,NULL),(407,490,'Nee',NULL,NULL),(408,491,'Ja',NULL,NULL),(409,491,'Nee',NULL,NULL),(410,492,'Ja',NULL,NULL),(411,492,'Nee',NULL,NULL),(412,493,'Ja',494,NULL),(413,493,'Nee',496,NULL),(414,496,'Ja',497,NULL),(415,496,'Nee',500,NULL),(416,500,'Ja',501,NULL),(417,500,'Nee',504,NULL),(418,507,'Ja',508,NULL),(419,507,'Nee',510,NULL),(420,510,'Ja',NULL,NULL),(421,510,'Nee',NULL,NULL),(422,512,'enkel',NULL,NULL),(423,512,'dubbel',NULL,NULL),(424,514,'Ja',515,NULL),(425,514,'Nee',518,NULL),(426,516,'Ja',NULL,NULL),(427,516,'Nee',NULL,NULL),(428,518,'Ja',519,NULL),(429,518,'Nee',520,NULL),(430,520,'eigenaar',NULL,NULL),(431,520,'huurder',NULL,NULL),(432,521,'Ja',522,NULL),(433,521,'Nee',526,NULL),(434,527,'Ja',528,NULL),(435,527,'Nee',529,NULL),(436,529,'Ja',NULL,NULL),(437,529,'Nee',NULL,NULL),(438,530,'varend onder zeil',531,NULL),(439,530,'varend op de motor',531,NULL),(440,530,'deelnemer aan wedstrijd',531,NULL),(441,530,'verhuurd',531,NULL),(442,530,'doorlopend bewoond',531,NULL),(443,530,'gemeerd',531,NULL),(444,535,'Ja',536,NULL),(445,535,'Nee',539,NULL),(446,541,'Ja',542,NULL),(447,541,'Nee',543,NULL),(448,545,'Ja',NULL,NULL),(449,545,'Nee',NULL,NULL),(450,552,'particulier',NULL,NULL),(451,552,'bedrijfsmatig',NULL,NULL),(452,553,'persoonlijk letsel',NULL,NULL),(453,553,'materiele schade',NULL,NULL),(454,562,'Ja',NULL,NULL),(455,562,'Nee',NULL,NULL),(456,565,'Ja',NULL,NULL),(457,565,'Nee',NULL,NULL),(458,566,'yes',97,NULL),(459,566,'no',NULL,NULL),(516,603,'Maand (automatische incasso verplicht)',NULL,NULL),(517,603,'Kwartaal (automatische incasso verplicht)',NULL,NULL),(518,603,'Halfjaar',NULL,NULL),(519,603,'Jaar',NULL,NULL),(520,604,'Ja',605,NULL),(521,604,'Nee',NULL,NULL),(522,606,'Ja',607,NULL),(523,606,'Nee',608,NULL),(524,608,'Ja',609,NULL),(525,608,'Nee',0,NULL),(526,610,'Ja',611,NULL),(527,610,'Nee',612,NULL),(528,612,'Ja',613,NULL),(529,612,'Nee',0,NULL),(530,614,'eeeep',NULL,NULL),(531,614,'eeee',NULL,NULL),(532,615,'awwqweweq',NULL,NULL),(533,615,'eee',NULL,NULL),(534,616,'test1',NULL,NULL),(535,616,'test2',NULL,NULL),(536,616,'test3',NULL,NULL),(566,633,'Ja',NULL,NULL),(567,633,'Nee',NULL,NULL),(568,634,'Ja',552,NULL),(569,634,'Nee',0,NULL),(570,102,'asd',NULL,NULL),(623,712,'ja',NULL,NULL),(624,712,'nee',NULL,NULL),(625,713,'ja',717,NULL),(626,713,'nee',719,NULL),(627,724,'ja',725,NULL),(628,724,'nee',726,NULL),(629,726,'ja',727,NULL),(630,726,'nee',728,NULL),(631,728,'particulier',NULL,NULL),(632,728,'bedrijfsmatig',NULL,NULL),(633,731,'persoonlijk letsel',NULL,NULL),(634,731,'materiele schade',NULL,NULL),(635,733,'ja',734,NULL),(636,733,'nee',736,NULL),(637,733,'niet bekend',736,NULL),(653,759,'ja',NULL,NULL),(654,759,'nee',NULL,NULL),(655,760,'ja',761,NULL),(656,760,'nee',763,NULL),(657,769,'ja',NULL,NULL),(658,769,'nee',NULL,NULL),(659,770,'beschadiging',771,NULL),(660,770,'diefstal/vermissing',778,NULL),(661,772,'ja',773,NULL),(662,772,'nee',775,NULL),(663,775,'ja',776,NULL),(664,775,'nee',783,NULL),(665,778,'ja',779,NULL),(666,778,'nee',780,NULL),(667,781,'ja',782,NULL),(668,781,'nee',783,NULL),(669,784,'ja',NULL,NULL),(670,784,'nee',NULL,NULL),(671,785,'ja',786,NULL),(672,785,'nee',788,NULL),(673,791,'eigenaar',NULL,NULL),(674,791,'huurder',NULL,NULL),(675,792,'ja',NULL,NULL),(676,792,'nee',NULL,NULL),(677,794,'ja',795,NULL),(678,794,'nee',796,NULL),(679,796,'ja',797,NULL),(680,796,'nee',798,NULL),(681,799,'ja',800,NULL),(682,799,'nee',802,NULL),(711,820,'test1',NULL,NULL),(712,820,'test2',NULL,NULL),(713,822,'test1',NULL,NULL),(733,838,'test1',NULL,NULL),(734,838,'test2',NULL,NULL),(735,840,'test1',NULL,NULL),(753,852,'test1',NULL,NULL),(754,852,'test2',NULL,NULL),(755,854,'test1',NULL,NULL),(756,861,'one',NULL,NULL),(757,861,'two',NULL,NULL);
/*!40000 ALTER TABLE `ins_forms_categories_answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_forms_categories_question`
--

DROP TABLE IF EXISTS `ins_forms_categories_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_forms_categories_question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `help_text` text NOT NULL,
  `sequence` int(11) NOT NULL,
  `answer_kind` varchar(25) NOT NULL,
  `required` tinyint(1) NOT NULL,
  PRIMARY KEY (`question_id`),
  KEY `cat_id` (`cat_id`),
  CONSTRAINT `ins_forms_categories_question_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `ins_forms_categories` (`cat_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=862 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_forms_categories_question`
--

LOCK TABLES `ins_forms_categories_question` WRITE;
/*!40000 ALTER TABLE `ins_forms_categories_question` DISABLE KEYS */;
INSERT INTO `ins_forms_categories_question` VALUES (96,26,'<p>Question 3 for Category 03</p>','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.',4,'text',1),(97,26,'<p>Question 4 for Category 03</p>','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.',2,'text',1),(102,52,'<p>How many people injured from it ?</p>','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.',1,'checkbox',0),(115,26,'<p>Address were acideint happend</p>','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.',1,'textarea',0),(119,67,'sadh lasdjasld asd;l asd;l sad; dsa;l asd;l asd;lkads;lkads;lads k;lasd k;lasd k;lasd k;sald kasd;lk asd ;lasd;l kasd;lksad;l ksad','',1,'text',0),(120,67,'alkasjasd - awdlkjl ads -','',2,'textarea',0),(128,66,'<p>Heeft een verzekeringsmaatschappij u ooit een verzekering opgezegd, geweigerd of tegen beperkende voorwaarden of verhoogde premie geaccepteerd of voortgezet?</p>\r\n<p>Zo ja, wilt u dan een toelichting geven over het soort verzekering, welke verzekeraar, de reden, de datum en eventueel het polisnummer?</p>','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.',2,'text',1),(132,66,'<p>Does it your fault ?</p>','',1,'checkbox',1),(133,70,'<p>sdfsdfsd</p>','          sdfsdfs                                          ',2,'checkbox',0),(134,75,'<p>Question 1 in cat 1</p>','Help text for category one',2,'text',1),(135,75,'<p>Question 2 in cat 1</p>','Help text for question 2',1,'text',1),(136,76,'<p>Question 1 in cat 2</p>','Help text for Question 1 in cat 2',1,'textarea',1),(137,76,'<p>Question 2 in cat 2</p>','Help text for Question 2 in cat 2                                                    ',2,'radio',1),(138,77,'<p>Question 1 in cat 3</p>','help text for Question 1 in cat 3',1,'text',1),(139,78,'<p>Question 1 in cat 4</p>','Help Text Question 1 in cat 4',1,'text',1),(152,26,'<p>Question 2 for Category 03</p>','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.',5,'radio',0),(164,68,'<p>hallo</p>','typ hallo',2,'checkbox',0),(165,67,'<p>w</p>','dfgd',3,'checkbox',0),(166,67,'<p>fdf</p>','dfd',2,'checkbox',0),(353,163,'<p>Is de schade al aan de maatschappij gemeld?</p>','',1,'radio',1),(354,163,'<p>Bent u ANWB-lid?</p>','',2,'radio',1),(355,163,'<p>Heeft u een Internationale Reis- en kredietbrief van de ANWB (IRK)?</p>','',3,'radio',1),(358,164,'<p>Wat is de reden van de annulering/aankomstvertraging/reisonderbreking?</p>','',1,'textarea',1),(359,164,'<p>Naam en voornaam van degene die het genoemde voorval is overkomen.</p>','',2,'textarea',0),(360,164,'<p>Geboortedatum van degene die het genoemde voorval is overkomen.</p>','',3,'text',0),(361,164,'<p>Adres van degene die het genoemde voorval is overkomen.</p>','',4,'text',0),(362,164,'<p>Woonplaats&nbsp;van degene die het genoemde voorval is overkomen.</p>','',5,'text',0),(363,164,'<p>Welke (familie)relatie bestaat er tot de verzekerde?</p>','',6,'textarea',0),(366,164,'<p>Hoeveel bedroeg de reissom of huursom van de bungalow of het appartement etc.?</p>','',7,'text',0),(367,164,'<p>Bij welk reisbureau of welke reisorganisatie heeft u de reis geboekt?</p>','',8,'textarea',0),(368,164,'<p>Bij annulering: wanneer (datum) heeft u om (gedeeltelijke) terugbetaling van de reissom of huursom gevraagd?</p>','',9,'text',0),(369,164,'<p>Bij reisonderbreking: wanneer (datum) vond de terugreis plaats?</p>','',10,'text',0),(370,164,'<p>Bent u weer teruggegaan naar de vakantiebestemming?</p>','',11,'radio',0),(371,164,'<p>Wanneer bent u weer teruggegaan naar de vakantiebestemming?</p>','',12,'text',0),(372,164,'<p>Soort motorrijtuig waarmee de reis werd/zal worden gemaakt.</p>','',13,'text',0),(373,164,'<p>Merk&nbsp;van het motorrijtuig waarmee de reis werd/zal worden gemaakt.</p>','',14,'text',0),(374,164,'<p>Bouwjaar van het motorrijtuig waarmee de reis werd/zal worden gemaakt.</p>','',15,'text',0),(375,164,'<p>Kenteken van het motorrijtuig waarmee de reis werd/zal worden gemaakt.</p>','',16,'text',0),(376,164,'<p>Naam van de eigenaar van het motorrijtuig.</p>','',17,'text',0),(377,164,'<p>Volledig adres&nbsp;van de eigenaar van het motorrijtuig.</p>','',18,'textarea',0),(378,164,'<p>Wat was de oorzaak van het uitvallen van het motorrijtuig (tevens omschrijving van de schade)?</p>','',19,'textarea',0),(379,164,'<p>Hoelang (in dagen) heeft de reparatie geduurd?</p>','',20,'text',0),(380,164,'<p>Werd of zal voor het motorrijtuig een ander motorrijtuig worden gehuurd?</p>','',21,'radio',0),(381,164,'<p>Hoeveel bedragen de kosten?</p>\n<p>(Factuur inzenden)</p>','',22,'text',0),(382,164,'<p>Werd TravelCare/de ANWB Alarmcentrale ingeschakeld?</p>','',23,'radio',0),(383,164,'<p>Waaruit bestaat het letsel?</p>','',24,'textarea',0),(384,164,'<p>Is er sprake van overlijden?</p>','',25,'radio',0),(385,164,'<p>Omschrijving van het ongeval (Oorzaak/toedracht enz.).</p>','',26,'textarea',0),(386,164,'<p>Wat was de aard van de ziekte?</p>','',27,'textarea',0),(387,164,'<p>Sinds wanneer lijdt u aan deze ziekte?</p>','',28,'text',0),(389,164,'<p>Werd reeds eerder aan deze ziekte geleden?</p>','',29,'radio',0),(390,164,'<p>Hoe vaak en wanneer?</p>','',30,'textarea',0),(391,164,'<p>Wanneer werd de huisarts geconsulteerd?</p>','',31,'text',0),(392,164,'<p>Wat is de naam van de huisarts?</p>','',32,'text',0),(393,164,'<p>Wat is het volledige adres van de huisarts?</p>','',33,'textarea',0),(394,164,'<p>Werd naar een specialist verwezen?</p>','',34,'radio',0),(395,164,'<p>Wat is de naam van&nbsp;de specialist?</p>','',35,'text',0),(396,164,'<p>Wat is het volledige adres&nbsp;van de specialist?</p>','',36,'textarea',0),(397,164,'<p>Is er sprake (geweest) van een ziekenhuisopname?</p>','',37,'radio',0),(399,164,'<p>Welk ziekenhuis, waar en wanneer?</p>','',38,'textarea',0),(405,163,'<p>Waar (land, plaats) heeft de schade plaatsgevonden?</p>','',4,'textarea',0),(406,163,'<p>Op welke datum heeft de schade plaatsgevonden?</p>','',5,'text',0),(407,163,'<p>Heeft u al eerder een schade op een reis-/annuleringsverzekering geleden?</p>','',10,'radio',1),(408,163,'<p>Bij welke maatschappij, wanneer en waaruit bestond de schade?</p>','',11,'textarea',0),(409,163,'<p>Is de schade door schuld van derden veroorzaakt?</p>','',6,'radio',0),(410,163,'<p>Naam veroorzaker.</p>','',7,'text',0),(411,163,'<p>Volledig adres veroorzaker.</p>','',8,'textarea',0),(412,163,'<p>Is politierapport/procesverbaal opgemaakt?</p>','',9,'radio',0),(413,165,'<p>Wanneer is de schade geconstateerd? (datum en tijd)</p>','',1,'text',0),(414,165,'<p>Oorzaak van de schade/omschrijving van de toedracht.</p>','',2,'textarea',0),(415,165,'<p>Heeft u aangifte gedaan bij de politie? (Zo ja, dan originele bewijs bijvoegen)</p>','',3,'radio',0),(416,165,'<p>Heeft u aangifte gedaan bij een andere instantie? (Zo ja, originele bewijs bijvoegen)</p>','',4,'radio',0),(417,165,'<p>Indien geen aangifte bij de politie werd gedaan, wat is hiervan de reden?</p>','',5,'textarea',0),(418,165,'<p>Indien geen aangifte bij een andere instatie werd gedaan, wat is hiervan de reden?</p>','',6,'textarea',0),(419,165,'<p>Bij diefstal uit de auto: Waar en waarom lagen de goederen in de auto?</p>\n<p>De reparatienota van de auto ter inzage meesturen.</p>','',7,'textarea',0),(420,165,'<p>Indien de schade is ontstaan tijdens een vliegreis of op de luchthaven: Werd een Property Irregularity Report (P.I.R.) opgemaakt?</p>\n<p>Wilt u het origineel van: de ticket(s), bagagelabel(s) en het P.I.R. bijvoegen!</p>','',8,'radio',0),(421,165,'<p>Is/zijn de bagage/kostbaarheden nog elders verzekerd?</p>','',9,'radio',0),(422,165,'<p>Bij welke maatschappij?</p>','',10,'text',0),(423,165,'<p>Onder welk polisnummer?</p>','',11,'text',0),(424,165,'<p>Beschadigde/vermiste voorwerpen; vermeld u hier:</p>\n<p>Merk, en type, aankoopbedrag, aankoopdatum en reparatiekosten.</p>\n<p>Bij ruimtegebrek eventueel een parte bijlage meesturen, deze kunt u toevoegen aan het formulier.</p>','',12,'textarea',0),(425,166,'<p>Extra reis-/verblijfkosten:</p>\n<p>Waarom zijn er extra verblijfkosten gemaakt?</p>','',21,'textarea',0),(426,166,'<p>Waar zou u normaliter verblijven?</p>','',22,'textarea',0),(427,166,'<p>Wat zouden de kosten hiervoor zijn geweest?</p>','',23,'text',0),(428,166,'<p>Waarom zijn er extra reiskosten gemaakt?</p>','',24,'textarea',0),(429,166,'<p>Op welke wijze zou u normaliter zijn teruggereisd?</p>','',25,'textarea',0),(430,166,'<p>Wat zouden hiervoor de kosten zijn geweest?</p>','',26,'text',0),(431,166,'<p>Bent u weer teruggegaan naar de vakantiebestemming?</p>','',27,'radio',0),(432,166,'<p>Wanneer bent u weer teruggegaan naar de vakantiebestemming?</p>','',28,'textarea',0),(433,166,'<p>Hoeveel bedragen de telefoonkosten?</p>','',29,'text',0),(434,166,'<p>Werd TravelCare/de ANWB Alarmcentrale ingeschakeld?</p>','',30,'radio',0),(435,166,'<p>In verband waarmee werd TravelCare/de ANWB Alarmcentrale inschakeld?</p>','',31,'textarea',0),(447,166,'<p>Waaruit bestaat het letsel?</p>','',1,'textarea',0),(448,166,'<p>Is er sprake van overlijden?</p>','',2,'radio',0),(449,166,'<p>Omschrijving van het ongeval (Oorzaak/toedracht enz.)</p>','',3,'textarea',0),(450,166,'<p>Wat was de aard van de ziekte?</p>','',4,'textarea',0),(451,166,'<p>Sinds wanneer lijdt u aan deze ziekte?</p>','',5,'text',0),(452,166,'<p>Werd reeds eerder aan deze ziekte geleden?</p>','',6,'radio',0),(453,166,'<p>Hoe vaak en wanneer?</p>','',7,'textarea',0),(454,166,'<p>Wanneer werd de huisarts geconsulteerd?</p>','',8,'text',0),(455,166,'<p>Wat is de naam van de huisarts?</p>','',9,'text',0),(456,166,'<p>Wat is het volledige adres van de huisarts?</p>','',10,'textarea',0),(457,166,'<p>Werd naar een specialist verwezen?</p>','',11,'radio',0),(458,166,'<p>Wat is de naam van de specialist?</p>','',12,'text',0),(459,166,'<p>Wat is het volledige adres van de specialist?</p>','',13,'textarea',0),(460,166,'<p>Is er sprake geweest van een ziekenhuisopname?</p>','',14,'radio',0),(461,166,'<p>Welk ziekenhuis, waar en wanneer?</p>','',15,'textarea',0),(462,166,'<p>Is verzekerde bij een andere maatschappij of bij een ziekenfonds verzekerd?</p>','',16,'radio',0),(473,171,'<p>Soort motorrijtuig waarmee de reis werd/zal worden gemaakt.</p>','',1,'text',0),(474,171,'<p>Merk van het motorrijtuig waarmee de reis werd/zal worden gemaakt.</p>','',2,'text',0),(475,171,'<p>Bouwjaar van het motorrijtuig waarmee de reis werd/zal worden gemaakt.</p>','',3,'text',0),(476,171,'<p>Kenteken van het motorrijtuig waarmee de reis werd/zal worden gemaakt.</p>','',4,'text',0),(477,171,'<p>Naam van de eigenaar van het motorrijtuig.</p>','',5,'text',0),(478,171,'<p>Volledig adres van de eigenaar van het motorrijtuig.</p>','',6,'textarea',0),(479,171,'<p>Wat was de oorzaak van het uitvallen van het motorrijtuig (tevens omschrijving van de schade)?</p>','',7,'textarea',0),(480,171,'<p>Hoelang (in dagen) heeft de reparatie geduurd?</p>','',8,'text',0),(481,171,'<p>Werd of zal voor het motorrijtuig een ander motorrijtuig worden gehuurd?</p>','',9,'radio',0),(482,171,'<p>Hoeveel bedragen de kosten? (factuur inzenden)</p>','',10,'text',0),(483,171,'<p>Werd TravelCare/de ANWB Alarmcentrale ingeschakeld?</p>','',11,'radio',0),(484,163,'<p>Welke schade heeft u geleden?</p>','',12,'checkbox',0),(485,163,'<p>Heeft u al eerder een schade op een reis-/annuleringsverzekering geleden?</p>','',10,'radio',0),(487,169,'<p>Wilt u in een aparte bijlage een uitgebreide omschrijving geven van het gebeurde en eventuele nota\\\'s en/of bewijsstukken bijvoegen? U kunt uw bijlage toevoegen aan dit formulier voordat u dit verstuurd.</p>','',1,'text',0),(488,166,'<p>Maatschappij/Ziekenfonds</p>','',17,'text',0),(489,166,'<p>Polisnummer</p>','',18,'text',0),(490,166,'<p>Zijn de kosten van geneeskundige behandeling al bij deze maatschappij ingediend?</p>','',19,'radio',0),(491,166,'<p>Geldt een eigen risico?</p>','',20,'radio',0),(492,89,'<p>Is er recht op aftrek BTW?</p>','',1,'radio',0),(493,89,'<p>Is deze schade al gemeld?</p>','',2,'radio',0),(494,89,'<p>Wanneer (datum) is deze schade gemeld?</p>','',3,'text',0),(495,89,'<p>Aan wie is deze schade gemeld?</p>','',4,'text',0),(496,89,'<p>Bent u elders tegen deze schade verzekerd?</p>','',5,'radio',0),(497,89,'<p>Wat is het verzekerde bedrag?</p>','',6,'text',0),(498,89,'<p>Bij welke maatschappij?</p>','',7,'text',0),(499,89,'<p>Onder welk polisnummer?</p>','',8,'text',0),(500,89,'<p>Zijn bepaalde voorwerpen apart verzekerd (bijvoorbeeld sieraden, postzegels e.d.)?</p>','',9,'radio',0),(501,89,'<p>Wat is het verzekerde bedrag?</p>','',10,'text',0),(502,89,'<p>Bij welke maatschappij?</p>','',11,'text',0),(503,89,'<p>Onder welk polisnummer?</p>','',12,'text',0),(504,172,'<p>Wat is de schadedatum?</p>','',1,'text',0),(505,172,'<p>Op welk tijdstip ondstond de schade?</p>','',2,'text',0),(506,172,'<p>Volledig adres van de plaats van de schade?</p>','',3,'textarea',0),(507,172,'<p>Zijn er sporen van braak?</p>','',4,'radio',0),(508,172,'<p>Omschrijving van de toedracht (zonodig een situatieschets en/of toelichting op een los blad toevoegen aan het formulier).</p>','',5,'textarea',0),(509,172,'<p>Beschadigde/vermiste voorwerpen: vermelde u hier: Merk en type, naam, (evt.volgnr.op polis), Frame- en motornummer, bouwjaar, aankoopbedrag, aankoopdatum en reparatiekosten. Bij ruimtegebrek eventueel een aparte bijlage meesturen, deze kunt u toevoegen aan het formulier</p>','',6,'textarea',0),(510,172,'<p>Is er sprake van schade aan glas en/of kunststof?</p>','',7,'radio',0),(511,172,'<p>Afmetingen glas/kunststof</p>','',8,'text',0),(512,172,'<p>Enkel of dubbel glas?</p>','',9,'radio',0),(513,172,'<p>Schatting van het schadebedrag</p>','',10,'text',0),(514,172,'<p>Is het glas gebroken en/of anderszins defect?</p>','',11,'radio',0),(515,172,'<p>Waaruit bestaat de beschadiging?</p>','',12,'textarea',0),(516,172,'<p>Werden er noodvoorzieningen aangebracht?</p>','',13,'radio',0),(517,172,'<p>Voor welk bedrag werden er noodvoorzieningen aangebracht?</p>','',14,'text',0),(518,172,'<p>Is het pand bewoond?</p>','',15,'radio',0),(519,172,'<p>Door wie wordt het pand bewoond?</p>','',16,'text',0),(520,172,'<p>Bent u eigenaar of huurder?</p>','',17,'radio',0),(521,172,'<p>Is de schade herstelbaar?</p>','',18,'radio',0),(522,172,'<p>Voor welk bedrag is de schade herstelbaar?</p>','',19,'text',0),(523,172,'<p>Naam van de reparateur</p>','',20,'text',0),(524,172,'<p>Volledig adres van de reparateur</p>','',21,'textarea',0),(525,172,'<p>Telefoonnummer van de reparateur</p>','',22,'text',0),(526,172,'<p>Waar en wanneer kan de schade worden opgenomen?</p>','',23,'textarea',0),(527,172,'<p>Is de reparatie reeds uitgevoerd?</p>','',24,'radio',0),(528,172,'<p>Voor welk bedrag is de reparatie uitgevoerd (nota\'s en/of schadebegroting bijvoegen)?</p>','',25,'text',0),(529,173,'<p>Is er sprake van pleziervaartuig schade?</p>','',1,'radio',0),(530,173,'<p>Was het vaartuig</p>','',2,'radio',0),(531,174,'<p>Naam veroorzaker</p>','',1,'text',0),(532,174,'<p>Volledig adres veroorzaker</p>','',2,'textarea',0),(533,174,'<p>Geboortedatum veroorzaker</p>','',3,'text',0),(534,174,'<p>In welke relatie staat de veroorzaker tot u (familie, dienstverband o.i.d.)?</p>','',4,'textarea',0),(535,174,'<p>Zijn er medeschuldigen?</p>','',5,'radio',0),(536,174,'<p>Naam medeschuldige</p>','',6,'text',0),(537,174,'<p>Volledig adres medeschuldige</p>','',7,'textarea',0),(538,174,'<p>Geboortedatum medeschuldige</p>','',8,'text',0),(539,174,'<p>Waarmee werd de schade veroorzaakt?</p>','',9,'textarea',0),(540,174,'<p>Waarmee was bovengenoemde bezig toen de schade werd veroorzaakt?</p>','',10,'textarea',0),(541,174,'<p>Is van de schade aangifte gedaan (a.u.b. verklaring van aangifte bijvoegen; eventueel van hotel, vervoersondernemeing, camping e.d.)?</p>','',11,'radio',0),(542,174,'<p>Op welk bureau werd aangifte gedaan?</p>','',12,'textarea',0),(543,174,'<p>Volledige naam en adres van getuige 1</p>','',14,'textarea',0),(544,174,'<p>Volledige naam en adres van getuige 2</p>','',15,'textarea',0),(545,174,'<p>Kan de schade naar uw mening verhaald worden op een ander?</p>','',16,'radio',0),(546,174,'<p>Naam van degene op wie de schade verhaald kan worden</p>','',17,'text',0),(547,174,'<p>Volledig adres van degene op wie de schade verhaald kan worden</p>','',18,'textarea',0),(548,174,'<p>Telefoonnummer&nbsp;van degene op wie de schade verhaald kan worden</p>','',19,'text',0),(549,174,'<p>Waarom meent u dat de schade verhaald kan worden op een ander?</p>','',20,'textarea',0),(550,174,'<p>Bij welke maatschappij is deze verzekerd?</p>','',21,'text',0),(551,174,'<p>Onder welk polisnummer?</p>','',22,'text',0),(552,175,'<p>In welke hoedanigheid wordt u aansprakelijk gesteld?</p>','',2,'radio',0),(553,175,'<p>Welke schade werd toegebracht?</p>','',3,'radio',0),(554,175,'<p>Naam van de benadeelde</p>','',4,'text',0),(555,175,'<p>Volledig adres van de benadeelde</p>','',5,'textarea',0),(556,175,'<p>Geboortedatum van de benadeelde</p>','',6,'text',0),(557,175,'<p>Giro-/bankrekeningnummer van de benadeelde</p>','',7,'text',0),(558,175,'<p>Telefoonnummer van de benadeelde</p>','',8,'text',0),(559,175,'<p>In welke relatie staat deze tot u respectievelijk de veroorzaker?</p>','',9,'textarea',0),(560,175,'<p>Korte omschrijving van de aard van het letsel en/of de materi&euml;le schade</p>','',10,'textarea',0),(561,175,'<p>Waar bevindt zich de getroffene (naam en volledig adres instelling)?</p>','',11,'textarea',0),(562,175,'<p>Is benadeelde zelf tegen deze schade verzekerd?</p>','',12,'radio',0),(563,175,'<p>Bij welke maatschappij is benadeelde tegen deze schade verzekerd?</p>','',13,'text',0),(564,175,'<p>Onder welk polisnummer is benadeelde daar verzekerd?</p>','',14,'text',0),(565,175,'<p>Is de schade daar gemeld?</p>','',15,'radio',0),(566,66,'<p>due to rain?</p>','',3,'radio',0),(603,182,'<p>Betalingstermijn premie</p>','',1,'radio',1),(604,182,'<p>Heeft een verzekeringsmaatschappij u ooit een verzekering opgezegd, geweigerd of tegen beperkende voorwaarden of verhoogde premie geaccepteerd of voortgezet?</p>\n<p>Zo ja, wilt u dan een toelichting geven over het soort verzekering, welke verzekeraar, de reden, de datum en eventueel het polisnummer?</p>','',2,'radio',1),(605,182,'<p>Toelichting verzekeringsverleden</p>','',3,'textarea',0),(606,182,'<p>Bent u in de laatste acht jaar als verdacht of voor uitvoering van een opgelegde (straf)maatregel, in aanraking geweest met politie of justitie in verband met onrechtmatig verkregen of te verkrijgen voordeel? Hierbij moet u denken aan diefstal, verduistering, bedrog, oplichting, valsheid in geschrifte of pogingen daartoe?</p>','',4,'radio',1),(607,182,'<p>Toelichting aanraking met politie of justitie</p>','',5,'textarea',0),(608,182,'<p>Bent u in de laatste acht jaar als verdacht of voor uitvoering van een opgelegde (straf)maatregel, in aanraking geweest met politie of justitie in verband met onrechtmatige benadeling van anderen? Hierbij moet u denken aan: vernieling, beschadiging, mishandeling, afpersing, bedreiging, enig ander misdrijf tegen de persoonlijke vrijheid of tegen het leven of een poging daartoe?</p>','',6,'radio',1),(609,182,'<p>Toelichting onrechtmatige benadeling van anderen</p>','',7,'textarea',0),(610,182,'<p>Bent u in de laatste acht jaar als verdacht of voor uitvoering van een opgelegde (straf)maatregel, in aanraking geweest met politie of justitie in verband met overtreding van de Wet wapens en munitie, de Opiumwet of de Wet economische delicten?</p>','',8,'radio',1),(611,182,'<p>Toelichting Wet wapens en munitie, de Opiumwet of de Wet economische delicten</p>','',9,'textarea',0),(612,182,'<p>Heeft u of een andere belanghebbende bij deze verzekering de laatste acht jaar ontzegging - al dan niet voorwaardelijk - van de rijbevoegdheid gehad?</p>','',10,'radio',1),(613,182,'<p>Toelichting rijontzegging</p>','',11,'textarea',0),(614,26,'<p>Test 1</p>','',7,'radio',0),(615,26,'<p>Test 2</p>','',6,'radio',1),(616,26,'<p>Test 3</p>','',3,'radio',0),(633,174,'<p>Zijn er getuigen?</p>','',13,'radio',0),(634,175,'<p>Is er sprake van schade aan anderen?</p>','',1,'radio',1),(712,195,'<p>Heeft u recht op aftrek BTW?</p>','',1,'radio',1),(713,195,'<p>Heeft u voor deze schade mogelijk ook dekking op een andere verzekering?</p>','',2,'radio',1),(717,195,'<p>Bij welke verzekeraar?</p>','',3,'text',0),(718,195,'<p>Onder welk polisnummer?</p>','',4,'text',0),(719,196,'<p>Waar heeft het voorval plaatsgevonden?</p>','',1,'textarea',0),(720,196,'<p>Op welk tijdstip heeft het voorval plaatsgevonden?</p>','',2,'text',1),(721,196,'<p>Omschrijf hier zo uitgebreid mogelijk wat er precies is gebeurd en hoe de schade is ontstaan?</p>','',3,'textarea',1),(722,196,'<p>Wie of wat heeft de schade veroorzaakt?<br />Bij een persoon graag de volgende gegevens vermelden:<br />- naam, adres, postcode en plaats<br />- telefoonnummer<br />- emailadres</p>','',4,'textarea',1),(723,196,'<p>Indien van toepassing: Wat is de geboortedatum van de veroorzaker?</p>','',5,'text',0),(724,196,'<p>Zijn er mede-schuldigen?</p>','',6,'radio',1),(725,196,'<p>Vul de volgende gegevens per medeschuldige in:<br />- naam, adres, postcode en woonplaats<br />- telefoonnummer<br />- emailadres</p>','',7,'textarea',0),(726,196,'<p>Heeft een van de betrokkenen aangifte gedaan bij de politie?</p>','',8,'radio',1),(727,196,'<p>Wie heeft er aangifte gedaan en waarom?</p>','',9,'textarea',0),(728,197,'<p>In welke hoedanigheid werd u aansprakelijk gesteld?</p>','',1,'radio',1),(729,197,'<p>Wie is de benadeelde, graag de volgende gegevens opgeven:<br />- naam, adres, postcode en woonplaats<br />- telefoonnummer<br />- emailadres</p>','',2,'textarea',1),(730,197,'<p>Wat is het bankrekeningnummer van de benadeelde?</p>','',3,'text',1),(731,197,'<p>Welke schade werd toegebracht?</p>','',4,'checkbox',1),(732,197,'<p>Wilt u omschrijven wat er werd beschadigd en/of welk letsel werd toegebracht?</p>','',5,'textarea',1),(733,197,'<p>is de benadeelde zelf tegen deze schade verzekerd?</p>','',6,'radio',1),(734,197,'<p>Bij welke verzekeraar?</p>','',7,'text',0),(735,197,'<p>Onder welk polisnummer?</p>','',8,'text',0),(736,197,'<p>Hier kunt u vragen en/of opmerkingen plaatsen.</p>','',9,'textarea',0),(759,201,'<p>Heeft u recht op aftrek BTW?</p>','',1,'radio',1),(760,201,'<p>Heeft u voor deze schade mogelijk ook dekking op een andere verzekering?</p>','',2,'radio',1),(761,201,'<p>Bij welke verzekeraar?</p>','',3,'text',0),(762,201,'<p>Onder welk polisnummer?</p>','',4,'text',0),(763,202,'<p>Waar heeft het voorval plaatsgevonden?</p>','',1,'textarea',1),(764,202,'<p>Op welk tijdstip heeft het voorval plaatsgevonden?</p>','',2,'text',1),(765,202,'<p>Omschrijf hier zo uitgebreid mogelijk wat er precies is gebeurd en hoe de schade is ontstaan?</p>','',3,'textarea',1),(766,203,'<p>Merk en type</p>','',1,'text',1),(767,203,'<p>Aankoopdatum</p>','',2,'text',1),(768,203,'<p>Aankoopprijs</p>','',3,'text',1),(769,203,'<p>Beschikt u over een aankoopnota?<br />(indien ja, dan graag bijvoegen)</p>','',4,'radio',1),(770,204,'<p>Is er sprake van</p>','',1,'radio',1),(771,205,'<p>Waaruit bestaat de beschadiging</p>','',1,'textarea',1),(772,205,'<p>Is de schade herstelbaar?</p>','',2,'radio',0),(773,205,'<p>Door welk bedrijf wordt de schade hersteld?<br />Vermeld u hier de volgende gegevens:<br />- naam, adres, postcode en plaats<br />- telefoonnummer<br />- emailadres</p>','',3,'textarea',0),(774,205,'<p>Voor welke bedrag?</p>','',4,'text',0),(775,205,'<p>Vindt u iemand anders verantwoordelijk voor de schade?</p>','',5,'radio',1),(776,205,'<p>Waarom vindt u iemand anders verantwoordelijk voor de schade?</p>','',6,'textarea',0),(777,205,'<p>Wilt u de volgende gegevens van de verantwoordelijke opgeven?<br />- naam, adres, postcode en plaats<br />- telefoonnummer<br />- emailadres</p>','',7,'textarea',0),(778,206,'<p>Heeft u aangifte gedaan bij de politie of een andere instantie?</p>','',1,'radio',1),(779,206,'<p>Waar en wanneer heeft u aangifte gedaan?<br />(aangifte als bijlage meesturen)</p>','',2,'textarea',0),(780,206,'<p>Waarom heeft u geen aangifte gedaan?</p>','',3,'textarea',0),(781,206,'<p>Is er sprake van diefstal na braak?</p>','',4,'radio',0),(782,206,'<p>Waaruit bestaan de braaksporen?</p>','',5,'textarea',0),(783,206,'<p>Hier kunt u vragen en/of opmerkingen plaatsen.</p>','',6,'textarea',0),(784,207,'<p>Heeft u recht op aftrek BTW?</p>','',1,'radio',1),(785,207,'<p>Heeft u voor de schade mogelijk ook dekking op een andere verzekering?</p>','',2,'radio',1),(786,207,'<p>Bij welke verzekeraar?</p>','',3,'text',0),(787,207,'<p>Onder welk polisnummer?</p>','',4,'text',0),(788,208,'<p>Waar heeft het voorval plaatsgevonden?</p>','',1,'textarea',1),(789,208,'<p>Op welk tijdstip heeft het voorval plaatsgevonden?</p>','',2,'text',1),(790,208,'<p>Omschrijf hier zo uitgebreid mogelijk wat er precies is gebeurd en hoe de lekkage is ontstaan?</p>','',3,'textarea',1),(791,208,'<p>Bent u eigenaar of huurder?</p>','',4,'radio',1),(792,208,'<p>Is het pand bewoond?</p>','',5,'radio',0),(793,209,'<p>Wat is er beschadigd en waaruit bestaa(t)(n) de beschadiging(en)?</p>','',1,'textarea',1),(794,209,'<p>Werden er noodvoorzieningen aangebracht?</p>','',2,'radio',1),(795,209,'<p>Voor welk bedrag werden noodvoorzieningen aangebracht?</p>','',3,'text',0),(796,209,'<p>Wilt u de schade (of een deel daarvan) zelf herstellen?</p>','',4,'radio',1),(797,209,'<p>Omschrijving van de werkzaamheden en de te gebruiken materialen.</p>','',5,'textarea',0),(798,209,'<p>Door welk bedrijf wilt u de schade laten herstellen?<br />Vermeld u hier de volgende gegevens:<br />- naam, adres, postcode en plaats<br />- telefoonnummer<br />- emailadres</p>','',6,'textarea',0),(799,209,'<p>Vindt u iemand anders verantwoordelijk voor de schade?</p>','',7,'radio',1),(800,209,'<p>Waarom vindt u iemand anders verantwoordelijk voor de schade?</p>','',8,'textarea',0),(801,209,'<p>Vermeld u hier de volgende gegevens van de verantwoordelijke:<br />- naam, adres, postcode en plaats<br />- telefoonnummer<br />- emailadres</p>','',9,'textarea',0),(802,209,'<p>Hier kunt u vragen en/of opmerkingen plaatsen.</p>','',10,'textarea',0),(829,66,'<p>test question for date</p>','testing date',4,'date',0),(830,66,'<p>test question for number</p>','testing number',5,'number',1),(831,66,'<p>test question for text</p>','testing text',6,'text',1),(860,52,'<p>test date for category2</p>','',2,'date',1),(861,66,'<p>radio test</p>','asf',7,'radio',1);
/*!40000 ALTER TABLE `ins_forms_categories_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_invoices`
--

DROP TABLE IF EXISTS `ins_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_invoices` (
  `invoice_number` int(10) NOT NULL AUTO_INCREMENT,
  `subscription_kind_id` int(11) DEFAULT NULL,
  `invoice_amount` decimal(8,2) NOT NULL,
  `invoice_date` date NOT NULL,
  `invoice_period_start` date NOT NULL,
  `invoiece_period_end` date NOT NULL,
  `invoice_subject` varchar(100) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `mail_sent` date DEFAULT NULL,
  PRIMARY KEY (`invoice_number`),
  KEY `invoices_client_id_fk` (`client_id`),
  CONSTRAINT `invoices_client_id_fk` FOREIGN KEY (`client_id`) REFERENCES `ins_clients` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_invoices`
--

LOCK TABLES `ins_invoices` WRITE;
/*!40000 ALTER TABLE `ins_invoices` DISABLE KEYS */;
INSERT INTO `ins_invoices` VALUES (1,1,14.00,'2013-07-24','2013-05-01','2013-07-31','Digital op Maat Factuur',1,'2013-09-02'),(2,NULL,70.00,'2013-09-03','2013-09-03','2013-10-02','Invoice for Online Insurance Forms',4,NULL);
/*!40000 ALTER TABLE `ins_invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_newz`
--

DROP TABLE IF EXISTS `ins_newz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_newz` (
  `newz_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `newz_text` text NOT NULL,
  `publication_date` date NOT NULL,
  `expire_date` date DEFAULT NULL,
  PRIMARY KEY (`newz_id`),
  KEY `client_newz_fk` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_newz`
--

LOCK TABLES `ins_newz` WRITE;
/*!40000 ALTER TABLE `ins_newz` DISABLE KEYS */;
INSERT INTO `ins_newz` VALUES (1,1,'News1','<p>\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim &nbsp;est laborum.\"</p>','2012-02-03','2012-03-30'),(7,1,'asd','<p>asd</p>','2013-05-16',NULL),(8,1,'news2','<p>\"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p>','2013-05-16','2013-05-17'),(9,1,'test4','<p>testingg</p>','2013-05-16',NULL);
/*!40000 ALTER TABLE `ins_newz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_policy_holder`
--

DROP TABLE IF EXISTS `ins_policy_holder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_policy_holder` (
  `policy_holder_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `policy_number` varchar(225) DEFAULT NULL,
  `first_name` varchar(65) DEFAULT NULL,
  `middle_name` varchar(65) DEFAULT NULL,
  `last_name` varchar(65) DEFAULT NULL,
  `email` varchar(65) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `private_number` varchar(20) DEFAULT NULL,
  `business_number` varchar(20) DEFAULT NULL,
  `mobile_number` varchar(20) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `register_date` date DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `random_str` varchar(200) DEFAULT NULL,
  `password_reset_random_string` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`policy_holder_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_policy_holder`
--

LOCK TABLES `ins_policy_holder` WRITE;
/*!40000 ALTER TABLE `ins_policy_holder` DISABLE KEYS */;
INSERT INTO `ins_policy_holder` VALUES (2,4,'aanvraag','Henry','','Middendorp','h.middendorp@uwassuradeuren.nl','M','0','','','','39abddf548b5d12e21c2237907499006','2013-08-22','A',NULL,'qHGDUmCHG9'),(4,4,'2000','Soyab',NULL,'Rana','soyab@devrepublic.nl',NULL,NULL,NULL,NULL,NULL,'698d51a19d8a121ce581499d7b701668',NULL,'A',NULL,'Mm4rBFKxaW'),(5,4,'aanvraag','Jeroen','','Hoorn','info@his-automatisering.nl','','0','0622774888','','0622774888','793181d6b81fdbaa314ec2e01e4f46af','2013-08-16','A',NULL,'6LK0tGERLU'),(6,4,'aanvraag','Henry','','Middendorp','h.middendorp@uiterwijkwinkel.nl','M','7822','','','','39abddf548b5d12e21c2237907499006','2013-08-22','A',NULL,'dRzHm3ci7P'),(9,1,'aanvraag','Edward','','Vernhout','edward@devrepublic.nl','','0','','','','21232f297a57a5a743894a0e4a801fc3','1970-01-01','A',NULL,'hVzt1kZtUx'),(11,4,'5324201960','Henry','','Middendorp prive','zomer007@hotmail.com','','0','','','',NULL,'1970-01-01','A',NULL,'vQmFWEhZSS'),(14,4,'aanvraag','Jeroen','','Hoorn','hc-69@hotmail.com','','0','','','','793181d6b81fdbaa314ec2e01e4f46af','2013-09-16','A',NULL,'Hb4TYCoXhw'),(15,1,'Uiterwijk Winkel Verzekeringen',NULL,NULL,NULL,'Verzekeringen',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'hD7bVQonNA'),(16,4,'5324201960','Mike','van','Schravendijk','schravendijkmh@hotmail.com','Rondgang 25','8391','0561750210','0561750210','0638501451','1f81ad1e18c5398e46389adb4073a126','2013-09-26','A',NULL,'M71ZeTMBMv'),(17,4,NULL,'Hans','van der','Starren','h.vanderstarren@uiterwijkwinkel.nl','Jacob Katsstraat 5','7927','0528361793','','','00936b9285d6b8665ae9122993fb8e91','2013-09-26','A',NULL,NULL),(18,4,'5324201960',NULL,NULL,NULL,'hans@vanderstarren.nl',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Gf4JLhDNKH'),(19,4,'',NULL,NULL,NULL,'administratie@his-automatisering.nl',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'osXrMR1LxG'),(21,1,'2000123','manish','bhai','bhadrecgha','dinesh@devrepublic.nl','porb','123','9510163333','079221731','98982424289',NULL,'1970-01-01','A',NULL,'wQHDrpQ9Eh');
/*!40000 ALTER TABLE `ins_policy_holder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_subscription_kinds`
--

DROP TABLE IF EXISTS `ins_subscription_kinds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_subscription_kinds` (
  `subscription_kinds_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_title` varchar(35) NOT NULL,
  `maximum_forms` int(4) NOT NULL,
  `subscription_fee` decimal(5,2) NOT NULL,
  `rate_per_extra_form` decimal(5,2) NOT NULL,
  `help_text` varchar(255) NOT NULL,
  `plan_image` varchar(65) DEFAULT NULL,
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`subscription_kinds_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_subscription_kinds`
--

LOCK TABLES `ins_subscription_kinds` WRITE;
/*!40000 ALTER TABLE `ins_subscription_kinds` DISABLE KEYS */;
INSERT INTO `ins_subscription_kinds` VALUES (1,'Small',500,14.00,0.40,'<p>asdfa sdfa sdfa sdfasdf&nbsp;</p>','','A'),(2,'Medium',1000,22.00,0.40,'',NULL,'A'),(3,'Large',2500,45.00,0.40,'',NULL,'A'),(4,'Extra large',5000,70.00,0.40,'','','A');
/*!40000 ALTER TABLE `ins_subscription_kinds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_super_admin`
--

DROP TABLE IF EXISTS `ins_super_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_super_admin` (
  `super_admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `super_admin_username` varchar(65) DEFAULT NULL,
  `super_admin_password` varchar(100) NOT NULL,
  `super_admin_first_name` varchar(65) NOT NULL,
  `super_admin_middle_name` varchar(45) DEFAULT NULL,
  `super_admin_last_name` varchar(65) NOT NULL,
  `super_admin_email` varchar(65) NOT NULL,
  PRIMARY KEY (`super_admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_super_admin`
--

LOCK TABLES `ins_super_admin` WRITE;
/*!40000 ALTER TABLE `ins_super_admin` DISABLE KEYS */;
INSERT INTO `ins_super_admin` VALUES (1,'dinesh@devrepublic.nl','21232f297a57a5a743894a0e4a801fc3','Dinesh','a1','Goraniya','dinesh@devrepublic.nl'),(2,'info@his-automatisering.nl','793181d6b81fdbaa314ec2e01e4f46af','Jeroen','a2','Hoorn','info@his-automatisering.nl');
/*!40000 ALTER TABLE `ins_super_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_super_admin_newz`
--

DROP TABLE IF EXISTS `ins_super_admin_newz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_super_admin_newz` (
  `newz_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `newz_text` text NOT NULL,
  `publication_date` date NOT NULL,
  `expire_date` date DEFAULT NULL,
  PRIMARY KEY (`newz_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_super_admin_newz`
--

LOCK TABLES `ins_super_admin_newz` WRITE;
/*!40000 ALTER TABLE `ins_super_admin_newz` DISABLE KEYS */;
INSERT INTO `ins_super_admin_newz` VALUES (8,'2013-09-11','1914 translation by H. Rackham','<p>\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p>','2013-09-06','2013-09-25'),(7,'2013-09-10','de Finibus Bonorum et Malorum\", written by Cicero in 45 BC','<p>\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p>\n<p>&nbsp;</p>\n<p><span>&nbsp;</span></p>','2013-09-06',NULL);
/*!40000 ALTER TABLE `ins_super_admin_newz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_super_admin_setting`
--

DROP TABLE IF EXISTS `ins_super_admin_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_super_admin_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `logo` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_super_admin_setting`
--

LOCK TABLES `ins_super_admin_setting` WRITE;
/*!40000 ALTER TABLE `ins_super_admin_setting` DISABLE KEYS */;
INSERT INTO `ins_super_admin_setting` VALUES (1,'info@digitaalopmaat.nl','');
/*!40000 ALTER TABLE `ins_super_admin_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_temp_forms_answers`
--

DROP TABLE IF EXISTS `ins_temp_forms_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_temp_forms_answers` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `claim_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_text` text,
  `answer_type` varchar(20) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `prev_question` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`answer_id`),
  KEY `claim_id_idx` (`claim_id`),
  KEY `cat_id_idx` (`cat_id`),
  KEY `fk_question_id` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_temp_forms_answers`
--

LOCK TABLES `ins_temp_forms_answers` WRITE;
/*!40000 ALTER TABLE `ins_temp_forms_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `ins_temp_forms_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_temp_forms_answers_details`
--

DROP TABLE IF EXISTS `ins_temp_forms_answers_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_temp_forms_answers_details` (
  `answer_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_answer_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  PRIMARY KEY (`answer_detail_id`),
  KEY `user_answer_id_idx` (`user_answer_id`),
  KEY `answer_id_idx` (`answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_temp_forms_answers_details`
--

LOCK TABLES `ins_temp_forms_answers_details` WRITE;
/*!40000 ALTER TABLE `ins_temp_forms_answers_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `ins_temp_forms_answers_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_texts`
--

DROP TABLE IF EXISTS `ins_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_texts` (
  `text_id` int(11) NOT NULL AUTO_INCREMENT,
  `text_title` varchar(65) NOT NULL,
  `text_content` text NOT NULL,
  `page_title` text NOT NULL,
  `meta_tags` text NOT NULL,
  `meta_desc` text NOT NULL,
  `is_no_index` tinyint(1) NOT NULL DEFAULT '0',
  `is_no_follow` tinyint(1) NOT NULL DEFAULT '0',
  `include_sitemap` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`text_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_texts`
--

LOCK TABLES `ins_texts` WRITE;
/*!40000 ALTER TABLE `ins_texts` DISABLE KEYS */;
INSERT INTO `ins_texts` VALUES (2,'Home','<h2>Digitaal op Maat</h2>\n<p><span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</span></p>','','','',0,0,0),(3,'Contact','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.</p>\r\n<p>Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu.</p>','','','',0,0,0),(4,'Over ons','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.</p>\r\n<p>Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu.</p>','','','',0,0,0),(5,'Abonnementen','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\r\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.</p>\r\n<p>Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu.</p>','','','',0,0,0),(6,'Privacy','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus.</p>\n<p>Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.</p>\n<p>Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu.</p>','title','tegs','<p>description</p>',0,1,0);
/*!40000 ALTER TABLE `ins_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ins_users`
--

DROP TABLE IF EXISTS `ins_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ins_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `first_name` varchar(65) NOT NULL,
  `middle_name` varchar(65) DEFAULT NULL,
  `last_name` varchar(65) NOT NULL,
  `department` int(11) NOT NULL DEFAULT '0',
  `mail_address` varchar(100) NOT NULL,
  `password` varchar(65) NOT NULL,
  `register_date` date NOT NULL,
  `role` varchar(15) NOT NULL,
  `status` char(1) NOT NULL,
  `is_handler` tinyint(1) NOT NULL DEFAULT '0',
  `password_reset_random_string` varchar(45) DEFAULT NULL,
  `last_login_date` date DEFAULT NULL,
  `current_login_date` date DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `client_id` (`client_id`),
  CONSTRAINT `ins_users_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `ins_clients` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ins_users`
--

LOCK TABLES `ins_users` WRITE;
/*!40000 ALTER TABLE `ins_users` DISABLE KEYS */;
INSERT INTO `ins_users` VALUES (1,1,'Soyab','C','Rana',0,'soyab@devrepublic.nl','21232f297a57a5a743894a0e4a801fc3','2013-02-13','admin','D',0,NULL,'2014-01-29','2014-01-31'),(2,1,'Nirav','','Makwana',0,'nirav@devrepublic.nl','21232f297a57a5a743894a0e4a801fc3','2013-02-12','admin','A',0,NULL,NULL,NULL),(6,1,'kalpit','sdf','patel',0,'abc@acb.com','d9b1d7db4cd6e70935368a1efb10e377','2013-04-30','admin','D',0,'VMXFkAhhoKtRA7zFy6M5FyOUmIKnvA','2013-07-01','2013-07-01'),(8,1,'Tests','Tests','Tests',0,'Tests@Testsc.com','90792de52961c34118f976ebe4af3a75','2013-05-07','admin','A',0,'',NULL,NULL),(9,4,'Hans','','van der Starren',0,'starren@his-automatisering.nl','22663134d0a309005e0501668289f49b','2013-05-15','admin','A',0,'','2013-12-16','2013-12-16'),(11,1,'Dinesh','H','Goraniya',0,'h.middendorp@uiterwijkwinkel.nl','e24d68defd5c24f18ff324076ee3657a','2013-05-16','admin','A',0,'',NULL,NULL),(20,1,'Jeroen',NULL,'Hoorn',0,'info1@his-automatisering.nl','21232f297a57a5a743894a0e4a801fc3','2013-06-05','admin','A',0,NULL,NULL,NULL),(21,1,'Edward','','Vernhout',0,'edward@devrepublic.nl','b8be2d60c17ae57853b7c807b337b2b5','2013-06-05','admin','A',0,'',NULL,NULL),(23,4,'Jeroen','','Hoorn',0,'info@his-automatisering.nl','12266606fe88cc9aa901a5013eec5292','2013-06-07','admin','A',0,'',NULL,NULL),(24,4,'Henry','','Middendorp',0,'h.middendorp@uiterwijkwinkel.nl','468e074e302925ab7644b42475e6519f','2013-06-21','admin','A',0,'',NULL,NULL),(25,1,'dinesh','H','Goraniya',0,'dinesh@devrepublic.nl','827ccb0eea8a706c4c34a16891f84e7b','2013-08-08','admin','A',0,'',NULL,NULL),(26,1,'jaykesh','D','patel',0,'jaykesh@devrepublic.nl','202cb962ac59075b964b07152d234b70','2013-08-08','admin','A',0,NULL,NULL,NULL),(27,4,'Mike','','van Schravendijk',0,'info@assuconsultancy.nl','0bad1626f295f9df8db875521f9133dc','2013-09-19','admin','A',0,'','2013-10-09','2013-10-10'),(28,1,'test user','h','maman',4,'soyab@devrepublic.nl','a0cde4d18493fc9ff40c819abbfd2f82','2014-01-21','admin','D',0,'',NULL,NULL);
/*!40000 ALTER TABLE `ins_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-01-31 10:44:43
