<?php

/**
 * Each insurance intermediary will have several admins
 * fo him who will be able to perform tasks for him.
 *
 * This model represents the same admin from the database table.
 *
 * @author umang
 *        
 */
class admin_model extends CI_Model
{
    
    // name of table
    public $table_name;
    //
    // table fields
    public $admin_id;

    public $password;

    public $first_name;

    public $last_name;

    public $mail_address;

    public $validation_rules;

    public $client_id;

    public $password_reset_random_string;

    function __construct ()
    {
        parent::__construct();
        $this->table_name = 'users';
        
        // load client model for get_client() method
        $this->load->model('clients_model');
        
        $this->validation_rules = array(
                array('field' => 'mail_address','label' => 'Mail',
                        'rules' => 'required|min_length[3]|valid_mail_address'
                ),
                array('field' => 'first_name','label' => 'First Name',
                        'rules' => 'required|min_length[3]|trim'
                ),
                array('field' => 'last_name','label' => 'Last Name',
                        'rules' => 'required|min_length[3]|trim'
                ),
                array('field' => 'password','label' => 'Password',
                        'rules' => 'required|min_length[3]'
                )
        );
    
    /**
     * to do
     */
    }

    function save ()
    {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->admin_id = $id;
        $this->after_save();
        return $id;
    
    }

    function update ()
    {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['admin_id']);
        $this->db->where('admin_id', $this->admin_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
        return $this->db->affected_rows();
    
    }

    public function delete ()
    {
        $this->db->where('admin_id', $this->admin_id);
        $this->db->delete($this->table_name);
    
    }

    public function get_all ()
    {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row)
        {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where ( $where )
    {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row)
        {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    
    }

    public function to_associative_array ()
    {
        $arr = array();
        $arr['admin_id'] = $this->admin_id;
        $arr['password'] = $this->password;
        $arr['first_name'] = $this->first_name;
        $arr['last_name'] = $this->last_name;
        $arr['mail_address'] = $this->mail_address;
        $arr['client_id'] = $this->client_id;
        $arr['password_reset_random_string'] = $this->password_reset_random_string;
        return $arr;
    
    }

    public function from_array ( $array )
    {
        $object = new admin_model();
        $object->admin_id = $array['admin_id'];
        $object->password = $array['password'];
        $object->first_name = $array['first_name'];
        $object->last_name = $array['last_name'];
        $object->mail_address = $array['mail_address'];
        $object->client_id = $array['client_id'];
        if (isset($array['password_reset_random_string']) &&
                 $array['password_reset_random_string'])
        {
            $object->password_reset_random_string = $array['password_reset_random_string'];
        }
        
        return $object;
    
    }

    public function from_raw_objct ( $old )
    {
        $new = new admin_model();
        
        $new->admin_id = $old->admin_id;
        $new->password = $old->password;
        $new->first_name = $old->first_name;
        $new->last_name = $old->last_name;
        $new->mail_address = $old->mail_address;
        $new->client_id = $old->client_id;
        if (isset($old->password_reset_random_string) &&
                 $old->password_reset_random_string)
        {
            $new->password_reset_random_string = $old->password_reset_random_string;
        }
        $new->after_save();
        return $new;
    
    }

    private function before_save ()
    {
        // does nothing for now
    }

    private function after_save ()
    {
        // does nothing for now
    }

    /**
     * application methods
     */
    function check_login ( $mail_address, $password )
    {
        $admins = $this->get_where(array('mail_address' => $mail_address
        ));
        
        // if there was no user found
        if (is_array($admins) && count($admins) > 0)
        {
            
            // user was found, now check the password
            $admin = $admins[0];
            
            // if password matches
            if (md5($password) == $admin->password)
            {
                return true;
            }
            else
            {
                throw new InvalidPasswordException();
            }
        }
        else
        {
            throw new InvalidEmailException();
        }
    
    }

    /**
     * returns an object of clients_model that is associated with given admin
     *
     * @return clients_model object of clients_model that is associated with
     *         given admin
     */
    function get_client ()
    {
        $clients = $this->clients_model->get_where(
                array('client_id' => $this->client_id
                ));
        return $clients[0];
    
    }

}
if (!class_exists('InvalidPasswordException'))
{

    class InvalidPasswordException extends Exception
    {
    
    }
}
if (!class_exists('InvalidEmailException'))
{

    class InvalidEmailException extends Exception
    {
    
    }
}

?>
