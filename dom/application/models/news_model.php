<?php

/**
 * Represents the newz items that belong to the
 * client (which will be maintained by the admin
 * for client)
 *
 * @author umang
 *        
 */
class news_model extends CI_model
{

    /**
     *
     * @var int primary key to uniquely identify the item
     */
    public $newz_id;

    /**
     *
     * @var int client id of the client to which the newz belongs to
     */
    public $client_id;

    /**
     *
     * @var string Title of the newz
     */
    public $title;

    /**
     *
     * @var string news text / actual content
     */
    public $newz_text;

    /**
     *
     * @var string date that represents the
     *      date from which the newz item becomes visible
     */
    public $publication_date;

    /**
     *
     * @var string date that represents the date from which the
     *      newz item becomes invisible
     */
    public $expire_date;

    /**
     *
     * @var string table to which the items are persisted
     */
    public $table_name;

    public $validation_rules;

    function __construct ()
    {
        parent::__construct();
        
        $this->load->model('clients_model');
        $this->table_name = 'ins_newz';
        
        $this->validation_rules = array(
                array('field' => 'title','label' => 'Title',
                        'rules' => 'required|max_length[240]'
                ),
                array('field' => 'newz_text','label' => 'News Text',
                        'rules' => 'required|max_length[240]'
                ),
                array('field' => 'title','label' => 'Title',
                        'rules' => 'required'
                ),
                array('field' => 'publication_date',
                        'label' => 'Publication Date',
                        'rules' => 'required|date_validator'
                ),
                array('field' => 'expire_date','label' => 'Expire Date',
                        'rules' => 'date_validator'
                )
        );
    
    }

    function fromObject ( $old )
    {
        $new = new news_model();
        $new->newz_id = $old->newz_id;
        $new->client_id = $old->client_id;
        $new->title = $this->getClientName($old->title);
        $new->newz_text = $old->newz_text;
        $new->publication_date = $old->publication_date;
        $new->expire_date = $old->expire_date;
        return $new;
    
    }

    public function get_all ()
    {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row)
        {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where ( $where )
    {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row)
        {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    
    }

    public function to_associative_array ()
    {
        $arr = array();
        $arr['newz_id'] = $this->newz_id;
        $arr['client_id'] = $this->client_id;
        $arr['title'] = $this->title;
        $arr['newz_text'] = $this->newz_text;
        $arr['publication_date'] = $this->publication_date;
        $arr['expire_date'] = $this->expire_date;
        return $arr;
    
    }

    public function from_array ( $array )
    {
        $object = new news_model();
        $object->newz_id = $array['newz_id'];
        $object->client_id = $array['client_id'];
        $object->title = $array['title'];
        $object->newz_text = $array['newz_text'];
        $object->publication_date = $array['publication_date'];
        $object->expire_date = $array['expire_date'];
        $object->after_save();
        return $object;
    
    }

    public function from_raw_objct ( $old )
    {
        $new = new news_model();
        $new->newz_id = $old->newz_id;
        $new->client_id = $old->client_id;
        $new->title = $old->title;
        $new->newz_text = $old->newz_text;
        $new->publication_date = $old->publication_date;
        $new->expire_date = $old->expire_date;
        $new->after_save();
        return $new;
    
    }

    private function before_save ()
    {
        $this->publication_date = date("Y-m-d", 
                strtotime($this->publication_date));
        if (isset($this->expire_date) && $this->expire_date != '')
        {
            $this->expire_date = date("Y-m-d", strtotime($this->expire_date));
        }
        else
        {
            $this->expire_date = null;
        }
    
    }

    private function after_save ()
    {
        $this->publication_date = date("d-m-Y", 
                strtotime($this->publication_date));
        if (isset($this->expire_date) && $this->expire_date != '')
        {
            $this->expire_date = date("d-m-Y", strtotime($this->expire_date));
        }
        else
        {
            $this->expire_date = null;
        }
    
    }

    public function delete ()
    {
        $this->db->where('newz_id', $this->newz_id);
        $this->db->delete($this->table_name);
    
    }

    function save ()
    {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->client_id = $id;
        $this->after_save();
        return $id;
    
    }

    function update ()
    {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['newz_id']);
        $this->db->where('newz_id', $this->newz_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    
    }

    /**
     * returns the client model associaed with the zewz
     *
     * @return clients_model client model associated with the newz
     */
    public function get_client ()
    {
        $clients_model = new clients_model();
        $clients = $clients_model->get_where(
                array('client_id' => $this->client_id
                ));
        return $clients[0];
    
    }

}

?>
