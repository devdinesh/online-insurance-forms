<?php

class feedback_category_model extends CI_Model {

    public $table_name = 'ins_feedback_category';
    public $feed_cat_id;
    public $feed_cat_name;
    public $feed_cat_status;
    public $feed_cat_img;
    public $validation_rules;
    // logo upload path
    public $logo_upload_path = './assets/images/feedback';

    function __construct() {
        parent::__construct();

        $this->validation_rules = array();
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->feed_cat_id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['feed_cat_id']);
        $this->db->where('feed_cat_id', $this->feed_cat_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('feed_cat_id', $this->feed_cat_id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        $this->db->order_by("feed_cat_id", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['feed_cat_id'] = $this->feed_cat_id;
        $arr['feed_cat_name'] = $this->feed_cat_name;
        $arr['feed_cat_status'] = $this->feed_cat_status;
        return $arr;
    }

    public function from_array($array) {
        $object = new feedback_category_model();
        $object->feed_cat_id = $array['feed_cat_id'];
        $object->feed_cat_name = $array['feed_cat_name'];
        $object->feed_cat_status = $array['feed_cat_status'];
        $object->feed_cat_img = $array['feed_cat_img'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new feedback_category_model();

        $new->feed_cat_id = $old->feed_cat_id;
        $new->feed_cat_name = $old->feed_cat_name;
        $new->feed_cat_status = $old->feed_cat_status;
        $new->feed_cat_img = $old->feed_cat_img;
        $new->after_save();
        return $new;
    }

    public function logo_upload_config() {
        $config['upload_path'] = './assets/images/feedback';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>
