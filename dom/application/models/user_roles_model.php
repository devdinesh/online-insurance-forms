<?php

/**
 * This model represents the table and mechanism
 * for accesing it from the database.
 */
class user_roles_model extends CI_Model {

    // name of table
    public $table_name = 'ins_user_roles';
    // fields
    public $id;
    public $name;
    public $show_dashbord;
    public $show_relations;
    public $show_forms;
    public $show_own_forms;
    public $show_news;
    public $show_statistics;
    public $show_newsletters;
    public $show_other;
    public $client_id;
    public $show_user_forms;
    // validation rules to be applied
    public $validation_rules;

    // logo upload path
    function __construct() {
        parent::__construct();

        $this->validation_rules = array(
            array('field' => 'name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'show_dashbord', 'label' => 'Dashboard', 'rules' => 'trim'),
            array('field' => 'show_relations', 'label' => 'Relaties', 'rules' => 'trim'),
            array('field' => 'show_forms', 'label' => 'Formulieren', 'rules' => 'trim'),
            array('field' => 'show_own_forms', 'label' => 'Eigen bibliotheek', 'rules' => 'trim'),
            array('field' => 'show_news', 'label' => 'Nieuws', 'rules' => 'trim'),
            array('field' => 'show_statistics', 'label' => 'Statistieken', 'rules' => 'trim'),
            array('field' => 'show_newsletters', 'label' => 'Nieuwsbrieven', 'rules' => 'trim'),
            array('field' => 'show_other', 'label' => 'Overig', 'rules' => 'trim'),
            array('field' => 'show_user_forms', 'label' => 'Show own forms', 'rules' => 'trim')
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['id']);
        $this->db->where('id', $this->id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        // $this->db->order_by("client_name", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['id'] = $this->id;
        $arr['name'] = $this->name;
        $arr['show_dashbord'] = $this->show_dashbord;
        $arr['show_relations'] = $this->show_relations;
        $arr['show_forms'] = $this->show_forms;
        $arr['show_own_forms'] = $this->show_own_forms;
        $arr['show_news'] = $this->show_news;
        $arr['show_statistics'] = $this->show_statistics;
        $arr['show_newsletters'] = $this->show_newsletters;
        $arr['show_other'] = $this->show_other;
        $arr['client_id'] = $this->client_id;
        $arr['show_user_forms'] = $this->show_user_forms;
        return $arr;
    }

    public function from_array($array) {
        $object = new user_roles_model();
        $object->id = $array['id'];
        $object->name = $array['name'];
        $object->show_dashbord = $array['show_dashbord'];
        $object->show_relations = $array['show_relations'];
        $object->show_forms = $array['show_forms'];
        $object->show_own_forms = $array['show_own_forms'];
        $object->show_news = $array['show_news'];
        $object->show_statistics = $array['show_statistics'];
        $object->show_newsletters = $array['show_newsletters'];
        $object->show_other = $array['show_other'];
        $object->client_id = $array['client_id'];
        $object->show_user_forms = $array['show_user_forms'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new user_roles_model();
        $new->id = $old->id;
        $new->name = $old->name;
        $new->show_dashbord = $old->show_dashbord;
        $new->show_relations = $old->show_relations;
        $new->show_forms = $old->show_forms;
        $new->show_own_forms = $old->show_own_forms;
        $new->show_news = $old->show_news;
        $new->show_statistics = $old->show_statistics;
        $new->show_newsletters = $old->show_newsletters;
        $new->show_other = $old->show_other;
        $new->client_id = $old->client_id;
        $new->show_user_forms = $old->show_user_forms;
        $new->after_save();
        return $new;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>
