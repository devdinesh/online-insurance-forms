<?php

/**
 * This model represents the table and mechanism
 * for accesing it from the database.
 */
class newsletter_article_model extends CI_Model {

    // name of table
    public $table_name = 'ins_newsletters_articles';
    // fields
    public $id;
    public $newsletter_id;
    public $article_id;
    public $sequence;
    // validation rules to be applied
    public $validation_rules;

    function __construct($id = '') {
        parent::__construct();

        $this->validation_rules = array(
            array('field' => 'newsletter_id', 'label' => 'Newsletter', 'rules' => 'required'),
            array('field' => 'article_id', 'label' => 'Article', 'rules' => 'required'),
            array('field' => 'sequence', 'label' => 'Sequence', 'rules' => 'required')
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['id']);
        $this->db->where('id', $this->id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        // $this->db->order_by("client_name", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function get_where_sort($where, $sort = 'desc', $limit = null) {
        $this->load->database();
        $objects = array();
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where($where);
        $this->db->order_by('sequence', $sort);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['id'] = $this->id;
        $arr['title'] = $this->title;
        $arr['content'] = $this->content;
        $arr['image'] = $this->image;
        $arr['client_id'] = $this->client_id;
        return $arr;
    }

    public function from_array($array) {
        $object = new newsletter_article_model();
        $object->id = $array['id'];
        $object->newsletter_id = $array['newsletter_id'];
        $object->article_id = $array['article_id'];
        $object->sequence = $array['sequence'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new newsletter_article_model();

        $new->id = $old->id;
        $new->newsletter_id = $old->newsletter_id;
        $new->article_id = $old->article_id;
        $new->sequence = $old->sequence;
        $new->after_save();
        return $new;
    }

    function getNewSequenceId($news_id) {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('newsletter_id', $news_id);
        $this->db->order_by('sequence', 'desc');
        $this->db->limit(1);
        $res = $this->db->get();
        $result = $res->result();
        if ($res->num_rows > 0) {
            return $result[0]->sequence + 1;
        } else {
            return 1;
        }
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

    function getNewsArticle($news_id) {
        $return_array = array();
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('newsletter_id', $news_id);
        $this->db->order_by('sequence', 'ASC');
        $res = $this->db->get();
        $result = $res->result();
        if (!empty($result)) {
            for ($i = 0; $i < count($result); $i++) {
                $return_array[$i] = $result[$i]->article_id;
            }
            return $return_array;
        } else {
            return $return_array;
        }
    }

    function deleteMultiplearticle($article_ids, $news_id) {
        $this->db->where_in('article_id', $article_ids)
                ->where('newsletter_id	', $news_id)
                ->delete($this->table_name);
        return $this->db->affected_rows() > 0;
    }

    function addMultipleArticle($insert_data) {
        $this->db->insert_batch($this->table_name, $insert_data);
        return $this->db->insert_id();
    }

    function updateAllSequence($article_id_array) {
        $count = 1;
        foreach ($article_id_array as $article_id) {
            $array = array('sequence' => $count);
            $this->db->where('id', $article_id);
            $this->db->update($this->table_name, $array);
            $count++;
        }
        return true;
    }

}

?>
