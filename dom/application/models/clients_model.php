<?php

/**
 * Each intermediary is known as client.
 * This model represents the table and mechanism
 * for accesing it from the database.
 * 
 * @author umang
 *        
 */
class clients_model extends CI_Model {

    // name of table
    public $table_name = 'clients';
    // fields
    public $client_id;
    public $client_name;
    public $address;
    public $zip_code;
    public $city;
    public $registration_date;
    public $logo;
    public $title_application;
    public $path_to_import;
    public $current_url;
    public $first_remind_mail;
    public $second_remind_mail;
    public $status;
    public $monthly_fee;
    public $amount_forms_month;
    public $additional_forms_fee;
    public $fewer_forms_fee;
    public $enable_newsletters;
    public $monthly_fee_newsletters;
    public $fee_per_newsletter;
    // validation rules to be applied
    public $validation_rules;
    // logo upload path
    public $logo_upload_path = './assets/upload/client_logo';

    function __construct() {
        parent::__construct();

        $this->validation_rules = array(
            array('field' => 'client_name', 'label' => 'Client Name',
                'rules' => 'required|min_length[3]'
            ),
            array('field' => 'address', 'label' => 'Address',
                'rules' => 'required|min_length[3]'
            ),
            array('field' => 'zip_code', 'label' => 'ZipCode',
                'rules' => 'required|min_length[3]'
            ),
            array('field' => 'city', 'label' => 'City',
                'rules' => 'required|min_length[3]|alpha'
            ),
            array('field' => 'registration_date',
                'label' => 'Registration Date',
                'rules' => 'required|date_validator'
            ),
            array('field' => 'logo', 'label' => 'Logo',
                'rules' => 'file_allowed_types[jpg,png]'
            ),
            array('field' => 'title_application',
                'label' => 'Title Application', 'rules' => ''
            ),
            array('field' => 'current_url', 'label' => 'Current URL',
                'rules' => ''
            ),
            array('field' => 'first_remind_mail',
                'label' => 'First Email Remind',
                'rules' => 'required|integer'
            ),
            array('field' => 'second_remind_mail',
                'label' => 'Second Email Reminds',
                'rules' => 'required|integer'
            ),
            array('field' => 'path_to_import', 'label' => 'Path to Import',
                'rules' => 'trim|min_length[2]|max_length[65]|alpha_numeric'
            ),
            array('field' => 'status', 'label' => 'Status', 'rules' => 'trim'),
            array('field' => 'monthly_fee', 'label' => 'Maandelijkse kosten', 'rules' => 'trim|numeric'),
            array('field' => 'amount_forms_month', 'label' => 'Antal formulieren per maand ', 'rules' => 'trim|numeric'),
            array('field' => 'additional_forms_fee', 'label' => 'Kosten extra formulier', 'rules' => 'trim|numeric'),
            array('field' => 'fewer_forms_fee', 'label' => 'Kosten minder formulier', 'rules' => 'trim|numeric'),
            array('field' => 'enable_newsletters', 'label' => 'Enable newsletters', 'rules' => 'trim'),
            array('field' => 'monthly_fee_newsletters', 'label' => 'Monthly fee newsletters', 'rules' => 'trim|numeric'),
            array('field' => 'fee_per_newsletter', 'label' => 'Fee per newsletter', 'rules' => 'trim|numeric')
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->client_id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['client_id']);
        $this->db->where('client_id', $this->client_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('client_id', $this->client_id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        $this->db->order_by("client_name", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['client_id'] = $this->client_id;
        $arr['client_name'] = $this->client_name;
        $arr['address'] = $this->address;
        $arr['zip_code'] = $this->zip_code;
        $arr['city'] = $this->city;
        $arr['registration_date'] = $this->registration_date;
        $arr['logo'] = $this->logo;
        $arr['title_application'] = $this->title_application;
        $arr['path_to_import'] = $this->path_to_import;
        $arr['current_url'] = $this->current_url;
        $arr['first_remind_mail'] = $this->first_remind_mail;
        $arr['second_remind_mail'] = $this->second_remind_mail;
        $arr['status'] = $this->status;
        $arr['monthly_fee'] = $this->monthly_fee;
        $arr['amount_forms_month'] = $this->amount_forms_month;
        $arr['additional_forms_fee'] = $this->additional_forms_fee;
        $arr['fewer_forms_fee'] = $this->fewer_forms_fee;
        $arr['enable_newsletters'] = $this->enable_newsletters;
        $arr['monthly_fee_newsletters'] = $this->monthly_fee_newsletters;
        $arr['fee_per_newsletter'] = $this->fee_per_newsletter;
        return $arr;
    }

    public function from_array($array) {
        $object = new clients_model();
        $object->client_id = $array['client_id'];
        $object->client_name = $array['client_name'];
        $object->address = $array['address'];
        $object->zip_code = $array['zip_code'];
        $object->city = $array['city'];
        $object->registration_date = $array['registration_date'];
        $object->logo = $array['logo'];
        $object->title_application = $array['title_application'];
        $object->path_to_import = $array['path_to_import'];
        $object->current_url = $array['current_url'];
        $object->first_remind_mail = $array['first_remind_mail'];
        $object->second_remind_mail = $array['second_remind_mail'];
        $object->status = $array['status'];
        $object->monthly_fee = $array['monthly_fee'];
        $object->amount_forms_month = $array['amount_forms_month'];
        $object->additional_forms_fee = $array['additional_forms_fee'];
        $object->fewer_forms_fee = $array['fewer_forms_fee'];
        $object->enable_newsletters = $array['enable_newsletters'];
        $object->monthly_fee_newsletters = $array['monthly_fee_newsletters'];
        $object->fee_per_newsletter = $array['fee_per_newsletter'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new clients_model();

        $new->client_id = $old->client_id;
        $new->client_name = $old->client_name;
        $new->address = $old->address;
        $new->zip_code = $old->zip_code;
        $new->city = $old->city;
        $new->registration_date = $old->registration_date;
        $new->logo = $old->logo;
        $new->title_application = $old->title_application;
        $new->path_to_import = $old->path_to_import;
        $new->current_url = $old->current_url;
        $new->first_remind_mail = $old->first_remind_mail;
        $new->second_remind_mail = $old->second_remind_mail;
        $new->status = $old->status;
        $new->monthly_fee = $old->monthly_fee;
        $new->amount_forms_month = $old->amount_forms_month;
        $new->additional_forms_fee = $old->additional_forms_fee;
        $new->fewer_forms_fee = $old->fewer_forms_fee;
        $new->enable_newsletters = $old->enable_newsletters;
        $new->monthly_fee_newsletters = $old->monthly_fee_newsletters;
        $new->fee_per_newsletter = $old->fee_per_newsletter;

        $new->after_save();
        return $new;
    }

    public function logo_upload_config() {
        $config['upload_path'] = './assets/upload/client_logo';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }

    private function before_save() {
        if (!isset($this->registration_date)) {
            $this->registration_date = date('d-m-Y', time());
        }
        $this->registration_date = date("Y-m-d", strtotime($this->registration_date));
    }

    private function after_save() {
        $this->registration_date = date("d-m-Y", strtotime($this->registration_date));
    }

//THIS FUNCTION IS USED TO GET CLIENT AND SETTING RELATED TO THAT CLIENT WHCI IS REQUIRED FOR THE AUTO IMPORT FEATURE .
    function get_auto_import_client() {
        $this->db->select('ins_clients.*, ins_client_setting.last_import');
        $this->db->from('ins_clients');
        $this->db->join('ins_client_setting', 'ins_clients.client_id = ins_client_setting.client_id');
        $this->db->where(array('ins_client_setting.automatic_import' => '1'));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return 0;
        }
    }

    //THIS FUNCTION IS USED TO UPDATE THE LAST IMPORTED TIME FOR AUTOMATICALLY IMPORTED FORM FOR EVERY CLIENT.
    function update_last_imported_time($id) {
        $array = array('last_import' => date('Y-m-d H:i:s'));
        $this->db->where_in('client_id', $id);
        return $this->db->update('ins_client_setting', $array);
    }

}

?>
