<?php

class log_mails_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getData($where = null) {
        if ($where == null) {
            $query = $this->db->get('ins_log_mails');
            //$this->db->order_by("DOMAIN_NAME", "asc"); 
        } else {
            $query = $this->db->get_where('ins_log_mails', $where);
        }
        $data = $query->result();
        return $data;
    }

    function insert_data($data) {
        $this->db->insert('ins_log_mails', $data);
        return $this->db->insert_id();
    }

    function update_data($data, $where) {
        return $this->db->update('ins_log_mails', $data, $where);
    }

    function delete_data($where) {
        $this->db->delete('ins_log_mails', $where);
    }

}