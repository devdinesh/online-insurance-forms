<?php

/**
 * This model represents the table and mechanism
 * for accesing it from the database.
 */
class banner_model extends CI_Model
{
    // name of table
    public $table_name = 'ins_banners';
    // fields
    public $banner_id;

    public $banner_name;

    public $banner_image;

    public $banner_url;

    public $banner_sequence;
    
    // validation rules to be applied
    public $validation_rules;
    // logo upload path
    public $logo_upload_path = './assets/upload/banner_image';

    function __construct ( $id = '' )
    {
        parent::__construct();
        
        $this->validation_rules = array(
                array('field' => 'banner_name','label' => 'Banner Name',
                        'rules' => 'required|min_length[3]'
                ),
                
                array('field' => 'banner_image','label' => 'Banner Image',
                        'rules' => 'trim'
                ),
                
                array('field' => 'banner_url','label' => 'Banner URL',
                        'rules' => ''
                ),
                array('field' => 'banner_sequence','label' => 'Banner Sequence',
                        'rules' => 'required|trim|integer|is_unique[ins_banners.banner_sequence]'
                )
        );
    
    }

    function save ()
    {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->banner_id = $id;
        $this->after_save();
        return $id;
    
    }

    function update ()
    {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['banner_id']);
        $this->db->where('banner_id', $this->banner_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    
    }

    public function delete ()
    {
        $this->db->where('banner_id', $this->banner_id);
        $this->db->delete($this->table_name);
    
    }

    public function get_all ()
    {
        $objects = array();
        // $this->db->order_by("client_name", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row)
        {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where ( $where )
    {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row)
        {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    
    }

    public function to_associative_array ()
    {
        $arr = array();
        $arr['banner_id'] = $this->banner_id;
        $arr['banner_name'] = $this->banner_name;
        $arr['banner_image'] = $this->banner_image;
        $arr['banner_url'] = $this->banner_url;
        $arr['banner_sequence'] = $this->banner_sequence;
        
        return $arr;
    
    }

    public function from_array ( $array )
    {
        $object = new banner_model();
        $object->banner_id = $array['banner_id'];
        $object->banner_name = $array['banner_name'];
        $object->banner_image = $array['banner_image'];
        $object->banner_url = $array['banner_url'];
        $object->banner_sequence = $array['banner_sequence'];
        
        return $object;
    
    }

    public function from_raw_objct ( $old )
    {
        $new = new banner_model();
        
        $new->banner_id = $old->banner_id;
        $new->banner_name = $old->banner_name;
        $new->banner_image = $old->banner_image;
        $new->banner_url = $old->banner_url;
        $new->banner_sequence = $old->banner_sequence;
        
        $new->after_save();
        return $new;
    
    }

    public function logo_upload_config ()
    {
        $config['upload_path'] = './assets/upload/banner_image';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    
    }

    function isDataExit ( $id, $field, $value )
    {
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                 $value . "' and banner_id='" . $id . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() == 1)
        {
            foreach ($query->result() as $row)
            {
                $objects[] = $this->from_raw_objct($row);
            }
        }
        else
        {
            $objects = FALSE;
        }
        
        return $objects;
    
    }
    function getSliderImages($filed = "banner_sequence", $orderby = 'desc', $limit = NULL) {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->order_by($filed, $orderby);
        if ($limit != NULL) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        $return = '<div class="liquid-slider"  id="main-slider">';
        foreach ($res->result() as $row) {
            $return .= '<div> <h2 class="title">&nbsp;</h2>';
            if ($row->banner_url != '' || $row->banner_url != null) {
                $url = $row->banner_url;
                $target = '_blank';
            } else {
                $url = '#';
                $target = '_self';
            }
    
            if ($row->banner_image != '' || $row->banner_image != null) {
                $file = file_exists('./assets/upload/banner_image/' . $row->banner_image);
                if ($file && $row->banner_id != ' no_image.png') {
                    $image = base_url() . 'assets/upload/banner_image/' .$row->banner_image;
                } else {
                    $image = 'no_image.png';
                }
            } else {
                $image = 'no_image.png';
            }
    
            $return .= '<a href="' . $url . '" target="' . $target . '"><img src="' . $image . '" /></a>';
            $return .= '</div>';
        }
        $return .= '</div>';
       
        return $return;
    }

    private function before_save ()
    {
    
    }

    private function after_save ()
    {
    
    }

}

?>
