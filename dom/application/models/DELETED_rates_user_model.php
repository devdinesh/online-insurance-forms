<?php

//AUTHOR : DINESH GORANIYA

class rates_user_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

//THIS FUNCTION IS USED TO GET THE ALL THE RECORDS OF USER RETES
    function getAllrates() {
        $sql = "select * from ins_rates_user";
        $Q = $this->db->query($sql);
        return $Q->result();
    }

//THIS FUNCTION IS USED TO GET THE USER RATE USING ANY FIELD WHICH IS IN INS_RATE TABLE
    function getWhere($where) {
        $this->db->select('*');
        $this->db->from('ins_rates_user');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

//THIS FUNCTION IS USED TO ADD NEW ROW IN INS_RATES_TABLE
    function save($data) {
        $this->db->set($data);
        $result = $this->db->insert('ins_rates_user', $data);
    }

//THIS FUNCTION IS USED TO EDIT THE ROW OF THE INS_RETES_USER
    function update($data, $where = NULL) {
        $this->db->set($data);
        $this->db->where('id', $where['id']);
        $result = $this->db->update('ins_rates_user', $this);
    }

//THIS FUNCTION IS USED TO DELETE THE FROM THE INS_RATES_USER
    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('ins_rates_user');
    }
//THIS FUNCTION IS USED TO GET THE RATE BETWEEN FROM AND TO USERS
    function getrates($users) {
        $query = $this->db->query("SELECT * FROM `ins_rates_user` WHERE `from` <=" . $users . " AND `to` >=" . $users . "");
        return $query->result();
    }

}
