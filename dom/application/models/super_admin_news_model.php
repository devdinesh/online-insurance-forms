<?php

class super_admin_news_model extends CI_model
{

    public $newz_id;

    public $date;

    public $title;

    public $newz_text;

    public $publication_date;

    public $expire_date;

    public $table_name;

    public $validation_rules;

    function __construct ()
    {
        parent::__construct();
        
        $this->table_name = 'ins_super_admin_newz';
        
        $this->validation_rules = array(
                array('field' => 'date','label' => 'Date',
                        'rules' => 'required|date_validator'
                ),
                array('field' => 'title','label' => 'Title',
                        'rules' => 'required|max_length[240]'
                ),
                array('field' => 'newz_text','label' => 'News Text',
                        'rules' => 'required'
                ),
                array('field' => 'title','label' => 'Title',
                        'rules' => 'required'
                ),
                array('field' => 'publication_date',
                        'label' => 'Publication Date',
                        'rules' => 'required|date_validator'
                ),
                array('field' => 'expire_date','label' => 'Expire Date',
                        'rules' => 'date_validator'
                )
        );
    
    }

    function fromObject ( $old )
    {
        $new = new super_admin_news_model();
        $new->newz_id = $old->newz_id;
        $new->date = $old->date;
        $new->title = $this->getClientName($old->title);
        $new->newz_text = $old->newz_text;
        $new->publication_date = $old->publication_date;
        $new->expire_date = $old->expire_date;
        return $new;
    
    }

    public function get_all ()
    {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row)
        {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    
    }

    public function get_where ( $where )
    {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row)
        {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    
    }

    public function to_associative_array ()
    {
        $arr = array();
        $arr['newz_id'] = $this->newz_id;
        $arr['date'] = $this->date;
        $arr['title'] = $this->title;
        $arr['newz_text'] = $this->newz_text;
        $arr['publication_date'] = $this->publication_date;
        $arr['expire_date'] = $this->expire_date;
        return $arr;
    
    }

    public function from_array ( $array )
    {
        $object = new super_admin_news_model();
        $object->newz_id = $array['newz_id'];
        $object->date = $array['date'];
        $object->title = $array['title'];
        $object->newz_text = $array['newz_text'];
        $object->publication_date = $array['publication_date'];
        $object->expire_date = $array['expire_date'];
        $object->after_save();
        return $object;
    
    }

    public function from_raw_objct ( $old )
    {
        $new = new super_admin_news_model();
        $new->newz_id = $old->newz_id;
        $new->date = $old->date;
        $new->title = $old->title;
        $new->newz_text = $old->newz_text;
        $new->publication_date = $old->publication_date;
        $new->expire_date = $old->expire_date;
        $new->after_save();
        return $new;
    
    }

    private function before_save ()
    {
        $this->publication_date = date("Y-m-d", 
                strtotime($this->publication_date));
        $this->date = date("Y-m-d", strtotime($this->date));
        if (isset($this->expire_date) && $this->expire_date != '')
        {
            $this->expire_date = date("Y-m-d", strtotime($this->expire_date));
        }
        else
        {
            $this->expire_date = null;
        }
    
    }

    private function after_save ()
    {
        $this->publication_date = date("d-m-Y", 
                strtotime($this->publication_date));
        $this->date = date("d-m-Y", strtotime($this->date));
        if (isset($this->expire_date) && $this->expire_date != '')
        {
            $this->expire_date = date("d-m-Y", strtotime($this->expire_date));
        }
        else
        {
            $this->expire_date = null;
        }
    
    }

    public function delete ()
    {
        $this->db->where('newz_id', $this->newz_id);
        $this->db->delete($this->table_name);
    
    }

    function save ()
    {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->newz_id = $id;
        $this->after_save();
        return $id;
    
    }

    function update ()
    {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['newz_id']);
        $this->db->where('newz_id', $this->newz_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    
    }

}

?>
