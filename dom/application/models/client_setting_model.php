<?php

class client_setting_model extends CI_Model {

// name of table
    public $table_name = 'ins_client_setting';
// fields
    public $setting_id;
    public $admin_logo;
    public $client_id;
    public $email;
    public $delete_term;
    public $welcome_text;
    public $about_us;
    public $contact;
    public $registration_text;
    public $password;
    public $smtp_server;
    public $smtp_port;
    public $default_rows;
    public $pdf_template;
    public $top_margin;
    public $bottom_margin;
    public $left_margin;
    public $right_margin;
    public $mail_logo;
    public $pdf_logo;
    public $automatic_import;
// public $interval;
    public $pdf_template_follow;
    public $top_margin_follow;
    public $bottom_margin_follow;
    public $left_margin_follow;
    public $right_margin_follow;
    public $main_user;
    public $mail_statistic;
    public $mail_type;
    public $letter_type;
    public $fontsize_h1;
    public $fontsize_h2;
    public $fontsize_h3;
    public $fontsize_normal;
    public $validation_rules;
// logo upload path
    public $logo_upload_path = './assets/upload/client_logo';
    public $pdf_template_upload_path = './assets/upload/pdf_template';
    public $mail_logo_upload_path = './assets/upload/mail_logo';
    public $pdf_logo_upload_path = './assets/upload/pdf_logo';
    public $pdf_template_follow_upload_path = './assets/upload/pdf_template_follow';

    function __construct() {
        parent::__construct();
        $this->load->model('clients_model');
        $this->validation_rules = array(
            array('field' => 'logo', 'label' => 'Logo',
                'rules' => 'file_allowed_types[jpg,png]'
            ),
            array('field' => 'delete_term', 'label' => 'Delete term',
                'rules' => 'trim|integer'
            ),
            array('field' => 'welcome_text', 'label' => 'Welcome Text',
                'rules' => 'trim'
            ),
            array('field' => 'about_us', 'label' => 'About Us',
                'rules' => 'trim'
            ),
            array('field' => 'contact', 'label' => 'Contact',
                'rules' => 'trim'
            ),
            array('field' => 'registration_text',
                'label' => 'Registration text', 'rules' => 'trim'
            ),
            array('field' => 'default_rows', 'label' => 'Default Rows',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'pdf_template', 'label' => 'PDF template',
                'rules' => 'file_allowed_types[jpg,png]'
            ),
            array('field' => 'mail_logo', 'label' => 'Mail Logo',
                'rules' => 'file_allowed_types[jpg,png]'
            ),
            array('field' => 'pdf_logo', 'label' => 'PDF Logo',
                'rules' => 'file_allowed_types[jpg,png]'
            ),
            array('field' => 'top_margin', 'label' => 'Top margin',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'bottom_margin', 'label' => 'Bottom margin',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'left_margin', 'label' => 'Left margin',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'right_margin', 'label' => 'Right margin',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'automatic_import', 'label' => 'Automatische import',
                'rules' => 'trim'
            ),
            array('field' => 'pdf_template_follow', 'label' => 'PDF sjabloon vervolgpagina"s',
                'rules' => 'file_allowed_types[jpg,png]'
            ),
            array('field' => 'top_margin_follow', 'label' => 'Bovenmarge',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'bottom_margin_follow', 'label' => 'Ondermarge',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'left_margin_follow', 'label' => 'Linkermarge',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'right_margin_follow', 'label' => 'Rechtermarge',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'letter_type', 'label' => 'Basic lettertype',
                'rules' => 'trim'
            ),
            array('field' => 'fontsize_h1', 'label' => 'Lettergrootte H1',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'fontsize_h2', 'label' => 'Lettergrootte H2',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'fontsize_h3', 'label' => 'Lettergrootte H3',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'fontsize_normal', 'label' => 'Lettergrootte normaal',
                'rules' => 'trim|numeric'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->setting_id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['setting_id']);
        $this->db->where('setting_id', $this->setting_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    function client_update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['client_id']);
        $this->db->where('client_id', $this->client_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('setting_id', $this->setting_id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();

        if ($this->setting_id != '')
            $arr['setting_id'] = $this->setting_id;

        if ($this->admin_logo != '')
            $arr['admin_logo'] = $this->admin_logo;

        if ($this->client_id != '')
            $arr['client_id'] = $this->client_id;

        if ($this->email != '')
            $arr['email'] = $this->email;

        if ($this->delete_term != '')
            $arr['delete_term'] = $this->delete_term;

        if ($this->welcome_text != '')
            $arr['welcome_text'] = $this->welcome_text;

        if ($this->about_us != '')
            $arr['about_us'] = $this->about_us;

        if ($this->contact != '')
            $arr['contact'] = $this->contact;

        if ($this->registration_text != '')
            $arr['registration_text'] = $this->registration_text;

        if ($this->password != '')
            $arr['password'] = $this->password;

        if ($this->smtp_server != '')
            $arr['smtp_server'] = $this->smtp_server;

        if ($this->smtp_port != '') {
            $arr['smtp_port'] = $this->smtp_port;
        } else {
            $arr['smtp_port'] = '0';
        }
        if ($this->default_rows != '')
            $arr['default_rows'] = $this->default_rows;

        if ($this->pdf_template != '')
            $arr['pdf_template'] = $this->pdf_template;

        if ($this->top_margin != '')
            $arr['top_margin'] = $this->top_margin;

        if ($this->bottom_margin != '')
            $arr['bottom_margin'] = $this->bottom_margin;

        if ($this->left_margin != '')
            $arr['left_margin'] = $this->left_margin;

        if ($this->right_margin != '')
            $arr['right_margin'] = $this->right_margin;

        if ($this->mail_logo != '')
            $arr['mail_logo'] = $this->mail_logo;

        if ($this->pdf_logo != '')
            $arr['pdf_logo'] = $this->pdf_logo;

//  if ($this->automatic_import != '')
        $arr['automatic_import'] = $this->automatic_import;

        if ($this->pdf_template_follow != '')
            $arr['pdf_template_follow'] = $this->pdf_template_follow;

        if ($this->top_margin_follow != '')
            $arr['top_margin_follow'] = $this->top_margin_follow;

        if ($this->bottom_margin_follow != '')
            $arr['bottom_margin_follow'] = $this->bottom_margin_follow;

        if ($this->left_margin_follow != '')
            $arr['left_margin_follow'] = $this->left_margin_follow;

        if ($this->right_margin_follow != '')
            $arr['right_margin_follow'] = $this->right_margin_follow;

//  if ($this->main_user != '')
        $arr['main_user'] = $this->main_user;
        $arr['mail_statistic'] = $this->mail_statistic;
        $arr['mail_type'] = $this->mail_type;
        $arr['letter_type'] = $this->letter_type;
        $arr['fontsize_h1'] = $this->fontsize_h1;
        $arr['fontsize_h2'] = $this->fontsize_h2;
        $arr['fontsize_h3'] = $this->fontsize_h3;
        $arr['fontsize_normal'] = $this->fontsize_normal;
        return $arr;
    }

    public function from_array($array) {
        $object = new client_setting_model();
        $object->setting_id = $array['setting_id'];
        $object->admin_logo = $array['admin_logo'];
        $object->client_id = $array['client_id'];
        $object->email = $array['email'];
        $object->delete_term = $array['delete_term'];
        $object->welcome_text = $array['welcome_text'];
        $object->about_us = $array['about_us'];
        $object->contact = $array['contact'];
        $object->registration_text = $array['registration_text'];
        $object->password = $array['password'];
        $object->smtp_server = $array['smtp_server'];
        $object->smtp_port = $array['smtp_port'];
        $object->default_rows = $array['default_rows'];
        $object->pdf_template = $array['pdf_template'];
        $object->top_margin = $array['top_margin'];
        $object->bottom_margin = $array['bottom_margin'];
        $object->left_margin = $array['left_margin'];
        $object->right_margin = $array['right_margin'];
        $object->mail_logo = $array['mail_logo'];
        $object->pdf_logo = $array['pdf_logo'];
        $object->automatic_import = $array['automatic_import'];
        $object->pdf_template_follow = $array['pdf_template_follow'];
        $object->top_margin_follow = $array['top_margin_follow'];
        $object->bottom_margin_follow = $array['bottom_margin_follow'];
        $object->left_margin_follow = $array['left_margin_follow'];
        $object->right_margin_follow = $array['right_margin_follow'];
        $object->main_user = $array['main_user'];
        $object->mail_statistic = $array['mail_statistic'];
        $object->mail_type = $array['mail_type'];
        $object->letter_type = $array['letter_type'];
        $object->fontsize_h1 = $array['fontsize_h1'];
        $object->fontsize_h2 = $array['fontsize_h2'];
        $object->fontsize_h3 = $array['fontsize_h3'];
        $object->fontsize_normal = $array['fontsize_normal'];

        return $object;
    }

    public function from_raw_objct($old) {
        $new = new client_setting_model();

        $new->setting_id = $old->setting_id;
        $new->admin_logo = $old->admin_logo;
        $new->client_id = $old->client_id;
        $new->email = $old->email;
        $new->delete_term = $old->delete_term;
        $new->welcome_text = $old->welcome_text;
        $new->about_us = $old->about_us;
        $new->contact = $old->contact;
        $new->registration_text = $old->registration_text;
        $new->password = $old->password;
        $new->smtp_server = $old->smtp_server;
        $new->smtp_port = $old->smtp_port;
        $new->default_rows = $old->default_rows;
        $new->pdf_template = $old->pdf_template;
        $new->top_margin = $old->top_margin;
        $new->bottom_margin = $old->bottom_margin;
        $new->left_margin = $old->left_margin;
        $new->right_margin = $old->right_margin;
        $new->mail_logo = $old->mail_logo;
        $new->pdf_logo = $old->pdf_logo;
        $new->automatic_import = $old->automatic_import;
        $new->pdf_template_follow = $old->pdf_template_follow;
        $new->top_margin_follow = $old->top_margin_follow;
        $new->bottom_margin_follow = $old->bottom_margin_follow;
        $new->left_margin_follow = $old->left_margin_follow;
        $new->right_margin_follow = $old->right_margin_follow;
        $new->main_user = $old->main_user;
        $new->mail_statistic = $old->mail_statistic;
        $new->mail_type = $old->mail_type;
        $new->letter_type = $old->letter_type;
        $new->fontsize_h1 = $old->fontsize_h1;
        $new->fontsize_h2 = $old->fontsize_h2;
        $new->fontsize_h3 = $old->fontsize_h3;
        $new->fontsize_normal = $old->fontsize_normal;

        return $new;
    }

    public function logo_upload_config() {
        $config['upload_path'] = './assets/upload/client_logo';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }

    public function pdf_template_upload_config() {
        $config['upload_path'] = './assets/upload/pdf_template';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }

    public function pdf_template_follow_upload_config() {
        $config['upload_path'] = './assets/upload/pdf_template_follow';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }

    public function mail_logo_upload_config() {
        $config['upload_path'] = './assets/upload/mail_logo';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }

    public function pdf_logo_upload_config() {
        $config['upload_path'] = './assets/upload/pdf_logo';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }

// get the client related information.
    public function get_client_object() {
        $clients = $this->clients_model->get_where(
                array('client_id' => $this->client_id
                ));
        return $clients[0];
    }

    public function delete_logo() {
        $this->before_save();
        $array = array('admin_logo' => NULL);
        unset($array['setting_id']);
        $this->db->where('setting_id', $this->setting_id);
        $this->db->update($this->table_name, $array);
        echo $this->db->last_query();
        $this->after_save();
    }

    public function delete_pdf_template() {
        $this->before_save();
        $array = array('pdf_template' => NULL);
        unset($array['setting_id']);
        $this->db->where('setting_id', $this->setting_id);
        $this->db->update($this->table_name, $array);
// echo $this->db->last_query();
        $this->after_save();
    }

    public function delete_pdf_template_follow() {
        $this->before_save();
        $array = array('pdf_template_follow' => NULL);
        unset($array['setting_id']);
        $this->db->where('setting_id', $this->setting_id);
        $this->db->update($this->table_name, $array);
// echo $this->db->last_query();
        $this->after_save();
    }

    public function delete_mail_logo() {
        $this->before_save();
        $array = array('mail_logo' => NULL);
        unset($array['setting_id']);
        $this->db->where('setting_id', $this->setting_id);
        $this->db->update($this->table_name, $array);
        echo $this->db->last_query();
        $this->after_save();
    }

    public function delete_pdf_logo() {
        $this->before_save();
        $array = array('pdf_logo' => NULL);
        unset($array['setting_id']);
        $this->db->where('setting_id', $this->setting_id);
        $this->db->update($this->table_name, $array);
        echo $this->db->last_query();
        $this->after_save();
    }

    function updatestatisticsetting() {
        $array = array('mail_statistic' => $this->mail_statistic, 'mail_type' => $this->mail_type);
        $this->db->where('client_id', $this->client_id);
        $this->db->update($this->table_name, $array);
        return true;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>
