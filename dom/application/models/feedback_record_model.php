<?php

class feedback_record_model extends CI_Model {

    public $table_name = 'ins_feedback_record';
    public $feed_id;
    public $feed_user_id;
    public $feed_username;
    public $feed_email;
    public $feed_cat_id;
    public $feed_rating;
    public $feed_remark;
    public $feed_created;
    public $validation_rules;
    // logo upload path
    public $logo_upload_path = './assets/images/feedback';

    function __construct() {
        parent::__construct();

        $this->validation_rules = array();
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->feed_id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['feed_id']);
        $this->db->where('feed_id', $this->feed_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('feed_id', $this->feed_id);
        $this->db->delete($this->table_name);
        return TRUE;
    }

    public function get_all() {
        $objects = array();
        $this->db->order_by("feed_id", "DESC");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['feed_id'] = $this->feed_id;
        $arr['feed_user_id'] = $this->feed_user_id;
        $arr['feed_username'] = $this->feed_username;
        $arr['feed_email'] = $this->feed_email;
        $arr['feed_cat_id'] = $this->feed_cat_id;
        $arr['feed_rating'] = $this->feed_rating;
        $arr['feed_remark'] = $this->feed_remark;
        $arr['feed_created'] = $this->feed_created;
        return $arr;
    }

    public function from_array($array) {
        $object = new feedback_record_model();
        $object->feed_id = $array['feed_id'];
        $object->feed_user_id = $array['feed_user_id'];
        $object->feed_username = $array['feed_username'];
        $object->feed_email = $array['feed_email'];
        $object->feed_cat_id = $array['feed_cat_id'];
        $object->feed_rating = $array['feed_rating'];
        $object->feed_remark = $array['feed_remark'];
        $object->feed_created = $array['feed_created'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new feedback_record_model();
        $new->feed_id = $old->feed_id;
        $new->feed_user_id = $old->feed_user_id;
        $new->feed_username = $old->feed_username;
        $new->feed_email = $old->feed_email;
        $new->feed_cat_id = $old->feed_cat_id;
        $new->feed_rating = $old->feed_rating;
        $new->feed_remark = $old->feed_remark;
        $new->feed_created = $old->feed_created;
        $new->after_save();
        return $new;
    }

    public function logo_upload_config() {
        $config['upload_path'] = './assets/images/feedback';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }

    private function before_save() {
        if (!isset($this->feed_created)) {
            $this->feed_created = date('d-m-Y', time());
        }
        $this->feed_created = date("Y-m-d", strtotime($this->feed_created));
    }

    private function after_save() {
        $this->feed_created = date("d-m-Y", strtotime($this->feed_created));
    }

}

?>
