<?php

/**
 * Each form will have many categories.
 * This model
 * represents the categories from the database.
 *
 * @author umang
 *        
 */
class forms_categories_model extends CI_Model {

    public $table_name;
    public $cat_id;
    public $form_id;
    public $form_name;
    public $cat_name;
    public $sequence;
    public $introduction_text;
    public $validation_rules;

    public function __construct($id = '') {
        parent::__construct();
        $this->load->model('forms_model');
        $this->table_name = 'ins_forms_categories';

        $this->validation_rules = array(
            array(
                'field' => 'cat_name',
                'label' => 'Categorienaam',
                'rules' => 'trim|required|name_validator|isDataExit_validator[' . implode(',', array($id, 'cat_name', 'forms_categories_model')) . ']'),
            array(
                'field' => 'introduction_text',
                'label' => 'Introductietekst',
                'rules' => 'trim'),
            array(
                'field' => 'sequence',
                'label' => 'Volgorde',
                'rules' => 'required|trim|integer|isDataExit_validator[' . implode(',', array($id, 'sequence', 'forms_categories_model')) . ']')
        );
    }

    function getFormName($form_id) {
        $res = $this->forms_model->selectSingleRecord('form_id', $form_id);
        return $res[0]->form_name;
    }

    public function dataUpdateSave() {
        $id = isset($this->cat_id);

        if ($id) {
            $query = $this->db->query(
                    "update  " . $this->table_name . " set form_id='" .
                    $this->db->escape_str($this->form_id) .
                    "', cat_name='" .
                    $this->db->escape_str($this->cat_name) .
                    "', sequence='" .
                    $this->db->escape_str($this->sequence) .
                    "', introduction_text='" .
                    $this->db->escape_str($this->introduction_text) .
                    "' where cat_id=" . $this->cat_id);
        } else {
            $data = array('form_id' => $this->db->escape_str($this->form_id),
                'cat_name' => $this->db->escape_str($this->cat_name),
                'sequence' => $this->db->escape_str($this->sequence),
                'introduction_text' => $this->db->escape_str(
                        $this->introduction_text)
            );

            $this->db->insert($this->table_name, $data);
        }

        return TRUE;
    }

    public function get_where($where) {
        $this->load->database();

        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->formObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function get_where_sort($where, $sort = 'desc', $limit = null) {
        $this->load->database();
        $objects = array();
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where($where);
        $this->db->order_by('sequence', $sort);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->formObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function get_all() {
        $objects = array();
        $sql = "SELECT * FROM " . $this->table_name . " order by cat_id desc";
        $query = $this->db->query($sql);
        foreach ($query->result() as $row) {
            $objects[] = $this->formObject($row);
        }
        return $objects;
    }

    public function formObject($old) {
        $new = new forms_categories_model();

        $new->cat_id = $old->cat_id;
        $new->form_id = $old->form_id;
        $new->form_name = $this->getFormName($old->form_id);
        $new->cat_name = $old->cat_name;
        $new->sequence = $old->sequence;
        $new->introduction_text = $old->introduction_text;

        return $new;
    }

    function selectSingleRecord($field, $value) {
        $value = $this->db->escape_str($value);

        $objects = array();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                $value . "' order by cat_id asc";
        $query = $this->db->query($sql);

        if ($query->num_rows() == 1) {
            foreach ($query->result() as $row) {
                $objects[] = $this->formObject($row);
            }
        } else {
            $objects = FALSE;
        }

        return $objects;
    }

    function selectMoreRecord($field, $value) {
        $value = $this->db->escape_str($value);

        $objects = array();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                $value . "' order by sequence asc";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $objects[] = $this->formObject($row);
            }
        } else {
            $objects = FALSE;
        }

        return $objects;
    }

    function deleteData($id) {
        $query = $this->db->query(
                'delete from ' . $this->table_name . ' where cat_id=' . $id);
        return TRUE;
    }

    function isDataExit($id, $field, $value) {
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                $value . "' and form_id='" . $id . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() == 1) {
            foreach ($query->result() as $row) {
                $objects[] = $this->formObject($row);
            }
        } else {
            $objects = FALSE;
        }

        return $objects;
    }

    /**
     * 1.
     * returns the first category if only form id is passed
     * 2. returns the next category(whos id is greater than passed category id)
     * 3. a blank array is returned if no category is found
     *
     * @param int $form_id
     *            is the form id for which you want to get category
     * @param int,string $category_id
     *            is optional. if you pass, it should be numeric
     */
    function get_next_category($form_id, $category_id = '') {
        $query = null;
        if ($category_id == '') {
            $this->db->order_by("sequence", "asc");
            $query = $this->db->get_where($this->table_name, array('form_id' => $form_id
                    ), 1);
        } else {
            // get the old category
            // so that we can get an item whos sequence is
            // greater than the sequence of item with category_id
            $old_categories = $this->get_where(
                    array('cat_id' => $category_id
                    ));
            $old_categorie = $old_categories[0];

            $this->db->order_by("sequence", "asc");
            $query = $this->db->get_where($this->table_name, array('form_id' => $form_id,
                'sequence > ' => $old_categorie->sequence
                    ), 1);
        }
        return $query->result();
    }

    /**
     * returns an array of objects of forms_categories_question_model that are
     * associated with given form category.
     *
     * @return array[forms_categories_question_model]
     */
    function get_questions() {
        $this->load->model('forms_categories_question_model');
        return $this->forms_categories_question_model->selectMoreRecord(
                        'cat_id', $this->cat_id);
    }

    /**
     * returns previous category category id if the previous category is there
     * that belongs to the
     * current FORM
     *
     * @return int previous category id
     */
    public function get_previous_category_id() {
        // get the category whos id is less tan thae current category id
        // the category must also belong to the same form
        $form_id = $this->form_id;
        $category_id = $this->cat_id;
        $this->db->where('cat_id <', $category_id);
        $this->db->order_by("cat_id", "desc");
        $res = $this->get_where(array('form_id' => $form_id
                ));

        if (isset($res[0])) {
            return $res[0]->cat_id;
        }
    }

}

?>
