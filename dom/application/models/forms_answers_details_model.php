<?php

/**
 * This model represents the table from database that
 * stores the actuals answers given by the policy holders.
 * 
 * @author umang
 *        
 */
class forms_answers_details_model extends CI_Model {

    /**
     *
     * @var string name of table to which this classes is mapped to
     */
    public $table_name = 'ins_forms_answers_details';

    /**
     *
     * @var int primary key for the table, used to uniquely identify the
     *      record/object
     */
    public $answer_detail_id;

    /**
     *
     * @var int id of the answer that was enetered by the policy holder
     */
    public $user_answer_id;

    /**
     *
     * @var int id of choice selected by the user
     */
    public $answer_id;

    /**
     *
     * @var array() validation rules to be applied for items in this model
     */
    public $validation_rules;

    /**
     * Default constructor for the classes.
     * Initializes array in $validation_rules field.
     */
    function __construct() {
        parent::__construct();

        $this->validation_rules = array();
    }

    /**
     * Saves the item to the database (inserts record).
     *
     * @return int id of the record just inserted
     */
    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->answer_detail_id = $id;
        $this->after_save();
        return $id;
    }

    /**
     * Updates the record in the database.
     *
     * @return void
     */
    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['answer_detail_id']);
        $this->db->where('answer_detail_id', $this->answer_detail_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    /**
     * Deletes the record from the database.
     *
     * @return void
     */
    public function delete() {
        $this->db->where('answer_detail_id', $this->answer_detail_id);
        $this->db->delete($this->table_name);
    }
    public function delete_temp_data() {
        $this->db->where('user_answer_id', $this->user_answer_id);
        $this->db->delete($this->table_name);
    }

    /**
     * Returns all te records for form_answer_details model
     *
     * @return forms_answers_details_model[]
     */
    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    /**
     * Returns associative array for the answer details
     *
     * @return array()
     */
    public function to_associative_array() {
        $arr = array();

        if ($this->answer_detail_id != '') {
            $arr['answer_detail_id'] = $this->answer_detail_id;
        }
        
        if ($this->user_answer_id != '') {
            $arr['user_answer_id'] = $this->user_answer_id;
        }
        
        if ($this->answer_id != '') {
            $arr['answer_id'] = $this->answer_id;
        }
        return $arr;
    }

    /**
     * Constructs an object from the array
     *
     * @param
     *            array() array that represents properties of the object that we
     *            want to construct
     * @return forms_answers_details_model object constructed from the array
     */
    public function from_array($array) {
        $object = new forms_answers_details_model();
        $object->answer_detail_id = $array['answer_detail_id'];
        $object->user_answer_id = $array['user_answer_id'];
        $object->answer_id = $array['answer_id'];
        return $object;
    }

    /**
     * This method convers the raw object to the real object.
     *
     * @param Object $old
     *            raw object that came from the active record query
     * @return forms_answers_details_model is the object that we want
     */
    public function from_raw_objct($old) {
        $new = new forms_answers_details_model();
        $new->answer_detail_id = $old->answer_detail_id;
        $new->user_answer_id = $old->user_answer_id;
        $new->answer_id = $old->answer_id;
        return $new;
    }

    /**
     * This is a life cyle method of the current class.
     * This method will be invoked before executing any statements of save.
     * Put things like converting from user readable date to database date etc
     * in this function.
     */
    private function before_save() {
        
    }

    /**
     * This is a life cycle method and of current class.
     * This method will be invoked after executing save method of the calss.
     * Put things like converting from database date to user readable date etc
     * in this function.
     */
    private function after_save() {
        
    }

    /**
     * returns object of forms_categories_question_answer_model (object of
     * available choice)
     *
     * @return forms_categories_question_answer_model
     */
    public function get_selected_item_details() {
        $this->load->model('forms_categories_question_answer_model');
        $answer = new forms_categories_question_answer_model();
        $answers = $answer->get_where(
                array('answer_id' => $this->answer_id
                ));
        $answer = $answers[0];
        return $answer;
    }

}

?>
