<?php

/**
 * a model that represents the client's email template from the database
 *
 * @author umang
 *        
 */
class client_email_template_model extends CI_Model {

// name of table
    public $table_name = 'client_email_templates';
// validation rules to be applied
    public $validation_rules;
// fields of the database
    public $email_id;
    public $email_name;
    public $email_type;
    public $email_subject;
    public $email_message;
    public $email_attachment;
    public $email_format_info;
    public $email_bcc_admin;
    public $client_id;
    public $manageable;
    public $email_dynamic_fields;

    function __construct() {
        parent::__construct();
        $this->load->model('clients_model');

// set the default value false for data member
        $this->email_bcc_admin = false;

        $this->validation_rules = array(
            array('field' => 'email_id', 'label' => 'ID',
                'rules' => 'required'
            ),
            array('field' => 'email_type', 'label' => 'Email Type',
                'rules' => 'required'
            ),
            array('field' => 'email_subject', 'label' => 'Email Subject',
                'rules' => 'required|trim'
            ),
            array('field' => 'email_message', 'label' => 'Email Message',
                'rules' => 'required|trim'
            ),
            array('field' => 'email_name', 'label' => 'Email Name',
                'rules' => 'required'
            ),
            array('field' => 'email_attachment',
                'label' => 'Email Attachement', 'rules' => ''
            ),
            array('field' => 'email_format_info',
                'label' => 'Email Format Info', 'rules' => ''
            ),
            array('field' => 'email_bcc_admin', 'label' => 'Email Bcc Admin',
                'rules' => 'required'
            ),
            array('field' => 'client_id', 'label' => 'Client Id',
                'rules' => 'required'
            ),
            array('field' => 'manageable', 'label' => 'manageable',
                'rules' => 'trim'
            ),
            array('field' => 'email_dynamic_fields', 'label' => 'Dynamic fields',
                'rules' => 'trim'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['email_id']);
        $this->db->where('email_id', $this->email_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('email_id', $this->email_id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['email_id'] = $this->email_id;
        $arr['email_name'] = $this->email_name;
        $arr['email_type'] = $this->email_type;
        $arr['email_subject'] = $this->email_subject;
        $arr['email_message'] = $this->email_message;
        $arr['email_attachment'] = $this->email_attachment;
        $arr['email_format_info'] = $this->email_format_info;
        $arr['email_bcc_admin'] = $this->email_bcc_admin;
        $arr['client_id'] = $this->client_id;
        $arr['manageable'] = $this->manageable;
        $arr['email_dynamic_fields'] = $this->email_dynamic_fields;
        return $arr;
    }

    public function from_array($array) {
        $object = new client_email_template_model();
        $object->email_id = $array['email_id'];
        $object->email_name = $array['email_name'];
        $object->email_type = $array['email_type'];
        $object->email_subject = $array['email_subject'];
        $object->email_message = $array['email_message'];
        $object->email_attachment = $array['email_attachment'];
        $object->email_format_info = $array['email_format_info'];
        $object->email_bcc_admin = $array['email_bcc_admin'];
        $object->client_id = $array['client_id'];
        $object->manageable = $array['manageable'];
        $object->email_dynamic_fields = $array['email_dynamic_fields'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new client_email_template_model();
        $new->email_id = $old->email_id;
        $new->email_name = $old->email_name;
        $new->email_type = $old->email_type;
        $new->email_subject = $old->email_subject;
        $new->email_message = $old->email_message;
        $new->email_attachment = $old->email_attachment;
        $new->email_format_info = $old->email_format_info;
        $new->email_bcc_admin = $old->email_bcc_admin;
        $new->client_id = $old->client_id;
        $new->manageable = $old->manageable;
        $new->email_dynamic_fields = $old->email_dynamic_fields;

        $new->after_save();
        return $new;
    }

    function get_client() {
        $client = new clients_model();
        $client = $client->get_where(
                array('client_id' => $this->client_id
                ));
        return $client[0];
    }

    private function before_save() {
        if ($this->email_bcc_admin == true) {
            $this->email_bcc_admin = 1;
        } else {
            $this->email_bcc_admin = 0;
        }
    }

    private function after_save() {
        if ($this->email_bcc_admin == 1) {
            $this->email_bcc_admin = true;
        } else {
            $this->email_bcc_admin = false;
        }
    }

}

?>
