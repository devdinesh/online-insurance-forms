<?php

/**
 * This model represents the table and mechanism
 * for accesing it from the database.
 */
class article_model extends CI_Model {

    // name of table
    public $table_name = 'ins_articles';
    // fields
    public $id;
    public $title;
    public $content;
    public $image;
    public $client_id;
    // validation rules to be applied
    public $validation_rules;
    // logo upload path
    public $logo_upload_path = './assets/upload/article_image';

    function __construct($id = '') {
        parent::__construct();

        $this->validation_rules = array(
            array('field' => 'title', 'label' => 'Titel',
                'rules' => 'required'
            ),
            array('field' => 'content', 'label' => 'Tekst',
                'rules' => 'trim'
            ),
            array('field' => 'image', 'label' => 'Afbeelding',
                'rules' => 'trim'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['id']);
        $this->db->where('id', $this->id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        // $this->db->order_by("client_name", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['id'] = $this->id;
        $arr['title'] = $this->title;
        $arr['content'] = $this->content;
        $arr['image'] = $this->image;
        $arr['client_id'] = $this->client_id;
        return $arr;
    }

    public function from_array($array) {
        $object = new article_model();
        $object->id = $array['id'];
        $object->title = $array['title'];
        $object->content = $array['content'];
        $object->image = $array['image'];
        $object->client_id = $array['client_id'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new article_model();

        $new->id = $old->id;
        $new->title = $old->title;
        $new->content = $old->content;
        $new->image = $old->image;
        $new->client_id = $old->client_id;
        $new->after_save();
        return $new;
    }

    public function logo_upload_config() {
        $config['upload_path'] = './assets/upload/article_image';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>
