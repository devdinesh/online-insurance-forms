<?php

/**
 * This model represents the table that is used to store the
 * questions that belng to a category.
 *
 * @author umang
 *        
 */
class standard_forms_categories_question_model extends CI_Model {

    public $table_name;
    public $child_table_name;
    public $question_id;
    public $cat_id;
    public $cat_name;
    public $question;
    public $help_text;
    public $sequence;
    public $answer_kind;
    public $validation_rules;
    public $answer;
    public $required;

    /**
     * this is default construct.
     * it will be called when an object is made.
     */
    public function __construct($id = '') {
        parent::__construct();
        $this->load->model('standard_forms_categories_model');
        $this->table_name = 'ins_standard_categories_question';
        $this->child_table_name = 'ins_standard_categories_answer';

        // / set the required to false by default
        $this->required = 0;

        $this->validation_rules = array(
            array('field' => 'question', 'label' => 'Question',
                'rules' => 'trim|required'
            ),
            array('field' => 'help_text', 'label' => 'Help Text',
                'rules' => 'trim'
            ),
            array('field' => 'answer_kind', 'label' => 'Answer Kind',
                'rules' => 'trim|required'
            )
        );
    }

    /**
     * this function gets the category name on passing cat_id
     *
     * @param type $cat_id
     *            : whose name we want
     * @return type
     */
    function getCategoryName($cat_id) {
        $res = $this->standard_forms_categories_model->selectSingleRecord('cat_id', $cat_id);
        return $res[0]->cat_name;
    }

    /**
     * this medthod save or update the data.
     * if $this->question_id is assigned then it will update data else
     * it will add new record to db.
     *
     * @return type
     */
    public function dataUpdateSave() {
        $status = FALSE;
        // check id is assigned
        $id = isset($this->question_id);

        // if yes update data
        if ($id) {
            $sql = "update  " . $this->table_name . " set cat_id='" .
                    $this->db->escape_str($this->cat_id) . "', question='" .
                    $this->db->escape_str($this->question) . "', sequence='" .
                    $this->db->escape_str($this->sequence) . "', help_text='" .
                    $this->db->escape_str($this->help_text) . "', answer_kind='" .
                    $this->db->escape_str($this->answer_kind) . "', required=" .
                    $this->required . "  where question_id=" .
                    $this->question_id;
            $query = $this->db->query($sql);

            $status = TRUE;
        } else {
            // else insert new record and in new record is inserted the get the
            // question id
            $data = array('cat_id' => $this->db->escape_str($this->cat_id),
                'question' => $this->db->escape_str($this->question),
                'sequence' => $this->db->escape_str($this->sequence),
                'help_text' => $this->db->escape_str($this->help_text),
                'required' => $this->required,
                'answer_kind' => $this->db->escape_str($this->answer_kind)
            );

            $this->db->insert($this->table_name, $data);
            $status = $this->db->insert_id();
        }

        return $status;
    }

    /**
     * this function get all the data from the question table.
     *
     * @return type
     */
    public function get_all() {
        $objects = array();
        $sql = "SELECT * FROM " . $this->table_name .
                " order by question_id desc";
        $query = $this->db->query($sql);
        foreach ($query->result() as $row) {
            $objects[] = $this->formObject($row);
        }
        return $objects;
    }

    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->formObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function get_where_sort($where, $sort = 'desc', $limit = null) {
        $this->load->database();
        $objects = array();
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where($where);
        $this->db->order_by('sequence', $sort);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->formObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    /**
     * this method convert the row into object.
     *
     * @param type $old            
     */
    public function formObject($old) {
        $new = new standard_forms_categories_question_model();

        $new->question_id = $old->question_id;
        $new->cat_id = $old->cat_id;
        $new->cat_name = $this->getCategoryName($old->cat_id);
        $new->question = $old->question;
        $new->help_text = $old->help_text;
        $new->sequence = $old->sequence;
        $new->answer_kind = $old->answer_kind;
        $new->required = $old->required;
        return $new;
    }

    /**
     * this medthod select the single record and it has to be single record only
     *
     * @param type $field
     *            : which filed we want to filter
     * @param type $value
     *            : what is the filter value.
     * @return boolean
     */
    function selectSingleRecord($field, $value) {
        $value = $this->db->escape_str($value);

        $objects = array();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                $value . "' order by question_id desc";
        $query = $this->db->query($sql);

        if ($query->num_rows() == 1) {
            foreach ($query->result() as $row) {
                $objects[] = $this->formObject($row);
            }
        } else {
            $objects = FALSE;
        }

        return $objects;
    }

    /**
     * this medthod select the More record and here it can be a single record or
     * more.
     *
     * @param type $field
     *            : which filed we want to filter
     * @param type $value
     *            : what is the filter value.
     * @return boolean
     */
    function selectMoreRecord($field, $value) {
        $value = $this->db->escape_str($value);

        $objects = array();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                $value . "' order by sequence asc";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $objects[] = $this->formObject($row);
            }
        } else {
            $objects = FALSE;
        }

        return $objects;
    }

    /**
     * this method will delete the data from the question table whose id is
     * passed.
     *
     * @param int $id
     *            : whcih data we want to delete.
     * @return boolean
     */
    function deleteData($id) {
        $sql = 'delete from ' . $this->table_name . ' where question_id=' . $id;
        $query = $this->db->query($sql);
        return TRUE;
    }

    public function delete_all_question() {
        $this->db->where('cat_id', $this->cat_id);
        $this->db->delete($this->table_name);
        return true;
    }

    /**
     * this medthod firslty delete the old data related to that question id in
     * Answer table.
     * and then add the new data in database.
     *
     * @param type $question_id            
     * @param type $values            
     * @return boolean
     */
    function saveAnswers($question_id, $values) {
        // first delete old data.
        $query = $this->db->query(
                'delete from ' . $this->child_table_name . ' where question_id=' .
                $question_id);

        // now enter new values in the database.
        foreach ($values as $value) {
            $data = array('question_id' => $question_id,
                'answer' => $this->db->escape_str($value)
            );
            $this->db->insert($this->child_table_name, $data);
        }

        return TRUE;
    }

    /**
     * the all the answers of related question id.
     *
     * @param type $question_id
     *            : whose answers we want.
     * @return boolean
     */
    function getAnswers($question_id) {
        $value = $this->db->escape_str($question_id);
        $objects = array();
        $sql = "select * from  " . $this->child_table_name . " where question_id='" . $value . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $objects[] = $this->formAnswerObject($row);
            }
        } else {
            $objects = FALSE;
        }

        return $objects;
    }

    /**
     * forming an object of an array for answer table
     *
     * @param type $old            
     * @return \standard_forms_categories_question_model
     */
    function formAnswerObject($old) {
        $new = new standard_forms_categories_question_model();
        $new->question_id = $old->question_id;
        $new->answer = $old->answer;
        $new->answer_id = $old->answer_id;
        $new->skip_to_questions = $old->skip_to_questions;
        return $new;
    }

    /**
     * this method delete the answer whose question _id and answer is passed.
     *
     * @param type $question_id            
     * @param type $answer            
     * @return boolean
     */
    function deleteAnswers($question_id, $answer) {
        $question_id = $this->db->escape_str($question_id);
        $answer = str_replace('%20', ' ', $this->db->escape_str($answer));
        $query = $this->db->query(
                "delete from " . $this->child_table_name . " where question_id=" .
                $question_id . " and answer like '%" . $answer . "%'");
        return TRUE;
    }

    public function get_cartegory() {
        $this->load->model('standard_forms_categories_model');
        $category = new standard_forms_categories_model();
        $category = $category->get_where(
                array('cat_id' => $this->cat_id
                ));
        $categorys = $category[0];
        return $categorys;
    }

    function isDataExit($id, $field, $value) {
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                $value . "' and cat_id='" . $id . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() == 1) {
            foreach ($query->result() as $row) {
                $objects[] = $this->formObject($row);
            }
        } else {
            $objects = FALSE;
        }

        return $objects;
    }

    function updateSequence() {
        $array = array('sequence' => $this->sequence
        );
        $this->db->where('question_id', $this->question_id);
        $this->db->update($this->table_name, $array);
        return true;
    }

    function getQuestionByCatSequence($catid, $sequence) {
        $objects = array();
        $sql = "select * from  " . $this->table_name . " where cat_id ='" .
                $catid . "' and sequence >= '" . $sequence .
                "' order by sequence asc";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $objects[] = $this->formObject($row);
            }
        } else {
            $objects = FALSE;
        }

        return $objects;
    }

    function getAllPreviousQuestion($question_id, $catid, $ques_sequence) {
        $query_get_all_question = $this->db->query(
                'SELECT fq.question_id, fq.cat_id, fq.sequence as ques_sequence , fc.sequence as cat_sequence
FROM ins_standard_categories_question fq,  ins_standard_categories fc
WHERE fq.cat_id in (select cat_id from ins_standard_categories where form_id = (select fc.form_id from ins_standard_categories fc, ins_standard_categories_question fq WHERE fq.question_id = ' .
                $question_id .
                ' and  fc.cat_id=fq.cat_id) order by sequence asc) and fc.cat_id=fq.cat_id order by fc.sequence, fq.sequence asc');

        $result_get_all_question = $query_get_all_question->result();
        $k = null;
        foreach ($result_get_all_question as $key => $res) {
            if ($res->question_id == $question_id && $res->cat_id == $catid && $res->ques_sequence == $ques_sequence) {
                $k = $key;
                break;
            }
        }
        $return = array();
        foreach ($result_get_all_question as $key1 => $res1) {
            if ($key1 < $k) {
                $return[] = $res1;
            }
        }

        return $return;
    }

    function getAllQuestionFromQiestionid($question_id, $cat_id, $sequence, $is_from_skip = false) {
        $obj_cat = new standard_forms_categories_model();
        $get_cat = $obj_cat->get_where(array('cat_id' => $cat_id
                ));

        $query_get_all_question = $this->db->query(
                'SELECT fq.question_id, fq.cat_id, fq.sequence as ques_sequence , fc.sequence as cat_sequence
FROM ins_standard_categories_question fq,  ins_standard_categories fc
WHERE fq.cat_id in (select cat_id from ins_standard_categories where form_id = (select fc.form_id from ins_standard_categories fc, ins_standard_categories_question fq WHERE fq.question_id = ' .
                $question_id .
                ' and  fc.cat_id=fq.cat_id) order by sequence asc) and fc.cat_id=fq.cat_id order by fc.sequence, fq.sequence asc');

        $result_get_all_question = $query_get_all_question->result();

        $only_question_ids_temp_1 = array();
        foreach ($result_get_all_question as $res) {
            $only_question_ids_temp_1[] = $res->question_id;
        }

        $get_key_from_question_array = array_search($question_id, $only_question_ids_temp_1);
        $temp_questions_ids = array();
        $test = array();
        if ($is_from_skip == false) {
            foreach ($only_question_ids_temp_1 as $key => $value) {
                if ($key > $get_key_from_question_array) {
                    $temp_questions_ids[] = $value;
                    $test[] = $result_get_all_question[$key];
                }
            }
        } else {
            foreach ($only_question_ids_temp_1 as $key => $value) {
                if ($key >= $get_key_from_question_array) {
                    $temp_questions_ids[] = $value;
                    $test[] = $result_get_all_question[$key];
                }
            }
        }

        $question_ids_string = implode(',', $temp_questions_ids);

        $query_get_smart_question = $this->db->query(
                'SELECT ques.question_id, ques.cat_id, ques.sequence
FROM ins_standard_categories_question ques, ins_standard_categories_answer ans 
where ans.question_id in (SELECT question_id FROM `ins_standard_categories_question` where cat_id in(select cat_id from ins_standard_categories where form_id = (select fc.form_id from ins_standard_categories fc, ins_standard_categories_question fq WHERE fq.question_id = ' .
                $question_id . ' and  fc.cat_id=fq.cat_id)) and answer_kind="radio" order by sequence asc) 
and ans.skip_to_questions IS NOT NULL 
and ques.question_id = ans.question_id and ques.question_id in (' .
                $question_ids_string . ')');


        $result_get_smart_question = $query_get_smart_question->result();

        if (!empty($result_get_smart_question)) {
            $question_ids_1 = array();
            foreach ($test as $res) {
                if ($res->cat_id == $result_get_smart_question[0]->cat_id && $res->ques_sequence ==
                        $result_get_smart_question[0]->sequence) {
                    $question_ids_1[] = $res;
                    break;
                } else {
                    $question_ids_1[] = $res;
                }
            }
            return $question_ids_1;
        } else {
            return $this->db->query('SELECT fq.question_id, fq.cat_id, fq.sequence as ques_sequence , fc.sequence as cat_sequence FROM ins_standard_categories_question fq,  ins_standard_categories fc WHERE fc.cat_id= fq.cat_id and fq.question_id in (' . $question_ids_string . ')')->result();
        }
    }

    function getAllQuestionForFormAtOnce($cat_id, $sequence) {
        $only_question_ids_temp_1 = $this->getAllQuestionID($cat_id);
        $question_details = $this->get_where_sort(array('cat_id' => $cat_id, 'sequence' => $sequence), 'desc');
        $get_key_from_question_array = array_search($question_details[0]->question_id, $only_question_ids_temp_1);
        $temp_questions_ids = array();
        foreach ($only_question_ids_temp_1 as $key => $value) {
            if ($key >= $get_key_from_question_array) {
                $temp_questions_ids[] = $value;
            }
        }

        $question_ids_string = implode(',', $temp_questions_ids);
        $result_get_smart_question = $this->getSmartQuestionDetials($cat_id, $question_ids_string);

        if (!empty($result_get_smart_question)) {
            $get_key_from_question_array_1 = array_search($result_get_smart_question[0]->question_id, $temp_questions_ids);
            // var_dump($only_question_ids_temp_1);
            // var_dump($get_key_from_question_array_1);
            $temp_questions_ids_1 = array();
            foreach ($temp_questions_ids as $key => $value) {
                if ($key <= $get_key_from_question_array_1) {
                    $temp_questions_ids_1[] = $value;
                }
            }
        } else {
            $temp_questions_ids_1 = array();
            foreach ($temp_questions_ids as $value) {
                $temp_questions_ids_1[] = $value;
            }
        }

        /* var_dump($only_question_ids_temp_1);
          var_dump($get_key_from_question_array);
          var_dump($temp_questions_ids);
          var_dump($get_key_from_question_array_1);
          var_dump($temp_questions_ids_1); */

        return $this->getSpeficyQuestionDetails($temp_questions_ids_1);
    }

    function getAllQuestionID($cat_id) {
        $query_get_all_question = $this->db->query('SELECT fq.question_id, fq.cat_id, fq.sequence as ques_sequence , fc.sequence as cat_sequence FROM ins_standard_categories_question fq,  ins_standard_categories fc WHERE fq.cat_id in  (select cat_id from ins_standard_categories where form_id = (select form_id from ins_standard_categories WHERE cat_id = ' . $cat_id . ') order by sequence asc) and fc.cat_id=fq.cat_id order by fc.sequence, fq.sequence asc');

        $result_get_all_question = $query_get_all_question->result();
        $only_question_ids = array();
        foreach ($result_get_all_question as $res) {
            $only_question_ids[] = $res->question_id;
        }

        return $only_question_ids;
    }

    function getSmartQuestionDetials($cat_id, $question_ids_string) {

        $query_get_smart_question = $this->db->query('SELECT ques.question_id, ques.cat_id, ques.sequence 
FROM ins_standard_categories_question ques, ins_standard_categories_answer ans, ins_standard_categories cat where ans.question_id in 
(SELECT question_id FROM ins_standard_categories_question ques1 , ins_standard_categories cat1 WHERE cat1.form_id = (select form_id from ins_standard_categories WHERE cat_id = ' . $cat_id . ') and cat1.cat_id=ques1.cat_id and answer_kind="radio" order by cat1.sequence asc) and ans.skip_to_questions IS NOT NULL and ques.question_id = ans.question_id and cat.cat_id=ques.cat_id and ques.question_id in (' . $question_ids_string . ') order by cat.sequence asc');
        // $query_get_smart_question = $this->db->query('SELECT ques.question_id, ques.cat_id, ques.sequence FROM ins_standard_categories_question ques, ins_standard_categories_answer ans where ans.question_id in (SELECT question_id FROM `ins_standard_categories_question` where cat_id in(select cat_id from ins_standard_categories where form_id = (select form_id from ins_standard_categories WHERE cat_id = ' . $cat_id . ')) and answer_kind="radio" order by sequence asc) and ans.skip_to_questions IS NOT NULL and ques.question_id = ans.question_id and ques.question_id in (' . $question_ids_string . ')');

        $result_get_smart_question = $query_get_smart_question->result();
        return $result_get_smart_question;
    }

    function getSpeficyQuestionDetails($temp_questions_ids) {
        return $this->db->query('SELECT fq.question_id, fq.cat_id, fq.sequence as ques_sequence , fc.sequence as cat_sequence FROM ins_standard_categories_question fq,  ins_standard_categories fc WHERE fc.cat_id= fq.cat_id and fq.question_id in (' . implode(',', $temp_questions_ids) . ') order by fc.sequence, fq.sequence asc')->result();
    }

    function anyQuestionLeft_allAtOnce($cat_id, $question_id) {
        $question_ids = $this->getAllQuestionID($cat_id);
        $count = count($question_ids) - 1;
        $get_key_from_question_array = array_search($question_id, $question_ids);
        if ($get_key_from_question_array == $count) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function updateAllSequence($question_id_array) {
        $count = 1;
        foreach ($question_id_array as $ques_id) {
            $array = array('sequence' => $count);
            $this->db->where('question_id', $ques_id);
            $this->db->update($this->table_name, $array);
            $count++;
        }
        return true;
    }

    function getNewSequenceId($cat_id) {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->where('cat_id', $cat_id);
        $this->db->order_by('sequence', 'desc');
        $this->db->limit(1);
        $res = $this->db->get();
        $result = $res->result();

        if ($res->num_rows > 0) {
            return $result[0]->sequence + 1;
        } else {
            return 1;
        }
    }

}

?>
