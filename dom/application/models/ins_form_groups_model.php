<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class ins_form_groups_model extends CI_model {

    public $form_group_id;
    public $form_group_name;
    public $created_date;
    public $updated_date;
    private $table_name = 'ins_form_groups';

    function __construct() {
        parent::__construct();
    }

    function validationRules() {
        $validation_rules = array(array('field' => 'form_group_name', 'label' => 'Group Name', 'rules' => 'required'));
        return $validation_rules;
    }

    function convertObject($old) {
        $new = new ins_form_groups_model();
        $new->form_group_id = $old->form_group_id;
        $new->form_group_name = $old->form_group_name;
        $new->created_date = $old->created_date;
        $new->updated_date = $old->updated_date;
        return $new;
    }

    function toArray() {
        $arr = array();
        if ($this->form_group_id != '')
            $arr['form_group_id'] = $this->form_group_id;

        if ($this->form_group_name != '')
            $arr['form_group_name'] = $this->form_group_name;

        if ($this->created_date != '')
            $arr['created_date'] = $this->created_date;

        if ($this->updated_date != '')
            $arr['updated_date'] = $this->updated_date;

        return $arr;
    }

    function getWhere($where, $limit = null, $orderby = null, $ordertype = null) {
        $objects = array();
        $this->db->select(' * ');
        $this->db->from($this->table_name);
        $this->db->where($where);
        if (is_null($orderby)) {
            $orderby = 'form_group_id';
        }
        if (is_null($ordertype)) {
            $ordertype = 'desc;';
        }
        $this->db->order_by($orderby, $ordertype);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->convertObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    function getAll($limit = null, $orderby = null, $ordertype = null) {
        $objects = array();
        $this->db->select(' * ');
        $this->db->from($this->table_name);
        if (is_null($orderby)) {
            $orderby = 'form_group_id';
        }
        if (is_null($ordertype)) {
            $ordertype = 'desc';
        }
        $this->db->order_by($orderby, $ordertype);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->convertObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    function insertData() {
        $array = $this->toArray();
        $this->db->insert($this->table_name, $array);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function updateData() {
        $array = $this->toArray();
        unset($array['form_group_id']);
        $this->db->where('form_group_id', $this->form_group_id);
        $this->db->update($this->table_name, $array);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function deleteData() {
        $this->db->where('form_group_id', $this->form_group_id);
        $this->db->delete($this->table_name);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>