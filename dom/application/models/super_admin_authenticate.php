<?php

/**
 * A class that represents the super admin.
 * Also used for authentication.
 * 
 * @author umang
 *        
 */
class super_admin_authenticate extends CI_Model
{

    public $table_name;

    public $super_admin_id;

    public $super_admin_username;

    public $super_admin_password;

    public $super_admin_first_name;

    public $super_admin_middle_name;

    public $super_admin_last_name;

    public $super_admin_email;

    public $validation_rules;

    function __construct ()
    {
        parent::__construct();
        $this->table_name = 'ins_super_admin';
        
        $this->validation_rules = array(
                array('field' => 'super_admin_email','label' => 'Mail',
                        'rules' => 'required|min_length[3]|valid_email'
                ),
                array('field' => 'super_admin_password','label' => 'Password',
                        'rules' => 'required|min_length[3]'
                )
        );
    
    }

    function get_by_email_id ( $email_address )
    {
        $super_admin = null;
        // construct the query
        $sql = "select * from  " . $this->table_name .
                 " where super_admin_email='" . $email_address . "'";
        $query = $this->db->query($sql);
        $result = $query->result();
        // if the data was there
        if (is_array($result))
        {
            foreach ($result as $row)
            {
                $super_admin = $this->construct_object_from_array($row);
            }
        }
        if (!isset($super_admin))
        {
            return false;
        }
        return $super_admin;
    
    }

    /**
     * this will check that login is valid or not
     * and if valid then data is passed else return false.
     *
     * @return boolean
     */
    function check_login ( $email_address, $password )
    {
        $login_status = false;
        
        // hash the password as it is md5 hashed in the database
        $password_hashed = md5($password);
        
        // get the object by email address
        $objects = new super_admin_authenticate();
        $super_admin = $this->get_by_email_id($email_address);
        
        // if the user was found
        if ($super_admin != false)
        {
            // if the password matches return success
            $passwords_equal = strcasecmp($super_admin->super_admin_password, 
                    $password_hashed);
            if ($passwords_equal == 0)
            {
                $login_status = true;
            }
            else
            {
                // the password does not match
                $login_status = "Gebruikersnaam en/of wachtwoord is niet juist.";
            }
        }
        else
        {
            // return the error message as the user was not found
            return "Gebruikersnaam en/of wachtwoord is niet juist.";
        }
        
        return $login_status;
    
    }

    /**
     * this function will check the mail for reset password link and if the mail
     * is matched the admin password is updated by random id.
     *
     * @param type $randid
     *            : alpha-numeric string.
     * @return int
     */
    public function checkMail ( $randid )
    {
        $email = $this->db->escape_str($this->input->post('super_admin_email'));
        $query = $this->db->query(
                "select super_admin_email from  " . $this->table_name .
                         " where super_admin_email='" . $email . "'");
        
        if ($query->num_rows() == 1)
        {
            $data = $query->result();
            if ($data[0]->super_admin_email === $email)
            {
                $str = $this->db->query(
                        "update  " . $this->table_name .
                                 " set super_admin_password='" . $randid .
                                 "' where super_admin_email ='" . $email . "'");
                return $email;
            }
            else
            {
                return 1;
            }
        }
        else
        {
            return 2;
        }
    
    }

    /**
     * reset the new password whose password is matched by the random id.
     *
     * @param type $randid
     *            : alpha-numeric string
     * @return boolean
     */
    function resetNewPassword ( $randid )
    {
        $randid = $this->db->escape_str($randid);
        $password = $this->db->escape_str(
                md5($this->input->post('new_password')));
        $update_query = $this->db->query(
                "update   " . $this->table_name . "  set super_admin_password ='" .
                         $password . "' where super_admin_password='" . $randid .
                         "'");
        return true;
    
    }

    /**
     * this will return the single reord according to data passed in filed and
     * value.
     *
     * @param type $field
     *            : which field is to be checked.
     * @param type $value
     *            : what is the value.
     */
    function selectSingleRecord ( $field, $value )
    {
        $value = $this->db->escape_str($value);
        
        $objects = new super_admin_authenticate();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                 $value . "'";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() == 1)
        {
            $result = $query->result();
            foreach ($result as $row)
            {
                $data = new super_admin_authenticate();
                $data->super_admin_id = $row->super_admin_id;
                $data->super_admin_first_name = $row->super_admin_first_name;
                $data->super_admin_middle_name = $row->super_admin_middle_name;
                $data->super_admin_last_name = $row->super_admin_last_name;
                $data->super_admin_email = $row->super_admin_email;
                $data->super_admin_username = $row->super_admin_username;
                $objects = $data;
            }
        }
        else
        {
            $objects = FALSE;
        }
    
    }

    function construct_object_from_array ( $row )
    {
        $super_admin = new super_admin_authenticate();
        $super_admin->super_admin_id = $row->super_admin_id;
        $super_admin->super_admin_first_name = $row->super_admin_first_name;
        $super_admin->super_admin_middle_name = $row->super_admin_middle_name;
        $super_admin->super_admin_last_name = $row->super_admin_last_name;
        $super_admin->super_admin_email = $row->super_admin_email;
        $super_admin->super_admin_username = $row->super_admin_username;
        $super_admin->super_admin_password = $row->super_admin_password;
        return $super_admin;
    
    }

    function to_array ()
    {
        $arr = array();
        $arr['super_admin_id'] = $this->super_admin_id;
        $arr['super_admin_first_name'] = $this->super_admin_first_name;
        $arr['super_admin_middle_name'] = $this->super_admin_middle_name;
        $arr['super_admin_last_name'] = $this->super_admin_last_name;
        $arr['super_admin_email'] = $this->super_admin_email;
        $arr['super_admin_username'] = $this->super_admin_username;
        $arr['super_admin_password'] = $this->super_admin_password;
        return $arr;
    
    }

    public function get_all ()
    {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row)
        {
            $super_admin = new super_admin_authenticate();
            $super_admin->super_admin_id = $row->super_admin_id;
            $super_admin->super_admin_first_name = $row->super_admin_first_name;
            $super_admin->super_admin_middle_name = $row->super_admin_middle_name;
            $super_admin->super_admin_last_name = $row->super_admin_last_name;
            $super_admin->super_admin_email = $row->super_admin_email;
            $super_admin->super_admin_username = $row->super_admin_username;
            $super_admin->super_admin_password = $row->super_admin_password;
            
            $objects[] = $super_admin;
        }
        return $objects;
    
    }

    public function save ()
    {
        $this->before_save();
        $array = $this->to_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->client_id = $id;
        $this->after_save();
        return $id;
    
    }

    public function before_save ()
    {
    
    }

    public function after_save ()
    {
    
    }

    public function get_where ( $where )
    {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row)
        {
            $obj = $this->from_raw_object($row);
            $objects[] = $obj;
        }
        return $objects;
    
    }

    public function from_raw_object ( $row )
    {
        $super_admin = new super_admin_authenticate();
        $super_admin->super_admin_id = $row->super_admin_id;
        $super_admin->super_admin_first_name = $row->super_admin_first_name;
        $super_admin->super_admin_middle_name = $row->super_admin_middle_name;
        $super_admin->super_admin_last_name = $row->super_admin_last_name;
        $super_admin->super_admin_email = $row->super_admin_email;
        $super_admin->super_admin_username = $row->super_admin_username;
        $super_admin->super_admin_password = $row->super_admin_password;
        return $super_admin;
    
    }

    function update ()
    {
        $this->before_save();
        $array = $this->to_array();
        unset($array['super_admin_id']);
        $this->db->where('super_admin_id', $this->super_admin_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    
    }

}

?>
