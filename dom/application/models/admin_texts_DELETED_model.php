<?php

/**
 * Represents text that can be maintained.
 *
 * Texts are associated with admins at the moment.
 *
 * @author umang
 *        
 */
class admin_texts_DELETED_model extends CI_model
{

    public $text_id;

    public $text_title;

    public $text_content;

    public $page_title;

    public $meta_tags;

    public $meta_desc;

    public $is_no_index;

    public $is_no_follow;

    public $include_sitemap;

    public $table_name;

    public $validation_rules;

    /**
     * this is the default constructor
     */
    function __construct ()
    {
        parent::__construct();
        
        $this->load->model('clients_model');
        $this->table_name = 'ins_texts';
        
        $this->validation_rules = array(
                array('field' => 'text_content','label' => 'Texts',
                        'rules' => 'required'
                ),
                array('field' => 'page_title','label' => 'Page Title',
                        'rules' => 'required'
                ),
                array('field' => 'meta_tags','label' => 'Meta Tags',
                        'rules' => 'required'
                ),
                array('field' => 'meta_desc','label' => 'Meta Description',
                        'rules' => 'required'
                )
        );
    
    }

    function fromObject ( $old )
    {
        $new = new admin_texts_DELETED_model();
        $new->text_id = $old->text_id;
        $new->text_title = $old->text_title;
        $new->text_content = $old->text_content;
        $new->page_title = $old->page_title;
        $new->meta_tags = $old->meta_tags;
        $new->meta_desc = $old->meta_desc;
        $new->is_no_index = $old->is_no_index;
        $new->is_no_follow = $old->is_no_follow;
        $new->include_sitemap = $old->include_sitemap;
        
        return $new;
    
    }

    function getAll ()
    {
        $objects = array();
        $sql = "SELECT * FROM " . $this->table_name . " order by text_id";
        $query = $this->db->query($sql);
        foreach ($query->result() as $row)
        {
            $objects[] = $this->fromObject($row);
        }
        return $objects;
    
    }

    function get_wher ( $where )
    {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row)
        {
            $obj = $this->fromObject($row);
            $objects[] = $obj;
        }
        return $objects;
    
    }

    function selectSingleRecord ( $field, $value )
    {
        $value = $this->db->escape_str($value);
        
        $objects = array();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                 $value . "' order by text_id";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() == 1)
        {
            foreach ($query->result() as $row)
            {
                $objects[] = $this->fromObject($row);
            }
        }
        else
        {
            $objects = FALSE;
        }
        
        return $objects;
    
    }

    function selectMoreRecord ( $field, $value )
    {
        $value = $this->db->escape_str($value);
        
        $objects = array();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                 $value . "' order by text_id";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $objects[] = $this->fromObject($row);
            }
        }
        else
        {
            $objects = FALSE;
        }
        
        return $objects;
    
    }

    function update ()
    {
        $array = $this->to_associative_array();
        unset($array['text_id']);
        $this->db->where('text_id', $this->text_id);
        $query_str=$this->db->update($this->table_name, $array);
        $query = $this->db->query($query_str);
        return TRUE;
    
    }
    public function to_associative_array ()
    {
        $arr = array();
        $arr['text_id'] = $this->text_id;
       // $arr['text_title'] = $this->text_title;
        $arr['text_content'] = $this->text_content;
        $arr['page_title'] = $this->page_title;
        $arr['meta_tags'] = $this->meta_tags;
        $arr['meta_desc'] = $this->meta_desc;
        $arr['is_no_index'] = $this->is_no_index;
        $arr['is_no_follow'] = $this->is_no_follow;
        $arr['include_sitemap'] = $this->include_sitemap;
        return $arr;
    
    }

}

?>
