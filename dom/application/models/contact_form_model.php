<?php

class contact_form_model extends CI_Model {

    // name of table
    public $table_name = 'contact_form';
    // fields
    public $id;
    public $name;
    public $company_name;
    public $email_address;
    public $phone_number;
    public $remark;
    public $validation_rules;

    function __construct() {
        parent::__construct();

        $this->validation_rules = array(
            array('field' => 'name', 'label' => 'Name',
                'rules' => 'required'
            ),
            array('field' => 'email_address', 'label' => 'Mail address',
                'rules' => 'required|email'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['id']);
        $this->db->where('id', $this->id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function get_all() {
        $objects = array();
        $this->db->order_by("id", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['id'] = $this->id;
        $arr['name'] = $this->name;
        $arr['company_name'] = $this->company_name;
        $arr['email_address'] = $this->email_address;
        $arr['phone_number'] = $this->phone_number;
        $arr['remark'] = $this->remark;
        return $arr;
    }

    public function from_array($array) {
        $object = new contact_form_model();
        $object->id = $array['id'];
        $object->name = $array['name'];
        $object->company_name = $array['company_name'];
        $object->email_address = $array['email_address'];
        $object->phone_number = $array['phone_number'];
        $object->remark = $array['remark'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new contact_form_model();

        $new->id = $old->id;
        $new->name = $old->name;
        $new->company_name = $old->company_name;
        $new->email_address = $old->email_address;
        $new->phone_number = $old->phone_number;
        $new->remark = $old->remark;
        $new->after_save();
        return $new;
    }

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table_name);
        return TRUE;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>
