<?php

class super_admin_setting_model extends CI_Model {

    // name of table
    public $table_name = 'ins_super_admin_setting';
    // fields
    public $id;
    public $email;
    public $logo;
    public $ftp_host;
    public $ftp_username;
    public $ftp_password;
    public $ftp_path;
    public $validation_rules;
    // logo upload path
    public $logo_upload_path = './assets/upload/client_logo';

    function __construct() {
        parent::__construct();

        $this->validation_rules = array(
            array('field' => 'logo', 'label' => 'Logo',
                'rules' => 'file_allowed_types[jpg,png]'
            ),
            array('field' => 'super_admin_email', 'label' => 'Mail',
                'rules' => 'required|min_length[3]|valid_email'
            ),
            array('field' => 'ftp_host', 'label' => 'FTP',
                'rules' => 'trim|required'
            ),
            array('field' => 'ftp_username', 'label' => 'User Name',
                'rules' => 'trim|required'
            ),
            array('field' => 'ftp_password', 'label' => 'Password',
                'rules' => 'trim|required'
            ),
            array('field' => 'ftp_path', 'label' => 'Directory Path',
                'rules' => 'trim|required'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['id']);
        $this->db->where('id', $this->id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['id'] = $this->id;
        $arr['email'] = $this->email;
        $arr['logo'] = $this->logo;
        $arr['ftp_host'] = $this->ftp_host;
        $arr['ftp_username'] = $this->ftp_username;
        $arr['ftp_password'] = $this->ftp_password;
        $arr['ftp_path'] = $this->ftp_path;
        return $arr;
    }

    public function from_array($array) {
        $object = new super_admin_setting_model();
        $object->id = $array['id'];
        $object->email = $array['email'];
        $object->logo = $array['logo'];
        $object->ftp_host = $array['ftp_host'];
        $object->ftp_username = $array['ftp_username'];
        $object->ftp_password = $array['ftp_password'];
        $object->ftp_path = $array['ftp_path'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new super_admin_setting_model();

        $new->id = $old->id;
        $new->email = $old->email;
        $new->logo = $old->logo;
        $new->ftp_host = $old->ftp_host;
        $new->ftp_username = $old->ftp_username;
        $new->ftp_password = $old->ftp_password;
        $new->ftp_path = $old->ftp_path;
        return $new;
    }

    public function logo_upload_config() {
        $config['upload_path'] = './assets/upload/client_logo';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>
