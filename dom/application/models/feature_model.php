<?php

/**
 * This model represents the table and mechanism
 * for accesing it from the database.
 */
class feature_model extends CI_Model {

    // name of table
    public $table_name = 'ins_feature';
    // fields
    public $feature_id;
    public $feature_label;
    public $feature_image;
    public $feature_text;
    public $sequence;
    // validation rules to be applied
    public $validation_rules;
    // logo upload path
    public $logo_upload_path = './assets/upload/feature_image';

    function __construct() {
        parent::__construct();

        $this->validation_rules = array(
            array('field' => 'feature_image', 'label' => 'Feature Image',
                'rules' => 'trim'
            ),
            array('field' => 'feature_text', 'label' => 'Feature Text',
                'rules' => 'required|trim'
            ),
            array('field' => 'feature_label', 'label' => 'Feature Label',
                'rules' => 'required|trim'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->feature_id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['feature_id']);
        $this->db->where('feature_id', $this->feature_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('feature_id', $this->feature_id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        $this->db->order_by("sequence", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['feature_id'] = $this->feature_id;
        $arr['feature_label'] = $this->feature_label;
        $arr['feature_image'] = $this->feature_image;
        $arr['feature_text'] = $this->feature_text;
        $arr['sequence'] = $this->sequence;
        return $arr;
    }

    public function from_array($array) {
        $object = new feature_model();
        $object->feature_id = $array['feature_id'];
        $object->feature_label = $array['feature_label'];
        $object->feature_image = $array['feature_image'];
        $object->feature_text = $array['feature_text'];
        $object->sequence = $array['sequence'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new feature_model();
        $new->feature_id = $old->feature_id;
        $new->feature_label = $old->feature_label;
        $new->feature_image = $old->feature_image;
        $new->feature_text = $old->feature_text;
        $new->sequence = $old->sequence;
        $new->after_save();
        return $new;
    }

    public function logo_upload_config() {
        $config['upload_path'] = './assets/upload/feature_image';
        $config['allowed_types'] = 'gif|jpg|png';
        return $config;
    }

    public function get_rendom_reference() {
        $array = array();
        $feature_data = $this->get_all();
        $i = 0;
        foreach ($feature_data as $feature) {
            $array[$i] = $feature->feature_id;
            $i += 1;
        }

        $ref_id = $array[rand(0, ( count($array) - 1))];
        $random_feature = $this->get_where(
                array('feature_id' => $ref_id
                ));
        if (count($random_feature) > 0) {
            return $random_feature[0];
        } else {
            return null;
        }
    }

    function getNewSequenceId() {
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->order_by('sequence', 'desc');
        $this->db->limit(1);
        $res = $this->db->get();
        $result = $res->result();
        if ($res->num_rows > 0) {
            return $result[0]->sequence + 1;
        } else {
            return 1;
        }
    }

    function updateAllSequence($feature_id_array) {
        $count = 1;
        foreach ($feature_id_array as $feature_id) {
            $array = array('sequence' => $count);
            $this->db->where('feature_id', $feature_id);
            $this->db->update($this->table_name, $array);
            $count++;
        }
        return true;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>
