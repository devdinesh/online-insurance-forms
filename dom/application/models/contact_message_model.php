<?php

class contact_message_model extends CI_Model {

    // name of table
    public $table_name = 'contact_message';
    // fields
    public $id;
    public $subject;
    public $message;
    public $date;
    public $user_id;
    public $validation_rules;

    function __construct() {
        parent::__construct();

        $this->validation_rules = array(
            array('field' => 'subject', 'label' => 'Subject',
                'rules' => 'required'
            ),
            array('field' => 'message', 'label' => 'Message',
                'rules' => 'required'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['id']);
        $this->db->where('id', $this->id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function get_all() {
        $objects = array();
        $this->db->order_by("date", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['id'] = $this->id;
        $arr['subject'] = $this->subject;
        $arr['message'] = $this->message;
        $arr['date'] = $this->date;
        $arr['user_id'] = $this->user_id;

        return $arr;
    }

    public function from_array($array) {
        $object = new contact_message_model();
        $object->id = $array['id'];
        $object->subject = $array['subject'];
        $object->message = $array['message'];
        $object->date = $array['date'];
        $object->user_id = $array['user_id'];

        return $object;
    }

    public function from_raw_objct($old) {
        $new = new contact_message_model();

        $new->id = $old->id;
        $new->subject = $old->subject;
        $new->message = $old->message;
        $new->date = $old->date;
        $new->user_id = $old->user_id;
        $new->after_save();
        return $new;
    }

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table_name);
        return TRUE;
    }

    private function before_save() {
        if (!isset($this->date)) {
            $this->date = date('d-m-Y', time());
        }
        $this->date = date("Y-m-d", strtotime($this->date));
    }

    private function after_save() {
        $this->date = date("d-m-Y", strtotime($this->date));
    }

}

?>
