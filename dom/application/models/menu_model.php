<?php

class menu_model extends CI_model {

    public $id;
    public $title;
    public $parent;
    public $page;
    public $special_feature;
    public $table_name;
    public $validation_rules;

    function __construct() {
        parent::__construct();
        $this->table_name = 'ins_menus';
        $this->validation_rules = array(
            array('field' => 'title', 'label' => 'Title',
                'rules' => 'required|max_length[240]'
            ),
            array('field' => 'parent',
                'label' => 'Parent',
                'rules' => 'trim'
            ),
            array('field' => 'page',
                'label' => 'Page',
                'rules' => 'required'
            ),
            array('field' => 'special_feature',
                'label' => 'Special_feature',
                'rules' => 'trim'
            )
        );
    }

    function fromObject($old) {
        $new = new menu_model();
        $new->id = $old->id;
        $new->title = $old->title;
        $new->parent = $old->parent;
        $new->page = $old->page;
        $new->special_feature = $old->special_feature;
        return $new;
    }

    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['id'] = $this->id;
        $arr['title'] = $this->title;
        $arr['parent'] = $this->parent;
        $arr['page'] = $this->page;
        $arr['special_feature'] = $this->special_feature;
        return $arr;
    }

    public function from_array($array) {
        $object = new menu_model();
        $object->id = $array['id'];
        $object->title = $array['title'];
        $object->parent = $array['parent'];
        $object->page = $array['page'];
        $object->special_feature = $array['special_feature'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new menu_model();
        $new->id = $old->id;
        $new->title = $old->title;
        $new->parent = $old->parent;
        $new->page = $old->page;
        $new->special_feature = $old->special_feature;
        return $new;
    }

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table_name);
    }

    function save() {
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        return $id;
    }

    function update() {
        $array = $this->to_associative_array();
        unset($array['id']);
        $this->db->where('id', $this->id);
        $this->db->update($this->table_name, $array);
    }

    function getmenus() {
        $this->db->select("m.id,m.title,m.parent,t.text_title,t.page_slug", false);
        $this->db->from('ins_menus as m');
        $this->db->join('ins_texts  as t', ' t.text_id =m.page', 'LEFT');
        $query = $this->db->get();
        $res1 = $query->result();
        return $res1;
    }

}

?>
