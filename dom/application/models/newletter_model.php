<?php

/**
 * This model represents the table and mechanism
 * for accesing it from the database.
 */
class newletter_model extends CI_Model {

    // name of table
    public $table_name = 'ins_newsletters';
    // fields
    public $id;
    public $title;
    public $form_code;
    public $template;
    public $introduction_text;
    public $closure_text;
    public $client_id;
    // validation rules to be applied
    public $validation_rules;

    // logo upload path
    function __construct() {
        parent::__construct();

        $this->validation_rules = array(
            array('field' => 'title', 'label' => 'Titel',
                'rules' => 'required'
            ),
            array('field' => 'form_code', 'label' => 'Formuliercode',
                'rules' => 'trim|required'
            ),
            array('field' => 'template', 'label' => 'Template',
                'rules' => 'trim'
            ),
            array('field' => 'introduction_text', 'label' => 'Introduction text',
                'rules' => 'trim'
            ),
            array('field' => 'closure_text', 'label' => 'Closure text',
                'rules' => 'trim'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['id']);
        $this->db->where('id', $this->id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        // $this->db->order_by("client_name", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['id'] = $this->id;
        $arr['title'] = $this->title;
        $arr['form_code'] = $this->form_code;
        $arr['template'] = $this->template;
        $arr['introduction_text'] = $this->introduction_text;
        $arr['closure_text'] = $this->closure_text;
        $arr['client_id'] = $this->client_id;
        return $arr;
    }

    public function from_array($array) {
        $object = new newletter_model();
        $object->id = $array['id'];
        $object->title = $array['title'];
        $object->form_code = $array['form_code'];
        $object->template = $array['template'];
        $object->introduction_text = $array['introduction_text'];
        $object->closure_text = $array['closure_text'];
        $object->client_id = $array['client_id'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new newletter_model();

        $new->id = $old->id;
        $new->title = $old->title;
        $new->form_code = $old->form_code;
        $new->template = $old->template;
        $new->introduction_text = $old->introduction_text;
        $new->closure_text = $old->closure_text;
        $new->client_id = $old->client_id;
        $new->after_save();
        return $new;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>
