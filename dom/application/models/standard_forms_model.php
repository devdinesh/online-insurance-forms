<?php

class standard_forms_model extends CI_Model {

    public $table_name;
    public $form_id;
    public $form_code;
    public $form_name;
    public $form_tag;
    public $fill_in_needed;
    public $first_remind;
    public $second_remind;
    public $select_for_policyholder;
    public $header_text;
    public $introduction_text;
    public $closure_text;
    public $show_all_question_at_once;
    public $current_date;
    public $validation_rules;

    public function __construct() {
        parent::__construct();
        $this->load->helper('time_worked');
        $this->table_name = 'ins_standard_forms';

        $this->validation_rules = array(
            array('field' => 'form_name', 'label' => 'Form Name', 'rules' => 'trim|required|name_validator'),
            array('field' => 'form_tag', 'label' => 'Form Tag', 'rules' => 'trim|required'),
            array('field' => 'header_text', 'label' => 'Header Text', 'rules' => 'trim'),
            array('field' => 'introduction_text', 'label' => 'Introduction Text', 'rules' => 'trim'),
            array('field' => 'closure_text', 'label' => 'Closure Text', 'rules' => 'trim'),
            array('field' => 'f_reminder', 'label' => 'First Reminder', 'rules' => 'integer'),
            array('field' => 's_reminder', 'label' => 'Second reminder', 'rules' => 'integer'),
            array('field' => 'select_for_policyholder', 'label' => 'Selectable for policyholder', 'rules' => 'trim'),
            array('field' => 'fill_in_needed', 'label' => 'fill In Needed', 'rules' => 'trim'),
            array('field' => 'show_all_question_at_once', 'label' => 'This field', 'rules' => 'trim')
        );
    }

    public function dataUpdateSave() {
        $result = false;
        // if id is set
        $id = isset($this->form_id);

        // if id is set then update the record
        if ($id) {
            $result = $this->update();
        } else {
            $result = $this->save();
        }

        return $result;
    }

    /**
     * updates the data in the database
     */
    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['form_id']);
        $this->db->where('form_id', $this->form_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
        return $this->db->affected_rows();
    }

    /**
     * inserts new record in the database
     */
    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->form_id = $id;
        $this->after_save();

        return $id;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['form_id'] = $this->form_id;
        $arr['form_code'] = $this->form_code;
        $arr['form_name'] = $this->form_name;
        $arr['form_tag'] = $this->form_tag;
        $arr['fill_in_needed'] = $this->fill_in_needed;
        $arr['first_remind'] = $this->first_remind;
        $arr['second_remind'] = $this->second_remind;
        if ($this->select_for_policyholder != '') {
            $arr['select_for_policyholder'] = $this->select_for_policyholder;
        }
        $arr['header_text'] = $this->header_text;
        $arr['introduction_text'] = $this->introduction_text;
        $arr['closure_text'] = $this->closure_text;
        $arr['show_all_question_at_once '] = $this->show_all_question_at_once;
        $arr['current_date'] = $this->current_date;
        return $arr;
    }

    public function get_all() {
        $objects = array();
        $sql = "SELECT * FROM " . $this->table_name . " order by form_id desc";
        $query = $this->db->query($sql);
        foreach ($query->result() as $row) {
            $objects[] = $this->formObject($row);
        }

        return $objects;
    }

    public function formObject($old) {
        $new = new standard_forms_model();
        $new->form_id = $old->form_id;
        $new->form_code = $old->form_code;
        $new->form_name = $old->form_name;
        $new->form_tag = $old->form_tag;
        $new->fill_in_needed = $old->fill_in_needed;
        $new->first_remind = $old->first_remind;
        $new->second_remind = $old->second_remind;
        $new->select_for_policyholder = $old->select_for_policyholder;
        $new->header_text = $old->header_text;
        $new->introduction_text = $old->introduction_text;
        $new->closure_text = $old->closure_text;
        $new->show_all_question_at_once = $old->show_all_question_at_once;
        $new->current_date = $old->current_date;
        return $new;
    }

    function selectSingleRecord($field, $value) {
        $value = $this->db->escape_str($value);

        $objects = array();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                $value . "' order by form_id desc";
        $query = $this->db->query($sql);
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $row) {
                $objects[] = $this->formObject($row);
            }
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $this->db->order_by("form_id", "desc");
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->formObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    function deleteData($id) {
        $query = $this->db->query(
                'delete from ' . $this->table_name . ' where form_id=' . $id);
        return TRUE;
    }

    public function Delete_all_forms_details() {
        mysql_query('SET foreign_key_checks = 0');
        // / 1) delete the forms fields value...
        $this->load->model('standard_form_field_model');
        $this->standard_form_field_model->form_id = $this->form_id;
        $check0 = $this->standard_form_field_model->delete_all_forms_fields();

        // /2)delete the claims and forms informations related to that claims
        $this->load->model('claims_model');
        $get_all_claims = $this->claims_model->where(
                array('form_id' => $this->form_id
                ));
        foreach ($get_all_claims as $claims_info) {
            $this->load->model('forms_answers_model');
            // 1)delete the forms details related to that claims.
            $this->forms_answers_model->claim_id = $claims_info->claim_id;
            $check1 = $this->forms_answers_model->delete_data();
            // 1)delete claim files,delete the forms fields values and delete
            // the claim at last
            $this->claims_model->claim_id = $claims_info->claim_id;
            $check3 = $this->claims_model->delete();
        }
        $this->load->model('standard_forms_categories_model');
        $get_all_category = $this->standard_forms_categories_model->get_where(
                array('form_id' => $this->form_id
                ));
        foreach ($get_all_category as $category_info) {
            $this->load->model('standard_forms_categories_question_model');
            $this->load->model('standard_forms_categories_question_answer_model');
            $get_all_question = $this->standard_forms_categories_question_model->get_where(
                    array('cat_id' => $category_info->cat_id
                    ));
            foreach ($get_all_question as $qoestion_details) {
                if ($qoestion_details->answer_kind == "radio" ||
                        $qoestion_details->answer_kind == "checkbox") {
                    $this->standard_forms_categories_question_answer_model->question_id = $qoestion_details->question_id;
                    $check4 = $this->standard_forms_categories_question_answer_model->delete_all_answer();
                }
            }
            $this->standard_forms_categories_question_model->cat_id = $category_info->cat_id;
            $check5 = $this->standard_forms_categories_question_model->delete_all_question();
        }
        $this->standard_forms_categories_model->form_id = $this->form_id;
        $check6 = $this->standard_forms_categories_model->delete_all_category();
        $check7 = $this->deleteData($this->form_id);
        mysql_query('SET foreign_key_checks = 1');
        return TRUE;
    }

    function getLastCopiedFormName($form_name) {
        $query = $this->db->query("SELECT form_name  FROM ins_standard_forms WHERE form_name LIKE '" .
                $form_name . "%' order by form_id desc limit 1");
        $result = $query->result();
        $form_num = substr($result[0]->form_name, ( strlen($form_name) + 6));
        return $form_num + 1;
    }

    function isDataExit($id, $field, $value) {
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                $value . "' and client_id='" . $id . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() == 1) {
            foreach ($query->result() as $row) {
                $objects[] = $this->formObject($row);
            }
        } else {
            $objects = FALSE;
        }

        return $objects;
    }

    /**
     * This method checks of the form tag exists for given client or not.
     *
     * @param string $form_tag
     *            is the tag of the form for which we want to search
     * @param int $client_id
     *            is the client id for which we want to find out form tag (form
     *            tag will be specifically searched for that client only)
     * @return boolean true of the form tag exists for that client, false if the
     *         form tag does not exists.
     */
    function form_with_tag_exists($form_tag, $client_id) {
        $ans = false;
        $query = $this->db->get_where('forms', array('form_tag' => $form_tag, 'client_id' => $client_id
                ));
        $result = $query->result();

        if (count($result) > 0) {
            $ans = true;
        }
        return $ans;
    }

    /**
     * this method was intended to convert date etc in format that database
     * understands.
     * This would convert the normal date to date etc that database understands
     */
    private function before_save() {
        
    }

    /**
     * This method is called after getting the data from database.
     * This converts database dates etc to normal human readable dates.
     */
    private function after_save() {
        
    }

    /**
     * returns an array of form categories that belongs to the given form
     *
     * @return array[standard_forms_categories_model]
     */
    function get_categories() {
        return $this->standard_forms_categories_model->selectMoreRecord('form_id', $this->form_id);
    }

    // for filtering data using client name
    function get_filter_Data($client) {
        $objects = array();
        $this->db->select('*');
        if ($client !== 'nl') {
            $this->db->where("client_id", $client);
        }
        $res = $this->db->get($this->table_name);
        foreach ($res->result() as $row) {
            $obj = $this->formObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    function created_forms_between_from_and_to_date($client_id, $from_date, $to_date) {
        if (isset($to_date) && $to_date != '' && $to_date != '1970-01-01') {
            $where = "client_id=" . $client_id . " and  `current_date` BETWEEN '" .
                    $from_date . "' AND '" . $to_date . "'";
        } else {

            $where = "client_id=" . $client_id . " and  `current_date` BETWEEN '" .
                    $from_date . "' AND '" .
                    get_current_time()->get_date_for_db() . "'";
        }

        $this->db->select('count(*) as total');
        $this->db->where($where, NULL, false);
        $res = $this->db->get($this->table_name);
        $res1 = $res->result();

        return $res1[0]->total;
    }

    function is_form_exist($form_name, $client_id) {
        $query = $this->db->query(
                "SELECT form_name  FROM ins_standard_forms WHERE form_name LIKE '" . $form_name . "%' order by form_id desc");
        $result = $query->result();
        return count($result);
    }

}

?>
