<?php

/**
 * Represents policy holder who maked claim.
 * Each policyy holder can have many claims as of now.
 *
 * @author umang
 *        
 */
class policy_holder_model extends CI_Model {

    // name of table
    public $table_name = 'ins_policy_holder';
    // fields
    public $policy_holder_id;
    public $client_id;
    public $policy_number;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $email;
    public $address;
    public $zipcode;
    public $private_number;
    public $business_number;
    public $mobile_number;
    public $password;
    public $register_date;
    public $status;
    public $random_str;
    public $password_reset_random_string;
    public $old_first_name;
    public $old_middle_name;
    public $old_last_name;
    public $old_email;
    public $old_address;
    public $old_zipcode;
    public $validation_rules;

    function __construct() {
        parent::__construct();

        $this->validation_rules = array(
            array('field' => 'f_name', 'label' => 'First Name',
                'rules' => 'trim|min_length[3]|name_validator'
            ),
            array('field' => 'm_name', 'label' => 'Middle Name',
                'rules' => 'trim|min_length[3]|name_validator'
            ),
            array('field' => 'l_name', 'label' => 'Last Name',
                'rules' => 'trim|min_length[3]|name_validator'
            ),
            array('field' => 'address', 'label' => 'Address',
                'rules' => 'trim'
            ),
            array('field' => 'zipcode', 'label' => 'Zipcode',
                'rules' => 'trim'
            ),
            array('field' => 'p_number', 'label' => 'Private Number',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'b_number', 'label' => 'Bussness Number',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'm_number', 'label' => 'Mobile Number',
                'rules' => 'trim|numeric|min_length[10]'
            )
            ,
            array('field' => 'email', 'label' => 'Email Address',
                'rules' => 'required|min_length[3]|valid_email'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->policy_holder_id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['policy_holder_id']);
        $this->db->where('policy_holder_id', $this->policy_holder_id);

        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('policy_holder_id', $this->policy_holder_id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['policy_holder_id'] = $this->policy_holder_id;
        $arr['policy_number'] = $this->policy_number;
        $arr['client_id'] = $this->client_id;
        $arr['first_name'] = $this->first_name;
        $arr['middle_name'] = $this->middle_name;
        $arr['last_name'] = $this->last_name;
        $arr['email'] = $this->email;
        $arr['address'] = $this->address;
        $arr['zipcode'] = $this->zipcode;
        $arr['private_number'] = $this->private_number;
        $arr['business_number'] = $this->business_number;
        $arr['mobile_number'] = $this->mobile_number;
        $arr['password'] = $this->password;
        $arr['register_date'] = $this->register_date;
        $arr['status'] = $this->status;
        $arr['random_str'] = $this->random_str;
        $arr['password_reset_random_string'] = $this->password_reset_random_string;
        $arr['old_first_name'] = $this->old_first_name;
        $arr['old_middle_name'] = $this->old_middle_name;
        $arr['old_last_name'] = $this->old_last_name;
        $arr['old_email'] = $this->old_email;
        $arr['old_address'] = $this->old_address;
        $arr['old_zipcode'] = $this->old_zipcode;
        return $arr;
    }

    public function from_array($array) {
        $object = new policy_holder_model();
        $object->policy_holder_id = $array['policy_holder_id'];
        $object->policy_number = $array['policy_number'];
        $object->client_id = $array['client_id'];
        $object->first_name = $array['first_name'];
        $object->middle_name = $array['middle_name'];
        $object->last_name = $array['last_name'];
        $object->email = $array['email'];
        $object->address = $array['address'];
        $object->zipcode = $array['zipcode'];
        $object->private_number = $array['private_number'];
        $object->business_number = $array['business_number'];
        $object->mobile_number = $array['mobile_number'];
        $object->password = $array['password'];
        $object->register_date = $array['register_date'];
        $object->status = $array['status'];
        $object->password_reset_random_string = $array['password_reset_random_string'];
        $object->old_first_name = $array['old_first_name'];
        $object->old_middle_name = $array['old_middle_name'];
        $object->old_last_name = $array['old_last_name'];
        $object->old_email = $array['old_email'];
        $object->old_address = $array['old_address'];
        $object->old_zipcode = $array['old_zipcode'];

        return $object;
    }

    public function from_raw_objct($old) {
        $new = new policy_holder_model();
        $new->policy_holder_id = $old->policy_holder_id;
        $new->client_id = $old->client_id;
        $new->policy_number = $old->policy_number;
        $new->first_name = $old->first_name;
        $new->middle_name = $old->middle_name;
        $new->last_name = $old->last_name;
        $new->email = $old->email;
        $new->address = $old->address;
        $new->zipcode = $old->zipcode;
        $new->private_number = $old->private_number;
        $new->business_number = $old->business_number;
        $new->mobile_number = $old->mobile_number;
        $new->password = $old->password;
        $new->register_date = $old->register_date;
        $new->status = $old->status;
        $new->random_str = $old->random_str;
        $new->password_reset_random_string = $old->password_reset_random_string;
        $new->old_first_name = $old->old_first_name;
        $new->old_middle_name = $old->old_middle_name;
        $new->old_last_name = $old->old_last_name;
        $new->old_email = $old->old_email;
        $new->old_address = $old->old_address;
        $new->old_zipcode = $old->old_zipcode;
        $new->after_save();
        return $new;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

    function updatePasswordRandomString() {
        $array = array(
            'password_reset_random_string' => $this->password_reset_random_string
        );
        $this->db->where('policy_holder_id', $this->policy_holder_id);
        $this->db->update($this->table_name, $array);
    }

    function updatePolicyNumber() {
        $array = array('policy_number' => $this->policy_number
        );
        $this->db->where('policy_holder_id', $this->policy_holder_id);
        $this->db->update($this->table_name, $array);
    }

    public function checkMail($email, $randid) {
        $query = $this->db->query(
                "select * from  " . $this->table_name . " where email='" .
                $this->db->escape_str($email) . "'");

        if ($query->num_rows() == 1) {
            $data = $query->result();
            if ($data[0]->email === $email) {
                if ($data[0]->status == 'A') {
                    $this->db->query(
                            "update  " . $this->table_name . " set password='" .
                            $randid . "' where email ='" . $email . "'");
                    $res = $query->result();
                    return $res[0];
                } else {
                    return 1;
                }
            } else {
                return 2;
            }
        } else {
            return 3;
        }
    }

    public function getDisctinctspolicyholder($client_id) {
        $policyholder = array();
        $this->db->select('policy_holder_id');
        $this->db->distinct();
        $this->db->where(array('client_id' => $client_id
        ));
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            if ($row->policy_holder_id !== NULL)
                $policyholder[] = $row->policy_holder_id;
        }
        return $policyholder;
    }

    function updatePassword($randid) {
        $array = array('password' => $this->password
        );
        $this->db->where('password', $randid);
        $this->db->update($this->table_name, $array);
        if ($this->db->affected_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

}

?>
