<?php

class rates_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function getAllrates() {
        $sql = "select * from ins_rates";
        $Q = $this->db->query($sql);
        return $Q->result();
    }

    function getWhere($where) {
        $this->db->select('*');
        $this->db->from('ins_rates');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    function save($data) {
        $this->db->set($data);
        $result = $this->db->insert('ins_rates', $data);
    }

    function update($data, $where = NULL) {
        $this->db->set($data);
        $this->db->where('id', $where['id']);
        $result = $this->db->update('ins_rates', $this);
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('ins_rates');
        
    }

}
