<?php

/**
 * Represents the users table.
 * User can be one of these types: policy holder/admin
 *
 * @author umang
 *        
 */
class users_model extends CI_model {

    public $user_id;
    public $client_id;
    public $client_name;
    public $first_name;
    public $middle_name;
    public $is_handler;
    public $last_name;
    public $department;
    public $mail_address;
    public $password;
    public $register_date;
    public $role;
    public $status;
    public $administrator;
    public $table_name;
    public $last_login_date;
    public $current_login_date;
    public $role_menu;
    public $company;
    public $validation_rules;
    public $password_reset_random_string;

    /**
     * this is the default constructor
     */
    function __construct() {
        parent::__construct();

        $this->load->model('clients_model');
        $this->table_name = 'ins_users';
        $this->load->helper('time_worked');

        $this->validation_rules = array(
            array('field' => 'client_id', 'label' => 'Client',
                'rules' => 'required'
            ),
            array('field' => 'first_name', 'label' => 'First Name',
                'rules' => 'trim|required|min_length[3]|name_validator'
            ),
            array('field' => 'middle_name', 'label' => 'Middle Name',
                'rules' => 'trim|name_validator'
            ),
            array('field' => 'last_name', 'label' => 'Last Name',
                'rules' => 'trim|required|min_length[3]|name_validator'
            ),
            array('field' => 'mail_address', 'label' => 'Mail Address',
                'rules' => 'trim|required|valid_email'
            ),
            array('field' => 'password', 'label' => 'Password',
                'rules' => 'trim|required'
            ),
            array('field' => 'password_confirmation',
                'label' => 'Password Confiramtion',
                'rules' => 'trim|required|matches[password]'
            ),
            array('field' => 'role', 'label' => 'Role', 'rules' => 'required'
            ),
            array('field' => 'department', 'label' => 'department', 'rules' => 'trim'
            ),
            array('field' => 'administrator', 'label' => 'Beheerder', 'rules' => 'trim'
            ),
            array('field' => 'role_menu', 'label' => 'Role', 'rules' => 'trim'),
            array('field' => 'company', 'label' => 'Company', 'rules' => 'trim')
        );
    }

    /**
     * inserts new record in the database
     */
    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['user_id']);
        unset($array['client_name']);
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->user_id = $id;
        $this->after_save();
        return $id;
    }

    function fromObject($old) {
        $new = new users_model();

        $new->user_id = $old->user_id;
        $new->client_id = $old->client_id;
        $new->client_name = $this->getClientName($old->client_id);
        $new->first_name = $old->first_name;
        $new->middle_name = $old->middle_name;
        $new->last_name = $old->last_name;
        $new->department = $old->department;
        $new->mail_address = $old->mail_address;
        $new->register_date = $old->register_date;
        $new->is_handler = $old->is_handler;
        $new->role = $old->role;
        $new->status = $old->status;
        $new->administrator = $old->administrator;
        $new->password = $old->password;
        $new->password_reset_random_string = $old->password_reset_random_string;
        $new->last_login_date = $old->last_login_date;
        $new->current_login_date = $old->current_login_date;
        $new->role_menu = $old->role_menu;
        $new->company = $old->company;

        return $new;
    }

    function getAll() {
        $objects = array();
        $sql = "SELECT * FROM " . $this->table_name . " order by user_id";
        $query = $this->db->query($sql);
        foreach ($query->result() as $row) {
            $objects[] = $this->fromObject($row);
        }
        return $objects;
    }

    function selectSingleRecord($field, $value) {
        $value = $this->db->escape_str($value);

        $objects = array();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                $value . "' order by user_id";
        $query = $this->db->query($sql);

        if ($query->num_rows() == 1) {
            foreach ($query->result() as $row) {
                $objects[] = $this->fromObject($row);
            }
        } else {
            $objects = FALSE;
        }

        return $objects;
    }

    function selectMoreRecord($field, $value) {
        $value = $this->db->escape_str($value);

        $objects = array();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                $value . "' order by user_id";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $objects[] = $this->fromObject($row);
            }
        } else {
            $objects = FALSE;
        }

        return $objects;
    }

    function getClientName($client_id) {
        $res = $this->clients_model->get_where(
                array('client_id' => $client_id
                ));
        return @$res[0]->client_name;
    }

    function dataUpdate($id) {
        $query = $this->db->query(
                "update " . $this->table_name . " set client_id='" .
                $this->db->escape_str($this->client_id) .
                "', first_name='" .
                $this->db->escape_str($this->first_name) .
                "', middle_name='" .
                $this->db->escape_str($this->middle_name) .
                "', last_name='" .
                $this->db->escape_str($this->last_name) .
                "', department='" .
                $this->db->escape_str($this->department) .
                "', password_reset_random_string='" . $this->db->escape_str(
                        $this->password_reset_random_string) .
                "', mail_address='" .
                $this->db->escape_str($this->mail_address) . "'
                , role='" . $this->db->escape_str($this->role) . "'
                ,status='" . $this->db->escape_str($this->status) . "'
                ,administrator='" . $this->administrator . "'
                 , is_handler='" . $this->db->escape_str($this->is_handler) .
                "' , role_menu=" . $this->role_menu .
                ", company='" . $this->db->escape_str($this->company) .
                "' where user_id=" . $this->db->escape_str($id));

        if (isset($this->password)) {
            $query = $this->db->query(
                    "update " . $this->table_name . " set password='" .
                    $this->db->escape_str(md5($this->password)) .
                    "' where user_id=" . $this->db->escape_str($id));
        }
        if (isset($this->password)) {
            $query = $this->db->query(
                    "update " . $this->table_name .
                    " set password_reset_random_string='" . $this->db->escape_str(
                            $this->password_reset_random_string) .
                    "' where user_id=" . $this->db->escape_str($id));
        }

        return TRUE;
    }

    function update_user_login_date($user_id) {
        $current_date = get_current_time()->get_date_for_db();
        $user_data = $this->get_where(array('user_id' => $user_id
                ));

        $this->before_save();
        $array = array('last_login_date' => $user_data[0]->current_login_date,
            'current_login_date' => $current_date
        );
        unset($array['$user_id']);
        $this->db->where('user_id', $user_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
        return $this->db->affected_rows();
    }

    function dataDelete($id) {
        $this->db->query(
                'delete from ' . $this->table_name . ' where user_id=' .
                $this->db->escape_str($id));
        return TRUE;
    }

    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where('users', $where);
        foreach ($res->result() as $row) {
            $obj = $this->fromObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();

        $arr['user_id'] = $this->user_id;
        $arr['client_id'] = $this->client_id;
        $arr['client_name'] = $this->client_name;
        $arr['first_name'] = $this->first_name;
        $arr['middle_name'] = $this->middle_name;
        $arr['last_name'] = $this->last_name;
        $arr['department'] = $this->department;
        $arr['mail_address'] = $this->mail_address;
        $arr['register_date'] = $this->register_date;
        $arr['role'] = $this->role;
        $arr['status'] = $this->status;
        $arr['administrator'] = $this->administrator;
        $arr['password'] = $this->password;
        $arr['password_reset_random_string'] = $this->password_reset_random_string;
        $arr['last_login_date'] = $this->last_login_date;
        $arr['current_login_date'] = $this->current_login_date;
        $arr['role_menu'] = $this->role_menu;
        $arr['company'] = $this->company;
        return $arr;
    }

    function check_email_and_password($mail_address, $password) {
        $objects = $this->get_where(
                array('mail_address ' => $mail_address
                ));
        // if we got an array with items more than 0
        if (is_array($objects) && count($objects) > 0) {
            $user = $objects[0];
            // if password matches

            if (md5($password) == $user->password) {
                return true;
            } else {

                throw new InvalidPasswordException();
            }
        } else {
            // if getting array was not successful then throw the error
            throw new InvalidEmailException();
        }
    }

    /**
     * returns an object of clients_model that is associated with given user
     *
     * @return clients_model object of clients_model that is associated with
     *         given user
     */
    function get_client() {
        $clients = $this->clients_model->get_where(
                array('client_id' => $this->client_id
                ));
        return $clients[0];
    }

    // for filtering data using client name
    function get_filter_Data($client) {
        $objects = array();
        $this->db->select('*');
        if ($client !== 'nl') {
            $this->db->where("client_id", $client);
        }
        $res = $this->db->get($this->table_name);
        foreach ($res->result() as $row) {
            $obj = $this->fromObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    private function before_save() {
        if (!isset($this->register_date)) {
            $this->register_date = date('d-m-Y', time());
        }
        $this->register_date = date("Y-m-d", strtotime($this->register_date));
    }

    private function after_save() {
        $this->register_date = date("d-m-Y", strtotime($this->register_date));
    }

}

if (!class_exists('InvalidPasswordException')) {

    class InvalidPasswordException extends Exception {
        
    }

}

if (!class_exists('InvalidEmailException')) {

    class InvalidEmailException extends Exception {
        
    }

}
?>
