<?php

/**
 * a model that represents the client's email template from the database
 *
 * @author umang
 *        
 */
class newsletter_template_model extends CI_Model {

// name of table
    public $table_name = 'newsletter_templates';
// validation rules to be applied
    public $validation_rules;
// fields of the database
    public $email_id;
    public $email_name;
    public $email_type;
    public $email_subject;
    public $email_message;
    public $client_id;
    public $email_dynamic_fields;

    function __construct() {
        parent::__construct();
        $this->load->model('clients_model');

// set the default value false for data member
        $this->validation_rules = array(
            array('field' => 'email_id', 'label' => 'ID',
                'rules' => 'required'
            ),
            array('field' => 'email_type', 'label' => 'Email Type',
                'rules' => 'required'
            ),
            array('field' => 'email_subject', 'label' => 'Email Subject',
                'rules' => 'required|trim'
            ),
            array('field' => 'email_message', 'label' => 'Email Message',
                'rules' => 'required|trim'
            ),
            array('field' => 'email_name', 'label' => 'Email Name',
                'rules' => 'required'
            ),
            array('field' => 'client_id', 'label' => 'Client Id',
                'rules' => 'required'
            ),
            array('field' => 'email_dynamic_fields', 'label' => 'Dynamic fields',
                'rules' => 'trim'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['email_id']);
        $this->db->where('email_id', $this->email_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('email_id', $this->email_id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['email_id'] = $this->email_id;
        $arr['email_name'] = $this->email_name;
        $arr['email_type'] = $this->email_type;
        $arr['email_subject'] = $this->email_subject;
        $arr['email_message'] = $this->email_message;
        $arr['client_id'] = $this->client_id;
        $arr['email_dynamic_fields'] = $this->email_dynamic_fields;
        return $arr;
    }

    public function from_array($array) {
        $object = new newsletter_template_model();
        $object->email_id = $array['email_id'];
        $object->email_name = $array['email_name'];
        $object->email_type = $array['email_type'];
        $object->email_subject = $array['email_subject'];
        $object->email_message = $array['email_message'];
        $object->client_id = $array['client_id'];
        $object->email_dynamic_fields = $array['email_dynamic_fields'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new newsletter_template_model();
        $new->email_id = $old->email_id;
        $new->email_name = $old->email_name;
        $new->email_type = $old->email_type;
        $new->email_subject = $old->email_subject;
        $new->email_message = $old->email_message;
        $new->client_id = $old->client_id;
        $new->email_dynamic_fields = $old->email_dynamic_fields;

        $new->after_save();
        return $new;
    }

    function get_client() {
        $client = new clients_model();
        $client = $client->get_where(
                array('client_id' => $this->client_id
                ));
        return $client[0];
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>
