<?php

class form_field_model extends CI_Model {

    // name of table
    public $table_name = 'ins_form_fields';
    // fields
    public $field_id;
    public $field_name;
    public $name_on_form;
    public $field_type;
    public $form_id;
    // validation rules to be applied
    public $validation_rules;

    function __construct() {
        parent::__construct();

        $this->load->model('forms_model');

        $this->validation_rules = array(
            array('field' => 'field_name', 'label' => 'field_name',
                'rules' => 'required'
            ),
            array('field' => 'name_on_form', 'label' => 'name_on_form',
                'rules' => 'required'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->field_id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();

        unset($array['field_id']);
        $this->db->where('field_id', $this->field_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('field_id', $this->field_id);
        $this->db->delete($this->table_name);
    }

    public function delete_all_forms_fields() {
        $this->db->where('form_id', $this->form_id);
        $this->db->delete($this->table_name);
        return true;
    }

    public function get_all() {
        $objects = array();

        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();

        $arr['field_id'] = $this->field_id;
        $arr['field_name'] = $this->field_name;
        $arr['name_on_form'] = $this->name_on_form;
        if ($this->field_type == 'E' || $this->field_type == 'P' || $this->field_type == 'B' || $this->field_type == 'S' || $this->field_type == 'BE') {
            $arr['field_type'] = $this->field_type;
        } else {
            $arr['field_type'] = 'N';
        }
        $arr['form_id'] = $this->form_id;

        return $arr;
    }

    public function from_array($array) {
        $object = new form_field_model();
        $object->field_id = $array['field_id'];
        $object->field_name = $array['field_name'];
        $object->name_on_forms = $array['name_on_form'];
        $object->form_id = $array['form_id'];
        $object->field_type = $array['field_type'];

        return $object;
    }

    public function from_raw_objct($old) {
        $new = new form_field_model();

        $new->field_id = $old->field_id;
        $new->field_name = $old->field_name;
        $new->name_on_form = $old->name_on_form;
        $new->field_type = $old->field_type;
        $new->form_id = $old->form_id;

        $new->after_save();
        return $new;
    }

    public function isMailFiledExit($form_id) {
        $records = $this->get_where(array('field_type' => 'E', 'form_id' => $form_id));

        if (count($records) == 0) {
            return FALSE;
        } else {
            return $records[0]->field_id;
        }
    }

    public function getMailFiledFromTag($form_tag, $client_id = NULL) {

        if ($client_id == NULL) {
            $client = admin_get_client_id();
            $client_id = $client->client_id;
        } else {
            $client_id = $client_id;
        }
        $obj_forms = new forms_model();
        $form_record = $obj_forms->get_where(array('form_tag' => $form_tag, 'client_id' => $client_id));

        if (count($form_record) == 1) {
            $records = $this->get_where(array('field_type' => 'E', 'form_id' => $form_record[0]->form_id));
            if (count($records) == 0) {
                return FALSE;
            } else {
                return $records[0];
            }
        } else {
            return FALSE;
        }
    }

    public function getMailFiledFromID($form_id) {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $obj_forms = new forms_model();
        $form_record = $obj_forms->get_where(array('form_id' => $form_id, 'client_id' => $client_id));

        if (count($form_record) == 1) {
            $records = $this->get_where(array('field_type' => 'E', 'form_id' => $form_record[0]->form_id));
            if (count($records) == 0) {
                return FALSE;
            } else {
                return $records[0];
            }
        } else {
            return FALSE;
        }
    }

    public function isPolicyNumberFiledExit($form_id) {
        $records = $this->get_where(array('field_type' => 'P', 'form_id' => $form_id));

        if (count($records) == 0) {
            return FALSE;
        } else {
            return $records[0]->field_id;
        }
    }

    public function isBehandlerEmailExit($form_id) {
        $records = $this->get_where(array('field_type' => 'BE', 'form_id' => $form_id));

        if (count($records) == 0) {
            return FALSE;
        } else {
            return $records[0]->field_id;
        }
    }

    public function getPolicyNumberFiledFromTag($form_tag, $client_id = NULL) {
        if ($client_id == NULL) {
            $client = admin_get_client_id();
            $client_id = $client->client_id;
        } else {
            $client_id = $client_id;
        }
        $obj_forms = new forms_model();
        $form_record = $obj_forms->get_where(array('form_tag' => $form_tag, 'client_id' => $client_id));

        if (count($form_record) == 1) {
            $records = $this->get_where(array('field_type' => 'P', 'form_id' => $form_record[0]->form_id));
            if (count($records) == 0) {
                return FALSE;
            } else {
                return $records[0];
            }
        } else {
            return FALSE;
        }
    }

    public function getBehandlerEmailFromTag($form_tag, $client_id = NULL) {
        if ($client_id == NULL) {
            $client = admin_get_client_id();
            $client_id = $client->client_id;
        } else {
            $client_id = $client_id;
        }
        $obj_forms = new forms_model();
        $form_record = $obj_forms->get_where(array('form_tag' => $form_tag, 'client_id' => $client_id));

        if (count($form_record) == 1) {
            $records = $this->get_where(array('field_type' => 'BE', 'form_id' => $form_record[0]->form_id));
            if (count($records) == 0) {
                return FALSE;
            } else {
                return $records[0];
            }
        } else {
            return FALSE;
        }
    }

    public function getPolicyNumberFiledFromID($form_id) {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $obj_forms = new forms_model();
        $form_record = $obj_forms->get_where(array('form_id' => $form_id, 'client_id' => $client_id));

        if (count($form_record) == 1) {
            $records = $this->get_where(array('field_type' => 'P', 'form_id' => $form_record[0]->form_id));
            if (count($records) == 0) {
                return FALSE;
            } else {
                return $records[0];
            }
        } else {
            return FALSE;
        }
    }

    public function isBehandlerFiledExit($form_id) {
        $records = $this->get_where(array('field_type' => 'B', 'form_id' => $form_id));

        if (count($records) == 0) {
            return FALSE;
        } else {
            return $records[0]->field_id;
        }
    }

    public function getBehandlerFiledFromTag($form_tag, $client_id = NULL) {
        if ($client_id == NULL) {
            $client = admin_get_client_id();
            $client_id = $client->client_id;
        } else {
            $client_id = $client_id;
        }
        $obj_forms = new forms_model();
        $form_record = $obj_forms->get_where(array('form_tag' => $form_tag, 'client_id' => $client_id));

        if (count($form_record) == 1) {
            $records = $this->get_where(array('field_type' => 'B', 'form_id' => $form_record[0]->form_id));
            if (count($records) == 0) {
                return FALSE;
            } else {
                return $records[0];
            }
        } else {
            return FALSE;
        }
    }

    public function getBehandlerFiledFromID($form_id) {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $obj_forms = new forms_model();
        $form_record = $obj_forms->get_where(array('form_id' => $form_id, 'client_id' => $client_id));

        if (count($form_record) == 1) {
            $records = $this->get_where(array('field_type' => 'B', 'form_id' => $form_record[0]->form_id));
            if (count($records) == 0) {
                return FALSE;
            } else {
                return $records[0];
            }
        } else {
            return FALSE;
        }
    }
       public function getBehandlerEmailFromID($form_id) {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $obj_forms = new forms_model();
        $form_record = $obj_forms->get_where(array('form_id' => $form_id, 'client_id' => $client_id));

        if (count($form_record) == 1) {
            $records = $this->get_where(array('field_type' => 'BE', 'form_id' => $form_record[0]->form_id));
            if (count($records) == 0) {
                return FALSE;
            } else {
                return $records[0];
            }
        } else {
            return FALSE;
        }
    }

    public function isschadenummerFiledExit($form_id) {
        $records = $this->get_where(array('field_type' => 'S', 'form_id' => $form_id));

        if (count($records) == 0) {
            return FALSE;
        } else {
            return $records[0]->field_id;
        }
    }

    public function getschadenummerFiledFromTag($form_tag, $client_id = NULL) {
        if ($client_id == NULL) {
            $client = admin_get_client_id();
            $client_id = $client->client_id;
        } else {
            $client_id = $client_id;
        }
        $obj_forms = new forms_model();
        $form_record = $obj_forms->get_where(array('form_tag' => $form_tag, 'client_id' => $client_id));

        if (count($form_record) == 1) {
            $records = $this->get_where(array('field_type' => 'S', 'form_id' => $form_record[0]->form_id));
            if (count($records) == 0) {
                return FALSE;
            } else {
                return $records[0];
            }
        } else {
            return FALSE;
        }
    }

    public function getschadenummerFiledFromID($form_id) {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $obj_forms = new forms_model();
        $form_record = $obj_forms->get_where(array('form_id' => $form_id, 'client_id' => $client_id));

        if (count($form_record) == 1) {
            $records = $this->get_where(array('field_type' => 'S', 'form_id' => $form_record[0]->form_id));
            if (count($records) == 0) {
                return FALSE;
            } else {
                return $records[0];
            }
        } else {
            return FALSE;
        }
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>