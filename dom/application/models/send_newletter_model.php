<?php

/**
 * This model represents the table and mechanism
 * for accesing it from the database.
 */
class send_newletter_model extends CI_Model {

    // name of table
    public $table_name = 'ins_send_newsletters';
    // fields
    public $id;
    public $newsletter_id;
    public $date;
    public $time;
    public $mail_address;
    public $name;
    // validation rules to be applied
    public $validation_rules;

    // logo upload path
    function __construct() {
        parent::__construct();

        $this->validation_rules = array(
            array('field' => 'newsletter_id', 'label' => 'Newsletter',
                'rules' => 'trim'
            ),
            array('field' => 'date', 'label' => 'Date',
                'rules' => 'trim'
            ),
            array('field' => 'time', 'label' => 'Time',
                'rules' => 'trim'
            ),
            array('field' => 'mail_address', 'label' => 'Mail Address',
                'rules' => 'trim'
            ),
            array('field' => 'name', 'label' => 'Name',
                'rules' => 'trim'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['id']);
        $this->db->where('id', $this->id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        // $this->db->order_by("client_name", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['id'] = $this->id;
        $arr['newsletter_id'] = $this->newsletter_id;
        $arr['date'] = $this->date;
        $arr['time'] = $this->time;
        $arr['mail_address'] = $this->mail_address;
        $arr['name'] = $this->name;
        return $arr;
    }

    public function from_array($array) {
        $object = new send_newletter_model();
        $object->id = $array['id'];
        $object->newsletter_id = $array['newsletter_id'];
        $object->date = $array['date'];
        $object->time = $array['time'];
        $object->mail_address = $array['mail_address'];
        $object->name = $array['name'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new send_newletter_model();

        $new->id = $old->id;
        $new->newsletter_id = $old->newsletter_id;
        $new->date = $old->date;
        $new->time = $old->time;
        $new->mail_address = $old->mail_address;
        $new->name = $old->name;
        $new->after_save();
        return $new;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>
