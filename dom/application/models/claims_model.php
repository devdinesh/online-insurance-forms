<?php

/**
 * For each policy holder there would be claim.
 * This model represents the details of that calim.
 *
 * This class represents the insurance form of a client.
 *
 * @author umang
 *        
 */
class claims_model extends CI_Model {

    public $table_name = 'claims';
    public $claim_id;
    public $claim_sequence_number;
    public $import_date;
    public $kind;
    public $handler;
    public $handler_email;
    public $schadenummer;
    public $userid;
    public $form_id;
    public $mail_send_date;
    public $status;
    public $policy_holder_id;
    public $client_id;
    public $conform_date;
    public $is_delete;
    public $first_remind_date;
    public $second_remind_date;
    public $archive_date;

    function __construct() {
        parent::__construct();
        $this->load->model('claims_files_model');
        $this->load->model('form_field_values_model');
        ini_set('memory_limit', '512M');
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['claim_id']);
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->claim_id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['claim_id']);
        $this->db->where('claim_id', $this->claim_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    function updatePolicyHolderId() {
        $array = array('policy_holder_id' => $this->policy_holder_id,
            'handler' => $this->handler,
            'schadenummer' => $this->schadenummer
        );
        $this->db->where('claim_id', $this->claim_id);
        $this->db->update($this->table_name, $array);
        return true;
    }

    function update_claim_hendler() {
        $array = array('handler_email' => $this->handler_email);
        $this->db->where('claim_id', $this->claim_id);
        $this->db->update($this->table_name, $array);
        return true;
    }

    function update_is_delete() {
        $array = array('is_delete' => $this->is_delete
        );

        $this->db->where('claim_id', $this->claim_id);
        $this->db->update($this->table_name, $array);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return true;
        } else {
            return false;
        }
    }

    function update_mail_send_date() {
        $this->before_save();
        $array = array('mail_send_date' => $this->mail_send_date,
            'status' => $this->status
        );

        unset($array['claim_id']);
        $this->db->where('claim_id', $this->claim_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    function update_claim_info() {
        $this->before_save();
        $array = array('status' => $this->status
        );
        unset($array['claim_id']);
        $this->db->where('claim_id', $this->claim_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->claims_files_model->delete_claim_info($this->claim_id);
        $this->form_field_values_model->delete_claim_info($this->claim_id);

        $this->db->where('claim_id', $this->claim_id);
        $this->db->delete($this->table_name);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function get_all() {
        $objects = array();
        $this->db->order_by("claim_id", "desc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }

        return $objects;
    }

    public function from_raw_objct($old) {
        $new = new claims_model();
        $new->claim_id = $old->claim_id;
        $new->claim_sequence_number = $old->claim_sequence_number;
        $new->import_date = $old->import_date;
        $new->kind = $old->kind;
        $new->handler = $old->handler;
        $new->handler_email = $old->handler_email;
        $new->schadenummer = $old->schadenummer;
        $new->userid = $old->userid;
        $new->form_id = $old->form_id;
        $new->mail_send_date = $old->mail_send_date;
        $new->status = $old->status;
        $new->policy_holder_id = $old->policy_holder_id;
        $new->client_id = $old->client_id;
        $new->conform_date = $old->conform_date;
        $new->is_delete = $old->is_delete;
        $new->first_remind_date = $old->first_remind_date;
        $new->second_remind_date = $old->second_remind_date;
        $new->archive_date = $old->archive_date;
        $new->after_save();
        return $new;
    }

    public function get_forms_count_where($where) {
        $this->load->database();
        $res = $this->db->where($where)->count_all_results($this->table_name);
        return $res;
    }

    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $this->db->order_by("claim_id", "desc");
        $this->db->from($this->table_name);
        $this->db->where($where);
        $this->db->limit(50);
        $res = $this->db->get();
        if (!empty($res)) {
            foreach ($res->result() as $row) {
                $obj = $this->from_raw_objct($row);
                $objects[] = $obj;
            }
            return $objects;
        } else {
            return FALSE;
        }
    }

    public function where($where) {
        $this->load->database();
        $objects = array();
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->order_by("claim_id", "desc");
        $this->db->where($where, NULL, FALSE);
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }

        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['claim_id'] = $this->claim_id;
        $arr['claim_sequence_number'] = $this->claim_sequence_number;
        $arr['import_date'] = $this->import_date;
        $arr['kind'] = $this->kind;
        $arr['handler'] = $this->handler;
        $arr['handler_email'] = $this->handler_email;
        $arr['schadenummer'] = $this->schadenummer;
        $arr['userid'] = $this->userid;
        $arr['form_id'] = $this->form_id;
        $arr['mail_send_date'] = $this->mail_send_date;
        $arr['status'] = $this->status;
        $arr['policy_holder_id'] = $this->policy_holder_id;
        $arr['client_id'] = $this->client_id;
        $arr['conform_date'] = $this->conform_date;
        $arr['is_delete'] = $this->is_delete;
        $arr['first_remind_date'] = $this->first_remind_date;
        $arr['second_remind_date'] = $this->second_remind_date;
        $arr['archive_date'] = $this->archive_date;

        return $arr;
    }

    public function from_array($array) {
        $object = new claims_model();
        $object->claim_id = $array['claim_id'];
        $object->import_date = $array['import_date'];
        $object->claim_sequence_number = $array['claim_sequence_number'];
        $object->kind = $array['kind'];
        $object->handler = $array['handler'];
        $object->handler_email = $array['handler_email'];
        $object->schadenummer = $array['schadenummer'];
        $object->userid = $array['userid'];
        $object->form_id = $array['form_id'];
        $object->mail_send_date = $array['mail_send_date'];
        $object->status = $array['status'];
        $object->policy_holder_id = $array['policy_holder_id'];
        $object->client_id = $array['client_id'];
        $object->conform_date = $array['conform_date'];
        $object->is_delete = $array['is_delete'];
        $object->first_remind_date = $array['first_remind_date'];
        $object->second_remind_date = $array['second_remind_date'];
        $object->archive_date = $array['archive_date'];

        return $object;
    }

    private function before_save() {
        if ($this->mail_send_date) {
            $this->mail_send_date = date("Y-m-d", strtotime($this->mail_send_date));
        }
        if ($this->import_date) {
            $this->import_date = date("Y-m-d", strtotime($this->import_date));
        }
        if ($this->conform_date) {
            $this->conform_date = date("Y-m-d", strtotime($this->conform_date));
        }
    }

    private function after_save() {
        if ($this->mail_send_date) {
            $this->mail_send_date = date("d-m-Y", strtotime($this->mail_send_date));
        }
        if ($this->import_date) {
            $this->import_date = date("d-m-Y", strtotime($this->import_date));
        }
        if ($this->conform_date) {
            $this->conform_date = date("d-m-Y", strtotime($this->conform_date));
        }
    }

    /**
     * for filtering the data by status, userid and kind.
     *
     * @param type $status
     *            must be entered. If you do not want to filter by this field
     *            then enter 'nl' in the value.
     * @param type $userid
     *            is the userid of the form. If you do not want to filter by
     *            this field
     *            then enter 'nl' in the value.
     * @param type $kind
     *            is the kind of form. If you do not want to filter by this
     *            field
     *            then enter 'nl' in the value.
     * @return insurance_forms_model[] returns an array of objects of
     *         list_insurance_model
     */
    public function get_filter_Data($form_id, $status, $handler, $schadenummer, $policyholder, $client_id) {
        $objects = array();
        $this->db->select('*');
        if ($form_id !== 'nl') {
            $this->db->where("form_id", $form_id);
        }

        if ($status !== 'nl') {
            $this->db->where("status", $status);
        }

        if ($handler != 'nl') {
            $this->db->where('handler', $handler);
        }
        if ($schadenummer != 'nl') {
            $this->db->where('schadenummer', $schadenummer);
        }
        if ($policyholder != 'nl') {
            $this->db->where('policy_holder_id', $policyholder);
        }
        $this->db->order_by("claim_id", "desc");
        $where = "`client_id` = " . $client_id . " and `is_delete` != 1";
        $this->db->where($where);
        $res = $this->db->get($this->table_name);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    /**
     * returns distinct statuses for given client
     *
     * @param string $client_id
     *            is the client id
     * @return array[string] is an array of statuses
     */
    public function get_disctinct_status($client_id) {

        $this->db->select('status');
        $this->db->distinct();
        $this->db->order_by("CAST(`status` AS CHAR)", "ASC");
        $this->db->where(array('client_id' => $client_id));
        return $this->db->get($this->table_name)->result();
    }

    /**
     * retusn names of disntict handlers for given client
     *
     * @param int $client_id
     *            is the id of the client for which we want
     *            to get the disctinct hanlders
     * @return array[string] is the list of distinct handlers
     */
    public function get_disctinct_handler($client_id) {
        $this->load->model('users_model');
        $handlers = array();
        $this->db->select('userid');
        $this->db->distinct();
        $this->db->where(array('client_id' => $client_id
        ));
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $obj_users = new users_model();
            $get_records = $obj_users->get_where(
                    array('user_id' => $row->userid
                    ));
            $handlers[] = $get_records[0];
        }
        return $handlers;
    }

    /**
     * retusn names of disntict kinds for given client
     *
     * @param int $client_id
     *            is the id of the client for which we want
     *            to get the disctinct kinds
     * @return array[string] is the list of distinct kinds
     */
    public function get_disctinct_kinds($client_id) {
        $kinds = array();
        $this->db->select('kind');
        $this->db->distinct();
        $this->db->where(array('client_id' => $client_id));
        $this->db->where(array('is_delete' => 0));
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $kinds[] = $row->kind;
        }
        return $kinds;
    }

    public function getDisctinctschadenummer($client_id) {
        $this->db->select('schadenummer');
        $this->db->distinct();
        $this->db->where(array('client_id' => $client_id));
        $this->db->where(array('is_delete' => 0));
        return $this->db->get($this->table_name)->result();
    }

    public function getDisctinctHandler($client_id) {
        $this->db->select('handler');
        $this->db->distinct();
        $this->db->where(array('client_id' => $client_id));
        return $this->db->get($this->table_name)->result();
    }

    /**
     * returns object of policy_holder_model that represents
     * policy holder associated with burrent claim
     */
    function get_policy_holder() {
        $this->load->model('policy_holder_model');

        $policy_hoders = $this->policy_holder_model->get_where(
                array('policy_holder_id' => $this->policy_holder_id
                ));
        if ((!is_array($policy_hoders) ) && count($policy_hoders) == 0) {
            echo "No policy holder associated with this claim";
            exit();
        }
        $policy_hoder = $policy_hoders[0];
        return $policy_hoder;
    }

    /**
     * returns an array of objects of forms_answers_model
     * that are related to the given claim.
     *
     * If there are no answers then an empty array is returned.
     */
    function get_answers() {
        $this->load->model('forms_answers_model');
        $answers = $this->forms_answers_model->get_where(
                array('claim_id' => $this->claim_id
                ));
        if (count($answers) == 0) {
            return array();
        }
        return $answers;
    }

    function get_answers_by_cat($cat_id, $claim_id) {
        $this->load->model('forms_answers_model');
        $answers = $this->forms_answers_model->get_where(
                array('claim_id' => $claim_id, 'cat_id' => $cat_id
                ));
        if (count($answers) == 0) {
            return array();
        }
        return $answers;
    }

    /**
     * return form number composed of year and claim id
     *
     * @return string form number composed of year and form number
     */
    public function get_form_number() {
        // get the year from import date
        $year = date('Y', strtotime($this->import_date));

        // concat year with
        $str = $year . str_pad($this->claim_id, 5, '0', STR_PAD_LEFT);

        return $str;
    }

    public function getFormNumberByClaim($claim_id) {
        $rec = $this->get_where(array('claim_id' => $claim_id
                ));
        // get the year from import date
        $year = date('Y', strtotime($rec[0]->import_date));

        // concat year with
        $str = $year . str_pad($claim_id, 5, '0', STR_PAD_LEFT);

        return $str;
    }

    /**
     * returns object of form that is associated with the given claim
     *
     * @return forms_model is the form associated with the claim
     */
    public function get_form_object() {
        $form_id = $this->form_id;
        $this->load->model('forms_model');
        $forms_model = new forms_model();
        $forms = $forms_model->get_where(
                array('form_id' => $form_id
                ));
        return $forms[0];
    }

    function getFormImportBetweenDates($client_id, $from_date, $to_date) {
        if (isset($to_date) && $to_date != '' && $to_date != '1970-01-01') {
            $where = "client_id=" . $client_id . " and  `import_date` BETWEEN '" .
                    $from_date . "' AND '" . $to_date . "'";
        } else {

            $where = "client_id=" . $client_id . " and  `import_date` BETWEEN '" .
                    $from_date . "' AND '" .
                    get_current_time()->get_date_for_db() . "'";
        }

        $this->db->select('count(*) as total');
        $this->db->where($where, NULL, false);
        $res = $this->db->get($this->table_name);
        $res1 = $res->result();

        return $res1[0]->total;
    }

    function getFormImportInMonth($client_id, $month, $year) {
        $where = "client_id=" . $client_id . " and  MONTH( import_date ) = " .
                $month . " AND YEAR( import_date ) =" . $year . " And is_delete =0 AND status!='Nieuw'";
        $this->db->select('count(*) as total');
        $this->db->where($where, NULL, false);
        $res = $this->db->get($this->table_name);
        $res1 = $res->result();

        return $res1[0]->total;
    }

    function getlastFormImportStatistic($client_id, $mail_type) {
        $current_date = get_current_time()->get_date_for_db();
        if ($mail_type == 'Daily') {
            $previous_date = date('Y-m-d', strtotime('-1 day', strtotime($current_date)));
            $where = "client_id=" . $client_id . " AND DAY( import_date ) = " . date('d', strtotime($previous_date)) . "   AND  MONTH( import_date ) = " .
                    date('m', strtotime($previous_date)) . " AND YEAR( import_date ) =" . date('Y', strtotime($previous_date)) . " And is_delete =0 AND status!='Nieuw'";
        } elseif ($mail_type == 'Weekly') {
            $prev_monday_t = time() - (date('N') + 6) * 86400;
            $prev_sunday_t = time() - date('N') * 86400;
            $start_date = date('Y-m-d', $prev_monday_t);
            $end_date = date('Y-m-d', $prev_sunday_t);
            $where = "client_id=" . $client_id . " AND (import_date BETWEEN '" . $start_date . "' AND '" . $end_date . "') And is_delete =0 AND status!='Nieuw'";
        } else {
            $previous_month = date('Y-m-d', strtotime('-1 months', strtotime($current_date)));
            $where = "client_id=" . $client_id . " AND  MONTH( import_date ) = " .
                    date('m', strtotime($previous_month)) . " AND YEAR( import_date ) =" . date('Y', strtotime($previous_month)) . " And is_delete =0 AND status!='Nieuw'";
        }
        $this->db->select('count(*) as total');
        $this->db->where($where, NULL, false);
        $res = $this->db->get($this->table_name);
        $res1 = $res->result();
        return $res1[0]->total;
    }

    function getFormListInMonth($client_id, $month, $year) {
        $where = "c.client_id=" . $client_id . " and  MONTH( c.import_date ) = " .
                $month . " AND YEAR( c.import_date ) =" . $year;
        $this->db->select("c.claim_id,c.claim_sequence_number,c.import_date,c.status,CONCAT( p.first_name,' ', p.last_name ) AS policy_holder_name,f.form_name", false);
        $this->db->from('ins_claims as c');
        $this->db->join('ins_policy_holder  as p', ' p.policy_holder_id =c.policy_holder_id', 'LEFT');
        $this->db->join('ins_forms as f', ' f.form_id =c.form_id', 'LEFT');
        $this->db->where($where, NULL, false);
        $query = $this->db->get();
        $res1 = $query->result();

        return $res1;
    }

    public function getDisctinctspolicyholder($client_id) {
        $this->db->select("ins_claims.policy_holder_id, CONCAT(ins_policy_holder.first_name , ins_policy_holder.last_name ) AS policy_holder_name");
        $this->db->distinct();
        $this->db->from($this->table_name);
        $this->db->join('ins_policy_holder', 'ins_policy_holder.policy_holder_id=ins_claims.policy_holder_id');
        $this->db->where(array('ins_claims.client_id' => $client_id, 'is_delete' => 0));
        $this->db->order_by('policy_holder_name', 'ASC');
        return $this->db->get()->result();
    }

    public function generateNewClaimSequenceNumber($client_id) {
        $last_id = 0;
        $new_id = 0;
        $year = get_current_time()->year;
        $this->db->select('claim_sequence_number');
        $this->db->from($this->table_name);
        $this->db->where('client_id', $client_id);
        $this->db->order_by('claim_id', 'desc');
        $this->db->limit(1);
        $res = $this->db->get();
        $result = $res->result();

        if ($res->num_rows > 0) {
            $last_year = substr($result[0]->claim_sequence_number, 0, 4);
            $last_id = substr($result[0]->claim_sequence_number, 4);

            if ($year != $last_year) {
                $last_id = 0;
            }
        }

        $new_id = $year . str_pad($last_id + 1, 4, '0', STR_PAD_LEFT);

        return $new_id;
    }

    public function update_reminder_date($claim_id, $value) {
        if ($value == 'first_remind') {
            $array = array('first_remind_date' => date("Y-m-d H:i:s"));
        }
        if ($value == 'second_remind') {
            $array = array('second_remind_date' => date("Y-m-d H:i:s"));
        }
        $this->db->where('claim_id', $claim_id);
        $this->db->update($this->table_name, $array);
    }

    public function get_claims_archive() {
        $client = admin_get_client_id();
        $objects = array();
        $client_setting = $this->client_setting_model->get_where(
                array('client_id' => $client->client_id
                ));
        $Verwijdertermijn = $client_setting[0]->delete_term;
        $this->db->select('*');
        $this->db->where(array('client_id' => $client->client_id, 'status' => 'Afgerond', 'is_delete' => 0));
        $this->db->where("DATE_ADD(conform_date, INTERVAL $Verwijdertermijn DAY) < CURDATE()");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
        //  $check = $this->db->update($this->table_name, array('status' => 'Archief', 'archive_date' => date('Y-m-d', time())));
        // $afftectedRows = $this->db->affected_rows();
        // return $objects;
    }

    public function update_status_archive($claim_id) {
        $this->db->where(array('claim_id' => $claim_id));
        $check = $this->db->update($this->table_name, array('status' => 'Archief', 'archive_date' => date('Y-m-d', time())));
        $afftectedRows = $this->db->affected_rows();
        return $afftectedRows;
    }

}
