<?php

/**
 *
 * @todo add documentation/refactor
 * @author umang
 *        
 */
class setting_change_mail_model extends CI_Model
{

    public $table_name;

    public $super_admin_email;

    public $validation_rules;

    public function __construct ()
    {
        parent::__construct();
        
        $this->table_name = 'ins_super_admin';
        
        $this->validation_rules = array(
                array('field' => 'super_admin_email','label' => 'Mail',
                        'rules' => 'required|min_length[3]|valid_email'
                )
        );
    
    }

    /**
     * this will return the single reord according to data passed in filed and
     * value.
     *
     * @param type $field
     *            : which field is to be checked.
     * @param type $value
     *            : what is the value.
     */
    function selectSingleRecord ( $field, $value )
    {
        $value = $this->db->escape_str($value);
        
        $objects = new setting_change_mail_model();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                 $value . "'";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() == 1)
        {
            $result = $query->result();
            foreach ($result as $row)
            {
                $data = new setting_change_mail_model();
                $data->super_admin_email = $row->super_admin_email;
                $objects = $data;
            }
        }
        else
        {
            $objects = FALSE;
        }
        
        return $objects;
    
    }

    function update ( $id )
    {
        $query = $this->db->query(
                "update ins_super_admin set super_admin_email='" .
                         $this->db->escape_str($this->super_admin_email) .
                         "' where super_admin_id='" . $id . "'");
        return TRUE;
    
    }

}

?>