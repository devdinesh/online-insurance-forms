<?php

/**
 * This model represents invoices that
 * are generated in the system
 *
 * file at : application/models/invoice_model.php
 *
 * @author umang
 *        
 */
class invoice_model extends CI_Model
{

    public $table_name = 'ins_invoices';

    public $invoice_number;

    public $subscription_kind_id;

    public $invoice_amount;

    public $invoice_date;

    public $invoice_period_start;

    public $invoiece_period_end;

    public $invoice_subject;

    public $client_id;

    public $mail_sent;

    public $validation_rules;

    function __construct ()
    {
        parent::__construct();
        $this->load->model('clients_model');
        
        $this->validation_rules = array(
                array('field' => 'invoice_amount','label' => 'Invoice Amount',
                        'rules' => 'required'
                ),
                array('field' => 'invoice_date','label' => 'Invoice Date',
                        'rules' => 'required|date_validator'
                ),
                array('field' => 'invoice_period_start',
                        'label' => 'Invoice Period Start',
                        'rules' => 'required|date_validator'
                ),
                array('field' => 'invoiece_period_end',
                        'label' => 'Invoice Period End',
                        'rules' => 'required|date_validator'
                ),
                array('field' => 'invoice_subject','label' => 'Invoice Subject',
                        'rules' => 'required|min_length[2]|max_length[100]'
                )
        );
    
    }

    /**
     * Saves the item to the database (inserts record).
     *
     * @return int id of the record just inserted
     */
    function save ()
    {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->invoice_number = $id;
        $this->after_save();
        return $id;
    
    }

    /**
     * Updates the record in the database.
     *
     * @return void
     */
    function update ()
    {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['invoice_number']);
        $this->db->where('invoice_number', $this->invoice_number);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    
    }

   function update_mail_sent_date(){
       $this->before_save();
       $array = array('mail_sent'=>$this->mail_sent);
       unset($array['invoice_number']);
       $this->db->where('invoice_number', $this->invoice_number);
       $this->db->update($this->table_name, $array);
       $this->after_save();
   }
    /**
     * Deletes the record from the database.
     *
     * @return void
     */
    public function delete ()
    {
        $this->db->where('invoice_number', $this->invoice_number);
        $this->db->delete($this->table_name);
    
    }

    /**
     * Returns all te records for invoice_model model
     *
     * @return invoice_model[]
     */
    public function get_all ()
    {
        $objects = array();
        $this->db->order_by("invoice_number", "asc");
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row)
        {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where ( $where )
    {
        $this->load->database();
        $objects = array();
        $this->db->order_by("invoice_number", "asc");
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row)
        {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    
    }

    /**
     * Returns associative array for the answer details
     *
     * @return array()
     */
    public function to_associative_array ()
    {
        $arr = array();
        $arr['invoice_number'] = $this->invoice_number;
        $arr['subscription_kind_id'] = $this->subscription_kind_id;
        $arr['invoice_amount'] = $this->invoice_amount;
        $arr['invoice_date'] = $this->invoice_date;
        $arr['invoice_period_start'] = $this->invoice_period_start;
        $arr['invoiece_period_end'] = $this->invoiece_period_end;
        $arr['invoice_subject'] = $this->invoice_subject;
        $arr['client_id'] = $this->client_id;
        $arr['mail_sent'] = $this->mail_sent;
        return $arr;
    
    }

    /**
     * Constructs an object from the array
     *
     * @param
     *            array() array that represents properties of the object that we
     *            want to construct
     * @return forms_answers_details_model object constructed from the array
     */
    public function from_array ( $array )
    {
        $object = new invoice_model();
        $object->invoice_number = $array['invoice_number'];
        $object->subscription_kind_id = $array['subscription_kind_id'];
        $object->invoice_amount = $array['invoice_amount'];
        $object->invoice_date = $array['invoice_date'];
        $object->invoice_period_start = $array['invoice_period_start'];
        $object->invoiece_period_end = $array['invoiece_period_end'];
        $object->invoice_subject = $array['invoice_subject'];
        $object->client_id = $array['client_id'];
        $object->mail_sent = $array['mail_sent'];
        return $object;
    
    }

    /**
     * This method convers the raw object to the real object.
     *
     * @param Object $old
     *            raw object that came from the active record query
     * @return invoice_model is the object that we want
     */
    public function from_raw_objct ( $old )
    {
        $new = new invoice_model();
        $year = date("Y");
        $new->invoice_number = $year .
                 str_pad($old->invoice_number, 4, '0', STR_PAD_LEFT);
        /**
         * to pad 0 10
         *
         * $new->invoice_number = str_pad($old->invoice_number, 10, '0',
         * STR_PAD_LEFT);
         */
        $new->invoice_amount = $old->invoice_amount;
        $new->subscription_kind_id = $old->subscription_kind_id;
        $new->invoice_date = $old->invoice_date;
        $new->invoice_period_start = $old->invoice_period_start;
        $new->invoiece_period_end = $old->invoiece_period_end;
        $new->invoice_subject = $old->invoice_subject;
        $new->client_id = $old->client_id;
        $new->mail_sent = $old->mail_sent;
        $new->after_save();
        return $new;
    
    }

    /**
     * This is a life-cyle method of the current class.
     * This method will be invoked before executing any statements of save.
     * Put things like converting from user readable date to database date etc
     * in this function.
     */
    private function before_save ()
    {
        // change the date format of the fields
        $this->invoice_date = date("Y-m-d", strtotime($this->invoice_date));
        $this->invoice_period_start = date("Y-m-d", 
                strtotime($this->invoice_period_start));
        $this->invoiece_period_end = date("Y-m-d", 
                strtotime($this->invoiece_period_end));
    
    }

    /**
     * This is a life-cycle method and of current class.
     * This method will be invoked after executing save method of the calss.
     * Put things like converting from database date to user readable date etc
     * in this function.
     */
    private function after_save ()
    {
        // change the date formats
        $this->invoice_date = date("d-m-Y", strtotime($this->invoice_date));
        $this->invoice_period_start = date("d-m-Y", 
                strtotime($this->invoice_period_start));
        $this->invoiece_period_end = date("d-m-Y", 
                strtotime($this->invoiece_period_end));
    
    }

    /**
     * This method returns an object of client that is associated with
     * the invoice.
     *
     * @return clients_model client object associated with the invoice
     */
    public function get_client ()
    {
        //var_dump($this->client_id);
        // load model
        $this->load->model('clients_model');
        $clients_model = new clients_model();
        
        // get the client with given id
        $clients = $clients_model->get_where(
                array('client_id' => $this->client_id
                ));
        
        // if there was no client
        if (count($clients) == 0)
        {
            //echo "Invalid client id. Error in Client model->get_client";
            return @$clients[0];
        }
        else
        {
            // return the client if it was found
            return @$clients[0];
        }
    
    }
    
    // for filtering data using client name
    function get_filter_Data ( $client )
    {
        $objects = array();
        $this->db->select('*');
        if ($client !== 'nl')
        {
            $this->db->where("client_id", $client);
        }
        $res = $this->db->get($this->table_name);
        foreach ($res->result() as $row)
        {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    
    }

    /**
     * This method returns the validation rules
     * for the field name passed.
     *
     *
     * @param string $field_name
     *            is item for which we want to get the
     *            validation rules
     * @return array of validation rules
     */
    public function get_validation_rules_for ( $field_name )
    {
        $validation_rules = $this->validation_rules;
        
        foreach ($validation_rules as $rule)
        {
            if ($rule['field'] == $field_name)
            {
                return $rule;
            }
        }
    
    }

}
