<?php

/**
 * Super admin defines questions and answers.
 * Each question will have many possible answers.
 * This model represents the possbile answers that the
 * policy holder can select.
 *
 * admin/super admin can add / update / delete the
 * answer types.
 *
 * The actuals answers to this will be given by policy holder.
 * Primary key of this table will be foreign key to
 * policy holder's answer.
 *
 * @author umang
 *        
 */
class temp_forms_answers_model extends CI_Model {

    // name of table
    public $table_name = 'ins_temp_forms_answers';
    // fields
    public $answer_id;
    public $claim_id;
    public $question_id;
    public $answer_text;
    public $answer_type;
    public $cat_id;
    public $prev_question;
    // validation rules to be applied
    public $validation_rules;

    // logo upload path
    function __construct() {
        parent::__construct();

        $this->validation_rules = array();
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->answer_id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['answer_id']);
        $this->db->where('answer_id', $this->answer_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('answer_id', $this->answer_id);
        $this->db->delete($this->table_name);
    }
    public function delete_temp_data() {
       $this->load->model('temp_forms_answers_details_model');
       $claim_info=$this->get_where(array('claim_id'=>$this->claim_id));
       foreach($claim_info as $claims){
           $this->temp_forms_answers_details_model->user_answer_id=$claims->answer_id;
           $this->temp_forms_answers_details_model->delete_temp_data();
       }
        $this->db->where('claim_id', $this->claim_id);
        $this->db->delete($this->table_name);
    }
    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();

        if ($this->answer_id != '') {
            $arr['answer_id'] = $this->answer_id;
        }

        if ($this->question_id != '') {
            $arr['question_id'] = $this->question_id;
        }

        if ($this->claim_id != '') {
            $arr['claim_id'] = $this->claim_id;
        }

        if ($this->answer_text != '') {
            $arr['answer_text'] = $this->answer_text;
        }

        if ($this->answer_type != '') {
            $arr['answer_type'] = $this->answer_type;
        }

        if ($this->cat_id != '') {
            $arr['cat_id'] = $this->cat_id;
        }

        if ($this->prev_question != '') {
            $arr['prev_question'] = $this->prev_question;
        }

        return $arr;
    }

    public function from_array($array) {
        $object = new temp_forms_answers_model();
        $object->answer_id = $array['answer_id'];
        $object->question_id = $array['question_id'];
        $object->claim_id = $array['claim_id'];
        $object->answer_text = $array['answer_text'];
        $object->answer_type = $array['answer_type'];
        $object->cat_id = $array['cat_id'];
        $object->prev_question = $array['prev_question'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new temp_forms_answers_model();

        $new->answer_id = $old->answer_id;
        $new->question_id = $old->question_id;
        $new->claim_id = $old->claim_id;
        $new->answer_text = $old->answer_text;
        $new->answer_type = $old->answer_type;
        $new->cat_id = $old->cat_id;
        $new->prev_question = $old->prev_question;
        return $new;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

    /**
     * returns an array of forms_answers_details_model
     * related to this form (form = claim/damage)
     *
     * if no chinds are ther (in case of single line/multi line answer) then
     * an empty array is returned
     */
    function get_answer_choices() {
        $this->load->model('temp_forms_answers_details_model');
        $answers_details = $this->temp_forms_answers_details_model->get_where(
                array('user_answer_id' => $this->answer_id
                ));
        if (count($answers_details) == 0) {
            return array();
        }
        return $answers_details;
    }

    /**
     * returns question object associated with the
     * currunt answer
     */
    function get_question() {
        $this->load->model('forms_categories_question_model');
        $question = new forms_categories_question_model();
        $questions = $question->get_where(
                array('question_id' => $this->question_id
                ));
        return $questions[0];
    }

    function getLastRecordInserted($claim_id) {
        $query = $this->db->query('SELECT * FROM ins_temp_forms_answers WHERE claim_id =' . $claim_id . ' ORDER BY answer_id desc');
        return $query->result();
    }

}

?>
