<?php

class form_field_values_model extends CI_Model {

    // name of table
    public $table_name = 'ins_form_fields_values';
    // fields
    public $filed_value_id;
    public $name;
    public $name_at_form;
    public $filed_value;
    public $claim_id;
    public $old_field_value;
    public $is_change;
    // validation rules to be applied
    public $validation_rules;

    function __construct() {
        parent::__construct();

        $this->validation_rules = array();
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->filed_value_id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['filed_value_id']);
        $this->db->where('filed_value_id', $this->filed_value_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    function update_Claim_Values($name_at_form, $value, $claimid, $old_field_value, $is_change) {
        $arr['filed_value'] = $value;
        $arr['old_field_value'] = $old_field_value;
        $arr['is_change'] = $is_change;
        $this->db->where(array('claim_id' => $claimid, 'name_at_form' => $name_at_form));
        $this->db->update($this->table_name, $arr);
        return true;
    }

    public function delete() {
        $this->db->where('filed_value_id', $this->filed_value_id);
        $this->db->delete($this->table_name);
    }

    public function delete_claim_info($claim_id) {
        $this->db->where('claim_id', $claim_id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();

        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['filed_value_id'] = $this->filed_value_id;
        $arr['name'] = $this->name;
        $arr['name_at_form'] = $this->name_at_form;
        $arr['filed_value'] = $this->filed_value;
        $arr['claim_id'] = $this->claim_id;
        $arr['old_field_value'] = $this->old_field_value;
        $arr['is_change'] = $this->is_change;
        return $arr;
    }

    public function from_array($array) {
        $object = new form_field_values_model();
        $object->filed_value_id = $array['filed_value_id'];
        $object->name = $array['name'];
        $object->name_at_form = $array['name_at_form'];
        $object->filed_value = $array['filed_value'];
        $object->claim_id = $array['claim_id'];
        $object->old_field_value = $array['old_field_value'];
        $object->is_change = $array['is_change'];

        return $object;
    }

    public function from_raw_objct($old) {
        $new = new form_field_values_model();

        $new->filed_value_id = $old->filed_value_id;
        $new->name = $old->name;
        $new->name_at_form = $old->name_at_form;
        $new->filed_value = $old->filed_value;
        $new->claim_id = $old->claim_id;
        $new->old_field_value = $old->old_field_value;
        $new->is_change = $old->is_change;

        $new->after_save();
        return $new;
    }

    public function delete_fieldvalue_info($claim_id) {
        $this->db->where('claim_id', $claim_id);
        $this->db->delete($this->table_name);
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

}

?>