<?php

class ins_import_log_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

//AUTHOR : MEHUL
//THIS FUNCTION IS USED TO GET ALL THE IMPORT LOG FROM THE DATABASE. IN THSI FUNCTION WE DONT PASS ANY PARAMETERS. THIS FUNCTION RETURNS THE ARRAY OF ALL THE IMPORT LOG FROM THE DATABASE.
    function getall() {
        $this->db->select('*');
        $this->db->from('ins_import_log');
        $query = $this->db->get();
        return $query->result();
    }

//END FUNCTION
//AUTHOR : MEHUL
//THIS FUNCTION IS USED TO GET PARTICULAR IMPORT LOG FOR THE EDIT FORM. IN THIS FUNCTION WE PASS THE IMPORT LOG ID. IF THE IMPORT LOG ID IS BLANK OR ZERO THEN THIS FUNCTION RETURNS THE FALSE MESSAGE. OTHERWISE THIS FUNCTION RETURNS THE OBJECT WITH ALL THE DETAILS OF THE IMPORT LOG.
    function getbyid($import_log_id) {
        $query = $this->db->get_where('ins_import_log', array('import_log_id' => $import_log_id));
        $result = $query->row();
        return $result;
    }

//END FUNCTION
//AUTHOR : 
//THIS FUNCTION IS USED TO ADD NEW ROW IN THE IMPORT LOG TABLE. IN THIS FUNCTION WE PASS THE DATA OF THE IMPORT LOG. IF THE DATA IS BLANK THEN THIS FUNCTION RETURNS FALSE MESSAGE OTHERWISE IT INSERT ONE NEW ROW IN THE DATABASE AND RETURSN TRUE MESSAGE.
    function insert($data) {
        $this->db->insert('ins_import_log', $data);
        $import_log_id = $this->db->insert_id();
        return true;
    }

//END FUNCTION
//AUTHOR : 
//THIS FUNCTION IS USED TO UPDATE THE IMPORT LOG IN THE DATABASE. IN THIS FUNCTION WE PASS THE DATA AND IMPORT LOG ID. IF THE DATA OR IMPORT LOG ID IS BLANK THEN THIS FUNCTION RETURSN THE FALSE MESSAGE. IF THE DATA OR IMPORT LOG ID IS NOT BLANK THEN THIS FUNCTION UPDATES THE DATA IN THE DATABASE AND RETURNS TRUE MESSAGE.
    function update($data = '', $import_log_id = 0) {
        if ($data == '' || $import_log_id == 0) {
            return false;
        } else {
            $this->db->where('import_log_id', $import_log_id);
            $this->db->update('ins_import_log', $data);

            return true;
        }
    }

//END FUNCTION
//AUTHOR : 
//THIS FUNCTION IS USED TO DELETE THE IMPORT LOG FROM THE DATABASE. IN THIS FUNCTION WE PASS THE IMPORT LOG ID. IF THE IMPORT LOG ID IS BLANK THEN THIS FUNCTION RETURNS THE FALSE MESSAGE. IF THE IMPORT LOG ID IS NOT BLANK THEN IT DELETE THE ROW FROM THE DATABASE AND RETURNS THE TRUE MESSAGE.
    function delete($import_log_id = 0) {
        if ($import_log_id == 0) {
            return false;
        } else {
            $this->db->where('import_log_id', $import_log_id);
            $this->db->delete('ins_import_log');

            return true;
        }
    }

//END FUNCTION
    public function get_where_sort($where, $sort = 'desc', $limit = null) {
        $this->load->database();
        $objects = array();
        $this->db->select('*');
        $this->db->from('ins_import_log');
        $this->db->where($where);
        $this->db->order_by('import_log_id', $sort);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
      
        return $res->result();
    }

}

?>