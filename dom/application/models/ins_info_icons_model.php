<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class ins_info_icons_model extends CI_model {

    public $info_icon_id;
    public $info_icon_name;
    public $info_icon_text;
    private $table_name = 'ins_info_icons';

    function __construct() {
        parent::__construct();
    }

    function validationRules() {
        $validation_rules = array(
            array('field' => 'info_icon_name', 'label' => 'Info icons name',
                'rules' => 'required|trim'
            ),
            array('field' => 'info_icon_text', 'label' => 'Info icons text',
                'rules' => 'required|trim'
            )
        );
        return $validation_rules;
    }

    function convertObject($old) {
        $new = new ins_info_icons_model();
        $new->info_icon_id = $old->info_icon_id;
        $new->info_icon_name = $old->info_icon_name;
        $new->info_icon_text = $old->info_icon_text;
        return $new;
    }

    function toArray() {
        $arr = array();
        if ($this->info_icon_id != '')
            $arr['info_icon_id'] = $this->info_icon_id;

        if ($this->info_icon_name != '')
            $arr['info_icon_name'] = $this->info_icon_name;

        if ($this->info_icon_text != '')
            $arr['info_icon_text'] = $this->info_icon_text;

        return $arr;
    }

    function getWhere($where, $limit = null, $orderby = null, $ordertype = null) {
        $objects = array();
        $this->db->select(' * ');
        $this->db->from($this->table_name);
        $this->db->where($where);
        if (is_null($orderby)) {
            $orderby = 'info_icon_id';
        }
        if (is_null($ordertype)) {
            $ordertype = 'desc;';
        }
        $this->db->order_by($orderby, $ordertype);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->convertObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    function getAll($limit = null, $orderby = null, $ordertype = null) {
        $objects = array();
        $this->db->select(' * ');
        $this->db->from($this->table_name);
        if (is_null($orderby)) {
            $orderby = 'info_icon_id';
        }
        if (is_null($ordertype)) {
            $ordertype = 'desc';
        }
        $this->db->order_by($orderby, $ordertype);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->convertObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    function insertData() {
        $array = $this->toArray();
        $this->db->insert($this->table_name, $array);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function updateData() {
        $array = $this->toArray();
        unset($array['info_icon_id']);
        $this->db->where('info_icon_id', $this->info_icon_id);
        $this->db->update($this->table_name, $array);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function deleteData() {
        $this->db->where('info_icon_id', $this->info_icon_id);
        $this->db->delete($this->table_name);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>