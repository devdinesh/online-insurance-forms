<?php

/**
 *
 * @todo delete this model if not appropriate
 * @author umang
 *        
 */
class email_template extends CI_model
{
    /*
     * this is the default constructor
     */
    function __construct ()
    {
        parent::__construct();
    
    }
    
    /*
     * this will fetch a single record $field : which filed is to be checked in
     * database $value : what will be the filed value
     */
    public function select_single_record ( $filed, $value )
    {
        $query = $this->db->query(
                " select * from em_email where " . $this->db->escape_str($filed) .
                         "='" . $this->db->escape_str($value) . "'");
        
        if ($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return FALSE;
        }
    
    }
    
    /*
     * return all the record in the database
     */
    function get_all_records ()
    {
        $query = $this->db->query("select email_id, email_type FROM em_email");
        return $query->result();
    
    }
    
    /*
     * update the data into the database $id : which record is to be updated
     */
    function update_data ( $id, $attachement = '' )
    {
        if ($this->input->post('email_bcc_admin') == '')
        {
            $bcc = 0;
        }
        else
        {
            $bcc = 1;
        }
        $this->db->query(
                "update em_email set email_subject='" . $this->db->escape_str(
                        $this->input->post('email_subject')) . "',email_message='" . $this->db->escape_str(
                        $this->input->post('email_message')) .
                         "', email_bcc_admin='" . $bcc .
                         "', email_modified_date=now() where email_id=" .
                         $this->db->escape($id));
        if ($attachement != '')
        {
            $this->db->query(
                    "update em_email set email_attachment='" .
                             $this->db->escape_str($attachement) .
                             "' where email_id=" . $this->db->escape_str($id));
        }
        
        return TRUE;
    
    }

}

?>
