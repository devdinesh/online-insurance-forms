<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class form_group_forms_model extends CI_model {

    public $id;
    public $form_group_id;
    public $standard_from_id;
    private $table_name = 'ins_form_group_forms';

    function __construct() {
        parent::__construct();
    }

    function validationRules() {
        $validation_rules = array(array('field' => 'standard_from_id', 'label' => 'Form', 'rules' => 'required|trim'));
        return $validation_rules;
    }

    function convertObject($old) {
        $new = new form_group_forms_model();
        $new->id = $old->id;
        $new->form_group_id = $old->form_group_id;
        $new->standard_from_id = $old->standard_from_id;
        return $new;
    }

    function toArray() {
        $arr = array();
        if ($this->id != '')
            $arr['id'] = $this->id;

        if ($this->form_group_id != '')
            $arr['form_group_id'] = $this->form_group_id;

        if ($this->standard_from_id != '')
            $arr['standard_from_id'] = $this->standard_from_id;

        return $arr;
    }

    function getWhere($where, $limit = null, $orderby = null, $ordertype = null) {
        $objects = array();
        $this->db->select(' * ');
        $this->db->from($this->table_name);
        $this->db->where($where);
        if (is_null($orderby)) {
            $orderby = 'id';
        }
        if (is_null($ordertype)) {
            $ordertype = 'desc;';
        }
        $this->db->order_by($orderby, $ordertype);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->convertObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    function getAll($limit = null, $orderby = null, $ordertype = null) {
        $objects = array();
        $this->db->select(' * ');
        $this->db->from($this->table_name);
        if (is_null($orderby)) {
            $orderby = 'id';
        }
        if (is_null($ordertype)) {
            $ordertype = 'desc';
        }
        $this->db->order_by($orderby, $ordertype);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->convertObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    function insertData() {
        $array = $this->toArray();
        $this->db->insert($this->table_name, $array);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function updateData() {
        $array = $this->toArray();
        unset($array['id']);
        $this->db->where('id', $this->id);
        $this->db->update($this->table_name, $array);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function deleteData() {
        $this->db->where('id', $this->id);
        $this->db->delete($this->table_name);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>