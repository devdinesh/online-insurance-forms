<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class ins_departments_model extends CI_model {

    public $account_id;
    public $name;
    public $client_id;
  // public $validation_rules;
    private $table_name = 'ins_departments';

    function __construct() {
        parent::__construct();
    }

    function validationRules() {
        $validation_rules = array(array('field' => 'name', 'label' => 'Department Name',
                'rules' => 'required|trim'
                ));
        return $validation_rules;
    }

    function convertObject($old) {
        $new = new ins_departments_model();
        $new->account_id = $old->account_id;
        $new->name = $old->name;
        $new->client_id = $old->client_id;
        return $new;
    }

    function toArray() {
        $arr = array();
        if ($this->account_id != '')
            $arr['account_id'] = $this->account_id;

        if ($this->name != '')
            $arr['name'] = $this->name;

        if ($this->client_id != '')
            $arr['client_id'] = $this->client_id;

        return $arr;
    }

    function getWhere($where, $limit = null, $orderby = null, $ordertype = null) {
        $objects = array();
        $this->db->select(' * ');
        $this->db->from($this->table_name);
        $this->db->where($where);
        if (is_null($orderby)) {
            $orderby = 'account_id';
        }
        if (is_null($ordertype)) {
            $ordertype = 'desc;';
        }
        $this->db->order_by($orderby, $ordertype);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->convertObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    function getAll($limit = null, $orderby = null, $ordertype = null) {
        $objects = array();
        $this->db->select(' * ');
        $this->db->from($this->table_name);
        if (is_null($orderby)) {
            $orderby = 'account_id';
        }
        if (is_null($ordertype)) {
            $ordertype = 'desc';
        }
        $this->db->order_by($orderby, $ordertype);
        if ($limit != null) {
            $this->db->limit($limit);
        }
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->convertObject($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    function insertData() {
        $array = $this->toArray();
        $this->db->insert($this->table_name, $array);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function updateData() {
        $array = $this->toArray();
        unset($array['account_id']);
        $this->db->where('account_id', $this->account_id);
        $this->db->update($this->table_name, $array);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function deleteData() {
        $this->db->where('account_id', $this->account_id);
        $this->db->delete($this->table_name);
        $check = $this->db->affected_rows();
        if ($check > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}

?>