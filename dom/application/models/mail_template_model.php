<?php

/**
 * This class represents the mail templates of super admin
 * maps to table: admin_email_templates
 * 
 * @author umang
 *        
 */
class mail_template_model extends CI_Model {

    // name of table
    public $table_name;
    public $validation_rules;
    public $email_id;
    public $email_type;
    public $email_subject;
    public $email_message;
    public $email_attachment;
    public $email_format_info;
    public $email_bcc_admin;
    public $email_dynamic_fields;

    function __construct() {
        parent::__construct();

        // set the default value false for data member
        $this->email_bcc_admin = false;
        $this->table_name = 'ins_admin_email_templates';

        $this->validation_rules = array(
            array('field' => 'email_id', 'label' => 'ID',
                'rules' => 'required'
            ),
            array('field' => 'email_type', 'label' => 'Email Type',
                'rules' => 'required'
            ),
            array('field' => 'email_subject', 'label' => 'Email Subject',
                'rules' => 'required|trim'
            ),
            array('field' => 'email_message', 'label' => 'Email Message',
                'rules' => 'required|trim'
            ),
            array('field' => 'email_attachment',
                'label' => 'Email Attachement', 'rules' => ''
            ),
            array('field' => 'email_format_info',
                'label' => 'Email Format Info', 'rules' => ''
            ),
            array('field' => 'email_bcc_admin', 'label' => 'Email Bcc Admin',
                'rules' => 'required'
            ),
            array('field' => 'email_dynamic_fields', 'label' => 'Dynamic Email Fields',
                'rules' => 'trim'
            )
        );
    }

    function save() {
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        return $id;
    }

    function update() {
        $array = $this->to_associative_array();
        unset($array['email_id']);
        $this->db->where('email_id', $this->email_id);
        $this->db->update($this->table_name, $array);
        return TRUE;
    }

    public function delete() {
        $this->db->where('email_id', $this->email_id);
        $this->db->delete($this->table_name);
        return TRUE;
    }

    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['email_id'] = $this->email_id;
        $arr['email_type'] = $this->email_type;
        $arr['email_subject'] = $this->email_subject;
        $arr['email_message'] = $this->email_message;
        $arr['email_attachment'] = $this->email_attachment;
        $arr['email_format_info'] = $this->email_format_info;
        $arr['email_bcc_admin'] = $this->email_bcc_admin;
        $arr['email_dynamic_fields'] = $this->email_dynamic_fields;
        return $arr;
    }

    public function from_array($array) {
        $object = new mail_template_model();
        $object->email_id = $array['email_id'];
        $object->email_type = $array['email_type'];
        $object->email_subject = $array['email_subject'];
        $object->email_message = $array['email_message'];
        $object->email_attachment = $array['email_attachment'];
        $object->email_format_info = $array['email_format_info'];
        $object->email_bcc_admin = $array['email_bcc_admin'];
        $object->email_dynamic_fields = $array['email_dynamic_fields'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new mail_template_model();
        $new->email_id = $old->email_id;
        $new->email_type = $old->email_type;
        $new->email_subject = $old->email_subject;
        $new->email_message = $old->email_message;
        $new->email_attachment = $old->email_attachment;
        $new->email_format_info = $old->email_format_info;
        $new->email_bcc_admin = $old->email_bcc_admin;
        $new->email_dynamic_fields = $old->email_dynamic_fields;
        return $new;
    }

}

?>