<?php

/**
 * There are subscription kinds that the admin can maintain.
 *
 * This class represents table for maintaining subscription kinds.
 * Subscription kindss can also be called subscriptio type.
 * 
 * @author umang
 *        
 */
class admin_subscription_kinds_model extends CI_Model
{

    public $table_name;

    public $subscription_kinds_id;

    public $subscription_title;

    public $maximum_forms;

    public $subscription_fee;

    public $rate_per_extra_form;

    public $help_text;

    public $plan_image;

    public $status;

    public $validation_rules;

    private $original_image_path;

    public $image_path_validation;

    public function __construct ()
    {
        parent::__construct();
        
        $this->table_name = 'ins_subscription_kinds';
        
        $this->validation_rules = array(
                array('field' => 'subscription_title',
                        'label' => 'Subscription Title',
                        'rules' => 'required|min_length[3]|name_validator'
                ),
                array('field' => 'maximum_forms','label' => 'Maximum Forms',
                        'rules' => 'required|integer'
                ),
                array('field' => 'subscription_fee',
                        'label' => 'Subscription Fees',
                        'rules' => 'required|decimal'
                ),
                array('field' => 'rate_per_extra_form',
                        'label' => 'Rate Per Additional Form',
                        'rules' => 'required|decimal'
                ),
                array('field' => 'help_text','label' => 'Help Text',
                        'rules' => 'trim'
                ),
                array('field' => 'plan_image','label' => 'Image',
                        'rules' => 'trim'
                )
        );
        
        $this->image_path_validation = array();
        $this->image_path_validation['upload_path'] = "./assets/plan_images";
        $this->image_path_validation['overwrite'] = TRUE;
        $this->image_path_validation['remove_spaces'] = TRUE;
        $this->image_path_validation['allowed_types'] = "jpg|png|gif|bmp";
        $this->image_path_validation['max_size'] = 2000;
    
    }

    public function dataUpdateSave ()
    {
        $this->change_image_path_for_saving_in_database();
        $id = isset($this->subscription_kinds_id);
        
        if ($id)
        {
            
            $query = $this->db->query(
                    "update  " . $this->table_name . " set subscription_title='" .
                             $this->db->escape_str($this->subscription_title) .
                             "', maximum_forms='" .
                             $this->db->escape_str($this->maximum_forms) .
                             "', subscription_fee='" .
                             $this->db->escape_str($this->subscription_fee) .
                             "', rate_per_extra_form='" .
                             $this->db->escape_str($this->rate_per_extra_form) .
                             "', help_text='" .
                             $this->db->escape_str($this->help_text) .
                             "', plan_image='" . $this->plan_image .
                             "', status='" . $this->db->escape_str(
                                    $this->status) .
                             "'  where subscription_kinds_id=" .
                             $this->subscription_kinds_id);
        }
        else
        {
            $data = array(
                    'subscription_title' => $this->db->escape_str(
                            $this->subscription_title),
                    'maximum_forms' => $this->db->escape_str(
                            $this->maximum_forms),
                    'subscription_fee' => $this->db->escape_str(
                            $this->subscription_fee),
                    'rate_per_extra_form' => $this->db->escape_str(
                            $this->rate_per_extra_form),
                    'help_text' => $this->db->escape_str($this->help_text),
                    'plan_image' => $this->plan_image,'status' => $this->status
            );
            
            $this->db->insert($this->table_name, $data);
        }
        
        return TRUE;
    
    }

    /**
     * changes the image path to string null if there was no value in the
     * image path field.
     * this helps in saving to the database.
     * if there was some value, it puts it in single quotes
     */
    function change_image_path_for_saving_in_database ()
    {
        $this->original_image_path = $this->plan_image;
        if (isset($this->plan_image))
        {
            $this->plan_image = $this->plan_image;
        }
        else
        {
            $this->plan_image = null;
        }
    
    }

    function upload_file ( $form_member_name )
    {
        $this->load->library('upload', $this->image_path_validation);
        
        $upload_success = $this->upload->do_upload($form_member_name);
        return $upload_success;
    
    }

    public function get_all ()
    {
        $objects = array();
        $sql = "SELECT * FROM " . $this->table_name .
                 " order by subscription_kinds_id desc";
        $query = $this->db->query($sql);
        foreach ($query->result() as $row)
        {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    
    }

    public function from_raw_objct ( $old )
    {
        $new = new admin_subscription_kinds_model();
        
        $new->subscription_kinds_id = $old->subscription_kinds_id;
        $new->subscription_title = $old->subscription_title;
        $new->maximum_forms = $old->maximum_forms;
        $new->subscription_fee = $old->subscription_fee;
        $new->rate_per_extra_form = $old->rate_per_extra_form;
        $new->help_text = $old->help_text;
        $new->plan_image = $old->plan_image;
        $new->status = $old->status;
        return $new;
    
    }

    function selectSingleRecord ( $field, $value )
    {
        $value = $this->db->escape_str($value);
        
        $objects = array();
        $sql = "select * from  " . $this->table_name . " where " . $field . "='" .
                 $value . "' order by subscription_kinds_id desc";
        $query = $this->db->query($sql);
        
        if ($query->num_rows() == 1)
        {
            foreach ($query->result() as $row)
            {
                $objects[] = $this->from_raw_objct($row);
            }
        }
        else
        {
            $objects = FALSE;
        }
        
        return $objects;
    
    }

    function deleteData ( $id )
    {
        $res = $this->selectSingleRecord('subscription_kinds_id', $id);
        
        if (isset($res[0]->plan_image))
        {
            if (unlink('assets/plan_images/' . $res[0]->plan_image))
            {
                $return = TRUE;
            }
        }
        $query = $this->db->query(
                'delete from ' . $this->table_name .
                         ' where subscription_kinds_id=' . $id);
        return TRUE;
    
    }

    function isPlanSubscribed ( $id )
    {
        $status = FALSE;
        $today_date = get_current_time()->get_date_for_db();
        
        $query = $this->db->query(
                "SELECT sk.subscription_kinds_id FROM ins_subscription_kinds sk, ins_client_subscription cs WHERE cs.from_date<=  '" .
                         $today_date . "' AND cs.end_date >=  '" . $today_date .
                         "' AND sk.subscription_kinds_id = cs.subscription_kinds_id and sk.subscription_kinds_id=" .
                         $id);
        
        if ($query->num_rows() > 0)
        {
            $status = $query->num_rows();
        }
        else
        {
            $status = FALSE;
        }
        
        return $status;
    
    }

    function get_wher ( $where )
    {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row)
        {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    
    }

}

?>
