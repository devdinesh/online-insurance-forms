<?php

/**
 * For each policy holder there would be claim.
 * This model represents the details of that calim.
 *
 * This class represents the insurance form of a client.
 *
 * @author umang
 *        
 */
class claim_archive_model extends CI_Model {

    public $table_name = 'claim_archive';
    public $claim_id;
    public $claim_sequence_number;
    public $import_date;
    public $kind;
    public $handler;
    public $handler_email;
    public $schadenummer;
    public $user_name;
    public $form_id;
    public $mail_send_date;
    public $status;
    public $policy_holder_name;
    public $policy_holder_mail;
    public $client_id;
    public $conform_date;
    public $is_delete;
    public $first_remind_date;
    public $second_remind_date;
    public $archive_date;
    public $text_form;

    function __construct() {
        parent::__construct();
        $this->load->model('claims_files_model');
        $this->load->model('form_field_values_model');
        ini_set('memory_limit', '512M');
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['claim_id']);
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->claim_id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['claim_id']);
        $this->db->where('claim_id', $this->claim_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function from_raw_objct($old) {
        $new = new claim_archive_model();

        $new->claim_id = $old->claim_id;
        $new->claim_sequence_number = $old->claim_sequence_number;
        $new->import_date = $old->import_date;
        $new->kind = $old->kind;
        $new->handler = $old->handler;
        $new->handler_email = $old->handler_email;
        $new->schadenummer = $old->schadenummer;
        $new->user_name = $old->user_name;
        $new->form_id = $old->form_id;
        $new->mail_send_date = $old->mail_send_date;
        $new->status = $old->status;
        $new->policy_holder_name = $old->policy_holder_name;
        $new->policy_holder_mail = $old->policy_holder_mail;
        $new->client_id = $old->client_id;
        $new->conform_date = $old->conform_date;
        $new->is_delete = $old->is_delete;
        $new->first_remind_date = $old->first_remind_date;
        $new->second_remind_date = $old->second_remind_date;
        $new->archive_date = $old->archive_date;
        $new->text_form = $old->text_form;
        $new->after_save();
        return $new;
    }

    public function get_forms_count_where($where) {
        $this->load->database();
        $res = $this->db->where($where)->count_all_results($this->table_name);
        return $res;
    }

    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $this->db->order_by("claim_id", "desc");
        $this->db->from($this->table_name);
        $this->db->where($where);
        $this->db->limit(50);
        $res = $this->db->get();
        if (!empty($res)) {
            foreach ($res->result() as $row) {
                $obj = $this->from_raw_objct($row);
                $objects[] = $obj;
            }
            return $objects;
        } else {
            return FALSE;
        }
    }

    public function where($where) {
        $this->load->database();
        $objects = array();
        $this->db->select('*');
        $this->db->from($this->table_name);
        $this->db->order_by("claim_id", "desc");
        $this->db->where($where, NULL, FALSE);
        $res = $this->db->get();
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }

        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['claim_id'] = $this->claim_id;
        $arr['claim_sequence_number'] = $this->claim_sequence_number;
        $arr['import_date'] = $this->import_date;
        $arr['kind'] = $this->kind;
        $arr['handler'] = $this->handler;
        $arr['handler_email'] = $this->handler_email;
        $arr['schadenummer'] = $this->schadenummer;
        $arr['user_name'] = $this->user_name;
        $arr['form_id'] = $this->form_id;
        $arr['mail_send_date'] = $this->mail_send_date;
        $arr['status'] = $this->status;
        $arr['policy_holder_name'] = $this->policy_holder_name;
        $arr['policy_holder_mail'] = $this->policy_holder_mail;
        $arr['client_id'] = $this->client_id;
        $arr['conform_date'] = $this->conform_date;
        $arr['is_delete'] = $this->is_delete;
        $arr['first_remind_date'] = $this->first_remind_date;
        $arr['second_remind_date'] = $this->second_remind_date;
        $arr['archive_date'] = $this->archive_date;
        $arr['text_form'] = $this->text_form;

        return $arr;
    }

    public function from_array($array) {
        $object = new claim_archive_model();
        $object->claim_id = $array['claim_id'];
        $object->import_date = $array['import_date'];
        $object->claim_sequence_number = $array['claim_sequence_number'];
        $object->kind = $array['kind'];
        $object->handler = $array['handler'];
        $object->handler_email = $array['handler_email'];
        $object->schadenummer = $array['schadenummer'];
        $object->user_name = $array['user_name'];
        $object->form_id = $array['form_id'];
        $object->mail_send_date = $array['mail_send_date'];
        $object->status = $array['status'];
        $object->policy_holder_name = $array['policy_holder_name'];
        $object->policy_holder_mail = $array['policy_holder_mail'];
        $object->client_id = $array['client_id'];
        $object->conform_date = $array['conform_date'];
        $object->is_delete = $array['is_delete'];
        $object->first_remind_date = $array['first_remind_date'];
        $object->second_remind_date = $array['second_remind_date'];
        $object->archive_date = $array['archive_date'];
        $object->text_form = $array['text_form'];

        return $object;
    }

    private function before_save() {
        if ($this->mail_send_date) {
            $this->mail_send_date = date("Y-m-d", strtotime($this->mail_send_date));
        }
        if ($this->import_date) {
            $this->import_date = date("Y-m-d", strtotime($this->import_date));
        }
        if ($this->conform_date) {
            $this->conform_date = date("Y-m-d", strtotime($this->conform_date));
        }
    }

    private function after_save() {
        if ($this->mail_send_date) {
            $this->mail_send_date = date("d-m-Y", strtotime($this->mail_send_date));
        }
        if ($this->import_date) {
            $this->import_date = date("d-m-Y", strtotime($this->import_date));
        }
        if ($this->conform_date) {
            $this->conform_date = date("d-m-Y", strtotime($this->conform_date));
        }
    }

}
