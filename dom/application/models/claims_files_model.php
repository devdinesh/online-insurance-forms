<?php

class claims_files_model extends CI_Model {

    // name of table
    public $table_name = 'ins_claim_files';
    // fields
    public $file_id;
    public $file_name; // not the complete path to file in folder
    public $claim_id;

    function __construct() {
        parent::__construct();
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->file_id = $id;
        // $this->after_save ();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['file_id']);
        $this->db->where('file_id', $this->file_id);
        $this->db->update($this->table_name, $array);
        // $this->after_save ();
    }

    public function delete() {
        $res = $this->get_where(array('file_id'=>$this->file_id));
        if(count($res) > 0){
            if(file_exists('./assets/claim_files/' . $res[0]->file_name))
                unlink('./assets/claim_files/' . $res[0]->file_name);
        }

        $this->db->where('file_id', $this->file_id);
        $this->db->delete($this->table_name);
    }

    public function delete_claim_info($claim_id) {
        $this->db->where('claim_id', $claim_id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['file_id'] = $this->file_id;
        $arr['file_name'] = $this->file_name;
        $arr['claim_id'] = $this->claim_id;
        return $arr;
    }

    public function from_array($array) {
        $object = new claims_files_model();
        $object->file_id = $array['file_id'];
        $object->file_name = $array['file_name'];
        $object->email = $array['claim_id'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new claims_files_model();

        $new->file_id = $old->file_id;
        $new->file_name = $old->file_name;
        $new->email = $old->claim_id;

        // $new->after_save ();
        return $new;
    }

    private function before_save() {
        
    }

    private function after_save() {
        
    }

    /**
     * static method which when called 
     * returns attachments related to the given 
     * claim id
     * @param int $claim_id is the claim id 
     * @return multitype:claims_files_model is array of 
     * attachments
     */
    public static function get_claims_for_claim_id($claim_id) {
        $claims_files_model = new claims_files_model();
        $claims = $claims_files_model->get_where(array('claim_id' => $claim_id
                ));
        return $claims;
    }

    public static function get_upload_config() {
        $config['upload_path'] = './' . claims_files_model::_get_location();
        $config['allowed_types'] = '*';
        return $config;
    }

    private static function _get_location() {
        return 'assets/claim_files';
    }

    public function get_path_for_web() {
        return base_url() . claims_files_model::_get_location() . "/" . $this->file_name;
    }

}

?>
