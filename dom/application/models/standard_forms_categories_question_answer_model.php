<?php

class standard_forms_categories_question_answer_model extends CI_Model {

    // name of the table that is mapped with this
    public $table_name;
    //
    // fields
    public $answer_id; // primary key
    public $question_id;
    public $answer;
    public $skip_to_questions; // question no to go to if this answer is given
    public $skip_to_category; // category to go to if this answer is given

    public function __construct() {
        parent::__construct();
        $this->table_name = 'ins_standard_categories_answer';

        $this->validation_rules = array(
            array('field' => 'answer', 'label' => 'Answer',
                'rules' => 'required|trim|min_length[2]|max_length[100]'
            ),
            array('field' => 'questions', 'label' => 'questions',
                'rules' => ''
            ),
            array('field' => 'questions', 'label' => 'proceed to question',
                'rules' => 'is_natural|max_length[4]'
            )
        );
    }

    function save() {
        $array = $this->to_associative_array();
        unset($array['answer_id']);
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        return $id;
    }

    function update() {
        $array = $this->to_associative_array();
        unset($array['answer_id']);
        unset($array['skip_to_category']);
        $this->db->where('answer_id', $this->answer_id);
        $this->db->update($this->table_name, $array);
    }

    public function delete() {
        $this->db->where('answer_id', $this->answer_id);
        $this->db->delete($this->table_name);
        return TRUE;
    }
    public function delete_all_answer() {
        $this->db->where('question_id', $this->question_id);
        $this->db->delete($this->table_name);
        return true;
    }
    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['answer_id'] = $this->answer_id;
        $arr['question_id'] = $this->question_id;
        $arr['answer'] = $this->answer;
        $arr['skip_to_questions'] = $this->skip_to_questions;
        $arr['skip_to_category'] = $this->skip_to_category;
        return $arr;
    }

    public function from_array($array) {
        $object = new standard_forms_categories_question_answer_model();
        $object->answer_id = $array['answer_id'];
        $object->question_id = $array['question_id'];
        $object->answer = $array['answer'];
        $object->skip_to_questions = $array['skip_to_questions'];
        $object->skip_to_category = $array['skip_to_category'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new standard_forms_categories_question_answer_model();

        $new->answer_id = $old->answer_id;
        $new->question_id = $old->question_id;
        $new->answer = $old->answer;
        $new->skip_to_questions = $old->skip_to_questions;
        if (isset($old->skip_to_category)) {
            $new->skip_to_category = $old->skip_to_category;
        }
        return $new;
    }

    private function before_save() {
        if (!$this->skip_to_category) {
            unset($this->skip_to_category);
        }
    }

    private function after_save() {
        
    }

    public function get_question() {
        $this->load->model('standard_forms_categories_question_model');
        $questions = new standard_forms_categories_question_model();
        $questions = $questions->get_where(
                array('question_id' => $this->question_id
                ));
        $question = $questions[0];
        return $question;
    }

}

?>
