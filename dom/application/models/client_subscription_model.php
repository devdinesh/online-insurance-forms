<?php

/**
 * This model represents the details about which
 * client has taken which subscription.
 *
 * @author umang
 *        
 */
class client_subscription_model extends CI_Model {

    // name of table
    public $table_name = 'client_subscription';
    // validation rules to be applied
    public $validation_rules;
    // fields of the database
    public $client_subscription_id;
    public $client_id;
    public $subscription_kinds_id;
    public $from_date;
    public $monthly_fee;
    public $end_date;
    // form_validations
    public $form_validations;

    function __construct() {
        parent::__construct();
        $this->table_name = 'client_subscription';
        $this->load->model('clients_model');
        $this->load->model('admin_subscription_kinds_model');
        $this->form_validations = array(
            array('field' => 'from_date', 'label' => 'From Date',
                'rules' => 'date_validator|required'
            ),
            array('field' => 'end_date', 'label' => 'End Date',
                'rules' => 'date_validator'
            )
        );
    }

    function save() {
        $this->before_save();
        $array = $this->to_associative_array();
        $this->db->insert($this->table_name, $array);
        $id = $this->db->insert_id();
        $this->id = $id;
        $this->after_save();
        return $id;
    }

    function update() {
        $this->before_save();
        $array = $this->to_associative_array();
        unset($array['client_subscription_id']);
        $this->db->where('client_subscription_id', $this->client_subscription_id);
        $this->db->update($this->table_name, $array);
        $this->after_save();
    }

    public function delete() {
        $this->db->where('client_subscription_id', $this->client_subscription_id);
        $this->db->delete($this->table_name);
    }

    public function get_all() {
        $objects = array();
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row) {
            $objects[] = $this->from_raw_objct($row);
        }
        return $objects;
    }

    /**
     * takes an active record query and returns the objects
     *
     * @param type $query            
     */
    public function get_where($where) {
        $this->load->database();
        $objects = array();
        $res = $this->db->get_where($this->table_name, $where);
        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    public function to_associative_array() {
        $arr = array();
        $arr['client_subscription_id'] = $this->client_subscription_id;
        $arr['client_id'] = $this->client_id;
        $arr['subscription_kinds_id'] = $this->subscription_kinds_id;
        $arr['from_date'] = $this->from_date;
        $arr['end_date'] = $this->end_date;
        $arr['monthly_fee'] = $this->monthly_fee;
        return $arr;
    }

    public function from_array($array) {
        $object = new client_subscription_model();
        $object->client_subscription_id = $array['client_subscription_id'];
        $object->client_id = $array['client_id'];
        $object->subscription_kinds_id = $array['subscription_kinds_id'];
        $object->from_date = $array['from_date'];
        $object->end_date = $array['end_date'];
        $object->monthly_fee = $array['monthly_fee'];
        return $object;
    }

    public function from_raw_objct($old) {
        $new = new client_subscription_model();
        $new->client_subscription_id = $old->client_subscription_id;
        $new->client_id = $old->client_id;
        $new->subscription_kinds_id = $old->subscription_kinds_id;
        $new->from_date = $old->from_date;
        $new->end_date = $old->end_date;
        $new->monthly_fee = $old->monthly_fee;
        $new->after_save();
        return $new;
    }

    function get_client() {
        $client = new clients_model();
        $client = $client->get_where(
                array('client_id' => $this->client_id
                ));
        return $client[0];
    }

    private function before_save() {
        if (isset($this->end_date) && $this->end_date != '') {
            $this->end_date = date("Y-m-d", strtotime($this->end_date));
        } else {
            $this->end_date = null;
        }

        $this->from_date = date("Y-m-d", strtotime($this->from_date));
    }

    private function after_save() {
        if (isset($this->end_date) && $this->end_date != '') {
            $this->end_date = date("d-m-Y", strtotime($this->end_date));
        } else {
            $this->end_date = null;
        }
        $this->from_date = date("d-m-Y", strtotime($this->from_date));
    }

    /**
     * This method tells whether the client is having subscription
     * without end date or not.
     *
     * @param int $client_id
     *            is the id of the client for whom we want to
     *            checke whether we have got the subscriptions without end date
     *            or not.
     * @return boolean true if the client is having the subscription without
     *         end date. false if the client is not having the subscription
     *         withoug
     *         end date entered.
     */
    public static function has_subscription_without_end_date($client_id) {
        $client_subscription_model = new client_subscription_model();
        $clients = $clients = $client_subscription_model->get_where(
                array('client_id' => $client_id, 'end_date' => NULL
                ));
        $count = count($clients);

        $has_subscriptios_withou_end_date = false;

        if ($count == 0) {
            $has_subscriptios_withou_end_date = FALSE;
        } else {
            $has_subscriptios_withou_end_date = TRUE;
        }
        return $has_subscriptios_withou_end_date;
    }

    function getLatestSubscription($client_id) {
        $result = $this->db->query(
                'SELECT *  FROM `ins_client_subscription` WHERE `client_id` = ' .
                $client_id . '  AND `from_date` <= \'' .
                get_current_time()->get_date_for_db() .
                '\' AND `end_date` IS NULL ');
        $res = $result->result();
        return $res;
    }

    public function get_disctinct_client_name() {
        $objects = array();
        $this->db->select('client_id');
        $this->db->distinct();
        $this->db->where(array());
        $query = $this->db->get($this->table_name);

        foreach ($query->result() as $row) {
            $this->load->model('clients_model');
            $clients_model = new clients_model();
            $clients = $clients_model->get_where(
                    array('client_id' => $row->client_id
                    ));
            $temp = array();
            $temp['id'] = $row->client_id;
            $temp['name'] = $clients[0]->client_name;
            $objects[] = $temp;
        }
        return $objects;
    }

    public function get_disctinct_kind_of_subscription() {
        $objects = array();
        $this->db->select('subscription_kinds_id');
        $this->db->distinct();
        $this->db->where(array());
        $query = $this->db->get($this->table_name);

        foreach ($query->result() as $row) {
            $this->load->model('admin_subscription_kinds_model');
            $subscription_type = $this->admin_subscription_kinds_model->selectSingleRecord(
                    'subscription_kinds_id', $row->subscription_kinds_id);
            $subscription_type = $subscription_type[0]->subscription_title;
            $temp = array();
            $temp['id'] = $row->subscription_kinds_id;
            $temp['type'] = $subscription_type;
            $objects[] = $temp;
        }
        return $objects;
    }

    public function get_filter_Data($status, $client_id, $subscription_type) {
        $objects = array();
        $this->db->select('*');


        if ($status == 'true') {
            $this->db->where("end_date", Null);
        } else {
            $where = ' `end_date` IS NOT NULL';
            $this->db->where($where, null, false);
        }

        if ($client_id != 'nl') {
            $this->db->where('client_id', $client_id);
        }

        if ($subscription_type != 'nl') {
            $this->db->where('subscription_kinds_id', $subscription_type);
        }
        // $this->db->order_by("claim_id", "desc");
        $res = $this->db->get($this->table_name);

        //echo $this->db->last_query();

        foreach ($res->result() as $row) {
            $obj = $this->from_raw_objct($row);
            $objects[] = $obj;
        }
        return $objects;
    }

    function getuserSubscription($client_id) {
        $result = $this->db->query(
                'SELECT *  FROM `ins_client_subscription` WHERE `client_id` = ' .
                $client_id . '  AND `from_date` <= \'' .
                get_current_time()->get_date_for_db() .
                '\' AND (`end_date` IS NULL OR `end_date` >= \'' .
                get_current_time()->get_date_for_db() .
                '\')');
        $res = $result->result();
        return $res;
    }

}

?>
