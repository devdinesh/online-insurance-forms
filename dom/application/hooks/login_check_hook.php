<?php

/**
 * This page checkes for login.
 * This is called by hook.
 * This is written assuming some convensions.
 * The dir name after base path is same as user type
 * eg type is super_admin ans the path also starts with super_admin
 *
 * also assuming that the url of authenticator is authenticate for all the page
 */
class login_check_hook extends CI_Controller {

    /**
     *
     * @var String[] stores the authentication urls for each type of URL
     */
    var $authentication_pages;

    /**
     * Default constructor initializes the authentication URLs
     */
    public function __construct() {
        parent::__construct();

        // map each user type/directory to his authenticate url
        // see assumptions in the title
        $this->authentication_pages = array();

        $this->authentication_pages['admin'] = 'admin/authenticate';
        $this->authentication_pages['super_admin'] = 'super_admin/authenticate';

        // the call to following function stops the user from clicking on back
        // buton.
        // it prevents the pages from being cached.
        $this->__clear_cache();
    }

    /**
     * Stops the pages from being cached at the brwoser side
     */
    private function __clear_cache() {
        $this->output->set_header(
                "Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
    }

    /**
     * Checks login.
     * Main method called by hook.
     * If the login is not done then redirects the user to logi page
     *
     * @return void
     */
    function checkLogin() {
        $ci = &get_instance();
        /**
         * If the called controller(class) is authenticate then do not check for
         * authentication.
         * if you remove this if it would result in redirect loop
         */
        if (!$this->_is_authentication_controller()) {
            $this->_authorize_for_url_with();
        }
    }

    /**
     * Returns the first directory name after base path
     *
     * @return String first directory name
     */
    function _get_root_directory_requested() {
        $dir_all = explode("/", uri_string());
        $dir = $dir_all[0];
        if ($dir == '') {
            $dir = false;
        }
        return $dir;
    }

    /**
     * redirectos admin to admin login and super admin
     * to super admin login as per the directory requested.
     *
     * This redirect happens even if the user is logged in
     * as another user
     *
     * @param String $name
     *            is the top level dire name (eg admin)
     */
    function _authorize_for_url_with() {

        $ci = &get_instance();
        $dir = $this->_get_root_directory_requested();

        if ($dir == 'super_admin') {
            if ($ci->session->userdata('super_admin') == false) {
                redirect($this->authentication_pages['super_admin']);
            }
        }
        if ($dir == 'admin') {
            if (!$ci->session->userdata('admin')) {
                redirect($this->authentication_pages['admin']);
            }
        }
    }

    /**
     * This method is called when the user is not logged in.
     * if the user requested the / then would redirect to employye login.
     * if user has requested for any page but could not be authenticated
     * because of some reason then would be redirected to appropriate page
     * (eg if the user requested for admin/user/admin but he is not
     * authenticated
     * then he would be redirected to admin/ and from there he would be
     * redirected
     * to the login page)
     */
    function _redirect_to_appropriate_login() {
        /*
         * $dir = $this->_get_root_directory_requested(); if ($dir) { if ($dir
         * == 'super_admin' || $dir == 'admin' || $dir == 'user') { redirect(
         * $this->authentication_pages[$dir] . '?redirect_to=' . uri_string(),
         * 'refresh'); } } else {
         * redirect($this->authentication_pages['super_admin'], 'refresh'); }
         */
    }

    /**
     * Checks whether the User is logged in or not.
     *
     * @return Boolean true if user is logged in
     */
    function _is_logged_in() {
        $ci = & get_instance();
        ;
        return $ci->session->userdata('logged_in');
    }

    /**
     * Return true if requested controller is any of the authentication
     * controller
     *
     * @return Boolean true if requested controller is the controller which
     *         needs to
     *         be allowed without login
     */
    function _is_authentication_controller() {
        $val = $this->router->fetch_class() == 'authenticate' ||
                $this->router->fetch_class() == 'welcome' ||
                $this->router->fetch_class() == 'test' ||
                $this->router->fetch_class() == 'contact_me' ||
                $this->router->fetch_class() == 'feedback' ||
                $this->router->fetch_class() == 'about_us'; // allow the tests to run
        return $val;
    }

}

?>