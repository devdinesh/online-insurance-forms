<?php

/**
 * This class is used for hook that 
 * checks if the claim that the user is trying to access
 * is valid and should be accesible to the given policy holder
 * @author umang
 *
 */
class claims_check_hook extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('lgin');
        $this->load->helper('authorization');
        $this->load->model('claims_model');
    }

    /**
     * method of hook is applied on the answer model and 
     * redirects the user to its home page if the user is trying to access
     * inappropriate claim
     */
    function check_claims()
    {
        $ci = & get_instance();
        
        // we check for valid claim id only if the answer controller is called
        if ($ci->router->fetch_class() == 'answer')
        {
            
            // get the policy holder id
            $get_policy_holder_id = user_get_policy_holder();
            $policy_holder_id = $get_policy_holder_id->policy_holder_id;
            
            // get the current url
            $url = current_url();
            $url_array = explode('/', $url); // explde with / so that we can get the claim id from URL
            $claim_id = $url_array[7]; // get the claim id
            $result = claim_id_belongs_to_policy_holder($claim_id, 
                    $policy_holder_id);
            
            // if the claim id was not allowed for the user
            // the set flash error and redirect to appropriate page
            if ($result == false)
            {
                $this->session->set_flashdata('error', 
                        "The form you were looking for was not found");
                redirect(base_url() . 'user/list_forms', 'refresh');
            }
        }
    }
} 