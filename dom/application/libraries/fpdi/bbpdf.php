<?php

$ci = &get_instance();

include_once(APPPATH . 'libraries/fpdf/FPDF.php');
include_once(APPPATH . 'libraries/fpdi/fpdi.php');

class BBPDF extends FPDI {

    private $_template = '';

    public function __construct($template, $template_back) {
        parent::__construct();

        $this->_template = $template;
        $this->_template_back = $template_back;
    }

    function AddPage($orientation = '', $size = '') {
        parent::AddPage($orientation, $size);
        if ($this->page > 1) {
            $this->setSourceFile($this->_template_back);
            $template_back = $this->ImportPage(1);
            $this->useTemplate($template_back);
        } else if (!empty($this->_template)) {
            $this->setSourceFile($this->_template);
            $template = $this->ImportPage(1);
            $this->useTemplate($template);
        }
    }

}