<?php

/**
 * This class will decide the language choose by the admin or user.
 */
class language_change {

    /**
     * This function checks the session and get the language according to the session set
     * @return string : language name
     */
    function getLanguage() {

        //by default set to dutch language
        $language_id = 1;


        /*   //if admin session is running the set admin language
          if (get_instance()->session->userdata('admin_details') != '') {
          $session_data = get_instance()->session->userdata('admin_details');
          if (isset($session_data['language']) && $session_data['language'] != "")
          $language_id = $session_data['language'];
          else
          $language_id = "";
          }

          //if user session is running then set user language
          if (get_instance()->session->userdata('session_user_details') != '') {
          $session_data = get_instance()->session->userdata('session_user_details');
          if (isset($session_data['language']) && $session_data['language'] != "")
          $language_id = $session_data['language'];
          else
          $language_id = "";
          } */

        // 0 = Dutch
        // 1 = English
        if ($language_id == 1) {
            $lang = "english";
        } else {
            $lang = "dutch";
        }

        return $lang;
    }

    /**
     * This function will set the language and get the content accoridng to language set.
     * @param type $file : from which the content should be get.
     * @return boolean
     */
    public static function setLanguage() {
        $lang = new language_change();
        $ci = & get_instance();
        $ci->language = $lang->getLanguage();
        $ci->config->set_item('language', $ci->language);
        $files = $lang->getUsersFiles();
        foreach ($files as $file) {
            $ci->lang->load($file, $ci->language);
        }
    }

    function getUsersFiles() {
        $files = array();
        $files[] = 'superadmin';
        //$files[] = 'user';
        return $files;
    }

}

?>