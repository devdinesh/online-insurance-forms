<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * | ------------------------------------------------------------------------- |
 * URI ROUTING |
 * ------------------------------------------------------------------------- |
 * This file lets you re-map URI requests to specific controller functions. | |
 * Typically there is a one-to-one relationship between a URL string | and its
 * corresponding controller class/method. The segments in a | URL normally
 * follow this pattern: | |	example.com/class/method/id/ | | In some instances,
 * however, you may want to remap this relationship | so that a different
 * class/function is called than the one | corresponding to the URL. | | Please
 * see the user guide for complete details: |
 * |	http://codeigniter.com/user_guide/general/routing.html | |
 * ------------------------------------------------------------------------- |
 * RESERVED ROUTES |
 * ------------------------------------------------------------------------- | |
 * There area two reserved routes: | |	$route['default_controller'] = 'welcome';
 * | | This route indicates which controller class should be loaded if the | URI
 * contains no data. In the above example, the "welcome" class | would be
 * loaded. | |	$route['404_override'] = 'errors/page_missing'; | | This route
 * will tell the Router what URI segments to use if those provided | in the URL
 * cannot be matched to a valid route. |
 */

$route['default_controller'] = "admin/authenticate/login";
$route['404_override'] = '';

require_once( BASEPATH . 'database/DB' . EXT );
$db = & DB();
//$query = $db->get('ins_texts');
//$result = $query->result();
//foreach ($result as $row) {
//    $route[$row->page_slug] = 'admin/authenticate/getPage/' . $row->page_slug;
//}

/*
 * ================== Super Admin : Login process ============================
 */
$route['super_admin'] = "super_admin/authenticate";
$route['super_admin/login'] = "super_admin/authenticate/checkLogin";
$route['super_admin/forgotpassword'] = "super_admin/authenticate/forgotPassword";
$route['super_admin/forgotpassword_listener'] = "super_admin/authenticate/forgotpassword_listener";
$route['super_admin/Reset/(:any)'] = "super_admin/authenticate/resetPassword/$1";
$route['super_admin/ResetPassword/(:any)'] = "super_admin/authenticate/resetPasswordListener/$1";

/*
 * ================== [User name] : [Main Menu] > [Sub Menu] > [Sub Menu] > ...
 * ===========================
 */

/* ================== Super Admin : Home  ============================ */
$route['super_admin/dashboard'] = "super_admin/dashboard/index";

/* ================== Super Admin : Clients ============================ */
$route['super_admin/clients'] = "super_admin/clients/index";
$route['super_admin/clients/edit/(:num)'] = "super_admin/clients/edit/$1";
$route['super_admin/subscriptions/add/(:num)'] = "super_admin/subscriptions/add/$1";
$route['super_admin/clients/update/(:num)'] = "super_admin/clients/editListener/$1";
$route['super_admin/clients/getJsonClients'] = "super_admin/clients/getJsonClients";

/* ================== Super Admin : Users ============================ */
$route['super_admin/users'] = "super_admin/users/index";
$route['super_admin/users/edit/(:num)'] = "super_admin/users/edit/$1";
$route['super_admin/users/update/(:num)'] = "super_admin/users/editListener/$1";
$route['super_admin/users/delete/(:num)'] = "super_admin/users/delete/$1";
$route['super_admin/users/getJson'] = "super_admin/users/getJson";
$route['super_admin/users/get_client/(:any)'] = "super_admin/users/getclient_info/$1";

/* ================== Super Admin : Froms ============================ */
$route['super_admin/forms'] = "super_admin/forms/index";
$route['super_admin/forms/add'] = "super_admin/forms/add";
$route['super_admin/forms/addListener'] = "super_admin/forms/addListener";
$route['super_admin/forms/edit/(:num)'] = "super_admin/forms/edit/$1";
$route['super_admin/forms/update/(:num)'] = "super_admin/forms/editListener/$1";
$route['super_admin/forms/delete/(:num)'] = "super_admin/forms/delete/$1";
$route['super_admin/forms/getJson'] = "super_admin/forms/getJson";
$route['super_admin/forms/copy/(:num)'] = "super_admin/forms/copyForm/$1";
$route['super_admin/forms/preview/(:num)/(:any)'] = "super_admin/forms/preview_form/$1/$2";
$route['super_admin/forms/preview/(:num)'] = "super_admin/forms/preview_form/$1"; // route
// for
// preview
// form
// link
// in
// the
// view
// all
// forms
// page
// of
// super
// admin

/*
 * ================== Super Admin : Forms [Categories]
 * ============================
 */
$route['super_admin/forms/categories/(:num)'] = "super_admin/forms_categories/index/$1";
$route['super_admin/forms/categories/getJson/(:num)'] = "super_admin/forms_categories/getJson/$1";
$route['super_admin/forms/categories/add/(:num)'] = "super_admin/forms_categories/add/$1";
$route['super_admin/forms/categories/addListener/(:num)'] = "super_admin/forms_categories/addListener/$1";
$route['super_admin/forms/categories/edit/(:num)'] = "super_admin/forms_categories/edit/$1";
$route['super_admin/forms/categories/update/(:num)'] = "super_admin/forms_categories/editListener/$1";
$route['super_admin/forms/categories/delete/(:num)/(:num)'] = "super_admin/forms_categories/delete/$1/$2";
$route['super_admin/forms/categories/sortable'] = "super_admin/forms_categories/sortable";

/*
 * ================== Super Admin : Forms [Categories][Questions]
 * ============================
 */
$route['super_admin/forms/categories/question/(:num)'] = "super_admin/forms_categories_question/index/$1";
$route['super_admin/forms/categories/question/getJson/(:num)'] = "super_admin/forms_categories_question/getJson/$1";
$route['super_admin/forms/categories/question/add/(:num)'] = "super_admin/forms_categories_question/add/$1";
$route['super_admin/forms/categories/question/andd_ans'] = "super_admin/forms_categories_question/add_answer";
$route['super_admin/forms/categories/question/addListener/(:num)'] = "super_admin/forms_categories_question/addListener/$1";
$route['super_admin/forms/categories/question/edit/(:num)'] = "super_admin/forms_categories_question/edit/$1";
$route['super_admin/forms/categories/question/update/(:num)'] = "super_admin/forms_categories_question/editListener/$1";
$route['super_admin/forms/categories/question/delete/(:num)/(:num)'] = "super_admin/forms_categories_question/deleteQuestion/$1/$2";
$route['super_admin/forms/categories/answers/delete/(:num)/(:any)'] = "super_admin/forms_categories_question/deleteAnswers/$1/$2";
$route['super_admin/categories_question/movedown/(:num)'] = "super_admin/forms_categories_question/move_sequence_down/$1";
$route['super_admin/categories_question/moveup/(:num)'] = "super_admin/forms_categories_question/move_sequence_up/$1";
$route['super_admin/categories_question/delete_answer/(:num)'] = "super_admin/forms_categories_question/deleteAnswer_check/$1";

$route['super_admin/form_pdf/(:num)'] = "super_admin/forms/create_pdf_for_forms/$1";

/*
 * ================== Super Admin : Admin > Mail Templates
 * ============================
 */
$route['super_admin/admin/mail'] = "super_admin/mail_templates/index";
$route['super_admin/admin/mail/edit/(:num)'] = "super_admin/mail_templates/edit/$1";
$route['super_admin/admin/mail/update/(:num)'] = "super_admin/mail_templates/editListener/$1";
$route['super_admin/admin/mail/getJson'] = "super_admin/mail_templates/getJson";
$route['super_admin/admin/mail/remove_attachment/(:num)'] = "super_admin/mail_templates/remove_attachment/$1";

/*
 * ================== Super Admin : Admin > Subscription Kinds
 * ============================
 */
$route['super_admin/admin/subscription'] = "super_admin/admin_subscription_kinds/index";
$route['super_admin/admin/subscription/getJson'] = "super_admin/admin_subscription_kinds/getJson";
$route['super_admin/admin/subscription/add'] = "super_admin/admin_subscription_kinds/add";
$route['super_admin/admin/subscription/addListener'] = "super_admin/admin_subscription_kinds/addListener";
$route['super_admin/admin/subscription/edit/(:num)'] = "super_admin/admin_subscription_kinds/edit/$1";
$route['super_admin/admin/subscription/update/(:num)'] = "super_admin/admin_subscription_kinds/editListener/$1";
$route['super_admin/admin/subscription/delete/(:num)'] = "super_admin/admin_subscription_kinds/deleteListener/$1";
$route['super_admin/admin/subscription/removeimage/(:num)'] = "super_admin/admin_subscription_kinds/removeImage/$1";

/*
 * ================== Super Admin : Admin > Setting ============================
 */
$route['super_admin/admin/setting'] = "super_admin/admin_setting/index";
$route['super_admin/admin/update_setting/(:num)'] = "super_admin/admin_setting/update_setting/$1";



/*
 * ================== Super Admin : Admin > Menus============================
 */
$route['super_admin/admin/menus'] = "super_admin/menus/index";
$route['super_admin/admin/menus/add'] = "super_admin/menus/add";
$route['super_admin/admin/menus/addListener'] = "super_admin/menus/addListener";
$route['super_admin/admin/menus/edit/(:num)'] = "super_admin/menus/edit/$1";
$route['super_admin/admin/menus/update/(:num)'] = "super_admin/menus/editListener/$1";
$route['super_admin/admin/menus/getJson'] = "super_admin/menus/getJson";
$route['super_admin/admin/menus/delete/(:num)'] = "super_admin/menus/deleteListener/$1";

/*
 * ================== Super Admin : Admin > Texts ============================
 */
$route['super_admin/admin/texts'] = "super_admin/admin_texts/index";
$route['super_admin/admin/texts/edit/(:num)'] = "super_admin/admin_texts/edit/$1";
$route['super_admin/admin/texts/update/(:num)'] = "super_admin/admin_texts/editListener/$1";
$route['super_admin/admin/texts/getJson'] = "super_admin/admin_texts/getJson";

/*
 * ================== Super Admin : Admin > Category > form > Question > answer
 * ===========
 */
$route['super_admin/answers/(:num)'] = "super_admin/forms_categories_question_answer/index/$1";
$route['super_admin/answers/get_json/(:num)'] = "super_admin/forms_categories_question_answer/get_json/$1";
$route['super_admin/answer/edit/(:num)'] = "super_admin/forms_categories_question_answer/edit/$1";
$route['super_admin/answer/edit_listener'] = "super_admin/forms_categories_question_answer/edit_listener";
$route['super_admin/answer/add/(:num)'] = "super_admin/forms_categories_question_answer/add/$1";
$route['super_admin/answer/add_listener'] = "super_admin/forms_categories_question_answer/add_listener";

/*
 * ================== Super Admin : Customer > subscription
 * ============================
 */
$route['super_admin/client_subscriptions/(:num)'] = "super_admin/subscriptions/client_subscription/$1";
$route['super_admin/get_subscription/(:any)'] = "super_admin/subscriptions/get_subscription/$1";
/*
 * ================== Super Admin : banner ============================
 */
$route['super_admin/admin/banner'] = "super_admin/banner/index";
$route['super_admin/admin/feature'] = "super_admin/feature/index";
$route['super_admin/admin/news'] = "super_admin/super_admin_news/index";

/* ============== super admin : admins for client ========== */
$route['super_admin/clients/admins/(:num)'] = "super_admin/client_admins/index/$1";
$route['super_admin/clients/admins/get_json/(:num)'] = "super_admin/client_admins/get_json/$1";
$route['super_admin/clients/admins/super_admin/clients/admins/edit/(:num)'] = "super_admin/client_admins/edit/$1";
$route['super_admin/clients/admins/super_admin/clients/admins/edit_listener'] = "super_admin/client_admins/edit_listener";
$route['super_admin/clients/admins/super_admin/clients/admins/change_password_listener'] = "super_admin/client_admins/change_password_listener";
$route['super_admin/clients/admins/add/(:num)'] = "super_admin/client_admins/add/$1";
$route['super_admin/clients/admins/add_listener'] = "super_admin/client_admins/add_listener";

/* ========================super admin forms fields ==================== */
$route['super_admin/super_form_fields/(:num)'] = "super_admin/form_fields/index/$1";

/* ========================super admin Contact Message ==================== */
$route['super_admin/contact_message'] = "super_admin/contact_message/index";

/* ========================super admin Contact Message ==================== */
$route['super_admin/feedback'] = "super_admin/feedback_details/index";
/* ========================super admin info icons Message ==================== */
$route['super_admin/info_icons'] = "super_admin/info_icons/index";
$route['super_admin/info_icons/add'] = "super_admin/info_icons/add";
$route['super_admin/info_icons/edit/(:num)'] = "super_admin/info_icons/edit/$1";


/* ==================== Admin Routes ================== */
// route for admin logo in session
$route['-/(:any)'] = "admin/authenticate/set_logo/$1";

$route['admin'] = "admin/list_insurance/index";
$route['login_home'] = "admin/authenticate/login";
$route['admin/login'] = "admin/authenticate/checkLogin";
$route['admin/show_page/(:any)'] = "admin/authenticate/show_page/$1";
$route['forgotpassword'] = "admin/authenticate/forgotPassword";
$route['admin/forgotpassword_listener'] = "admin/authenticate/forgotpassword_listener";
$route['admin/Reset/(:any)'] = "admin/authenticate/resetPassword/$1";
$route['admin/ResetPassword/(:any)'] = "admin/authenticate/resetPasswordListener/$1";


//////befor login page....
//$route['hoe_werkt_het'] = "admin/authenticate/hoe_werkt_het";
//$route['pakketten'] = "admin/authenticate/pakketten";
//$route['faq'] = "admin/authenticate/faq";
//$route['over_ons'] = "admin/authenticate/over_ons";
//$route['voordelen'] = "admin/authenticate/voordelen";
//$route['contact'] = "admin/authenticate/contact";
//$route['disclaimer'] = "admin/authenticate/disclaimer";
//$route['privacy_disclaimer'] = "admin/authenticate/privacy_disclaimer";
//$route['home'] = "admin/authenticate/index";
//$route['uitleg'] = "admin/authenticate/uitleg";
//$route['demo'] = "admin/authenticate/demo";
///////

$route['admin/dashboard'] = "admin/list_insurance/index";
$route['subscription_information'] = "admin/authenticate/subscription_information";
$route['about_us'] = "admin/authenticate/about_us";
$route['news'] = "admin/authenticate/show_news";
$route['Contact_me'] = "admin/contact_me";
$route['authenticate'] = "admin/authenticate";
$route['contact_me_function'] = "admin/contact_me/contact_me_function";
$route['save_contact'] = "admin/contact_me/save_contact";


$route['admin/content/(:any)'] = "admin/dashboard/viewDynamicText/$1";
/* ========================admin policy holder ==================== */
$route['admin/policy_holder'] = "admin/policy_holder/index";
/* ========================admin forms ==================== */
$route['admin/form_pdf/(:num)'] = "admin/client_forms/create_pdf_for_forms/$1";
/* ========================admin Statistieken ==================== */
$route['admin/statistic'] = "admin/list_insurance/form_overview";
$route['admin/statistic/save'] = "admin/list_insurance/saveStatistics";
$route['admin/statisticview/(:num)/(:num)'] = "admin/list_insurance/listsFormSent/$1/$2";
$route['admin/sendstatistic'] = "admin/list_insurance/sendStatistics";
/* ========================admin categories ==================== */
$route['admin/categories/(:num)'] = "admin/admin_forms_categories/index/$1";
$route['admin/categories/edit/(:num)'] = "admin/admin_forms_categories/edit/$1";
$route['admin/categories/add/(:num)'] = "admin/admin_forms_categories/add/$1";
$route['admin/categories/addListener/(:num)'] = "admin/admin_forms_categories/addListener/$1";
$route['admin/categories/editListener/(:num)'] = "admin/admin_forms_categories/editListener/$1";
$route['admin/categories/editListener/(:num)'] = "admin/admin_forms_categories/editListener/$1";
$route['admin/categories/sortable'] = "admin/admin_forms_categories/sortable";

/* ======================== admin categories questions==================== */
$route['admin/categories_question/(:num)'] = "admin/admin_forms_categories_question/index/$1";
$route['admin/categories_question/edit/(:num)'] = "admin/admin_forms_categories_question/edit/$1";
$route['admin/categories_question/add/(:num)'] = "admin/admin_forms_categories_question/add/$1";
$route['admin/categories_question/addListener/(:num)'] = "admin/admin_forms_categories_question/addListener/$1";
$route['admin/categories_question/editListener/(:num)'] = "admin/admin_forms_categories_question/editListener/$1";
$route['admin/categories_question/sortable'] = "admin/admin_forms_categories_question/sortable";
$route['admin/categories_question/delete_answer/(:num)'] = "admin/admin_forms_categories_question/deleteAnswer/$1";

/*
 * ======================== admin categories questions answers
 * ====================
 */
$route['admin/categories_question_answer/(:num)'] = "admin/admin_forms_categories_question_answer/index/$1";
$route['admin/categories_question_answer/edit/(:num)'] = "admin/admin_forms_categories_question_answer/edit/$1";
$route['admin/categories_question_answer/add/(:num)'] = "admin/admin_forms_categories_question_answer/add/$1";
$route['admin/categories_question_answer/addListener'] = "admin/admin_forms_categories_question_answer/add_listener";
$route['admin/categories_question_answer/editListener'] = "admin/admin_forms_categories_question_answer/edit_listener";

/* =================== admin User Routes ==================== */
$route['admin/admin_user'] = "admin/admin_user/index";
$route['admin/admin_user/update/(:num)'] = "admin/admin_user/editListener/$1";

/* ========================admin forms fields ==================== */
$route['admin/form_fields/(:num)'] = "admin/form_fields/index/$1";

// Admin Subscription
$route['admin/subscription/stop/(:num)'] = 'admin/admin_client_subscriptions/stopSubscription/$1';

// Admin departments
$route['admin/departments'] = 'admin/admin_department/index';
$route['admin/add_departments'] = 'admin/admin_department/add';
$route['admin/edit_departments/(:num)'] = 'admin/admin_department/edit/$1';
$route['admin/admin_departments/getJson'] = 'admin/admin_department/getJson';
$route['admin/admin_departments/delete/(:num)'] = 'admin/admin_department/delete/$1';
$route['admin/addListener_departments'] = 'admin/admin_department/addListener';
$route['admin/editListener_departments/(:num)'] = 'admin/admin_department/editListener/$1';

// Admin  invocie
$route['admin/invoices'] = 'admin/invoices/index'; 
// Admin  Standard form
$route['admin/standard_form_pdf/(:num)'] = "admin/standard_forms/create_pdf_for_forms/$1";
$route['admin/import_menually'] = "admin/import_menually/import_listener";

/* =================== User Routes ==================== */
$route['user'] = "user/dashboard/index";
$route['user/login'] = "user/authenticate/checkLogin";
$route['user/dashboard'] = "user/dashboard/index";
$route['user/welcome'] = "user/show_client_setting_page/welcome";
$route['user/about_us'] = "user/show_client_setting_page/about_us";
$route['user/contact'] = "user/show_client_setting_page/contact";
$route['user/news'] = "user/news/index";
$route['admin/send_newletters/(:num)'] = "admin/newletter/send_newletters/$1";

$route['polishouders'] = "user/authenticate/index";
$route['polishouder/edit_profile'] = "user/policy_holder_info/edit_policy_holder";
$route['polishouders/wachtwoord_vergeten'] = "user/authenticate/forgot_pasword";
$route['polishouders/wachtwoord_reset/(:any)'] = "user/authenticate/reset_pasword/$1";
$route['polishouders/logout'] = "user/authenticate/logout";
$route['polishouders/add_claim'] = "user/list_forms/add_claim";

/* =================== super admin standard forms ==================== */

$route['super_admin/standard_form_fields/(:num)'] = "super_admin/standard_form_fields/index/$1";

$route['super_admin/standard_form_pdf/(:num)'] = "super_admin/standard_forms/create_pdf_for_forms/$1";

$route['super_admin/standard_categories/(:num)'] = "super_admin/standard_forms_categories/index/$1";
$route['super_admin/standard_categories/edit/(:num)'] = "super_admin/standard_forms_categories/edit/$1";
$route['super_admin/standard_categories/add/(:num)'] = "super_admin/standard_forms_categories/add/$1";
$route['super_admin/standard_categories/addListener/(:num)'] = "super_admin/standard_forms_categories/addListener/$1";
$route['super_admin/standard_categories/editListener/(:num)'] = "super_admin/standard_forms_categories/editListener/$1";
$route['super_admin/standard_categories/editListener/(:num)'] = "super_admin/standard_forms_categories/editListener/$1";
$route['super_admin/standard_categories/sortable'] = "super_admin/standard_forms_categories/sortable";
$route['super_admin/standard_categories/getJson/(:num)'] = "super_admin/standard_forms_categories/getJson/$1";
$route['super_admin/standard_categories/delete/(:num)/(:num)'] = "super_admin/standard_forms_categories/delete/$1/$2";

$route['super_admin/standard_categories_question/(:num)'] = "super_admin/standard_categories_question/index/$1";
$route['super_admin/standard_categories_question/edit/(:num)'] = "super_admin/standard_categories_question/edit/$1";
$route['super_admin/standard_categories_question/add/(:num)'] = "super_admin/standard_categories_question/add/$1";
$route['super_admin/standard_categories_question/addListener/(:num)'] = "super_admin/standard_categories_question/addListener/$1";
$route['super_admin/standard_categories_question/editListener/(:num)'] = "super_admin/standard_categories_question/editListener/$1";
$route['super_admin/standard_categories_question/sortable'] = "super_admin/standard_categories_question/sortable";
$route['super_admin/standard_categories_question/delete_answer/(:num)'] = "super_admin/standard_categories_question/deleteAnswer/$1";
$route['super_admin/standard_categories_question/getJson/(:num)'] = "super_admin/standard_categories_question/getJson/$1";
$route['super_admin/standard_categories_question/delete/(:num)/(:num)'] = "super_admin/standard_categories_question/deleteQuestion/$1/$2";

$route['super_admin/standard_categories_question_answer/(:num)'] = "super_admin/standard_question_answer/index/$1";
$route['super_admin/standard_categories_question_answer/edit/(:num)'] = "super_admin/standard_question_answer/edit/$1";
$route['super_admin/standard_categories_question_answer/add/(:num)'] = "super_admin/standard_question_answer/add/$1";
$route['super_admin/standard_categories_question_answer/addListener'] = "super_admin/standard_question_answer/add_listener";
$route['super_admin/standard_categories_question_answer/editListener'] = "super_admin/standard_question_answer/edit_listener";
$route['super_admin/standard_categories_question_answer/get_json/(:num)'] = "super_admin/standard_question_answer/get_json/$1";
$route['super_admin/standard_categories_question_answer/delete/(:num)/(:num)'] = "super_admin/standard_question_answer/delete/$1/$2";

$route['super_admin/standard_group_forms/(:num)'] = "super_admin/standard_group_forms/index/$1";
$route['super_admin/standard_group_forms/getJson/(:num)'] = "super_admin/standard_group_forms/getJson/$1";
$route['super_admin/standard_group_forms/add/(:num)'] = "super_admin/standard_group_forms/add/$1";
$route['super_admin/standard_group_forms/edit/(:num)'] = "super_admin/standard_group_forms/edit/$1";
$route['super_admin/standard_group_forms/delete/(:num)'] = "super_admin/standard_group_forms/delete/$1";

$route['super_admin/log_mails'] = "super_admin/log_mails/index";
$route['super_admin/log_mails/get_json'] = "super_admin/log_mails/get_json";

$route['super_admin/archive_form'] = "super_admin/archive_form/index";








/* End of file routes.php */
/* Location: ./application/config/routes.php */
