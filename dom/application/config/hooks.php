<?php

if (! defined('BASEPATH'))
    exit('No direct script access allowed');
    /*
 * | ------------------------------------------------------------------------- |
 * Hooks |
 * ------------------------------------------------------------------------- |
 * This file lets you define "hooks" to extend CI without hacking the core |
 * files. Please see the user guide for info: |
 * |	http://codeigniter.com/user_guide/general/hooks.html |
 */
    
// the hook to check for login
$hook['pre_controller'][] = array(
        'class' => 'login_check_hook',
        'function' => 'checkLogin',
        'filename' => 'login_check_hook.php',
        'filepath' => 'hooks'
);

// this hook is to make sure that the user is not accesing some one else's
// claim. redirects to home page if inappropriate page is accessed by the
// policy holder
/*$hook['pre_controller'][] = array(
        'class' => 'claims_check_hook',
        'function' => 'check_claims',
        'filename' => 'claims_check_hook.php',
        'filepath' => 'hooks'
);*/
$hook['pre_controller'][] = array(
        'class' => 'policy_holder_validation',
        'function' => 'checkLogin',
        'filename' => 'policy_holder_validation.php',
        'filepath' => 'controllers/user/'
);




/* End of file hooks.php */
/* Location: ./application/config/hooks.php */