
<h2 class="title-margin"><span>Welkom</span></h2>
<?php echo $content; ?>
<h2 class="title-margin"><span>Mijn formulieren</span></h2>
<div class="clear"></div>
<div class="responsive-table" style="margin-top: 10px;">
    <div class="special-list-form">
        <table id="list_clients" class="tbl table table-striped table-condensed" width="100%">
            <thead>
                <tr class="tbl_hd">
                    <th width="12%">Datum</th>
                    <th>Nummer</th>
                    <th>Formuliernaam</th>
                    <th>Polisnummer</th>
                    <th>Behandelaar</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<!--<div>
    <form
        action="<?php echo base_url() . 'polishouders/add_claim'; ?>"  method="post" enctype="multipart/form-data">
        <table class="table table-striped table-condensed">
            <tr>
                <td valign="top" style="width: 20%">
<?php
if (isset($form_info) && $form_info != '') {
    ?>
                                            
                                        <select  name="form">
                                            <option value="">Selecteer formulier</option>
    <?php
    foreach ($form_info as $form) {
        ?>
                                                                <option value="<?php echo $form->form_id; ?>"><?php echo htmlentities($form->form_name); ?></option>
        <?php
    }
    ?>
                                        </select>
                                            
<?php } ?>
                </td>
                <td valign="top">
                    <input type="submit" value="Vul formulier in" class="btn btn-large btn-primary" />
                </td>
                <td><?php echo form_error('form'); ?></td>
            </tr>
        </table>
    </form>
</div>-->
<?php
if (count($news) > 0) {
    ?>
    <div class="home-news">
        <div class="container">
            <div class="row-fluid">
                <div class="span12">
                    <div class="policyholder-news">
                        <h2><span>Laatste nieuws</span></h2>
                        <div class="policyholder-news-box">
                            <?php
                            $i = 1;
                            foreach ($news as $news_item) {
                                ?>
                                <div class="<?php
                        if ($i % 2 == 0) {
                            echo 'pull-right';
                        } else {
                            echo 'pull-left';
                        }
                                ?> custom-contain span6 margin-killer">
                                    <h3><?php echo $news_item->title; ?></h3> 
                                    <?php
                                    echo $news_item->newz_text;
                                    ?>
                                </div>
                                <?php
                                $i = $i + 1;
                            }
                            ?>
                        </div> 
                    </div>
                </div>
            </div></div>
    </div>
<?php } ?>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_clients').dataTable( {

            "bProcessing":true,
            "bPaginate" : false,
            "bFilter": false,
            "bInfo": false,
            "bSort": false,
            "aaSorting": [[ 0, "desc" ]],
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center no-wrap"},{"sClass":"align_center"},
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center no-wrap force-to-center"}
            ],
            "sAjaxSource": "<?php echo site_url("user/list_forms/get_json"); ?>"
        } );
    } );

</script>
<style type="text/css">
    .align-top{vertical-align: top;}
    .force-to-center{text-align:center !important;}
    .force-to-center img{max-width:inherit !important;}
</style>