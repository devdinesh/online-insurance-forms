<div>
    <h2 class="title-margin">  <?php echo $form->form_name; ?> </h2>
     <form method="post" action="<?php echo base_url() . "user/answer/edit_details/" . $claim_id; ?>">
        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>
                <?php
                foreach ($field_value as $field) {
                    ?>
                    <tr>
                        <td><?php echo $field->name_at_form; ?></td>

                        <td>
                            <?php if ($field->name == $email_field->field_name) {
                                ?>
                                <input type="text" name="<?php echo str_replace(' ', '_', $field->name_at_form); ?>"
                                       value="<?php echo htmlentities($field->filed_value); ?>">
                                       <?php
                                       if (isset($field->is_change) && $field->is_change == '1') {
                                           echo " !";
                                       }
                                   } else {
                                       echo htmlentities($field->filed_value);
                                   }
                                   ?>
                        </td>
                        <td></td>
                    </tr>
                <?php } ?>

                <tr>
                    <td colspan="2"><input class="btn" type="submit" value="Opslaan"> 
                        <a class="btn" href="<?php echo base_url() . 'user/list_forms'; ?>"> Terug </a> 
                    </td>
                    <td></td>
                </tr>

            </tbody>
        </table>
    </form>
</div>