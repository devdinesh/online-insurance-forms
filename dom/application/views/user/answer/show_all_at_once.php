<!--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->
<script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery-1.9.1.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>bootstrap-tooltip.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery-ui.js"></script>
<!--<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>-->
<script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery.validate.js"></script>
<script>
    $(function() {
        $( document ).tooltip();
    });
    $(function() {
        jQuery(".datepicker").datepicker({  changeYear: true,changeMonth: true,yearRange:'-90:+20',dateFormat: "dd/mm/yy" }).val();
    });
</script>
<script>
    function textAreaAdjust(o) {
        o.style.height = "1px";
        o.style.height = (25+o.scrollHeight)+"px";
    }
</script>
<script>
    Number.prototype.formatMoney = function(c, d, t){
        var n = this, 
        c = isNaN(c = Math.abs(c)) ? 2 : c, 
        d = d == undefined ? "." : d, 
        t = t == undefined ? "," : t, 
        s = n < 0 ? "-" : "", 
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
        j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };
    jQuery(document).ready(function(){
        jQuery("#main_frm").validate({
            onfocusout:function(element) {
                if($(element).hasClass('currency')){
                    return false;
                }else{
                    return true;
                }
            },
            onkeyup: function(element) {
                if($(element).hasClass('currency')){
                    return false;
                }else{
                    return true;
                }
            },
            errorElement:'span',
            errorPlacement: function(error, element) {
                if ($(element).attr('type') == 'checkbox' || $(element).attr('type') == 'radio') {
                    $(element).parents('.q_parent').siblings('.q_error').html(error);
                } else {
                    error.insertAfter(element);
                }
            }
        });
        $.validator.addMethod("currency", function (value, element) {
            return this.optional(element) || /^\€ (\d{1,3}(\.\d{3})*|(\d+))(\,\d{2})?$/.test(value);
        }, "Please enter valid financially amount."); 
    });
    function formate_currency(obj){
        var value=Number($(obj).val());
        if(isNaN(value)===false && $(obj).val().length > 0){
            $(obj).val('€ '+value.formatMoney(2,',','.'));
        }
    }
</script>
<style type="text/css">
    .que_box{
        border:none !important;
        background:#fff !important;
    }
    h2 > span{
        font-size: 23px !important;
        padding: 0 5px 7px 0 !important;}
    .que_box > h3{
        font-size: 18px !important;
    }
    .q_parent{
        float: left;
        width: 100% !important;
        margin:0.5% 0 0 0 !important;
    }
</style>
<h2><span><?php echo $form->form_name; ?></span></h2>
<div class="que_box">
    <p>
        <?php
        $form_intorduction_text = get_form_all_text($claim_id, $form->introduction_text);
        echo $form_intorduction_text;
        ?>
    </p>
</div>
<!-- Start Questions -->
<form id='main_frm' method="post" action="<?php echo base_url() . "user/answer/get_smart_questions/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form->form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $category_id . '/' . $sequence; ?>">
    <div class="que_box">
        <input type="hidden" name="action" value="">
        <input type="hidden" name="last_answer_id" id="last_answer_id" value="">
        <?php
        $i = 0;
        if (isset($category_info)) {
            foreach ($category_info as $cat_info) {
                $i++;
                ?>

                <h3><?php echo $cat_info->cat_name; ?></h3>
                <p>
                    <?php
                    $cat_intorduction_text = get_category_introduction_text($claim_id, $cat_info->cat_id);
                    echo $cat_intorduction_text;
                    ?> 
                </p>
                <br/>
                <?php
                if (isset($questions)) {
                    if (is_array($questions) && count($questions) > 0) {
                        foreach ($questions as $question) {
                            if ($cat_info->cat_id == $question->cat_id) {
                                ?>
                                <?php if ($question->answer_kind == 'text') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span  <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>  </span>
                                            </h4>
                                        </div>
                                        <div class="que_bg_right">
                                            <input  name="<?php echo $question->question_id, '_'; ?>" type="text"  class="que_textfiled" <?php echo $question->required == 1 ? "required" : ""; ?> value="<?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_policy_holder($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                    ?>">
                                            <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($question->answer_kind == 'date') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span
                                                    <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                                </span>
                                            </h4>

                                        </div>
                                        <div class="que_bg_right">
                                            <input
                                                name="<?php echo $question->question_id, '_'; ?>" type="text"
                                                class="que_textfiled datepicker" <?php echo $question->required == 1 ? "required" : ""; ?>
                                                value="<?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_policy_holder($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                                    ?>">
                                            <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($question->answer_kind == 'number') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span
                                                    <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                                </span> 
                                            </h4>
                                        </div>
                                        <div class="que_bg_right">
                                            <input
                                                name="<?php echo $question->question_id, '_'; ?>" type="text"
                                                class="que_textfiled number" <?php echo $question->required == 1 ? "required" : ""; ?>
                                                value="<?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_policy_holder($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                                    ?>">
                                            <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($question->answer_kind == 'financieel') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span
                                                    <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                                </span> 
                                            </h4>
                                        </div>
                                        <div class="que_bg_right">
                                            <input
                                                name="<?php echo $question->question_id, '_'; ?>" type="text"
                                                class="que_textfiled currency"  onchange="formate_currency(this);" <?php echo $question->required == 1 ? "required" : ""; ?>
                                                value="<?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_policy_holder($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                                    ?>">
                                            <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($question->answer_kind == 'textarea') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span
                                                    <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                                </span> 
                                            </h4>
                                        </div>
                                        <div class="que_bg_right">
                                            <textarea class="que_textarea"
                                                      name="<?php echo $question->question_id . "_"; ?>" onkeyup="textAreaAdjust(this)"
                                                      <?php echo $question->required == 1 ? "required" : ""; ?>><?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_policy_holder($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo htmlentities($user_ans->answer_text);
                                }
                            }
                                                      ?></textarea>
                                            <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($question->answer_kind == 'radio') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span
                                                    <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                                </span> 
                                            </h4>
                                        </div>
                                        <div class="que_bg_right">
                                            <?php
                                            // get the options and interate thru them
                                            $this->load->model(
                                                    'forms_categories_question_model');
                                            $answers = $this->forms_categories_question_answer_model->get_where(array('question_id' => $question->question_id));
                                            $skip_question = false;
                                            foreach ($answers as $answer) {
                                                if ($answer->skip_to_questions != NULL) {
                                                    $skip_question = true;
                                                    break;
                                                }
                                            }

                                            $user_ans = get_answer_given_by_policy_holder($claim_id, $question->question_id);
                                            ?>
                                            <div class="span12 form-blank-span">&nbsp;</div>
                                            <?php
                                            foreach ($answers as $answer) {
                                                if (isset($skip_question) && $skip_question == true) {
                                                    ?>
                                                    <div class="span5 q_parent">
                                                        <div class="radio_bg">
                                                            <input name="<?php echo (isset($question->question_id) ? $question->question_id : '') . "_"; ?>" type="radio" <?php
                                    if ($this->input->post($question->question_id . '_') == $answer->answer_id) {
                                        echo "checked='checked'";
                                    } elseif (isset($user_ans)) {
                                        if ($user_ans->answer_text == $answer->answer_id) {
                                            echo "checked='checked'";
                                        }
                                    }
                                                    ?> value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>" <?php echo $question->required == 1 ? "required" : ""; ?> onclick="checkselection(this)" />
                                                                   <?php $last_question_name = $question->question_id . '_'; ?>
                                                            <span><?php echo $answer->answer; ?></span>

                                                        </div>
                                                    </div>

                                                    <?php
                                                } else {
                                                    ?>
                                                    <div class="span5 q_parent">
                                                        <div class="radio_bg">
                                                            <input name="<?php echo (isset($question->question_id) ? $question->question_id : '') . "_"; ?>" type="radio" <?php
                                    if ($this->input->post($question->question_id . '_') == $answer->answer_id) {
                                        echo "checked='checked'";
                                    } elseif (isset($user_ans)) {
                                        if ($user_ans->answer_text == $answer->answer_id) {
                                            echo "checked='checked'";
                                        }
                                    }
                                                    ?> value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>" <?php echo $question->required == 1 ? "required" : ""; ?>  />
                                                            <span><?php echo $answer->answer; ?></span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <span class="error q_error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($question->answer_kind == 'checkbox') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span
                                                    <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                                </span> 
                                            </h4>

                                        </div>
                                        <div class="que_bg_right">
                                            <?php
                                            $count_check = 0;

                                            // get the options and interate thru them
                                            $this->load->model(
                                                    'forms_categories_question_answer_model');
                                            $forms_categories_question_answer_model = new forms_categories_question_answer_model();
                                            $answers = $forms_categories_question_answer_model->get_where(
                                                    array(
                                                        'question_id' => $question->question_id
                                                    ));
                                            $user_ans = get_answer_given_by_policy_holder(
                                                    $claim_id, $question->question_id);

                                            $this->load->model(
                                                    'forms_answers_details_model');
                                            $obj_form_answer = new forms_answers_details_model();
                                            if (isset($user_ans)) {
                                                $re_ans = $obj_form_answer->get_where(
                                                        array(
                                                            'user_answer_id' => $user_ans->answer_id
                                                        ));
                                            }
                                            ?>
                                            <div class="span12 form-blank-span">&nbsp;</div>
                                            <?php
                                            foreach ($answers as $answer) {
                                                $count_check++;
                                                ?>
                                                <div class="span5 q_parent">
                                                    <div class="radio_bg">
                                                        <input  name="<?php echo $question->question_id, '_[]'; ?>"
                                                        <?php
                                                        if (isset($re_ans)) {
                                                            foreach ($re_ans as $r_ans) {
                                                                if ($r_ans->answer_id == $answer->answer_id) {
                                                                    echo "checked='checked'";
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                                id="<?php echo $question->question_id, '_', $count_check; ?>"
                                                                type="checkbox" <?php echo set_checkbox($question->question_id . '_', $answer->answer_id); ?> value="<?php echo $answer->answer_id; ?>"
                                                                <?php echo $question->required == 1 ? "required" : ""; ?>>
                                                        <span><?php echo $answer->answer; ?></span>
                                                        <input type="hidden"
                                                               value="<?php echo $count_check; ?>"
                                                               name='<?php echo $question->question_id, '_', 'count' ?>'>
                                                    </div>
                                                </div>

                                            <?php } ?>
                                            <span class="error q_error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                            }
                        }
                    }
                }
                ?>
                <?php if ($i != count($category_info)) { ?>
<!--                    <hr class="border">-->
                    <?php
                }
            }
        }
        ?>
    </div>
</form>
<?php if ($show_end_data == TRUE) { ?>
    <div class="que_box">
        <img class="exp_img"  src='<?php echo assets_url_img . "exclamation.png"; ?>' alt='exclamation' title='exclamation'>
        <p>
            <?php
            $form_closure_text = get_form_all_text($claim_id, $form->closure_text);
            echo $form_closure_text;
            ?>
        </p>
    </div>
    <div class="que_box">
        <table style="background: #fff;">
            <?php
            $this->load->model('claims_files_model');
            $files = claims_files_model::get_claims_for_claim_id($claim_id);
            $check_file_error = $this->session->flashdata('file_errors');
            if (isset($check_file_error) && $check_file_error != '') {
                echo '<p style="color: red;">' . $check_file_error . '</p>';
            }
            if (count($files) == 0) {
                echo "<tr><td colspan=\"3\"><b>Bijlagen</b></td></tr>";
            } else {
                echo "<tr><td colspan=\"3\"><b>Bijlagen</b><br/>";
                foreach ($files as $file) {
                    echo anchor($file->get_path_for_web(), $file->file_name, 'target="_blank"');
                    echo '&nbsp;&nbsp;&nbsp;';
                    echo anchor(base_url() . 'user/answer/deleteAttachmentOnce/' . $this->encrypt->encode($claim_id, "**DOM_CLAIM_DEVREPUBLIC**") . "/" . $this->encrypt->encode($form->form_id, "**DOM_CLAIM_DEVREPUBLIC**") . '/' . $category_id . '/' . $sequence . '/' . $this->encrypt->encode($file->file_id, "**DOM_CLAIM_DEVREPUBLIC**"), '<i class="icon-trash"></i>');
                    echo "<br>";
                }
                echo "</br>";
                echo "</td></tr>";
            }
            ?>
            <tr>
                <td colspan="3">
                    <form method="post" id="main_frm_1" action="<?php echo base_url() . 'user/answer/uploadAttachmentOnce/' . $this->encrypt->encode($claim_id, "**DOM_CLAIM_DEVREPUBLIC**") . "/" . $this->encrypt->encode($form->form_id, "**DOM_CLAIM_DEVREPUBLIC**") . '/' . $category_id . '/' . $sequence; ?>" enctype="multipart/form-data">
                        <input type="file" class="input" name="attachment" />
                        <input type="submit" value="Toevoegen" class="btn btn-large btn-primary" id="submit_upload_id" />
                    </form>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <label class="checkbox">
                        <input type="checkbox" value="yes" name="filled_in_honestly" id="filled_in_honestly"> Naar waarheid ingevuld en gecontroleerd. <font color="red">*</font> 
                        <span id="error-text" style="color: red;"></span><br>
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="row-fluid">
                        <div class="btn_form">  
                            <a href="<?php echo base_url() . 'user/list_forms'; ?>"
                               class="btn btn-primary btn-large">Terug</a>
                        </div>
                        <!-- link for Opslaan button -->
                        <form method="get" action="#" class="pull-left">
                            <div class="btn_form">  
                                <input type="button" value="Opslaan in concept" onclick="save_form()" class="btn btn-primary btn-large" />
                            </div>
                        </form>
                        <!-- link for Opslaan button -->
                        <form method="get" action="#" class="pull-left">
                            <div class="btn_form">  
                                <input type="button" value="Bevestig en verstuur" onclick="confirm_and_send()" class="btn btn-large btn-primary" />
                            </div>
                        </form>
                    </div>
                </td>
            </tr>
        </table>

    </div>
<?php } else { ?>
    <div class="btn_form"><a href="<?php echo base_url() . 'user/list_forms'; ?>" class="btn btn-primary btn-large">Terug</a></div>
    <div class="btn_form"><input type="button" value="volgende" onclick="opslaan_form()" class="btn btn-primary btn-large" /></div>
    <?php }
    ?>

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#submit_upload_id').click(function(e){
            e.preventDefault();
            submit_both_form();
        })
    })
    function submit_both_form(){
        var data = jQuery('#main_frm').serialize(); 
        $.ajax({
            url: '<?php echo base_url() . 'user/answer/saveShowallAtOnce/' . $this->encrypt->encode($claim_id, "**DOM_CLAIM_DEVREPUBLIC**") . "/" . $this->encrypt->encode($form->form_id, "**DOM_CLAIM_DEVREPUBLIC**") . '/' . $category_id . '/' . $sequence ?>',
            data: data,
            success: function(data){   
                // console.log(data);
                document.getElementById('main_frm_1').submit();
            },
            cache: false,
            type: "POST"

        });
        return false;
    }
</script>
<script type="text/javascript">
<?php if ($show_end_data == TRUE) { ?>
        function check_confirm()
        {
            if ( document.getElementById('filled_in_honestly').checked ==false) 
            {
                document.getElementById('error-text').innerHTML = 'Dit veld is verplicht';
                return false ;
            }
            else
            {
                return true ;
            } 
        }

        function save_form()
        {
            if (jQuery('#main_frm').validate().form()){
                document.forms['main_frm'].action.value = 'Opslaan in concept';
                if(check_confirm()==false)
                {
                    return false;
                }		
                else
                {
                    $('#main_frm').attr('action', '<?php echo base_url() . 'user/answer/wholeFormSaveActionAtOnce/' . $this->encrypt->encode($claim_id, "**DOM_CLAIM_DEVREPUBLIC**") . "/" . $this->encrypt->encode($form->form_id, "**DOM_CLAIM_DEVREPUBLIC**") . '/' . $category_id . '/' . $sequence; ?>'); //this fails silently
                    $('#main_frm').get(0).setAttribute('action', '<?php echo base_url() . 'user/answer/wholeFormSaveActionAtOnce/' . $this->encrypt->encode($claim_id, "**DOM_CLAIM_DEVREPUBLIC**") . "/" . $this->encrypt->encode($form->form_id, "**DOM_CLAIM_DEVREPUBLIC**") . '/' . $category_id . '/' . $sequence; ?>'); //this works
                    document.getElementById("main_frm").submit();
                }
            }
        }

        function confirm_and_send()
        {
            if (jQuery('#main_frm').validate().form()){
                document.forms['main_frm'].action.value = 'confirm';
                if(check_confirm()==false)
                {
                    return false;
                }		
                else
                {
                    $('#main_frm').attr('action', '<?php echo base_url() . 'user/answer/wholeFormSaveActionAtOnce/' . $this->encrypt->encode($claim_id, "**DOM_CLAIM_DEVREPUBLIC**") . "/" . $this->encrypt->encode($form->form_id, "**DOM_CLAIM_DEVREPUBLIC**") . '/' . $category_id . '/' . $sequence; ?>'); //this fails silently
                    $('#main_frm').get(0).setAttribute('action', '<?php echo base_url() . 'user/answer/wholeFormSaveActionAtOnce/' . $this->encrypt->encode($claim_id, "**DOM_CLAIM_DEVREPUBLIC**") . "/" . $this->encrypt->encode($form->form_id, "**DOM_CLAIM_DEVREPUBLIC**") . '/' . $category_id . '/' . $sequence; ?>'); //this works
                    document.getElementById("main_frm").submit();
                }
            }
        }
<?php } ?>
    function checkselection(ele){
        var answer_id=ele.value;
        jQuery('#last_answer_id').val(answer_id);
        if (jQuery('#main_frm').validate().form()){
            document.getElementById("main_frm").submit();
            /*window.location = "<?php echo base_url() . 'user/answer/get_smart_questions/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence . '/'; ?>" + answer_id;*/
        }
    }
    function opslaan_form(){
        var question_name='<?php echo @$last_question_name; ?>';
        var answer_id=$('[name='+question_name+']:checked').val();
        jQuery('#last_answer_id').val(answer_id);
        if (jQuery('#main_frm').validate().form()){
            if(answer_id!==undefined){
            
                document.getElementById("main_frm").submit();
                /*window.location = "<?php echo base_url() . 'user/answer/get_smart_questions/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence . '/'; ?>" + answer_id;*/
            }
            else{
                alert('Please fill the last question first.');
            }
        }
    }
</script>
