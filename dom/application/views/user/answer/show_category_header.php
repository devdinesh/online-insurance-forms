<style type="text/css">
    .que_box{
        border:none !important;
        background:#fff !important;
    }
    h2 > span{
        font-size: 23px !important;
        padding: 0 5px 7px 0 !important;}
    .que_box > h3{
        font-size: 18px !important;
    }
</style>
<h2><span><?php echo $form->form_name; ?></span></h2>
<div>
<!--    <h3><?php echo @$total_form_completed; ?>% form compleet</h3>-->
    <div class="progress progress-striped active">
        <div class="bar" style="width: <?php echo @$total_form_completed . '%'; ?>;"></div>
    </div>
</div>
<div class="que_box">
    <p>
        <?php
        $form_intorduction_text = get_form_all_text($claim_id, $form->introduction_text);
        echo $form_intorduction_text;
        ?>
    </p>
</div>
<div class="btn_form"><a href="<?php echo base_url() . 'user/list_forms'; ?>" class="btn btn-primary btn-large">Terug</a></div>
<div class="btn_form">  
    <?php if (isset($next_category)) { ?>
        <a href="<?php echo base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form->form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($next_category['next_cat']->cat_id, '**DOM_CLAIM_DEVREPUBLIC**'); ?>" class="btn btn-primary btn-large">Volgende</a>

    <?php } ?>
</div>
