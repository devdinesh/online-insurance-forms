
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<script>
    $(function() {
        $( document ).tooltip();
    });

    <!--
    function go_back()
    {
        document.forms['main_frm'].action.value = 'Terug';
        document.getElementById("main_frm").submit();
    }
    //-->
</script>
<style>
    label {
        display: inline-block;
        /*width: 4em;*/
    }
</style>
<div>
    <!-- questions -->
    <form id='main_frm' method="post" action="<?php
if ($form->show_all_question_at_once != 1) {
    if (trim($categoty_id) != 'end' && trim($next_category_id) != '#') {
        echo base_url() . "user/answer/preview_form/" . $claim_id . "/" . $form->form_id . "/" . $next_category_id;
    } else if (trim($categoty_id) != 'end') {
        echo base_url() . "user/answer/preview_form/" . $claim_id . "/" . $form->form_id . "/end";
    } else if ($next_category_id == 'end' || $next_category_id == '#') {
        if (isset($previous_category_id)) {
            echo base_url() . "user/answer/preview_form/" . $claim_id . "/" . $form->form_id . "/" . $previous_category_id;
        }
    }
} else if ($categoty_id == 'end') {
    echo base_url() . "user/answer/preview_form/" . $claim_id . "/" . $form->form_id . "/end";
} else {
    echo base_url() . "user/answer/whole_form_save_action/" . $claim_id . "/" . $form_id;
}
?>">
        <input type="hidden" name="action" value="">
        <input type="hidden" name="sequence" value="<?php echo $sequence ?>">
        <!--  to be used when show at once -->
        <table class="table">
            <?php
            if (isset($categoryies)) {
                foreach ($categoryies as $category) {
                    ?>
                    <tr>
                        <td colspan="3">

                            <h3><?php echo $category->cat_name; ?> </h3>
                            <?php echo $category->introduction_text; ?>        
                        </td>
                    </tr>
                    <?php
                    $questions = $category->get_questions();

                    if (isset($questions)) {
                        if (is_array($questions) && count($questions) > 0) {
                            foreach ($questions as $question) {
                                if ($question->answer_kind == 'text') {
                                    ?>
                                    <tr>
                                        <td width="30%" style="vertical-align: text-top;" valign="top"> <?php echo substr($question->question, 3, count($question->question) - 5) ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?> </td>
                                        <td><input name="<?php echo $question->question_id, '_'; ?>"
                                                   type="text" class="input"
                                                   <?php echo $question->required == 1 ? "" : ""; ?>
                                                   value="<?php
                           $user_ans = get_answer_given_by_policy_holder(
                                   $claim_id, $question->question_id);

                           if (isset($user_ans)) {
                               echo $user_ans->answer_text;
                           }
                                                   ?>"> <!--  help text begins --> <span
                                                   <?php
                                                   if ($question->help_text != Null) {
                                                       ?>
                                                       title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                        class="icon-question-sign"></i>
                                                    <?php } ?>
                                            </span> <!--  help text ends --></td>



                                        <td>
                                            <?php echo form_error($question->question_id . "_"); ?>
                                        </td>

                                    </tr>
                                    <?php
                                } else if ($question->answer_kind == 'textarea') {
                                    ?>
                                    <tr>
                                        <td style="vertical-align: text-top;" valign="top"> <?php echo substr($question->question, 3, count($question->question) - 5) ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?> </td>
                                        <td><textarea name="<?php echo $question->question_id . "_"; ?>"
                                                      <?php echo $question->required == 1 ? "" : ""; ?>><?php
                              $user_ans =
                                      get_answer_given_by_policy_holder($claim_id, $question->question_id);
                              if
                              (isset($user_ans)) {
                                  echo
                                  htmlentities($user_ans->answer_text);
                              }
                                                      ?></textarea> <!--  help text begins -->
                                            <?php if ($question->help_text != null) {
                                                ?>
                                                <span title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                        class="icon-question-sign"></i>
                                                </span>
                                            <?php } ?>
                                            <!--  help text ends --> </span></td>

                                        <td>
                                            <?php echo form_error($question->question_id . "_"); ?>
                                        </td>
                                    </tr>
                                    <?php
                                } else if ($question->answer_kind == 'radio') {
                                    ?>
                                    <tr>
                                        <td style="vertical-align: text-top;" valign="top"> <?php echo htmlentities(substr($question->question, 3, count($question->question) - 5)) ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?> </td>
                                        <td>
                                            <?php
                                            // get the options and interate thru them
                                            $this->load->model('forms_categories_question_model');
                                            $answers = $this->forms_categories_question_answer_model->get_where(array('question_id' => $question->question_id));

                                            $user_ans = get_answer_given_by_policy_holder(
                                                    $claim_id, $question->question_id);

                                            foreach ($answers as $answer) {
                                                ?>
                                                <label class="radio">  <input
                                                        name="<?php echo (isset($question->question_id) ? $question->question_id : '') . "_"; ?>" type="radio" 
                                                        <?php
                                                        if (isset($user_ans)) {
                                                            if ($user_ans->answer_text == $answer->answer_id) {
                                                                echo "checked='checked'";
                                                            }
                                                        }
                                                        ?>
                                                        value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>" class="input"
                                                        <?php echo $question->required == 1 ? "" : ""; ?>> <?php echo $answer->answer; ?>

                                                    </input> </label>&nbsp;<br/>
                                                <?php
                                            }
                                            ?>
                                            <!--  help text begins -->
                                            <?php
                                            if ($question->help_text != Null) {
                                                ?>
                                                <span title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                        class="icon-question-sign"></i>
                                                </span>
                                            <?php } ?>
                                            <!--  help text ends -->

                                        </td>
                                        <td>
                                            <?php echo form_error($question->question_id . "_"); ?>
                                        </td>
                                    </tr>
                                    <?php
                                } else if ($question->answer_kind == 'checkbox') {
                                    ?>
                                    <tr>
                                        <td style="vertical-align: text-top;" valign="top"> <?php echo substr($question->question, 3, count($question->question) - 5) ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?> </td>
                                        <td>
                                            <?php
                                            $count_check = 0;

                                            // get the options and interate thru them
                                            $this->load->model(
                                                    'forms_categories_question_answer_model');
                                            $forms_categories_question_answer_model = new forms_categories_question_answer_model();
                                            $answers = $forms_categories_question_answer_model->get_where(
                                                    array(
                                                        'question_id' => $question->question_id
                                                    ));
                                            $user_ans = get_answer_given_by_policy_holder(
                                                    $claim_id, $question->question_id);

                                            $this->load->model('forms_answers_details_model');
                                            $obj_form_answer = new forms_answers_details_model;
                                            if (isset($user_ans)) {
                                                $re_ans = $obj_form_answer->get_where(array('user_answer_id' => $user_ans->answer_id));
                                            }
                                            foreach ($answers as $answer) {
                                                $count_check++;
                                                ?>
                                                <label class="checkbox checkbox-align_preview_form label_checkbox" style="line-height : 10px !importan; margin-bottom:-15px;"> <input
                                                        name="<?php echo $question->question_id, '_[]'; ?>"
                                                        <?php
                                                        if (isset($re_ans)) {
                                                            foreach ($re_ans as $r_ans) {
                                                                if ($r_ans->answer_id == $answer->answer_id) {
                                                                    echo "checked='checked'";
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        id="<?php echo $question->question_id, '_', $count_check; ?>"
                                                        type="checkbox" value="<?php echo $answer->answer_id; ?>"
                                                        <?php echo $question->required == 1 ? " " : ""; ?> class="input"> <?php echo $answer->answer; ?> </input>
                                                </label> &nbsp;
                                                <?php
                                            }
                                            ?>
                                            <input type="hidden"
                                                   value="<?php echo $count_check; ?>"
                                                   name='<?php echo $question->question_id, '_', 'count' ?>'> <!--  help text begins -->
                                                   <?php
                                                   if ($question->help_text != Null) {
                                                       ?>
                                                <span
                                                    class="icon-question-sign" style="display: block; float: left; margin-right: 15px; margin-top:20px;" title="<?php echo htmlentities($question->help_text); ?>"></span>
                                                <?php } ?>
                                            <!--  help text ends -->


                                        </td>
                                        <td>
                                            <?php echo form_error($question->question_id . "_"); ?>
                                        </td>
                                    </tr>
                                    <?php
                                } else {
                                    echo "<tr><td>Invalid question type</td></tr>";
                                }
                            }
                            ?>
                <!-- <tr> <td colspan="2"> <a href="#" class="btn pull-right"> Opslaan </a> </td> </tr> -->
                            <?php
                        }
                    } else {
                        echo "No Questions in this category";
                    }
                }
            }
            ?>


            <?php
// show closure text
            if (trim($categoty_id) == 'end' ||
                    $form->show_all_question_at_once == 1) {
                ?>
                <tr>
                    <td colspan="3">
                        <img class="pull-left" src='<?php echo assets_url_img . "exclamation.png"; ?>'
                             alt='exclamation' title='exclamation' style="margin:3px;">
                             <?php echo $form->closure_text; ?>
                    </td>
                </tr>
                <?php
            }
            ?>
            <!-- start mandetory field -->
            <?php
            if (isset($categoryies)) {
                ?>
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td colspan="3">De met een <font color="red">* </font> gemarkeerde velden zijn verplicht.
                    </td>

                </tr>
                <?php
            }
            ?>
            <!-- end mandetory field -->
        </table>


        <?php
        $cat_id = $this->input->post('old_categoty_id');

        // if the category id is set but is blank
        if (isset($cat_id) && $cat_id == '' && isset($categoryies)) {

            // get the previous category id here
            // print_r($category);
            $cat_id = $category->get_previous_category_id();
        }
        if ($categoty_id == "") {
            if ($form->show_all_question_at_once == 0) {
                ?>
                <table class="pull-right">
                    <tr>
                        <td>
                            <!-- Terug button on first page --> <input type="button"
                                                                       value="Terug" class="button btn"
                                                                       onclick="window.location.href='<?php echo base_url() . "user/list_forms"; ?>'" />
                        </td>
                        <td><input type="submit" value="Volgende" name="Volgende" class="button btn"></td>
                    </tr>
                </table>
                <?php
            }
        } else if ($form->show_all_question_at_once != 1 &&
                trim($categoty_id) != 'end' && trim($next_category_id) != '#') {
            ?>
            <table class="pull-right">
                <tr>
                    <td><input value="Terug" type="button"
                               onclick="window.location.href='<?php
        echo base_url() . "user/answer/preview_form/" . $claim_id . "/" .
        $form->form_id . "/" . $cat_id;
            ?>'"
                               class="button btn pull-right" /></td>
                    <td><input type="submit" value="Volgende" name="Volgende" class="button btn"></td>

                </tr>
            </table> <?php
                       } else if ($form->show_all_question_at_once != 1 &&
                               trim($categoty_id) != 'end') {
            ?>
            <table class="pull-right">
                <tr>
                    <td><input value="Terug" type="button"
                               onclick="window.location.href='<?php
        echo base_url() . "user/answer/preview_form/" . $claim_id . "/" .
        $form->form_id . "/" . $cat_id;
            ?>'"
                               class="button btn pull-right" /></td>
                    <td><input type="submit" value="Volgende" name="Volgende" class="button btn"></td>
                </tr>
            </table>

            <?php
        }
        ?>


        <?php
        if (isset($categoty_id) && $categoty_id) {
            ?>
            <input type="hidden" value="<?php echo $categoty_id; ?>"
                   name="old_categoty_id">
                   <?php
               }
               ?>
    </form>



    <?php
    // show the upload button and the uploaded files
    if (trim($categoty_id) == 'end' || $form->show_all_question_at_once == 1) {

        $this->load->model('claims_files_model');
        $files = claims_files_model::get_claims_for_claim_id($claim_id);

        if (count($files) == 0) {
            echo "<b>Bijlagen</b>";
        } else {
            echo "<b>Bijlagen</b><br>";
            foreach ($files as $file) {
                echo anchor($file->get_path_for_web(), $file->file_name, 'target="_blank"');
                echo '&nbsp;&nbsp;&nbsp;';
                echo anchor(
                        base_url() . 'user/answer/delete_attachment/' .
                        $claim_id . '/' . $form->form_id . '/' .
                        $file->file_id, '<i class="icon-trash"></i>');
                echo "<br>";
            }
        }
        ?>

        <br>
        <form method="post"
              action="<?php echo base_url() . "user/answer/upload_attachment/" . $claim_id; ?>"
              enctype="multipart/form-data">

            <input type="hidden" name="form_id" value="<?php echo $form->form_id; ?>" />

            <input type="file" class="input" name="attachment" /> <input
                type="submit" value="Toevoegen" class="button btn">
        </form>



        <?php
    }
    ?>



    <?php
    // show filled_in_honestly only if its the last page
    if (trim($categoty_id) == 'end' || $form->show_all_question_at_once == 1) {
        ?>

        <input type="checkbox" value="yes" name="filled_in_honestly" id="filled_in_honestly"> &nbsp;Ik heb het formulier naar waarheid ingevuld  <font color="red">*</font> <span id="error-text" style="color:red;"></span><br>
    <?php } ?>


    <?php
    if (trim($categoty_id) == 'end' || $form->show_all_question_at_once == 1) {
        ?>

        <br/>
        <table class="" style="background-color: white;">

            <tr>
                <td>
                    <?php
                    // Terug button shown at the last page, encl page
                    if (trim($categoty_id) == 'end' ||
                            $form->show_all_question_at_once == 1) {
                        ?> 
                        <form>
                            <input value="Terug" type="button" class="button btn"
                                   onclick="window.location.href='<?php
                echo base_url() . "user/answer/preview_form/" . $claim_id . "/" .
                $form->form_id . "/" . $cat_id;
                        ?>'" /> 

                        </form>		
                        <?php
                    }
                    ?>
                </td>

                <?php
                // if whole form is not to be shown at once then
                if ($form->show_all_question_at_once != 1) {
                    ?>
                    <td>
                        <!-- link for Opslaan button -->
                        <form method="post"
                              action="<?php echo base_url() . "user/answer/end_preview_save/" . $claim_id; ?>">
                            <input type="submit" value="Opslaan in concept" class="button btn " />
                        </form>
                    </td>
                    <td>
                        <!-- link for Opslaan and submit button -->
                        <form method="post"
                              action="<?php echo base_url() . "user/answer/end_preview_confirm_and_send/" . $claim_id; ?>">
                            <input type="submit" value="Bevestig en verstuur" class="button btn"
                                   onclick="return check_confirm()" />
                        </form>
                    </td>
                    <?php
                } else {
                    ?>


                    <td>
                        <!-- link for Opslaan button --> <script type="text/javascript">
                            function save_form()
                            {
                                document.forms['main_frm'].action.value = 'Opslaan in concept';		
                                document.getElementById("main_frm").submit();
                            }
                        </script>
                        <form method="get" action="#">
                            <input type="button" value="Opslaan in concept" onclick="save_form()"
                                   class="button btn " />
                        </form>

                    </td>
                    <td>
                        <!-- link for Opslaan button --> <script type="text/javascript">
                            function confirm_and_send()
                            {
                                document.forms['main_frm'].action.value = 'confirm';
                                if(check_confirm()==false)
                                {
                                    return false;
                                }		
                                else
                                {
                                    document.getElementById("main_frm").submit();
                                }
                                                                                                                        	
                            }
                        </script>
                        <form method="get" action="#">
                            <input type="button" value="Bevestig en verstuur"
                                   onclick="confirm_and_send()" class="button btn " />
                        </form>

                    </td>

                    <?php
                }
                ?>
            </tr>
        </table>


        <script type="text/javascript">
            function check_confirm()
            {
                if ( document.getElementById('filled_in_honestly').checked ==false) 
                {
                    document.getElementById('error-text').innerHTML = 'Dit veld is verplicht';
                    //alert('Please check "I Filled in form honestly"');
                    return false ;
                }
                else
                {
                    return true ;
                } 
            }
        </script>



        <!--  <a class="button btn pull-right"
                href="<?php echo base_url() . "user/list_forms" ?>"> Continue to forms
                List </a>  -->
        <?php
    }
    ?>

</div>

<br>
<br>
<br>
<br>

