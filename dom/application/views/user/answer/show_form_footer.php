<style type="text/css">
    .que_box{
        border:none !important;
        background:#fff !important;
    }
    h2 > span{
        font-size: 23px !important;
        padding: 0 5px 7px 0 !important;}
    /*    .que_box > h3{
            font-size: 18px !important;
        }*/
</style>
<h2><span><?php echo $form->form_name; ?></span></h2>
<div>
<!--    <h3><?php echo @$total_form_completed; ?>% form compleet</h3>-->
    <div class="progress progress-striped active">
        <div class="bar" style="width: <?php echo @$total_form_completed . '%'; ?>;"></div>
    </div>
</div>
<div class="que_box">
    <img class="exp_img"  src='<?php echo assets_url_img . "exclamation.png"; ?>' alt='exclamation' title='exclamation'>
    <p>
        <?php
        $form_closure_text = get_form_all_text($claim_id, $form->closure_text);
        echo $form_closure_text;
        ?>
    </p>
</div>
<div class="que_box">

    <table style="background: #fff;">
        <tr>
            <td>
                <?php
                $this->load->model('claims_files_model');
                $files = claims_files_model::get_claims_for_claim_id($claim_id);
                $check_file_error = $this->session->flashdata('file_errors');
                if (isset($check_file_error) && $check_file_error != '') {
                    echo '<p style="color: red;">' . $check_file_error . '</p>';
                }
                if (count($files) == 0) {
                    echo "<b>Bijlagen</b>";
                } else {
                    echo "<b>Bijlagen</b><br>";
                    foreach ($files as $file) {
                        echo anchor($file->get_path_for_web(), $file->file_name, 'target="_blank"');
                        echo '&nbsp;&nbsp;&nbsp;';
                        echo anchor(base_url() . 'user/answer/delete_attachment/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form->form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($file->file_id, '**DOM_CLAIM_DEVREPUBLIC**'), '<i class="icon-trash"></i>');
                        echo "<br>";
                    }
                    echo "<br>";
                }
                ?>
                <form method="post" action="<?php echo base_url() . "user/answer/upload_attachment_once/" . $this->encrypt->encode($claim_id, 'DOM_CLAIM_DEVREPUBLIC'); ?>" enctype="multipart/form-data">
                    <input type="hidden" name="prev_url" value="<?php echo $prev_url; ?>" />
                    <input type="hidden" name="form_id" value="<?php echo $form->form_id; ?>" />
                    <input type="file" class="input" name="attachment" /> <input type="submit" value="Toevoegen" class="btn btn-large btn-primary">
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <label class="checkbox">
                    <input type="checkbox" value="yes" name="filled_in_honestly" id="filled_in_honestly"> Naar waarheid ingevuld en gecontroleerd. <font color="red">*</font> 
                    <span id="error-text" style="color: red;"></span><br>
                </label>
            </td>
        </tr>
        <tr>
            <td>
                De met een <font color="red">* </font> gemarkeerde velden zijn verplicht.

            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <div class="row-fluid">
                    <?php
                    $check = $this->session->flashdata('flash_prev_url');
                    $this->session->keep_flashdata('flash_prev_url');
                    if ($check != '') {
                        $url = $check;
                    } else if (isset($prev_url)) {
                        $url = $prev_url;
                    }
                    if (isset($url)) {
                        ?>
                        <div class="btn_form">
                            <a href="<?php echo str_replace('PreviewQuestionListener', 'preview_question', $url); ?>" class="btn btn-primary btn-large">Terug</a>
                        </div>
                    <?php } else { ?>
                        <div class="btn_form">
                            <a href="<?php echo base_url() . 'user/answer/preview_question_start/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form->form_id, '**DOM_CLAIM_DEVREPUBLIC**'); ?>" class="btn btn-primary btn-large">Terug</a>
                        </div>
                    <?php } ?>
                    <!-- link for Opslaan button -->
                    <form method="post" id="form_one_by_one" action="<?php echo base_url() . "user/answer/end_preview_one_by_one/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**'); ?>" class=" pull-left multiform-margin">
                        <div class="btn_form">
                            <input type="button" value="Opslaan in concept" class="btn btn-primary btn-large" onclick="return check_confirm()" />
                        </div>
                    </form>
                    <!-- link for Opslaan and submit button -->
                    <form method="post" id="form_one_by_one1" action="<?php echo base_url() . "user/answer/end_preview_confirm_and_send_one_by_one/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**'); ?>" class="pull-left multiform-margin">
                        <div class="btn_form">
                            <input type="button" value="Bevestig en verstuur" class="btn btn-primary btn-large" onclick="return check_confirm1()" />
                        </div>
                    </form>
                </div>
            </td>
        </tr>

    </table>

</div>

<script type="text/javascript">
    function check_confirm()
    {
        if ( document.getElementById('filled_in_honestly').checked ==false) 
        {
            document.getElementById('error-text').innerHTML = 'Dit veld is verplicht';
            document.getElementById('remove-star').innerHTML = '';
            //alert('Please check "I Filled in form honestly"');
            return false ;
        }
        else
        {
            $('#form_one_by_one').submit();
        } 
    }
</script>
<script type="text/javascript">
    function check_confirm1()
    {
        if ( document.getElementById('filled_in_honestly').checked ==false) 
        {
            document.getElementById('error-text').innerHTML = 'Dit veld is verplicht';
            document.getElementById('remove-star').innerHTML = '';
            //alert('Please check "I Filled in form honestly"');
            return false ;
        }
        else
        {
            $('#form_one_by_one1').submit();
        } 
    }
</script>
