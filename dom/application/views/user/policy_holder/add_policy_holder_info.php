 <div class="row-fluid">
	<p class="">
	<?php 
	    echo $registratin_text;
	 ?>
</p>
<br/>
</div>

<div>
	<form action="<?php echo base_url() . 'user/policy_holder_info/editListener/'.$randid; ?>"
		method="post" enctype="multipart/form-data">

		<table width="100%" class="table table-striped table-condensed">
			<tbody>
				
				<tr>
					<td>Voornaam:<span class="mandatory">*</span>
					</td>
					<td><input class="input" type="text" name="f_name"
						value="<?php echo htmlentities((set_value('f_name') != '') ? set_value('f_name') : '' ); ?>" /></td>
					<td><?php echo form_error('f_name'); ?></td>
				</tr>
                <tr>
					<td>Tussenvoegsel</td>
					<td><input class="input" type="text" name="m_name"
						value="<?php echo htmlentities((set_value('m_name') != '') ? set_value('m_name') : '' ); ?>" /></td>
					<td><?php echo form_error('m_name'); ?></td>
				</tr>
				<tr>
					<td>Achternaam:<span class="mandatory">*</span>
					</td>
					<td><input class="input" type="text" name="l_name"
						value="<?php echo htmlentities((set_value('l_name') != '') ? set_value('l_name') : '' ); ?>" /></td>
					<td><?php echo form_error('l_name'); ?></td>
				</tr>
					<tr>
				<td> E-mailadres<span class="mandatory">*</span></td>
				<td><input type="text" autocomplete="off" name="email"
					value="<?php
    echo set_value('email') != '' ? set_value('email') : isset(
            $policy_holder->email) ? $policy_holder->email : '';
    ?>" /></td>
				<td><?php echo form_error('email'); ?></td>
			</tr>
                <tr>
					<td>Adres:
					</td>
					<td><input class="input" type="text" name="address"
						value="<?php echo htmlentities((set_value('address') != '') ? set_value('address') : '' ); ?>" /></td>
					<td><?php echo form_error('address'); ?></td>
				</tr>
				<tr>
					<td>Postcode:
					</td>
					<td><input class="input" type="text" name="zipcode"
						value="<?php echo htmlentities((set_value('zipcode') != '') ? set_value('zipcode') : '' ); ?>" /></td>
					<td><?php echo form_error('zipcode'); ?></td>
				</tr>
				 <tr>
					<td>Telefoonnummer:
					</td>
					<td><input class="input" type="text" name="p_number"
						value="<?php echo htmlentities((set_value('p_number') != '') ? set_value('p_number') : '' ); ?>" /></td>
					<td><?php echo form_error('p_number'); ?></td>
				</tr>
				 <tr>
					<td>Zakelijk telefoonnummer:
					</td>
					<td><input class="input" type="text" name="b_number"
						value="<?php echo htmlentities((set_value('b_number') != '') ? set_value('b_number') : '' ); ?>" /></td>
					<td><?php echo form_error('b_number'); ?></td>
				</tr>
				 <tr>
					<td>Mobiele telefoonnummer:
					</td>
					<td><input class="input" type="text" name="m_number"
						value="<?php echo htmlentities((set_value('m_number') != '') ? set_value('m_number') : '' ); ?>" /></td>
					<td><?php echo form_error('m_number'); ?></td>
				</tr>
				<tr>
					<td>Wachtwoord:<span class="mandatory">*</span>
					</td>
					<td><input class="input" type="password" name="passward"
						value="<?php echo htmlentities((set_value('passward') != '') ? set_value('passward') : '' ); ?>" /></td>
					<td><?php echo form_error('passward'); ?></td>
				</tr>
				<tr>
					<td>Wachtwoord (bevestiging):<span class="mandatory">*</span>
					</td>
					<td><input class="input"  type="password" name="c_passward"
						value="<?php echo htmlentities((set_value('c_passward') != '') ? set_value('c_passward') : '' ); ?>" /></td>
					<td><?php echo form_error('c_passward'); ?></td>
				</tr>
				
				
				<tr>
					<td colspan="3"><button type="submit" class="btn">Opslaan</button>
						&nbsp;&nbsp;</td>

				</tr>

				<tr>
					<td colspan="3">Velden die gemarkeerd zijn met een <span
						class="mandatory">*</span> zijn verplicht.
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</div>
<br />
<br />
