<h2 class="title-margin"> <span>Details Relatie</span></h2>
<div>
    <form action="<?php echo base_url() . 'user/policy_holder_info/editpolicyListener/' . $policy_holder->policy_holder_id; ?>"
          method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-striped">
            <tbody>
                <tr>
                    <td >Actief:</td>
                    <td ><input class="input" type="checkbox" name="status"
                                value="A"
                                <?php
                                if ($policy_holder->status == 'A') {
                                    echo 'checked=checked';
                                }
                                ?> /></td>

                    <td></td>
                </tr>
                <tr>
                    <td>Voornaam:
                    </td>
                    <td><input class="input pull-left" type="text" name="f_name"
                               value="<?php
                                if (isset($policy_holder->first_name) && $policy_holder->first_name !== '') {
                                    echo $policy_holder->first_name;
                                } else {
                                    echo htmlentities((set_value('f_name') != '') ? set_value('f_name') : '' );
                                }
                                ?>" />
                               <?php
                               if (isset($policy_holder->old_first_name) && $policy_holder->old_first_name != NULL) {
                                   ?>
                            <i class="icon-exclamation-new pull-left margin-exlamentary"></i>
                        <?php } ?>
                    </td>
                    <td><?php echo form_error('f_name'); ?></td>
                </tr>
                <tr>
                    <td>Tussenvoegsel:
                    </td>
                    <td><input class="input pull-left" type="text" name="m_name"
                               value="<?php
                        if (isset($policy_holder->middle_name) && $policy_holder->middle_name !== '') {
                            echo $policy_holder->middle_name;
                        } else {
                            echo htmlentities((set_value('m_name') != '') ? set_value('m_name') : '' );
                        }
                        ?>" />
                               <?php
                               if (isset($policy_holder->old_middle_name) && $policy_holder->old_middle_name != NULL) {
                                   ?>
                            <i class="icon-exclamation-new pull-left margin-exlamentary"></i>
                        <?php } ?>
                    </td>
                    <td><?php echo form_error('m_name'); ?></td>
                </tr>
                <tr>
                    <td>Achternaam:
                    </td>
                    <td><input class="input pull-left" type="text" name="l_name"
                               value="<?php
                        if (isset($policy_holder->last_name) && $policy_holder->last_name !== '') {
                            echo $policy_holder->last_name;
                        } else {
                            echo htmlentities((set_value('l_name') != '') ? set_value('l_name') : '' );
                        }
                        ?>" />
                               <?php
                               if (isset($policy_holder->old_last_name) && $policy_holder->old_last_name != NULL) {
                                   ?>
                            <i class="icon-exclamation-new pull-left margin-exlamentary"></i>
                        <?php } ?>
                    </td>
                    <td><?php echo form_error('l_name'); ?></td>
                </tr>
                <tr>
                    <td>Adres:
                    </td>
                    <td><input class="input pull-left" type="text" name="address"
                               value="<?php
                        if (isset($policy_holder->address) && $policy_holder->address !== '') {
                            echo $policy_holder->address;
                        } else {
                            echo htmlentities((set_value('address') != '') ? set_value('address') : '' );
                        }
                        ?>" />
                               <?php
                               if (isset($policy_holder->old_address) && $policy_holder->old_address != NULL) {
                                   ?>
                            <i class="icon-exclamation-new pull-left margin-exlamentary"></i>
                        <?php } ?>
                    </td>
                    <td><?php echo form_error('address'); ?></td>
                </tr>
                <tr>
                    <td>E-mailadres:<span class="mandatory">*</span></td>
                    <td><input type="text" class="input pull-left" name="email" value="<?php echo set_value('email') != '' ? set_value('email') : isset($policy_holder->email) ? $policy_holder->email : '';
                        ?>" />
                               <?php
                               if (isset($policy_holder->old_email) && $policy_holder->old_email != NULL) {
                                   ?>
                            <i class="icon-exclamation-new pull-left margin-exlamentary"></i>
                        <?php } ?>
                    </td>
                    <td><?php echo form_error('email'); ?></td>
                </tr>
                <tr>
                    <td>Postcode:
                    </td>
                    <td><input class="input pull-left" type="text" name="zipcode"
                               value="<?php
                        if (isset($policy_holder->zipcode) && $policy_holder->zipcode !== '') {
                            echo $policy_holder->zipcode;
                        } else {
                            echo htmlentities((set_value('zipcode') != '') ? set_value('zipcode') : '' );
                        }
                        ?>" />
                               <?php
                               if (isset($policy_holder->old_zipcode) && $policy_holder->old_zipcode != NULL) {
                                   ?>
                            <i class="icon-exclamation-new pull-left margin-exlamentary"></i>
                        <?php } ?>
                    </td>
                    <td><?php echo form_error('zipcode'); ?></td>
                </tr>
                <tr>
                    <td>Telefoonnummer:
                    </td>
                    <td><input class="input" type="text" name="p_number"
                               value="<?php
                        if (isset($policy_holder->private_number) && $policy_holder->private_number !== '') {
                            echo $policy_holder->private_number;
                        } else {
                            echo htmlentities((set_value('p_number') != '') ? set_value('p_number') : '' );
                        }
                        ?>" /></td>
                    <td><?php echo form_error('p_number'); ?></td>
                </tr>
                <tr>
                    <td>Zakelijk Telefoonnummer:
                    </td>
                    <td><input class="input" type="text" name="b_number"
                               value="<?php
                               if (isset($policy_holder->business_number) && $policy_holder->business_number !== '') {
                                   echo $policy_holder->business_number;
                               } else {
                                   echo htmlentities((set_value('b_number') != '') ? set_value('b_number') : '' );
                               }
                        ?>" /></td>
                    <td><?php echo form_error('b_number'); ?></td>
                </tr>
                <tr>
                    <td>Mobiele Telefoonnummer:
                    </td>
                    <td><input class="input" type="text" name="m_number"
                               value="<?php
                               if (isset($policy_holder->mobile_number) && $policy_holder->mobile_number !== '') {
                                   echo $policy_holder->mobile_number;
                               } else {
                                   echo htmlentities((set_value('m_number') != '') ? set_value('m_number') : '' );
                               }
                        ?>" /></td>
                    <td><?php echo form_error('m_number'); ?></td>
                </tr>
        <!--	<tr>
                        <td>Password:<span class="mandatory">*</span>
                        </td>
                        <td><input class="input" type="password" name="passward"
                                value="<?php echo htmlentities((set_value('passward') != '') ? set_value('passward') : '' ); ?>" /></td>
                        <td><?php echo form_error('passward'); ?></td>
                </tr>
                <tr>
                        <td>Conform Password:<span class="mandatory">*</span>
                        </td>
                        <td><input class="input"  type="password" name="c_passward"
                                value="<?php echo htmlentities((set_value('c_passward') != '') ? set_value('c_passward') : '' ); ?>" /></td>
                        <td><?php echo form_error('c_passward'); ?></td>
                </tr>
                -->

                <tr>
                    <td colspan="3">
                        <button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                    </td>

                <tr>


                <tr>
                    <td colspan="3">Velden die gemarkeerd zijn met een <span
                            class="mandatory">*</span> zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
