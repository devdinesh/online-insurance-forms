<html>
<body>
	<form
		action="<?php echo base_url() . "user/authenticate/check_policy_holder_Login"; ?>"
		method="post">
		<table border="0" width="60%">
		    <tr>
					<td>E-mailadres:<span class="mandatory">*</span>
					</td>
					<td><input class="input" type="text" name="p_email"
						value="<?php echo htmlentities((set_value('p_email') != '') ? set_value('p_email') : '' ); ?>" /></td>
					<td><?php echo form_error('p_email'); ?></td>
		   </tr>
			<tr>
					<td>Password:<span class="mandatory">*</span>
					</td>
					<td><input class="input" type="password" name="password"
						value="<?php echo htmlentities((set_value('password') != '') ? set_value('password') : '' ); ?>" /></td>
					<td><?php echo form_error('password'); ?></td>
		   </tr>
		   <tr>
					<td colspan="3"><button type="submit" class="btn">Login</button></td>
		  </tr>
		</table>
	</form>
</body>
</html>