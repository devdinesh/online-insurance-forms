<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo $title; ?> | Digitaal op Maat </title>

        <link rel="stylesheet" type="text/css"
              href="<?php echo assets_url_css; ?>base.css" />
        <link rel="stylesheet" type="text/css"
              href="<?php echo assets_url_css; ?>bootstrap.css" />
        <link rel="stylesheet" type="text/css"
              href="<?php echo assets_url_css; ?>bootstrap-responsive.css" />
        <script src="<?php echo assets_url_js; ?>/jquery-1.7.1.min.js"></script>
        <script src="<?php echo assets_url_js; ?>/bootstrap-dropdown.js"></script>

        <!-- Data table -->
        <link href="<?php echo assets_url_css; ?>demo_table_jui.css"
              rel="stylesheet" type="text/css" />
        <link href="<?php echo assets_url_css; ?>demo_page.css" rel="stylesheet"
              type="text/css" />
        <link href="<?php echo assets_url_css; ?>demo_table.css"
              rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript"
        src="<?php echo assets_url_js; ?>jquery-1.8.3.js"></script>
        <script type="text/javascript" language="javascript"
        src="<?php echo assets_url_js; ?>jquery.dataTables.js"></script>
        <!-- End Data Table -->
        <script type="text/javascript" language="javascript"
        src="<?php echo assets_url_js; ?>bootstrap-tooltip.js"></script>


        <meta name="google-translate-customization"
              content="2227024e11d5699-516aaa854de6b7d6-g515b8caffd13378a-30"></meta>
        <!-- includes for javascript ui -->


        <link href="<?php echo assets_url_css; ?>jquery-ui.css" rel="stylesheet"
              type="text/css" />
        <script type="text/javascript" language="javascript"
        src="<?php echo assets_url_js; ?>jquery-ui.js"></script>

        <!-- end includes for javascript ui -->
    </head>
    <body>


    <body>
        <div id="wrapper" class="container">
            <div class="container">
                <div id="top">
                    <div class="row-fluid ">
                        <div class="span6 ">
                            <a href="#"><img src="<?php echo assets_url_img; ?>logo.jpg"
                                             width="103" height="120" alt="logo" /></a>
                        </div>
                    </div>
                </div>
                <div class="navbar-inner container">
                    <div id="navbar-example" class="navbar navbar-static">
                        <div class="navbar-inner">
                            <div class="" style="width: auto;">
                                <ul class="nav" role="navigation">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="main">
                        <?php if ($this->session->flashdata('success') != '') { ?>
                            <tr>
                                <td colspan="3">
                                    <div class="alert alert-success">
                                        <a class="close" data-dismiss="alert"
                                           href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('success'); ?>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if ($this->session->flashdata('error') != '') { ?>
                            <tr>
                                <td colspan="3">
                                    <div class="alert alert-error">
                                        <a class="close" data-dismiss="alert"
                                           href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('error'); ?>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        <div class="row-fluid">
                            <h2 class="text-center">Polishouders Wachtwoord Vergeten</h2>
                            <form action="<?php echo base_url() . "user/authenticate/forgotPasswordListener"; ?>" method="post">
                                <table class="table-condensed" style="background-color: #fff; width: 60%; margin-left: 31%;">
                                    <tr>
                                        <td width="20%">E-mailadres:<span class="mandatory">*</span>
                                        </td>
                                        <td width="20%"><input class="input" type="email" name="p_email" value="<?php echo htmlentities((set_value('p_email') != '') ? set_value('p_email') : '' ); ?>" required="required"/></td>
                                        <td><?php echo form_error('p_email'); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <a href="<?php echo base_url() . 'polishouders'; ?>" class="btn pull-right" style="margin-left: 5px;">Terug</a>
                                            <button type="submit" class="btn pull-right">Verstuur E-mail</button>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <div id="footer">
                        <div class="footer text-center">
                            <a href="#">About Us</a> | <a
                                href="<?php echo "#" /* base_url() . "admin/contact_me" */ ?>">Contact</a>
                        </div>
                    </div>
                </div>
            </div>

    </body>
</html>

