<h2>Forgot Password</h2>
<form
	action="<?php echo base_url(); ?>admin/authenticate/forgotpassword_listener"
	method="post">
	<table border="0" width="50%">

        <?php
        if (isset($login_error))
        {
            echo "<tr><td colspan=\"3\"> " .
                     htmlentities($login_error, ENT_QUOTES) . "</td></tr>";
        }
        ?>
        <tr>
			<td width="10%">Mail :</td>
			<td width="15%"><input type="email" name="email"></td>
			<td><?php echo form_error('email'); ?></td>
		</tr>
		<tr>
			<td colspan="3">
				<button type="submit" class="btn">Send Password</button>
			</td>
		</tr>
	</table>