<!DOCTYPE html>
<html>
<head>
<title> <?php echo $title; ?> | Digitaal op Maat </title>

<link rel="stylesheet" type="text/css"
	href="<?php echo assets_url_css; ?>base.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo assets_url_css; ?>bootstrap.css" />
<link rel="stylesheet" type="text/css"
	href="<?php echo assets_url_css; ?>bootstrap-responsive.css" />
<script src="<?php echo assets_url_js; ?>/jquery-1.7.1.min.js"></script>
<script src="<?php echo assets_url_js; ?>/bootstrap-dropdown.js"></script>

<!-- Data table -->
<link href="<?php echo assets_url_css; ?>demo_table_jui.css"
	rel="stylesheet" type="text/css" />
<link href="<?php echo assets_url_css; ?>demo_page.css" rel="stylesheet"
	type="text/css" />
<link href="<?php echo assets_url_css; ?>demo_table.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript"
	src="<?php echo assets_url_js; ?>jquery-1.8.3.js"></script>
<script type="text/javascript" language="javascript"
	src="<?php echo assets_url_js; ?>jquery.dataTables.js"></script>
<!-- End Data Table -->
<script type="text/javascript" language="javascript"
	src="<?php echo assets_url_js; ?>bootstrap-tooltip.js"></script>


<meta name="google-translate-customization"
	content="2227024e11d5699-516aaa854de6b7d6-g515b8caffd13378a-30"></meta>
<!-- includes for javascript ui -->


<link href="<?php echo assets_url_css; ?>jquery-ui.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" language="javascript"
	src="<?php echo assets_url_js; ?>jquery-ui.js"></script>

<!-- end includes for javascript ui -->
</head>
<body>


<body>
	<div id="wrapper" class="container">
		<div class="container">
			<div id="top">
				<div class="row-fluid">
					<div class="span4 ">
						<a href="#"><img src="<?php echo assets_url_img; ?>logo.jpg"
							width="103" height="120" alt="logo" /></a>
					</div>
					<div class="span4" id="google_translate_element"></div>
					<div class="span4 text-right">
                            <?php
                            $super_admin = $this->session->userdata(
                                    'super_admin');
                            echo $super_admin['super_admin_first_name'] . ' ' .
                                     $super_admin['super_admin_last_name'];
                            ?> | 
                            <a
							href="<?php echo base_url() . 'super_admin/authenticate/logout' ?>">
							Logout</a>
					</div>
				</div>
			</div>
		</div>
		<div class="navbar-inner container">
			<div id="navbar-example" class="navbar navbar-static">
				<div class="navbar-inner">
					<div class="" style="width: auto;">
						<ul class="nav" role="navigation">
							<li role="presentation"><a role="menuitem" tabindex="0"
								href="<?php echo base_url() . "super_admin/dashboard" ?>"> Home
							</a></li>
							<li role="presentation"><a role="menuitem" tabindex="0"
								href="<?php echo base_url() . "super_admin/clients" ?>"> Client
							</a></li>
							<li role="presentation"><a role="menuitem" tabindex="0"
								href="<?php echo base_url() . "super_admin/users" ?>"> Users </a></li>
							<li role="presentation"><a role="menuitem" tabindex="0"
								href="<?php echo base_url() . "super_admin/subscriptions" ?>">
									Subscriptions </a></li>
							<li role="presentation"><a role="menuitem" tabindex="0"
								href="<?php echo base_url() . "super_admin/invoices" ?>">
									Invoices </a></li>
							<li role="presentation"><a role="menuitem" tabindex="0"
								href="<?php echo base_url() . "super_admin/forms" ?>"> Forms </a></li>
							<li class="dropdown"><a href="#" id="drop2" role="button"
								class="dropdown-toggle" data-toggle="dropdown">Admin<b
									class="caret"></b></a>
								<ul class="dropdown-menu" role="menu" aria-labelledby="drop2">
									<li role="presentation"><a role="menuitem" tabindex="-1"
										href="<?php echo base_url() . "super_admin/admin/mail" ?>">
											Mail template </a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1"
										href="<?php echo base_url() . "super_admin/super_admin_mail_template" ?>">
											Super Admin Email Templates </a></li>

									<li role="presentation"><a role="menuitem" tabindex="-1"
										href="<?php echo base_url() . "super_admin/admin/subscription" ?>">
											Subscription kinds </a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1"
										href="<?php echo base_url() . "super_admin/admin/setting" ?>">
											Setting </a></li>
									<li role="presentation"><a role="menuitem" tabindex="-1"
										href="<?php echo base_url() . "super_admin/admin/texts" ?>">
											Texts </a></li>
								</ul></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="main">
				<!-- actual page content begins -->
                    <?php $this->load->view(get_page_name($page_name)); ?>
                    <!-- actual page content ends -->
				<br>
			</div>
			<div id="footer">
				<div class="footer text-center">
					<a href="#">About Us</a> | <a href="#">Contact</a>
				</div>
			</div>
		</div>

</body>
</html>
