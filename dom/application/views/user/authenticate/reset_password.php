<html>
<head>
<title>
            <?php echo $title; ?>
        </title>
</head>

<body>
	<form
		action="<?php echo base_url() . "super_admin/ResetPassword/" . $randid; ?>"
		method="post">
		<table border="0" width="60%">
			<tr>
				<td colspan="3"><h3>Reset Password</h3></td>
			</tr>

                <?php
                if (isset($login_error))
                {
                    echo "<tr><td colspan=\"3\"> " .
                             htmlentities($login_error, ENT_QUOTES) .
                             "</td></tr>";
                }
                ?>

                <tr>
				<td width="14%">New Password :</td>
				<td width="15%"><input type="password" name="new_password"></td>
				<td><?php echo form_error('new_password'); ?></td>
			</tr>

			<tr>
				<td>Confirm Password :</td>
				<td><input type="password" name="confirm_password"></td>
				<td><?php echo form_error('confirm_password'); ?></td>
			</tr>

			<tr>
				<td colspan="3">
					<button type="submit">Reset Password</button>
				</td>
			</tr>
			<tr>
				<td colspan="3"><a href="<?php echo base_url(); ?>super_admin">Back
						to Login Page</a></td>
			</tr>
		</table>
	</form>
</body>
</html>
