<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <title>Feddback</title>
        <link href="<?php echo base_url(); ?>assets/css/feedback.css" rel="stylesheet" type="text/css" />
        <script	src="<?php echo base_url() ?>assets/js/jquery-1.7.1.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            function onsubmit_check(){
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                if($('#feed_email').val()==''){
                    $('#alert1').html('* Vul je e-mailadres in.');
                    return false;
                }else if(reg.test($('#feed_email').val()) == false){
                    $('#alert1').html('* Please Enter Valid Email.');
                    return false;
                }
                else{ return true;}
            }
        </script>
    </head>
    <body>
        <form id="Mform" name="Mform" enctype="multipart/form-data" method="post" onsubmit="return onsubmit_check()" action="<?php echo base_url() . 'admin/feedback/save_feedback_2'; ?>">

            <table cellspacing="0" cellpadding="0" border="0" style="width: 485px;" id="maintable">
                <tbody>
                    <tr>
                        <td align="right" valign="bottom" class="titelwit" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table cellspacing="0" cellpadding="0" border="0"
                                   style="width: 485px;" id="maintable">
                                <tbody>
                                    <tr>
                                        <td height="110" background="<?php echo base_url() . 'assets/images/feedback/table_top_l.png'; ?>" width="42" class="groentableN_top_l"></td>
                                        <td background="<?php echo base_url() . 'assets/images/feedback/table_top_m_skoda.png'; ?>" align="left" class="groentableN_top_m">
                                            <img height="25" width="131" src="<?php echo base_url() . 'assets/images/feedback/feedback.png'; ?>" /> </td>
                                        <td background="<?php echo base_url() . 'assets/images/feedback/table_top_m_close.png'; ?>" width="43" class="groentableN_top_close" valign="top">
                                            <a href="#" style="position:absolute; text-align:center; margin-top:30px; margin-left:17px;" onclick="parent.close_feedback_panel()" class="close"><img height="9" width="9" src="<?php echo base_url() . 'assets/images/feedback/close.png'; ?>" /></a>
                                        </td>
                                        <td background="<?php echo base_url() . 'assets/images/feedback/table_top_r_skoda.png' ?>" width="43" class="groentableN_top_r"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="42" background="http://pon.mopinion.nl/img/table_mid_l.png" style="font-size:9pt;"> </td>
                        <td align="left" width="400" valign="top" bgcolor="#FFFFFF" id="mainTD" style="font-size:9pt;">
                            Hartelijk dank voor het delen van je feedback of het stellen van je vraag. Wil je op de hoogte gehouden worden van je feedback of een reactie op je vraag? 
                            <br><br>
                                    Geef dan je e-mailadres op:
                                    <input type="text" name="feed_email" id="feed_email" style="width:250px;" onfocus="document.getElementById('alert1').innerHTML = ''" class="formfield" >
                                        <div style="color:red;" id="alert1"></div>

                                        </td> 
                                        <td width="43" background="<?php echo base_url() . 'assets/images/feedback/table_mid_r.png" class="groentableN_rowW_r'; ?>"></td> 
                                        </tr>
                                        <tr>
                                            <td background="<?php echo base_url() . 'assets/images/feedback/table_mid_l.png" width="42" class="groentableN_pijl_l'; ?>">&nbsp;</td>
                                            <td height="27" bgcolor="#FFFFFF" align="right" valign="bottom" class="groentableN_rowW_m">
                                                <div class="streepjes" id="streepjes3"></div>
                                                <table width="100%">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <div style="text-align:left; display:none;" id="buttonBottom" class="remarkPanel">
                                                                    <input type="submit" alt="" class="but_grijspon" />
                                                                </div>
                                                                <div style="text-align: left;"  id="buttonBottom">
                                                                    <input type="submit" class="but_grijspon" />
                                                                </div>
                                                            </td>
                                                            <td>

                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td background="<?php echo base_url() . 'assets/images/feedback/table_mid_r.png'; ?>"
                                                class="groentableN_rowW_r"></td>
                                        </tr>
                                        <tr>
                                            <td height="45" background="<?php echo base_url() . 'assets/images/feedback/table_bot_l.png" class="groentableN_bottom_l'; ?>"></td>
                                            <td background="<?php echo base_url() . 'assets/images/feedback/table_bot_m.png'; ?>" class="groentableN_bottom_m"></td>
                                            <td background="<?php echo base_url() . 'assets/images/feedback/table_bot_r.png'; ?>" class="groentableN_bottom_r"></td>
                                        </tr>
                                        </tbody>
                                        </table>
                                        <input type="hidden" name="feed_remark" id="feed_remark" value="<?php echo $feed_remark; ?>" />
                                        <input type="hidden" name="feed_rating" id="feed_rating" value="<?php echo $feed_rating; ?>" />
                                        <input type="hidden" name="feed_cat_id" id="feed_cat_id" value="<?php echo $feed_cat_id; ?>" />
                                        </form>

                                        <script type="text/javascript">
                                            var starClickFlg=true;
                                            $(document).ready(function(){
                                            });
                                        </script>
                                        </body>
                                        </html>