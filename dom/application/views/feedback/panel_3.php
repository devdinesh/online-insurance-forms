<style type="text/css">
    .sluiten {
        color: #FFFFFF; font-size: 18px; text-align: center;
        vertical-align: middle; text-shadow: 0 -1px 0 #666;
    }

    .sluiten {
        font-family: SkodaProRegular; display: block; width: 100px;
        height: 25px;
        background:url(<?php echo base_url().'assets/images/feedback/but_sluiten.png';?>) no-repeat; text-decoration: none; color: #FFFFFF; font-size: 0px; border:none; cursor:pointer;
        text-align: center; vertical-align: middle; padding-top: 5px;
    }

    .sluiten:hover {
        background-position: -100px 0; text-decoration: none;
    }

    .sluiten:visited {
        color: #FFF; text-decoration: none;
    }
</style>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <title>Feddback</title>
        <link href="<?php echo base_url(); ?>assets/css/feedback.css" rel="stylesheet" type="text/css" />
        <script	src="<?php echo base_url() ?>assets/js/jquery-1.7.1.min.js" type="text/javascript"></script>
    </head>
    <body>
        <table cellspacing="0" cellpadding="0" border="0" style="width: 485px;" id="maintable">
            <tbody>
                <tr>
                    <td align="right" valign="bottom" class="titelwit" colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table cellspacing="0" cellpadding="0" border="0"
                               style="width: 485px;" id="maintable">
                            <tbody>
                                <tr>
                                    <td height="110" background="<?php echo base_url() . 'assets/images/feedback/table_top_l.png'; ?>" width="42" class="groentableN_top_l"></td>
                                    <td background="<?php echo base_url() . 'assets/images/feedback/table_top_m_skoda.png'; ?>" align="left" class="groentableN_top_m">
                                        <img height="25" width="131" src="<?php echo base_url() . 'assets/images/feedback/feedback.png'; ?>" /> </td>
                                    <td background="<?php echo base_url() . 'assets/images/feedback/table_top_m_close.png'; ?>" width="43" class="groentableN_top_close" valign="top">
                                        <a href="#" style="position:absolute; text-align:center; margin-top:30px; margin-left:17px;" onclick="parent.close_feedback_panel()" class="close"><img height="9" width="9" src="<?php echo base_url() . 'assets/images/feedback/close.png'; ?>" /></a>
                                    </td>
                                    <td background="<?php echo base_url() . 'assets/images/feedback/table_top_r_skoda.png'; ?>" width="43" class="groentableN_top_r"></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="42" background="<?php echo base_url() . 'assets/images/feedback/table_mid_l.png'; ?>" style="font-size:9pt;"> </td>
                    <td align="left" width="400" valign="top" bgcolor="#FFFFFF" id="mainTD" style="font-size:9pt;">
                        Hartelijk dank voor het geven van je feedback op www.digitaalopmaat.nl. Indien nodig nemen we zo spoedig mogelijk contact met je op.
                    </td> 
                    <td width="43" background="<?php echo base_url() . 'assets/images/feedback/table_mid_r.png" class="groentableN_rowW_r'; ?>"></td> 
                </tr>
                <tr>
                    <td background="<?php echo base_url() . 'assets/images/feedback/table_mid_l.png" width="42" class="groentableN_pijl_l'; ?>">&nbsp;</td>
                    <td height="27" bgcolor="#FFFFFF" align="right" valign="bottom" class="groentableN_rowW_m">
                        <div class="streepjes" id="streepjes3"></div>
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="text-align: left;"  id="buttonBottom">
                                            <a class="close sluiten" onclick="parent.close_feedback_panel()" style="text-align:center; margin-top:6px; margin-left:17px;" href="#">&nbsp;</a>
                                        </div>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td background="<?php echo base_url() . 'assets/images/feedback/table_mid_r.png'; ?>"
                        class="groentableN_rowW_r"></td>
                </tr>
                <tr>
                    <td height="45" background="<?php echo base_url() . 'assets/images/feedback/table_bot_l.png'; ?>" class="groentableN_bottom_l"></td>
                    <td background="<?php echo base_url() . 'assets/images/feedback/table_bot_m.png'; ?>" class="groentableN_bottom_m"></td>
                    <td background="<?php echo base_url() . 'assets/images/feedback/table_bot_r.png'; ?>" class="groentableN_bottom_r"></td>
                </tr>
            </tbody>
        </table>
        <script type="text/javascript">
            var starClickFlg=true;
            $(document).ready(function(){
            });
 
        </script>
    </body>
</html>
