<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <title>Feddback</title>
        <link href="<?php echo base_url(); ?>assets/css/feedback.css" rel="stylesheet" type="text/css" />
        <script	src="<?php echo base_url() ?>assets/js/jquery-1.7.1.min.js" type="text/javascript"></script>
        <script type="text/javascript">

            function onsubmit_check(){
                if($('#feed_remark').val()==''){
                    $('#alert1').html('* Enter remarks.');
                    return false;
                }else return true;
            }
        </script>
    </head>
    <body>
        <form id="feedform" name="feedform" method="post" onsubmit="return onsubmit_check()" action="<?php echo base_url() . 'admin/feedback/save_feedback_1'; ?>">
            <table cellspacing="0" cellpadding="0" border="0" width="530" id="maintable">
                <tbody>
                    <tr>
                        <td align="right" valign="bottom" class="titelwit" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table cellspacing="0" cellpadding="0" border="0" width="530" id="maintable">
                                <tbody>
                                    <tr>
                                        <td height="110" background="<?php echo base_url() . 'assets/images/feedback/table_top_l.png'; ?>" width="42" class="groentableN_top_l"></td>
                                        <td background="<?php echo base_url() . 'assets/images/feedback/table_top_m_skoda.png'; ?>" align="left" class="groentableN_top_m">
                                            <img height="25" width="131" src="<?php echo base_url() . 'assets/images/feedback/feedback.png'; ?>" /> </td>
                                        <td background="<?php echo base_url() . 'assets/images/feedback/table_top_m_close.png'; ?>" width="43" class="groentableN_top_close" valign="top">
                                            <a href="#" style="position:absolute; text-align:center; margin-top:30px; margin-left:17px;" onclick="parent.close_feedback_panel()" class="close"><img height="9" width="9" src="<?php echo base_url() . 'assets/images/feedback/close.png'; ?>" /></a>
                                        </td>
                                        <td background="<?php echo base_url() . 'assets/images/feedback/table_top_r_skoda.png'; ?>" width="43" class="groentableN_top_r"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="<?php echo base_url() . 'assets/images/feedback/table_mid_l.png'; ?>"
                            width="42" class="groentableN_pijl_l"></td>
                        <td bgcolor="#FFFFFF" align="left" width="445" valign="top"
                            id="mainTD" class="groentableN_rowW_m">
                            <table cellspacing="0" cellpadding="0" border="0" width="365" style="margin-left:40px;">
                                <tbody>
                                    <tr>
                                        <td height="35" align="center" valign="middle"><img height="32" width="32" alt="" src="<?php echo base_url() . 'assets/images/feedback/icon_thumbdown.png'; ?>" /></td>
                                        <td align="left" colspan="8" valign="middle"></td>
                                        <td align="center" valign="middle"><img height="32" width="32" alt="" src="<?php echo base_url() . 'assets/images/feedback/icon_thumbup.png'; ?>" /></td>
                                    </tr>
                                    <tr>
                                        <td height="20" align="center" width="10%" valign="middle">
                                            <div>
                                                <a class="rate-star" href="javascript:void(0);"
                                                   onmouseout="changeMouse(0,1);" onmouseover="changeMouse(1,1);" onclick="setStar(1);rate(1);">
                                                    <img border="0" width="19" src="<?php echo base_url() . 'assets/images/feedback/star0.png'; ?>"></a>
                                            </div></td>
                                        <td height="20" align="center" width="10%" valign="middle">
                                            <div>
                                                <a class="rate-star" href="javascript:void(0);"
                                                   onmouseout="changeMouse(0,2);" onmouseover="changeMouse(1,2);" onclick="setStar(2);rate(1);">
                                                    <img border="0" width="19" src="<?php echo base_url() . 'assets/images/feedback/star0.png'; ?>"></a>
                                            </div></td>
                                        <td height="20" align="center" width="10%" valign="middle">
                                            <div>
                                                <a class="rate-star" href="javascript:void(0);"
                                                   onmouseout="changeMouse(0,3);" onmouseover="changeMouse(1,3);" onclick="setStar(3);rate(1);">
                                                    <img border="0" width="19" src="<?php echo base_url() . 'assets/images/feedback/star0.png'; ?>"></a>
                                            </div></td>
                                        <td height="20" align="center" width="10%" valign="middle">
                                            <div>
                                                <a class="rate-star" href="javascript:void(0);"
                                                   onmouseout="changeMouse(0,4);" onmouseover="changeMouse(1,4);" onclick="setStar(4);rate(1);">
                                                    <img border="0" width="19" src="<?php echo base_url() . 'assets/images/feedback/star0.png'; ?>"></a>
                                            </div></td>
                                        <td height="20" align="center" width="10%" valign="middle">
                                            <div>
                                                <a class="rate-star" href="javascript:void(0);"
                                                   onmouseout="changeMouse(0,5);" onmouseover="changeMouse(1,5);" onclick="setStar(5);">
                                                    <img border="0" width="19" src="<?php echo base_url() . 'assets/images/feedback/star0.png'; ?>"></a>
                                            </div></td>
                                        <td height="20" align="center" width="10%" valign="middle">
                                            <div>
                                                <a class="rate-star" href="javascript:void(0);"
                                                   onmouseout="changeMouse(0,6);" onmouseover="changeMouse(1,6);" onclick="setStar(6);">
                                                    <img border="0" width="19" src="<?php echo base_url() . 'assets/images/feedback/star0.png'; ?>"></a>
                                            </div></td>
                                        <td height="20" align="center" width="10%" valign="middle">
                                            <div>
                                                <a class="rate-star" href="javascript:void(0);"
                                                   onmouseout="changeMouse(0,7);" onmouseover="changeMouse(1,7);" onclick="setStar(7);">
                                                    <img border="0" width="19" src="<?php echo base_url() . 'assets/images/feedback/star0.png'; ?>"></a>
                                            </div></td>
                                        <td height="20" align="center" width="10%" valign="middle">
                                            <div>
                                                <a class="rate-star" onmouseout="changeMouse(0,8);"
                                                   onmouseover="changeMouse(1,8);"
                                                   onclick="setStar(8);"
                                                   href="javascript:void(0);" class="unselected2">
                                                    <img border="0" width="19" src="<?php echo base_url() . 'assets/images/feedback/star0.png'; ?>"></a>
                                            </div></td>
                                        <td height="20" align="center" width="10%" valign="middle">
                                            <div>
                                                <a class="rate-star" onmouseout="changeMouse(0,9);"
                                                   onmouseover="changeMouse(1,9);"
                                                   onclick="setStar(9);"
                                                   href="javascript:void(0);" class="unselected2">
                                                    <img border="0" width="19" src="<?php echo base_url() . 'assets/images/feedback/star0.png'; ?>"></a>
                                            </div></td>
                                        <td height="20" align="center" width="10%" valign="middle">
                                            <div>
                                                <a class="rate-star" onmouseout="changeMouse(0,10);"
                                                   onmouseover="changeMouse(1,10);"
                                                   onclick="setStar(10);"
                                                   href="javascript:void(0);">
                                                    <img border="0" width="19" src="<?php echo base_url() . 'assets/images/feedback/star0.png'; ?>"></a>
                                            </div></td>

                                    </tr>
                                </tbody>
                            </table>
                            <br />
                            <span class="titel2"><br> Wij zijn benieuwd hoe wij onze website
                                    en organisatie kunnen verbeteren. Kies een onderwerp en geef je
                                    feedback. </span> <br>
                                <div class="streepjes" id="streepjes2"></div>
                                <br />
                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table cellspacing="5" cellpadding="0" border="0" width="445">
                                                    <tbody>
                                                        <?php
                                                        $catLimit = ceil(count($feedback_cat_details) / 3);
                                                        $k = 1;
                                                        ?>
                                                        <tr>
                                                            <?php foreach ($feedback_cat_details as $row): ?>
                                                                <td height="35" width="145px" valign="top">
                                                                    <div id="question" class="cats">
                                                                        <a class="subnav_cat" href="javascript:void(0)" onclick="toggleSubcat(this,'<?php echo $row->feed_cat_id; ?>','<?php echo $row->feed_cat_name; ?>')">
                                                                            <img border="0" align="absmiddle" src="<?php echo base_url() . 'assets/images/feedback/' . $row->feed_cat_img; ?>" default_src="<?php echo base_url() . 'assets/images/feedback/' . $row->feed_cat_img; ?>" />
                                                                            &nbsp;&nbsp;<span><?php echo $row->feed_cat_name; ?></span>
                                                                            <img border="0" align="absmiddle" src="<?php echo base_url() . 'assets/images/feedback/collapse.png'; ?>" class="down-arrow" style="display:none;" />
                                                                        </a>
                                                                    </div>
                                                                </td>
                                                                <?php
                                                                if ($k++ == 3) {
                                                                    echo '</tr><tr>';
                                                                    $k = 1;
                                                                }
                                                            endforeach;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div id="areas">
                                                    <div style="display: none;" id="remarkPanel" class="remarkPanel">
                                                        <span id="remarkHeader" style="font-size:9pt;"></span><br />
                                                        <textarea onfocus="document.getElementById('alert1').innerHTML = ''"
                                                                  style="width:445px; resize:none;" id="feed_remark"
                                                                  name="feed_remark" class="formfield" cols="30" rows="3"></textarea>
                                                        <div style="color:red;font-size:9pt;" id="alert1"></div>
                                                        <br />
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                        </td>
                        <td background="<?php echo base_url() . 'assets/images/feedback/table_mid_r.png'; ?>"
                            width="43" class="groentableN_rowW_r"></td>
                    </tr>
                    <tr>
                        <td background="<?php echo base_url() . 'assets/images/feedback/table_mid_l.png'; ?>"
                            width="42" class="groentableN_pijl_l">&nbsp;</td>
                        <td height="27" bgcolor="#FFFFFF" align="right" valign="bottom"
                            class="groentableN_rowW_m">
                            <div class="streepjes" id="streepjes3"></div>
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div style="text-align:left; display:none;" id="buttonBottom" class="remarkPanel">
                                                <input type="submit" alt="" class="but_grijspon" />
                                            </div>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td background="<?php echo base_url() . 'assets/images/feedback/table_mid_r.png'; ?>" class="groentableN_rowW_r"></td>
                    </tr>
                    <tr>
                        <td height="45" background="<?php echo base_url() . 'assets/images/feedback/table_bot_l.png'; ?>" class="groentableN_bottom_l"></td>
                        <td background="<?php echo base_url() . 'assets/images/feedback/table_bot_m.png'; ?>" class="groentableN_bottom_m"></td>
                        <td background="<?php echo base_url() . 'assets/images/feedback/table_bot_r.png'; ?>" class="groentableN_bottom_r"></td>
                    </tr>
                </tbody>
            </table>
            </td>
            <td background="<?php echo base_url() . 'assets/images/feedback/table_mid_r.png'; ?>" class="groentableN_rowW_r"></td>
            </tr>

            <input type="hidden" name="feed_rating" id="feed_rating" value="0" />
            <input type="hidden" name="feed_cat_id" id="feed_cat_id" value="0" />
        </form>
        <script type="text/javascript">
            var starClickFlg=true;
            $(document).ready(function(){
            });
            function changeMouse(starFlag,starCount){
                if(starClickFlg){
                    var src1='<?php echo base_url(); ?>' + 'assets/images/feedback/star' + starFlag + '.png';
                    var src2='<?php echo base_url(); ?>' + 'assets/images/feedback/star0.png';
                    $('.rate-star img').each(function(i){ (i<starCount)?$(this).attr('src',src1):$(this).attr('src',src2);});
                }else starClickFlg=true;
            }
            function setStar(starCount){
                starClickFlg=false;
                var src='<?php echo base_url(); ?>' + 'assets/images/feedback/star1.png';
                $('.rate-star img').each(function(i){ if(i==starCount)return false;$(this).attr('src',src);});
                $('#feed_rating').val(starCount);
            }
            function toggleSubcat(ele,catId, header){
                $('.remarkPanel').hide();
                var subcatEle=$(ele).parent().next();
                var downArrowEle=$(ele).children().next().next();
                $('.down-arrow').not(downArrowEle).hide();
                $(downArrowEle).toggle('medium');
                $(subcatEle).toggle('medium');
                $('#remarkHeader').html(header);
                $('.remarkPanel').show('medium');
                $('#feed_cat_id').val(catId);
            }
        </script>
    </body>
</html>