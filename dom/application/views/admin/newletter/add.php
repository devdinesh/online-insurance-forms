<?php if (isset($error) && $error != NULL) { ?>
    <br/>
    <div class="alert alert-error">
        <a class="close" data-dismiss="alert"
           href="<?php echo current_url(); ?>">x</a><?php echo $error; ?>
    </div>
<?php } ?>
<h2 class="title-margin"><span><?php echo $title; ?></span></h2>
<div>
    <form  action="<?php echo base_url() . 'admin/newletter/add_listener'; ?>"  method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>
                <tr>
                    <td>Titel <span class="mandatory">*</span></td>
                    <td><input type="text" name="title" value="<?php echo set_value('title'); ?>" /></td>
                    <td><?php echo form_error('title'); ?></td>
                </tr>
                <tr>
                    <td>Formuliercode <span class="mandatory">*</span></td>
                    <td><input type="text" name="form_code" value="<?php echo set_value('form_code'); ?>" /> &nbsp; <span>De code moet beginnen met 'NB'</span></td>
                    <td><?php echo form_error('form_code'); ?></td>
                </tr>
                <tr>
                    <td>Template </td>
                    <td><select name="template"  class="custom-select">
                            <option value="">Selecteer</option>
                            <?php
                            if (!empty($templates)) {
                                foreach ($templates as $template) {
                                    ?>
                                    <option value="<?php echo $template->email_id; ?>" <?php if (set_value('template') == $template->email_id) echo "selected='selected'"; ?> ><?php echo $template->email_name; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select></td>
                    <td><?php echo form_error('template'); ?></td>
                </tr>
                <tr>
                    <td class="vertical_align">Introduction Text</td>
                    <td><textarea class="tinymce span6" name="introduction_text"><?php echo htmlentities((set_value('introduction_text') != '') ? set_value('introduction_text') : '' ); ?> </textarea></td>
                    <td><?php echo form_error('introduction_text'); ?></td>
                </tr>
                <tr>
                    <td class="vertical_align">Closure Text</td>
                    <td><textarea class="tinymce span6" name="closure_text"><?php echo htmlentities((set_value('closure_text') != '') ? set_value('closure_text') : '' ); ?> </textarea></td>
                    <td><?php echo form_error('closure_text'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button> &nbsp;&nbsp;  
                        <a  href="<?php echo base_url() . 'admin/newletter'; ?>" class="btn btn-large btn-primary">Annuleer</a>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">Fields marked with <span class="mandatory">*</span>
                        are mandatory.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script>
<!-- Load TinyMCE -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',

            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,|,bullist,numlist,|,link,unlink,image",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>
<!-- /TinyMCE -->