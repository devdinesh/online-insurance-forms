<h2 class="title-margin"><span><?php echo $title; ?></span></h2>
<div>
    <form  action="<?php echo base_url() . 'admin/userrole/edit_listener/' . $userrole->id; ?>"  method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>
                <tr>
                    <td>Naam <span class="mandatory">*</span></td>
                    <td><input type="text" name="name" value="<?php echo htmlentities((set_value('name') != '') ? set_value('name') : (isset($userrole->name) ? $userrole->name : '') ); ?>" /></td>
                    <td><?php echo form_error('name'); ?></td>
                </tr>
                <tr>
                    <td>Toon alleen eigen formulieren</td>
                    <td><input class="input" type="checkbox" name="show_user_forms" value="1"  <?php echo set_value('show_user_forms') == 1 ? 'checked=checked' : (isset($userrole->show_user_forms) && $userrole->show_user_forms == 1 ? 'checked=checked' : ''); ?>/></td>
                    <td><?php echo form_error('show_user_forms'); ?></td>
                </tr>
                <tr>
                    <td>Dashboard</td>
                    <td><input class="input" type="checkbox" name="show_dashbord" value="1"  <?php echo set_value('show_dashbord') == 1 ? 'checked=checked' : (isset($userrole->show_dashbord) && $userrole->show_dashbord == 1 ? 'checked=checked' : ''); ?>/></td>
                    <td><?php echo form_error('show_dashbord'); ?></td>
                </tr>
                <tr>
                    <td>Relaties</td>
                    <td><input class="input" type="checkbox" name="show_relations" value="1" <?php echo set_value('show_relations') == 1 ? 'checked=checked' : (isset($userrole->show_relations) && $userrole->show_relations == 1 ? 'checked=checked' : ''); ?>/></td>
                    <td><?php echo form_error('show_relations'); ?></td>
                </tr>
                <tr>
                    <td>Formulieren</td>
                    <td><input class="input" type="checkbox" name="show_forms" value="1" <?php echo set_value('show_forms') == 1 ? 'checked=checked' : (isset($userrole->show_forms) && $userrole->show_forms == 1 ? 'checked=checked' : ''); ?>/></td>
                    <td><?php echo form_error('show_forms'); ?></td>
                </tr>
                <tr>
                    <td>Eigen bibliotheek</td>
                    <td><input class="input" type="checkbox" name="show_own_forms" value="1" <?php echo set_value('show_own_forms') == 1 ? 'checked=checked' : (isset($userrole->show_own_forms) && $userrole->show_own_forms == 1 ? 'checked=checked' : ''); ?>/></td>
                    <td><?php echo form_error('show_own_forms'); ?></td>
                </tr>
                <tr>
                    <td>Nieuws</td>
                    <td><input class="input" type="checkbox" name="show_news" value="1" <?php echo set_value('show_news') == 1 ? 'checked=checked' : (isset($userrole->show_news) && $userrole->show_news == 1 ? 'checked=checked' : ''); ?>/></td>
                    <td><?php echo form_error('show_news'); ?></td>
                </tr>
                <tr>
                    <td>Statistieken</td>
                    <td><input class="input" type="checkbox" name="show_statistics" value="1" <?php echo set_value('show_statistics') == 1 ? 'checked=checked' : (isset($userrole->show_statistics) && $userrole->show_statistics == 1 ? 'checked=checked' : ''); ?>/></td>
                    <td><?php echo form_error('show_statistics'); ?></td>
                </tr>
                <tr>
                    <td>Nieuwsbrieven</td>
                    <td><input class="input" type="checkbox" name="show_newsletters" value="1" <?php echo set_value('show_newsletters') == 1 ? 'checked=checked' : (isset($userrole->show_newsletters) && $userrole->show_newsletters == 1 ? 'checked=checked' : ''); ?>/></td>
                    <td><?php echo form_error('show_newsletters'); ?></td>
                </tr>
                <tr>
                    <td>Overig</td>
                    <td><input class="input" type="checkbox" name="show_other" value="1" <?php echo set_value('show_other') == 1 ? 'checked=checked' : (isset($userrole->show_other) && $userrole->show_other == 1 ? 'checked=checked' : ''); ?>/></td>
                    <td><?php echo form_error('show_other'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button> &nbsp;&nbsp;  
                        <a  href="<?php echo base_url() . 'admin/userrole'; ?>" class="btn btn-large btn-primary">Annuleer</a>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">De met een <span class="mandatory">*</span> gemarkeerde velden zijn verplicht.</td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
