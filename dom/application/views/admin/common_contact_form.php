<div class="special-table">
<!--    <h2 class="title-margin"><span>Contact form</span></h2> -->
    <div class="span12" id="message_box"> </div>
    <form action="javascript:void(0);"
          method="post" enctype="multipart/form-data" name="contact_form" id="contact_form">
        <table class="table table-striped table-condensed">
            <tr>
                <td width="20%" width="15%" align="left" valign="top">Naam <span class="mandatory">*</span></td>
                <td width="24%" align="left" valign="top"><input type="text" name="name" value="<?php echo set_value('name'); ?>" /></td>
                <td align="left" valign="top"><?php echo form_error('name'); ?></td>
            </tr>
            <tr>
                <td align="left" valign="top" width="20%">Bedrijfsnaam</td>
                <td align="left" valign="top" width="24%"><input type="text"  name="company_name" value="<?php echo set_value('company_name'); ?>" /></td>
                <td><?php echo form_error('company_name'); ?></td>
            </tr>
            <tr>
                <td align="left" valign="top">E-mailadres<span class="mandatory">*</span></td>
                <td align="left" valign="top"><input type="text" name="email_address" value="<?php echo set_value('email_address'); ?>" /></td>
                <td><?php echo form_error('email_address'); ?></td>
            </tr>
            <tr>
                <td align="left" valign="top">Telefoonnummer</td>
                <td align="left" valign="top"><input type="text" name="phone_number" value="<?php echo set_value('phone_number'); ?>" /></td>
                <td><?php echo form_error('phone_number'); ?></td>
            </tr>
            <tr>
                <td align="left" valign="top">Opmerkingen</td>
                <td align="left" valign="top"><textarea name="remark"><?php echo set_value('remark'); ?></textarea></td>
                <td align="left" valign="top"><?php echo form_error('remark'); ?></td>
            </tr>

            <tr>
                <td align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top"><input type="submit" value="Verstuur"  class="btn btn-primary" /></td>
                <td align="left" valign="top">&nbsp;</td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    //<![CDATA[
    $(document).ready(function(){
        $("#contact_form").validate({
            rules: {
                "name": {
                    required: true
                },
                "email_address": {
                    required: true,
                    email: true
                },
                "phone_number": {
                    number: true
                }
            },
            onkeyup: false,
            onblur: true
        });
    }); 
    jQuery('#contact_form').submit(function(event){
        event.preventDefault();
        if( jQuery('#contact_form').valid()){
            var searzlie = jQuery('#contact_form').serialize();
            jQuery.ajax({
                url : "<?php echo base_url() . 'save_contact'; ?>", 
                type: "post",
                data: searzlie,
                dataType: 'json',
                success: function(data){
                    jQuery('#contact_form')[0].reset(); 
                    jQuery('#message_box').html(data.message);
                     
                }
            });      }
    });
    

</script>
