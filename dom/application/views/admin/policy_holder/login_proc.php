
<html>
<body>
	<form
		action="<?php echo base_url() . "user/authenticate/check_policy_holder_Login"; ?>"
		method="post">
		<table border="0" class="offset3" style="background-color: #fff;">
			<tr>
				<td colspan="3"><h2 class="offset1">Policy Holder Login</h2></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<td width="10%">E-mailadres:<span class="mandatory">*</span>
				</td>
				<td><input class="input" type="text" name="p_email"
					value="<?php echo htmlentities((set_value('p_email') != '') ? set_value('p_email') : '' ); ?>" /></td>
				<td><?php echo form_error('p_email'); ?></td>
			</tr>
			<tr>
				<td>Password:<span class="mandatory">*</span>
				</td>
				<td><input class="input" type="password" name="p_password"
					value="<?php echo htmlentities((set_value('p_password') != '') ? set_value('p_password') : '' ); ?>" /></td>
				<td><?php echo form_error('p_password'); ?></td>
			</tr>
			<tr>
				<td colspan="3"><button type="submit" class="btn offset2">Login</button></td>
			</tr>
		</table>
	</form>
</body>
</html>