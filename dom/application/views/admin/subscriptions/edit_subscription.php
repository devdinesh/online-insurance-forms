
<script type="text/javascript">
    $(function() {
        $( "#from_date").datepicker({ dateFormat: 'dd-mm-yy' });
        $( "#end_date").datepicker({ dateFormat: 'dd-mm-yy'});
    });
</script>
<h2 class="title">Abonnementen Bewerken</h2>

<form method="post"
	action="<?php echo base_url() . 'admin/admin_client_subscriptions/edit_listener/' . $subscription->client_subscription_id; ?>"
	enctype="multipart/form-data" id="1">
	<table width="100%" class="table table-striped table-condensed">
		<tbody>
			<tr>
				<td width="20%">Ingangsdatum  <span class="mandatory">*</span></td>
				<td><input class="input" id="from_date" type="text" name="from_date"
					value="<?php echo htmlentities((set_value('from_date') != '') ? set_value('from_date') : (isset($subscription->from_date) ? $subscription->from_date : '') ); ?>" />
				</td>
				<td><?php echo form_error('from_date'); ?></td>
			</tr>

			<tr>
				<td>Einddatum 
				</td>
				<td><input class="input" id="end_date" type="text" name="end_date"
					value="<?php echo htmlentities((set_value('end_date') != '') ? set_value('end_date') : (isset($subscription->end_date) ? $subscription->end_date : '') ); ?>" /></td>
				<td><?php echo form_error('end_date'); ?></td>
			</tr>
			<tr>
				<td>Soort abonnement <span class="mandatory">*</span>
				</td>
				<td><select name="subscription_kinds_id">
                        <?php
                        foreach ($subcription_kinds as $subcription_kind)
                        {
                            ?>
                            <option
							<?php echo $subscription->subscription_kinds_id == $subcription_kind->subscription_kinds_id ? 'selected="selected"' : ''?>
							value="<?php print_r($subcription_kind->subscription_kinds_id) ?>"><?php print_r($subcription_kind->subscription_title) ?></option>
                            <?php
                        }
                        ?>
                    </select></td>
				<td><?php echo form_error('address'); ?></td>
			</tr>
			<tr>
				<td colspan="2"><button type="submit" class="btn">Opslaan</button>
					&nbsp; &nbsp; &nbsp; &nbsp; <a
					href="<?php echo base_url() . 'admin/admin_client_subscriptions'; ?>" class="btn">Annuleer</a></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="3">Velden gemarkeerd met een <span class="mendetory">*</span>
					 zijn verplicht.
				</td>
			</tr>
		</tbody>
	</table>
</form>
<br>

<br>