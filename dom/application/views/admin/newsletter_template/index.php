<h2 class="title-margin"><span><?php echo $title; ?></span></h2>
<div class="row-fluid">
    <p class="span12">
        <a href="<?php echo base_url() . 'admin/newsletter_template/add'; ?>" class="btn btn-large btn-primary">Voeg nieuwsbrief templates</a>
    </p>
</div>
<div>
    <table id="list_clients"
           class="tbl table table-striped table-condensed" width="100%"
           cellspacing="0" cellpadding="0">
        <thead>
            <tr class="tbl_hd">
                <th width="40%">Naam</th>
                <th width="50%">Onderwerp</th>
                <th>Verwijderen</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>etc</td>
                <td>etc</td>
                <td>etc</td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_clients').dataTable( {
            //  "bJQueryUI": true,
            "bProcessing":true,
            "bPaginate" : false,
            "bFilter": false,
            "bInfo": false,
            "bSort": false, 
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},{"sClass":"align_center"}
               
            ],
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": "<?php echo site_url("admin/newsletter_template/getJson"); ?>"
        } );
    } );
    function deleteRow(ele){
        var current_id = $(ele).attr('id');
        //   var parent = $(ele).parent().parent();
        if( confirm("Do you want to delete?"))
        {
            location.href="<?php echo base_url(); ?>admin/newsletter_template/delete/"+current_id;
        }
                    
        return false;
    }
</script>