<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',

            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,|,bullist,numlist,|,link,unlink,image,|,code",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>
<!-- /TinyMCE -->
<h2 class="title-margin"><span><?php echo $title; ?></span></h2>

<form id="edit_email_format" name="edit_email_format"
      action="<?php echo base_url() . 'admin/newsletter_template/editListener/' . $email_id; ?>"
      method="post" enctype="multipart/form-data">
    <table width="100%" class="table table-striped table-condensed">
        <tr>
            <td>Naam :</td>
            <td><input type="text" name="email_name" class="inptcust"
                       value="<?php
echo htmlentities(
        set_value('email_name') != '' ? set_value('email_name') : ( isset(
                        $email_template->email_name) ? $email_template->email_name : '' ));
?>" /></td>
            <td><?php echo form_error('email_name'); ?></td>
        </tr>
        <tr>
            <td>Onderwerp:</td>
            <td><input type="text" name="email_subject" class="inptcust"
                       value="<?php
                       echo htmlentities(
                               set_value('email_subject') != '' ? set_value('email_subject') : ( isset(
                                               $email_template->email_subject) ? $email_template->email_subject : '' ));
?>" /></td>
            <td><?php echo form_error('email_subject'); ?></td>
        </tr>

        <tr>
            <td class="vertical_align">Tekst :</td>
            <td><textarea id="email_message" name="email_message" rows="15"
                          class="tinymce span6">
                              <?php
                              echo htmlentities(
                                      set_value('email_message') != '' ? set_value(
                                                      'email_message') : isset(
                                                      $email_template->email_message) ? $email_template->email_message : '');
                              ?>
                </textarea></td>
            <td><?php echo form_error('email_message'); ?></td>
        </tr>
        <?php if (isset($email_template->email_dynamic_fields) && $email_template->email_dynamic_fields != NULL) { ?>
            <tr>
                <td class="vertical_align">&nbsp;</td>
                <td><?php echo htmlentities($email_template->email_dynamic_fields); ?></td>
                <td>&nbsp;</td>
            </tr>
        <?php } ?>
        <tr>
            <td>
                <button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                &nbsp;&nbsp; 
                <a  href="<?php echo base_url() . 'admin/newsletter_template'; ?>">
                    <button type="button" class="btn btn-large btn-primary">Annuleer</button>
                </a>
            </td>
            <td></td>
        </tr>
    </table>
    <br>
</form>
