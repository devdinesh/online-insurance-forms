<script src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script> 
<script>
   
    $(document).ready(function(){
        $("#show_calculated_benefit").hide();
        $("#cal_benefits").validate({
            rules: {
               
                number_of_form: {
                    required: true
                },
                cost_of_form: {
                    required: true
                },
                number_of_user: {
                    required: true
                }
            },
            messages: {
                number_of_form: {
                    required: "Dit veld is verplicht om in te vullen"
                },
                cost_of_form: {
                    required: "Dit veld is verplicht om in te vullen"
                },
                number_of_user: {
                    required: "Dit veld is verplicht om in te vullen"
                }
            }
        });
    });
    
    function calculate_benefit(){
        var validate_status = $("#cal_benefits").valid();
        if(validate_status == true){
            var number_of_form=$('#number_of_form').val();
            var cost_of_form=$('#cost_of_form').val();
            cost_of_form=cost_of_form.replace(",", "."); 
            var http_host_js = "<?php echo base_url(); ?>";
            if(number_of_form!='' && cost_of_form!='' ){
                $("#show_calculated_benefit").show();
                $.ajax({
                    type : 'POST',
                    url : http_host_js+'admin/authenticate/calculate_benefit/'+number_of_form+'/'+cost_of_form,
                    data: id =number_of_form,
                    success: function(data) {
                        var rate_info =$.parseJSON(data);
                        $('#cost_without_dom').html(rate_info.cost_without_dom);
                        $('#cost_with_dom').html(rate_info.cost_with_dom);
                        $('#benefits').html(rate_info.benefits);
                        // $('#annual_users').html(rate_info.annual_users);
                    },
                    error : function(XMLHttpRequest, textStatus, errorThrown) {
                        alert('error');
                        exit;
                    }
                });
            }else{
                $("#show_calculated_benefit").hide();
            }
            return false;
        }else{
            $("#show_calculated_benefit").hide();
            return false;
        }
        
    } 
</script>
<style type="text/css">
    .input-symbol-euro {
        position: relative;
    }
    .input-symbol-euro input {
        padding-left:15px;
    }
    .input-symbol-euro:before {
        position: absolute;
        top: -5px;
        content:"€";
        left: 5px;
    }
    .input-symbol-euro .text-error{
        float:none!important;
    }
</style>
<div class="container banner">
    <div class="hero-unit">
        <?php echo $banner_images; ?>
        <!-- Feel free to load scripts in the footer --> 
        <script>
            $('#main-slider').liquidSlider();
        </script> 
    </div>
</div> 
<div class="container">
    <div class="row-fluid">
        <div class="span8">
            <?php if (isset($text_content) && !empty($text_content)) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <h1><span><?php echo $text_content->page_title; ?></span></h1>
                        <?php echo str_replace('../../../../', base_url(), $text_content->text_content); ?>   
                    </div> 
                    <?php
                    if (isset($contact_form) && $contact_form == 1) {
                        ?>
                        <div class="span12">
                            <?php
                            echo show_contact_form();
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <?php
                    if (isset($Pakketten) && $Pakketten == TRUE) {
                        if (!empty($rates)) {
                            ?>
                            <!--                            <div class="span12 table-min-width">
                                                            <h1><span>Onze tarieven</span></h1>
                                                            <p>Kosten verstuurde formulieren</p>
                                                            <table class="table table-bordered table-striped" style="width:70%">
                                                                <tr>
                                                                    <th>Van</th>
                                                                    <th>Tot</th>
                                                                    <th>Tarief</th>
                                                                </tr>
                            <?php foreach ($rates as $rate) { ?>
                                                                                <tr>
                                                                                    <td><?php echo $rate->number_from; ?></td>
                                                                                    <td><?php echo $rate->number_to; ?></td>
                                                                                    <td> &euro; <?php echo convert_eng_dutch($rate->rate); ?></td>
                                                                                </tr>
                            <?php } ?>
                                                            </table>
                                                                                            <div>
                            <?php
                            if (!empty($user_rates)) {
                                ?>
                                                                                                                                    <p>Maandelijkse licentiekosten gebruikers</p>
                                                                                                                                    <table class="table table-bordered table-striped" style="width:70%">
                                                                                                                                        <tr>
                                                                                                                                            <th>Van</th>
                                                                                                                                            <th>Tot</th>
                                                                                                                                            <th>Tarief</th>
                                                                                                                                        </tr>
                                <?php foreach ($user_rates as $user_rate) { ?>
                                                                                                                                                                                <tr>
                                                                                                                                                                                    <td><?php echo $user_rate->from; ?></td>
                                                                                                                                                                                    <td><?php echo $user_rate->to; ?></td>
                                                                                                                                                                                    <td> &euro; <?php echo convert_eng_dutch($user_rate->rate); ?></td>
                                                                                                                                                                                </tr>
                                <?php } ?>
                                                                                                                                    </table>
                                                                                                
                                <?php
                            }
                            ?> 
                                                                                            </div>
                                                        </div>-->
                            <div class="span12">
                                <h1><span>Bereken uw voordeel</span></h1>
                                <div>
                                    <form  action="#" onsubmit="return calculate_benefit();"
                                           method="post" enctype="multipart/form-data" id="cal_benefits">
                                        <table width="100%" class="table table-condensed"  border="0">

                                            <tbody>
                                                <tr>
                                                    <td style="width:50%;">Aantal verstuurde formulieren per jaar : <span class="mandatory">*</span></td>
                                                    <td> <span><input class="required number " type="text" name="number_of_form" id="number_of_form" /></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Gemiddelde kosten per formulier : <span class="mandatory">*</span></td>
                                                    <td><span><input  type="text" name="cost_of_form" class="required currencyInput" id="cost_of_form" /></span></td>
                                                </tr>
            <!--                                                <tr>
                                                    <td>Aantal gewenste gebruikers DOM : <span class="mandatory">*</span></td>
                                                    <td><input  type="text" name="number_of_user" class="required number" id="number_of_user"/></td>
                                                </tr>-->
                                                <tr>
                                                    <td colspan="3">
                                                        <button type="submit" class="btn btn-large btn-primary" onclick="calculate_benefit();">Bereken mijn voordeel</button> &nbsp;&nbsp;
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                                <table id="show_calculated_benefit">
                                    <tr>
                                        <td> Huidige jaarlijkse kosten :</td>
                                        <td>
                                            <span id="cost_without_dom"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Jaarlijkse kosten formulieren met Digitaal op Maat :</td>
                                        <td>
                                            <span id="cost_with_dom"></span>
                                        </td>
                                    </tr>
            <!--                                    <tr>
                                        <td> Jaarlijkse licentiekosten gebruikers :</td>
                                        <td>
                                            <span id="annual_users"></span>
                                        </td>
                                    </tr>-->
                                    <tr>
                                        <td>Mijn jaarlijkse voordeel : </td>
                                        <td>
                                            <span id="benefits"> </span>
                                        </td>
                                    </tr>

                                </table>
                            </div>

                            <?php
                        }
                    }
                    ?>


                </div>
            <?php } ?>
            <?php
            if (isset($feature_details) && $feature_details != '') {
                foreach ($feature_details as $feature) {
                    ?>
                    <div class="row-fluid"><!--/span-->
                        <div class="span12 feature_point_left">
                            <h2><span><?php echo $feature->feature_label; ?></span></h2>
                            <?php echo $feature->feature_text; ?>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
            <!--/row-->
        </div>
        <div class="span4 pull-right">
            <div class="row-fluid">
                <div class="span12">
                    <?php if (isset($text_points) && !empty($text_points)) { ?>
                        <h2><span><?php echo $text_points->page_title; ?></span></h2>
                        <div class="sidebar-nav paddingtop0">
                            <?php echo str_replace('../../../../', base_url(), $text_points->text_content); ?> 
                        </div>
                    <?php } ?>
                </div>
                <?php if (isset($text_video) && !empty($text_video)) { ?>
                    <div class="span12">
                        <h2><span><?php echo $text_video->page_title; ?></span></h2>
                        <!-- <div class="video_img">
                               <img src="<?php echo assets_url_img; ?>new_img/video_img.png" alt="" title=""> 
                         </div>-->
                        <div class="w sidebar-nav">
                            <?php echo str_replace('../../../../', base_url(), $text_video->text_content); ?> 
                        </div>
                    </div>
                <?php } ?>
                <div class="span12">
                    <h2><span>Vraag demo aan</span></h2> 
                    <div class="well sidebar-nav">
                        <a href="<?php echo base_url() . 'contact'; ?>">
                            <img src="<?php echo base_url(); ?>assets/upload/client_logo/vraagdemoaan.jpg" alt="Vraag demo aan" title="Vraag demo aan"> 
                        </a>
                    </div>
                </div>
            </div><!--/.well -->
        </div><!--/span-->
        <!--/span-->
    </div><!--/row-->
</div>

