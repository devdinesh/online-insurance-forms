
<h2 class="title-margin"><span>Details Gebruikers</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'admin/admin_user/update/' . $user_id; ?>"
        method="post">
        <table width="100%" class="table table-striped table-condensed">
            <tr>
                <td width="16%">Actief</td>
                <td width="25%"><input class="input" type="checkbox" name="status"
                                       value="A"
                                       <?php
                                       if ($user->status == 'A') {
                                           echo 'checked=checked';
                                       }
                                       ?> /></td>

                <td></td>
            </tr>
            <tr>
                <td width="16%">Beheerder</td>
                <td width="25%"><input class="input" type="checkbox" name="administrator"
                                       value="1"
                                       <?php
                                       if ($user->administrator == 1) {
                                           echo 'checked=checked';
                                       }
                                       ?> /></td>

                <td></td>
            </tr>
            <tr>
                <td>Voornaam<span class="mandatory">*</span></td>
                <td><input type="text" name="first_name"
                           value="<?php
                                       echo set_value('first_name') != '' ? set_value('first_name') : isset(
                                                       $user->first_name) ? $user->first_name : '';
                                       ?>" /></td>
                <td><?php echo form_error('first_name'); ?></td>
            </tr>

            <tr>
                <td>Tussenvoegsel</td>
                <td><input type="text" name="middle_name"
                           value="<?php
                           echo set_value('middle_name') != '' ? set_value('middle_name') : isset(
                                           $user->middle_name) ? $user->middle_name : '';
                                       ?>" /></td>
                <td><?php echo form_error('middle_name'); ?></td>
            </tr>

            <tr>
                <td>Achternaam<span class="mandatory">*</span></td>
                <td><input type="text" name="last_name"
                           value="<?php
                           echo set_value('last_name') != '' ? set_value('last_name') : isset(
                                           $user->last_name) ? $user->last_name : '';
                                       ?>" /></td>
                <td><?php echo form_error('last_name'); ?></td>
            </tr>
            <tr>
                <td>Bedrijf </td>
                <td><input type="text" name="company"
                           value="<?php
                           echo set_value('company') != '' ? set_value('company') : isset(
                                           $user->company) ? $user->company : '';
                                       ?>" /></td>
                <td><?php echo form_error('company'); ?></td>
            </tr>
            <tr>
                <td>Afdeling </td>
                <td><select name="department" class="custom-select">
                        <option value="">Selecteer Afdeling</option>
                        <?php
                        if (!empty($departments)) {
                            foreach ($departments as $department) {
                                ?>
                                <option value="<?php echo $department->account_id; ?>" <?php if (set_value('department') == $department->account_id || $department->account_id == $user->department) echo "selected='selected'"; ?> ><?php echo $department->name; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select></td>
                <td><?php echo form_error('department'); ?></td>
            </tr>

            <tr>
                <td> E-mailadres<span class="mandatory">*</span></td>
                <td><input type="text" autocomplete="off" name="mail_address"
                           value="<?php
                        echo set_value('mail_address') != '' ? set_value('mail_address') : isset(
                                        $user->mail_address) ? $user->mail_address : '';
                        ?>" /></td>
                <td><?php echo form_error('mail_address'); ?></td>
            </tr>
            <tr>
                <td>Rol </td>
                <td><select name="role_menu" class="custom-select">
                        <option value="">Selecteer </option>
                        <?php
                        if (!empty($userroles)) {
                            foreach ($userroles as $userrole) {
                                ?>
                                <option value="<?php echo $userrole->id; ?>" <?php if (set_value('role_menu') == $userrole->id || $userrole->id == $user->role_menu) echo "selected='selected'"; ?> ><?php echo $userrole->name; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select></td>
                <td><?php echo form_error('role_menu'); ?></td>
            </tr>
            <tr>
                <td>Wachtwoord</td>
                <td><input type="password" autocomplete="off" name="password" /></td>
                <td><?php echo form_error('password'); ?></td>
            </tr>

            <tr>
                <td>Password Confirmation</td>
                <td><input type="password" autocomplete="off"
                           name="password_confirmation" /></td>
                <td><?php echo form_error('password_confirmation'); ?></td>
            </tr>

            <tr>
                <td colspan="3">
                    <button type="submit" class="btn btn-large btn-primary">Opslaan</button> <a
                        href="<?php echo base_url() . 'admin/admin_user'; ?>" class="btn btn-large btn-primary">Annuleer</a>
                </td>
            </tr>

            <tr>
                <td colspan="3">Velden die gemarkeerd zijn met een <span class="mandatory">*</span>
                    zijn verplicht.
                </td>
            </tr>
        </table>
    </form>
</div>
<br>
