
<h2 class="title-margin"><span>Add New User<span></h2>
            <div>
                <form
                    action="<?php echo base_url() . 'admin/admin_user/addListener'; ?>"
                    method="post" enctype="multipart/form-data">
                    <table width="100%" class="table table-striped table-condensed"
                           border="0">
                        <tbody>

                            <tr>
                                <td width="16%">Active</td>
                                <td width="25%"><input class="input" type="checkbox" name="status"
                                                       value="A" /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="16%">Beheerder</td>
                                <td width="25%"><input class="input" type="checkbox" name="administrator"  value="1" /></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>First Name<span class="mandatory">*</span></td>
                                <td><input type="text" name="first_name"
                                           value="<?php echo set_value('first_name'); ?>" /></td>
                                <td><?php echo form_error('first_name'); ?></td>
                            </tr>

                            <tr>
                                <td>Middle Name</td>
                                <td><input type="text" name="middle_name"
                                           value="<?php set_value('middle_name'); ?>" /></td>
                                <td><?php echo form_error('middle_name'); ?></td>
                            </tr>
                            <tr>
                                <td>Last Name<span class="mandatory">*</span></td>
                                <td><input type="text" name="last_name"
                                           value="<?php echo set_value('last_name'); ?>" /></td>
                                <td><?php echo form_error('last_name'); ?></td>
                            </tr>
                            <tr>
                                <td>Bedrijf</td>
                                <td><input type="text" name="company"
                                           value="<?php echo set_value('company'); ?>" /></td>
                                <td><?php echo form_error('company'); ?></td>
                            </tr>
                            <tr>
                                <td>Afdeling </td>
                                <td><select name="department"  class="custom-select">
                                        <option value="">Selecteer Afdeling</option>
                                        <?php
                                        if (!empty($departments)) {
                                            foreach ($departments as $department) {
                                                ?>
                                                <option value="<?php echo $department->account_id; ?>" <?php if (set_value('department') == $department->account_id) echo "selected='selected'"; ?> ><?php echo $department->name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select></td>
                                <td><?php echo form_error('department'); ?></td>
                            </tr>
                            <tr>
                                <td>Mail Address<span class="mandatory">*</span></td>
                                <td><input type="text" autocomplete="off" name="mail_address"
                                           value="<?php echo set_value('mail_address'); ?>" /></td>
                                <td><?php echo form_error('mail_address'); ?></td>
                            </tr>
                            <tr>
                                <td>Rol</td>
                                <td><select name="role_menu" class="custom-select">
                                        <option value="">Selecteer</option>
                                        <?php
                                        if (!empty($userroles)) {
                                            foreach ($userroles as $userrole) {
                                                ?>
                                                <option value="<?php echo $userrole->id; ?>" <?php if (set_value('role_menu') == $userrole->id) echo "selected='selected'"; ?> ><?php echo $userrole->name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select></td>
                                <td><?php echo form_error('role_menu'); ?></td>
                            </tr>
                            <tr>
                                <td>Password <span class="mandatory">*</span></td>
                                <td><input type="password" autocomplete="off" name="password"
                                           value="<?php echo set_value('password'); ?>" /></td>
                                <td><?php echo form_error('password'); ?></td>
                            </tr>
                            <tr>
                                <td>Password Confirmation <span class="mandatory">*</span></td>
                                <td><input type="password" autocomplete="off"
                                           name="password_confirmation"
                                           value="<?php echo set_value('password_confirmation'); ?>" /></td>
                                <td><?php echo form_error('password_confirmation'); ?></td>
                            </tr>

                            <tr>
                                <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Save</button> &nbsp;&nbsp;  
                                    <a  href="<?php echo base_url() . 'admin/admin_user'; ?>" class="btn btn-large btn-primary">Annuleer</a>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="3">Fields marked with <span class="mandatory">*</span>
                                    are mandatory.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <br />
            <br />
