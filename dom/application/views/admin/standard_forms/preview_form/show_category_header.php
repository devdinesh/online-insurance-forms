<style type="text/css">
    .que_box{
        border:none !important;
        background:#fff !important;
    }
    h2 > span{
        font-size: 23px !important;
        padding: 0 5px 7px 0 !important;}
    .que_box > h3{
        font-size: 18px !important;
    }
</style>
<h2><span><?php echo $form->form_name; ?></span></h2>
<div>
    <h3><?php echo @$total_form_completed; ?>% form compleet</h3>
    <div class="progress progress-striped active">
        <div class="bar" style="width: <?php echo @$total_form_completed . '%'; ?>;"></div>
    </div>
</div>
<div class="que_box">
    <p><?php echo $form->introduction_text; ?></p>
</div>
<div class="btn_form"><a href="<?php echo base_url() . 'admin/standard_forms'; ?>" class="btn btn-primary">Terug</a></div>
<div class="btn_form">  
    <?php if (isset($next_category)) { ?>
        <a href="<?php echo base_url() . 'admin/standard_forms/preview_category/' . $claim_id . '/' . $form->form_id . '/' . $next_category['next_cat']->cat_id; ?>" class="btn btn-primary">Volgende</a>
    <?php } ?>
</div>
