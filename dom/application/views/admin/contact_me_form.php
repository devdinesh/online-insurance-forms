<div class="container">
    <h2>Neem Contact Op</h2>
    <?php
    if (isset($text->text_content)) {
        echo $text->text_content;
    } else {
        echo "Text not set ";
    }
    ?>
    <div class="special-table">
        <form
            action="<?php echo base_url() . 'contact_me_function'; ?>"
            method="post" enctype="multipart/form-data">

            <table class="table table-striped table-condensed">

                <tr>
                    <td colspan="3" align="left" valign="middle">
                        <h3>Contactpersoon</h3>
                    </td>
                </tr>
                <tr>
                    <td width="20%" width="15%" align="left" valign="middle"
                        class="contact-me-lables-2">Bedrijfsnaam <span class="mandatory">*</span></td>
                    <td width="24%" align="left" valign="middle"><input type="text"
                                                                        name="company_name"
                                                                        value="<?php echo set_value('company_name'); ?>" /></td>
                    <td align="left" valign="middle"><?php echo form_error('company_name'); ?></td>
                </tr>
                <tr>
                    <td align="left" valign="middle" class="contact-me-lables-2"
                        width="20%">Voornaam</td>
                    <td align="left" valign="middle" width="24%"><input type="text"
                                                                        name="f_name" value="<?php echo set_value('f_name'); ?>" /></td>
                    <td><?php echo form_error('f_name'); ?></td>
                </tr>
                <tr>
                    <td align="left" valign="middle" class="contact-me-lables-2">Achternaam<span
                            class="mandatory">*</span></td>
                    <td align="left" valign="middle"><input type="text" name="l_name"
                                                            value="<?php echo set_value('l_name'); ?>" /></td>
                    <td><?php echo form_error('l_name'); ?></td>
                </tr>
                <tr>
                    <td align="left" valign="middle" class="contact-me-lables-2">E-mailadres<span
                            class="mandatory">*</span>
                    </td>
                    <td align="left" valign="middle"><input type="text"
                                                            name="person_email"
                                                            value="<?php echo set_value('person_email'); ?>" /></td>
                    <td><?php echo form_error('person_email'); ?></td>
                </tr>
                <tr>
                    <td align="left" valign="middle" class="contact-me-lables-2">Adres<span
                            class="mandatory">*</span></td>
                    <td align="left" valign="middle"><input type="text" name="street"
                                                            value="<?php echo set_value('street'); ?>" /></td>
                    <td align="left" valign="middle"><?php echo form_error('street'); ?></td>
                </tr>
                <tr>
                    <td align="left" valign="middle" class="contact-me-lables-2">Postcode<span
                            class="mandatory">*</span></td>
                    <td align="left" valign="middle"><input type="text" name="zipcode"
                                                            value="<?php echo set_value('zipcode'); ?>" /></td>
                    <td align="left" valign="middle"><?php echo form_error('zipcode'); ?></td>
                </tr>
                <tr>
                    <td align="left" valign="middle" class="contact-me-lables-2">Stad<span
                            class="mandatory">*</span></td>
                    <td align="left" valign="middle"><input type="text" name="city"
                                                            value="<?php echo set_value('city'); ?>" /></td>
                    <td align="left" valign="middle"><?php echo form_error('city'); ?></td>
                </tr>

<!-- 	<tr>
                <td align="left" valign="middle" class="contact-me-lables-2">Website</td>
                <td align="left" valign="middle"><input type="text" name="website"
                        value="<?php echo set_value('website'); ?>" /></td>
                <td align="left" valign="middle"><?php echo form_error('website'); ?></td>
        </tr>
                -->

                </tbody>
            </table>

            <div>
                <input type="submit" value="Verstuur"
                       class="btn btn-primary" /><br />Velden met een <span
                       class="mandatory">*</span> zijn verplicht om in te vullen.
            </div>


        </form>
    </div>
</div>
