<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',

            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,|,bullist,numlist,|,link,unlink,image,|,code",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>
<!-- /TinyMCE -->
<h2 class="title-margin"><span>E-Mailsjabloon</span></h2>
<?php if ($this->session->flashdata('file_errors') != '') { ?>
    <tr>
        <td>
            <div class="alert alert-error">
                <?php
                $upload_error = $this->session->flashdata('file_errors');
                echo $upload_error['logo'];
                ?>
            </div>
        </td>
    </tr>
<?php } ?>


<form id="edit_email_format" name="add_email_format"
      action="<?php echo base_url() . 'admin/admin_mail_template/addListener'; ?>"
      method="post" enctype="multipart/form-data">
    <table width="100%" class="table table-striped table-condensed">
        <tr>
            <td> Naam :</td>
            <td><input type="text" name="email_name" class="inptcust" value="<?php echo htmlentities(set_value('email_name') != '' ? set_value('email_name') : NULL); ?>" /></td>
            <td><?php echo form_error('email_name'); ?></td>
        </tr>
        <tr>
            <td>Onderwerp:</td>
            <td><input type="text" name="email_subject" class="inptcust" value="<?php echo htmlentities(set_value('email_subject') != '' ? set_value('email_subject') : NULL); ?>" /></td>
            <td><?php echo form_error('email_subject'); ?></td>
        </tr>

        <tr>
            <td class="vertical_align">Tekst :</td>
            <td>
                <textarea id="email_message" name="email_message" rows="15"  class="tinymce span6"> 
                    <?php echo htmlentities(set_value('email_message') != '' ? set_value('email_message') : NULL); ?>
                </textarea>
            </td>
            <td><?php echo form_error('email_message'); ?></td>
        </tr>
        <tr>
            <td class="vertical_align">&nbsp;</td>
            <td><?php echo htmlentities('<link>,<logo>,<voornaam>,<tussenvoegsel>,<achternaam>,<bedrijf>'); ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Bijlage :</td>
            <td><input type="file" name="email_attachment" /></td>
            <td><?php
                    if ($this->session->flashdata('file_errors')) {
                        $file_errors = $this->session->flashdata('file_errors');
                        if (isset($file_errors['email_attachment'])) {
                            echo $file_errors['email_attachment'];
                        }
                    }
                    ?></td>
        </tr>
        <tr>
            <td>BCC Naar Beheerder:</td>
            <td><input type="checkbox" name="email_bcc_admin" value="1"/></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                &nbsp;&nbsp; 
                <a  href="<?php echo base_url() . 'admin/admin_mail_template'; ?>">
                    <button type="button" class="btn btn-large btn-primary">Annuleer</button>
                </a>
            </td>
            <td></td>
        </tr>
    </table>
    <br>
</form>
