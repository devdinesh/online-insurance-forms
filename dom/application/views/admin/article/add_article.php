<h2 class="title-margin"><span><?php echo $title; ?></span></h2>
<div>
    <form
        action="<?php echo base_url() . 'admin/article/addListener'; ?>"
        method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>
                <tr>
                    <td>Titel <span class="mandatory">*</span></td>
                    <td><input type="text" name="title" value="<?php echo set_value('title'); ?>" /></td>
                    <td><?php echo form_error('title'); ?></td>
                </tr>
                <tr>
                    <td class="vertical_align">Tekst</td>
                    <td><textarea class="tinymce span6" name="content"><?php echo htmlentities((set_value('content') != '') ? set_value('content') : '' ); ?> </textarea></td>
                    <td><?php echo form_error('content'); ?></td>
                </tr>
                <tr>
                    <td>Afbeelding</td>
                    <td><input type="file" name="image" class="input" /></td>
                    <td class="label-error"><?php
$error = form_error('image');
if ($this->session->flashdata('file_errors') != '') {
    echo $this->session->flashdata('file_errors');
} else if ($error != '') {
    echo $error;
} else if (isset($image_error) && $image_error != NULL) {
    echo $image_error;
}
?></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <a  href="<?php echo base_url() . 'admin/article'; ?>" class="btn btn-large btn-primary">Annuleer</a>&nbsp;&nbsp;  
                        <button type="submit" class="btn btn-large btn-primary">Opslaan</button> 

                    </td>
                </tr>

                <tr>
                    <td colspan="3">De met een <span class="mandatory">*</span>
                        gemarkeerde velden zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script>
<!-- Load TinyMCE -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',

            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,|,bullist,numlist,|,link,unlink,image",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>
<!-- /TinyMCE -->
