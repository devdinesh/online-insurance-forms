<?php
/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
?>
<div class="row-fluid">
	<p class="span12">
		<a href="<?php echo base_url() . "admin/"; ?>"> Home </a> &gt; Import
		Insurance form
	</p>
</div>
<h2>Import of insurance forms</h2>

<?php
// error if the form tag does not exists
if ($this->session->flashdata('error') != '')
{
    ?>

<div class="alert alert-error">
        <?php
    $error = $this->session->flashdata('error');
    echo $error;
    ?>
    </div>
<?php } ?>


<?php
// error if file upload did not work properly
if ($this->session->flashdata('file_errors') != '')
{
    ?>

<div class="alert alert-error">
        <?php
    $upload_error = $this->session->flashdata('file_errors');
    echo $upload_error['logo'];
    ?>
    </div>
<?php } ?>
<form method="post"
	action="<?php echo base_url() . 'admin/import/import_listener' ?>"
	enctype="multipart/form-data">
	<table width="100%" class="table table-striped table-condensed">

		<tr>
			<td>Insurance form:</td>

			<td><input type="file" name="insurance_file" id="insurance_file_id">
				<br /></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="3"><input type="submit" value="Import"></td>
		</tr>
	</table>
</form>

