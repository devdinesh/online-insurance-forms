<?php
$filename = date('Ymd-His') . '-Import.xls';
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . $filename);
header('Pragma: no-cache');
header('Expires: 0');
?>
<table  width="100%" border="1" cellspacing="0" cellpadding="0" >
    <thead>
        <tr>
            <th width="10%">Client Naam</th>
            <th width="10%">File Naam</th>
            <th width="10%">File Location</th>
            <th width="10%">Date</th>
            <th width="10%">Time</th>
            <th width="10%">Status</th>
        </tr>
    </thead>
    <tbody>

        <?php
        if (count($import_list) > 0) {
            foreach ($import_list AS $import) {
                if ($import->claim_id == NULL) {
                    $status = "Rejected";
                } else {
                    $status = "Imported";
                }

                echo '<tr>';
                echo '<td>' . $client_name . '</td>';
                echo '<td>' . $import->file_name . '</td>';
                echo '<td>' . $import->file_location . '</td>';
                echo '<td>' . $import->date . '</td>';
                echo '<td>' . $import->time . '</td>';
                echo '<td>' . $status . '</td>';
                echo '</tr>';
            }
        }
        ?>
</table>
