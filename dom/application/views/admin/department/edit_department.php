<h2 class="title-margin"><span>Afdeling bewerken</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'admin/editListener_departments/' . $department->account_id; ?>"
        method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Naam<span class="mandatory">*</span></td>
                    <td><input type="text" name="name"
                               value="<?php
echo set_value('name') != '' ? set_value('name') : isset(
                $department->name) ? $department->name : '';
?>" /></td>
                    <td><?php echo form_error('name'); ?></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <button type="submit" class="btn btn-large btn-primary">Opslaan</button> &nbsp;&nbsp; 
                        <a  href="<?php echo base_url() . 'admin/departments'; ?>" class="btn btn-large btn-primary">Annuleer</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">Velden gemarkeerd met <span class="mandatory">*</span> zijn verplicht.</td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
