<script src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script> 
<div class="container">
    <div class="row-fluid">
        <div class="span12">
            <form
                action="<?php echo base_url() . "admin/authenticate/resetPasswordListener/" . $randid; ?>"
                method="post">
                <table border="0" class="table table-striped table-condensed">
                    <tr>
                        <td colspan="3"><h3>Reset Password</h3></td>
                    </tr>

                    <?php
                    if (isset($login_error)) {
                        echo "<tr><td colspan=\"3\"> " .
                        htmlentities($login_error, ENT_QUOTES) .
                        "</td></tr>";
                    }
                    ?>

                    <tr>
                        <td>New Password <span class="mendetory">*</span>
                        </td>
                        <td><input type="password" name="new_password"></td>
                        <td><?php echo form_error('new_password'); ?></td>
                    </tr>

                    <tr>
                        <td>Confirm Password <span class="mendetory">*</span>
                        </td>
                        <td><input type="password" name="confirm_password"></td>
                        <td><?php echo form_error('confirm_password'); ?></td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            <button type="submit" class="btn btn-large btn-primary">Reset Password</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">Fields marked with <span class="mendetory">*</span>
                            are mandatory.
                        </td>
                    </tr>
                </table>
            </form>
        </div>

    </div><!--/row-->
</div><!--/.fluid-container-->


