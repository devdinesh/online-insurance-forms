
<script src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script> 
<script>
    //<![CDATA[
    $(document).ready(function(){
        $(document).ready(function(){
            $("#logindform").validate({
                rules: {
                    "email": {
                        required: true,
                        email: true
                    },
                    "password": {
                        required: true
                    }
                },
                onkeyup: false,
                onblur: true
            });
        }); 
       
    });
    //]]>
</script>
<div class="container">
    <div class="row-fluid">
        <div class="span12">

            <form  action="<?php echo base_url(); ?>admin/login?redirect_to=<?php echo @$redirect_to; ?>" name="logindform" id="logindform" class="form-signin" method="post">
                <input type="hidden" name="redirect_to"  value="<?php echo @$redirect_to; ?>">
                <h2 class="form-signin-heading"><span>Login</span></h2>
                <div class="front-login-error">
                    <?php
                    if (isset($login_error) && $login_error != '') {
                        echo $login_error;
                    }
                    ?>
                    <?php if ($this->session->flashdata('success') != '') { ?>
                        <br/>
                        <div class="alert alert-success">
                            <a class="close" data-dismiss="alert"
                               href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } ?>
                    <?php if ($this->session->flashdata('error') != '') { ?>
                        <br/>
                        <div class="alert alert-error">
                            <a class="close" data-dismiss="alert"
                               href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                </div>
                <input  type="text" name="email" id='email_err' data-placement='bottom' data-original-title='<?php
                    if (form_error('email')) {
                        $tempa1 = str_replace('<p class="label-error">', '', form_error('email'));
                        $tempa2 = str_replace('</p>', '', $tempa1);
                        echo $tempa2;
                    }
                    ?>' value="<?php echo htmlentities(set_value('email')); ?>" placeholder="E-mailadres" class="input-block-level">
                <input type="password" name="password" id='password_err' data-placement='bottom' data-original-title='<?php
                        if (form_error('password')) {
                            $temp1 = str_replace('<p class="label-error">', '', form_error('password'));
                            $temp2 = str_replace('</p>', '', $temp1);
                            echo $temp2;
                        }
                    ?>'  class="input-block-level" placeholder="Wachtwoord">

                <label><a href="<?php echo base_url(); ?>forgotpassword"> Wachtwoord Vergeten?</a></label>
                <button class="btn btn-large btn-primary" type="submit">inloggen</button>
            </form>
        </div>

    </div><!--/row-->
</div><!--/.fluid-container-->
