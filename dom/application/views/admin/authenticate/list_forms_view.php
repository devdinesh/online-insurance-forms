<div class="row-fluid">
	<p class="span12">
		<a href="<?php echo base_url() . "admin"; ?>"> Home </a> &gt; Forms
	</p>
</div>
<h2>List All Forms</h2>
<div class="row-fluid">
	<p class="span12">
		<a href="#"><button type="button" class="btn">Add New Form</button></a>
	</p>
</div>

<div>
	<table width="100%" cellspacing="0" cellpadding="0">
        <?php if ($this->session->flashdata('success') != '') { ?>
            <tr>
			<td>
				<div class="alert alert-success">
					<a class="close" data-dismiss="alert"
						href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('success'); ?>
                    </div>
			</td>
		</tr>
        <?php } ?>
        <?php if ($this->session->flashdata('error') != '') { ?>
            <tr>
			<td>
				<div class="alert alert-error">
					<a class="close" data-dismiss="alert"
						href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('error'); ?>
                    </div>
			</td>
		</tr>
        <?php } ?>
    </table>
	<div class="clear"></div>
	<table class="table table-striped table-condensed"
		id="list_subscription_kinds" width="100%" border="1" cellspacing="0"
		cellpadding="0">
		<thead>
			<tr>
				<th width="15%">Name</th>
				<th width="15%">Client</th>
				<th width="15%">Form Code</th>
				<th width="5%">Categories</th>
				<th width="5%">Operations</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>etc</td>
				<td>etc</td>
				<td>etc</td>
				<td>etc</td>
				<td>etc</td>

			</tr>
		</tbody>
	</table>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_subscription_kinds').dataTable( {
            //  "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "bProcessing": true, 
            'iDisplayLength': 100,     
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"}
            ],
            "sAjaxSource": "<?php echo site_url("admin/client_forms/getJson"); ?>"
        } );
    } );
    
    function deleteRow(ele){
        var current_id = $(ele).attr('id');
        //   var parent = $(ele).parent().parent();
        if( confirm("Do you want to delete ?"))
        {
            location.href="#";
        }
                    
        return false;
    }
</script>