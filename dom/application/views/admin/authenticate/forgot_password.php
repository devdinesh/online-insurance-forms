<script>
    //<![CDATA[
    $(document).ready(function(){
        $("#forgotpassword").validate({
        });
    });
    //]]>
</script>
<div class="container">
    <div class="row-fluid">
        <div class="span12">
            <form  action="<?php echo base_url(); ?>admin/authenticate/forgotpassword_listener" name="forgotpassword" id="forgotpassword" class="form-signin" method="post">

                <input type="hidden" name="redirect_to"  value="<?php echo @$redirect_to; ?>">
                <h2 class="form-signin-heading"><span>Login</span></h2>
                <div class="front-login-error">
                    <?php
                    if (isset($login_error) && $login_error != '') {
                        echo htmlentities($login_error, ENT_QUOTES);
                    }
                    ?>
                </div>
                <input  type="email" name="email" id='email_err' data-placement='bottom' data-original-title='<?php
                    if (form_error('email')) {
                        $tempa1 = str_replace('<p class="label-error">', '', form_error('email'));
                        $tempa2 = str_replace('</p>', '', $tempa1);
                        echo $tempa2;
                    }
                    ?>' value="<?php echo htmlentities(set_value('email')); ?>" placeholder="E-mailadres"  required="required" class="input-block-level ">
                <button class="btn btn-large btn-primary" type="submit">Verstuur</button>
            </form>
        </div>

    </div><!--/row-->
</div><!--/.fluid-container-->
 