<h2 class="title-margin"><span>Contact</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'admin/dashboard/add_content'; ?>"
        method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-condensed"
               border="0">
            <tbody>
                <tr>
                    <td style="width:15%;">Subject <span class="mandatory">*</span></td>
                    <td style="width:35%;"><input class="input-append" type="text" name="subject"
                                                  value="<?php echo set_value('subject'); ?>" /></td>
                    <td><?php echo form_error('subject'); ?></td>
                </tr>
                <tr>
                    <td>Message <span class="mandatory">*</span></td>
                    <td><textarea id="headder_text" name="message" rows="5"
                                  class="tinymce"><?php echo (set_value('message')); ?></textarea></td>
                    <td><?php echo form_error('message'); ?></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <button type="submit" class="btn btn-large btn-primary">Opslaan</button> &nbsp;&nbsp;
                        <a href="<?php echo base_url() . 'admin/dashboard'; ?>" class="btn btn-large btn-primary">Annuleer</a>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                        zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>
<div class="clear"></div>