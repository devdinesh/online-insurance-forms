<h2 class="title-margin"><span>Toevoegen Vraag</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'admin/categories_question/addListener/' . $category_details->cat_id; ?>"
        method="post">
        <input type="hidden" name="category_id"
               value="<?php echo $category_details->cat_id; ?>">

        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>
                <tr>
                    <td>Categorie</td>
                    <td><?php echo $category_details->cat_name; ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Vraag<span class="mandatory">*</span></td>
                    <td><textarea id="question" name="question" rows="5"
                                  class="tinymce">
                                      <?php echo (set_value('question')); ?>
                        </textarea></td>
                    <td>
                        <?php echo form_error('question'); ?>
                        <span style="color: red;">
                            <?php echo @$this->session->flashdata('error-text'); ?>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Helptekst</td>
                    <td colspan="2"><textarea id="help_text" class="custom-font-16"
                                              name="help_text" rows="5"><?php echo (set_value('help_text')); ?></textarea></td>
                </tr>
                <tr>
                    <td>Is Verplicht</td>
                    <td><input type="checkbox" name="required" /></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Antwoordsoort<span class="mandatory">*</span></td>
                    <td colspan="1">
                        <div class="span4" style="margin-left: 0px;">
                            <label class="radio">
                                <input type="radio" name="answer_kind" value="date" onclick="checkselection(this)" <?php
                            if (set_value('answer_kind') == 'date') {
                                echo "checked=\"checked\"";
                            }
                            ?>> Enkele regel (datum)
                            </label>
                            <label class="radio">
                                <input type="radio" name="answer_kind" value="number" onclick="checkselection(this)" <?php
                                       if (set_value('answer_kind') == 'number') {
                                           echo "checked=\"checked\"";
                                       }
                            ?>>
                                Enkele regel (getal)  
                            </label>
                            <label class="radio">
                                <input type="radio" name="answer_kind" value="text" onclick="checkselection(this)" <?php
                                       if (set_value('answer_kind') == 'text') {
                                           echo "checked=\"checked\"";
                                       }
                            ?>>
                                Enkele regel (tekst)
                            </label>
                            <label class="radio">
                                <input type="radio" name="answer_kind" value="financieel" onclick="checkselection(this)" <?php
                                       if (set_value('answer_kind') == 'financieel') {
                                           echo "checked=\"checked\"";
                                       }
                            ?>>
                                Financieel
                            </label>
                        </div>
                        <div class="span3">
                            <label class="radio">
                                <input type="radio" name="answer_kind"
                                       value="textarea" onclick="checkselection(this)"
                                       <?php
                                       if (set_value('answer_kind') == 'textarea') {
                                           echo "checked=\"checked\"";
                                       }
                                       ?>>
                                Meerdere regels
                            </label>
                            <label class="radio">
                                <input type="radio" name="answer_kind"
                                       value="radio" onclick="checkselection(this)"
                                       <?php
                                       if (set_value('answer_kind') == 'radio') {
                                           echo "checked=\"checked\"";
                                       }
                                       ?>>
                                Enkele selectie
                            </label>
                            <label class="radio">
                                <input type="radio" name="answer_kind"
                                       value="checkbox" onclick="checkselection(this)"
                                       <?php
                                       if (set_value('answer_kind') == 'checkbox') {
                                           echo "checked=\"checked\"";
                                       }
                                       ?>>
                                Meerdere selecties
                            </label>
                        </div>
                    </td>
                    <td>
                        <?php echo form_error('answer_kind'); ?>
                    </td>
                </tr>
                <tr id="show_on_selection2" style="display: none">
                    <td>&nbsp;</td>
                    <td colspan="2"><input type='button' class="btn" value='+ Add Answer'
                                           id='addButton'></td>
                </tr>
                <tr id="show_on_selection3" style="display: none">
                    <td>&nbsp;</td>
                    <td colspan="2"><input type="hidden" name="total_runtime_checkbox"
                                           id="total_runtime_checkbox" value="2" />
                        <div id='TextBoxesGroup'>
                            <div id="TextBoxDiv1">
                                <input type='textbox' id='textbox1' class="custom-textfield" name="answers1" /> 
                                <?php echo form_error('answers1'); ?>
                            </div>
                            <div id="TextBoxDiv2">
                                <input type='textbox' id='textbox2' class="custom-textfield" name="answers2" /> 
                                <?php echo form_error('answers2'); ?>
                            </div>
                        </div></td>
                </tr>
                <tr>
                    <td colspan="3"><span id="next_step">  <button type="submit" class="btn btn-large btn-primary">Opslaan</button></span> &nbsp;&nbsp; 
                        <a href="<?php echo base_url() . 'admin/categories_question/' . $category_details->cat_id; ?>" class="btn btn-large btn-primary">Annuleer</a>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                        zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>
<script src="<?php echo assets_url_js; ?>jquery-ui.min.js"></script>
<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',
            // General options
            theme : "advanced",
            mode : "exact",
            elements : "ajaxfilemanager",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
       
            theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,|,bullist,numlist,|,undo,redo,|,link,unlink,image",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,
            //add icons to upload /maintain images/files at the server
            file_browser_callback : "ajaxfilemanager", 

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });

    //add icons to upload /maintain images/files at the server

    function ajaxfilemanager(field_name, url, type, win) {
        var ajaxfilemanagerurl = "<?php echo base_url(); ?>assets/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
        var view = 'detail';
        switch (type) {
            case "image":
                view = 'thumbnail';
                break;
            case "media":
                break;
            case "flash": 
                break;
            case "file":
                break;
            default:
                return false;
        }
        tinyMCE.activeEditor.windowManager.open({
            url: "<?php echo base_url(); ?>assets/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
            width: 782,
            height: 440,
            inline : "yes",
            close_previous : "no"
        },{
            window : win,
            input : field_name
        });
    
    }
    
    
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var counter = 3;
        $("#addButton").click(function () {
            var string = '<div id="TextBoxDiv'+counter+'"><input type="textbox" class="custom-textfield" id="textbox'+counter+'" name="answers'+counter+'" /><a id="' +counter+ '" onclick="removeans(this)"><img src="<?php echo base_url() . 'assets/img/'; ?>icon_delete.png" width="14" height="15" alt="Delete" /></a>';
            jQuery('#TextBoxesGroup').append(string);
            jQuery('#total_runtime_checkbox').val(counter);
            counter++;
        });
        checkonpageLoad();
    });
    
    function checkonpageLoad(){
        if($('input[type="radio"]:checked').val() == 'radio' || $('input[type="radio"]:checked').val() == 'checkbox'){
            $("#show_on_selection1").css("display", 'table-row');
            $("#show_on_selection2").css("display", 'table-row');
            $("#show_on_selection3").css("display", 'table-row');
        }else{
            $("#show_on_selection1").css("display", 'none');
            $("#show_on_selection2").css("display", 'none');
            $("#show_on_selection3").css("display", 'none');
        }
    }
    
    function removeans(ele){
        var current_id = $(ele).attr('id');
        $("#TextBoxDiv" + current_id).remove();
    }
    
    function checkselection(ele){
        // alert(ele.value);
        if(ele.value == 'radio' || ele.value == 'checkbox'){
            $("#show_on_selection1").css("display", 'table-row');
            $("#show_on_selection2").css("display", 'table-row');
            $("#show_on_selection3").css("display", 'table-row');
        }else{
            $("#show_on_selection1").css("display", 'none');
            $("#show_on_selection2").css("display", 'none');
            $("#show_on_selection3").css("display", 'none');
        }
    }
</script>
<style>
    #TextBoxesGroup img {
        margin: -7px 0 0 10px;
    }

    #TextBoxesGroup div { margin: 10px 0; }
</style>