<script type="text/javascript">
    function field_to_remove(field_name)
    {
        //   alert(field_name);
        var form = document.getElementById("form_add");
        
        var hidden_element = document.createElement("input");

        hidden_element.setAttribute("type", "hidden");

        hidden_element.setAttribute("name", "element_to_be_removed");

        hidden_element.setAttribute("value", field_name);

        //append to form element that you want .
        form.appendChild(hidden_element);
        form.submit();
        //alert(form.innerHTML);
    }
</script>
<h2 class="title-margin" >Add Answers</h2>
<?php
if ($this->session->flashdata('message_error')) {
    ?>

    <!-- <div class="alert-message error fade in hide span16" data-alert="alert">
             <a class="close" href="#">&times;</a>
             <p><?php echo $this->session->flashdata('message_error') ?></p>
         </div> -->


    <div class="alert alert-error">
        <?php echo $this->session->flashdata('message_error'); ?>
    </div>

    <?php
}
?>
<div>
    <table class="table table-striped">
        <form method="POST" id="form_add"
              action="<?php echo base_url() . "admin/admin_forms_categories_question/add_answer"; ?>">
            <input type="hidden" name="question"
                   value="<?php echo htmlentities($question); ?>"> <input type="hidden"
                   name="help_text" value="<?php echo htmlentities($help_text); ?>"> <input
                   type="hidden" name="required"
                   value="<?php echo htmlentities($required); ?>"> <input type="hidden"
                   name="sequence" value="<?php echo htmlentities($sequence); ?>"> <input
                   type="hidden" name="answer_kind"
                   value="<?php echo htmlentities($answer_kind); ?>"> <input
                   type="hidden" name="category_id"
                   value="<?php echo htmlentities($category_id); ?>">
                   <?php $ans_id = 1; ?>
            <tr>
                <td colspan="3"><input type="submit" class="btn" name="action"
                                       value="Add Answer"></td>
            </tr>
            <?php
            for (; $ans_id <= $answer_count; ++$ans_id) {
                $name_var = 'ans_' . $ans_id;
                if (isset($$name_var)) {
                    ?>
                    <tr>
                        <td>Answer</td>
                        <td><input type="text" class="input" name="<?php echo $name_var ?>"
                                   value="<?php echo htmlentities($$name_var) ?>" /> &nbsp; <input
                                   type="button" class="btn" class="input" name="action"
                                   value="Remove"
                                   onclick="field_to_remove('<?php echo $name_var; ?>')" /></td>
                        <td>
        <?php echo form_error($name_var); ?> 
                        </td>
                    </tr>
                            <?php
                        }
                    }
                    ?>

            <?php
            if (isset($add_row) && $add_row) {
                ?>
                <tr>
                    <td>Answer:</td>
                    <td><input type="text"
                               name="<?php
                // ++$ans_id;
                echo 'ans_' . ( $ans_id );
                $answer_count++;
                ?>"
                               value=""></td>
                </tr>
                               <?php
                           }
                           ?>
            <tr>
                <td colspan="3"><input type="submit" class="btn" name="action"
                                       claas="btn btn-primary" value="Save"></td>
            </tr>

            <input type="hidden" name="answer_count"
                   value="<?php echo htmlentities($answer_count); ?>">
        </form>
    </table>
</div>