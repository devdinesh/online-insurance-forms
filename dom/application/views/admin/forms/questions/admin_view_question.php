
<h2 class="title-margin"><span>Overzicht vragen van de categorie <?php echo $category_details->cat_name; ?> </span></h2>
<div class="row-fluid">
    <p class="span12">
        <a
            href="<?php echo base_url() . 'admin/categories_question/add/' . $category_details->cat_id; ?>"
            class="btn btn-large btn-primary">Toevoegen vraag</a> <a
            href="<?php echo base_url() . 'admin/categories/' . $category_details->form_id; ?>"
            class="btn btn-large btn-primary">Terug</a>
    </p>
</div>
<div>
    <table class="table table-striped table-condensed"
           id="list_category_question" width="100%" border="0" cellspacing="0"
           cellpadding="0" title="Ga met de muis op een vraag staan en sleep deze naar de juiste positie om de volgorde te wijzigen.">
        <thead>
            <tr>
                <th width="9%">Vraagnummer</th>
                <th>Vraag</th>
                <th width="10%">Antwoorden</th>
                <th width="10%"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>etc</td>
                <td>etc</td>
                <td>etc</td>
                <td>etc</td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_category_question').dataTable( {
            //  "bJQueryUI": true,
            "bProcessing":true,
            "bPaginate" : false,
            "bFilter": false,
            "bInfo": false,
            "bSort": false, 
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"},{"sClass":"align_center"}
            ],
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": "<?php echo site_url("admin/admin_forms_categories_question/getJson/$category_details->cat_id"); ?>",
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                $(nRow).attr('id', aData[4]);
            }
        } );
        $(function() {
            $("#list_category_question tbody").sortable({ opacity: 0.6, cursor: 'move', update: function() {
                    var order = $(this).sortable("serialize");
                    var url = "<?php echo base_url() . 'admin/categories_question/sortable'; ?>";
                    $.post(url, order, function(){
                        window.location.reload();
                    });   
                }                                
            });
        });
    } );
    
    function deleteRow(ele){
        var id = $(ele).attr('id');
        //   var parent = $(ele).parent().parent();
        if( confirm("Do you want to delete ?"))
        {
            location.href="<?php echo base_url(); ?>admin/admin_forms_categories_question/deleteQuestion/" +<?php echo $category_details->cat_id; ?>+"/"+id;
        }
                    
        return false;
    }
</script>