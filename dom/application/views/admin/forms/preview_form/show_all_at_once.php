<?php if (isset($show_data) && $show_data == 1) { ?>
    <script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery-1.8.3.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>bootstrap-tooltip.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery-ui.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery.validate.js"></script>

    <style>
        label { display: inline-block; }
        .multiform-margin { margin-right: 20px; }
        form { margin: 0px; }
    </style>
<?php } ?>

<script>
    Number.prototype.formatMoney = function(c, d, t){
        var n = this, 
        c = isNaN(c = Math.abs(c)) ? 2 : c, 
        d = d == undefined ? "." : d, 
        t = t == undefined ? "," : t, 
        s = n < 0 ? "-" : "", 
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
        j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    
    jQuery(document).ready(function(){
<?php if ($show_end_data == TRUE) { ?>
            jQuery("#display_back").hide();
<?php } ?>
        jQuery("#main_frm").validate({
            onfocusout:function(element) {
                if($(element).hasClass('currency')){
                    return false;
                }else{
                    return true;
                }
            },
            onkeyup: function(element) {
                if($(element).hasClass('currency')){
                    return false;
                }else{
                    return true;
                }
            },
            errorElement:'span'
        });
        $.validator.addMethod("currency", function (value, element) {
            return this.optional(element) || /^\€ (\d{1,3}(\.\d{3})*|(\d+))(\,\d{2})?$/.test(value);
        }, "Please enter valid financially amount."); 
        jQuery( document ).tooltip();
        jQuery(".datepicker").datepicker({  changeYear: true,changeMonth:true,yearRange:'-90:+20',dateFormat: "dd/mm/yy" }).val();
    });
    function formate_currency(obj){
        var value=Number($(obj).val());
        if(isNaN(value)===false && $(obj).val().length > 0){
            $(obj).val('€ '+value.formatMoney(2,',','.'));
        }
    }
</script>
<style type="text/css">
    .table{
        border:none !important;
        background:#fff !important;
    }
    .row-fluid .span12 h2 span{
        font-size: 23px;
        padding: 0 5px 7px 0;}
    .row-fluid .span12 h3{
        font-size: 18px;
    }
</style>
<!-- actual page content begins -->
<?php if (isset($show_data) && $show_data == 1) { ?>
    <form id='main_frm' method="post" action="#">
        <div style="font-size: 14px; font-weight: normal;" id="main_div">
            <h2 class="title-margin"><span><?php echo $form->form_name; ?></span></h2>
            <table class="table" style="margin-top: 10px;">
                <tr>
                    <td>
                        <?php echo $form->introduction_text; ?>
                    </td>
                </tr>
            </table>

            <!-- questions -->

            <input type="hidden" name="action" value="">
            <input type="hidden" name="last_answer_id" id="last_answer_id" value="">
        <?php } ?>
        <table class="table email-data-table">
            <?php
            if (isset($category_info)) {
                foreach ($category_info as $cat_info) {
                    if (isset($prev_cat_id) && $prev_cat_id != '') {
                        if ($prev_cat_id != $cat_info->cat_id) {
                            ?>
                            <tr>
                                <td colspan="3">
                                    <h3><?php echo $cat_info->cat_name; ?> </h3>
                                    <?php echo $cat_info->introduction_text; ?>        
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="3">
                                <h3><?php echo $cat_info->cat_name; ?> </h3>
                                <?php echo $cat_info->introduction_text; ?>        
                            </td>
                        </tr>

                        <?php
                    }
                    if (isset($questions)) {
                        if (is_array($questions) && count($questions) > 0) {
                            foreach ($questions as $question) {
                                if ($cat_info->cat_id == $question->cat_id) {
                                    if ($question->answer_kind == 'text') {
                                        ?>
                                        <tr id="<?php echo 'view_' . $question->question_id; ?>">
                                            <td width="55%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <!--  help text begins --> <span
                                                <?php
                                                if ($question->help_text != Null) {
                                                    ?>
                                                        title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                            class="icon-question-sign"></i>
                                                        <?php } ?>
                                                </span> <!--  help text ends --></td>
                                            <td width="25%"><input
                                                    name="<?php echo $question->question_id, '_'; ?>" type="text"
                                                    <?php echo $question->required == 1 ? 'class="input required"' : 'class="input"'; ?>
                                                    value="<?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                                    ?>"></td>

                                            <td width="20%">
                                                <?php echo form_error($question->question_id . "_"); ?>
                                            </td>

                                        </tr>
                                        <?php
                                    } else if ($question->answer_kind == 'date') {
                                        ?>
                                        <tr id="<?php echo 'view_' . $question->question_id; ?>">
                                            <td width="55%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <!--  help text begins --> <span
                                                <?php
                                                if ($question->help_text != Null) {
                                                    ?>
                                                        title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                            class="icon-question-sign"></i>
                                                        <?php } ?>
                                                </span> <!--  help text ends --></td>
                                            <td width="25%"><input 
                                                    name="<?php echo $question->question_id, '_'; ?>" type="text"  <?php echo $question->required == 1 ? 'class="input datepicker required date"' : 'class="input datepicker date"'; ?>
                                                    value="<?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                                        ?>"></td>

                                            <td width="20%">
                                                <?php echo form_error($question->question_id . "_"); ?>
                                            </td>

                                        </tr>
                                        <?php
                                    } else if ($question->answer_kind == 'number') {
                                        ?>
                                        <tr id="<?php echo 'view_' . $question->question_id; ?>">
                                            <td width="55%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <!--  help text begins --> <span
                                                <?php
                                                if ($question->help_text != Null) {
                                                    ?>
                                                        title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                            class="icon-question-sign"></i>
                                                        <?php } ?>
                                                </span> <!--  help text ends --></td>
                                            <td width="25%"><input
                                                    name="<?php echo $question->question_id, '_'; ?>" type="text"
                                                    <?php echo $question->required == 1 ? 'class="input number required"' : 'class="input number"'; ?>
                                                    value="<?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                                    ?>"></td>

                                            <td width="20%">
                                                <?php echo form_error($question->question_id . "_"); ?>
                                            </td>

                                        </tr>
                                        <?php
                                    } else if ($question->answer_kind == 'financieel') {
                                        ?>
                                        <tr id="<?php echo 'view_' . $question->question_id; ?>">
                                            <td width="55%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <!--  help text begins --> <span
                                                <?php
                                                if ($question->help_text != Null) {
                                                    ?>
                                                        title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                            class="icon-question-sign"></i>
                                                        <?php } ?>
                                                </span> <!--  help text ends --></td>
                                            <td width="25%"><input
                                                    name="<?php echo $question->question_id, '_'; ?>" type="text"
                                                    <?php echo $question->required == 1 ? 'class="input currency required"' : 'class="input currency"'; ?>
                                                    onchange="formate_currency(this);" value="<?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                                    ?>"></td>

                                            <td width="20%">
                                                <?php echo form_error($question->question_id . "_"); ?>
                                            </td>

                                        </tr>
                                        <?php
                                    } else if ($question->answer_kind == 'textarea') {
                                        ?>
                                        <tr id="<?php echo 'view_' . $question->question_id; ?>">
                                            <td width="55%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <!--  help text begins -->
                                                <?php
                                                if ($question->help_text != null) {
                                                    ?>
                                                    <span
                                                        title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                            class="icon-question-sign"></i>
                                                    </span>
                                                <?php } ?>
                                                <!--  help text ends --> </span>
                                            </td>
                                            <td width="25%"><textarea onkeyup="textAreaAdjust(this)"
                                                                      name="<?php echo $question->question_id . "_"; ?>"
                                                                      <?php echo $question->required == 1 ? "required" : ""; ?>><?php
                                          if ($this->input->post($question->question_id . '_') != '') {
                                              echo $this->input->post($question->question_id . '_');
                                          } else {
                                              $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                                              if (isset($user_ans)) {
                                                  echo htmlentities($user_ans->answer_text);
                                              }
                                          }
                                                                      ?></textarea></td>

                                            <td width="20%">
                                                <?php echo form_error($question->question_id . "_"); ?>
                                            </td>
                                        </tr>
                                        <?php
                                    } else if ($question->answer_kind == 'radio') {
                                        ?>
                                        <tr id="<?php echo 'view_' . $question->question_id; ?>">
                                            <td width="55%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?> <!--  help text begins -->
                                                <?php
                                                if ($question->help_text != Null) {
                                                    ?>
                                                    <span
                                                        title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                            class="icon-question-sign"></i>
                                                    </span>
                                                <?php } ?>
                                                <!--  help text ends -->
                                            </td>
                                            <td width="25%" id="radio-tr">
                                                <?php
                                                $count_check_raio = 0;
                                                // get the options and interate thru them
                                                $this->load->model(
                                                        'forms_categories_question_model');
                                                $answers = $this->forms_categories_question_answer_model->get_where(
                                                        array(
                                                            'question_id' => $question->question_id
                                                        ));
                                                $skip_question = false;
                                                foreach ($answers as $answer) {
                                                    if ($answer->skip_to_questions != NULL) {
                                                        $skip_question = true;
                                                        break;
                                                    }
                                                }

                                                $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);

                                                foreach ($answers as $answer) {
                                                    $count_check_raio++;
                                                    if (isset($skip_question) && $skip_question == true) {
                                                        ?>
                                                        <label class="radio"> 
                                                            <input name="<?php echo (isset($question->question_id) ? $question->question_id : '') . "_"; ?>" type="radio" <?php
                                    if ($this->input->post($question->question_id . '_') == $answer->answer_id) {
                                        echo "checked='checked'";
                                    } elseif (isset($user_ans)) {
                                        if ($user_ans->answer_text == $answer->answer_id) {
                                            echo "checked='checked'";
                                        }
                                    }
                                                        ?> value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>"  class="input" <?php echo $question->required == 1 ? "" : ""; ?> onclick="checkselection(this)" /> <?php echo $answer->answer; ?>
                                                        </label>&nbsp;<br />
                                                        <?php $last_question_name = $question->question_id . '_'; ?>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <label class="radio"> 
                                                            <input name="<?php echo (isset($question->question_id) ? $question->question_id : '') . "_"; ?>" type="radio" <?php
                                    if ($this->input->post($question->question_id . '_') == $answer->answer_id) {
                                        echo "checked='checked'";
                                    } elseif (isset($user_ans)) {
                                        if ($user_ans->answer_text == $answer->answer_id) {
                                            echo "checked='checked'";
                                        }
                                    }
                                                        ?> value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>"  <?php
                                    if ($count_check_raio == 1) {
                                        echo $question->required == 1 ? 'class="input required"' : 'class="input"';
                                    }
                                                        ?>  /> <?php echo $answer->answer; ?>
                                                        </label>&nbsp;<br />
                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </td>
                                            <td width="20%">
                                                <?php echo form_error($question->question_id . "_"); ?>
                                            </td>
                                        </tr>
                                        <?php
                                    } else if ($question->answer_kind == 'checkbox') {
                                        ?>
                                        <tr id="<?php echo 'view_' . $question->question_id; ?>">
                                            <td width="55%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <!--  help text begins -->
                                                <?php
                                                if ($question->help_text != Null) {
                                                    ?>
                                                    <span
                                                        class="icon-question-sign"
                                                        title="<?php echo htmlentities($question->help_text); ?>"></span>
                                                    <?php } ?>
                                                <!--  help text ends -->
                                            </td>
                                            <td width="25%" id="checkbox-tr">
                                                <?php
                                                $count_check = 0;

                                                // get the options and interate thru them
                                                $this->load->model(
                                                        'forms_categories_question_answer_model');
                                                $forms_categories_question_answer_model = new forms_categories_question_answer_model();
                                                $answers = $forms_categories_question_answer_model->get_where(
                                                        array(
                                                            'question_id' => $question->question_id
                                                        ));
                                                $user_ans = get_answer_given_by_admin(
                                                        $claim_id, $question->question_id);

                                                $this->load->model(
                                                        'forms_answers_details_model');
                                                $obj_form_answer = new forms_answers_details_model();
                                                if (isset($user_ans)) {
                                                    $re_ans = $obj_form_answer->get_where(
                                                            array(
                                                                'user_answer_id' => $user_ans->answer_id
                                                            ));
                                                }
                                                foreach ($answers as $answer) {
                                                    $count_check++;
                                                    ?>
                                                    <label
                                                        class="checkbox checkbox-align_preview_form label_checkbox"
                                                        style="line-height: 10px!importan; margin-bottom: -15px;"> <input
                                                            name="<?php echo $question->question_id, '_[]'; ?>"
                                                            <?php
                                                            if (isset($re_ans)) {
                                                                foreach ($re_ans as $r_ans) {
                                                                    if ($r_ans->answer_id == $answer->answer_id) {
                                                                        echo "checked='checked'";
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                            id="<?php echo $question->question_id, '_', $count_check; ?>"
                                                            type="checkbox" <?php echo set_checkbox($question->question_id . '_', $answer->answer_id); ?>  value="<?php echo $answer->answer_id; ?>" 
                                                            <?php
                                                            if ($count_check == 1) {
                                                                echo $question->required == 1 ? 'class="input required"' : 'class="input"';
                                                            }
                                                            ?> > <?php echo $answer->answer; ?> </input>
                                                    </label> &nbsp;
                                                    <?php
                                                }
                                                ?>
                                                <input type="hidden"
                                                       value="<?php echo $count_check; ?>"
                                                       name='<?php echo $question->question_id, '_', 'count' ?>'>

                                            </td>
                                            <td width="20%">
                                                <?php echo form_error($question->question_id . "_"); ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                <?php
                            }
                        }
                    }
                }
            }
            ?>
        </table>
        <div id="email_data"></div>
        <?php if ($show_end_data == TRUE) { ?>
            <table class="table ">
                <tr id="end_form">
                    <td style="vertical-align: top">
                        <img class="pull-left" src='<?php echo assets_url_img . "exclamation.png"; ?>' alt='exclamation' title='exclamation' style="margin: 3px;">
                    </td>
                    <td> <?php echo $form->closure_text; ?></td>
                </tr>
            </table>
            <table style="background: #fff;">
                <?php
                $this->load->model('claims_files_model');
                $files = claims_files_model::get_claims_for_claim_id($claim_id);

                if (count($files) == 0) {
                    echo "<tr><td colspan=\"3\"><b>Bijlagen</b></td></tr>";
                } else {
                    echo "<tr><td colspan=\"3\"><b>Bijlagen</b><br/>";
                    foreach ($files as $file) {
                        echo anchor($file->get_path_for_web(), $file->file_name, 'target="_blank"');
                        echo '&nbsp;&nbsp;&nbsp;';
                        echo anchor(
                                base_url() . 'user/answer/delete_attachment/' . $claim_id .
                                '/' . $form->form_id . '/' . $file->file_id, '<i class="icon-trash"></i>');
                        echo "<br>";
                    }
                    echo "</td></tr>";
                }
                ?>
                <tr>
                    <td colspan="3">
                        <form method="post" action="<?php echo base_url() . "user/answer/upload_attachment/" . $claim_id; ?>"  enctype="multipart/form-data">
                            <input type="hidden" disabled="disabled" name="form_id"   value="<?php echo $form->form_id; ?>" /> <input type="file"   disabled="disabled" class="input" name="attachment" /> <input  type="submit" disabled="disabled" value="Toevoegen" class="btn btn-large btn-primary">
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <label class="checkbox">
                            <input type="checkbox" value="yes" name="filled_in_honestly" id="filled_in_honestly"> Naar waarheid ingevuld en gecontroleerd. <font color="red">*</font> 
                            <span id="error-text" style="color: red;"></span><br>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="row-fluid">
                            <a href="<?php echo base_url() . 'admin/client_forms'; ?>"
                               class="btn btn-large btn-primary">Terug</a>
                            <!-- link for Opslaan button -->
                            <input type="button" value="Opslaan in concept" onclick="save_form()" class="btn btn-large btn-primary" />
                            <!-- link for Opslaan button -->
                            <input type="button" value="Bevestig en verstuur" onclick="confirm_and_send()" class="btn btn-large btn-primary" />
                        </div>
                    </td>
                </tr>
            </table>
            <?php
        } else {
            if (isset($show_data) && $show_data == 1) {
                ?>
                <table style="background: #fff;" id="display_back">
                    <tr>
                        <td colspan="3">
                            <div class="row-fluid">
                                <a href="<?php echo base_url() . 'admin/client_forms'; ?>"
                                   class="btn btn-large btn-primary">Terug</a>
                                   <?php if (!empty($category_info)) { ?>
                                    <input type="button" value="volgende" id="buttoOpslaanForm" onclick="opslaan_form()" class="btn btn-large btn-primary" />
                                <?php } ?>
                            </div>

                        </td>
                    </tr>
                </table>
                <?php
            }
        }
        ?>
        <?php if (isset($show_data) && $show_data == 1) { ?>
        </div>
    </form>
<?php } ?>
<script type="text/javascript">
<?php if ($show_end_data == TRUE) { ?>
        function check_confirm()
        {  
            if ( document.getElementById('filled_in_honestly').checked ==false) 
            {
                document.getElementById('error-text').innerHTML = 'Dit veld is verplicht';
                return false ;
            }
            else
            {
                return true;
            } 
        }
        function save_form()
        {
            document.forms['main_frm'].action.value = 'Opslaan in concept';
            jQuery('#main_frm').attr('action', '<?php echo base_url() . 'admin/client_forms/wholeFormSaveActionAtOnce/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence; ?>'); //this fails silently
            jQuery('#main_frm').get(0).setAttribute('action', '<?php echo base_url() . 'admin/client_forms/wholeFormSaveActionAtOnce/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence; ?>'); //this works
            document.getElementById("main_frm").submit();
        }

        function confirm_and_send()
        {  
            if(check_confirm()==false)
            {
                return false;
            }		
            else
            {
                document.forms['main_frm'].action.value = 'confirm';                                                                      
                if (jQuery('#main_frm').validate().form()){
                    $('#main_frm').attr('action', '<?php echo base_url() . 'admin/client_forms/wholeFormSaveActionAtOnce/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence; ?>'); //this fails silently
                    $('#main_frm').get(0).setAttribute('action', '<?php echo base_url() . 'admin/client_forms/wholeFormSaveActionAtOnce/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence; ?>'); //this works
                    document.getElementById("main_frm").submit();
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                }
            }
        }
<?php } ?>
    function checkselection(ele){
        var answer_id=ele.value;
        jQuery('#last_answer_id').val(answer_id);
        if (jQuery('#main_frm').validate().form()){
            var searzlie = jQuery('#main_frm').serialize();
            $('#buttoOpslaanForm').attr('disabled','disabled')
            jQuery.ajax({
                url : "<?php echo base_url() . 'admin/client_forms/get_smart_questions/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence ?>", 
                type: "post",
                data: searzlie,
                success: function(data){
                    jQuery('#email_data').append(data);
                    // console.log(data);
                    //jQuery('#email_data').html(data);
                    $('#buttoOpslaanForm').removeAttr('disabled');
                }
            });
           
        }
    }
    function opslaan_form(){
        var question_name='<?php echo @$last_question_name; ?>';
        var answer_id=$('[name='+question_name+']:checked').val();
        jQuery('#last_answer_id').val(answer_id);
        if (jQuery('#main_frm').validate().form()){
            if(answer_id!==undefined){
                $('#buttoOpslaanForm').attr('disabled','disabled');
                var searzlie = jQuery('#main_frm').serialize();
                jQuery.ajax({
                    url : "<?php echo base_url() . 'admin/client_forms/get_smart_questions/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence ?>", 
                    type: "post",
                    data: searzlie,
                    success: function(data){
                        jQuery('#email_data').append(data);
                        // console.log(data);
                        //jQuery('#email_data').html(data);
                        $('#buttoOpslaanForm').removeAttr('disabled');
                    }
                }); 
            }else{
                alert('Please fill the last question first.');
            }
        }
    }
</script>
<script>
    function textAreaAdjust(o) {
        o.style.height = "1px";
        o.style.height = (25+o.scrollHeight)+"px";
    }
</script>

