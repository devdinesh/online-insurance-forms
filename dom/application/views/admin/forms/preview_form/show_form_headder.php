<style type="text/css">
    .table{
        border:none !important;
        background:#fff !important;
    }
    .row-fluid .span12 h2 span{
        font-size: 23px;
        padding: 0 5px 7px 0;}
</style>
<div>
    <h2 class="title-margin"><span><?php echo $form->form_name; ?></span></h2>
    <div class="span11">
<!--        <h3><?php echo @$total_form_completed; ?>% form compleet</h3>-->
        <div class="progress progress-striped active">
            <div class="bar" style="width: <?php echo @$total_form_completed . '%'; ?>;"></div>
        </div>
    </div>

    <table class="table" style="margin-top: 10px;">
        <tr>
            <td>
                <?php echo $form->introduction_text; ?>
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td>
                <a href="<?php echo base_url() . 'admin/client_forms'; ?>" class="btn btn-large btn-primary">Terug</a>
                <?php if (isset($next_question)) { ?>
                    <a href="<?php echo base_url() . 'admin/client_forms/preview_question/' . $claim_id . '/' . $form->form_id . '/' . $next_question['next_cat']->cat_id . '/' . $next_question['next_question']->question_id; ?>" class="btn btn-large btn-primary">Volgende</a>
                <?php } ?>
            </td>
        </tr>
    </table>
</div>