<h2 class="title-margin"><span>Details formulierveld</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'admin/form_fields/editListener/' . $field->field_id; ?>"
        method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>

                <tr>
                    <td width="31%">Naam import item: <span class="mandatory">*</span></td>
                    <td><input class="input-append" type="text" name="field_name"
                               value="<?php echo htmlentities(set_value('field_name') !== '' ? set_value('field_name') : isset($field->field_name) ? $field->field_name : '');
?>" /></td>
                    <td><?php echo form_error('field_name'); ?></td>
                </tr>

                <tr>
                    <td>Weergeven als:<span class="mandatory">*</span></td>
                    <td><input type="text" name="name_on_form"
                               value="<?php echo htmlentities(set_value('name_on_form') !== '' ? set_value('name_on_form') : isset($field->name_on_form) ? $field->name_on_form : '');
?>" /></td>
                    <td><?php echo form_error('name_on_form'); ?></td>
                </tr>
                <tr>
                    <td>Markeer veld als: </td>
                    <td>
                        <label class="radio inline">
                            <input  type="radio" name="field_type" value="N" <?php
                               if ($field->field_type == 'N') {
                                   echo 'checked=checked';
                               }
?> />Normaal
                        </label>
                        <?php if (isset($show_mail) && ($show_mail == FALSE or $show_mail == $field->field_id)) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="E" <?php
                        if ($field->field_type == 'E') {
                            echo 'checked=checked';
                        }
                            ?>/>E-mail
                            </label>
                        <?php } ?>
                        <?php if (isset($show_ploicy_number) && ($show_ploicy_number == FALSE or $show_ploicy_number == $field->field_id)) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="P" <?php
                        if ($field->field_type == 'P') {
                            echo 'checked=checked';
                        }
                            ?>/>Polisnummer
                            </label>
                        <?php } ?>
                        <?php if (isset($show_schadenummer) && ($show_schadenummer == FALSE or $show_schadenummer == $field->field_id)) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="S" <?php
                        if ($field->field_type == 'S') {
                            echo 'checked=checked';
                        }
                            ?>/>Schadenummer
                            </label>
                        <?php } ?>
                        <?php if (isset($show_behandler) && ($show_behandler == FALSE or $show_behandler == $field->field_id)) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="B" <?php
                        if ($field->field_type == 'B') {
                            echo 'checked=checked';
                        }
                            ?>/>Behandelaar
                            </label>
                        <?php } ?>
                        <?php if (isset($show_behandler_emial) && ($show_behandler_emial == FALSE or $show_behandler_emial == $field->field_id)) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="BE" <?php
                        if ($field->field_type == 'BE') {
                            echo 'checked=checked';
                        }
                            ?>/>Behandelaar E-mail
                            </label>
                        <?php } ?>
                    </td>
                    <td><?php echo form_error('field_type'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>  &nbsp;&nbsp; 
                        <a href="<?php echo base_url() . 'admin/form_fields/' . $field->form_id; ?>"  class="btn btn-large btn-primary">Annuleer</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                        zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
