<h2 class="title-margin"><span>Details formulierveld</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'admin/form_fields/addListener/' . $form_id; ?>"
        method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>

                <tr>
                    <td width="31%">Naam import item: <span class="mandatory">*</span></td>
                    <td><input class="input-append" type="text" name="field_name"
                               value="<?php echo set_value('field_name'); ?>" /></td>
                    <td><?php echo form_error('field_name'); ?></td>
                </tr>

                <tr>
                    <td>Weergeven als:<span class="mandatory">*</span></td>
                    <td><input type="text" name="name_on_form"
                               value="<?php echo set_value('name_on_form'); ?>" /></td>
                    <td><?php echo form_error('name_on_form'); ?></td>
                </tr>
                <tr>
                    <td>Markeer veld als: </td>

                    <td> 
                        <label class="radio inline">
                            <input  type="radio" name="field_type" value="N" <?php echo set_radio('field_type', 'N'); ?> checked="checked">Normaal
                        </label>
                        <?php if (isset($show_mail) && $show_mail == FALSE) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="E" <?php echo set_radio('field_type', 'E'); ?>>E-mail
                            </label>
                        <?php } ?>
                        <?php if (isset($show_ploicy_number) && $show_ploicy_number == FALSE) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="P" <?php echo set_radio('field_type', 'p'); ?>>Polisnummer
                            </label>
                        <?php } ?>
                        <?php if (isset($show_schadenummer) && $show_schadenummer == FALSE) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="S" <?php echo set_radio('field_type', 'S'); ?>>Schadenummer
                            </label>
                        <?php } ?>
                        <?php if (isset($show_behandler) && $show_behandler == FALSE) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="B" <?php echo set_radio('field_type', 'B'); ?>>Behandelaar
                            </label>
                        <?php } ?>
                        <?php if (isset($show_behandler_emial) && $show_behandler_emial == FALSE) { ?>

                            <label class="radio inline">
                                <input type="radio" name="field_type" value="BE" <?php echo set_radio('field_type', 'BE'); ?>>Behandelaar E-mail
                            </label>
                        <?php } ?>

                    </td>
                    <td><?php echo form_error('field_type'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button> &nbsp;&nbsp; 
                        <a  href="<?php echo base_url() . 'admin/form_fields/' . $form_id; ?>"  class="btn btn-large btn-primary">Annuleer</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                        zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />

