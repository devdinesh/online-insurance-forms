<h2><span>Antwoorden</span></h2>

<p>Overzicht antwoorden voor vraag <?php echo strip_tags($question); ?></p>

<div>
    <p> 
        <a class="btn btn-large btn-primary" style="margin-bottom: 5px;" href="<?php echo base_url() . "admin/categories_question_answer/add/" . $question_id ?>"> Toevoegen antwoord </a> &nbsp;
        <a class="btn btn-large btn-primary" style="margin-bottom: 5px;" href="<?php echo base_url() . "admin/categories_question/" . $cat_id; ?>"> Terug </a>
    </p>
    <!-- main table start -->
    <table class="table table-striped table-condensed"
           id="list_category_question" width="100%" border="0" cellspacing="0"
           cellpadding="0">
        <thead>
            <tr>
                <th>Antwoord</th>
                <th>Ga naar vraag</th>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td>etc</td>
                <td>etc</td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_category_question').dataTable( {
            //  "bJQueryUI": true,
            "bProcessing":true,
            "bPaginate" : false,
            "bFilter": false,
            "bInfo": false,
            "bSort": false, 
            "sPaginationType": "full_numbers",
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"}
        
            ],
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": "<?php echo site_url("admin/admin_forms_categories_question_answer/get_json/$question_id"); ?>"
        } );
    } );
    
    function deleteRow(ele){
        var id = $(ele).attr('id');
        //   var parent = $(ele).parent().parent();
        if( confirm("Do you want to delete ?"))
        {
            location.href="<?php echo base_url(); ?>admin/admin_forms_categories_question_answer/get_json/" +<?php echo $question_id; ?>+"/"+id;
        }
                    
        return false;
    }
</script>