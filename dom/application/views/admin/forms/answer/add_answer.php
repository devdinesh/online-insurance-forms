<script type="text/javascript">

    $(document).ready(function() {
        /**
when the document is loaded, by default the radio button will be checked,
        according to the radio button checked, we call approapriate function to load
        the checkboxes
         */
        var go_to_none_var = document.getElementById("go_to_none");
        var go_to_end_var = document.getElementById("go_to_end");
        var go_to_question_var = document.getElementById("go_to_question");
	
        if(go_to_none_var.checked==true)
        {
            show_none();
        }
        else if(go_to_question_var.checked==true)
        {
            show_question_list();
        }
        else if(go_to_end_var.checked==true)
        {
            show_none();
        }
	
	  
    });
	
  
    function show_none()
    {
        // puts empy contents 
        $('#aa').load('<?php echo base_url('admin/admin_forms_categories_question_answer/get_edit_question_answer_cat_list_for_edit_page/none'); ?>');
        $('#bb').load('<?php echo base_url('admin/admin_forms_categories_question_answer/get_edit_question_answer_cat_list_for_edit_page_values/none/0'); ?>');
        $('#cc').load('<?php echo base_url('admin/admin_forms_categories_question_answer/get_edit_question_answer_cat_list_for_edit_page/none'); ?>');
    }


    function show_question_list()
    {
        // load the list of category list
        $('#aa').load('<?php echo base_url('admin/admin_forms_categories_question_answer/get_edit_question_answer_cat_list_for_edit_page/to_question'); ?>');
        $('#bb').load('<?php echo base_url('admin/admin_forms_categories_question_answer/get_edit_question_answer_cat_list_for_add_page_values/to_question/' . $question_id); ?>');	
    }



</script>

<h2><span>Toevoegen antwoord</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'admin/categories_question_answer/addListener'; ?>"
        method="post" enctype="multipart/form-data">
        <input type="hidden" name="question_id"
               value="<?php echo $question_id; ?>">
        <table width="100%" class="table table-striped table-condensed"
               border="0px">
            <tbody>
                <tr>
                    <td>Antwoord <span class="mandatory">*</span></td>
                    <td><input class="input-append" type="text" name="answer"
                               value="<?php echo set_value('answer'); ?>" /></td>
                    <td><?php echo form_error('answer'); ?></td>
                </tr>
                <?php if ($display_smat_form == true) { ?>
                    <tr>
                        <td>Ga naar vraag / categorie</td>
                        <td>

                            <div class="control-group">

                                <div class="controls">

                                    <!-- span for go to none -->
                                    <span> <label for="go_to_none" style="width: 70px;"
                                                  class="radio inline"> <input type="radio" id="go_to_none"
                                                                     name="go_to_type" value="to_none"
                                                                     <?php echo 'checked="checked"'; ?> onclick="show_none()">
                                            Geen
                                        </label>
                                    </span>
                                    <!-- span for go to question -->
                                    <span> <label for="go_to_question" style="width: 100px;"
                                                  class="radio inline"> <input type="radio" id="go_to_question"
                                                                     name="go_to_type" value="to_question"
                                                                     onclick="show_question_list()"> Naar vraag
                                        </label>
                                    </span>
                                    <span> <label for="go_to_end" style="width: 100px;" class="radio inline">
                                            <input type="radio" id="go_to_end" name="go_to_type" value="to_end" onclick="show_none()"> Einde formulier
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                <?php } ?>
                <tr id="hide_item">
                    <td><span id="aa"> </span></td>
                    <td><span id="bb"> </span></td>
                    <td></td>
                </tr>
                <tr id="cc">
                </tr>
                <tr>
                    <td colspan="3">
                        <button type="submit" class="btn btn-large btn-primary">Opslaan</button>   &nbsp;&nbsp;
                        <a  href="<?php echo base_url() . "admin/categories_question_answer/" . $question_id; ?>" class="btn btn-large btn-primary"> Annuleer </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">Velden die gemarkeerd zijn met een <span class="mandatory">*</span> zijn verplicht
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',
            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>
<script type="text/javascript">
    function getcategoryquestion(value){
	
        var cat_id=value;
	
        var question_id="<?php echo $question_id; ?>";
	
        var url = "<?php echo base_url(); ?>" +  "admin/admin_forms_categories_question_answer/get_category_question_for_add/" + cat_id+"/"+question_id;
        $('#cc').load(url);
    }
</script>
