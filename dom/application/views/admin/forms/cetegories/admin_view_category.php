<h2 class="title-margin"><span>Overzicht categorieen</span></h2>
<div class="row-fluid margin-bottom">
    <p class="span12">
        <a  href="<?php echo base_url() . 'admin/categories/add/' . $form_details->form_id; ?>" class="btn btn-large btn-primary">Toevoegen Categorie</a> &nbsp; 
        <a href="<?php echo base_url() . 'admin/client_forms'; ?>" class="btn btn-large btn-primary">Terug</a>
    </p>
</div>
<table class="table table-striped table-condensed"
       id="tbl_category_list" width="100%" border="0" cellspacing="0"
       cellpadding="0">
    <thead>
        <tr>
            <th width="30%">Categorienaam</th>
            <th width="20%">Vragen</th>
            <th width="2%"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>etc</td>
            <td>etc</td>
        </tr>
    </tbody>
</table>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#tbl_category_list').dataTable( {
            //  "bJQueryUI": true,
            "bProcessing":true,
            "bPaginate" : false,
            "bFilter": false,
            "bInfo": false,
            "bSort": false,   
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"} 
            ],
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": "<?php echo site_url("admin/admin_forms_categories/getJson/$form_details->form_id"); ?>",
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                $(nRow).attr('id', aData[3]);
            }
        } );
        $(function() {
            $("#tbl_category_list tbody").sortable({ opacity: 0.6, cursor: 'move', update: function() {
                    var order = $(this).sortable("serialize");
                    var url = "<?php echo base_url() . 'admin/categories/sortable'; ?>";
                    $.post(url, order, function(){
                        window.location.reload();
                    });   
                }                                
            });
        });
    } );
 
    
    
    function deleteRow(ele){
        var id = $(ele).attr('id');
        //   var parent = $(ele).parent().parent();
        if( confirm("Do you want to delete ?"))
        {
            location.href="<?php echo base_url(); ?>admin/admin_forms_categories/delete/" +<?php echo $form_details->form_id; ?>+"/"+id;
        }
                    
        return false;
    }
</script>