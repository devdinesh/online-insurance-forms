<h2 class="title-margin"><span>Details Categorie</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'admin/categories/editListener/' . $cat_details->cat_id; ?>"
        method="post">
        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>

                <tr>
                    <td>Categorienaam <span class="mandatory">*</span></td>
                    <td><input class="input-append" type="text" name="cat_name"
                               value="<?php
echo htmlentities(
        set_value('cat_name') != '' ? set_value('cat_name') : isset(
                        $cat_details->cat_name) ? $cat_details->cat_name : '');
?>" /></td>
                    <td><?php echo form_error('cat_name'); ?></td>
                </tr>
                <tr>
                    <td class="vertical_align">Introductietekst</td>

                    <td colspan="2"><textarea id="intorduction_text"
                                              name="introduction_text" rows="5" class="tinymce span6"><?php
                               if (set_value('introduction_text') != '') {
                                   $value = set_value('introduction_text');
                               } else {
                                   if (isset($cat_details->introduction_text)) {
                                       $value = $cat_details->introduction_text;
                                   } else {
                                       $value = '';
                                   }
                               }
                               echo htmlentities($value);
?></textarea></td>

                </tr>

                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>   &nbsp;&nbsp; 
                        <a  href="<?php echo base_url() . 'admin/categories/' . $cat_details->form_id; ?>" class="btn btn-large btn-primary">Annuleer</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                        zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',
            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,|,bullist,numlist,|,undo,redo,|,link,unlink,image",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>