<link rel="stylesheet" type="text/css" href="<?php echo assets_url_css; ?>font-awesome.min.css" />

<h2><span>Overzicht Formulieren</span></h2>
<div class="row-fluid">
    <p class="span12">
        <a href="<?php echo base_url() . 'admin/statistic'; ?>" class="btn btn-large btn-primary">Terug</a>
    </p>
</div>
<div>
    <div class="clear"></div>
    <div class="responsive-table" style="margin-top: 10px;">
        <div class="special-list-form">
            <table class="table table-striped table-condensed" id="staticformlist" width="100%">
                <thead>
                    <tr>   
                        <th>Nummer</th>
                        <th>Formulier</th>
                        <th>Datum</th>
                        <th>Relatie</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <br />
        </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8"> 
    $(document).ready(function() {
        sort_by_emp_and_type();
    });
 
    function sort_by_emp_and_type()
    {
        var month ='<?php echo $month; ?>';
        var Year ='<?php echo $year; ?>';
       
 
        var oTable1 = $('#staticformlist').dataTable();
        oTable1.fnDestroy();

        var oTable = $('#staticformlist').dataTable( {
            //  "bJQueryUI": true,
            "bProcessing":true,
            "sPaginationType": "full_numbers",          
            "bSort": true,
            "bPaginate": true,
            "bServerSide": true,
            "bInfo": true,
            "bFilter": true,
            "aaSorting": [[ 0, "desc" ]],
            'iDisplayLength': '<?php echo $setting[0]->default_rows; ?>',
            "aoColumns":[
                {"sClass":"align_center align-top"},{"sClass":"no-wrap align-top"},{"sClass":"align_center align-top"},
                {"sClass":"align_center align-top"},{"sClass":"align_center align-top"}
            ] ,
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": <?php echo "\"" . base_url() . "admin/list_insurance/getStatisticJson/\"" ?> +month + "/" +Year,
            'fnRowCallback': function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if(aData[9] == false){
                    nRow.className = "error";
                }
            }  
        } );
    }
   
</script>
<style type="text/css">
    .align-top{vertical-align: top;}
    .force-to-center{text-align:center !important;}
    .force-to-center img{max-width:inherit !important;}
</style>