<script>
    $(function() {
        $( "#datepicker2" ).datepicker({ dateFormat: 'dd-mm-yy' });
        $( "#datepicker1" ).datepicker({ dateFormat: 'dd-mm-yy' });
        $( "#datepicker3" ).datepicker({ dateFormat: 'dd-mm-yy' });
       
    });
</script>
<h2><span><?php echo $form_value[0]->form_name; ?></span></h2>
<div class="span12" style="margin-left: 0px;">
    <form method="post" action="<?php echo base_url() . "admin/list_insurance/edit_details/" . $claim_info[0]->claim_id; ?>" style="margin-bottom: 0px;">
        <table width="100%" class="table table-striped table-condensed" border="0" style="margin-bottom: 0px;">
            <tbody>
                <tr>
                    <td style="width: 30%;">Status:</td>
                    <td><select class="" name="status" id="status_id">
                            <option value="Nieuw"<?php
if ($claim_info[0]->status == "Nieuw") {
    echo "selected='selected'";
}
?>>Nieuw</option>
                            <option value="open" <?php
                                    if ($claim_info[0]->status == "open") {
                                        echo "selected='selected'";
                                    }
?>>Open</option>] <option value="Afgerond" <?php
                                    if ($claim_info[0]->status == "Afgerond") {
                                        echo "selected='selected'";
                                    }
?>>Afgerond</option>
                            <option value="Archief" <?php
                                    if ($claim_info[0]->status == "Archief") {
                                        echo "selected='selected'";
                                    }
?>>Archief</option>

                        </select></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="width: 30%;">Importdatum:</td>
                    <td>
                        <?php
                        if (isset($claim_info[0]->import_date) && $claim_info[0]->import_date !== "01-01-1970") {
                            echo date('d-m-Y', strtotime($claim_info[0]->import_date));
                        }
                        ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="width: 30%;">E-mail verstuurd:</td>
                    <td><?php
                        if (isset($claim_info[0]->mail_send_date) && $claim_info[0]->mail_send_date !== "01-01-1970") {
                            echo date('d-m-Y', strtotime($claim_info[0]->mail_send_date));
                        }
                        ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td style="width: 30%;">Bevestigingsdatum:</td>
                    <td><?php
                        if (isset($claim_info[0]->conform_date) && $claim_info[0]->conform_date !== "01-01-1970") {
                            echo date('d-m-Y', strtotime($claim_info[0]->conform_date));
                        }
                        ?></td>
                    <td></td>
                </tr>

                <?php
                foreach ($field_value as $field) {
                    ?>
                    <tr>
                        <td><?php echo $field->name_at_form; ?></td>

                        <td><input type="text"
                                   name="<?php echo str_replace(' ', '_', $field->name_at_form); ?>"
                                   value="<?php echo set_value(str_replace(' ', '_', $field->name_at_form)) != '' ? set_value(str_replace(' ', '_', $field->name_at_form)) : isset($field->filed_value) ? $field->filed_value != '0' ? $field->filed_value : ''  : ''; ?>">
                                   <?php
                                   if (isset($field->is_change) && $field->is_change == '1') {
                                       ?>
                                <i class="icon-exclamation-new"></i>
                                <?php
                            }
                            ?>
                        </td>
                        <td> <?php echo form_error($field->name_at_form); ?></td>
                    </tr>
                <?php } ?>
                <?php if (!empty($claim_attechment)) { ?>
                    <tr>
                        <td>Attachment :</td>
                        <td>
                            <?php
                            foreach ($claim_attechment as $attachment) {
                                ?>
                                <a
                                    href="<?php echo base_url() . "/assets/claim_files/" . $attachment->file_name; ?>"
                                    target="_blank"><?php echo $attachment->file_name; ?></a> <br />
                                    <?php
                                }
                                ?>
                        </td>
                        <td></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td colspan="2">
                        <input class="btn btn-large btn-primary <?php if ($claim_info[0]->status == "Archief") {
                    echo 'hide';
                } ?>" type="submit" value="Opslaan">
                        <a  class="btn btn-large btn-primary"  href="<?php echo base_url() . 'admin/list_insurance'; ?>"> Terug </a>
                    </td>
                    <td></td>
                </tr>

            </tbody>
        </table>
    </form>
</div>

