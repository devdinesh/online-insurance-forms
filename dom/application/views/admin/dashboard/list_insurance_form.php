<link rel="stylesheet" type="text/css" href="<?php echo assets_url_css; ?>font-awesome.min.css" />
<script src="<?php echo assets_url_js; ?>new_js/bootstrap-modal.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo assets_url_css; ?>new_css/select2.css" />
<script src="<?php echo assets_url_js; ?>new_js/select2.js"></script> 
<div class="row-fluid stat-data">
    <div class="span6 responsive pull-left" data-tablet="span6" data-desktop="span3">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="icon-comments"></i>
            </div>
            <div class="details">
                <div class="number"><?php echo $total_conform_after_last_login; ?></div>
                <div class="desc">Nieuw ingevulde formulieren</div>
            </div>
            <span class="more" href="#">&nbsp;</span> </a>
        </div>
    </div>
    <div class="span6 responsive pull-right" data-tablet="span6" data-desktop="span3" id="my-temp">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="icon-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number"><?php echo number_format(@$total_forms, 0, ',', '.'); ?></div>
                <div class="desc">Totaal verzonden formulieren</div>
            </div>
            <a class="more"
               href="<?php echo base_url() . 'admin/statistic'; ?>">
                Bekijk INFO <i class="icon-circle-arrow-right"
                               style="font-size: 18px;"></i>
            </a>
        </div>
    </div>
</div>
<h2><span>Overzicht Formulieren</span></h2>
<div class="row-fluid " style="margin-bottom: 10px;">
    <a href="#" onclick="show_import_feature();" class="btn btn-large btn-primary">Import</a>
    <a href="#" onclick="open_archiveer_model();" class="btn btn-large btn-primary">Archiveer</a>
    <?php if (count($form_lists) != 0) { ?>
        <a href="#" onclick="show_import_menually();" class="btn btn-large btn-primary">Verstuur formulier</a>
    <?php } ?>
</div>

<!--<div class="row-fluid" style="margin-bottom: 10px;">-->

<div class="row-fluid hide" id="show_import_feature">
    <h3><a href="<?php echo base_url() . "admin/import_log" ?>">Import logboek</a></h3>
    <div class="float-left">
        <form method="post"
              action="<?php echo base_url('admin/list_insurance/upload_template'); ?>"
              enctype="multipart/form-data">
            <input class="input" type="file" name="upload_file"
                   style="height: auto !important;"> <input class="btn btn-large btn-primary" type="submit"
                   value="Upload">
                   <?php
                   if (isset($file_error)) {
                       echo '<p class="label-error">' . $file_error . "</p>";
                   }
                   ?>
        </form>
    </div>
    <span class="float-left">&nbsp;&nbsp;&nbsp;</span>
    <div class="float-left">
        <a href="#" onclick="open_import_model();" class="btn btn-large btn-primary">Importeren</a>
<!--               <a href="<?php echo base_url() . 'admin/import/index'; ?>" class="btn btn-large btn-primary">Importeren</a>-->

    </div>
</div>

<div class="margin-killer-left">
    <div class="span2 margin-killer-left width-120px">
        <div class="form-group">
            <div class="col-md-4">
                <select class="form-control input-large select2me" data-placeholder="Select..." name="form_id" id="form_id" onchange="sort_by_emp_and_type()">
                    <option value="nl">Formulier</option>
                    <?php
                    foreach ($form_lists as $result) {
                        ?>
                        <option value="<?php echo htmlentities($result->form_id); ?>"><?php echo htmlentities($result->form_name); ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="span2 special-margin-left width-120px">
        <div class="form-group">
            <div class="col-md-4">
                <select class="form-control input-large select2me" data-placeholder="Select..." name="status" id="stustu_id" onchange="sort_by_emp_and_type()" >
                    <option value="nl">Status</option>
                    <?php
                    foreach ($statuses as $result) {
                        ?>
                        <option value="<?php echo htmlentities($result->status); ?>" <?php
                    if (isset($default_status) && $default_status == $result->status) {
                        echo "selected='selected'";
                    }
                        ?> ><?php echo htmlentities($result->status); ?></option>
                                <?php
                            }
                            ?>
                </select>
            </div>
        </div>
    </div>
    <!--    <div class="span2 special-margin-left width-120px">
            <select class="width-100" name="handler" id="handler_id"
                    onchange="sort_by_emp_and_type()">
                <option value="nl">Behandelaar</option>
    <?php
    foreach ($handlers as $result) {
        ?>
                                                                                                                                                                                                                                                                                                                                        <option value="<?php echo htmlentities($result->handler); ?>"><?php echo ucwords($result->handler); ?></option>
        <?php
    }
    ?>
    
            </select>
        </div>
        <div class="span2 special-margin-left width-120px">
            <select class="width-100" name="schadenummer"
                    id="schadenummer_name" onchange="sort_by_emp_and_type()">
                <option value="nl">Schadenummer</option>
    <?php
    foreach ($schadenummer as $result) {
        ?>
                                                                                                                                                                                                                                                                                                                                        <option value="<?php echo htmlentities($result->schadenummer); ?>"><?php echo htmlentities($result->schadenummer); ?></option>
        <?php
    }
    ?>
            </select>
        </div>-->
    <div class="span2 special-margin-left width-120px">
        <div class="form-group">
            <div class="col-md-4">
                <select class="form-control input-large select2me" data-placeholder="Select..." name="policyholder"
                        id="policy_holder_id" onchange="sort_by_emp_and_type()" onload="sort_by_emp_and_type()" >
                    <option value="nl">Relatie</option>
                    <?php foreach ($policyholder as $policy_holder) { ?>
                        <?php if (isset($policy_holder_id) && $policy_holder_id == $policy_holder) { ?>
                            <script type="text/javascript">
                                $(document).ready(function() {
                                    //sort_by_emp_and_type();
                                });    
                            </script>
                        <?php } ?>
                        <option value="<?php echo $policy_holder->policy_holder_id; ?>" <?php
                    if (isset($policy_holder_id) && $policy_holder_id == $policy_holder->policy_holder_id) {
                        echo "selected='selected'";
                    }
                        ?>><?php echo htmlentities($policy_holder->policy_holder_name); ?></option>
                            <?php } ?>
                </select>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="responsive-table" style="margin-top: 10px;">
    <div class="special-list-form">
        <table class="table table-striped table-condensed" id="list_insurance_form_id" width="100%">
            <thead>
                <tr>   
                    <th>Nummer</th>
                    <th>Formulier</th>
                    <th width="12%">Datum</th>
                    <th>Relatie</th>
                    <th>Polisnummer</th>
                    <th>Schadenummer</th>
                    <th>Behandelaar</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>

            <tbody>
            </tbody>
        </table>
        <br />
    </div>
</div>
<div class="modal" id="modal_dislog" >
    <div class="modal-dialog">
        <div class="modal-content pull-left span12" style="margin-left: 0px;">
            <div class="modal-body" style="margin-left: 0px;">
                <div id="display_data"></div>
            </div>
        </div>
    </div>
</div> 
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span>Importeren formulieren</span></h4>
            </div>
            <form action="#" id="remarkform" method="post" accept-charset="utf-8">
                <div class="modal-body">
                    <div  id="show_import_file" >


                    </div>
                </div>
                <div class="modal-footer">
                      <!--               <a href="<?php echo base_url() . 'admin/import/index'; ?>" class="btn btn-large btn-primary">Importeren</a>-->
                    <button type="button" class="btn btn-large btn-primary"  data-dismiss="modal">Annuleer</button>
                    <a href="<?php echo base_url() . 'admin/import/index'; ?>" class="btn btn-large btn-primary" id="ja_button" >Ja</a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span>Archiveer</span></h4>
            </div>
            <form action="#" id="remarkform" method="post" accept-charset="utf-8">
                <div class="modal-body">
                    <div>
                        Weet je het zeker dat je de formulieren die gereed zijn wilt archiveren?
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="<?php echo base_url() . 'admin/list_insurance/add_status_archief'; ?>" class="btn btn-large btn-primary" id="ja_button" >Ja</a>
                    <button type="button" class="btn btn-large btn-primary"  data-dismiss="modal">Nee</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade custom-model-width" id="myModalMenually" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><span>Verstuur formulier</span></h4>
            </div>
            <form action="<?php echo base_url('admin/import_menually'); ?>" method="post" id="manualImportform" accept-charset="utf-8">
                <div class="modal-body">
                    <table width="100%" class="" border="0" style="margin-bottom: 0px;background: none;">
                        <tbody>
                            <tr>
                                <td>E-mailadres <span class="mandatory">*</span> </td>
                                <td><input type="text" autocomplete="off" class="email required" name="manual_mail_address" /></td>
                                <td> </td>
                            </tr>
                            <tr <?php
                            if (count($form_lists) == 1) {
                                echo 'class="hide"';
                            }
                            ?>>
                                <td>Formulier <span class="mandatory">*</span></td>
                                <td>
                                    <select class="form-control custom-select select2me required" data-placeholder="Select..." name="manual_form">
                                        <option value="">Selecteer</option>
                                        <?php
                                        if (!empty($form_lists)) {
                                            foreach ($form_lists as $result) {
                                                ?>
                                                <option value="<?php echo htmlentities($result->form_id); ?>"  <?php
                                        if (count($form_lists) == 1) {
                                            echo 'selected="selected"';
                                        }
                                                ?>><?php echo htmlentities($result->form_name); ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-large btn-primary"  data-dismiss="modal">Annuleer</button>
                    <button type="submit" class="btn btn-large btn-primary">Verstuur</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function() {
        $( "#datepicker2" ).datepicker({ dateFormat: 'dd-mm-yy' });
        $( "#datepicker1" ).datepicker({ dateFormat: 'dd-mm-yy' });
        $( "#datepicker3" ).datepicker({ dateFormat: 'dd-mm-yy' });
       
    });
</script>
<script type="text/javascript" charset="utf-8"> 
    $(document).ready(function() {
        sort_by_emp_and_type();
        $( "#modal_dislog" ).hide();
        $( "#myModalMenually" ).hide();
        $('.select2me').select2();  
        $("#manualImportform").validate({
            rules: {},
            messages:{},
            errorElement: "span"
        }); 
    });
    function show_import_feature(){
        if($('#show_import_feature').hasClass('hide')){
            $('#show_import_feature').removeClass('hide');
        }else{
            $('#show_import_feature').addClass('hide');
        } 
        
          
    }
 
    function openModal(claim_id){
        jQuery('#display_data').load('<?php echo base_url() ?>admin/list_insurance/view_insurace_index/' + claim_id);
        // console.log(data);
        //jQuery('#email_data').html(data);
        $( "#modal_dislog" ).show();
        $('#display_data').show();
        //        var searzlie = jQuery('#display_data').serialize();
        //        jQuery.ajax({
        //            url : "<?php echo base_url() . 'admin/list_insurance/view_insurace_index_ajax/'; ?>" + claim_id,
        //            type: "post",
        //            data: searzlie,
        //            success: function(data){
        //                jQuery('#display_data').load('<?php echo base_url() ?>admin/list_insurance/view_insurace_index/' + claim_id);
        //                // console.log(data);
        //                //jQuery('#email_data').html(data);
        //                $( "#modal_dislog" ).show();
        //                $('#display_data').show();
        //            }
        //        }); 
    }
    function open_import_model(){
        $('#ja_button').hide();
        jQuery.ajax({
            url : "<?php echo base_url() . 'admin/import/get_model_list_file'; ?>",
            type: "post",
            dataType:'json',
            success: function(data){
                jQuery('#show_import_file').html(data);
                $('#myModal').modal('show');
                //console.log(data);
            }
        }); 
    }
    function open_archiveer_model(){
        $('#myModal1').modal('show');
    }
    function sort_by_emp_and_type()
    {
        var form_id= document.getElementById("form_id");
        var form_value = form_id.value; // name of selected status
        
        var status_id= document.getElementById("stustu_id");
        var status_value = status_id.value; // name of selected status
        
        //        var handler_id= document.getElementById("handler_id");
        //        var handler_value = handler_id.value; // name of selected handler
        //        
        //        var schadenummer_name= document.getElementById("schadenummer_name");
        //        var schadenummer_name_value = schadenummer_name.value; // name of selected schadenummer
        //        
        var policy_holder_id= document.getElementById("policy_holder_id");
        var policy_holder_value = policy_holder_id.value; 
        
        var oTable1 = $('#list_insurance_form_id').dataTable();
        oTable1.fnDestroy();

        var oTable = $('#list_insurance_form_id').dataTable( {
            //  "bJQueryUI": true,
            "bProcessing":true,
            "sPaginationType": "full_numbers",          
            "bSort": true,
            "bPaginate": true,
            "bServerSide": true,
            "bInfo": true,
            "bFilter": true,
            "aaSorting": [[ 0, "desc" ]],
            "aLengthMenu": [[50, 100, 250, 500,1000,-1], [50, 100, 250, 500,1000,"All"]],
            //'iDisplayLength': '<?php echo $setting[0]->default_rows; ?>',
            'iDisplayLength': '50',
            "aoColumns":[
                {"sClass":"align_center align-top"},{"sClass":"no-wrap align-top"},{"sClass":"align_center align-top"},{"sClass":"align_center align-top"},
                {"sClass":"align_center align-top"},{"sClass":"align_center align-top"},{"sClass":"align_center align-top"},{"sClass":"align_center align-top"},
                {"sClass":"no-wrap align-top force-to-center"}
            ] ,
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": <?php echo "\"" . base_url() . "admin/list_insurance/getField/\"" ?> +form_value + "/" +status_value +"/"+policy_holder_value,
            'fnRowCallback': function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if(aData[9] == false){
                    nRow.className = "error";
                }
            }  
        } );
    }
    function show_import_menually(){
        $("#manualImportform").validate().resetForm();
        $("#manualImportform").find('input').val('');
        $('#myModalMenually').modal('show');
    }
</script>
<style type="text/css">
    .align-top{vertical-align: top;}
    .force-to-center{text-align:center !important;}
    .force-to-center img{max-width:inherit !important;}
</style>