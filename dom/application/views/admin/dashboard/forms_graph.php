<link rel="stylesheet" type="text/css" href="<?php echo assets_url_css; ?>font-awesome.min.css" />
<script src="<?php echo assets_url_js; ?>new_js/bootstrap-modal.js"></script>
<style type="text/css">
    .statistic_botton{
        float: left;
        margin-right: 20px;
    }
    #showradios{
        float: left;
        margin: auto
    }
</style>
<div class="row-fluid">
    <h2><span>Statistieken</span></h2>
    <span>&nbsp;</span>
    <div class="row-fluid">
        <div class="span6 responsive" data-tablet="span6"
             data-desktop="span3">
            <div class="dashboard-stat green">
                <div class="visual">
                    <i class="icon-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number"><?php echo $total_forms ?></div>
                    <div class="desc">Totaal verzonden Formulieren</div>
                </div>
                <span class="more" href="#">&nbsp;</span>
            </div>
        </div>
        <div class="span6 responsive" data-tablet="span6"
             data-desktop="span3" id="my-temp">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="icon-comments"></i>
                </div>
                <div class="details">
                    <div class="number"><?php echo round(((int) $total_forms / count($form_imported_data)), 1); ?></div>
                    <div class="desc">Gemiddeld per maand</div>
                </div>
                <span class="more" href="#">&nbsp;</span>
            </div>
        </div>

    </div> 
    <form action="<?php echo base_url() . 'admin/statistic/save'; ?>" id="staticform" method="post" enctype="multipart/form-data">
        <div class="row-fluid">
            <div class="span3">E-mail de statistieken <span class="offset1"> <input class="input checkbox-align" onchange="return statisticChange()" type="checkbox" <?php
if (isset($client_setting->mail_statistic) && $client_setting->mail_statistic == 1) {
    echo 'checked="checked";';
}
?> name="mail_statistic" id="mailstatistic" value="1"/>
                </span>
            </div>
            <div id="showradios">
                <div class="statistic_botton">
                    <select name="mail_type">
                        <option value="">Selecteer</option>
                        <option value="Daily" <?php
                                                                                    if (isset($client_setting->mail_type) && $client_setting->mail_type == 'Daily') {
                                                                                        echo 'selected="selected"';
                                                                                    }
?>  >Dagelijks</option>
                        <option value="Weekly" <?php
                                if (isset($client_setting->mail_type) && $client_setting->mail_type == 'Weekly') {
                                    echo 'selected="selected"';
                                }
?>  >Wekelijks</option>
                        <option value="Monthly" <?php
                                if (isset($client_setting->mail_type) && $client_setting->mail_type == 'Monthly') {
                                    echo 'selected="selected"';
                                }
?>  >Maandelijks</option>
                    </select>
                </div>
                <div class="statistic_botton">
                    <button type="button" class="btn btn-large btn-primary" onclick="send_statistic();">Verstuur nu</button>
                </div>
            </div>
            <button type="submit" class="btn btn-large btn-primary">Bewaar</button>
        </div>
<!--        <div class="row-fluid">
            <div class="span3">E-mail de statistieken <span class="offset1"> <input class="input checkbox-align" onchange="return statisticChange()" type="checkbox" <?php
                                if (isset($client_setting->mail_statistic) && $client_setting->mail_statistic == 1) {
                                    echo 'checked="checked";';
                                }
?> name="mail_statistic" id="mailstatistic" value="1"/></span>
            </div>
            <div id="showradios">
                <div class="span2">
                    <select name="mail_type">
                        <option value="">Selecteer</option>
                        <option value="Daily" <?php
                                                                                    if (isset($client_setting->mail_type) && $client_setting->mail_type == 'Daily') {
                                                                                        echo 'selected="selected"';
                                                                                    }
?>  >Dagelijks</option>
                        <option value="Weekly" <?php
                                if (isset($client_setting->mail_type) && $client_setting->mail_type == 'Weekly') {
                                    echo 'selected="selected"';
                                }
?>  >Wekelijks</option>
                        <option value="Monthly" <?php
                                if (isset($client_setting->mail_type) && $client_setting->mail_type == 'Monthly') {
                                    echo 'selected="selected"';
                                }
?>  >Maandelijks</option>
                    </select>
                </div>
                <div class="span2">
                    <button type="button" class="btn btn-large btn-primary" onclick="send_statistic();">Verstuur nu</button>
                </div>
            </div>
            <div class="span2">
                <div class="row-fluid">
                    <button type="submit" class="btn btn-large btn-primary">Bewaar</button>
                </div>
            </div>

        </div>-->
    </form>
    <div id="statisticdata">
        <table class="table table-striped" style="width:40%;" >
            <tr>
                <th style="text-align: left;">Maand</th>
                <th style="text-align: left;">Verzonden formulieren</th>
            </tr>
            <?php foreach ($form_imported_data as $form_imported) { ?>
                <tr>
                    <td>
                        <?php
                        $month = array('01' => 'januari', '02' => 'februari', '03' => 'maart',
                            '04' => 'april', '05' => 'mei', '06' => 'juni', '07' => 'juli',
                            '08' => 'augustus', '09' => 'september', '10' => 'oktober',
                            '11' => 'november', '12' => 'december'
                        );
                        echo ucwords($month[$form_imported[0]]) . '-' . $form_imported[1];
                        ?>
                    </td>
                    <td class="text-center">
                        <?php
                        if (@$form_imported[2] > 0) {
                            ?> 
                            <a href="<?php echo base_url() . 'admin/statisticview/' . $form_imported[0] . '/' . $form_imported[1]; ?>"> <?php echo $form_imported[2]; ?></a>
                            <?php
                        } else {
                            echo @$form_imported[2];
                        }
                        ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>


<script type="text/javascript" charset="utf-8"> 
    $(document).ready(function() {
        if($("#mailstatistic").is(':checked')){
            $("#showradios").show();  // checked
        }
        else {
            $("#showradios").hide(); 
        }
    });
    function statisticChange(){
        if($("#mailstatistic").is(':checked')){
            $("#showradios").show();  // checked
        }
        else {
            $("#showradios").hide(); 
        }
    }
    function send_statistic(){
        var datastring = $("#staticform").serialize();
        $('#statisticdata a').each(function(){
            var val =$(this).html();
            $(this).replaceWith(val);
        });
        var statistic_table_data=$('#statisticdata').html(); 
        datastring+='&statisticdata='+statistic_table_data;
        jQuery.ajax({
            type: "post",
            url : "<?php echo base_url() . 'admin/sendstatistic'; ?>",
            data: datastring,
            dataType:'json',
            success: function(data){
                location.reload();
            }
        }); 
    }
     
     
</script>