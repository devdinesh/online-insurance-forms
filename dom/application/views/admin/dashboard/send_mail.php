<?php
/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
?>
<h2>Send Mail</h2>
<div>
	<form
		action="<?php echo base_url() . 'admin/list_insurance/send_mail_action/' . $policy_holder_id ; ?>"
		method="post" enctype="multipart/form-data">
		<table width="100%" class="table table-striped table-condensed"
			border="0">
			<tr>
				<td>Mail Address Of Policy Holder <span class="mandatory">*</span></td>
				<td><input type="text" name="policy_mail_address"
					value="<?php echo set_value('policy_mail_address') != '' ? set_value('policy_mail_address') : $email ; ?>" />
				</td>
				<td><?php echo form_error('policy_mail_address'); ?></td>
			</tr>
			<tr>
				<td>Personal Text <span class="mandatory">*</span></td>
				<td><textarea rows="5" cols="" name="message_text" class="tinymce"><?php echo set_value('message_text'); ?></textarea>
				</td>
				<td><?php echo form_error('message_text'); ?></td>
			</tr>
			<tr>
				<td colspan="3">Fields marked with <span class="mandatory">*</span>
					are mandatory.
				</td>
			</tr>
			<tr>
				<td><input type="submit" value="Send" class="btn" /> <a
					href="<?php echo base_url() . 'admin/list_insurance'; ?>">
						<button type="button" class="btn">Cancel</button>
				</a></td>
				<td></td>
			</tr>
		</table>
	</form>
</div>
<!-- Load TinyMCE -->
<script type="text/javascript"
	src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',
            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>