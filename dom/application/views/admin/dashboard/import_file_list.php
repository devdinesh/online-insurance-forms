<h2><span> Aanwezige formulieren op de server :</span></h2>
<div class="span12" style="margin-left: 0px;">

    <table width="100%" class="table table-striped table-condensed" border="0" style="margin-bottom: 0px; margin-top: 10px;">
        <tbody>
            <?php
            for ($i = 0; $i < count($files); $i++) {
                ?>
                <tr>
                    <td><?php echo $files[$i]; ?></td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

</div>
<span class="click_formula">Klik op 'Ja' om de formulieren te importeren.</span>

<script type="text/javascript">
    $('#ja_button').show();
</script>
