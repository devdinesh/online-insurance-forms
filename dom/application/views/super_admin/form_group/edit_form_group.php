<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'super_admin/form_groups'; ?>"><?php echo $this->lang->line('form_groups'); ?></a></li>
            <li class="active"><?php echo $title; ?></li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-8">
                <form class="form form-validate" id="editFormgroup" action="<?php echo base_url() . 'super_admin/form_groups/editListener/' . $group_info->form_group_id; ?>"  method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><?php echo $title; ?></header>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="form_group_name" value="<?php
if (set_value('form_group_name') != '') {
    echo set_value('form_group_name');
} else {
    echo $group_info->form_group_name;
}
?>" required/> 
                                        <label for="form_group_name"> <?php echo $this->lang->line('form_group_name'); ?> <span class="required">*</span></label>
                                        <?php echo form_error('form_group_name'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <a class="btn btn-flat" href="<?php echo base_url() . 'super_admin/form_groups'; ?>"><?php echo $this->lang->line('cancel'); ?></a>
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                        </div>
                    </div> 
                    <em class="text-caption"><?php echo $this->lang->line('requred_text_labels_text'); ?></em>
                </form>
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>
<script type="text/javascript">
    $( "#editFormgroup" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#editFormgroup')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#editFormgroup')).hide();
            label.remove();
        }
    });
</script>
