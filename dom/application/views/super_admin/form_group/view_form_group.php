<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary"><?php echo $title; ?></h2>
    </div>
    <?php if ($this->session->flashdata('success') != '') { ?>
        <div class="alert alert-success" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error') != '') { ?>

        <div class="alert alert-danger" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <div class="section-body">
        <!-- BEGIN DATATABLE 1 -->
        <div class="row">
            <div class="col-md-12 margin-bottom-xxl">
                <a href="<?php echo base_url() . 'super_admin/form_groups/add'; ?>" class="btn ink-reaction btn-primary" ><?php echo $this->lang->line('add_from_group'); ?></a>

            </div><!--end .col -->
        </div><!--end .row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="list_form_group" class="table table-striped table-hover" title="<?php echo $this->lang->line('mouse_hover_error'); ?>">
                        <thead>
                            <tr>
                                <th width="20%"><?php echo $this->lang->line('name'); ?></th>
                                <th width="40%"><?php echo $this->lang->line('forms'); ?></th>
                                <th width="20%"></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>etc</td>
                                <td>etc</td>
                                <td>etc</td>
                                <td>etc</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!--end .table-responsive -->
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END DATATABLE 1 -->
    </div><!--end .section-body -->
</section>
<div class="modal" id="modal_dislog" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="display_data"></div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_form_group').dataTable( {
            //  "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            'iDisplayLength': 100, 
            "bProcessing": true,
            "oLanguage":translate_dutch_lag,
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center","bSortable": false},{"sClass":"align_center","bSortable": false},{"sClass":"align_center","bSortable": false}
            ],
            "sAjaxSource": "<?php echo site_url("super_admin/form_groups/getJson"); ?>"
           
        } );
    } );
    function openModal(form_group_id){
        var searzlie = jQuery('#display_data').serialize();
        jQuery.ajax({
            url : "<?php echo base_url() . 'super_admin/form_groups/view_copy_form/'; ?>" + form_group_id,
            type: "post",
            data: searzlie,
            success: function(data){
                jQuery('#display_data').load('<?php echo base_url() ?>super_admin/form_groups/view_copy_form/' + form_group_id);
                // console.log(data);
                //jQuery('#email_data').html(data);
                $( "#modal_dislog" ).show();
                $('#display_data').show();
            }
        }); 
    }
    function deleteRow(ele){
        var current_id = $(ele).attr('id');
        var parent = $(ele).parent().parent();
        $.confirm({
            'title' : 'Digitaal op maat',
            'message'   : "<?php echo $this->lang->line('delete_question'); ?>",
            'buttons'   : {
                'Ja'    : {'class'      : 'btn ink-reaction btn-raised btn-sm btn-success',
                    'action': function(){
                        $.ajax({
                            type : 'POST',
                            url : http_host_js+'super_admin/form_groups/delete/'+current_id,
                            data: id =current_id,
                            beforeSend: function() {
                            },
                            success: function() {
                                parent.slideUp(500,function() {
                                    parent.remove();
                                    window.location.reload();
                                });
                            },
                            error : function(XMLHttpRequest, textStatus, errorThrown) {
                                alert('error');
                            }
                        });
                    }
                },
                'Nee'   : {
                    'class'     : 'btn ink-reaction btn-raised btn-sm btn-danger',
                    'action': function(){}      // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });             
        return false;
    }
</script>
