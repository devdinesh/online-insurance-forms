

<script>
    $(function() {
        $( document ).tooltip();
    });
</script>
<style>
    label {
        display: inline-block;
        /*width: 4em;*/
    }
</style>

<!-- actual page content begins -->


<h2><span>List All Question of <?php echo $category_details->cat_name; ?> Category in <?php echo $category_details->form_name; ?> Form</span></h2>
<div class="row-fluid">
    <p class="span12">
        <a
            href="<?php echo base_url() . 'super_admin/forms/categories/question/add/' . $category_details->cat_id; ?>"
            class="btn btn-large btn-primary">Add New Questions</a> <a
            href="<?php echo base_url() . 'super_admin/forms/categories/' . $category_details->form_id; ?>"
            class="btn btn-large btn-primary">Back</a>
    </p>

</div>

<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <?php if ($this->session->flashdata('success') != '') { ?>
            <tr>
                <td>
                    <div class="alert alert-success">
                        <a class="close" data-dismiss="alert"
                           href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('success'); ?>
                    </div>
                </td>
            </tr>
        <?php } ?>
        <?php if ($this->session->flashdata('error') != '') { ?>
            <tr>
                <td>
                    <div class="alert alert-error">
                        <a class="close" data-dismiss="alert"
                           href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('error'); ?>
                    </div>
                </td>
            </tr>
        <?php } ?>
    </table>
    <table class="table table-striped table-condensed"
           id="list_category_question" width="100%" border="0" cellspacing="0"
           cellpadding="0" title="Ga met de muis op een vraag staan en sleep deze naar de juiste positie om de volgorde te wijzigen.">
        <thead>
            <tr>
                <th width="9%">Vraagnummer</th>
                <th>Vraag</th>
                <th width="5%">Antwoorden</th>
                <th width="5%">Volgorde</th>
                <th width="2%"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>etc</td>
                <td>etc</td>
                <td>etc</td>
                <td>etc</td>
                <td>etc</td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_category_question').dataTable( {
            //  "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            'iDisplayLength': 100, 
            "bProcessing": true,   
            "bSort":false,
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"}
            ],
            "sAjaxSource": "<?php echo site_url("super_admin/forms/categories/question/getJson/$category_details->cat_id"); ?>"
        } );
    } );
    
    function deleteRow(ele){
        var id = $(ele).attr('id');
        //   var parent = $(ele).parent().parent();
        if( confirm("Wil je het formuier echt verwijderen?"))
        {
            location.href="<?php echo base_url(); ?>super_admin/forms/categories/question/delete/" +<?php echo $category_details->cat_id; ?>+"/"+id;
        }
                    
        return false;
    }
</script>