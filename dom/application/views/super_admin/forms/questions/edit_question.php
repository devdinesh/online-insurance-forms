
<h2><span>Details vraag</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'super_admin/forms/categories/question/update/' . $question_details->question_id; ?>"
        method="post">
        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>
                <tr>
                    <td>Categorie</td>
                    <td><?php echo $category_details->cat_name; ?></td>

                    <td></td>
                </tr>

                <tr>
                    <td>Vraag<span class="mandatory">*</span></td>
                    <td><textarea id="question" name="question" rows="5"
                                  class="tinymce">
                                      <?php
                                      if (set_value('question') != '') {
                                          $value = set_value('question');
                                      } else {
                                          if (isset($question_details->question)) {
                                              $value = strip_slashes(
                                                      $question_details->question);
                                          } else {
                                              $value = '';
                                          }
                                      }
                                      echo $value;
                                      ?>
                        </textarea></td>
                    <td><?php echo form_error('question'); ?></td>
                </tr>
                <tr>
                    <td>Helptekst</td>
                    <td colspan="2"><textarea id="help_text" class="custom-font-16"
                                              name="help_text" rows="5"><?php
                                      if (set_value('help_text') != '') {
                                          $value = set_value('help_text');
                                      } else {
                                          if (isset($question_details->help_text)) {
                                              $value = $question_details->help_text;
                                          } else {
                                              $value = '';
                                          }
                                      }
                                      echo htmlentities($value);
                                      ?></textarea></td>
                </tr>
                <tr>
                    <td>Is Verplicht</td>
                    <td><input type="checkbox" name="required"
                               <?php echo $question_details->required == 1 ? "checked=''" : "" ?> />
                    </td>
                </tr>
                <tr>
                    <td>Volgorde<span class="mandatory">*</span></td>
                    <td><input type="number" name="sequence" min="0"
                               value="<?php
                               echo htmlentities(
                                       set_value('sequence') != '' ? set_value('sequence') : isset(
                                                       $question_details->sequence) ? $question_details->sequence : '');
                               ?>" /></td>
                    <td><?php echo form_error('sequence'); ?></td>
                </tr>

                <tr>
                    <td>Antwoordsoort</td>
                    <td><div class="span2">
                            <label class="radio big-radio-label">
                                <input type="radio" name="answer_kind" value="date" <?php
                               if ($question_details->answer_kind == 'date') {
                                   echo "checked=\"checked\"";
                               }
                               ?> disabled="disabled">
                                Enkele regel(datum)
                            </label>
                            <label class="radio big-radio-label">
                                <input type="radio" name="answer_kind" value="number" <?php
                                       if ($question_details->answer_kind == 'number') {
                                           echo "checked=\"checked\"";
                                       }
                               ?> disabled="disabled">
                                Enkele regel(getal)  
                            </label>
                            <label class="radio big-radio-label">
                                <input type="radio" name="answer_kind" value="text" <?php
                                       if ($question_details->answer_kind == 'text') {
                                           echo "checked=\"checked\"";
                                       }
                               ?> disabled="disabled">
                                Enkele regel(tekst)
                            </label>
                        </div>
                        <div class="span2">
                            <label class="radio big-radio-label">
                                <input type="radio" name="answer_kind" value="textarea" <?php
                                       if ($question_details->answer_kind == 'textarea') {
                                           echo "checked=\"checked\"";
                                       }
                               ?> disabled="disabled"> 
                                Meerdere Regels
                            </label>
                            <label class="radio big-radio-label">
                                <input type="radio" name="answer_kind" value="radio" <?php
                                       if ($question_details->answer_kind == 'radio') {
                                           echo "checked=\"checked\"";
                                       }
                               ?> disabled="disabled">
                                Enkele Selectie
                            </label>
                            <label class="radio big-radio-label">
                                <input type="radio" name="answer_kind" value="checkbox" <?php
                                       if ($question_details->answer_kind == 'checkbox') {
                                           echo "checked=\"checked\"";
                                       }
                               ?> disabled="disabled">
                                Meerdere Selecties
                            </label>
                        </div></td>
                    <td>
                        <?php echo form_error('answer_kind'); ?>
                    </td>
                </tr>
                <!-- editing is starting -->
                <?php if (isset($answer_details) && !empty($answer_details)) { ?>

                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2">
                            <?php
                            foreach ($answer_details as $answer) {
                                ?>
                                <div id="given_answers">
                                    <span class="answer-text-box">
                                        <input type='textbox' class="custom-textfield" name="<?php echo 'given_answer_' . $answer->answer_id; ?>" value="<?php echo $answer->answer; ?>">
                                    </span>
                                    <?php echo form_error('answers[]'); ?>
                                    <span class="answer-delete-box">
                                        <a href="<?php echo base_url() . 'super_admin/categories_question/delete_answer/' . $answer->answer_id; ?>">
                                            <img src="<?php echo base_url() . 'assets/img/'; ?>icon_delete.png" width="14" height="15" alt="Delete"  title="Delete Answer"/>
                                        </a>
                                    </span>
                                </div>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                <?php } ?>

                <tr id="show_on_selection2" style="display: none">
                    <td>&nbsp;</td>
                    <td colspan="2">
                        <input type='button' value='+ Add Answer' class="btn btn-large btn-primary" id='addButton'>
                    </td>
                </tr>
                <tr id="show_on_selection3" style="display: none">
                    <td>&nbsp;</td>
                    <td colspan="2">
                        <input type="hidden" name="total_runtime_checkbox" id="total_runtime_checkbox" value="2" />
                        <div id='TextBoxesGroup'>
                            <div id="TextBoxDiv1">
                                <input type='textbox' id='textbox1' class="custom-textfield" name="answers1" />
                                <?php echo form_error('answers1'); ?>
                            </div>
                            <!--   <div id="TextBoxDiv2">
                                  <input type='textbox' id='textbox2' class="custom-textfield" name="answers2" />
                                 
                            <?php echo form_error('answers2'); ?>
                              </div>
                            -->
                        </div>
                    </td>
                </tr>


                <!-- editing is now over -->

                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                        &nbsp;&nbsp; <a
                            href="<?php echo base_url() . 'super_admin/forms/categories/question/' . $category_details->cat_id; ?>"
                            class="btn btn-large btn-primary">Annuleer</a></td>
                </tr>
                <tr>
                    <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                        zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
<script src="<?php echo assets_url_js; ?>jquery-ui.min.js.js"></script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>

<script type="text/javascript">

    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',
            // General options
            theme : "advanced",
            mode : "exact",
            elements : "ajaxfilemanager",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
           
            theme_advanced_buttons1 : ",link,unlink,image",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,
            //add icons to upload /maintain images/files at the server
            file_browser_callback : "ajaxfilemanager", 

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
    
    //add icons to upload /maintain images/files at the server
   
    function ajaxfilemanager(field_name, url, type, win) {
        var ajaxfilemanagerurl = "<?php echo base_url(); ?>assets/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
        var view = 'detail';
        switch (type) {
            case "image":
                view = 'thumbnail';
                break;
            case "media":
                break;
            case "flash": 
                break;
            case "file":
                break;
            default:
                return false;
        }
        tinyMCE.activeEditor.windowManager.open({
            url: "<?php echo base_url(); ?>assets/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
            width: 782,
            height: 440,
            inline : "yes",
            close_previous : "no"
        },{
            window : win,
            input : field_name
        });
        
    }
    //end of add icons to upload /maintain images/files at the server
    function deleteRow(tableID,txtid) 
    {
        document.getElementById(tableID).deleteRow(txtid);
    }
</script>
<script type="text/javascript">
    $(document).ready(function(){
        var counter = 3;
        $("#addButton").click(function () {
            var string = '<div id="TextBoxDiv'+counter+'"><input type="textbox" class="custom-textfield" id="textbox'+counter+'" name="answers'+counter+'" /><a id="' +counter+ '" onclick="removeans(this)"><img src="<?php echo base_url() . 'assets/img/'; ?>icon_delete.png" width="14" height="15" alt="Delete" /></a>';
            jQuery('#TextBoxesGroup').append(string);
            jQuery('#total_runtime_checkbox').val(counter);
            counter++;
        });
        checkonpageLoad();
    });
    
    function checkonpageLoad(){
        if($('input[type="radio"]:checked').val() == 'radio' || $('input[type="radio"]:checked').val() == 'checkbox'){
            $("#show_on_selection1").css("display", 'table-row');
            $("#show_on_selection2").css("display", 'table-row');
            $("#show_on_selection3").css("display", 'table-row');
        }else{
            $("#show_on_selection1").css("display", 'none');
            $("#show_on_selection2").css("display", 'none');
            $("#show_on_selection3").css("display", 'none');
          
        }
    }
    
    function removeans(ele){
        var current_id = $(ele).attr('id');
        $("#TextBoxDiv" + current_id).remove();
    }
    
    function checkselection(ele){
        // alert(ele.value);
        if(ele.value == 'radio' || ele.value == 'checkbox'){
            $("#show_on_selection1").css("display", 'table-row');
            $("#show_on_selection2").css("display", 'table-row');
            $("#show_on_selection3").css("display", 'table-row');
        }else{
            $("#show_on_selection1").css("display", 'none');
            $("#show_on_selection2").css("display", 'none');
            $("#show_on_selection3").css("display", 'none');
          
        }
    }
</script>
<style>
    #TextBoxesGroup img {
        margin: 6px;
    }

    #given_answers {
        float: left;
        width: 100%
    }

    .answer-image {
        float: left;
        max-height: 200px;
        max-width: 300px;
        margin: 0px 0px 10px 0px;
    }

    .answer-text-box {
        float: left;
        width: 30%;
    }

    .answer-img-box {
        float: left;
        width: 25%;
    }

    .answer-delete-box img {
        margin: 6px 0 0 0;
    }
</style>