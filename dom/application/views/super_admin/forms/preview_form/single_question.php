<script>
    $(function() {
        $( document ).tooltip();
    });
    $(function() {
        $(".datepicker").datepicker({ dateFormat: "dd-mm-yy" }).val()
    });
</script>
<div>

    <h2>  <?php echo $form->form_name; ?> </h2>
    <div class="span9">
        <h3><?php echo @$total_form_completed; ?>% compleet</h3>
        <div class="progress progress-striped active">
            <div class="bar" style="width: <?php echo @$total_form_completed . '%'; ?>;"></div>
        </div>
    </div>
    <form method="post" action="<?php echo base_url() . 'super_admin/forms/PreviewQuestionListener/' . $claim_id . '/' . $form->form_id . '/' . $category_details->cat_id . '/' . $question_details->question_id; ?>">
        <input type="hidden" value="<?php echo $category_details->cat_id; ?>" name="old_category_id">
        <table class="table">   
            <!-- Category Name -->
            <tr>
                <td colspan="3">
                    <h2><?php echo @$category_details->cat_name; ?></h2>
                </td>
            </tr>
            <!-- ********** -->

            <!-- Category Introduction Text -->
            <tr>
                <td colspan="3">
                    <?php echo @$category_details->introduction_text; ?>
                </td>
            </tr>
            <!-- ********** -->
            <!-- Question -->
            <tr>
                <td width="70%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question_details->question);?> <?php echo $question_details->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>  <!--  help text begins -->
                    <?php
                    if ($question_details->help_text != Null) {
                        ?>
                        <span title="<?php echo htmlentities($question_details->help_text); ?>"> <i
                                class="icon-question-sign"></i>
                        </span>
                    <?php } ?>
                    <!--  help text ends -->
                </td>

                <?php if (isset($answer_radio)) { ?>
                    <td width="20%" >
                        <?php
                        // get the options and interate thru them
                        $this->load->model('forms_categories_question_model');
                        $answers = $this->forms_categories_question_answer_model->get_where(array('question_id' => $question_details->question_id));
                        $user_ans = get_answer_given_by_admin($claim_id, $question_details->question_id);

                        foreach ($answers as $answer) {
                            ?>
                            <label class="radio">  <input
                                    name="<?php echo (isset($question_details->question_id) ? $question_details->question_id : '') . "_"; ?>" type="radio" 
                                    <?php
                                    if (isset($user_ans)) {
                                        if ($user_ans->answer_text == $answer->answer_id) {
                                            echo "checked='checked'";
                                        }
                                    }
                                    ?>
                                    value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>" class="input"
                                    <?php echo $question_details->required == 1 ? "" : ""; ?>> <?php echo $answer->answer; ?>

                                </input> </label>&nbsp;<br/>
                            <?php
                        }
                        ?>
                    </td>
                    <td width="9%" >
                        <?php echo form_error($question_details->question_id . "_"); ?>
                    </td>
                <?php } ?>

                <?php if (isset($answer_check_box)) { ?>
                    <td width="20%" >
                        <?php
                        $count_check = 0;
                        $this->load->model('forms_categories_question_answer_model');
                        $forms_categories_question_answer_model = new forms_categories_question_answer_model();
                        $answers = $forms_categories_question_answer_model->get_where(
                                array(
                                    'question_id' => $question_details->question_id
                                ));
                        $user_ans = get_answer_given_by_admin(
                                $claim_id, $question_details->question_id);

                        $this->load->model('temp_forms_answers_details_model');
                        $obj_form_answer = new temp_forms_answers_details_model;
                        if (isset($user_ans)) {
                            $re_ans = $obj_form_answer->get_where(array('user_answer_id' => $user_ans->answer_id));
                        }
                        foreach ($answers as $answer) {
                            $count_check++;
                            ?>
                            <label class="checkbox checkbox-align_preview_form label_checkbox" style="line-height : 10px !important; margin-bottom:-15px;"> <input
                                    name="<?php echo $question_details->question_id, '_[]'; ?>"
                                    <?php
                                    if (isset($re_ans)) {
                                        foreach ($re_ans as $r_ans) {
                                            if ($r_ans->answer_id == $answer->answer_id) {
                                                echo "checked='checked'";
                                            }
                                        }
                                    }
                                    ?>
                                    id="<?php echo $question_details->question_id, '_', $count_check; ?>"
                                    type="checkbox" value="<?php echo $answer->answer_id; ?>"
                                    <?php echo $question_details->required == 1 ? " " : ""; ?> class="input"> <?php echo $answer->answer; ?> </input>
                            </label> &nbsp;
                            <?php
                        }
                        ?>
                        <input type="hidden" value="<?php echo $count_check; ?>" name='<?php echo $question_details->question_id, '_', 'count' ?>'> 
                    </td>
                    <td width="9%" >
                        <?php echo form_error($question_details->question_id . "_"); ?>
                    </td>
                <?php } ?>

                <?php if (isset($answer_text)) { ?>
                    <td width="20%"><input name="<?php echo $question_details->question_id, '_'; ?>"
                                           type="text" class="input"
                                           <?php echo $question_details->required == 1 ? "" : ""; ?>
                                           value="<?php
                                       $user_ans = get_answer_given_by_admin(
                                               $claim_id, $question_details->question_id);

                                       if (isset($user_ans)) {
                                           echo $user_ans->answer_text;
                                       }
                                           ?>"></td>
                    <td width="9%">
                        <?php echo form_error($question_details->question_id . "_"); ?>
                    </td>
                <?php } ?>
                <?php if (isset($answer_date)) { ?>
                    <td width="20%"><input name="<?php echo $question_details->question_id, '_'; ?>"
                                           type="text" class="input datepicker"
                                           <?php echo $question_details->required == 1 ? "" : ""; ?>
                                           value="<?php
                                       $user_ans = get_answer_given_by_admin(
                                               $claim_id, $question_details->question_id);

                                       if (isset($user_ans)) {
                                           echo $user_ans->answer_text;
                                       }
                                           ?>"></td>
                    <td width="9%">
                        <?php echo form_error($question_details->question_id . "_"); ?>
                    </td>
                <?php } ?>
                <?php if (isset($answer_number)) { ?>
                    <td width="20%"><input name="<?php echo $question_details->question_id, '_'; ?>"
                                           type="text" class="input"
                                           <?php echo $question_details->required == 1 ? "" : ""; ?>
                                           value="<?php
                                       $user_ans = get_answer_given_by_admin(
                                               $claim_id, $question_details->question_id);

                                       if (isset($user_ans)) {
                                           echo $user_ans->answer_text;
                                       }
                                           ?>"></td>
                    <td width="9%">
                        <?php echo form_error($question_details->question_id . "_"); ?>
                    </td>
                <?php } ?>

                <?php if (isset($answer_textarea)) { ?>
                    <td width="20%"><textarea name="<?php echo $question_details->question_id . "_"; ?>"
                                              <?php echo $question_details->required == 1 ? "" : ""; ?>><?php
                                          $user_ans =
                                                  get_answer_given_by_admin($claim_id, $question_details->question_id);
                                          if
                                          (isset($user_ans)) {
                                              echo
                                              htmlentities($user_ans->answer_text);
                                          }
                                              ?></textarea></span></td>

                    <td width="9%">
                        <?php echo form_error($question_details->question_id . "_"); ?>
                    </td>
                <?php } ?>
            </tr>
            <!-- ********** -->

            <tr>
                <td colspan="3">
                    <?php if (isset($prev_url)) { ?>
                        <a href="<?php echo str_replace('PreviewQuestionListener', 'preview_question', $prev_url); ?>" class="btn">Terug</a>
                        <?php
                    } else {
                        if (isset($prev_question) && $prev_question['prev_cat']->cat_id != 'end' && $prev_question['prev_question']->question_id != 'end') {
                            ?>
                            <a href="<?php echo base_url() . 'super_admin/forms/preview_question/' . $claim_id . '/' . $form->form_id . '/' . $prev_question['prev_cat']->cat_id . '/' . $prev_question['prev_question']->question_id; ?>" class="btn">Terug</a>
                        <?php } else { ?>
                            <a href="<?php echo base_url() . 'super_admin/forms/preview_question_start/' . $claim_id . '/' . $form->form_id; ?>" class="btn">Terug</a>
                            <?php
                        }
                    } if (isset($next_question)) {
                        ?>
                        <input type="submit" class="btn" value="Volgende" />
                    <?php } ?>
                </td>
            </tr>
        </table>
    </form>
</div>