<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script>
    $(function() {
        $( document ).tooltip();
    });
    $(function() {
        $(".datepicker").datepicker({ dateFormat: "dd-mm-yy" }).val()
    });
</script>
<style>
    label {
        display: inline-block;
        /*width: 4em;*/
    }

    .multiform-margin {
        margin-right: 20px;
    }

    form {
        margin: 0px;
    }
</style>

<!-- actual page content begins -->

<div>
    <h2>  <?php echo $form->form_name; ?> </h2>
    <div class="span9">
        <h3><?php echo @$total_form_completed; ?>% form completed</h3>
        <div class="progress progress-striped active">
            <div class="bar" style="width: <?php echo @$total_form_completed . '%'; ?>;"></div>
        </div>
    </div>


    <!-- questions -->
    <form method="post"
          action="  <?php
if (isset($category) && $category->cat_id != '') {
    if (isset($smart_sequence) && $smart_sequence != null) {
        echo base_url() . "super_admin/forms/PreviewCategoryListener/" .
        $claim_id . "/" . $form->form_id . "/" . $category->cat_id . "/" .
        $smart_sequence;
    } else {
        echo base_url() . "super_admin/forms/PreviewCategoryListener/" .
        $claim_id . "/" . $form->form_id . "/" . $category->cat_id;
    }
} else {
    echo base_url() . "super_admin/forms/PreviewCategoryListener/" . $claim_id .
    "/" . $form->form_id . "/end";
}
?>
          ">

        <table class="table">
            <?php
            if (isset($category)) {

                if (isset($smart_sequence) && $smart_sequence != null) {
                    $this->load->model('forms_categories_question_model');
                    $obj_ques = new forms_categories_question_model();
                    $get_ques = $obj_ques->get_where(
                            array('cat_id' => $category->cat_id,
                                'sequence >' => $smart_sequence
                            ));

                    if (count($get_ques) > 0) {
                        $questions = $category->get_question_if_smart(
                                $smart_sequence, false);
                    } else {
                        $questions = $category->get_question_if_smart(
                                $smart_sequence, true);
                    }
                } else {
                    $questions = $category->get_question_smart();
                }
                ?>
                <tr>
                    <td colspan="3">
                        <h3><?php echo $category->cat_name; ?> </h3>
                        <?php echo $category->introduction_text; ?>        
                    </td>
                </tr>
                <?php
                if (isset($questions)) {
                    if (is_array($questions) && count($questions) > 0) {
                        foreach ($questions as $question) {
                            if ($question->answer_kind == 'text') {
                                ?>
                                <tr>
                                    <td width="70%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                        <!--  help text begins --> <span
                                        <?php
                                        if ($question->help_text != Null) {
                                            ?>
                                                title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                    class="icon-question-sign"></i>
                                                <?php } ?>
                                        </span> <!--  help text ends --></td>
                                    <td width="20%"><input
                                            name="<?php echo $question->question_id, '_'; ?>" type="text"
                                            class="input" <?php echo $question->required == 1 ? "" : ""; ?>
                                            value="<?php
                            if ($this->input->post(
                                            $question->question_id . '_') != '') {
                                echo $this->input->post(
                                        $question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_admin(
                                        $claim_id, $question->question_id);

                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                                ?>"></td>

                                    <td>
                                        <?php echo form_error($question->question_id . "_"); ?>
                                    </td>

                                </tr>
                                <?php
                            } elseif ($question->answer_kind == 'date') {
                                ?>
                                <tr>
                                    <td width="70%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                        <!--  help text begins --> <span
                                        <?php
                                        if ($question->help_text != Null) {
                                            ?>
                                                title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                    class="icon-question-sign"></i>
                                                <?php } ?>
                                        </span> <!--  help text ends --></td>
                                    <td width="20%"><input
                                            name="<?php echo $question->question_id, '_'; ?>" type="text"
                                            class="input datepicker" <?php echo $question->required == 1 ? "" : ""; ?>
                                            value="<?php
                                                if ($this->input->post(
                                                                $question->question_id . '_') != '') {
                                                    echo $this->input->post(
                                                            $question->question_id . '_');
                                                } else {
                                                    $user_ans = get_answer_given_by_admin(
                                                            $claim_id, $question->question_id);

                                                    if (isset($user_ans)) {
                                                        echo $user_ans->answer_text;
                                                    }
                                                }
                                                ?>"></td>

                                    <td>
                                        <?php echo form_error($question->question_id . "_"); ?>
                                    </td>

                                </tr>
                                <?php
                            } else if ($question->answer_kind == 'number') {
                                ?>
                                <tr>
                                    <td width="70%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                        <!--  help text begins --> <span
                    <?php
                    if ($question->help_text != Null) {
                        ?>
                                                title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                    class="icon-question-sign"></i>
                    <?php } ?>
                                        </span> <!--  help text ends --></td>
                                    <td width="20%"><input
                                            name="<?php echo $question->question_id, '_'; ?>" type="text"
                                            class="input" <?php echo $question->required == 1 ? "" : ""; ?>
                                            value="<?php
                    if ($this->input->post(
                                    $question->question_id . '_') != '') {
                        echo $this->input->post(
                                $question->question_id . '_');
                    } else {
                        $user_ans = get_answer_given_by_admin(
                                $claim_id, $question->question_id);

                        if (isset($user_ans)) {
                            echo $user_ans->answer_text;
                        }
                    }
                    ?>"></td>

                                    <td>
                    <?php echo form_error($question->question_id . "_"); ?>
                                    </td>

                                </tr>
                    <?php
                } else if ($question->answer_kind == 'textarea') {
                    ?>
                                <tr>
                                    <td width="70%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                        <!--  help text begins -->
                    <?php
                    if ($question->help_text != null) {
                        ?>
                                            <span
                                                title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                    class="icon-question-sign"></i>
                                            </span>
                    <?php } ?>
                                        <!--  help text ends --> </span>
                                    </td>
                                    <td width="20%"><textarea
                                            name="<?php echo $question->question_id . "_"; ?>"
                    <?php echo $question->required == 1 ? "" : ""; ?>><?php
                    if ($this->input->post(
                                    $question->question_id . '_') != '') {
                        echo $this->input->post(
                                $question->question_id . '_');
                    } else {
                        $user_ans = get_answer_given_by_admin(
                                $claim_id, $question->question_id);
                        if (isset($user_ans)) {
                            echo htmlentities(
                                    $user_ans->answer_text);
                        }
                    }
                    ?></textarea></td>

                                    <td>
                                            <?php echo form_error($question->question_id . "_"); ?>
                                    </td>
                                </tr>
                                        <?php
                                    } else if ($question->answer_kind == 'radio') {
                                        ?>
                                <tr>
                                    <td width="70%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?> <!--  help text begins -->
                                <?php
                                if ($question->help_text != Null) {
                                    ?>
                                            <span
                                                title="<?php echo htmlentities($question->help_text); ?>"> <i
                                                    class="icon-question-sign"></i>
                                            </span>
                    <?php } ?>
                                        <!--  help text ends -->
                                    </td>
                                    <td width="20%">
                    <?php
                    // get the options and interate thru them
                    $this->load->model(
                            'forms_categories_question_model');
                    $answers = $this->forms_categories_question_answer_model->get_where(
                            array(
                                'question_id' => $question->question_id
                            ));

                    $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);

                    foreach ($answers as $answer) {
                        ?>
                                            <label class="radio"> <input
                                                    name="<?php echo (isset($question->question_id) ? $question->question_id : '') . "_"; ?>"
                                                    type="radio"
                        <?php
                        if ($this->input->post(
                                        $question->question_id . '_') ==
                                $answer->answer_id) {
                            echo "checked='checked'";
                        } elseif (isset($user_ans)) {
                            if ($user_ans->answer_text ==
                                    $answer->answer_id) {
                                echo "checked='checked'";
                            }
                        }
                        ?>
                                                    value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>"
                                                    class="input" <?php echo $question->required == 1 ? "" : ""; ?>> <?php echo $answer->answer; ?>

                                                </input>
                                            </label>&nbsp;<br />
                        <?php
                    }
                    ?>

                                    </td>
                                    <td>
                    <?php echo form_error($question->question_id . "_"); ?>
                                    </td>
                                </tr>
                                        <?php
                                    } else if ($question->answer_kind == 'checkbox') {
                                        ?>
                                <tr>
                                    <td width="70%" style="vertical-align: text-top;" valign="top"> <?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                        <!--  help text begins -->
                    <?php
                    if ($question->help_text != Null) {
                        ?>
                                            <span
                                                class="icon-question-sign"
                                                title="<?php echo htmlentities($question->help_text); ?>"></span>
                    <?php } ?>
                                        <!--  help text ends -->
                                    </td>
                                    <td width="20%">  <?php
                    $count_check = 0;

                    // get the options and interate thru them
                    $this->load->model(
                            'forms_categories_question_answer_model');
                    $forms_categories_question_answer_model = new forms_categories_question_answer_model();
                    $answers = $forms_categories_question_answer_model->get_where(
                            array(
                                'question_id' => $question->question_id
                            ));
                    $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);

                    $this->load->model(
                            'temp_forms_answers_details_model');
                    $obj_form_answer = new temp_forms_answers_details_model();
                    if (isset($user_ans)) {
                        $re_ans = $obj_form_answer->get_where(
                                array(
                                    'user_answer_id' => $user_ans->answer_id
                                ));
                    }
                    foreach ($answers as $answer) {
                        $count_check++;
                        ?>
                                            <label
                                                class="checkbox checkbox-align_preview_form label_checkbox"
                                                style="line-height: 10px!importan; margin-bottom: -15px;"> <input
                                                    name="<?php echo $question->question_id, '_[]'; ?>"
                        <?php
                        if (isset($re_ans)) {
                            foreach ($re_ans as $r_ans) {
                                if ($r_ans->answer_id ==
                                        $answer->answer_id) {
                                    echo "checked='checked'";
                                }
                            }
                        }
                        ?>
                                                    id="<?php echo $question->question_id, '_', $count_check; ?>"
                                                    type="checkbox"
                                                    <?php echo set_checkbox($question->question_id . '_', $answer->answer_id); ?>
                                                    value="<?php echo $answer->answer_id; ?>"
                                                    <?php echo $question->required == 1 ? " " : ""; ?> class="input"> <?php echo $answer->answer; ?> </input>
                                            </label> &nbsp;
                                                    <?php
                                                }
                                                ?>
                                        <input type="hidden"
                                               value="<?php echo $count_check; ?>"
                                               name='<?php echo $question->question_id, '_', 'count' ?>'>

                                    </td>
                                    <td>
                    <?php echo form_error($question->question_id . "_"); ?>
                                    </td>
                                </tr>
                                        <?php
                                    } else {
                                        echo "<tr><td>Invalid question type</td></tr>";
                                    }
                                }
                                ?>
            <!-- <tr> <td colspan="2"> <a href="#" class="btn pull-right"> Opslaan </a> </td> </tr> -->
                        <?php
                    }
                } else {
                    echo "No Questions in this category";
                }
            }
            ?>

        </table>
        <table style="background: #fff;">
            <tr>
                <td colspan="3">

<?php
// var_dump($prev_url_smart);

if (isset($prev_url_smart) && $prev_url_smart != '') {
    $count = count($prev_url_smart);
    $check = array_search(current_url(), $prev_url_smart);
    $prev = ( $check === false ) ? ( $count - 1 ) : ( $check - 1 );
    if ($prev >= 0) {
        ?>
                            <a href="<?php echo $prev_url_smart[$prev]; ?>"
                               class="btn">Terug</a>
                            <?php
                        } else {
                            ?>
                            <a
                                href="<?php echo base_url() . 'super_admin/forms/preview_category_start/' . $claim_id . '/' . $form->form_id; ?>"
                                class="btn">Terug</a>

        <?php
    }
} else {
    ?>
                        <a
                            href="<?php echo base_url() . 'super_admin/forms/preview_category_start/' . $claim_id . '/' . $form->form_id; ?>"
                            class="btn">Terug</a>

    <?php
}
?>
                    <input type="submit" class="btn" value="Volgende" />

                </td>
            </tr>
        </table>
    </form>