<style>
    .multiform-margin{margin-right: 20px;}
    form { margin: 0px; }
</style>
<div>
    <h2>  <?php echo $form->form_name; ?> </h2>
    <div class="span9">
        <h3><?php echo @$total_form_completed; ?>% compleet</h3>
        <div class="progress progress-striped active">
            <div class="bar" style="width: <?php echo @$total_form_completed . '%'; ?>;"></div>
        </div>
    </div>
    <table class="table ">
        <tr>
            <td style="vertical-align: top">
                <img class="pull-left" src='<?php echo assets_url_img . "exclamation.png"; ?>' alt='exclamation' title='exclamation' style="margin: 3px;">
            </td>
            <td> <?php echo $form->closure_text; ?></td>
        </tr>

    </table>
    <table style="background: #fff;">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <?php
                $this->load->model('claims_files_model');
                $files = claims_files_model::get_claims_for_claim_id($claim_id);

                if (count($files) == 0) {
                    echo "<b>Bijlagen</b>";
                } else {
                    echo "<b>Bijlagen</b><br>";
                    foreach ($files as $file) {
                        echo anchor($file->get_path_for_web(), $file->file_name, 'target="_blank"');
                        echo '&nbsp;&nbsp;&nbsp;';
                        echo anchor(base_url() . 'admin/client_forms/delete_attachment/' . $claim_id . '/' . $form->form_id . '/' . $file->file_id, '<i class="icon-trash"></i>');
                        echo "<br>";
                    }
                }
                ?>
                <form method="post" action="<?php echo base_url() . "user/answer/upload_attachment_once/" . $claim_id; ?>" enctype="multipart/form-data">
                    <input type="hidden" disabled="disabled" name="prev_url" value="<?php echo $prev_url; ?>" />
                    <input type="hidden" disabled="disabled" name="form_id" value="<?php echo $form->form_id; ?>" />
                    <input type="file" disabled="disabled" class="input" name="attachment" /> <input type="submit" disabled="disabled" value="Toevoegen" class="button btn">
                </form>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <label class="checkbox">
                    <input type="checkbox" value="yes" name="filled_in_honestly" id="filled_in_honestly"> Naar waarheid ingevuld of gecontroleerd. <font color="red">*</font> 
                    <span id="error-text" style="color: red;"></span><br>
                </label>
            </td>
        </tr>
        <tr>
            <td><br>
                De met een <font color="red">* </font> gemarkeerde velden zijn verplicht.

            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <div class="row-fluid">
                    <?php
                    $check = $this->session->flashdata('flash_prev_url');
                    $this->session->keep_flashdata('flash_prev_url');
                    if ($check != '') {
                        $url = $check;
                    } else if (isset($prev_url)) {
                        $url = $prev_url;
                    }
                    if (isset($url)) {
                        ?>
                        <a href="<?php echo str_replace('PreviewQuestionListener', 'preview_question', $url); ?>" class="btn pull-left multiform-margin">Terug</a>
                    <?php } else { ?>
                        <a href="<?php echo base_url() . 'super_admin/forms/preview_question_start/' . $claim_id . '/' . $form->form_id; ?>" class="btn pull-left multiform-margin">Terug</a>
                    <?php } ?>
                    <!-- link for Opslaan button -->
                    <form method="post" action="<?php echo base_url() . "super_admin/forms/end_preview_one_by_one/" . $claim_id; ?>" class=" pull-left multiform-margin">
                        <input type="submit" value="Opslaan in concept" class="button btn" />
                    </form>
                    <!-- link for Opslaan and submit button -->
                    <form method="post" action="<?php echo base_url() . "super_admin/forms/end_preview_confirm_and_send_one_by_one/" . $claim_id; ?>" class="pull-left multiform-margin">
                        <input type="submit" value="Bevestig en verstuur" class="button btn" onclick="return check_confirm()" />
                    </form>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    function check_confirm()
    {
        if ( document.getElementById('filled_in_honestly').checked ==false) 
        {
            document.getElementById('error-text').innerHTML = '* Dit veld is verplicht';
            document.getElementById('remove-star').innerHTML = '';
            //alert('Please check "I Filled in form honestly"');
            return false ;
        }
        else
        {
            return true ;
        } 
    }
</script>