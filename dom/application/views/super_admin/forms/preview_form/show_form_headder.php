<div>
	<h2>  <?php echo $form->form_name; ?> </h2>
	<div class="span9">
		<h3><?php echo @$total_form_completed; ?>% form completed</h3>
		<div class="progress progress-striped active">
			<div class="bar" style="width: <?php echo @$total_form_completed . '%'; ?>;"></div>
		</div>
	</div>
 <table class="table" style="margin-top: 10px;">
		<tr>
			<td>

                <?php echo $form->introduction_text; ?>
            </td>
		</tr>
	</table>

	<table>
		<tr>
			<td><a href="<?php echo base_url() . 'super_admin/forms'; ?>"
				class="btn">Terug</a>
                <?php if (isset($next_question)) { ?>
                    <a
				href="<?php echo base_url() . 'super_admin/forms/preview_question/' . $claim_id . '/' . $form->form_id . '/' . $next_question['next_cat']->cat_id . '/' . $next_question['next_question']->question_id; ?>"
				class="btn">Volgende</a>
                <?php } ?>
            </td>
		</tr>
	</table>
</div>