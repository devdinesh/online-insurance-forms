<h2><span>Toevoegen formulier</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'super_admin/forms/addListener'; ?>"
        method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-striped table-condensed"
               border="1">
            <tbody>
                <tr>
                    <td>Client<span class="mandatory">*</span></td>
                    <td><select name="client_id">
                            <option value="">Select Client</option>
                            <?php foreach ($client_details as $client) { ?>
                                <option
                                    value="<?php echo $client->client_id; ?>"
                                    <?php
                                    if (set_value('client_id') == $client->client_id) {
                                        echo "selected=selected";
                                    }
                                    ?>><?php echo $client->client_name; ?></option>
                                <?php } ?>
                        </select></td>

                    <td><?php echo form_error('client_id'); ?></td>
                </tr>

                <tr>
                    <td>Naam <span class="mandatory">*</span></td>
                    <td><input class="input-append" type="text" name="form_name"
                               value="<?php echo set_value('form_name'); ?>" /></td>
                    <td><?php echo form_error('form_name'); ?></td>
                </tr>

                <tr>
                    <td>Code<span class="mandatory">*</span></td>
                    <td><input type="text" name="form_tag"
                               value="<?php echo set_value('form_tag'); ?>" /></td>
                    <td><?php echo form_error('form_tag'); ?></td>
                </tr>


                <tr>
                    <td>Eerste Herinnering:</td>
                    <td><input type="text" name="f_reminder"
                               value="<?php echo set_value('f_reminder'); ?>" />Dagen</td>
                    <td><?php echo form_error('f_reminder'); ?></td>
                </tr>
                <tr>
                    <td>Tweede Herinnering:</td>
                    <td><input type="text" name="s_reminder"
                               value="<?php echo set_value('s_reminder'); ?>" />Dagen</td>
                    <td><?php echo form_error('s_reminder'); ?></td>
                </tr>

                <tr>
                    <td>Polishouder moet formulier invullen</td>
                    <td colspan="1"><input type="radio" name="fill_in_needed" value="1"
                        <?php
                        if (set_value('fill_in_needed') == '1') {
                            echo "checked=\"checked\"";
                        }
                        ?>
                                           checked="checked"> Ja &nbsp; <input type="radio"
                                           name="fill_in_needed" value="0"
<?php
if (set_value('fill_in_needed') == '0') {
    echo "checked=\"checked\"";
}
?>> Nee &nbsp;</td>
                    <td><?php echo form_error('fill_in_needed'); ?></td>
                </tr>

                <tr>
                    <td>Koptekst</td>

                    <td><textarea id="headder_text" name="header_text" rows="5"
                                  class="tinymce"><?php echo (set_value('header_text')); ?></textarea></td>
                    <td><?php echo form_error('header_text'); ?></td>
                </tr>

                <tr>
                    <td>Introductietekst</td>

                    <td><textarea id="intorduction_text" name="introduction_text"
                                  rows="5" class="tinymce"><?php echo (set_value('introduction_text')); ?></textarea></td>
                    <td><?php echo form_error('introduction_text'); ?></td>
                </tr>
                <tr>
                    <td>Afsluitende Tekst</td>

                    <td><textarea id="closure_text" name="closure_text" rows="5"
                                  class="tinymce"><?php echo (set_value('closure_text')); ?></textarea></td>
                    <td><?php echo form_error('closure_text'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><input type="radio" value="0"
                                           name="show_all_question_at_once" checked="checked" /> Toon alle
                        vragen in een keer &nbsp; <input type="radio" <?php if (set_value('show_all_question_at_once') == '1') {
                                               echo 'checked="checked"';
                                           }
?> value="1"
                                                         name="show_all_question_at_once" /> Toon vragen ��n voor ��n&nbsp; <input type="radio" value="2"
                                                         <?php if (set_value('show_all_question_at_once') == '2') {
                                                             echo 'checked="checked"';
                                                         }
                                                         ?>
                                                         name="show_all_question_at_once" /> Toon alle vragen in ��n keer per categorie&nbsp;</td>
                </tr>

                <tr>
                    <td colspan="3"><button type="submit"
                                            class="btn btn-large btn-primary">Opslaan</button> &nbsp;&nbsp; <a
                                            href="<?php echo base_url() . 'super_admin/forms'; ?>" class="btn btn-large btn-primary">Annuleer</a></td>
                </tr>

                <tr>
                    <td colspan="3">Velden gemarkeerd met een<span class="mandatory">*</span>
                        zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',
            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>