<!-- <div class="row-fluid">
        <p class="span12">
                <a href="<?php echo base_url() . "super_admin"; ?>"> Home </a> &gt; <a
                        href="<?php echo base_url() . "super_admin/forms"; ?>"> Formulieren </a>
                &gt; <a
                        href="<?php echo base_url() . "super_admin/forms/categories/" . $frm_id; ?>">
                        Categorieen</a> &gt; <a
                        href="<?php echo base_url() . "super_admin/forms/categories/question/" . $cat_id; ?>">
                        Vraag</a> &gt; <a
                        href="<?php echo base_url() . "super_admin/answers/" . $question_id; ?>">
                        Antwoorden</a> &gt; Uitgeven
        </p>
</div>
-->
<script type="text/javascript">

    $(document).ready(function() {
        /**
when the document is loaded, by default the radio button will be checked,
        according to the radio button checked, we call approapriate function to load
        the checkboxes
         */
        var go_to_none_var = document.getElementById("go_to_none");
        var go_to_end_var = document.getElementById("go_to_end");
        var go_to_question_var = document.getElementById("go_to_question");
	
        if(go_to_none_var.checked==true)
        {
            show_none();
        }
        else if(go_to_question_var.checked==true)
        {
            show_question_list();
            getcategoryquestion(<?php echo @$skip_cat_id; ?>);
        }else if(go_to_end_var.checked==true)
        {
            show_none();
        }
	 
    });
	
  
    function show_none()
    {
        // puts empy contents 
        $('#aa').load('<?php echo base_url('super_admin/forms_categories_question_answer/get_edit_question_answer_cat_list_for_edit_page/none'); ?>');
        $('#bb').load('<?php echo base_url('super_admin/forms_categories_question_answer/get_edit_question_answer_cat_list_for_edit_page_values/none/0'); ?>');
        $('#cc').load('<?php echo base_url('super_admin/forms_categories_question_answer/get_edit_question_answer_cat_list_for_edit_page/none'); ?>');
    }


    function show_question_list()
    {
        // loads the list of questions
        $('#aa').load('<?php echo base_url('super_admin/forms_categories_question_answer/get_edit_question_answer_cat_list_for_edit_page/to_question'); ?>');
        $('#bb').load('<?php echo base_url('super_admin/forms_categories_question_answer/get_edit_question_answer_cat_list_for_edit_page_values/to_question/' . $answer->answer_id); ?>');
    }
</script>



<h2><span>Details Antwoord</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'super_admin/answer/edit_listener' ?>"
        method="post">
        <input type="hidden" value="<?php echo $answer->answer_id; ?>"
               name="answer_id"> <input type="hidden"
               value="<?php echo $answer->question_id; ?>" name="question_id">
        <table class="table table-striped table-condensed">
            <tbody>
                <tr>
                    <td>Antwoord<span class="mandatory">*</span></td>
                    <td><input type="text" name="answer" min="0"
                               value="<?php
echo htmlentities(
        set_value('answer') != '' ? set_value('answer') : isset(
                        $answer->answer) ? $answer->answer : '');
?>" /></td>
                    <td><?php echo form_error('answer'); ?></td>
                </tr>
                <?php if ($display_smat_form == true) { ?>
                    <tr>
                        <td>Ga naar vraag / categorie</td>
                        <td>

                            <div class="control-group">

                                <div class="controls">

                                    <!-- span for go to none -->
                                    <span> <label for="go_to_none" style="width: 70px;"
                                                  class="radio inline"> <input type="radio" id="go_to_none"
                                                                     name="go_to_type" value="to_none"
                                                                     <?php echo (!$answer->skip_to_questions) && (!$answer->skip_to_category) ? 'checked="checked"' : ''; ?>
                                                                     onclick="show_none()"> Geen
                                        </label>
                                    </span>
                                    <!-- span for go to question -->
                                    <span> <label for="go_to_question" style="width: 100px;"
                                                  class="radio inline"> <input type="radio" id="go_to_question"
                                                      <?php echo ($answer->skip_to_questions) ? 'checked="checked"' : ''; ?>
                                                                     name="go_to_type" value="to_question"
                                                                     onclick="show_question_list()"> Naar vraag
                                        </label>
                                    </span>
                                    <span> <label for="go_to_end" style="width: 100px;" class="radio inline">
                                            <input <?php echo ($answer->skip_to_questions === '0') ? 'checked="checked"' : ''; ?> type="radio" id="go_to_end" name="go_to_type" value="to_end" onclick="show_none()"> Einde formulier
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                <?php } ?>
                <tr id="hide_item">
                    <td><span id="aa"> </span></td>
                    <td><span id="bb"> </span></td>
                    <td></td>
                </tr>
                <tr id="cc">
                </tr>
                <tr>
                    <td colspan="3"><input type="submit" class="btn btn-large btn-primary" value="Opslaan"> <!--  main save button -->

                        <!-- cancel button --> <a
                            href="<?php echo base_url() . "super_admin/answers/" . $answer->question_id; ?>"
                            class="btn btn-large btn-primary"> Annuleer </a></td>
                </tr>
                <tr>
                    <td colspan="3">Velden die gemarkeerd zijn met een <span
                            class="mandatory">*</span> zijn verplicht
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
<script type="text/javascript">
    function getcategoryquestion(value){

        var cat_id=value;
        var answer_id="<?php echo $answer->answer_id; ?>";
	
        var url = "<?php echo base_url(); ?>" +  "super_admin/forms_categories_question_answer/get_category_question/" + cat_id+"/"+answer_id ;
        $('#cc').load(url);
    }
</script>

