<!--  <div class="row-fluid">
        <p class="span12">
                <a href="<?php echo base_url() . "super_admin"; ?>"> Home </a> &gt; <a
                        href="<?php echo base_url() . "super_admin/forms"; ?>"> Formulieren </a>
                &gt; <a
                        href="<?php echo base_url() . "super_admin/forms/categories/" . $frm_id; ?>">
                        Categorieen</a> &gt; <a
                        href="<?php echo base_url() . "super_admin/forms/categories/question/" . $cat_id; ?>">
                        Vraag</a>&gt; Vraag
        </p>
</div>
-->
<h2><span>Antwoorden</span></h2>
<p>Overzicht antwoorden voor vraag <?php echo strip_tags($question); ?></p>

<div>
    <p>
        <a class="btn btn-large btn-primary" style="margin-bottom: 5px;"
           href="<?php echo base_url() . "super_admin/answer/add/" . $question_id ?>">
            Toevoegen antwoord </a> &nbsp; <a class="btn btn-large btn-primary"
                                          style="margin-bottom: 5px;"
                                          href="<?php echo base_url() . "super_admin/forms/categories/question/" . $cat_id; ?>">
            Terug </a>
    </p>

    <!-- error message start -->
    <table width="100%" cellspacing="0" cellpadding="0">
        <?php if ($this->session->flashdata('success') != '') { ?>
            <tr>
                <td>
                    <div class="alert alert-success">
                        <a class="close" data-dismiss="alert"
                           href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('success'); ?>
                    </div>
                </td>
            </tr>
        <?php } ?>
        <?php if ($this->session->flashdata('error') != '') { ?>
            <tr>
                <td>
                    <div class="alert alert-error">
                        <a class="close" data-dismiss="alert"
                           href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('error'); ?>
                    </div>
                </td>
            </tr>
        <?php } ?>

    </table>
    <!-- error message end -->

    <!-- main table start -->
    <table class="table table-striped table-condensed"
           id="list_category_question" width="100%" border="0" cellspacing="0"
           cellpadding="0">
        <thead>
            <tr>
                <th>Antwoord</th>
                <th>Ga naar vraag</th>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td>etc</td>
                <td>etc</td>

            </tr>
        </tbody>
    </table>
</div>
<br />
<br />
<br />
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_category_question').dataTable( {
            //  "bJQueryUI": true,
            "bProcessing":true,
            "bPaginate" : false,
            "bFilter": false,
            "bInfo": false,
            "bSort": false, 
            "sPaginationType": "full_numbers",
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"}
            ],
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": "<?php echo site_url("super_admin/answers/get_json/$question_id"); ?>"
        } );
    } );
    
    function deleteRow(ele){
        var id = $(ele).attr('id');
        //   var parent = $(ele).parent().parent();
        if( confirm("Do you want to delete?"))
        {
            location.href="<?php echo base_url(); ?>super_admin/answers/get_json/" +<?php echo $question_id; ?>+"/"+id;
        }
                    
        return false;
    }
</script>