<h2><span>List All Categories of <?php echo $form_details->form_name; ?> Form</span></h2>
<div class="row-fluid">
    <p class="span12">
        <a
            href="<?php echo base_url() . 'super_admin/forms/categories/add/' . $form_details->form_id; ?>"
            class="btn btn-large btn-primary">Add New Category</a> &nbsp; <a
            href="<?php echo base_url() . 'super_admin/forms'; ?>" class="btn btn-large btn-primary">Back</a>
    </p>

</div>
<div>
    <table width="100%" cellspacing="0" cellpadding="0">
        <?php if ($this->session->flashdata('success') != '') { ?>
            <tr>
                <td>
                    <div class="alert alert-success">
                        <a class="close" data-dismiss="alert"
                           href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('success'); ?>
                    </div>
                </td>
            </tr>
        <?php } ?>
        <?php if ($this->session->flashdata('error') != '') { ?>
            <tr>
                <td>
                    <div class="alert alert-error">
                        <a class="close" data-dismiss="alert"
                           href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('error'); ?>
                    </div>
                </td>
            </tr>
        <?php } ?>
    </table>
    <table class="table table-striped table-condensed"
           id="tbl_category_list" width="100%" border="0" cellspacing="0"
           cellpadding="0">
        <thead>
            <tr>
                <th width="30%">Categorienaam</th>
                <th width="20%">Vragen</th>
                <th width="2%"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>etc</td>
                <td>etc</td>
                <td>etc</td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#tbl_category_list').dataTable( {
            //  "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            'iDisplayLength': 100, 
            "bProcessing": true,   
            "bSort":false,
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"}
            ],
            "sAjaxSource": "<?php echo site_url("super_admin/forms/categories/getJson/$form_details->form_id"); ?>",
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                $(nRow).attr('id', aData[3]);
            }
        } );
        $(function() {
            $("#tbl_category_list tbody").sortable({ opacity: 0.6, cursor: 'move', update: function() {
                    var order = $(this).sortable("serialize");
                    var url = "<?php echo base_url() . 'super_admin/forms/categories/sortable'; ?>";
                    $.post(url, order, function(){
                        window.location.reload();
                    });   
                }                                
            });
        });
    } );
    
    function deleteRow(ele){
        var id = $(ele).attr('id');
        //   var parent = $(ele).parent().parent();
        if( confirm("Do you want to delete?"))
        {
            location.href="<?php echo base_url(); ?>super_admin/forms/categories/delete/" +<?php echo $form_details->form_id; ?>+"/"+id;
        }
                    
        return false;
    }
</script>