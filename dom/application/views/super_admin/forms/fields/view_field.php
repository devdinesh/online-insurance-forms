<h2><span>Lijst import velden</span></h2>
<div class="row-fluid">
    <p class="span12">
        <a href="<?php echo base_url() . 'super_admin/form_fields/add/' . $form_id; ?>" class="btn btn-large btn-primary">Add New Field</a> 
    </p>

</div>

<div>

    <table class="table table-striped table-condensed"
           id="list_forms_field" width="100%" border="0" cellspacing="0"
           cellpadding="0">
        <thead>
            <tr>
                <th>Naam import Item</th>
                <th>Weergeven als</th>
                <th></th>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td>etc</td>
                <td>etc</td>
                <td>etc</td>

            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_forms_field').dataTable( {
            //  "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            'iDisplayLength': 100,  
            "bProcessing": true,   
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"}
                
            ],
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": "<?php echo site_url("super_admin/form_fields/getJson/$form_id"); ?>"
        } );
    } );

    function deleteRow(ele){
        var id = $(ele).attr('id');
        //   var parent = $(ele).parent().parent();
        if( confirm("Do you want to delete ?"))
        {
            location.href="<?php echo base_url(); ?>super_admin/form_fields/delete_field/" +id;
        }
                    
        return false;
    }
   
</script>