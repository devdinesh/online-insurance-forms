
<h2><span>forms Fields</span></h2>

<div>
    <form
        action="<?php echo base_url() . 'super_admin/form_fields/addListener/' . $form_id; ?>"
        method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>

                <tr>
                    <td>Naam import Item: <span class="mandatory">*</span></td>
                    <td><input class="input-append" type="text" name="field_name"
                               value="<?php echo set_value('field_name'); ?>" /></td>
                    <td><?php echo form_error('field_name'); ?></td>
                </tr>

                <tr>
                    <td>Weergeven als:<span class="mandatory">*</span></td>
                    <td><input type="text" name="name_on_form"
                               value="<?php echo set_value('name_on_form'); ?>" /></td>
                    <td><?php echo form_error('name_on_form'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                        &nbsp;&nbsp; <a
                            href="<?php echo base_url() . 'super_admin/super_form_fields/' . $form_id; ?>"
                            class="btn btn-large btn-primary">Annuleer</a></td>
                </tr>

                <tr>
                    <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                        zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />

