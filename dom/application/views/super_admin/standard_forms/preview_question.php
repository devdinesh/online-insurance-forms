<script type="text/javascript">
    <!--
    function go_back()
    {
        document.forms['main_frm'].action.value = 'back';
        document.getElementById("main_frm").submit();
    }
    //-->
</script>
<h2><span><?php echo($form->form_name); ?> : </span></h2>
<form method="post" id="main_frm"
      action="<?php echo base_url('/admin/client_forms/go_to_question_lietener/' . $form->form_id . '/' . $question->question_id . '/' . $previous_category_id); ?>">
    <input type="hidden" name="sequence" value="<?php echo $sequence ?>">
    <input type="hidden" name="action" value="">
    <table class="table"><?php
if ($question->answer_kind == 'text') {
   ?>


        <tr>
            <td width="30%" style="vertical-align: text-top;" valign="top"> <?php echo substr($question->question, 3, count($question->question) - 5) ?> <?php echo $question->required == 1 ? "<span style='color:red;'>*</span>" : ""; ?> </td>
            <td><input name="<?php echo $question->question_id, '_'; ?>"
                       type="text" class="input"
                           <?php echo $question->required == 1 ? "" : ""; ?> value=""> <!--  help text begins -->
                    <span title="<?php echo htmlentities($question->help_text); ?>"> <i
                            class="icon-question-sign"></i>
                    </span> <!--  help text ends --></td>



                <td>
                    <?php echo form_error($question->question_id . "_"); ?>
                </td>

            </tr>
            <?php
        } else if ($question->answer_kind == 'textarea') {
            ?>
            <tr>
                <td style="vertical-align: text-top;" valign="top"> <?php echo substr($question->question, 3, count($question->question) - 5) ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?> </td>
                <td><textarea name="<?php echo $question->question_id . "_"; ?>"
    <?php echo $question->required == 1 ? "" : ""; ?>><?php
    /*
     * $user_ans = get_answer_given_by_policy_holder(); if (isset($user_ans)) {
     * echo htmlentities($user_ans->answer_text); }
     */

    echo set_value($question->question_id . "_");
    ?></textarea> <!--  help text begins --> <span
                        title="<?php echo htmlentities($question->help_text); ?>"> <i
                            class="icon-question-sign"></i>
                    </span> <!--  help text ends --> </span></td>

                <td>
    <?php echo form_error($question->question_id . "_"); ?>
                </td>
            </tr>
                    <?php
                } else if ($question->answer_kind == 'radio') {
                    ?>
            <tr>
                <td style="vertical-align: text-top;" valign="top"> <?php echo htmlentities(substr($question->question, 3, count($question->question) - 5)) ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?> </td>
                <td>
            <?php
            // get the options and interate thru them
            $this->load->model('forms_categories_question_model');
            $answers = $this->forms_categories_question_answer_model->get_where(
                    array('question_id' => $question->question_id
                    ));
            foreach ($answers as $answer) {
                ?>
                        <label class="radio"><input
                                name="<?php echo $question->question_id . "_"; ?>" type="radio"
                                value="<?php echo $answer->answer_id; //->answer_id; ?>"
                        <?php echo (set_value($question->question_id . "_") == $answer->answer_id) ? 'checked="checked"' : ''; ?>
                                class="input" <?php echo $question->required == 1 ? "" : ""; ?> /><?php echo $answer->answer; ?></label> &nbsp; &nbsp;
                        <?php
                    }
                    ?>
                    <!--  help text begins -->
                    <span title="<?php echo htmlentities($question->help_text); ?>"> <i
                            class="icon-question-sign"></i>
                    </span> <!--  help text ends -->

                </td>
                <td>
    <?php echo form_error($question->question_id . "_"); ?>
                </td>
            </tr>
    <?php
} else if ($question->answer_kind == 'checkbox') {
    ?>
            <tr>
                <td style="vertical-align: text-top;" valign="top"> <?php echo substr($question->question, 3, count($question->question) - 5) ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?> </td>
                <td>
            <?php
            $count_check = 0;

            // get the options and interate thru them
            $this->load->model('forms_categories_question_answer_model');
            $forms_categories_question_answer_model = new forms_categories_question_answer_model();
            $answers = $forms_categories_question_answer_model->get_where(
                    array('question_id' => $question->question_id
                    ));

            foreach ($answers as $answer) {
                $count_check++;
                ?>
                        <label $previous_category_id
                            class="checkbox checkbox-align"
                            style="display: block; float: left; margin-right: 15px;"> <input
                                name="<?php echo $question->question_id, '_[]'; ?>"
                                id="<?php echo $question->question_id, '_', $count_check; ?>"
                                type="checkbox" value="<?php echo $answer->answer_id; ?>"
                        <?php echo $question->required == 1 ? " " : ""; ?> class="input"> <?php echo $answer->answer; ?> </input>
                        </label> &nbsp;
                        <?php
                    }
                    ?>
                    <input type="hidden"
                           value="<?php echo $count_check; ?>"
                           name='<?php echo $question->question_id, '_', 'count' ?>'> <!--  help text begins -->

                    <span class="icon-question-sign"
                          style="display: block; float: left; margin-right: 15px;"
                          title="<?php echo htmlentities($question->help_text); ?>"></span> <!--  help text ends -->


                </td>
                <td>
    <?php echo form_error($question->question_id . "_"); ?>
                </td>
            </tr><?php
} else {
    echo "<tr><td>Invalid question type</td></tr>";
}
?>
    </table>
    <table class="pull-right" style="background-color: white;">
        <tr>
            <td >

                <input type="button" class="btn btn-large btn-primary" onclick="go_back()" value="Back" />
                <input class="btn btn-large btn-primary" type="submit" value="Next" />
            </td>

        </tr>
    </table>
</form>

