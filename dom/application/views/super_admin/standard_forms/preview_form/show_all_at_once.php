<?php if (isset($show_data) && $show_data == 1) { ?>
    <script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery-1.8.3.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>bootstrap-tooltip.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery-ui.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery.validate.js"></script>

    <style>
        label { display: inline-block; }
        .multiform-margin { margin-right: 20px; }
        form { margin: 0px; }
    </style>
<?php } ?>
<script>
    jQuery(document).ready(function(){
<?php if ($show_end_data == TRUE) { ?>
            jQuery("#display_back").hide();
<?php } ?>
        jQuery("#main_frm").validate({
            errorElement:'span',
            errorPlacement: function(error, element) {
                if ($(element).attr('type') == 'checkbox' || $(element).attr('type') == 'radio') {
                    $(element).parents('.q_parent').siblings('.q_error').html(error);
                } else {
                    error.insertAfter(element);
                }
            }
        });
        jQuery( document ).tooltip();
        jQuery(".datepicker").datepicker({  changeYear: true,changeMonth: false,yearRange:'-90:+20',dateFormat: "dd/mm/yy" }).val();
    });
</script>

<!-- actual page content begins -->
<?php if (isset($show_data) && $show_data == 1) { ?>
    <form id='main_frm' method="post" action="#">
        <h2><span><?php echo $form->form_name; ?></span></h2>
        <div class="que_box">
            <p><?php echo $form->introduction_text; ?></p>
        </div>
        <input type="hidden" name="action" value="">
        <input type="hidden" name="last_answer_id" id="last_answer_id" value="">
    <?php } ?>
    <div class="que_box">
        <?php
        $i = 0;
        if (isset($category_info)) {
            foreach ($category_info as $cat_info) {
                $i++;
                ?>

                <?php
                if (isset($prev_cat_id) && $prev_cat_id != '') {
                    if ($prev_cat_id != $cat_info->cat_id) {
                        ?>
                        <h3><?php echo $cat_info->cat_name; ?></h3>
                        <p>
                            <?php
                            echo $cat_info->introduction_text;
                            ?> 
                        </p>
                        <?php
                    }
                } else {
                    ?>
                    <h3><?php echo $cat_info->cat_name; ?></h3>
                    <p>
                        <?php
                        echo $cat_info->introduction_text;
                        ?> 
                    </p>
                    <?php
                }
                ?>
                <br/>
                <?php
                if (isset($questions)) {
                    if (is_array($questions) && count($questions) > 0) {
                        foreach ($questions as $question) {
                            if ($cat_info->cat_id == $question->cat_id) {
                                ?>
                                <?php if ($question->answer_kind == 'text') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span  <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>  </span>
                                            </h4>
                                        </div>
                                        <div class="que_bg_right">
                                            <input  name="<?php echo $question->question_id, '_'; ?>" type="text"  class="que_textfiled" <?php echo $question->required == 1 ? "required" : ""; ?> value="<?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                    ?>">
                                            <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($question->answer_kind == 'date') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span
                                                    <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                                </span>
                                            </h4>

                                        </div>
                                        <div class="que_bg_right">
                                            <input
                                                name="<?php echo $question->question_id, '_'; ?>" type="text"
                                                class="que_textfiled datepicker" <?php echo $question->required == 1 ? "required" : ""; ?>
                                                value="<?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                                    ?>">
                                            <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($question->answer_kind == 'number') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span
                                                    <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                                </span> 
                                            </h4>
                                        </div>
                                        <div class="que_bg_right">
                                            <input
                                                name="<?php echo $question->question_id, '_'; ?>" type="text"
                                                class="que_textfiled number" <?php echo $question->required == 1 ? "required" : ""; ?>
                                                value="<?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo $user_ans->answer_text;
                                }
                            }
                                                    ?>">
                                            <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($question->answer_kind == 'textarea') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span
                                                    <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                                </span> 
                                            </h4>
                                        </div>
                                        <div class="que_bg_right">
                                            <textarea class="que_textarea"
                                                      name="<?php echo $question->question_id . "_"; ?>" onkeyup="textAreaAdjust(this)"
                                                      <?php echo $question->required == 1 ? "required" : ""; ?>><?php
                            if ($this->input->post($question->question_id . '_') != '') {
                                echo $this->input->post($question->question_id . '_');
                            } else {
                                $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                                if (isset($user_ans)) {
                                    echo htmlentities($user_ans->answer_text);
                                }
                            }
                                                      ?></textarea>
                                            <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($question->answer_kind == 'radio') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span
                                                    <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                                </span> 
                                            </h4>
                                        </div>
                                        <div class="que_bg_right">
                                            <?php
                                            // get the options and interate thru them
                                            $this->load->model(
                                                    'standard_forms_categories_question_answer_model');
                                            $answers = $this->standard_forms_categories_question_answer_model->get_where(array('question_id' => $question->question_id));
                                            $skip_question = false;
                                            foreach ($answers as $answer) {
                                                if ($answer->skip_to_questions != NULL) {
                                                    $skip_question = true;
                                                    break;
                                                }
                                            }

                                            $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                                            ?>
                                            <div class="span12 form-blank-span">&nbsp;</div>
                                            <?php
                                            foreach ($answers as $answer) {
                                                if (isset($skip_question) && $skip_question == true) {
                                                    ?>
                                                    <div class="span5 q_parent">
                                                        <div class="radio_bg">
                                                            <input name="<?php echo (isset($question->question_id) ? $question->question_id : '') . "_"; ?>" type="radio" <?php
                                    if ($this->input->post($question->question_id . '_') == $answer->answer_id) {
                                        echo "checked='checked'";
                                    } elseif (isset($user_ans)) {
                                        if ($user_ans->answer_text == $answer->answer_id) {
                                            echo "checked='checked'";
                                        }
                                    }
                                                    ?> value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>" <?php echo $question->required == 1 ? "required" : ""; ?> onclick="checkselection(this)" />
                                                                   <?php $last_question_name = $question->question_id . '_'; ?>
                                                            <span><?php echo $answer->answer; ?></span>

                                                        </div>
                                                    </div>

                                                    <?php
                                                } else {
                                                    ?>
                                                    <div class="span5 q_parent">
                                                        <div class="radio_bg">
                                                            <input name="<?php echo (isset($question->question_id) ? $question->question_id : '') . "_"; ?>" type="radio" <?php
                                    if ($this->input->post($question->question_id . '_') == $answer->answer_id) {
                                        echo "checked='checked'";
                                    } elseif (isset($user_ans)) {
                                        if ($user_ans->answer_text == $answer->answer_id) {
                                            echo "checked='checked'";
                                        }
                                    }
                                                    ?> value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>" <?php echo $question->required == 1 ? "required" : ""; ?>  />
                                                            <span><?php echo $answer->answer; ?></span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <span class="q_error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($question->answer_kind == 'checkbox') { ?>
                                    <div class="que_bg">
                                        <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                            <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                                <span
                                                    <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                                </span> 
                                            </h4>

                                        </div>
                                        <div class="que_bg_right">
                                            <?php
                                            $count_check = 0;

                                            // get the options and interate thru them
                                            $this->load->model(
                                                    'standard_forms_categories_question_answer_model');
                                            $standard_forms_categories_question_answer_model = new standard_forms_categories_question_answer_model();
                                            $answers = $standard_forms_categories_question_answer_model->get_where(
                                                    array(
                                                        'question_id' => $question->question_id
                                                    ));
                                            $user_ans = get_answer_given_by_admin(
                                                    $claim_id, $question->question_id);

                                            $this->load->model(
                                                    'temp_forms_answers_details_model');
                                            $obj_form_answer = new temp_forms_answers_details_model();
                                            if (isset($user_ans)) {
                                                $re_ans = $obj_form_answer->get_where(
                                                        array(
                                                            'user_answer_id' => $user_ans->answer_id
                                                        ));
                                            }
                                            ?>
                                            <div class="span12 form-blank-span">&nbsp;</div>
                                            <?php
                                            foreach ($answers as $answer) {
                                                $count_check++;
                                                ?>
                                                <div class="span5 q_parent">
                                                    <div class="radio_bg">
                                                        <input  name="<?php echo $question->question_id, '_[]'; ?>"
                                                        <?php
                                                        if (isset($re_ans)) {
                                                            foreach ($re_ans as $r_ans) {
                                                                if ($r_ans->answer_id == $answer->answer_id) {
                                                                    echo "checked='checked'";
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                                id="<?php echo $question->question_id, '_', $count_check; ?>"
                                                                type="checkbox" <?php echo set_checkbox($question->question_id . '_', $answer->answer_id); ?> value="<?php echo $answer->answer_id; ?>"
                                                                <?php echo $question->required == 1 ? "required" : ""; ?>>
                                                        <span><?php echo $answer->answer; ?></span>
                                                        <input type="hidden"
                                                               value="<?php echo $count_check; ?>"
                                                               name='<?php echo $question->question_id, '_', 'count' ?>'>
                                                    </div>
                                                </div>

                                            <?php } ?>
                                            <span class="q_error"><?php echo form_error($question->question_id . "_"); ?></span>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                            }
                        }
                    }
                }
                ?>
                <?php if ($i != count($category_info)) { ?>
                    <hr class="border">

                    <?php
                }
            }
        }
        ?>  
    </div>
    <div id="email_data"></div>
    <?php if ($show_end_data == TRUE) { ?>
        <div class="que_box">
            <img class="exp_img"  src='<?php echo assets_url_img . "exclamation.png"; ?>' alt='exclamation' title='exclamation'>
            <p><?php echo $form->closure_text; ?></p>
        </div>
        <div class="que_box">
            <table style="background: #fff;">
                <?php
                $this->load->model('claims_files_model');
                $files = claims_files_model::get_claims_for_claim_id($claim_id);

                if (count($files) == 0) {
                    echo "<tr><td colspan=\"3\"><b>Bijlagen</b></td></tr>";
                } else {
                    echo "<tr><td colspan=\"3\"><b>Bijlagen</b><br/>";
                    foreach ($files as $file) {
                        echo anchor($file->get_path_for_web(), $file->file_name, 'target="_blank"');
                        echo '&nbsp;&nbsp;&nbsp;';
                        echo anchor(
                                base_url() . 'user/answer/delete_attachment/' . $claim_id .
                                '/' . $form->form_id . '/' . $file->file_id, '<i class="icon-trash"></i>');
                        echo "<br>";
                    }
                    echo "</td></tr>";
                }
                ?>
                <tr>
                    <td colspan="3">
                        <form method="post" action="<?php echo base_url() . "user/answer/upload_attachment/" . $claim_id; ?>"  enctype="multipart/form-data">
                            <input type="hidden" disabled="disabled" name="form_id"   value="<?php echo $form->form_id; ?>" /> <input type="file"   disabled="disabled" class="input" name="attachment" /> <input  type="submit" disabled="disabled" value="Toevoegen" class="btn btn-primary">
                        </form>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <label class="checkbox">
                            <input type="checkbox" value="yes" name="filled_in_honestly" id="filled_in_honestly"> Naar waarheid ingevuld of gecontroleerd. <font color="red">*</font> 
                            <span id="error-text" style="color: red;"></span><br>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="row-fluid">
                            <div class="btn_form"> 
                                <a href="<?php echo base_url() . 'super_admin/standard_forms'; ?>"
                                   class="btn btn-primary">Terug</a>
                            </div>
                            <!-- link for Opslaan button -->
                            <div class="btn_form"> 
                                <input type="button" value="Opslaan in concept" onclick="save_form()" class="btn btn-primary" />
                            </div>
                            <!-- link for Opslaan button -->
                            <div class="btn_form"> 
                                <input type="button" value="Bevestig en verstuur" onclick="confirm_and_send()" class="btn btn-primary" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <?php
    } else {
        if (isset($show_data) && $show_data == 1) {
            ?>
    <div id="display_back">
                <div class="btn_form">  <a href="<?php echo base_url() . 'super_admin/standard_forms'; ?>"
                                           class="btn btn-primary">Terug</a></div>
                <div class="btn_form">  <input type="button" value="volgende" onclick="opslaan_form()" class="btn btn-primary" /></div>
            </div>
            <?php
        }
    }
    ?>
    <?php if (isset($show_data) && $show_data == 1) { ?>
    </form>
<?php } ?>
<script type="text/javascript">
<?php if ($show_end_data == TRUE) { ?>
        function check_confirm()
        {  
            if ( document.getElementById('filled_in_honestly').checked ==false) 
            {
                document.getElementById('error-text').innerHTML = 'Dit veld is verplicht';
                return false ;
            }
            else
            {
                return true;
            } 
        }
        function save_form()
        {
            document.forms['main_frm'].action.value = 'Opslaan in concept';
            if(check_confirm()==false)
            {
                return false;
            }		
            else
            {
                if (jQuery('#main_frm').validate().form()){
                    jQuery('#main_frm').attr('action', '<?php echo base_url() . 'super_admin/standard_forms/wholeFormSaveActionAtOnce/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence; ?>'); //this fails silently
                    jQuery('#main_frm').get(0).setAttribute('action', '<?php echo base_url() . 'super_admin/standard_forms/wholeFormSaveActionAtOnce/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence; ?>'); //this works
                    document.getElementById("main_frm").submit();
                }
            }
        }

        function confirm_and_send()
        {  
            if(check_confirm()==false)
            {
                return false;
            }		
            else
            {
                document.forms['main_frm'].action.value = 'confirm';                                                                      
                if (jQuery('#main_frm').validate().form()){
                    $('#main_frm').attr('action', '<?php echo base_url() . 'super_admin/standard_forms/wholeFormSaveActionAtOnce/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence; ?>'); //this fails silently
                    $('#main_frm').get(0).setAttribute('action', '<?php echo base_url() . 'super_admin/standard_forms/wholeFormSaveActionAtOnce/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence; ?>'); //this works
                    document.getElementById("main_frm").submit();
                }
            }
        }
<?php } ?>
    function checkselection(ele){
        var answer_id=ele.value;
        jQuery('#last_answer_id').val(answer_id);
        if (jQuery('#main_frm').validate().form()){
            var searzlie = jQuery('#main_frm').serialize();
            jQuery.ajax({
                url : "<?php echo base_url() . 'super_admin/standard_forms/get_smart_questions/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence ?>", 
                type: "post",
                data: searzlie,
                success: function(data){
                    jQuery('#email_data').append(data);
                    // console.log(data);
                    //jQuery('#email_data').html(data);
                }
            });      
        }
    }
    function opslaan_form(){
        var question_name='<?php echo @$last_question_name; ?>';
        var answer_id=$('[name='+question_name+']:checked').val();
        jQuery('#last_answer_id').val(answer_id);
        if (jQuery('#main_frm').validate().form()){
            if(answer_id!==undefined){
                var searzlie = jQuery('#main_frm').serialize();
                jQuery.ajax({
                    url : "<?php echo base_url() . 'super_admin/standard_forms/get_smart_questions/' . $claim_id . "/" . $form->form_id . '/' . $category_id . '/' . $sequence ?>", 
                    type: "post",
                    data: searzlie,
                    success: function(data){
                        jQuery('#email_data').append(data);
                        // console.log(data);
                        //jQuery('#email_data').html(data);
                    }
                }); 
            }else{
                alert('Please fill the last question first.');
            }
        }
    }
</script>
<script>
    function textAreaAdjust(o) {
        o.style.height = "1px";
        o.style.height = (25+o.scrollHeight)+"px";
    }
</script>
