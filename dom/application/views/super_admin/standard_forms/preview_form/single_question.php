<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery.validate.js"></script>
<script>
    function textAreaAdjust(o) {
        o.style.height = "1px";
        o.style.height = (25+o.scrollHeight)+"px";
    }
</script>
<script>
    $(function() {
        $( document ).tooltip();
    });
    $(function() {
        jQuery(".datepicker").datepicker({  changeYear: true,changeMonth: false,yearRange:'-90:+20',dateFormat: "dd/mm/yy" }).val();
    });

    $(document).ready(function() {
        $("#single_forms").validate({
            errorElement:'span',
            errorPlacement: function(error, element) {
                if ($(element).attr('type') == 'checkbox' || $(element).attr('type') == 'radio') {
                   $(element).parents('.q_parent').siblings('.q_error').html(error);
                } else {
                    error.insertAfter(element);
                }
            }
        });    
    });
</script>
<h2><span><?php echo $form->form_name; ?></span></h2>
<div>
    <h3><?php echo @$total_form_completed; ?>% form compleet</h3>
    <div class="progress progress-striped active">
        <div class="bar" style="width: <?php echo @$total_form_completed . '%'; ?>;"></div>
    </div>
</div>
<form id="single_forms" method="post" action="<?php echo base_url() . 'super_admin/standard_forms/PreviewQuestionListener/' . $claim_id . '/' . $form->form_id . '/' . $category_details->cat_id . '/' . $question_details->question_id; ?>">
    <input type="hidden" value="<?php echo $category_details->cat_id; ?>" name="old_category_id">

    <div class="que_box">
        <h3><?php echo @$category_details->cat_name; ?></h3>
        <p>
            <?php echo @$category_details->introduction_text; ?>  
        </p>
        <br/>
        <div class="que_bg">
            <div class="que_bg_left">
                <h4><?php echo modifyText($question_details->question); ?> <?php echo $question_details->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                    <span  <?php if ($question_details->help_text != Null) { ?>  title="<?php echo htmlentities($question_details->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>  </span>
                </h4>
            </div>
            <div class="que_bg_right">
                <?php if (isset($answer_text)) { ?>
                    <input name="<?php echo $question_details->question_id, '_'; ?>"  type="text" class="que_textfiled"  <?php echo $question_details->required == 1 ? "required='required'" : ""; ?>  value="<?php
                $user_ans = get_answer_given_by_admin(
                        $claim_id, $question_details->question_id);
                if (isset($user_ans)) {
                    echo $user_ans->answer_text;
                }
                    ?>">
                    <span class="error"> <?php echo form_error($question_details->question_id . "_"); ?></span>
                <?php } ?>
                <?php if (isset($answer_date)) { ?>
                    <input name="<?php echo $question_details->question_id, '_'; ?>"
                           type="text" class="que_textfiled datepicker"
                           <?php echo $question_details->required == 1 ? "required='required'" : ""; ?>
                           value="<?php
                       $user_ans = get_answer_given_by_admin(
                               $claim_id, $question_details->question_id);

                       if (isset($user_ans)) {
                           echo $user_ans->answer_text;
                       }
                           ?>">
                    <span class="error"> <?php echo form_error($question_details->question_id . "_"); ?></span>
                <?php } ?>
                <?php if (isset($answer_number)) { ?>
                    <input name="<?php echo $question_details->question_id, '_'; ?>"
                           type="text" class="que_textfiled"
                           <?php echo $question_details->required == 1 ? "required='required'" : ""; ?>
                           value="<?php
                       $user_ans = get_answer_given_by_admin(
                               $claim_id, $question_details->question_id);

                       if (isset($user_ans)) {
                           echo $user_ans->answer_text;
                       }
                           ?>">
                    <span class="error"> <?php echo form_error($question_details->question_id . "_"); ?></span>
                <?php } ?>
                <?php if (isset($answer_textarea)) { ?>
                    <textarea class="que_textarea" name="<?php echo $question_details->question_id . "_"; ?>" onkeyup="textAreaAdjust(this)"
                              <?php echo $question_details->required == 1 ? "required='required'" : ""; ?>><?php
                          $user_ans =
                                  get_answer_given_by_admin($claim_id, $question_details->question_id);
                          if
                          (isset($user_ans)) {
                              echo htmlentities($user_ans->answer_text);
                          }
                              ?></textarea>
                    <span class="error"> <?php echo form_error($question_details->question_id . "_"); ?></span>
                <?php } ?>
                <?php
                if (isset($answer_radio)) {
                    $this->load->model('standard_forms_categories_question_answer_model');
                    $answers = $this->standard_forms_categories_question_answer_model->get_where(array('question_id' => $question_details->question_id));
                    $user_ans = get_answer_given_by_admin($claim_id, $question_details->question_id);
                    ?>
                    <div class="span12 form-blank-span">&nbsp;</div>
                    <?php
                    foreach ($answers as $answer) {
                        ?>
                        <div class="span5 q_parent">
                            <div class="radio_bg">
                                <input name="<?php echo (isset($question_details->question_id) ? $question_details->question_id : '') . "_"; ?>" type="radio"   <?php
                if (isset($user_ans)) {
                    if ($user_ans->answer_text == $answer->answer_id) {
                        echo "checked='checked'";
                    }
                }
                        ?>
                                       value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>"
                                       <?php echo $question_details->required == 1 ? "required='required'" : ""; ?>>

                                <span><?php echo $answer->answer; ?></span>
                            </div>
                        </div>

                    <?php } ?>
                    <span class="q_error"> <?php echo form_error($question_details->question_id . "_"); ?></span>
                <?php } ?>
                <?php
                if (isset($answer_check_box)) {
                    $count_check = 0;
                    $this->load->model('standard_forms_categories_question_answer_model');
                    $standard_forms_categories_question_answer_model = new standard_forms_categories_question_answer_model();
                    $answers = $standard_forms_categories_question_answer_model->get_where(
                            array(
                                'question_id' => $question_details->question_id
                            ));
                    $user_ans = get_answer_given_by_admin(
                            $claim_id, $question_details->question_id);

                    $this->load->model('temp_forms_answers_details_model');
                    $obj_form_answer = new temp_forms_answers_details_model();
                    if (isset($user_ans)) {
                        $re_ans = $obj_form_answer->get_where(array('user_answer_id' => $user_ans->answer_id));
                    }
                    ?>
                    <div class="span12 form-blank-span">&nbsp;</div>
                    <?php
                    foreach ($answers as $answer) {
                        $count_check++;
                        ?>
                        <div class="span5 q_parent">
                            <div class="radio_bg">
                                <input
                                    name="<?php echo $question_details->question_id, '_[]'; ?>"
                                    <?php
                                    if (isset($re_ans)) {
                                        foreach ($re_ans as $r_ans) {
                                            if ($r_ans->answer_id == $answer->answer_id) {
                                                echo "checked='checked'";
                                            }
                                        }
                                    }
                                    ?>
                                    id="<?php echo $question_details->question_id, '_', $count_check; ?>"
                                    type="checkbox" value="<?php echo $answer->answer_id; ?>"
                                    <?php echo $question_details->required == 1 ? "required='required'" : ""; ?>>

                                <span>
                                    <?php echo $answer->answer; ?>
                                </span>

                            </div>
                        </div>
                    <?php } ?>
                    <input type="hidden" value="<?php echo $count_check; ?>" name='<?php echo $question_details->question_id, '_', 'count' ?>'> 
                    <span class="q_error"> <?php echo form_error($question_details->question_id . "_"); ?></span>
                <?php } ?>
            </div>

        </div>
    </div>

    <?php if (isset($prev_url)) {
        ?>

        <div class="btn_form">
            <a href="<?php echo str_replace('PreviewQuestionListener', 'preview_question', $prev_url); ?>" class="btn btn-primary">Terug</a>
        </div>
        <?php
    } else {
        if (isset($prev_question) && $prev_question['prev_cat']->cat_id != 'end' && $prev_question['prev_question']->question_id != 'end') {
            ?>
            <div class="btn_form">
                <a href="<?php echo base_url() . 'super_admin/standard_forms/preview_question/' . $claim_id . '/' . $form->form_id . '/' . $prev_question['prev_cat']->cat_id . '/' . $prev_question['prev_question']->question_id; ?>" class="btn btn-primary">Terug</a></div>
        <?php } else { ?>
            <div class="btn_form">
                <a href="<?php echo base_url() . 'super_admin/standard_forms/preview_question_start/' . $claim_id . '/' . $form->form_id; ?>" class="btn btn-primary">Terug</a>
            </div>
            <?php
        }
    } if (isset($next_question)) {
        ?>
        <div class="btn_form">
            <input type="submit" class="btn btn-primary" value="Volgende" />
        </div>
    <?php } ?>
</form>
