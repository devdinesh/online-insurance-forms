<h2><span><?php echo $form->form_name; ?></span></h2>
<div class="">
    <h3><?php echo @$total_form_completed; ?>% form compleet</h3>
    <div class="progress progress-striped active">
        <div class="bar" style="width: <?php echo @$total_form_completed . '%'; ?>;"></div>
    </div>
</div>
<div class="que_box">
    <p><?php echo $form->introduction_text; ?></p>
</div>
<div class="btn_form"><a href="<?php echo base_url() . 'super_admin/standard_forms'; ?>" class="btn btn-primary">Terug</a></div>
<div class="btn_form">  
    <?php if (isset($next_question)) { ?>
        <a href="<?php echo base_url() . 'super_admin/standard_forms/preview_question/' . $claim_id . '/' . $form->form_id . '/' . $next_question['next_cat']->cat_id . '/' . $next_question['next_question']->question_id; ?>" class="btn btn-primary">Volgende</a>
    <?php } ?>
</div>



