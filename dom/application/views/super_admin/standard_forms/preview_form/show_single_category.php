<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery.validate.js"></script>
<script>
    $(function() {
        $( document ).tooltip();
    });
    $(function() {
        jQuery(".datepicker").datepicker({  changeYear: true,changeMonth: false,yearRange:'-90:+20',dateFormat: "dd/mm/yy" }).val();
    });

    $(document).ready(function() {
        $("#single_forms").validate({
            errorElement:'span',
            errorPlacement: function(error, element) {
                if ($(element).attr('type') == 'checkbox' || $(element).attr('type') == 'radio') {
                    $(element).parents('.q_parent').siblings('.q_error').html(error);
                } else {
                    error.insertAfter(element);
                }
            }
        });
    });

</script>
<script>
    function textAreaAdjust(o) {
        o.style.height = "1px";
        o.style.height = (25+o.scrollHeight)+"px";
    }
</script>
<h2><span><?php echo $form->form_name; ?></span></h2>
<div>
    <h3><?php echo @$total_form_completed; ?>% form compleet</h3>
    <div class="progress progress-striped active">
        <div class="bar" style="width: <?php echo @$total_form_completed . '%'; ?>;"></div>
    </div>
</div>
<form id="single_forms" method="post" action="<?php
if (isset($category) && $category->cat_id != '') {
    if (isset($smart_sequence) && $smart_sequence != null) {
        echo base_url() . "super_admin/standard_forms/PreviewCategoryListener/" .
        $claim_id . "/" . $form->form_id . "/" . $category->cat_id . "/" .
        $smart_sequence;
    } else {
        echo base_url() . "super_admin/standard_forms/PreviewCategoryListener/" .
        $claim_id . "/" . $form->form_id . "/" . $category->cat_id;
    }
} else {
    echo base_url() . "super_admin/standard_forms/PreviewCategoryListener/" . $claim_id .
    "/" . $form->form_id . "/end";
}
?> ">
    <div class="que_box">
        <?php
        if (isset($category)) {
            if (isset($smart_sequence) && $smart_sequence != null) {
                $this->load->model('standard_forms_categories_question_model');
                $obj_ques = new standard_forms_categories_question_model();
                $get_ques = $obj_ques->get_where(
                        array('cat_id' => $category->cat_id,
                            'sequence >' => $smart_sequence
                        ));

                if (count($get_ques) > 0) {
                    $questions = $category->get_question_if_smart(
                            $smart_sequence, false);
                } else {
                    $questions = $category->get_question_if_smart(
                            $smart_sequence, true);
                }
            } else {
                $questions = $category->get_question_smart();
            }
            ?>
            <h3><?php echo $category->cat_name; ?></h3>
            <p>
                <?php echo $category->introduction_text; ?> 
            </p>
            <br/>
            <?php
            if (isset($questions)) {
                if (is_array($questions) && count($questions) > 0) {
                    foreach ($questions as $question) {
                        ?>
                        <?php if ($question->answer_kind == 'text') { ?>
                            <div class="que_bg">
                                <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                    <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                        <span  <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>  </span>
                                    </h4>
                                </div>
                                <div class="que_bg_right">
                                    <input  name="<?php echo $question->question_id, '_'; ?>" type="text"  class="que_textfiled" <?php echo $question->required == 1 ? "required" : ""; ?> value="<?php
                    if ($this->input->post($question->question_id . '_') != '') {
                        echo $this->input->post($question->question_id . '_');
                    } else {
                        $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                        if (isset($user_ans)) {
                            echo $user_ans->answer_text;
                        }
                    }
                            ?>">
                                    <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($question->answer_kind == 'date') { ?>
                            <div class="que_bg">
                                <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                    <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                        <span
                                            <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                        </span>
                                    </h4>

                                </div>
                                <div class="que_bg_right">
                                    <input
                                        name="<?php echo $question->question_id, '_'; ?>" type="text"
                                        class="que_textfiled datepicker" <?php echo $question->required == 1 ? "required" : ""; ?>
                                        value="<?php
                        if ($this->input->post($question->question_id . '_') != '') {
                            echo $this->input->post($question->question_id . '_');
                        } else {
                            $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                            if (isset($user_ans)) {
                                echo $user_ans->answer_text;
                            }
                        }
                                            ?>">
                                    <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($question->answer_kind == 'number') { ?>
                            <div class="que_bg">
                                <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                    <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                        <span
                                            <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                        </span> 
                                    </h4>
                                </div>
                                <div class="que_bg_right">
                                    <input
                                        name="<?php echo $question->question_id, '_'; ?>" type="text"
                                        class="que_textfiled number" <?php echo $question->required == 1 ? "required" : ""; ?>
                                        value="<?php
                        if ($this->input->post($question->question_id . '_') != '') {
                            echo $this->input->post($question->question_id . '_');
                        } else {
                            $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                            if (isset($user_ans)) {
                                echo $user_ans->answer_text;
                            }
                        }
                                            ?>">
                                    <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($question->answer_kind == 'textarea') { ?>
                            <div class="que_bg">
                                <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                    <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                        <span
                                            <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                        </span> 
                                    </h4>
                                </div>
                                <div class="que_bg_right">
                                    <textarea class="que_textarea"
                                              name="<?php echo $question->question_id . "_"; ?>" onkeyup="textAreaAdjust(this)"
                                              <?php echo $question->required == 1 ? "required" : ""; ?>><?php
                          if ($this->input->post($question->question_id . '_') != '') {
                              echo $this->input->post($question->question_id . '_');
                          } else {
                              $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                              if (isset($user_ans)) {
                                  echo htmlentities($user_ans->answer_text);
                              }
                          }
                                              ?></textarea>
                                    <span class="error"><?php echo form_error($question->question_id . "_"); ?></span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($question->answer_kind == 'radio') { ?>
                            <div class="que_bg">
                                <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                    <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                        <span
                                            <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                        </span> 
                                    </h4>
                                </div>
                                <div class="que_bg_right">
                                    <?php
                                    // get the options and interate thru them
                                    $this->load->model(
                                            'standard_forms_categories_question_answer_model');
                                    $answers = $this->standard_forms_categories_question_answer_model->get_where(array('question_id' => $question->question_id));
                                    $skip_question = false;
                                    foreach ($answers as $answer) {
                                        if ($answer->skip_to_questions != NULL) {
                                            $skip_question = true;
                                            break;
                                        }
                                    }

                                    $user_ans = get_answer_given_by_admin($claim_id, $question->question_id);
                                    ?>
                                    <div class="span12 form-blank-span">&nbsp;</div>
                                    <?php
                                    foreach ($answers as $answer) {
                                        if (isset($skip_question) && $skip_question == true) {
                                            ?>
                                            <div class="span5 q_parent">
                                                <div class="radio_bg">
                                                    <input name="<?php echo (isset($question->question_id) ? $question->question_id : '') . "_"; ?>" type="radio" <?php
                            if ($this->input->post($question->question_id . '_') == $answer->answer_id) {
                                echo "checked='checked'";
                            } elseif (isset($user_ans)) {
                                if ($user_ans->answer_text == $answer->answer_id) {
                                    echo "checked='checked'";
                                }
                            }
                                            ?> value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>" <?php echo $question->required == 1 ? "required" : ""; ?> onclick="checkselection(this)" />
                                                           <?php $last_question_name = $question->question_id . '_'; ?>
                                                    <span><?php echo $answer->answer; ?></span>

                                                </div>
                                            </div>

                                            <?php
                                        } else {
                                            ?>
                                            <div class="span5 q_parent">
                                                <div class="radio_bg">
                                                    <input name="<?php echo (isset($question->question_id) ? $question->question_id : '') . "_"; ?>" type="radio" <?php
                            if ($this->input->post($question->question_id . '_') == $answer->answer_id) {
                                echo "checked='checked'";
                            } elseif (isset($user_ans)) {
                                if ($user_ans->answer_text == $answer->answer_id) {
                                    echo "checked='checked'";
                                }
                            }
                                            ?> value="<?php echo (isset($answer->answer_id) ? $answer->answer_id : ''); ?>" <?php echo $question->required == 1 ? "required" : ""; ?>  />
                                                    <span><?php echo $answer->answer; ?></span>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <span class="q_error"><?php echo form_error($question->question_id . "_"); ?></span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($question->answer_kind == 'checkbox') { ?>
                            <div class="que_bg">
                                <div class="que_bg_left" id="<?php echo 'view_' . $question->question_id; ?>">
                                    <h4><?php echo modifyText($question->question); ?> <?php echo $question->required == 1 ? "<font color=\"red\">*</font>" : ""; ?>
                                        <span
                                            <?php if ($question->help_text != Null) { ?>  title="<?php echo htmlentities($question->help_text); ?>"> <i  class="icon-question-sign"></i>  <?php } ?>
                                        </span> 
                                    </h4>

                                </div>
                                <div class="que_bg_right">
                                    <?php
                                    $count_check = 0;

                                    // get the options and interate thru them
                                    $this->load->model(
                                            'standard_forms_categories_question_answer_model');
                                    $standard_forms_categories_question_answer_model = new standard_forms_categories_question_answer_model();
                                    $answers = $standard_forms_categories_question_answer_model->get_where(
                                            array(
                                                'question_id' => $question->question_id
                                            ));
                                    $user_ans = get_answer_given_by_admin(
                                            $claim_id, $question->question_id);

                                    $this->load->model(
                                            'temp_forms_answers_details_model');
                                    $obj_form_answer = new temp_forms_answers_details_model();
                                    if (isset($user_ans)) {
                                        $re_ans = $obj_form_answer->get_where(
                                                array(
                                                    'user_answer_id' => $user_ans->answer_id
                                                ));
                                    }
                                    ?>
                                    <div class="span12 form-blank-span">&nbsp;</div>
                                    <?php
                                    foreach ($answers as $answer) {
                                        $count_check++;
                                        ?>
                                        <div class="span5 q_parent">
                                            <div class="radio_bg">
                                                <input  name="<?php echo $question->question_id, '_[]'; ?>"
                                                <?php
                                                if (isset($re_ans)) {
                                                    foreach ($re_ans as $r_ans) {
                                                        if ($r_ans->answer_id == $answer->answer_id) {
                                                            echo "checked='checked'";
                                                        }
                                                    }
                                                }
                                                ?>
                                                        id="<?php echo $question->question_id, '_', $count_check; ?>"
                                                        type="checkbox" <?php echo set_checkbox($question->question_id . '_', $answer->answer_id); ?> value="<?php echo $answer->answer_id; ?>"
                                                        <?php echo $question->required == 1 ? "required" : ""; ?>>
                                                <span><?php echo $answer->answer; ?></span>
                                                <input type="hidden"
                                                       value="<?php echo $count_check; ?>"
                                                       name='<?php echo $question->question_id, '_', 'count' ?>'>
                                            </div>
                                        </div>

                                    <?php } ?>
                                    <span class="q_error"><?php echo form_error($question->question_id . "_"); ?></span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php
                    }
                }
            }
        }
        ?>
    </div>
    <?php
    if (isset($prev_url_smart) && $prev_url_smart != '') {
        $count = count($prev_url_smart);
        $check = array_search(current_url(), $prev_url_smart);

        $prev = ( $check === false ) ? ( $count - 1 ) : ( $check - 1 );
        if ($prev >= 0 && (isset($prev_cat) && $prev_cat != '')) {
            ?>
            <div class="btn_form">
                <a href="<?php echo $prev_url_smart[$prev]; ?>" class="btn btn-primary">Terug</a>
            </div>
            <?php
        } else {
            ?>
            <div class="btn_form">
                <a href="<?php echo base_url() . 'super_admin/standard_forms/preview_category_start/' . $claim_id . '/' . $form->form_id; ?>"
                   class="btn btn-primary">Terug</a>
            </div>

            <?php
        }
    } else {
        ?>
        <div class="btn_form">
            <a  href="<?php echo base_url() . 'super_admin/standard_forms/preview_category_start/' . $claim_id . '/' . $form->form_id; ?>"
                class="btn btn-primary">Terug</a>
        </div>

        <?php
    }
    ?>
    <div class="btn_form">
        <input type="submit" class="btn btn-primary" value="Volgende" />
    </div>
</form>
