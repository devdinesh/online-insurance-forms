<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "super_admin/standard_forms" ?>"><?php echo $this->lang->line('standard_forms'); ?></a></li>
            <li class="active"><?php echo $title; ?></li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">

            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-12">
                <form class="form form-validate" id="editStandardForm" action="<?php echo base_url() . 'super_admin/standard_forms/editListener/' . $form_id; ?>"  method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><?php echo $title; ?></header>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="form_name" value="<?php echo htmlentities(set_value('form_name') !== '' ? set_value('form_name') : isset($form_details->form_name) ? $form_details->form_name : ''); ?>" required/> 
                                        <label for="form_name"> <?php echo $this->lang->line('name'); ?> <span class="required">*</span></label>
                                        <?php echo form_error('form_name'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="form_tag" value="<?php echo htmlentities(set_value('form_tag') !== '' ? set_value('form_tag') : isset($form_details->form_tag) ? $form_details->form_tag : ''); ?>" required/> 
                                        <label for="form_tag"><?php echo $this->lang->line('code'); ?> <span class="required">*</span></label>
                                        <?php echo form_error('form_tag'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="radio-inline radio-styled">
                                        <?php echo $this->lang->line('policyholder_complete_form'); ?>
                                    </label>
                                    <label class="radio-inline radio-styled">
                                        <input type="radio" name="fill_in_needed" value="1"
                                        <?php
                                        if (set_value('fill_in_needed') == '1' || $form_details->fill_in_needed ==
                                                '1') {
                                            echo "checked=\"checked\"";
                                        }
                                        ?>> <?php echo $this->lang->line('yes'); ?>
                                    </label>
                                    <label class="radio-inline radio-styled">
                                        <input type="radio" name="fill_in_needed" value="0"
                                        <?php
                                        if (set_value('fill_in_needed') == '0' || $form_details->fill_in_needed ==
                                                '0') {
                                            echo "checked=\"checked\"";
                                        }
                                        ?>> <?php echo $this->lang->line('no'); ?>
                                    </label>
                                    <?php echo form_error('fill_in_needed'); ?>
                                </div>
                                <div class="col-sm-6">
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" name="select_for_policyholder" value="1"  <?php
                                    if (set_value('select_for_policyholder') == '1' || $form_details->select_for_policyholder == '1') {
                                        echo "checked=\"checked\"";
                                    }
                                    ?>>    <span><?php echo $this->lang->line('self_administered'); ?></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input class="form-control" type="text" name="f_reminder" value="<?php echo htmlentities(set_value('f_reminder') !== '' ? set_value('f_reminder') : isset($form_details->f_reminder) ? $form_details->f_reminder : ''); ?>" data-rule-number="true" /> 
                                                <label for="f_reminder"><?php echo $this->lang->line('first_reminder'); ?></label>
                                                <?php echo form_error('f_reminder'); ?>
                                            </div>
                                            <span class="input-group-addon"><?php echo $this->lang->line('days'); ?></span>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input class="form-control" type="text" name="s_reminder" value="<?php echo htmlentities(set_value('s_reminder') !== '' ? set_value('s_reminder') : isset($form_details->s_reminder) ? $form_details->s_reminder : ''); ?>"  data-rule-number="true"/> 
                                                <label for="s_reminder"><?php echo $this->lang->line('second_reminder'); ?></label>
                                                <?php echo form_error('s_reminder'); ?>
                                            </div>
                                            <span class="input-group-addon"><?php echo $this->lang->line('days'); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="header_text"><?php echo $this->lang->line('header_text'); ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group floating-label">
                                        <textarea name="header_text" id="header_text" class=" tinymce form-control" rows="5" placeholder=""><?php
                                                if (set_value('header_text') != '') {
                                                    $value = set_value('header_text');
                                                } else {
                                                    if (isset($form_details->header_text)) {
                                                        $value = $form_details->header_text;
                                                    } else {
                                                        $value = '';
                                                    }
                                                }
                                                echo htmlentities($value);
                                                ?></textarea>
                                        <?php echo form_error('header_text'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="introduction_text"><?php echo $this->lang->line('introduction_text'); ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group floating-label">
                                        <textarea name="introduction_text" id="introduction_text" class=" tinymce form-control" rows="5" placeholder=""><?php
                                        if (set_value('introduction_text') != '') {
                                            $value = set_value('introduction_text');
                                        } else {
                                            if (isset($form_details->introduction_text)) {
                                                $value = $form_details->introduction_text;
                                            } else {
                                                $value = '';
                                            }
                                        }
                                        echo htmlentities($value);
                                        ?></textarea>
                                        <?php echo form_error('introduction_text'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="closure_text"><?php echo $this->lang->line('closer_text'); ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group floating-label">
                                        <textarea name="closure_text" id="closure_text" class=" tinymce form-control" rows="5" placeholder=""><?php
                                        if (set_value('closure_text') != '') {
                                            $value = set_value('closure_text');
                                        } else {
                                            if (isset($form_details->closure_text)) {
                                                $value = $form_details->closure_text;
                                            } else {
                                                $value = '';
                                            }
                                        }
                                        echo htmlentities($value);
                                        ?></textarea>
                                        <?php echo form_error('closure_text'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="radio-inline radio-styled">
                                        <input type="radio" value="0"  name="show_all_question_at_once" <?php echo $form_details->show_all_question_at_once == '0' ? "checked=''" : "" ?> /> Toon alle  vragen in een keer
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label class="radio-inline radio-styled">
                                        <input type="radio" <?php echo $form_details->show_all_question_at_once == '1' ? "checked=''" : "" ?> value="1" name="show_all_question_at_once" />  Toon vragen een voor een
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label class="radio-inline radio-styled">
                                        <input type="radio" value="2" <?php echo $form_details->show_all_question_at_once == '2' ? "checked=''" : "" ?> name="show_all_question_at_once" /> Toon alle vragen in een keer per categorie
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <a class="btn btn-flat" href="<?php echo base_url() . 'super_admin/standard_forms'; ?>"><?php echo $this->lang->line('cancel');?></a>
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction"><?php echo $this->lang->line('save');?></button>
                            </div>
                        </div>
                    </div> 
                    <em class="text-caption"><?php echo $this->lang->line('requred_text_labels_text');?></em>
                </form>
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>

<script type="text/javascript">
    $( "#editStandardForm" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            password_confirmation: {
                equalTo: "#password"
            },
            logo:{
                extension:"gif|jpg|png"
            },
            pdf_template:{
                extension:"gif|jpg|png"
            }
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#editStandardForm')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#editStandardForm')).hide();
            label.remove();
        }
    });
</script>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',
            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,|,bullist,numlist,|,undo,redo,|,link,unlink,image",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>