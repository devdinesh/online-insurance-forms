<div class="modal-header">
<!--    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
    <h4 class="modal-title" id="formModalLabel"><?php echo $this->lang->line('select_client_copy_form'); ?></h4>
</div>
<form class="form-horizontal form-validate" id="copyFormpopup" method="post" action="<?php echo base_url() . "super_admin/standard_forms/copy_all_form/" . $form_id; ?>">
    <div class="modal-body">
        <div class="form-group">
            <div class="col-sm-2">
                <label for="client_id"><?php echo $this->lang->line('client') ?> <span class="required">*</span></label>
            </div>
            <div class="col-sm-10">
                <select id="client_id" name="client_id" class="form-control" required onchange="copy_all_form();">
                    <option value=""></option>
                    <?php foreach ($client_info as $client) { ?>
                        <option value="<?php echo $client->client_id; ?>" ><?php echo $client->client_name; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <!--        <button type="button" class="btn btn-default" data-dismiss="modal">Terug</button>-->
        <a  class="btn btn-default" href="<?php echo base_url() . 'super_admin/standard_forms'; ?>"> Terug </a>
        <input type="submit" value="Kopieer" class="btn btn-primary" id="button_copy"/>
    </div>
</form>
<script type="text/javascript" charset="utf-8"> 
    $(document).ready(function() {
        $( "#button_copy" ).hide();
    });
    function copy_all_form(){
        var client_id= document.getElementById("client_id");
        var client_value = client_id.value;
        if(client_value!='nl'){
            $( "#button_copy" ).show();
        }else{
            $( "#button_copy" ).hide();
        }
    }
    $( "#copyFormpopup" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            client_id: {
                required: true
            } 
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#copyFormpopup')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#copyFormpopup')).hide();
            label.remove();
        }
    });
</script>


