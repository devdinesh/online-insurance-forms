<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'super_admin/standard_categories/' . $form_details->form_id; ?>"><?php echo $this->lang->line('overview_categories'); ?></a></li>
            <li class="active"><?php echo $title; ?></li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-8">
                <form class="form form-validate" id="addCategory" action="<?php echo base_url() . 'super_admin/standard_categories/addListener/' . $form_details->form_id; ?>"  method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><?php echo $title; ?></header>
                        </div>
                        <div class="card-body">
                            <!--                            <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <input class="form-control" type="text" name="form_name" value="<?php echo $form_details->form_name; ?>" disabled> 
                                                                    <label for="form_name"> <?php echo $this->lang->line('form_name'); ?></label>
                                                                </div>
                                                            </div>
                                                        </div>-->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="cat_name" value="<?php echo htmlentities((set_value('cat_name') != '') ? set_value('cat_name') : '' ); ?>" required/> 
                                        <label for="cat_name"> <?php echo $this->lang->line('category_name'); ?> <span class="required">*</span></label>
                                        <?php echo form_error('cat_name'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="introduction_text"><?php echo $this->lang->line('introduction_text'); ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group floating-label">
                                        <textarea name="introduction_text" id="introduction_text" class=" tinymce form-control" rows="5" placeholder=""><?php echo (set_value('introduction_text')); ?></textarea>
                                        <?php echo form_error('introduction_text'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <a class="btn btn-flat" href="<?php echo base_url() . 'super_admin/standard_categories/' . $form_details->form_id; ?>">Annuleer</a>
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction">Opslaan</button>
                            </div>
                        </div>
                    </div> 
                    <em class="text-caption"><?php echo $this->lang->line('requred_text_labels_text'); ?></em>
                </form>
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>

<script type="text/javascript">
    $( "#addCategory" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#addCategory')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#addCategory')).hide();
            label.remove();
        }
    });
</script>

<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',
            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,|,bullist,numlist,|,undo,redo,|,link,unlink,image",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>