<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary"><?php echo $title; ?></h2>
    </div>
    <?php if ($this->session->flashdata('success') != '') { ?>
        <div class="alert alert-success" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error') != '') { ?>

        <div class="alert alert-danger" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <div class="section-body">
        <!-- BEGIN DATATABLE 1 -->
        <div class="row">
            <div class="col-md-12 margin-bottom-xxl">
                <a href="<?php echo base_url() . 'super_admin/standard_categories/add/' . $form_details->form_id; ?>" class="btn ink-reaction btn-primary" ><?php echo $this->lang->line('add_category'); ?></a>
                <a href="<?php echo base_url() . 'super_admin/standard_forms'; ?>" class="btn ink-reaction btn-default" ><?php echo $this->lang->line('back'); ?></a>
            </div><!--end .col -->
        </div><!--end .row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="tbl_category_list" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="40%"><?php echo $this->lang->line('category_name'); ?></th>
                                <th width="40%"><?php echo $this->lang->line('ask'); ?></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>etc</td>
                                <td>etc</td>
                                <td>etc</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!--end .table-responsive -->
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END DATATABLE 1 -->
    </div><!--end .section-body -->
</section>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#tbl_category_list').dataTable( {
            //  "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "bProcessing": true,  
            "bSort":false,
            'iDisplayLength': 100,    
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"} 
            ],
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": " <?php echo base_url() . 'super_admin/standard_categories/getJson/' . $form_details->form_id; ?>",
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                $(nRow).attr('id', aData[3]);
            }
        } );
        $(function() {
            $("#tbl_category_list tbody").sortable({ opacity: 0.6, cursor: 'move', update: function() {
                    var order = $(this).sortable("serialize");
                    var url = "<?php echo base_url() . 'super_admin/standard_categories/sortable'; ?>";
                    $.post(url, order, function(){
                        window.location.reload();
                    });   
                }                                
            });
        });
    } );
    function deleteRow(ele){
        var current_id = $(ele).attr('id');
        var parent = $(ele).parent().parent();
        $.confirm({
            'title' : 'Digitaal op maat',
            'message'   : "<?php echo $this->lang->line('delete_question'); ?>",
            'buttons'   : {
                'Ja'    : {'class'      : 'btn ink-reaction btn-raised btn-sm btn-success',
                    'action': function(){
                        $.ajax({
                            type : 'POST',
                            url : http_host_js+'super_admin/standard_categories/delete/'+<?php echo $form_details->form_id; ?>+'/'+current_id,
                            data: id =current_id,
                            beforeSend: function() {
                            },
                            success: function() {
                                parent.slideUp(500,function() {
                                    parent.remove();
                                    window.location.reload();
                                });
                            },
                            error : function(XMLHttpRequest, textStatus, errorThrown) {
                                alert('error');
                            }
                        });
                    }
                },
                'Nee'   : {
                    'class'     : 'btn ink-reaction btn-raised btn-sm btn-danger',
                    'action': function(){}      // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });             
        return false;
    }
</script>
