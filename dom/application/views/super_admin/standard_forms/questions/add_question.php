<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'super_admin/standard_categories_question/' . $category_details->cat_id; ?>"><?php echo $this->lang->line('question'); ?></a></li>
            <li class="active"><?php echo $title; ?></li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-8">
                <form class="form form-validate" id="addQuestion" action="<?php echo base_url() . 'super_admin/standard_categories_question/addListener/' . $category_details->cat_id; ?>"  method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><?php echo $title; ?></header>
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="category_id"  value="<?php echo $category_details->cat_id; ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="category" value="<?php echo $category_details->cat_name; ?>" disabled/> 
                                        <label for="category"> <?php echo $this->lang->line('category'); ?> </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="question"><?php echo $this->lang->line('question'); ?> <span class="required">*</span></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group floating-label">
                                        <textarea name="question" id="introduction_text" required class=" tinymce form-control" rows="5"   placeholder="" ><?php echo (set_value('question')); ?></textarea>
                                        <?php echo form_error('question'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="help_text"><?php echo $this->lang->line('help_text'); ?> </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group floating-label">
                                        <textarea name="help_text" id="help_text" class=" form-control"  rows="5"  placeholder=""><?php echo (set_value('help_text')); ?></textarea>
                                        <?php echo form_error('help_text'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" name="required" value="1"  <?php
                                        if (set_value('required') != "" && set_value('required') == '1') {
                                            echo 'checked="checked"';
                                        }
                                        ?>>
                                            <span><?php echo $this->lang->line('is_mandatory'); ?></span>
                                        </label>
                                    </div>
                                </div>  
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="answer_kind"><?php echo $this->lang->line('answer_type'); ?> <span class="required">*</span>  :</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="answer_kind" value="date" onclick="checkselection(this)" <?php
                                                   if (set_value('answer_kind') == 'date') {
                                                       echo "checked=\"checked\"";
                                                   }
                                        ?>> <?php echo $this->lang->line('single_line_date') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="answer_kind" value="number" onclick="checkselection(this)" <?php
                                                   if (set_value('answer_kind') == 'number') {
                                                       echo "checked=\"checked\"";
                                                   }
                                        ?>>
                                                   <?php echo $this->lang->line('single_line_number'); ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="answer_kind" value="text" onclick="checkselection(this)" <?php
                                                   if (set_value('answer_kind') == 'text') {
                                                       echo "checked=\"checked\"";
                                                   }
                                                   ?>><?php echo $this->lang->line('single_line_text'); ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="answer_kind" value="financieel" onclick="checkselection(this)" <?php
                                                   if (set_value('answer_kind') == 'financieel') {
                                                       echo "checked=\"checked\"";
                                                   }
                                                   ?>>
                                                   <?php echo $this->lang->line('financial'); ?>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="answer_kind"
                                                   value="textarea" onclick="checkselection(this)"
                                                   <?php
                                                   if (set_value('answer_kind') == 'textarea') {
                                                       echo "checked=\"checked\"";
                                                   }
                                                   ?>>
                                                   <?php echo $this->lang->line('multiple_lines'); ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="answer_kind"
                                                   value="radio" onclick="checkselection(this)"
                                                   <?php
                                                   if (set_value('answer_kind') == 'radio') {
                                                       echo "checked=\"checked\"";
                                                   }
                                                   ?>>
                                                   <?php echo $this->lang->line('some_selection'); ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="answer_kind"
                                                   value="checkbox" onclick="checkselection(this)"
                                                   <?php
                                                   if (set_value('answer_kind') == 'checkbox') {
                                                       echo "checked=\"checked\"";
                                                   }
                                                   ?>>
                                                   <?php echo $this->lang->line('multiple_selections'); ?>
                                        </label>
                                    </div>
                                </div>
                                <?php echo form_error('answer_kind'); ?>
                            </div> 
                            <div class="row" id="show_on_selection2" style="display: none">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type='button' class="btn btn-large btn-primary" value='<?php echo $this->lang->line('+_add_answer'); ?>'id='addButton'>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="show_on_selection3" style="display: none">
                                <input type="hidden" name="total_runtime_checkbox" id="total_runtime_checkbox" value="2" />
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div id='TextBoxesGroup'>
                                            <div id="TextBoxDiv1">
                                                <input type='textbox' id='textbox1' class="custom-textfield" name="answers1" required  /> 
                                                <?php echo form_error('answers1'); ?>
                                            </div>
                                            <div id="TextBoxDiv2">
                                                <input type='textbox' id='textbox2' class="custom-textfield" name="answers2" required /> 
                                                <?php echo form_error('answers2'); ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <a class="btn btn-flat" href="<?php echo base_url() . 'super_admin/standard_categories_question/' . $category_details->cat_id; ?>"><?php echo $this->lang->line('cancel'); ?></a>
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                        </div>
                    </div> 
                    <em class="text-caption"><?php echo $this->lang->line('requred_text_labels_text'); ?></em>
                </form>
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>

<script type="text/javascript">
    $(document).ready(function(){
        var counter = 3;
        $("#addButton").click(function () {
            var string = '<div id="TextBoxDiv'+counter+'"><input type="textbox" class="custom-textfield" id="textbox'+counter+'" name="answers'+counter+'" /><a id="' +counter+ '" onclick="removeans(this)"><i class="md md-delete"></i></a>';
            jQuery('#TextBoxesGroup').append(string);
            jQuery('#total_runtime_checkbox').val(counter);
            counter++;
        });
        checkonpageLoad();
    });
    
    function checkonpageLoad(){
        if($('input[type="radio"]:checked').val() == 'radio' || $('input[type="radio"]:checked').val() == 'checkbox'){
            $("#show_on_selection1").css("display", 'table-row');
            $("#show_on_selection2").css("display", 'table-row');
            $("#show_on_selection3").css("display", 'table-row');
        }else{
            $("#show_on_selection1").css("display", 'none');
            $("#show_on_selection2").css("display", 'none');
            $("#show_on_selection3").css("display", 'none');
        }
    }
    
    function removeans(ele){
        var current_id = $(ele).attr('id');
        $("#TextBoxDiv" + current_id).remove();
    }
    
    function checkselection(ele){
        // alert(ele.value);
        if(ele.value == 'radio' || ele.value == 'checkbox'){
            $("#show_on_selection1").css("display", 'table-row');
            $("#show_on_selection2").css("display", 'table-row');
            $("#show_on_selection3").css("display", 'table-row');
        }else{
            $("#show_on_selection1").css("display", 'none');
            $("#show_on_selection2").css("display", 'none');
            $("#show_on_selection3").css("display", 'none');
        }
    }
    $( "#addQuestion" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#addQuestion')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#addQuestion')).hide();
            label.remove();
        }
    });
</script>
<style>
    #TextBoxesGroup i {
        margin: -7px 0 0 10px;
    }

    #TextBoxesGroup div { margin: 10px 0; }
</style>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<script src="<?php echo assets_url_js; ?>jquery-ui.min.js"></script>
<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',
            // General options
            theme : "advanced",
            mode : "exact",
            elements : "ajaxfilemanager",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
       
            theme_advanced_buttons1 : ",link,unlink,image",
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,
            //add icons to upload /maintain images/files at the server
            file_browser_callback : "ajaxfilemanager", 

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });

    //add icons to upload /maintain images/files at the server

    function ajaxfilemanager(field_name, url, type, win) {
        var ajaxfilemanagerurl = "<?php echo base_url(); ?>assets/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
        var view = 'detail';
        switch (type) {
            case "image":
                view = 'thumbnail';
                break;
            case "media":
                break;
            case "flash": 
                break;
            case "file":
                break;
            default:
                return false;
        }
        tinyMCE.activeEditor.windowManager.open({
            url: "<?php echo base_url(); ?>assets/jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
            width: 782,
            height: 440,
            inline : "yes",
            close_previous : "no"
        },{
            window : win,
            input : field_name
        });
    
    }
    
</script>

