<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary"><?php echo $this->lang->line('overview_question_category') . ' ' . $category_details->cat_name; ?></h2>
    </div>
    <?php if ($this->session->flashdata('success') != '') { ?>
        <div class="alert alert-success" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error') != '') { ?>

        <div class="alert alert-danger" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <div class="section-body">
        <!-- BEGIN DATATABLE 1 -->
        <div class="row">
            <div class="col-md-12 margin-bottom-xxl">
                <a href="<?php echo base_url() . 'super_admin/standard_categories_question/add/' . $category_details->cat_id; ?>" class="btn ink-reaction btn-primary" ><?php echo $this->lang->line('add_question'); ?></a>
                <a href="<?php echo base_url() . 'super_admin/standard_categories/' . $category_details->form_id; ?>" class="btn ink-reaction btn-default" ><?php echo $this->lang->line('back'); ?></a>
            </div><!--end .col -->
        </div><!--end .row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="list_category_question" class="table table-striped table-hover" title="<?php echo $this->lang->line('mouse_hover_error'); ?>">
                        <thead>
                            <tr>
                                <th width="10%"><?php echo $this->lang->line('question_number');?></th>
                                <th width="60%"><?php echo $this->lang->line('question');?></th>
                                <th width="20%"> <?php echo $this->lang->line('answer');?></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>etc</td>
                                <td>etc</td>
                                <td>etc</td>
                                <td>etc</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!--end .table-responsive -->
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END DATATABLE 1 -->
    </div><!--end .section-body -->
</section>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_category_question').dataTable( {
            //  "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            'iDisplayLength': 100,   
            "bProcessing": true, 
            "bSort":false,  
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"},{"sClass":"align_center"}
            ],
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": "<?php echo site_url("super_admin/standard_categories_question/getJson/$category_details->cat_id"); ?>",
            "fnCreatedRow": function( nRow, aData, iDataIndex ) {
                $(nRow).attr('id', aData[4]);
            }
        } );
        $(function() {
            $("#list_category_question tbody").sortable({ opacity: 0.6, cursor: 'move', update: function() {
                    var order = $(this).sortable("serialize");
                    var url = "<?php echo base_url() . 'super_admin/standard_categories_question/sortable'; ?>";
                    $.post(url, order, function(){
                        window.location.reload();
                    });   
                }                                
            });
        });
    } );
    function deleteRow(ele){
        var current_id = $(ele).attr('id');
        var parent = $(ele).parent().parent();
        $.confirm({
            'title' : 'Digitaal op maat',
            'message'   : "<?php echo $this->lang->line('delete_question'); ?>",
            'buttons'   : {
                'Ja'    : {'class'      : 'btn ink-reaction btn-raised btn-sm btn-success',
                    'action': function(){
                        $.ajax({
                            type : 'POST',
                            url : http_host_js+'super_admin/standard_categories_question/delete/'+<?php echo $category_details->cat_id; ?>+'/'+current_id,
                            data: id =current_id,
                            beforeSend: function() {
                            },
                            success: function() {
                                parent.slideUp(500,function() {
                                    parent.remove();
                                    window.location.reload();
                                });
                            },
                            error : function(XMLHttpRequest, textStatus, errorThrown) {
                                alert('error');
                            }
                        });
                    }
                },
                'Nee'   : {
                    'class'     : 'btn ink-reaction btn-raised btn-sm btn-danger',
                    'action': function(){}      // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });             
        return false;
    }
</script>
