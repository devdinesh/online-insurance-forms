<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary"><?php echo $this->lang->line('overview_question_answer') . ' ' . strip_tags($question); ?></h2>
    </div>
    <?php if ($this->session->flashdata('success') != '') { ?>
        <div class="alert alert-success" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error') != '') { ?>

        <div class="alert alert-danger" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <div class="section-body">
        <!-- BEGIN DATATABLE 1 -->
        <div class="row">
            <div class="col-md-12 margin-bottom-xxl">
                <a href="<?php echo base_url() . "super_admin/standard_categories_question_answer/add/" . $question_id ?>" class="btn ink-reaction btn-primary" ><?php echo $this->lang->line('add_answer'); ?></a>
                <a href="<?php echo base_url() . "super_admin/standard_categories_question/" . $cat_id; ?>" class="btn ink-reaction btn-default" ><?php echo $this->lang->line('back'); ?></a>
            </div><!--end .col -->
        </div><!--end .row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="list_question_answers" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th><?php echo $this->lang->line('answer'); ?></th>
                                <th><?php echo $this->lang->line('go_to_question'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>etc</td>
                                <td>etc</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!--end .table-responsive -->
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END DATATABLE 1 -->
    </div><!--end .section-body -->
</section>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_question_answers').dataTable( {
            //  "bJQueryUI": true,
            "bProcessing":true,
            "bPaginate" : false,
            "bFilter": false,
            "bInfo": false,
            "bSort": false, 
            "sPaginationType": "full_numbers",
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"}
        
            ],
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": "<?php echo site_url("super_admin/standard_categories_question_answer/get_json/$question_id"); ?>"
        } );
    } );

</script>

