<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "super_admin/standard_categories_question_answer/" . $answer->question_id; ?>"><?php echo $this->lang->line('answer'); ?></a></li>
            <li class="active"><?php echo $title; ?></li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-8">
                <form class="form form-validate" id="editAnswer" action="<?php echo base_url() . 'super_admin/standard_categories_question_answer/editListener' ?>"  method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><?php echo $title; ?></header>
                        </div>
                        <div class="card-body">
                            <input type="hidden" value="<?php echo $answer->answer_id; ?>"
                                   name="answer_id"> <input type="hidden"
                                   value="<?php echo $answer->question_id; ?>" name="question_id">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="answer" value="<?php
echo htmlentities(
        set_value('answer') != '' ? set_value('answer') : isset(
                        $answer->answer) ? $answer->answer : '');
?>" required/> 
                                        <label for="answer"> <?php echo $this->lang->line('answer'); ?> <span class="required">*</span></label>
                                        <?php echo form_error('answer'); ?>
                                    </div>
                                </div>
                            </div>
                            <?php if ($display_smat_form == true) { ?>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="go_to_type"><?php echo $this->lang->line('go_to_question_category'); ?> :</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label class="radio-inline radio-styled">
                                                <input type="radio" id="go_to_none"
                                                       name="go_to_type" value="to_none"
                                                       <?php echo (!$answer->skip_to_questions) && (!$answer->skip_to_category) ? 'checked="checked"' : ''; ?>
                                                       onclick="show_none()">  <?php echo $this->lang->line('geen'); ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="radio-inline radio-styled">
                                                <input type="radio" id="go_to_question"
                                                <?php echo ($answer->skip_to_questions) ? 'checked="checked"' : ''; ?>
                                                       name="go_to_type" value="to_question"
                                                       onclick="show_question_list()"> <?php echo $this->lang->line('to_question'); ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="radio-inline radio-styled">
                                                <input <?php echo ($answer->skip_to_questions === '0') ? 'checked="checked"' : ''; ?> type="radio" id="go_to_end" name="go_to_type" value="to_end" onclick="show_none()"><?php echo $this->lang->line('end_form'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <?php echo form_error('go_to_type'); ?>
                                </div>
                            <?php } ?>
                            <div class="row" id="hide_item" style="margin-top: 20px;">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <span id="bb"> </span>
                                        <label for="category"><p id="aa"></p></label>

                                    </div>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col-sm-12">
                                    <div class="form-group" id="cc">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <a class="btn btn-flat" href="<?php echo base_url() . "super_admin/standard_categories_question_answer/" . $answer->question_id; ?>"><?php echo $this->lang->line('cancel'); ?></a>
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                        </div>
                    </div> 
                    <em class="text-caption"><?php echo $this->lang->line('requred_text_labels_text'); ?></em>
                </form>
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>

<script type="text/javascript">

    $(document).ready(function() {
        /**
when the document is loaded, by default the radio button will be checked,
        according to the radio button checked, we call approapriate function to load
        the checkboxes
         */
        var go_to_none_var = document.getElementById("go_to_none");
        var go_to_end_var = document.getElementById("go_to_end");
        var go_to_question_var = document.getElementById("go_to_question");
	
        // when the document is loaded show some choices selected
        // by default on the edit page
        if(go_to_none_var.checked==true)
        {
            show_none();
        }
        else if(go_to_question_var.checked==true)
        {
            show_question_list();
            getcategoryquestion(<?php echo @$skip_cat_id; ?>);
        }else if(go_to_end_var.checked==true)
        {
            show_none();
        }
	
    });
	 
    function show_none()
    {
        // puts empy contents 
        $('#aa').load('<?php echo base_url('super_admin/standard_question_answer/get_edit_question_answer_cat_list_for_edit_page/none'); ?>');
        $('#bb').load('<?php echo base_url('super_admin/standard_question_answer/get_edit_question_answer_cat_list_for_edit_page_values/none/0'); ?>');
        $('#cc').load('<?php echo base_url('super_admin/standard_question_answer/get_edit_question_answer_cat_list_for_edit_page/none'); ?>');
    }


    function show_question_list()
    {
        // loads the list of questions
        $('#aa').load('<?php echo base_url('super_admin/standard_question_answer/get_edit_question_answer_cat_list_for_edit_page/to_question'); ?>');
        $('#bb').load('<?php echo base_url('super_admin/standard_question_answer/get_edit_question_answer_cat_list_for_edit_page_values/to_question/' . $answer->answer_id); ?>');
    }

    function getcategoryquestion(value){

        var cat_id=value;
        var answer_id="<?php echo $answer->answer_id; ?>";
	
        var url = "<?php echo base_url(); ?>" +  "super_admin/standard_question_answer/get_category_question/" + cat_id+"/"+answer_id ;
        $('#cc').load(url);
    }
    $( "#editAnswer" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#editAnswer')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#editAnswer')).hide();
            label.remove();
        }
    });
</script>



