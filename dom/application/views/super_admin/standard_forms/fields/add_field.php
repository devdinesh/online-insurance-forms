<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'super_admin/standard_form_fields/' . $form_id; ?>"><?php echo $this->lang->line('list_import_fields'); ?></a></li>
            <li class="active"><?php echo $title; ?></li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-8">
                <form class="form form-validate" id="addFormfield" action="<?php echo base_url() . 'super_admin/standard_form_fields/addListener/' . $form_id; ?>"  method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><?php echo $title; ?></header>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="field_name" value="<?php echo htmlentities((set_value('field_name') != '') ? set_value('field_name') : '' ); ?>" required/> 
                                        <label for="field_name"> <?php echo $this->lang->line('name_import_item'); ?> <span class="required">*</span></label>
                                        <?php echo form_error('field_name'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="name_on_form" value="<?php echo htmlentities((set_value('name_on_form') != '') ? set_value('name_on_form') : '' ); ?>" required/> 
                                        <label for="name_on_form"> <?php echo $this->lang->line('display_as'); ?> <span class="required">*</span></label>
                                        <?php echo form_error('name_on_form'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="field_type"><?php echo $this->lang->line('mark_field_as'); ?> :</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="radio-inline radio-styled">
                                        <input  type="radio" name="field_type" value="N" <?php echo set_radio('field_type', 'N'); ?> checked="checked">Normaal
                                    </label>
                                    <?php if (isset($show_mail) && $show_mail == FALSE) { ?>
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="field_type" value="E" <?php echo set_radio('field_type', 'E'); ?>>E-mail
                                        </label>
                                    <?php } ?>
                                    <?php if (isset($show_ploicy_number) && $show_ploicy_number == FALSE) { ?>
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="field_type" value="P" <?php echo set_radio('field_type', 'p'); ?>>Polisnummer
                                        </label>
                                    <?php } ?>
                                    <?php if (isset($show_schadenummer) && $show_schadenummer == FALSE) { ?>
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="field_type" value="S" <?php echo set_radio('field_type', 'S'); ?>>Schadenummer
                                        </label>
                                    <?php } ?>
                                    <?php if (isset($show_behandler) && $show_behandler == FALSE) { ?>
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="field_type" value="B" <?php echo set_radio('field_type', 'B'); ?>>Behandelaar
                                        </label>
                                    <?php } ?>
                                    <?php if (isset($show_behandler_emial) && $show_behandler_emial == FALSE) { ?>

                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="field_type" value="BE" <?php echo set_radio('field_type', 'BE'); ?>>Behandelaar E-mail
                                        </label>
                                    <?php } ?>
                                </div>
                                <?php echo form_error('field_type'); ?>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <a class="btn btn-flat" href="<?php echo base_url() . 'super_admin/standard_form_fields/' . $form_id; ?>"><?php echo $this->lang->line('cancel');?></a>
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction"><?php echo $this->lang->line('save');?></button>
                            </div>
                        </div>
                    </div> 
                    <em class="text-caption"><?php echo $this->lang->line('requred_text_labels_text'); ?></em>
                </form>
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>
<script type="text/javascript">
    $( "#addFormfield" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#addFormfield')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#addFormfield')).hide();
            label.remove();
        }
    });
</script>