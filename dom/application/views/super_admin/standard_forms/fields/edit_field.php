<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'super_admin/standard_form_fields/' . $field->form_id; ?>"><?php echo $this->lang->line('list_import_fields'); ?></a></li>
            <li class="active"><?php echo $title; ?></li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-8">
                <form class="form form-validate" id="editFormfield" action="<?php echo base_url() . 'super_admin/standard_form_fields/editListener/' . $field->field_id; ?>"  method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><?php echo $title; ?></header>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="field_name" value="<?php echo htmlentities(set_value('field_name') !== '' ? set_value('field_name') : isset($field->field_name) ? $field->field_name : ''); ?>" required/> 
                                        <label for="field_name"> <?php echo $this->lang->line('name_import_item'); ?> <span class="required">*</span></label>
                                        <?php echo form_error('field_name'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="name_on_form" value="<?php echo htmlentities(set_value('name_on_form') !== '' ? set_value('name_on_form') : isset($field->name_on_form) ? $field->name_on_form : ''); ?>" required/> 
                                        <label for="name_on_form"> <?php echo $this->lang->line('display_as'); ?> <span class="required">*</span></label>
                                        <?php echo form_error('name_on_form'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="field_type"><?php echo $this->lang->line('mark_field_as'); ?> :</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="radio-inline radio-styled">
                                        <input  type="radio" name="field_type" value="N" <?php
                                        if ($field->field_type == 'N') {
                                            echo 'checked=checked';
                                        }
                                        ?>>Normaal
                                    </label>
                                    <?php if (isset($show_mail) && ($show_mail == FALSE or $show_mail == $field->field_id)) { ?>
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="field_type" value="E" <?php
                                    if ($field->field_type == 'E') {
                                        echo 'checked=checked';
                                    }
                                        ?>>E-mail
                                        </label>
                                    <?php } ?>
                                    <?php if (isset($show_ploicy_number) && ($show_ploicy_number == FALSE or $show_ploicy_number == $field->field_id)) { ?>
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="field_type" value="P" <?php
                                    if ($field->field_type == 'P') {
                                        echo 'checked=checked';
                                    }
                                        ?>>Polisnummer
                                        </label>
                                    <?php } ?>
                                    <?php if (isset($show_schadenummer) && ($show_schadenummer == FALSE or $show_schadenummer == $field->field_id)) { ?>
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="field_type" value="S" <?php
                                    if ($field->field_type == 'S') {
                                        echo 'checked=checked';
                                    }
                                        ?>/>Schadenummer
                                        </label>
                                    <?php } ?>
                                    <?php if (isset($show_behandler) && ($show_behandler == FALSE or $show_behandler == $field->field_id)) { ?>
                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="field_type" value="B" <?php
                                    if ($field->field_type == 'B') {
                                        echo 'checked=checked';
                                    }
                                        ?>>Behandelaar
                                        </label>
                                    <?php } ?>
                                    <?php if (isset($show_behandler_emial) && ($show_behandler_emial == FALSE or $show_behandler_emial == $field->field_id)) { ?>

                                        <label class="radio-inline radio-styled">
                                            <input type="radio" name="field_type" value="BE" <?php
                                    if ($field->field_type == 'BE') {
                                        echo 'checked=checked';
                                    }
                                        ?>>Behandelaar E-mail
                                        </label>
                                    <?php } ?>
                                </div>
                                <?php echo form_error('field_type'); ?>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <a class="btn btn-flat" href="<?php echo base_url() . 'super_admin/standard_form_fields/' . $field->form_id; ?>"><?php echo $this->lang->line('cancel'); ?></a>
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                        </div>
                    </div> 
                    <em class="text-caption"><?php echo $this->lang->line('requred_text_labels_text'); ?></em>
                </form>
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>
<script type="text/javascript">
    $( "#editFormfield" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#editFormfield')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#editFormfield')).hide();
            label.remove();
        }
    });
</script>
<!--

/// old data going welll
<h2><span>Details formulierveld</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'super_admin/standard_form_fields/editListener/' . $field->field_id; ?>"
        method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-striped table-condensed"
               border="0">
            <tbody>

                <tr>
                    <td width="31%">Naam import item: <span class="mandatory">*</span></td>
                    <td><input class="input-append" type="text" name="field_name"
                               value="<?php echo htmlentities(set_value('field_name') !== '' ? set_value('field_name') : isset($field->field_name) ? $field->field_name : '');
                                ?>" /></td>
                    <td><?php echo form_error('field_name'); ?></td>
                </tr>

                <tr>
                    <td>Weergeven als:<span class="mandatory">*</span></td>
                    <td><input type="text" name="name_on_form"
                               value="<?php echo htmlentities(set_value('name_on_form') !== '' ? set_value('name_on_form') : isset($field->name_on_form) ? $field->name_on_form : '');
                                ?>" /></td>
                    <td><?php echo form_error('name_on_form'); ?></td>
                </tr>
                <tr>
                    <td>Markeer veld als: </td>
                    <td>
                        <label class="radio inline">
                            <input  type="radio" name="field_type" value="N" <?php
                               if ($field->field_type == 'N') {
                                   echo 'checked=checked';
                               }
                                ?> />Normaal
                        </label>
                        <?php if (isset($show_mail) && ($show_mail == FALSE or $show_mail == $field->field_id)) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="E" <?php
                        if ($field->field_type == 'E') {
                            echo 'checked=checked';
                        }
                            ?>/>E-mail
                            </label>
                        <?php } ?>
                        <?php if (isset($show_ploicy_number) && ($show_ploicy_number == FALSE or $show_ploicy_number == $field->field_id)) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="P" <?php
                        if ($field->field_type == 'P') {
                            echo 'checked=checked';
                        }
                            ?>/>Polisnummer
                            </label>
                        <?php } ?>
                        <?php if (isset($show_schadenummer) && ($show_schadenummer == FALSE or $show_schadenummer == $field->field_id)) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="S" <?php
                        if ($field->field_type == 'S') {
                            echo 'checked=checked';
                        }
                            ?>/>Schadenummer
                            </label>
                        <?php } ?>
                        <?php if (isset($show_behandler) && ($show_behandler == FALSE or $show_behandler == $field->field_id)) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="B" <?php
                        if ($field->field_type == 'B') {
                            echo 'checked=checked';
                        }
                            ?>/>Behandelaar
                            </label>
                        <?php } ?>
                        <?php if (isset($show_behandler_emial) && ($show_behandler_emial == FALSE or $show_behandler_emial == $field->field_id)) { ?>
                            <label class="radio inline">
                                <input type="radio" name="field_type" value="BE" <?php
                        if ($field->field_type == 'BE') {
                            echo 'checked=checked';
                        }
                            ?>/>Behandelaar E-mail
                            </label>
                        <?php } ?>
                    </td>
                    <td><?php echo form_error('field_type'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>  &nbsp;&nbsp; 
                        <a href="<?php echo base_url() . 'super_admin/standard_form_fields/' . $field->form_id; ?>"  class="btn btn-large btn-primary">Annuleer</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                        zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />-->
