<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary"><?php echo $title; ?></h2>
    </div>
    <?php if ($this->session->flashdata('success') != '') { ?>
        <div class="alert alert-success" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error') != '') { ?>

        <div class="alert alert-danger" role="alert">
            <a class="close" data-dismiss="alert" href="<?php echo current_url(); ?>">x</a><?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <div class="section-body">
        <!-- BEGIN DATATABLE 1 -->
        <div class="row">
            <div class="col-md-12 margin-bottom-xxl">
                <a href="<?php echo base_url() . 'super_admin/standard_form_fields/add/' . $form_id; ?>" class="btn ink-reaction btn-primary" ><?php echo $this->lang->line('add_new_field'); ?></a>
                <a href="<?php echo base_url() . 'super_admin/standard_forms'; ?>" class="btn ink-reaction btn-default" ><?php echo $this->lang->line('back'); ?></a>
            </div><!--end .col -->
        </div><!--end .row -->

        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="list_forms_field" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="35%"><?php echo $this->lang->line('name_import_item'); ?></th>
                                <th width="35%"><?php echo $this->lang->line('display_as'); ?></th>
                                <th width="20%"><?php echo $this->lang->line('type_field'); ?></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>etc</td>
                                <td>etc</td>
                                <td>etc</td>
                                <td>etc</td>

                            </tr>
                        </tbody>
                    </table>
                </div><!--end .table-responsive -->
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END DATATABLE 1 -->
    </div><!--end .section-body -->
</section>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_forms_field').dataTable( {
            //  "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "bProcessing": true,
            'iDisplayLength': 100,   
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center","bSortable": false},{"sClass":"align_center","bSortable": false}
                
            ],
            "oLanguage":translate_dutch_lag,
            "sAjaxSource": "<?php echo site_url("super_admin/standard_form_fields/getJson/$form_id"); ?>"
        } );
    } );
    function deleteRow(ele){
        var current_id = $(ele).attr('id');
        var parent = $(ele).parent().parent();
        $.confirm({
            'title' : 'Digitaal op maat',
            'message'   : '<p>Do you want to delete?</p>',
            'buttons'   : {
                'Ja'    : {'class'      : 'btn ink-reaction btn-raised btn-sm btn-success',
                    'action': function(){
                        $.ajax({
                            type : 'POST',
                            url : http_host_js+'super_admin/standard_form_fields/delete_field/'+current_id,
                            data: id =current_id,
                            beforeSend: function() {
                            },
                            success: function() {
                                parent.slideUp(500,function() {
                                    parent.remove();
                                    window.location.reload();
                                });
                            },
                            error : function(XMLHttpRequest, textStatus, errorThrown) {
                                alert('error');
                            }
                        });
                    }
                },
                'Nee'   : {
                    'class'     : 'btn ink-reaction btn-raised btn-sm btn-danger',
                    'action': function(){}      // Nothing to do in this case. You can as well omit the action property.
                }
            }
        });             
        return false;
    }
</script>