<style>
    .mendetory {
        color: red;
    }
</style>
<div class="row-fluid">
    <p class="span12"></p>
</div>

<h2><span>Edit Details</span></h2>
<form method="post"
      action="<?php echo base_url() . 'super_admin/maintain_super_admin/edit_details_listener/' . $super_admin->super_admin_id; ?>"
      enctype="multipart/form-data" id="1">
    <table width="100%" class="table table-striped table-condensed">
        <tbody>

            <tr>
                <td>First Name<span class="mandatory">*</span></td>
                <td><input class="input" type="text" name="super_admin_first_name"
                           value="<?php echo set_value('super_admin_first_name') != '' ? set_value('super_admin_first_name') : (isset($super_admin->super_admin_first_name) ? $super_admin->super_admin_first_name : ''); ?>" />
                </td>
                <td><?php echo form_error('super_admin_first_name'); ?></td>
            </tr>
            <tr>
                <td>Middle Name<span class="mandatory">*</span></td>
                <td><input class="input" type="text" name="super_admin_middle_name"
                           value="<?php echo set_value('super_admin_middle_name') != '' ? set_value('super_admin_middle_name') : (isset($super_admin->super_admin_middle_name) ? $super_admin->super_admin_middle_name : ''); ?>" />
                </td>
                <td><?php echo form_error('super_admin_middle_name'); ?></td>
            </tr>
            <tr>
                <td>Last Name<span class="mandatory">*</span></td>
                <td><input class="input" type="text" name="super_admin_last_name"
                           value="<?php echo set_value('super_admin_last_name') != '' ? set_value('super_admin_last_name') : (isset($super_admin->super_admin_last_name) ? $super_admin->super_admin_last_name : ''); ?>" />
                </td>
                <td><?php echo form_error('super_admin_last_name'); ?></td>
            </tr>
            <tr>
                <td>Email<span class="mandatory">*</span></td>
                <td><input class="input" type="text" name="super_admin_email"
                           value="<?php echo set_value('super_admin_email') != '' ? set_value('super_admin_email') : (isset($super_admin->super_admin_email) ? $super_admin->super_admin_email : ''); ?>" />
                </td>
                <td><?php echo form_error('super_admin_email'); ?></td>
            </tr>

            <tr>
                <td>
                    <button type="submit" class="btn btn-large btn-primary">Opslaan</button> &nbsp; <a
                        href="<?php echo base_url() . 'super_admin/maintain_super_admin/index'; ?>" class="btn btn-large btn-primary">Annuleer </a>
                </td>
                <td></td>
            </tr>
            <tr>
                 <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                    zijn verplicht.
                </td>
            </tr>
        </tbody>
    </table>
</form>

<h2><span>Change Password</span></h2>
<form method="post"
      action="<?php echo base_url() . 'super_admin/maintain_super_admin/chage_password_listener/' . $super_admin->super_admin_id; ?>"
      enctype="multipart/form-data" id="1">
    <table width="100%" class="table table-striped table-condensed">
        <tbody>

            <tr>
                <td>Password<span class="mandatory">*</span></td>
                <td><input class="input" type="password" name="super_admin_password" />
                </td>
                <td><?php echo form_error('super_admin_password'); ?></td>
            </tr>

            <tr>
                <td>Repeat Password<span class="mandatory">*</span></td>
                <td><input class="input" type="password"
                           name="super_admin_password_repeat" /></td>
                <td><?php echo form_error('super_admin_password_repeat'); ?></td>
            </tr>
            <tr>
                <td>
                    <button type="submit" class="btn btn-large btn-primary">Opslaan</button> &nbsp; <a
                        href="<?php echo base_url() . 'super_admin/maintain_super_admin/index'; ?>" class="btn btn-large btn-primary">Annuleer</a>
                </td>
                <td></td>
            </tr>
            <tr>
                 <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                    zijn verplicht.
                </td>
            </tr>
        </tbody>
    </table>
</form>

<br>

<br>