
<h2><span>Add super admin user</span></h2>
<div>
    <form
        action="<?php echo base_url() . 'super_admin/maintain_super_admin/add_listener'; ?>"
        method="post">
        <table width="100%" class="table table-striped table-condensed">

            <tr>
                <td>First Name<span class="mandatory">*</span></td>
                <td><input type="text" name="super_admin_first_name"
                           value="<?php
echo set_value('super_admin_first_name') != '' ? set_value(
                'super_admin_first_name') : "";
?>" /></td>
                <td><?php echo form_error('super_admin_first_name'); ?></td>
            </tr>
            <tr>
                <td>Middle Name<span class="mandatory">*</span></td>
                <td><input type="text" name="super_admin_middle_name"
                           value="<?php
                           echo set_value('super_admin_middle_name') != '' ? set_value(
                                           'super_admin_middle_name') : "";
?>" /></td>
                <td><?php echo form_error('super_admin_middle_name'); ?></td>
            </tr>



            <tr>
                <td>Last Name<span class="mandatory">*</span></td>
                <td><input type="text" name="super_admin_last_name"
                           value="<?php
                           echo set_value('super_admin_last_name') != '' ? set_value(
                                           'super_admin_last_name') : "";
?>" /></td>
                <td><?php echo form_error('super_admin_last_name'); ?></td>
            </tr>
            <tr>
                <td>Email<span class="mandatory">*</span></td>
                <td><input type="text" name="super_admin_email"
                           value="<?php
                           echo set_value('super_admin_email') != '' ? set_value('super_admin_email') : "";
?>" /></td>
                <td><?php echo form_error('super_admin_email'); ?></td>
            </tr>


            <tr>
                <td>Password<span class="mandatory">*</span></td>
                <td><input type="password" name="super_admin_password" /></td>
                <td><?php echo form_error('super_admin_password'); ?></td>
            </tr>

            <tr>
                <td>Re Enter Passwor<span class="mandatory">*</span></td>
                <td><input type="password" name="super_admin_password_repeat" /></td>
                <td><?php echo form_error('super_admin_password_repeat'); ?></td>
            </tr>

            <tr>
                <td colspan="3">
                    <button type="submit" class="btn btn-large btn-primary">Opslaan</button> <a
                        href="<?php echo base_url() . 'super_admin/maintain_super_admin/index'; ?>" class="btn btn-large btn-primary">Annuleer</a>
                </td>
            </tr>

            <tr>
                <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                    zijn verplicht.
                </td>
            </tr>
        </table>
    </form>
</div>
<br>
