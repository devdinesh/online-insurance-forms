<h2><span>Tarieven</span></h2>
<form method="post"
      action="<?php echo base_url() . 'super_admin/rates/save'; ?>"
      enctype="multipart/form-data" id="1">
    <table width="100%" class="table table-striped table-condensed">
        <tbody>
            <tr>
        <input type="hidden" name="rate_id" value="<?php echo @$rate->id; ?>">
        <td>Van <span class="mandatory">*</span>
        </td>
        <td><input class="input" type="text"  name="number_from"  value="<?php echo htmlentities((set_value('number_from') != '') ? set_value('number_from') : (isset($rate->number_from) ? $rate->number_from : '') ); ?>" /></td>
        <td><?php echo form_error('number_from'); ?></td>
        </tr>
        <tr>
            <td>Tot <span class="mandatory">*</span>
            </td>
            <td><input class="input" type="text" name="number_to"
                       value="<?php echo htmlentities((set_value('number_to') != '') ? set_value('number_to') : (isset($rate->number_to) ? $rate->number_to : '') ); ?>" /></td>
            <td><?php echo form_error('number_to'); ?></td>
        </tr>
        <tr>
            <td>Tarief<span class="mandatory">*</span>
            </td>
            <td><input class="input" type="text" name="rate"
                       value="<?php echo htmlentities((set_value('rate') != '') ? set_value('rate') : (isset($rate->rate) ? $rate->rate : '') ); ?>" /></td>
            <td><?php echo form_error('rate'); ?></td>
        </tr>
        <tr>
            <td colspan="2"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                <a
                    href="<?php echo base_url() . 'super_admin/rates'; ?>"
                    class="btn btn-large btn-primary">Annuleer</a></td>
            <td></td>
        </tr>  
        <tr>
            <td colspan="3">Velden die gemarkeerd zijn met een <span
                    class="mandatory">*</span> zijn verplicht.
            </td>
        </tr>
        </tbody>
    </table>
</form>
<br>
<br>