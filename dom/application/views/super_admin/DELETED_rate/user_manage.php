<h2><span>User Rates</span></h2>
<form method="post"
      action="<?php echo base_url() . 'super_admin/rates_user/save'; ?>"
      enctype="multipart/form-data" id="1">
    <table width="100%" class="table table-striped table-condensed">
        <tbody>
            <tr>
        <input type="hidden" name="rate_id" value="<?php echo @$rate->id; ?>">
        <td>Van <span class="mandatory">*</span>
        </td>
        <td><input class="input" type="text"  name="from"  value="<?php echo htmlentities((set_value('from') != '') ? set_value('from') : (isset($rate->from) ? $rate->from : '') ); ?>" /></td>
        <td><?php echo form_error('from'); ?></td>
        </tr>
        <tr>
            <td>Tot <span class="mandatory">*</span>
            </td>
            <td><input class="input" type="text" name="to"
                       value="<?php echo htmlentities((set_value('to') != '') ? set_value('to') : (isset($rate->to) ? $rate->to : '') ); ?>" /></td>
            <td><?php echo form_error('to'); ?></td>
        </tr>
        <tr>
            <td>Tarief<span class="mandatory">*</span>
            </td>
            <td><input class="input" type="text" name="rate"
                       value="<?php echo htmlentities((set_value('rate') != '') ? convert_eng_dutch(set_value('rate')) : (isset($rate->rate) ? convert_eng_dutch($rate->rate): '') ); ?>" /></td>
            <td><?php echo form_error('rate'); ?></td>
        </tr>
        <tr>
            <td colspan="2"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                <a
                    href="<?php echo base_url() . 'super_admin/rates_user'; ?>"
                    class="btn btn-large btn-primary">Annuleer</a></td>
            <td></td>
        </tr>  
        <tr>
            <td colspan="3">Velden die gemarkeerd zijn met een <span
                    class="mandatory">*</span> zijn verplicht.
            </td>
        </tr>
        </tbody>
    </table>
</form>
<br>
<br>