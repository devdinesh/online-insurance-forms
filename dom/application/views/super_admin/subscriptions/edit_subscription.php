<!-- <div class="row-fluid">
	<p class="span12">
		<a href="<?php echo base_url() . "super_admin"; ?>"> Home </a> &gt; <a
			href="<?php echo base_url() . "super_admin/clients" ?>"> Clients </a>
		&gt; <a
			href="<?php echo base_url() . "super_admin/clients/edit/" . $subscription->client_id; ?>">
			Client </a> &gt; <a
			href="<?php echo base_url() . "super_admin/client_subscriptions/" . $subscription->client_id ?>">
			Subscriptions </a> <br />
	</p>
</div>
 -->
<script type="text/javascript">
    $(function() {
        $( "#from_date").datepicker({ dateFormat: 'dd-mm-yy' });
        $( "#end_date").datepicker({ dateFormat: 'dd-mm-yy'});
    });
</script>
<h2 class="title">Edit Subscription</h2>

<form method="post"
	action="<?php echo base_url() . 'super_admin/subscriptions/edit_listener/' . $subscription->client_subscription_id; ?>"
	enctype="multipart/form-data" id="1">
	<table width="100%" class="table table-striped table-condensed">
		<tbody>
			<tr>
				<td width="20%">Starting Date</td>
				<td><input class="input" id="from_date" type="text" name="from_date"
					value="<?php echo htmlentities((set_value('from_date') != '') ? set_value('from_date') : (isset($subscription->from_date) ? $subscription->from_date : '') ); ?>" />
				</td>
				<td><?php echo form_error('from_date'); ?></td>
			</tr>

			<tr>
				<td>Ending Date <span class="mendetory">*</span>
				</td>
				<td><input class="input" id="end_date" type="text" name="end_date"
					value="<?php echo htmlentities((set_value('end_date') != '') ? set_value('end_date') : (isset($subscription->end_date) ? $subscription->end_date : '') ); ?>" /></td>
				<td><?php echo form_error('end_date'); ?></td>
			</tr>
			<tr>
				<td>Subscription Kinds <span class="mendetory">*</span>
				</td>
				<td><select name="subscription_kinds_id">
                        <?php
                        foreach ($subcription_kinds as $subcription_kind)
                        {
                            ?>
                            <option
							<?php echo $subscription->subscription_kinds_id == $subcription_kind->subscription_kinds_id ? 'selected="selected"' : ''?>
							value="<?php print_r($subcription_kind->subscription_kinds_id) ?>"><?php print_r($subcription_kind->subscription_title) ?></option>
                            <?php
                        }
                        ?>
                    </select></td>
				<td><?php echo form_error('address'); ?></td>
			</tr>
			<tr>
				<td colspan="2"><button type="submit" class="btn">Save</button>
					&nbsp; &nbsp; &nbsp; &nbsp; <a
					href="<?php echo base_url() . 'super_admin/clients'; ?>" class="btn">Cancel</a></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="3">Fields marked with <span class="mendetory">*</span>
					are mandatory.
				</td>
			</tr>
		</tbody>
	</table>
</form>
<br>

<br>