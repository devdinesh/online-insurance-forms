<!-- 
<div class="row-fluid">
	<p class="span12">
		<a href="<?php echo base_url() . "super_admin"; ?>"> Home </a> &gt; <a
			href="<?php echo base_url() . "super_admin/clients" ?>"> Clients </a>
		&gt; <a
			href="<?php echo base_url() . "super_admin/client_subscriptions/".$client_id ?>">
			Client Subscription</a> &gt; Add Subscription
	</p>
</div>
 -->
<script type="text/javascript">
    $(function() {
        $( "#from_date").datepicker({ dateFormat: 'dd-mm-yy' });
        $( "#end_date").datepicker({ dateFormat: 'dd-mm-yy'});
    });
</script>
<h2 class="title title-margin"  >Add Subscription</h2>

<form method="post"
	action="<?php echo base_url() . 'super_admin/subscriptions/add_listener/'.$client_id; ?>"
	enctype="multipart/form-data" id="1">
	<table width="100%" class="table table-striped table-condensed">
		<tbody>
			<tr>
				<td width="20%">Starting Date <span class="mandatory">*</span></td>
				<td><input class="input" id="from_date" type="text" name="from_date"
					value="<?php echo htmlentities((set_value('from_date') != '') ? set_value('from_date'):'' ); ?>" />
				</td>
				<td><?php echo form_error('from_date'); ?></td>
			</tr>

			<tr>
				<td>Ending Date 
				</td>
				<td><input class="input" id="end_date" type="text" name="end_date"
					value="<?php echo htmlentities((set_value('end_date') != '') ? set_value('end_date') : ''); ?>" /></td>
				<td><?php echo form_error('end_date'); ?></td>
			</tr>
			<tr>
				<td>Subscription Kinds <span class="mandatory">*</span>
				</td>
				<td><select name="subscription_kinds_id">
						<option value="">Select Subscription_kind</option>
                        <?php
                        foreach ($subcription_kinds as $subcription_kind)
                        {
                            ?>
                            <option
							value="<?php print_r($subcription_kind->subscription_kinds_id) ?>"<?php if(set_value('subscription_kinds_id')==$subcription_kind->subscription_kinds_id){echo "selected='selected'";}?>><?php print_r($subcription_kind->subscription_title) ?></option>
                            <?php
                        }
                        ?>
                    </select></td>
				<td><?php echo form_error('subscription_kinds_id'); ?></td>
			</tr>
			<tr>
				<td colspan="2"><button type="submit" class="btn">Add</button>
					&nbsp; <a
					href="<?php echo base_url() . 'super_admin/client_subscriptions/'.$client_id; ?>" class="btn">Cancel</a></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="3">Fields marked with <span class="mandatory">*</span>
					are mandatory.
				</td>
			</tr>
		</tbody>
	</table>
</form>
<br>

<br>