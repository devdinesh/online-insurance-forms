<!-- 
<div class="row-fluid">
	<p class="span12">
		<a href="<?php echo base_url() . "super_admin"; ?>"> Home </a> &gt; <a
			href="<?php echo base_url() . "super_admin/clients" ?>"> Clients </a>
		&gt; <a
			href="<?php echo base_url() . "super_admin/clients/edit/" . $client->client_id; ?>">  <?php echo $client->client_name ?> </a>
		&gt; Subscription <br />

	</p>
</div>
 -->
 <h2><span><?php echo $client->client_name; ?></span></h2>
<h2><span>Client Subscription</span></h2>

<?php

/*
 * if client is not having subscription without end date then only we show the
 * button for adding the subscription
 */
if (!client_subscription_model::has_subscription_without_end_date(
        $client->client_id))
{
    ?>
<div class="row-fluid">
	<p class="span12">
		<a
			href="<?php echo base_url() . 'super_admin/subscriptions/add/'.$client->client_id; ?>" class="btn">Add Subscription</a>
	</p>
</div>

<?php
}
?>
<div>
	<!-- Client( <a href="<?php echo base_url() . 'super_admin/clients/edit/' . $client->client_id ?>"> asdf </a> ) </a> &gt; Subscription -->
	<table id="list_clients"
		class="tbl table table-striped table-condensed">
		<thead>
			<tr class="tbl_hd">
				<th>Kind Of Subscription</th>
				<th>Start Date</th>
				<th>End Date</th>
				<th>Subscription Fee</th>
				<th>#Forms</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>etc</td>
				<td>etc</td>
				<td>etc</td>
				<td>etc</td>
				<td>etc</td>

			</tr>
		</tbody>
	</table>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_clients').dataTable( {
            //  "bJQueryUI": true,
        	"bProcessing":true,
            "bPaginate" : false,
            "bFilter": false,
            "bInfo": false,
            "bSort": false, 
            'iDisplayLength': 100, 
           
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"}
            ],
            "sAjaxSource": "<?php echo base_url(); ?>super_admin/subscriptions/client_subscriptions_json/<?php echo $client->client_id; ?>"
        } );
    } );
</script>
<!-- actual page content ends -->

<br>
<h2><span>Invoices</span></h2>


<div>
	<div class="clear"></div>
	<table class="table table-striped table-condensed"
		id="list_subscription_kinds">
		<thead>
			<tr>
				<th>Invoice Number</th>
				<th>Invoice Amount</th>
				<th>Invoice Date</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>etc</td>
				<td>etc</td>
				<td>etc</td>
				<td>etc</td>
			</tr>
		</tbody>
	</table>
</div>
<br>
<br>
<br>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        var oTable = $('#list_subscription_kinds').dataTable( {
       	 //  "bJQueryUI": true,
        	"bProcessing":true,
            "bPaginate" : false,
            "bFilter": false,
            "bInfo": false,
            "bSort": false, 
            'iDisplayLength': 100,  
            "aoColumns":[
                {"sClass":"align_center"},{"sClass":"align_center"},
                {"sClass":"align_center"},{"sClass":"align_center"}
            ],
            "sAjaxSource": "<?php echo site_url("super_admin/subscriptions/client_invoice_json/" . $client->client_id  ) ; ?>"
        } );
    } );
</script>



