<html>
<head>
<title>
            <?php echo $title; ?>
        </title>
</head>

<body>
	<form
		action="<?php echo base_url(); ?>super_admin/forgotpassword_listener"
		method="post">
		<table border="0" width="50%">
			<tr>
				<td colspan="3"><h3>Super Admin Forgot Password</h3></td>
			</tr>
                <?php
                if (isset($login_error))
                {
                    echo "<tr><td colspan=\"3\"> " .
                             htmlentities($login_error, ENT_QUOTES) .
                             "</td></tr>";
                }
                ?>
                <tr>
				<td width="10%">Mail :</td>
				<td width="15%"><input type="email" name="super_admin_email"></td>
				<td><?php echo form_error('super_admin_email'); ?></td>
			</tr>
			<tr>
				<td colspan="3">
					<button type="submit">Send Password</button>
				</td>
			</tr>
			<tr>
				<td colspan="3"><a href="<?php echo base_url(); ?>super_admin">Back
						to Login Page</a></td>
			</tr>
		</table>
	</form>
</body>
</html>
