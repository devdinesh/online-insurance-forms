<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . 'super_admin/standard_group_forms/' . $group_info->form_group_id; ?>"><?php echo $this->lang->line('form_groups'); ?></a></li>
            <li class="active"><?php echo $title; ?></li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-8">
                <form class="form form-validate" id="editForm" action="<?php echo base_url() . 'super_admin/standard_group_forms/editListener/' . $group_form->id; ?>"  method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><?php echo $title; ?></header>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <input type="hidden" name="form_group_id" value="<?php echo $group_info->form_group_id; ?>"/>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <select id="standard_from_id" name="standard_from_id" class="form-control" required>
                                            <option value=""></option>
                                            <?php foreach ($standard_forms as $form) { ?>
                                                <option value="<?php echo $form->form_id; ?>"
                                                <?php
                                                $value = ( $group_form->standard_from_id == '' ) ? '' : $value = $group_form->standard_from_id;
                                                if ($value == $form->form_id) {
                                                    echo "selected=selected";
                                                }else
                                                    
                                                    ?>><?php echo $form->form_name; ?></option>
                                                    <?php } ?>
                                        </select>
                                        <label for="standard_from_id"> <?php echo $this->lang->line('select_form'); ?> <span class="required">*</span></label>
                                        <?php echo form_error('standard_from_id'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <a class="btn btn-flat" href="<?php echo base_url() . 'super_admin/standard_group_forms/' . $group_info->form_group_id; ?>"><?php echo $this->lang->line('cancel'); ?></a>
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction"><?php echo $this->lang->line('save'); ?></button>
                            </div>
                        </div>
                    </div> 
                    <em class="text-caption"><?php echo $this->lang->line('requred_text_labels_text'); ?></em>
                </form>
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>
<script type="text/javascript">
    $( "#editForm" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#editForm')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#editForm')).hide();
            label.remove();
        }
    });
</script>

<!--
old form data ....going well  Please continue ... 
<h2><span>Add New Form</span></h2>
<div>
    <form  action="<?php echo base_url() . 'super_admin/standard_group_forms/editListener/' . $group_form->id; ?>"  method="post">
        <table width="100%" class="table table-striped table-condensed"  border="0">
            <tbody>
                <tr>
                    <td colspan="3"><input type="hidden" name="form_group_id" value="<?php echo $group_info->form_group_id; ?>"/></td>
                </tr>
                <tr>
                    <td>Select Form <span class="mandatory">*</span></td>
                    <td>
                        <select name="standard_from_id"> 
                            <option value="">Select Form</option>
                            <?php foreach ($standard_forms as $form) { ?>
                                <option value="<?php echo $form->form_id; ?>"
                                <?php
                                $value = ( $group_form->standard_from_id == '' ) ? '' : $value = $group_form->standard_from_id;
                                if ($value == $form->form_id) {
                                    echo "selected=selected";
                                }else
                                    
                                    ?>><?php echo $form->form_name; ?></option>
                                    <?php } ?>
                        </select></td>
                    <td><?php echo form_error('standard_from_id'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Save</button>
                        &nbsp;&nbsp; <a  href="<?php echo base_url() . 'super_admin/standard_group_forms/' . $group_info->form_group_id; ?>"  class="btn btn-large btn-primary">Cancel</a></td>
                </tr>
                <tr>
                    <td colspan="3">Fields marked with <span class="mandatory">*</span> are mandatory.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />-->
