<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $title; ?> | Online Insurance Forms</title>

        <!-- BEGIN META -->
        <meta name="google-translate-customization"  content="2227024e11d5699-516aaa854de6b7d6-g515b8caffd13378a-30"></meta>
        <!-- END META -->

        <!-- BEGIN STYLESHEETS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
        <link type="text/css" rel="stylesheet" href="<?php echo assets_mat_css; ?>theme-default/bootstrap.css?1422792965" />
        <link type="text/css" rel="stylesheet" href="<?php echo assets_mat_css; ?>theme-default/materialadmin.css?1425466319" />
        <link type="text/css" rel="stylesheet" href="<?php echo assets_mat_css; ?>theme-default/font-awesome.min.css?1422529194" />
        <link type="text/css" rel="stylesheet" href="<?php echo assets_mat_css; ?>theme-default/material-design-iconic-font.min.css?1421434286" />
        <!-- END STYLESHEETS -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="../../assets/js/libs/utils/html5shiv.js?1403934957"></script>
        <script type="text/javascript" src="../../assets/js/libs/utils/respond.min.js?1403934956"></script>
        <![endif]-->
    </head>
    <body class="menubar-hoverable header-fixed ">

        <!-- BEGIN LOGIN SECTION -->
        <section class="section-account">
            <div class="img-backdrop" style="background-image: url('<?php echo assets_mat_img; ?>img16.jpg')"></div>
            <div class="spacer"></div>
            <div class="card contain-sm style-transparent">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <a href="#"><img src="<?php echo assets_url_img; ?>s_logo.png" alt="logo" /></a>
                        </div>
                    </div>
                    <br>
                    <?php
                    if (isset($login_error) && $login_error != '') {
                        ?>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="alert alert-danger" role="alert">
                                    <a class="close" data-dismiss="alert"
                                       href="<?php echo current_url(); ?>">x</a><?php echo $login_error; ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <span class="text-lg text-bold text-primary">Super Admin Login</span>
                            <br/>
                            <form class="form form-validate" action="<?php echo base_url(); ?>super_admin/login?redirect_to=<?php echo $redirect_to; ?>" accept-charset="utf-8" method="post">
                                <input type="hidden" name="redirect_to" value="<?php echo $redirect_to; ?>">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="super_admin_email" name="super_admin_email" value="<?php echo htmlentities(set_value('super_admin_email')); ?>" required>
                                    <label for="super_admin_email">E-mailadres</label>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="super_admin_password" name="super_admin_password" required>
                                    <label for="super_admin_password">Wachtwoord</label>
<!--                                    <p class="help-block"><a href="#">Forgotten?</a></p>-->
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-xs-6 text-left">
                                        &nbsp;
                                    </div><!--end .col -->
                                    <div class="col-xs-6 text-right">
                                        <button class="btn btn-primary btn-raised" type="submit">inloggen</button>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                            </form>
                        </div><!--end .col -->

                    </div><!--end .row -->
                </div><!--end .card-body -->
            </div><!--end .card -->
        </section>
        <!-- END LOGIN SECTION -->

        <!-- BEGIN JAVASCRIPT -->
        <script src="<?php echo assets_mat_js; ?>libs/jquery/jquery-1.11.2.min.js"></script>
        <script src="<?php echo assets_mat_js; ?>libs/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo assets_mat_js; ?>libs/bootstrap/bootstrap.min.js"></script>
        <script src="<?php echo assets_mat_js; ?>libs/spin.js/spin.min.js"></script>
        <script src="<?php echo assets_mat_js; ?>libs/autosize/jquery.autosize.min.js"></script>
        <script src="<?php echo assets_mat_js; ?>libs/nanoscroller/jquery.nanoscroller.min.js"></script>
        <script src="<?php echo assets_mat_js; ?>core/source/App.js"></script>
        <script src="<?php echo assets_mat_js; ?>libs/jquery-validation/dist/jquery.validate.min.js"></script>
          <script src="<?php echo assets_mat_js; ?>libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="<?php echo assets_mat_js; ?>libs/jquery-validation/dist/localization/messages_nl.min.js"></script>
        <script src="<?php echo assets_mat_js; ?>core/source/AppNavigation.js"></script>
        <script src="<?php echo assets_mat_js; ?>core/source/AppOffcanvas.js"></script>
        <script src="<?php echo assets_mat_js; ?>core/source/AppCard.js"></script>
        <script src="<?php echo assets_mat_js; ?>core/source/AppForm.js"></script>
        <script src="<?php echo assets_mat_js; ?>core/source/AppNavSearch.js"></script>
        <script src="<?php echo assets_mat_js; ?>core/source/AppVendor.js"></script>
        <script src="<?php echo assets_mat_js; ?>core/demo/Demo.js"></script>
        <!-- END JAVASCRIPT -->
    </body>
</html>
