<h2><span>Add New Feature</span></h2>
<div>
    <form id="add_content"
          action="<?php echo base_url() . 'super_admin/feature/add_listener'; ?>"
          method="post" enctype="multipart/form-data">

        <table width="100%" class="table table-striped table-condensed">
            <tbody>

                <tr>
                    <td>Feature Label <span class="mandatory">*</span>
                    </td>
                    <td><input class="input" type="text" name="feature_label"
                               value="<?php echo (set_value('feature_label')); ?>" /></td>
                    <td><?php echo form_error('feature_label'); ?></td>
                </tr>
                <tr>
                    <td width="17%">Feature Image </td>
                    <td width="40%"><input type="file" name="feature_image" class="input span3"
                                           style="padding: 0 12px 0 0;" /></td>
                    <td><?php
$error = form_error('feature_image');
if ($image_error != null) {
    echo $image_error;
} else if ($error != '') {
    echo $error;
}
?></td>
                </tr>

                <tr>
                    <td class="vertical_align">Feature Text<span class="mandatory">*</span></td>

                    <td><textarea id="feature_text" name="feature_text" rows="5"
                                  class="tinymce span6"><?php echo (set_value('feature_text')); ?></textarea></td>
                    <td><?php echo form_error('feature_text'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                        &nbsp;&nbsp; <a
                            href="<?php echo base_url() . 'super_admin/admin/feature'; ?>"
                            class="btn btn-large btn-primary">Annuleer</a></td>
                </tr>

                <tr>
                    <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                         zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />

<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
 
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',
            // General options
        
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,|,bullist,numlist,|,undo,redo,|,link,unlink,image",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
        
    });
</script>
