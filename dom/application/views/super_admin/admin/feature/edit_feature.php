
<style>
    .mendetory {
        color: red;
    }
</style>
<h2><span>Edit Feature</span></h2>
<form method="post"
      action="<?php echo base_url() . 'super_admin/feature/editListener/' . $feature_id; ?>"
      enctype="multipart/form-data" id="1">
    <table width="100%" class="table table-striped table-condensed">
        <tbody>
            <tr>
                <td>Feature Label <span class="mandatory">*</span>
                </td>
                <td><input class="input" type="text" name="feature_label"
                           value="<?php echo htmlentities(set_value('feature_label') != '' ? set_value('feature_label') : (isset($feature->feature_label) ? $feature->feature_label : '' )); ?>" /></td>
                <td><?php echo form_error('feature_label'); ?></td>
            </tr>
            <tr>
                <td>Current Feature Image</td>
                <td><img class="thumbnail pull-left" style="margin-right: 15px;"
                         width=100 height=100
                         src="<?php echo base_url() . substr($feature->logo_upload_path, 2) . "/" . $feature->feature_image; ?>" /></td>
                <td></td>
            </tr>

            <tr>
                <td>New Feature Image</td>
                <td><input type="file" name="feature_image" class="input" /></td>
                <td><?php
if ($this->session->flashdata('file_errors')) {
    $file_errors = $this->session->flashdata('file_errors');
    if (isset($file_errors['feature_image'])) {
        echo $file_errors['feature_image'];
    }
}
?>
                </td>
            </tr>

            <tr>
                <td class="vertical_align">Feature Text<span class="mandatory">*</span></td>

                <td><textarea id="feature_text" name="feature_text" rows="5"
                              class="tinymce span6"><?php echo htmlentities(set_value('feature_text') != '' ? set_value('feature_text') : (isset($feature->feature_text) ? $feature->feature_text : '' )); ?></textarea></td>
                <td><?php echo form_error('feature_text'); ?></td>
            </tr>
            <tr>
                <td colspan="2"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                    &nbsp; &nbsp; &nbsp; &nbsp; <a
                        href="<?php echo base_url() . 'super_admin/admin/feature'; ?>"
                        class="btn btn-large btn-primary">Annuleer</a></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                    zijn verplicht.
                </td>
            </tr>
        </tbody>
    </table>
</form>
<br>

<br>


<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
 
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',
            // General options
        
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,|,bullist,numlist,|,undo,redo,|,link,unlink,image", 
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
        
    });
</script>