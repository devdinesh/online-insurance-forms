<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- date picker start-->
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<!-- date picker end -->

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',

            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,|,bullist,numlist,|,undo,redo,|,link,unlink,image",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>
<!-- /TinyMCE -->


<script type="text/javascript">
    $(function() {
<?php $date = date('m/d/Y', strtotime(get_current_time()->get_date_for_db())); ?>
        $("#publication_date").datepicker({ dateFormat: 'dd-mm-yy',maxDate:<?php echo $date; ?>});
    });
    $(function() {
        var exp_date=document.getElementById("publication_date").value;
<?php $date = date('m/d/Y', strtotime(get_current_time()->get_date_for_db())); ?>
        $("#Expire_date").datepicker({ dateFormat: 'dd-mm-yy', minDate:exp_date});
    });

    $(function() {
<?php $date = date('m/d/Y', strtotime(get_current_time()->get_date_for_db())); ?>
        $("#date").datepicker({ dateFormat: 'dd-mm-yy', minDate:<?php echo $date; ?>});
    });


</script>

<style>
    .mendetory {
        color: red;
    }
</style>
<h2><span>Details nieuws</span></h2>
<form method="post"
      action="<?php echo base_url() . 'super_admin/super_admin_news/edit_listener/' . $news->newz_id; ?>"
      enctype="multipart/form-data" id="1">
    <table width="100%" class="table table-striped table-condensed">
        <tbody>

            <tr>
                <td>Date <span class="mendetory">*</span>
                </td>
                <td><input class="input" type="text" id="date" name="date"
                           value="<?php echo htmlentities((set_value('date') != '') ? set_value('date') : (isset($news->date) ? $news->date : '') ); ?>" /></td>
                <td><?php echo form_error('date'); ?></td>
            </tr>
            <tr>
                <td>Titel <span class="mendetory">*</span>
                </td>
                <td><input class="input" type="text" name="title"
                           value="<?php echo htmlentities((set_value('title') != '') ? set_value('title') : (isset($news->title) ? $news->title : '') ); ?>" /></td>
                <td><?php echo form_error('title'); ?></td>
            </tr>

            <tr>
                <td style="vertical-align: top;">Tekst <span class="mendetory">*</span>
                </td>
                <td><textarea class="tinymce span6" name="newz_text"><?php echo htmlentities((set_value('newz_text') != '') ? set_value('newz_text') : (isset($news->newz_text) ? $news->newz_text : '') ); ?></textarea></td>
                <td><?php echo form_error('newz_text'); ?></td>
            </tr>


            <tr>
                <td>Publicatiedatum <span class="mendetory">*</span>
                </td>
                <td><input class="input" type="text" id="publication_date"
                           name="publication_date"
                           value="<?php echo htmlentities((set_value('publication_date') != '') ? set_value('publication_date') : (isset($news->publication_date) ? $news->publication_date : '') ); ?>" /></td>
                <td><?php echo form_error('publication_date'); ?></td>
            </tr>



            <tr>
                <td>Vervaldatum</td>
                <td><input class="input" type="text" id="Expire_date"
                           name="expire_date"
                           value="<?php echo htmlentities((set_value('expire_date') != '') ? set_value('expire_date') : (isset($news->expire_date) ? $news->expire_date : '') ); ?>" /></td>
                <td><?php echo form_error('expire_date'); ?></td>
            </tr>



            <tr>
                <td colspan="2"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                    &nbsp; &nbsp; &nbsp; &nbsp; <a
                        href="<?php echo base_url() . 'super_admin/admin/news'; ?>"
                        class="btn btn-large btn-primary">Annuleer</a></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3">Velden die gemarkeerd zijn met een <span
                        class="mendetory">*</span> zijn verplicht.
                </td>
            </tr>
        </tbody>
    </table>
</form>
<br>

<br>