<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- date picker start -->
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<!-- date picker end -->

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',

            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,|,bullist,numlist,|,undo,redo,|,link,unlink,image",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>
<!-- /TinyMCE -->
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<div>
    <h2><span>Add Info icons</span></h2>
    <form
        action="<?php echo base_url() . 'super_admin/info_icons/add_listener'; ?>"
        method="post" enctype="multipart/form-data">

        <table width="100%" class="table table-striped table-condensed">
            <tbody>

                <tr>
                    <td>Name <span class="mandatory">*</span>
                    </td>
                    <td><input class="input" type="text" name="info_icon_name"
                               value="<?php echo htmlentities((set_value('info_icon_name') != '') ? set_value('info_icon_name') : '' ); ?>" /></td>
                    <td><?php echo form_error('info_icon_name'); ?></td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">Text <span class="mandatory">*</span>
                    </td>
                    <td><textarea class="tinymce span6" name="info_icon_text"><?php echo htmlentities((set_value('info_icon_text') != '') ? set_value('info_icon_text') : '' ); ?> </textarea>
                    </td>
                    <td><?php echo form_error('info_icon_text'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                        &nbsp;&nbsp; <a
                            href="<?php echo base_url() . 'super_admin/info_icons'; ?>"
                            class="btn btn-large btn-primary">Annuleer</a></td>
                </tr>
                <tr>
                    <td colspan="3">Velden die gemarkeerd zijn met een <span
                            class="mandatory">*</span> zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
