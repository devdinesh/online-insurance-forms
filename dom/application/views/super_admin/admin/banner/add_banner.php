<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.validate.js"></script>
<script>
    //<![CDATA[
    $(document).ready(function(){
        $("#add_content").validate({
            rules: {
                banner_name:{required:true},
                banner_image:{required:true},
                banner_sequence:{required: true}
            },
            messages:{
                banner_name:{required:'Banner Name is Required.'},
                banner_image:{required:'Banner image is Required.'},
                banner_sequence:{required:'Sequence is Required'}
            },
            errorElement: "span"
        });
    });
    //]]>
</script>
<h2><span>Add New Banner</span></h2>
<div>
    <form id="add_content"
          action="<?php echo base_url() . 'super_admin/banner/add_listener'; ?>"
          method="post" enctype="multipart/form-data">

        <table width="100%" class="table table-striped table-condensed">
            <tbody>
                <tr>
                    <td width="200">Name <span class="mandatory">*</span>
                    </td>
                    <td><input class="input span3" type="text"  name="banner_name"
                               value="<?php echo htmlentities((set_value('banner_name') != '') ? set_value('banner_name') : '' ); ?>" /></td>
                    <td><?php echo form_error('banner_name'); ?></td>
                </tr>
                <tr>
                    <td>Image<span class="mandatory">*</span></td>
                    <td><input type="file" name="banner_image" class="input span3" style="padding:0 12px 0  0;" /></td>
                    <td><?php
$error = form_error('banner_image');
if ($this->session->flashdata('file_errors') != '') {
    echo $this->session->flashdata('file_errors');
} else if ($error != '') {
    echo $error;
}
?></td>
                </tr>
                <tr>
                    <td>Banner URL</td>
                    <td><input type="text" class="span3" name="banner_url"
                               value="<?php echo htmlentities(set_value('banner_url') != '' ? set_value('banner_url') : ''); ?>" /></td>
                    <td><?php echo form_error('banner_url'); ?></td>
                </tr>
                <tr>
                    <td>Banner Sequence<span class="mandatory">*</span>
                    </td>
                    <td><input type="text" class="span3" name="banner_sequence"
                               value="<?php echo htmlentities(set_value('banner_sequence') != '' ? set_value('banner_sequence') : ''); ?>" /></td>
                    <td><?php echo form_error('banner_sequence'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                        &nbsp;&nbsp; <a
                            href="<?php echo base_url() . 'super_admin/admin/banner'; ?>"
                            class="btn btn-large btn-primary">Annuleer</a></td>
                </tr>

                <tr>
                    <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                        zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
