
<style>
    .mendetory {
        color: red;
    }
</style>
<h2><span>Edit Banner</span></h2>
<form method="post"
      action="<?php echo base_url() . 'super_admin/banner/editListener/' . $banner_id; ?>"
      enctype="multipart/form-data" id="1">
    <table width="100%" class="table table-striped table-condensed">
        <tbody>
            <tr>
                <td>Name <span class="mendetory">*</span>
                </td>
                <td><input class="input" type="text" name="banner_name"
                           value="<?php echo htmlentities((set_value('banner_name') != '') ? set_value('banner_name') : (isset($banner->banner_name) ? $banner->banner_name : '') ); ?>" /></td>
                <td><?php echo form_error('banner_name'); ?></td>
            </tr>
            <tr>
                <td>Current Banner Image</td>
                <td>
                    <img class="thumbnail pull-left" style="margin-right: 15px;"
                         width=100 height=100
                         src="<?php echo base_url() . substr($banner->logo_upload_path, 2) . "/" . $banner->banner_image; ?>" /></td>
                <td></td>
            </tr>

            <tr>
                <td>New Banner Image</td>
                <td><input type="file" name="banner_image" class="input" /></td>
                <td><?php
if ($this->session->flashdata('file_errors')) {
    $file_errors = $this->session->flashdata('file_errors');
    if (isset($file_errors['banner_image'])) {
        echo $file_errors['banner_image'];
    }
}
?>
                </td>
            </tr>

            <tr>
                <td>Banner URL</td>
                <td><input type="text" name="banner_url"
                           value="<?php echo htmlentities(set_value('banner_url') != '' ? set_value('banner_url') : (isset($banner->banner_url) ? $banner->banner_url : '' )); ?>" /></td>
                <td><?php echo form_error('banner_url'); ?></td>
            </tr>
            <tr>
                <td>Banner Sequence <span class="mendetory">*</span></td>
                <td><input type="text" name="banner_sequence"
                           value="<?php echo htmlentities(set_value('banner_sequence') != '' ? set_value('banner_sequence') : (isset($banner->banner_sequence) ? $banner->banner_sequence : '' )); ?>" /></td>
                <td><?php echo form_error('banner_sequence'); ?></td>
            </tr>


            <tr>
                <td colspan="2"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                    &nbsp; &nbsp; &nbsp; &nbsp; <a
                        href="<?php echo base_url() . 'super_admin/admin/banner'; ?>"
                        class="btn btn-large btn-primary">Annuleer</a></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                    zijn verplicht.
                </td>
            </tr>
        </tbody>
    </table>
</form>
<br>

<br>