<h2><span>Bewerken Pagina's</span></h2>
<form
    action="<?php echo base_url() . 'super_admin/admin/texts/update/' . $texts_id; ?>"
    method="post">
    <table width="100%" class="table table-striped table-condensed">
        <tbody>
            <tr>
                <td>Name</td>
                <td><?php
if (isset($texts_details->text_title)) {
    echo $texts_details->text_title;
}
?></td>
                <td></td>
            </tr>
            <tr>
                <td class="vertical_align">Text<span class="mandatory">*</span></td>
                <td><textarea id="text_content" name="text_content" rows="15"
                              class="tinymce span6">
                                  <?php
                                  if (set_value('text_content') != '') {
                                      $value = set_value('text_content');
                                  } else {
                                      if (isset($texts_details->text_content)) {
                                          $value = $texts_details->text_content;
                                      } else {
                                          $value = '';
                                      }
                                  }
                                  echo htmlentities($value);
                                  ?>
                    </textarea></td>
                <td><?php echo form_error('text_content'); ?></td>
            </tr>
            <tr>
                <td>Page Title<span class="mandatory">*</span>
                </td>
                <td><input class="input" type="text" name="page_title"
                           value="<?php
                                  if (isset($texts_details->page_title) && $texts_details->page_title !== '') {
                                      echo $texts_details->page_title;
                                  } else {
                                      echo htmlentities((set_value('page_title') != '') ? set_value('page_title') : '' );
                                  }
                                  ?>" /></td>
                <td><?php echo form_error('page_title'); ?></td>
            </tr>
            <tr>
                <td>Meta Tags<span class="mandatory">*</span>
                </td>
                <td><input class="input" type="text" name="meta_tags"
                           value="<?php
                           if (isset($texts_details->meta_tags) && $texts_details->meta_tags !== '') {
                               echo $texts_details->meta_tags;
                           } else {
                               echo htmlentities((set_value('meta_tags') != '') ? set_value('meta_tags') : '' );
                           }
                                  ?>" /></td>
                <td><?php echo form_error('meta_tags'); ?></td>
            </tr>
            <tr>
                <td class="vertical_align">Meta Description<span class="mandatory">*</span></td>
                <td><textarea id="meta_desc" name="meta_desc" rows="5" style="width:78%"><?php
                                  if (set_value('meta_desc') != '') {
                                      $value = set_value('meta_desc');
                                  } else {
                                      if (isset($texts_details->meta_desc)) {
                                          $value = $texts_details->meta_desc;
                                      } else {
                                          $value = '';
                                      }
                                  }
                                  echo htmlentities($value);
                                  ?>
                    </textarea></td>
                <td><?php echo form_error('meta_desc'); ?></td>
            </tr>
            <tr>
                <td ></td>
                <td ><div class="checkbox">
                        <input class="input" type="checkbox" name="is_no_index"
                               value="1"
                               <?php
                               if ($texts_details->is_no_index == '1') {
                                   echo 'checked=checked';
                               }
                               ?> />No-index</div></td>
                <td></td>
            </tr>
            <tr>
                <td ></td>
                <td ><div class="checkbox"><input class="input" type="checkbox" name="is_no_follow"
                                                  value="1"
                                                  <?php
                                                  if ($texts_details->is_no_follow == '1') {
                                                      echo 'checked=checked';
                                                  }
                                                  ?> />No-follow</div></td>
                <td></td>
            </tr>
            <tr>
                <td ></td>
                <td ><div class="checkbox"><input class="input" type="checkbox" name="include_sitemap"
                                                  value="1"
                                                  <?php
                                                  if ($texts_details->include_sitemap == '1') {
                                                      echo 'checked=checked';
                                                  }
                                                  ?> />Include in sitemap</div></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>&nbsp;
                    <a href="<?php echo base_url() . 'super_admin/admin/texts'; ?>"
                       class="btn btn-large btn-primary">Annuleer</a></td>
            </tr>
            <tr>
                <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                    zijn verplicht.
                </td>
            </tr>
        </tbody>
    </table>
</form>
<br />
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',

            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,|,bullist,numlist,|,undo,redo,|,link,unlink,image,code",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>
<!-- /TinyMCE -->