<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',

            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,|,bullist,numlist,|,undo,redo,|,link,unlink,image",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>
<!-- /TinyMCE -->
<h2><span>Mail Templates</span></h2>
<?php if ($this->session->flashdata('file_errors') != '') { ?>
    <tr>
        <td>
            <div class="alert alert-error">
                <?php
                $upload_error = $this->session->flashdata('file_errors');
                echo ( $upload_error['logo'] );
                ?>
            </div>
        </td>
    </tr>
<?php } ?>
<form id="edit_email_format" name="edit_email_format"
      action="<?php echo base_url() . 'super_admin/super_admin_mail_template/editMailListener/' . $email_id; ?>"
      method="post" enctype="multipart/form-data">
    <table width="100%" class="table table-striped table-condensed">
        <tr>
            <td>Email type :</td>
            <td><?php
if (isset($email_template->email_type)) {
    echo "<span class='input'>" . $email_template->email_type . "</span>";
}
?></td>
            <td></td>
        </tr>
        <tr>
            <td>Subject :</td>
            <td><input type="text" style="width: 600px;" name="email_subject"
                       class="inpt1"
                       value="<?php
                echo htmlentities(
                        set_value('email_subject') != '' ? set_value('email_subject') : ( isset(
                                        $email_template->email_subject) ? $email_template->email_subject : '' ));
?>" /></td>
            <td><?php echo form_error('email_subject'); ?></td>
        </tr>
        <tr>
            <td>Attachment :</td>
            <td><input type="file" name="email_attachment" /> <br />
                <?php
                if (isset($email_template->email_attachment) &&
                        $email_template->email_attachment !== "") {
                    $full_path = './assets/admin_assets/email_attachments' . "/" .
                            $email_template->email_attachment; // absolute
                    // path
                    // to
                    // be
                    // removed
                    $url = base_url() . "/" . substr($full_path, 2); // remove ./ from
                    // the absolute
                    // location

                    echo "<a href='$url' target='_blank'> Show Attachement</a> &nbsp;&nbsp;&nbsp;&nbsp;";
                    echo "<a href='" . base_url() .
                    "super_admin/super_admin_mail_template/removeAttachment/" .
                    $email_id . "'> Remove Attachment </a>";
                } else {
                    echo "No attachments yet";
                }
                ?>
            </td>
            <td><?php
                if ($this->session->flashdata('file_errors')) {
                    $file_errors = $this->session->flashdata('file_errors');
                    if (isset($file_errors['email_attachment'])) {
                        echo $file_errors['email_attachment'];
                    }
                }
                ?></td>
        </tr>
        <tr>
            <td class="vertical_align">Message :</td>
            <td><textarea id="email_message" name="email_message" rows="15"
                          class="tinymce span6">
                              <?php
                              echo htmlentities(
                                      set_value('email_message') != '' ? set_value(
                                                      'email_message') : isset(
                                                      $email_template->email_message) ? $email_template->email_message : '');
                              ?>
                </textarea></td>
            <td><?php echo form_error('email_message'); ?></td>
        </tr>
        <?php if (isset($email_template->email_dynamic_fields) && $email_template->email_dynamic_fields != NULL) { ?>
            <tr>
                <td class="vertical_align">&nbsp;</td>
                <td><?php echo htmlentities($email_template->email_dynamic_fields); ?></td>
                <td>&nbsp;</td>
            </tr>
        <?php } ?>
        <tr>
            <td></td>
            <td><font color="#666" size="1">
                <?php
                if (isset($email_format_info))
                    echo $email_format_info;
                ?></font></td>
            <td></td>
        </tr>
        <tr>
            <td>BCC to administrator :</td>
            <td colspan="2"><input type="checkbox" name="email_bcc_admin"
                                   value="1"
                                   <?php
                                   if (isset($email_template->email_bcc_admin) &&
                                           $email_template->email_bcc_admin == 1) {
                                       echo 'checked=checked';
                                   }
                                   ?> /></td>
        </tr>
        <tr>
            <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                &nbsp;&nbsp;&nbsp;&nbsp; <a
                    href="<?php echo base_url() . 'super_admin/super_admin_mail_template'; ?>"
                    class="btn btn-large btn-primary">Annuleer</a></td>
        </tr>
    </table>
    <br>
</form>
