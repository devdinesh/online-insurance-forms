
<h2><span>Edit Subscription Plan</span></h2>
<form
    action="<?php echo base_url() . 'super_admin/admin/subscription/update/' . $subscription_id; ?>"
    method="post" enctype="multipart/form-data">
    <table width="100%" class="table table-striped table-condensed">
        <tbody>
            <tr>
                <td width="19%">Active</td>
                <td width="25%"><input type="checkbox" name="status" value="A"
                    <?php
                    if ($subscription->status == 'A') {
                        echo 'checked=checked';
                    }
                    ?> /></td>
                <td></td>
            </tr>
            <tr>
                <td>Title<span class="mandatory">*</span></td>
                <td><input type="text" name="subscription_title"
                           value="<?php
                    echo htmlentities(
                            set_value('subscription_title') != '' ? set_value(
                                            'subscription_title') : isset(
                                            $subscription->subscription_title) ? $subscription->subscription_title : '');
                    ?>" /></td>
                <td><?php echo form_error('subscription_title'); ?></td>
            </tr>

            <tr>
                <td>Maximum No Forms<span class="mandatory">*</span></td>
                <td><input type="number" min="0" name="maximum_forms"
                           value="<?php
                           echo set_value('maximum_forms') != '' ? set_value('maximum_forms') : isset(
                                           $subscription->maximum_forms) ? $subscription->maximum_forms : '';
                    ?>" /></td>
                <td><?php echo form_error('maximum_forms'); ?></td>
            </tr>

            <tr>
                <td>Subscription Fee<span class="mandatory">*</span></td>
                <td><input type="text" name="subscription_fee"
                           value="<?php
                           echo set_value('subscription_fee') != '' ? set_value('subscription_fee') : isset(
                                           $subscription->subscription_fee) ? $subscription->subscription_fee : '';
                    ?>" /></td>
                <td><?php echo form_error('subscription_fee'); ?></td>
            </tr>

            <tr>
                <td>Rate Per Additional Form<span class="mandatory">*</span></td>
                <td><input type="text" name="rate_per_extra_form"
                           value="<?php
                           echo set_value('rate_per_extra_form') != '' ? set_value(
                                           'rate_per_extra_form') : isset($subscription->rate_per_extra_form) ? $subscription->rate_per_extra_form : '';
                    ?>" /></td>
                <td><?php echo form_error('rate_per_extra_form'); ?></td>
            </tr>

            <tr>
                <td>Help Text</td>
                <td class="vertical_align"><textarea id="help_text" name="help_text" rows="15"
                              class="tinymce span6">
                                  <?php
                                  if (set_value('help_text') != '') {
                                      $value = set_value('help_text');
                                  } else {
                                      if (isset($subscription->help_text)) {
                                          $value = $subscription->help_text;
                                      } else {
                                          $value = '';
                                      }
                                  }
                                  echo htmlentities($value);
                                  ?>
                    </textarea></td>
                <td><?php echo form_error('help_text'); ?></td>
            </tr>
                                  <?php if (isset($subscription->plan_image) && $subscription->plan_image != null) { ?>
                <tr>
                    <td>Old Plan Image</td>
                    <td><img class="thumbnail pull-left" style="margin-right: 15px;"
                             width=100 height=100
                             src="<?php echo base_url() . 'assets/plan_images/' . $subscription->plan_image; ?>" />
                        <br /> <a
                            href="<?php echo base_url() . 'super_admin/admin/subscription/removeimage/' . $subscription_id; ?>"><button
                                type="button" class="btn">Remove Image Directly</button></a></td>
                    <td></td>
                </tr>
<?php } ?>
            <tr>
                <td>Plan Image</td>
                <td><input type="file" name="plan_image" /></td>
                <td><?php
if (isset($plan_image_error)) {
    echo $plan_image_error;
}
?></td>
            </tr>

            <tr>
                <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Update</button>
                    &nbsp;&nbsp; <a
                        href="<?php echo base_url() . 'super_admin/admin/subscription'; ?>" class="btn btn-large btn-primary">Cancel</a></td>
            </tr>

            <tr>
                <td colspan="3">Fields marked with <span class="mandatory">*</span>
                    are mandatory.
                </td>
            </tr>
        </tbody>
    </table>
</form>
</div>
<br />
<br />
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("jquery", "1");
</script>

<!-- Load TinyMCE -->
<script type="text/javascript"
src="<?php echo base_url(); ?>assets/jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
    $().ready(function() {
        $('textarea.tinymce').tinymce({
            // Location of TinyMCE script
            script_url : '<?php echo base_url(); ?>assets/jscripts/tiny_mce/tiny_mce.js',

            // General options
            theme : "advanced",
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,|,bullist,numlist,|,undo,redo,|,link,unlink,image",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            force_p_newlines : false,

            // Example content CSS (should be your site CSS)
            content_css : "<?php echo assets_url_css; ?>tinymce_content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    });
</script>