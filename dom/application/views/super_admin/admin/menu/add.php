<h2><span>Add New Feature</span></h2>
<div>
    <form id="add_content"
          action="<?php echo base_url() . 'super_admin/admin/menus/addListener'; ?>"
          method="post" enctype="multipart/form-data">

        <table width="100%" class="table table-striped table-condensed">
            <tbody>

                <tr>
                    <td>Title<span class="mandatory">*</span>
                    </td>
                    <td><input class="input" type="text" name="title"
                               value="<?php echo (set_value('title')); ?>" /></td>
                    <td><?php echo form_error('title'); ?></td>
                </tr>
                <tr>
                    <td>Parent </td>
                    <td><select name="parent" class="input-large">
                            <option value="">Select</option>
                            <?php foreach ($menus as $menu) { ?>
                                <option
                                    value="<?php echo $menu->id; ?>"
                                    <?php
                                    $value = ( set_value('parent') == '' ) ? '' : $value = set_value(
                                            'parent');
                                    if ($value == $menu->id) {
                                        echo "selected=selected";
                                    }
                                    ?>><?php echo $menu->title; ?></option>
                                <?php } ?>
                        </select></td>

                    <td><?php echo form_error('parent'); ?></td>
                </tr>
                <tr>
                    <td>Page <span class="mandatory">*</span></td>
                    <td><select name="page" class="input-large">
                            <option value="">Select</option>
                            <?php foreach ($pages as $page) { ?>
                                <option
                                    value="<?php echo $page->text_id; ?>"
                                    <?php
                                    $value = ( set_value('page') == '' ) ? '' : $value = set_value(
                                            'page');
                                    if ($value == $page->text_id) {
                                        echo "selected=selected";
                                    }
                                    ?>><?php echo $page->text_title; ?></option>
                                <?php } ?>
                        </select></td>

                    <td><?php echo form_error('page'); ?></td>
                </tr>
                <tr>
                    <td>Special feature</td>
                    <td><select name="special_feature" class="input-large">
                            <option value="">Select</option>
                            <option value="Calculator" <?php
                                $value = ( set_value('special_feature') == '' ) ? '' : $value = set_value('special_feature');
                                if ($value == 'Calculator') {
                                    echo "selected=selected";
                                }
                                ?>>Calculator</option>
                            <option value="Contact form" <?php
                                    $value = ( set_value('special_feature') == '' ) ? '' : $value = set_value('special_feature');
                                    if ($value == 'Contact form') {
                                        echo "selected=selected";
                                    }
                                ?>>Contact form</option>

                            <option value="Features" <?php
                                    $value = ( set_value('special_feature') == '' ) ? '' : $value = set_value('special_feature');
                                    if ($value == 'Features') {
                                        echo "selected=selected";
                                    }
                                ?>>Features</option>
                        </select></td>

                    <td><?php echo form_error('special_feature'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                        &nbsp;&nbsp; <a href="<?php echo base_url() . 'super_admin/admin/menus'; ?>"class="btn btn-large btn-primary">Annuleer</a></td>
                </tr>
                <tr>
                    <td colspan="3">Velden gemarkeerd met een <span class="mandatory">*</span>
                        zijn verplicht.
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />

