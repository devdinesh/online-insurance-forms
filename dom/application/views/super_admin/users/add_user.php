<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "super_admin/users" ?>">Users</a></li>
            <li class="active"><?php echo $title; ?></li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">

            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-8">
                <form class="form form-validate" id="addUser" action="<?php echo base_url() . 'super_admin/users/addListener'; ?>"  method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><?php echo $title; ?></header>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" name="status" value="A"  <?php
if (set_value('status') != "" && set_value('status') == 'A') {
    echo 'checked="checked"';
}
?>>
                                            <span>Active</span>
                                        </label>
                                    </div>
                                </div>  
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" name="administrator" value="1"  <?php
                                                   if (set_value('administrator') != "" && set_value('administrator') == '1') {
                                                       echo 'checked="checked"';
                                                   }
?>>
                                            <span>Beheerder</span>
                                        </label>
                                    </div>
                                </div>  
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <select id="client_id" name="client_id" class="form-control" required>
                                               <option value=""></option>
                                            <?php foreach ($client_details as $client) { ?>
                                                <option
                                                    value="<?php echo $client->client_id; ?>"
                                                    <?php
                                                    $value = ( set_value('client_id') == '' ) ? '' : $value = set_value(
                                                            'client_id');
                                                    if ($value == $client->client_id) {
                                                        echo "selected=selected";
                                                    }
                                                    ?>><?php echo $client->client_name; ?></option>
                                                <?php } ?>
                                        </select>
                                        <label for="client_id">Client <span class="required">*</span></label>
                                        <?php echo form_error('client_id'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="first_name" value="<?php echo htmlentities((set_value('first_name') != '') ? set_value('first_name') : '' ); ?>" required/> 
                                        <label for="first_name"> First Name <span class="required">*</span></label>
                                        <?php echo form_error('first_name'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="middle_name" value="<?php echo htmlentities((set_value('middle_name') != '') ? set_value('middle_name') : '' ); ?>"/> 
                                        <label for="middle_name">Middle Name</label>
                                        <?php echo form_error('middle_name'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="last_name" value="<?php echo htmlentities((set_value('last_name') != '') ? set_value('last_name') : '' ); ?>" required/> 
                                        <label for="last_name"> Last Name <span class="required">*</span></label>
                                        <?php echo form_error('last_name'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="email" name="mail_address" value="<?php echo htmlentities((set_value('mail_address') != '') ? set_value('mail_address') : '' ); ?>" required/> 
                                        <label for="mail_address">Mail Address <span class="required">*</span></label>
                                        <?php echo form_error('mail_address'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="password" id="password" name="password" value="<?php echo htmlentities((set_value('password') != '') ? set_value('password') : '' ); ?>"  required/> 
                                        <label for="password">Password <span class="required">*</span></label>
                                        <?php echo form_error('password'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="form-control" type="password" name="password_confirmation" /> 
                                        <label for="password_confirmation">Password Confirmation</label>
                                        <?php echo form_error('password_confirmation'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <select id="role" name="role" class="form-control" required>
                                            <option value=""></option>
                                            <option value="admin"
                                            <?php
                                            ( set_value('role') == '' ) ? '' : $value = set_value('role');
                                            if ($value == 'admin') {
                                                echo "selected=selected";
                                            }
                                            ?>>Administrator</option>
                                            <option value="viewer"
                                            <?php
                                            ( set_value('role') == '' ) ? '' : $value = set_value('role');
                                            if ($value == 'viewer') {
                                                echo "selected=selected";
                                            }
                                            ?>>Viewer</option>
                                        </select>
                                        <label for="client_id">Role <span class="required">*</span></label>
                                        <?php echo form_error('role'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <a class="btn btn-flat" href="<?php echo base_url('super_admin/users'); ?>">Annuleer</a>
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction">Opslaan</button>
                            </div>
                        </div>
                    </div> 
                    <em class="text-caption">Velden die gemarkeerd zijn met een <span class="required">*</span> zijn verplicht.</em>
                </form>
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>

<script type="text/javascript">
    $( "#addUser" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            password_confirmation: {
                equalTo: "#password"
            } 
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#addUser')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#addUser')).hide();
            label.remove();
        }
    });
</script>
