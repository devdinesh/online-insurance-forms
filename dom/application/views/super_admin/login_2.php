<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo $title; ?> | Online Insurance Forms </title>

        <link rel="stylesheet" type="text/css"
              href="<?php echo assets_url_css; ?>base.css" />
        <link rel="stylesheet" type="text/css"
              href="<?php echo base_url(); ?>assets/css/new_css/bootstrap.css" />
        <link rel="stylesheet" type="text/css"
              href="<?php echo assets_url_css; ?>bootstrap-responsive.css" />
        <script src="<?php echo assets_url_js; ?>/jquery-1.7.1.min.js"></script>
        <script src="<?php echo assets_url_js; ?>/bootstrap-dropdown.js"></script>

        <!-- Data table -->
        <link href="<?php echo assets_url_css; ?>demo_table_jui.css"
              rel="stylesheet" type="text/css" />
        <link href="<?php echo assets_url_css; ?>demo_page.css" rel="stylesheet"
              type="text/css" />
        <link href="<?php echo assets_url_css; ?>demo_table.css"
              rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript"
        src="<?php echo assets_url_js; ?>jquery-1.8.3.js"></script>
        <script type="text/javascript" language="javascript"
        src="<?php echo assets_url_js; ?>jquery.dataTables.js"></script>
        <!-- End Data Table -->
        <script type="text/javascript" language="javascript"
        src="<?php echo assets_url_js; ?>bootstrap-tooltip.js"></script>


        <meta name="google-translate-customization"
              content="2227024e11d5699-516aaa854de6b7d6-g515b8caffd13378a-30"></meta>
        <!-- includes for javascript ui -->


        <link href="<?php echo assets_url_css; ?>jquery-ui.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="<?php echo assets_url_js; ?>jquery-ui.js"></script>


        <!-- end includes for javascript ui -->
    </head>



    <body>

        <div id="wrapper" class="container">
            <div class="container">
                <div id="top">
                    <div class="row-fluid ">
                        <div class="span6 ">

                        </div>
                        <div class="span6">
                            <form   action="<?php echo base_url(); ?>super_admin/login?redirect_to=<?php echo $redirect_to; ?>"  method="post">  <input type="hidden" name="redirect_to" value="<?php echo $redirect_to; ?>">
                            </form>
                        </div>
                    </div>
                </div>
                <!-- <div class="navbar-inner container">
                        <div id="navbar-example" class="navbar navbar-static">
                                <div class="navbar-inner">
                                        <div class="" style="width: auto;">
                                                <ul class="nav" role="navigation">
                                                        <li role="presentation"><a role="menuitem" tabindex="0"
                                                                href="<?php echo base_url() . "super_admin/authenticate" ?>">
                                                                        Super Admin Login </a></li>

                                                </ul>
                                        </div>
                                </div>
                        </div>
                </div> -->  
                <div class="container">
                    <div class="row-fluid">
                        <div class="span12 super_admin_contain" id="sup_adm_logo">
                            <a href="#"><img class="" style="margin-top: 100px;"
                                             src="<?php echo assets_url_img; ?>logo_devrepublic.png"
                                             alt="logo" /></a>
                            <form action="<?php echo base_url(); ?>super_admin/login?redirect_to=<?php echo $redirect_to; ?>" class="form-signin" method="post">
                                <input type="hidden" name="redirect_to" value="<?php echo $redirect_to; ?>">
                                <h2 class="form-signin-heading"><span>Super Admin Login</span></h2>


                                <input class="input-block-level " type="text" name="super_admin_email" value="<?php echo htmlentities(set_value('super_admin_email')); ?>">
                                <?php
                                if (form_error('super_admin_email')) {
                                    echo "<span class='super-admin-login-error'>", form_error(
                                            'super_admin_email'), "</span>";
                                }
                                ?>
                                <input type="password" name="super_admin_password" class="input-block-level">

                                <?php
                                if (form_error('super_admin_password')) {
                                    echo "<span class='super-admin-login-error'>", form_error('super_admin_password'), "</span>";
                                }
                                ?>
                                <button class="btn btn-large btn-primary" type="submit">inloggen</button>
                            </form>
                        </div>

                    </div>
                </div>

            </div>


    </body>
</html>
<!--
<html>
    <head>
        <title>
<?php echo $title; ?>
        </title>
    </head>

    <body>
        <form action="<?php echo base_url(); ?>super_admin/login?redirect_to=<?php echo $redirect_to; ?>" method="post">
            <input type="hidden" name="redirect_to" value="<?php echo $redirect_to; ?>">
            <table border="0" width="50%">
                <tr>
                    <td colspan="3"><h3>Super Admin Login</h3></td>
                </tr>

<?php
if (isset($login_error)) {
    echo "<tr><td colspan=\"3\"> " . htmlentities($login_error, ENT_QUOTES) .
    "</td></tr>";
}
?>

                <tr>
                    <td width="10%">Mail :</td>
                    <td width="15%"><input type="email" name="super_admin_email" value="<?php echo htmlentities(set_value('super_admin_email')); ?>" ></td>
                    <td><?php echo form_error('super_admin_email'); ?></td>
                </tr>

                <tr>
                    <td>Password :</td>
                    <td><input type="password" name="super_admin_password"></td>
                    <td><?php echo form_error('super_admin_password'); ?></td>
                </tr>

                <tr>
                    <td colspan="3"> <button type="submit"> Login </button></td>
                </tr>
                <tr>
                    <td colspan="3"><a href="<?php echo base_url(); ?>super_admin/forgotpassword">Forgot Password </a></td>
                </tr>
            </table>
        </form>
    </body>
</html>
-->