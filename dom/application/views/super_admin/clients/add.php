<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "super_admin/clients" ?>">Client</a></li>
            <li class="active"><?php echo $title; ?></li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">

            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-12">
                <form class="form form-validate" id="addClient" action="<?php echo base_url() . 'super_admin/clients/add_listener'; ?>"  method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><?php echo $title; ?></header>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="client_name" value="<?php echo htmlentities((set_value('client_name') != '') ? set_value('client_name') : '' ); ?>" required/> 
                                        <label for="client_name"> Name <span class="required">*</span></label>
                                        <?php echo form_error('client_name'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="address" value="<?php echo htmlentities((set_value('address') != '') ? set_value('address') : '' ); ?>" required/> 
                                        <label for="address"> Address <span class="required">*</span></label>
                                        <?php echo form_error('address'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="zip_code" value="<?php echo htmlentities((set_value('zip_code') != '') ? set_value('zip_code') : '' ); ?>" required/> 
                                        <label for="zip_code">Zip Code <span class="required">*</span></label>
                                        <?php echo form_error('zip_code'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="city" value="<?php echo htmlentities((set_value('city') != '') ? set_value('city') : '' ); ?>" required/> 
                                        <label for="city"> City <span class="required">*</span></label>
                                        <?php echo form_error('city'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php $value = NULL; ?>
                                        <select id="status" name="status" class="form-control">
                                            <option value=""></option>
                                            <option value="Demo" <?php
                                        ( set_value('status') == '' ) ? '' : $value = set_value('status');
                                        if ($value == 'Demo') {
                                            echo "selected=selected";
                                        }
                                        ?>>Demo</option>
                                            <option value="Live"
                                            <?php
                                            ( set_value('status') == '' ) ? '' : $value = set_value('status');
                                            if ($value == 'Live') {
                                                echo "selected=selected";
                                            }
                                            ?>>Live</option>
                                            <option value="Pilot"
                                            <?php
                                            ( set_value('status') == '' ) ? '' : $value = set_value('status');
                                            if ($value == 'Pilot') {
                                                echo "selected=selected";
                                            }
                                            ?>>Pilot</option>
                                        </select>
                                        <label for="status">Status</label>
                                        <?php echo form_error('status'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="file" name="logo" class="margin-top-11"/>
                                        <label for="file">Logo</label>
                                        <span><?php
                                        if (isset($message['data'])) {
                                            echo $message['data'];
                                        }
                                        ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="title_application" value="<?php echo htmlentities((set_value('title_application') != '') ? set_value('title_application') : '' ); ?>" /> 
                                        <label for="title_application">Title Application</label>
                                        <?php echo form_error('title_application'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="path_to_import" value="<?php echo htmlentities((set_value('path_to_import') != '') ? set_value('path_to_import') : '' ); ?>" /> 
                                        <label for="path_to_import">Path To Import</label>
                                        <?php echo form_error('path_to_import'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="current_url" value="<?php echo htmlentities((set_value('current_url') != '') ? set_value('current_url') : '' ); ?>"/> 
                                        <label for="current_url">Current URL </label>
                                        <?php echo form_error('current_url'); ?> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="number"  name="first_remind_mail" min="1" value="<?php echo htmlentities((set_value('first_remind_mail') != '') ? set_value('first_remind_mail') : '' ); ?>" required /> 
                                        <label for="first_remind_mail">First Reminder Mail <span class="required">*</span></label>
                                        <?php echo form_error('first_remind_mail'); ?>
                                        <p  class="help-block">Days</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="number" name="second_remind_mail" min="1" value="<?php echo htmlentities((set_value('second_remind_mail') != '') ? set_value('second_remind_mail') : '' ); ?>" required/> 
                                        <label for="second_remind_mail">Second Reminder Mail <span class="required">*</span></label>
                                        <?php echo form_error('second_remind_mail'); ?>
                                        <p  class="help-block">Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="smtp_server" value="<?php echo htmlentities((set_value('smtp_server') != '') ? set_value('smtp_server') : '' ); ?>" required/> 
                                        <label for="smtp_server">SMTP Server <span class="required">*</span></label>
                                        <?php echo form_error('smtp_server'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="smtp_port" data-rule-digits="true" value="<?php echo htmlentities((set_value('smtp_port') != '') ? set_value('smtp_port') : '' ); ?>" /> 
                                        <label for="smtp_port">SMTP Port</label>
                                        <?php echo form_error('smtp_port'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="email" name="email" value="<?php echo htmlentities((set_value('email') != '') ? set_value('email') : '' ); ?>" required/> 
                                        <label for="email">Afzender Mail Adres <span class="required">*</span></label>
                                        <?php echo form_error('email'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="password" id="password" name="password" value="<?php echo htmlentities((set_value('password') != '') ? set_value('password') : '' ); ?>" /> 
                                        <label for="password">Wachtwoord</label>
                                        <?php echo form_error('password'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="password" name="password_confirmation" /> 
                                        <label for="password">Password Confirmation</label>
                                        <?php echo form_error('password_confirmation'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="file" name="pdf_template" class="margin-top-11" />
                                        <label>PDF sjabloon.</label>
                                        <?php
                                        if ($this->session->flashdata('pdf_template_error')) {
                                            echo $this->session->flashdata('pdf_template_error');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input class="form-control" type="text" name="top_margin" value="<?php echo htmlentities((set_value('top_margin') != '') ? set_value('top_margin') : '' ); ?>" data-rule-number="true" /> 
                                                <label for="top_margin">Bovenmarge</label>
                                                <?php echo form_error('top_margin'); ?>
                                            </div>

                                            <span class="input-group-addon">%</span>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input class="form-control" type="text" name="bottom_margin" value="<?php echo htmlentities((set_value('bottom_margin') != '') ? set_value('bottom_margin') : '' ); ?>"  data-rule-number="true"/> 
                                                <label for="bottom_margin">Ondermarge</label>
                                                <?php echo form_error('bottom_margin'); ?>
                                            </div>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input class="form-control" type="text" name="left_margin" value="<?php echo htmlentities((set_value('left_margin') != '') ? set_value('left_margin') : '' ); ?>" data-rule-number="true" /> 
                                                <label for="left_margin">Linkermarge</label>
                                                <?php echo form_error('left_margin'); ?>
                                            </div>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input class="form-control" type="text" name="right_margin" value="<?php echo htmlentities((set_value('right_margin') != '') ? set_value('right_margin') : '' ); ?>" data-rule-number="true"/> 
                                                <label for="right_margin">Rechtermarge</label>
                                                <?php echo form_error('right_margin'); ?>
                                            </div>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="monthly_fee" value="<?php echo htmlentities((set_value('monthly_fee') != '') ? set_value('monthly_fee') : '' ); ?>" data-rule-number="true"/> 
                                        <label for="monthly_fee">Maandelijkse kosten</label>
                                        <?php echo form_error('monthly_fee'); ?> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="amount_forms_month" value="<?php echo htmlentities((set_value('amount_forms_month') != '') ? set_value('amount_forms_month') : '' ); ?>" data-rule-number="true"/> 
                                        <label for="amount_forms_month">Antal formulieren per maand</label>
                                        <?php echo form_error('amount_forms_month'); ?> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="additional_forms_fee" value="<?php echo htmlentities((set_value('additional_forms_fee') != '') ? set_value('additional_forms_fee') : '' ); ?>" data-rule-number="true"/> 
                                        <label for="additional_forms_fee">Kosten extra formulier</label>
                                        <?php echo form_error('additional_forms_fee'); ?> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="fewer_forms_fee" value="<?php echo htmlentities((set_value('fewer_forms_fee') != '') ? set_value('fewer_forms_fee') : '' ); ?>" data-rule-number="true"/> 
                                        <label for="fewer_forms_fee">Kosten minder formulier</label>
                                        <?php echo form_error('fewer_forms_fee'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" name="enable_newsletters" value="1"  <?php
                                        if (set_value('enable_newsletters') != "" && set_value('enable_newsletters') == 1) {
                                            echo 'checked="checked"';
                                        }
                                        ?>>
                                            <span>Enable newsletters</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="monthly_fee_newsletters" value="<?php echo htmlentities((set_value('monthly_fee_newsletters') != '') ? set_value('monthly_fee_newsletters') : '' ); ?>" data-rule-number="true"/> 
                                        <label for="monthly_fee_newsletters">Monthly fee newsletters</label>
                                        <?php echo form_error('monthly_fee_newsletters'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="fee_per_newsletter" value="<?php echo htmlentities((set_value('fee_per_newsletter') != '') ? set_value('fee_per_newsletter') : '' ); ?>" data-rule-number="true"/> 
                                        <label for="fee_per_newsletter">Fee per newsletter</label>
                                        <?php echo form_error('fee_per_newsletter'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <a class="btn btn-flat" href="<?php echo base_url() . 'super_admin/clients'; ?>">Annuleer</a>
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction">Opslaan</button>
                            </div>
                        </div>
                    </div> 
                    <em class="text-caption">Velden die gemarkeerd zijn met een <span class="required">*</span> zijn verplicht.</em>
                </form>
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>

<script type="text/javascript">
    $( "#addClient" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            password_confirmation: {
                equalTo: "#password"
            },
            logo:{
                extension:"gif|jpg|png"
            },
            pdf_template:{
                extension:"gif|jpg|png"
            }
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#addClient')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#addClient')).hide();
            label.remove();
        }
    });
</script>
