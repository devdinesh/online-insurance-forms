<section>
    <div class="section-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url() . "super_admin/clients" ?>">Client</a></li>
            <li class="active"><?php echo $title; ?></li>
        </ol>
    </div>
    <div class="section-body contain-lg">
        <div class="row">

            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-12">
                <form class="form form-validate" id="editClient" action="<?php echo base_url() . 'super_admin/clients/update/' . $client_id; ?>"  method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="card-head style-primary">
                            <header><?php echo $title; ?></header>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="client_name" value="<?php echo htmlentities((set_value('client_name') != '') ? set_value('client_name') : (isset($client->client_name) ? $client->client_name : '') ); ?>" required/> 
                                        <label for="client_name"> Name <span class="required">*</span></label>
                                        <?php echo form_error('client_name'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="address" value="<?php echo htmlentities(set_value('address') != '' ? set_value('address') : (isset($client->address) ? $client->address : '' )); ?>" required/> 
                                        <label for="address"> Address <span class="required">*</span></label>
                                        <?php echo form_error('address'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="zip_code" value="<?php echo htmlentities(set_value('zip_code') != '' ? set_value('zip_code') : (isset($client->zip_code) ? $client->zip_code : '' )); ?>" required/> 
                                        <label for="zip_code">Zip Code <span class="required">*</span></label>
                                        <?php echo form_error('zip_code'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="city" value="<?php echo htmlentities(set_value('city') != '' ? set_value('city') : (isset($client->city) ? $client->city : '' )); ?>" required/> 
                                        <label for="city"> City <span class="required">*</span></label>
                                        <?php echo form_error('city'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php $value = NULL; ?>
                                        <select id="status" name="status" class="form-control">
                                            <option value=""></option>
                                            <option value="Demo"  <?php
                                        if ($client->status == 'Demo') {
                                            echo "selected=selected";
                                        }
                                        ?> >Demo</option>
                                            <option value="Live"  <?php
                                                    if ($client->status == 'Live') {
                                                        echo "selected=selected";
                                                    }
                                        ?> >Live</option>
                                            <option value="Pilot"  <?php
                                                    if ($client->status == 'Pilot') {
                                                        echo "selected=selected";
                                                    }
                                        ?> >Pilot</option>
                                        </select>
                                        <label for="status">Status</label>
                                        <?php echo form_error('status'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" disabled type="text"  value="<?php echo set_value('registration_date') != '' ? set_value('registration_date') : (isset($client->registration_date) ? $client->registration_date : ''); ?>" /> 
                                        <label for="registration_date">Registered Date</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php if (isset($client->logo) && $client->logo != NULL) { ?>
                                            <img class="thumbnail pull-left margin-top-11" style="margin-right: 15px;"
                                                 width=100 height=100
                                                 src="<?php echo base_url() . substr($client->logo_upload_path, 2) . "/" . $client->logo; ?>" />
                                             <?php } ?>
                                        <label for="current_logo">Current Logo</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="file" name="logo" class="margin-top-11"/>
                                        <label for="file">Logo</label>
                                        <span><?php
                                             if ($this->session->flashdata('file_errors')) {
                                                 $file_errors = $this->session->flashdata('file_errors');
                                                 if (isset($file_errors['logo'])) {
                                                     echo $file_errors['logo'];
                                                 }
                                             }
                                             ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="title_application" value="<?php echo htmlentities(set_value('title_application') != '' ? set_value('title_application') : (isset($client->title_application) ? $client->title_application : '') ); ?>" /> 
                                        <label for="title_application">Title Application</label>
                                        <?php echo form_error('title_application'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="path_to_import" value="<?php echo htmlentities(set_value('path_to_import') != '' ? set_value('path_to_import') : (isset($client->path_to_import) ? $client->path_to_import : '') ); ?>" /> 
                                        <label for="path_to_import">Path To Import</label>
                                        <?php echo form_error('path_to_import'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="current_url" value="<?php echo htmlentities(set_value('current_url') != '' ? set_value('current_url') : (isset($client->current_url) ? $client->current_url : '' )); ?>"/> 
                                        <label for="current_url">Current URL </label>
                                        <?php echo form_error('current_url'); ?> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="number"  name="first_remind_mail" min="1" value="<?php echo htmlentities(set_value('first_remind_mail') != '' ? set_value('first_remind_mail') : (isset($client->first_remind_mail) ? $client->first_remind_mail : '' ) ); ?>" required /> 
                                        <label for="first_remind_mail">First Reminder Mail <span class="required">*</span></label>
                                        <?php echo form_error('first_remind_mail'); ?>
                                        <p  class="help-block">Days</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="number" name="second_remind_mail" min="1" value="<?php echo htmlentities(set_value('second_remind_mail') != '' ? set_value('second_remind_mail') : (isset($client->second_remind_mail) ? $client->second_remind_mail : '' )); ?>" required/> 
                                        <label for="second_remind_mail">Second Reminder Mail <span class="required">*</span></label>
                                        <?php echo form_error('second_remind_mail'); ?>
                                        <p  class="help-block">Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="smtp_server" value="<?php echo htmlentities(set_value('smtp_server') != '' ? set_value('smtp_server') : (isset($admin->smtp_server) ? $admin->smtp_server : '' )); ?>" required/> 
                                        <label for="smtp_server">SMTP Server <span class="required">*</span></label>
                                        <?php echo form_error('smtp_server'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="smtp_port" data-rule-digits="true" value="<?php echo htmlentities(set_value('smtp_port') != '' ? set_value('smtp_port') : (isset($admin->smtp_port) ? $admin->smtp_port : '' )); ?>" /> 
                                        <label for="smtp_port">SMTP Port</label>
                                        <?php echo form_error('smtp_port'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="email" name="email" value="<?php echo htmlentities(set_value('email') != '' ? set_value('email') : (isset($admin->email) ? $admin->email : '' )); ?>" required/> 
                                        <label for="email">Afzender Mail Adres <span class="required">*</span></label>
                                        <?php echo form_error('email'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="password" id="password" name="password" value="<?php echo htmlentities((set_value('password') != '') ? set_value('password') : '' ); ?>" /> 
                                        <label for="password">Wachtwoord</label>
                                        <?php echo form_error('password'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="password" name="password_confirmation" /> 
                                        <label for="password">Password Confirmation</label>
                                        <?php echo form_error('password_confirmation'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" name="automatic_import" value="1"   <?php
                                        if (@$admin->automatic_import == '1') {
                                            echo 'checked=checked';
                                        }
                                        ?>>
                                            <span>Automatische import</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <select id="status" name="main_user" class="form-control">
                                            <option value=""></option>
                                            <?php
                                            if (!empty($user_records)) {
                                                foreach ($user_records as $user) {
                                                    ?>
                                                    <option value="<?php echo $user->user_id; ?>"
                                                    <?php
                                                    if ($user->user_id == @$admin->main_user) {
                                                        echo "selected=selected";
                                                    }
                                                    ?>><?php echo $user->first_name . ' ' . $user->last_name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                        </select>
                                        <label for="status">Main User</label>
                                        <?php echo form_error('main_user'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php if (isset($admin->pdf_template) && ($admin->pdf_template !== "")) { ?>
                                        <a
                                                href="<?php echo base_url() . substr($admin->pdf_template_upload_path, 2) . "/" . $admin->pdf_template; ?>"
                                                target="_blank"> <img class="thumbnail pull-left margin-top-11"
                                                                  style="margin-right: 15px;" width=100 height=100
                                                                  src="<?php echo base_url() . substr($admin->pdf_template_upload_path, 2) . "/" . $admin->pdf_template; ?>" />
                                            </a>
                                        <?php } ?>
                                        <label for="current_pdf_template">Huidige PDF sjabloon</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="file" name="pdf_template" class="margin-top-11" />
                                        <label>PDF sjabloon.</label>
                                        <?php
                                        if ($this->session->flashdata('file_errors')) {
                                            $file_errors = $this->session->flashdata('file_errors');
                                            if (isset($file_errors['pdf_template'])) {
                                                echo $file_errors['pdf_template'];
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input class="form-control" type="text" name="top_margin" value="<?php echo htmlentities((set_value('top_margin') != '') ? set_value('top_margin') : (isset($admin->top_margin) ? $admin->top_margin : '') ); ?>" data-rule-number="true" /> 
                                                <label for="top_margin">Bovenmarge</label>
                                                <?php echo form_error('top_margin'); ?>
                                            </div>

                                            <span class="input-group-addon">%</span>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input class="form-control" type="text" name="bottom_margin" value="<?php echo htmlentities((set_value('bottom_margin') != '') ? set_value('bottom_margin') : (isset($admin->bottom_margin) ? $admin->bottom_margin : '') ); ?>"  data-rule-number="true"/> 
                                                <label for="bottom_margin">Ondermarge</label>
                                                <?php echo form_error('bottom_margin'); ?>
                                            </div>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input class="form-control" type="text" name="left_margin" value="<?php echo htmlentities((set_value('left_margin') != '') ? set_value('left_margin') : (isset($admin->left_margin) ? $admin->left_margin : '') ); ?>" data-rule-number="true" /> 
                                                <label for="left_margin">Linkermarge</label>
                                                <?php echo form_error('left_margin'); ?>
                                            </div>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input class="form-control" type="text" name="right_margin" value="<?php echo htmlentities((set_value('right_margin') != '') ? set_value('right_margin') : (isset($admin->right_margin) ? $admin->left_margin : '') ); ?>" data-rule-number="true"/> 
                                                <label for="right_margin">Rechtermarge</label>
                                                <?php echo form_error('right_margin'); ?>
                                            </div>
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="monthly_fee" value="<?php echo htmlentities((set_value('monthly_fee') != '') ? set_value('monthly_fee') : (isset($client->monthly_fee) ? $client->monthly_fee : '') ); ?>" data-rule-number="true"/> 
                                        <label for="monthly_fee">Maandelijkse kosten</label>
                                        <?php echo form_error('monthly_fee'); ?> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="amount_forms_month" value="<?php echo htmlentities((set_value('amount_forms_month') != '') ? set_value('amount_forms_month') : (isset($client->amount_forms_month) ? $client->amount_forms_month : '') ); ?>" data-rule-number="true"/> 
                                        <label for="amount_forms_month">Antal formulieren per maand</label>
                                        <?php echo form_error('amount_forms_month'); ?> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="additional_forms_fee" value="<?php echo htmlentities((set_value('additional_forms_fee') != '') ? set_value('additional_forms_fee') : (isset($client->additional_forms_fee) ? $client->additional_forms_fee : '') ); ?>" data-rule-number="true"/> 
                                        <label for="additional_forms_fee">Kosten extra formulier</label>
                                        <?php echo form_error('additional_forms_fee'); ?> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="fewer_forms_fee" value="<?php echo htmlentities((set_value('fewer_forms_fee') != '') ? set_value('fewer_forms_fee') : (isset($client->fewer_forms_fee) ? $client->fewer_forms_fee : '') ); ?>" data-rule-number="true"/> 
                                        <label for="fewer_forms_fee">Kosten minder formulier</label>
                                        <?php echo form_error('fewer_forms_fee'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" value="1" name="enable_newsletters" <?php
                    if ((set_value('enable_newsletters') !== "" && set_value('enable_newsletters') == 1 ) || isset($client->enable_newsletters) && $client->enable_newsletters == 1) {
                        echo 'checked="checked"';
                    }
                ?>>
                                            <span>Enable newsletters</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="monthly_fee_newsletters" value="<?php echo htmlentities((set_value('monthly_fee_newsletters') != '') ? set_value('monthly_fee_newsletters') : (isset($client->monthly_fee_newsletters) ? $client->monthly_fee_newsletters : '') ); ?>" data-rule-number="true"/> 
                                        <label for="monthly_fee_newsletters">Monthly fee newsletters</label>
                                        <?php echo form_error('monthly_fee_newsletters'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="fee_per_newsletter" value="<?php echo htmlentities((set_value('fee_per_newsletter') != '') ? set_value('fee_per_newsletter') : (isset($client->fee_per_newsletter) ? $client->fee_per_newsletter : '') ); ?>" data-rule-number="true"/> 
                                        <label for="fee_per_newsletter">Fee per newsletter</label>
                                        <?php echo form_error('fee_per_newsletter'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <a class="btn btn-flat" href="<?php echo base_url() . 'super_admin/clients'; ?>">Annuleer</a>
                                <button type="submit" class="btn btn-flat btn-primary ink-reaction">Opslaan</button>
                            </div>
                        </div>
                    </div> 
                    <em class="text-caption">Velden die gemarkeerd zijn met een <span class="required">*</span> zijn verplicht.</em>
                </form>
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>

<script type="text/javascript">
    $( "#editClient" ).validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        rules: {
            password_confirmation: {
                equalTo: "#password"
            },
            logo:{
                extension:"gif|jpg|png"
            },
            pdf_template:{
                extension:"gif|jpg|png"
            }
        },
        highlight: function(element) { // hightlight error inputs
            $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
            $('.alert-danger', $('#editClient')).show();
        },
        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            $('.alert-danger', $('#editClient')).hide();
            label.remove();
        }
    });
</script>
