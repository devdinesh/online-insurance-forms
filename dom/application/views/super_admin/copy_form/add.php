<div>
    <h2><span>Copy form</span></h2>
    <form  action="<?php echo base_url() . 'super_admin/copy_client_form/copy_form'; ?>"  method="post" enctype="multipart/form-data">
        <table width="100%" class="table table-striped table-condensed">
            <tbody>
                <tr>
                    <td>Client <span class="mandatory">*</span></td>
                    <td>
                        <select name="client_id">
                            <option value="">select client</option>
                            <?php
                            foreach ($clients as $client) {
                                ?>
                                <option value="<?php echo $client->client_id; ?>"><?php echo $client->client_name; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                    <td><?php echo form_error('client_id'); ?></td>
                </tr>
                <tr>
                    <td colspan="3"><button type="submit" class="btn btn-large btn-primary">Opslaan</button>
                        &nbsp;&nbsp; <a
                            href="<?php echo base_url() . 'super_admin/dashboard'; ?>"
                            class="btn btn-large btn-primary">Annuleer</a></td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<br />
<br />
