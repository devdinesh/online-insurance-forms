<?php

if (!function_exists('total_time'))
{
    /*
     * calculate the total time form given time range
     */
    function total_time ( $date, $start_time, $end_time = '' )
    {
        $ci = get_instance();
        
        // if end time is not give take the curent time
        if ($date == get_current_time()->get_date_for_db() && $end_time == '')
        {
            $end_time = get_current_time()->get_time_for_db();
        }
        
        $start = strtotime($date . " " . $start_time);
        $stop = strtotime($date . " " . $end_time);
        $difference = $stop - $start;
        $hours = floor($difference / ( 60 * 60 ));
        $minutes = round(( $difference % ( 60 * 60 ) ) / 60);
        $time['hours'] = $hours;
        $time['minutes'] = $minutes;
        $currunt_date = get_current_time()->get_date_for_db();
        if ($currunt_date != $date)
        {
            if ($hours < 0.0 || $minutes < 0.0)
            {
                $time['hours'] = 0;
                $time['minutes'] = 0;
            }
        }
        return $time;
    
    }
}

if (!function_exists('time_employee_worked_before_lunch'))
{
    /*
     * return the total time employee worked before lunch
     */
    function time_employee_worked_before_lunch ( $employee_id, $date )
    {
        $ci = get_instance();
        
        // returns the data whose emp id is passesd with date
        $check_data = $ci->employee_time_table->select_single_record(
                $employee_id, $date);
        
        $time = array();
        
        if (is_array($check_data))
        {
            
            if ($check_data[0]->check_out_before_lunch == NULL)
            {
                $t1 = total_time($check_data[0]->today_date, 
                        $check_data[0]->check_in_start_day);
            }
            else
            {
                $t1 = total_time($check_data[0]->today_date, 
                        $check_data[0]->check_in_start_day, 
                        $check_data[0]->check_out_before_lunch);
            }
            $time['hours'] = $t1['hours'];
            $time['minutes'] = $t1['minutes'];
        }
        
        return $time;
    
    }
}

if (!function_exists('time_employee_worked_after_lunch'))
{
    /*
     * return the total time employee worked after lunch
     */
    function time_employee_worked_after_lunch ( $employee_id, $date )
    {
        $ci = get_instance();
        
        // returns the data whose emp id is passesd with date
        $check_data = $ci->employee_time_table->select_single_record(
                $employee_id, $date);
        
        $time = array();
        
        if (is_array($check_data))
        {
            
            if ($check_data[0]->check_in_after_lunch != NULL &&
                     $check_data[0]->check_out_end_day == NULL)
            {
                $t2 = total_time($check_data[0]->today_date, 
                        $check_data[0]->check_in_after_lunch);
            }
            else if ($check_data[0]->check_out_end_day != NULL)
            {
                $t2 = total_time($check_data[0]->today_date, 
                        $check_data[0]->check_in_after_lunch, 
                        $check_data[0]->check_out_end_day);
            }
            else
            {
                $t2['hours'] = 0;
                $t2['minutes'] = 0;
            }
            $time['hours'] = $t2['hours'];
            $time['minutes'] = $t2['minutes'];
        }
        else
        {
            $time['hours'] = 0;
            $time['minutes'] = 0;
        }
        return $time;
    
    }
}

if (!function_exists('get_total_time_worked'))
{
    /*
     * gives the total time worked through out the day.
     */
    function get_total_time_worked ( $employee_id, $date )
    {
        $time = array();
        
        $date_before_lunch = time_employee_worked_before_lunch($employee_id, 
                $date);
        $date_after_lunch = time_employee_worked_after_lunch($employee_id, 
                $date);
        
        if (count($date_before_lunch) != 0)
        {
            $total_hours = $date_before_lunch['hours'] +
                     $date_after_lunch['hours'];
        }
        else
        {
            $total_hours = 0;
        }
        
        if (count($date_before_lunch) != 0)
        {
            $total_min = $date_before_lunch['minutes'] +
                     $date_after_lunch['minutes'];
            if ($total_min > 59)
            {
                $total_hours = $total_hours + 1;
                $total_min = $total_min - 60;
            }
        }
        else
        {
            $total_min = 0;
        }
        
        $time['hours'] = $total_hours;
        $time['minutes'] = $total_min;
        return $time;
    
    }
}

if (!function_exists('get_time_zone'))
{

    function get_time_zone ()
    {
        // $arr = get_instance()->session->userdata[0]->time_zone ;
        $timeZone = get_time_zone_for_employee(
                get_instance()->session->userdata('employeeid'));
        // $timeZone = 'UP55';
        return $timeZone;
    
    }
}

if (!function_exists('get_time_zone_for_employee'))
{

    function get_time_zone_for_employee ( $id = '' )
    {
        $time_zone = '';
        if ($id != '')
        {
            $query = get_instance()->db->query(
                    "SELECT time_zone FROM em_clients c, em_employees e WHERE c.client_id = e.client_id AND e.employeeid =$id");
            $row = $query->result();
            $time_zone = $row[0]->time_zone;
        }
        else
        {
            $time_zone = get_instance()->session->userdata[0]->time_zone;
        }
        return $time_zone;
    
    }
}

if (!function_exists('get_daylight_setting'))
{

    function get_daylight_setting ()
    {
        // $arr = get_instance()->session->userdata[0]->time_zone ;
        $daylight = get_daylight_setting_for_employee(
                get_instance()->session->userdata('employeeid'));
        // $timeZone = 'UP55';
        return $daylight;
    
    }
}

if (!function_exists('get_daylight_setting_for_employee'))
{

    function get_daylight_setting_for_employee ( $id = '' )
    {
        $daylight = '';
        if ($id != '')
        {
            $query = get_instance()->db->query(
                    "SELECT daylight_setting FROM em_clients c, em_employees e WHERE c.client_id = e.client_id AND e.employeeid =$id");
            $row = $query->result();
            $daylight = $row[0]->daylight_setting;
        }
        else
        {
            $daylight = get_instance()->session->userdata[0]->daylight_setting;
        }
        return $daylight;
    
    }
}

if (!function_exists('get_current_time'))
{
    /*
     * gives the total time worked through out the day.
     */
    class MyTime
    {

        var $day;

        var $month;

        var $year;

        var $hour;

        var $minute;

        var $second;

        function get_time_for_db ()
        {
            return $this->hour . ':' . $this->minute . ':' . $this->second;
        
        }

        function get_date_for_db ()
        {
            return $this->year . '-' . $this->month . '-' . $this->day;
        
        }

        function get_date_time_for_db ()
        {
            return $this->get_date_for_db() . ' ' . $this->get_time_for_db();
        
        }

        function get_date_time ()
        {
            return $this->day . "-" . $this->month . "-" . $this->year . " " .
                     $this->hour . ":" . $this->minute . ":" . $this->second;
        
        }
    
    }

    function get_current_time ()
    {
        $time = array();
        date_default_timezone_set('GMT');
        $timezone = 'GMT';
        $daylight = '2';
        $timestamp = strtotime($daylight . ' hour', mktime());
        
        $mytime = new MyTime();
        
        $mytime->day = date('d', gmt_to_local($timestamp, $timezone, TRUE));
        $mytime->month = date('m', gmt_to_local($timestamp, $timezone, TRUE));
        $mytime->year = date('Y', gmt_to_local($timestamp, $timezone, TRUE));
        $mytime->hour = date('H', gmt_to_local($timestamp, $timezone, TRUE));
        $mytime->minute = date('i', gmt_to_local($timestamp, $timezone, TRUE));
        $mytime->second = date('s', gmt_to_local($timestamp, $timezone, TRUE));
        
        return $mytime;
    
    }
}

?>