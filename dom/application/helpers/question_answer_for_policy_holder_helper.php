<?php

function get_answer_given_by_policy_holder($claim_id, $question_id) {
    $ci = & get_instance();
    $ci->load->model('forms_answers_model');
    $forms_answers_model = new forms_answers_model();
    $answers = $forms_answers_model->get_where(
            array('claim_id' => $claim_id, 'question_id' => $question_id
            ));
    if (isset($answers[0])) {
        return $answers[0];
    }
    // return "asdf ";
}

function get_answer_given_by_admin($claim_id, $question_id) {
    $ci = & get_instance();
    $ci->load->model('temp_forms_answers_model');
    $forms_answers_model = new temp_forms_answers_model();
    $answers = $forms_answers_model->get_where(
            array('claim_id' => $claim_id, 'question_id' => $question_id
            ));
    if (isset($answers[0])) {
        return $answers[0];
    }
    // return "asdf ";
}

function get_category_introduction_text($claim_id, $cat_id) {
    $ci = & get_instance();
    $ci->load->model('forms_categories_model');
    $ci->load->model('form_field_values_model');
    $cat_info = $ci->forms_categories_model->get_where(array('cat_id' => $cat_id));
    $new_text = $cat_info[0]->introduction_text;
    $field_info = $ci->form_field_values_model->get_where(
            array('claim_id' => $claim_id
            ));
    $get_filed = $ci->_extract_form_tag($new_text);

    $name = array();

    foreach ($field_info as $filed) {
        foreach ($get_filed[0] as $x) {
            $get_field_name = str_replace('&gt;', '', str_replace('&lt;', '', $x));
            if (strtolower($filed->name) == strtolower($get_field_name)) {
                $new_text = str_replace($x, $filed->filed_value, $new_text);
            }
        }
    }
    return str_replace('&gt;', '', str_replace('&lt;', '', $new_text));
}

function _extract_form_tag($content) {
// echo $content;
    $matches = array();
    $regx = '/\&lt;(.*?)\&gt;/';
    preg_match_all($regx, $content, $matches);
    return $matches;
}

function modifyText($string) {
    $temp = substr($string, 3, count($string) - 5);
    return html_entity_decode(htmlentities($temp));
}

function convert_eng_dutch($val) {
    $number = '';
    if ($val != '') {
        $part1 = explode(".", $val);
        if (count($part1) == 2) {
            $num1 = $part1[0];
            $arr1 = str_split(strrev($num1), 3);
            $num1 = implode(".", $arr1);
            $number = strrev($num1) . "," . $part1[1];
        } else {
            $num1 = $part1[0];
            $arr1 = str_split(strrev($num1), 3);
            $num1 = implode(".", $arr1);
            $number = strrev($num1);
        }
    }
    return $number;
}

function convert_dutch_eng($val) {
    $number = '';
    if ($val != '') {
        //var_dump($val);
        $part1 = explode(",", $val);
        if (count($part1) == 2) {
            $num1 = $part1[0];
            $num1 = str_replace(".", "", $num1);
            $number = $num1 . "." . $part1[1];
        } else {
            $num1 = $part1[0];
            $num1 = str_replace(".", "", $num1);
            $number = $num1;
        }
    }
    return $number;
}

function get_form_all_text($claim_id, $new_text) {

    $ci = & get_instance();
    $ci->load->model('form_field_values_model');
    $field_info = $ci->form_field_values_model->get_where(
            array('claim_id' => $claim_id
            ));
    $get_filed = $ci->_extract_form_tag($new_text);
    $name = array();
    foreach ($field_info as $filed) {
        foreach ($get_filed[0] as $x) {
            $get_field_name = str_replace('&gt;', '', str_replace('&lt;', '', $x));
            if (strtolower($filed->name) == strtolower($get_field_name)) {
                $new_text = str_replace($x, $filed->filed_value, $new_text);
            }
        }
    }
    return str_replace('&gt;', '', str_replace('&lt;', '', $new_text));
}

function get_reminder_mail_info($form_id) {
    $reminder_info = array();
    $ci = & get_instance();
    $ci->load->model('forms_model');
    $form_info = $ci->forms_model->get_where(array('form_id' => $form_id));
    $reminder_info['first_remind'] = $form_info[0]->first_remind;
    $reminder_info['second_remind'] = $form_info[0]->second_remind;
    $reminder_info['f_reminder_text'] = $form_info[0]->f_reminder_text;
    $reminder_info['s_reminder_text'] = $form_info[0]->s_reminder_text;
    return $reminder_info;
}

function get_ftp_info($id = 1) {
    $ftp_info = array();
    $ci = & get_instance();
    $ci->load->model('super_admin_setting_model');
    $result = $ci->super_admin_setting_model->get_where(array('id' => $id));
    $ftp_info['ftp_host'] = $result[0]->ftp_host;
    $ftp_info['ftp_username'] = $result[0]->ftp_username;
    $ftp_info['ftp_password'] = $result[0]->ftp_password;
    $ftp_info['ftp_path'] = $result[0]->ftp_path;
    return $ftp_info;
}

function show_contact_form() {
    $ci = & get_instance();
    return $ci->load->view('admin/common_contact_form', $data = NULL, true);
}

function get_menus() {
    $ci = & get_instance();
    $ci->load->model('menu_model');
    $getMenus = $ci->menu_model->getmenus();
    $result = array();

    if (!empty($getMenus)) {

        foreach ($getMenus as $getMenu) {
            if ($getMenu->parent != NULL) {
                $result[$getMenu->parent]['sub'][] = $getMenu;
            } else {
                $result[$getMenu->id]['id'] = $getMenu->id;
                $result[$getMenu->id]['title'] = $getMenu->title;
                $result[$getMenu->id]['text_title'] = $getMenu->text_title;
                $result[$getMenu->id]['page_slug'] = $getMenu->page_slug;
                $result[$getMenu->id]['sub'] = array();
            }
        }
    }
    return $result;
}

function GetMailTemplate() {
    $get_client_id = admin_get_client_id();
    $client_id = $get_client_id->client_id;
    $ci = & get_instance();
    $ci->load->model('client_email_template_model');
    $result = $ci->client_email_template_model->get_where(array('client_id' => $client_id, 'manageable' => 1));
    return $result;
}

function GetFontType() {
    $get_client_id = admin_get_client_id();
    $client_id = $get_client_id->client_id;
    $ci = & get_instance();
    $ci->load->model('client_setting_model');
    $result = $ci->client_setting_model->get_where(array('client_id' => $client_id));
    $fonttype = array();
    if ($result[0]->letter_type) {
        $fonttype['font-family'] = $result[0]->letter_type;
    } else {
        $fonttype['font-family'] = '"Open Sans",sans-serif';
    }
    $fonttype['fontsize_h1'] = $result[0]->fontsize_h1;
    $fonttype['fontsize_h2'] = $result[0]->fontsize_h2;
    $fonttype['fontsize_h3'] = $result[0]->fontsize_h3;
    $fonttype['fontsize_normal'] = $result[0]->fontsize_normal;
    return $fonttype;
}

function GetMenus() {
    $array_menu = array();
    $array_menu['show_dashbord'] = TRUE;
    $array_menu['show_relations'] = TRUE;
    $array_menu['show_forms'] = TRUE;
    $array_menu['show_own_forms'] = TRUE;
    $array_menu['show_news'] = TRUE;
    $array_menu['show_statistics'] = TRUE;
    $array_menu['show_newsletters'] = TRUE;
    $array_menu['show_other'] = TRUE;
    $array_menu['show_user_forms'] = FALSE;
    $ci = & get_instance();
    $user_info = $ci->session->userdata('admin');

    $role_menu = $user_info['role_menu'];
    $client_id = $user_info['client_id'];

    if (isset($role_menu) && $role_menu != NULL) {
        $ci->load->model('user_roles_model');
        $result = $ci->user_roles_model->get_where(array('client_id' => $client_id, 'id' => $role_menu));
        if (!empty($result)) {
            $array_menu['show_dashbord'] = $result[0]->show_dashbord == 1 ? TRUE : FALSE;
            $array_menu['show_relations'] = $result[0]->show_relations == 1 ? TRUE : FALSE;
            $array_menu['show_forms'] = $result[0]->show_forms == 1 ? TRUE : FALSE;
            $array_menu['show_own_forms'] = $result[0]->show_own_forms == 1 ? TRUE : FALSE;
            $array_menu['show_news'] = $result[0]->show_news == 1 ? TRUE : FALSE;
            $array_menu['show_statistics'] = $result[0]->show_statistics == 1 ? TRUE : FALSE;
            $array_menu['show_newsletters'] = $result[0]->show_newsletters == 1 ? TRUE : FALSE;
            $array_menu['show_other'] = $result[0]->show_other == 1 ? TRUE : FALSE;
            $array_menu['show_user_forms'] = $result[0]->show_user_forms == 1 ? TRUE : FALSE;
        }
    }

    $client_info = admin_get_client_id();
    if (isset($client_info->enable_newsletters) && $client_info->enable_newsletters == 1) {
        $array_menu['show_newsletters'] = TRUE;
    } else {
        $array_menu['show_newsletters'] = FALSE;
    }
    return $array_menu;
}