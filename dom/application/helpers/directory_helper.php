<?php

if (! function_exists('check_or_create_admin_isurance_form_location'))
{

    function check_or_create_admin_isurance_form_location ( $path_to_import )
    {
        $path_to_import = 'assets/admin_assets/import_files/' . $path_to_import;
        if (! is_dir($path_to_import))
        {
            $oldmask = umask(0);
            mkdir($path_to_import, 0777);
            umask($oldmask);
        }
        return is_dir($path_to_import);
    }
}