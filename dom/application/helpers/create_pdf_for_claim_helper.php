<?php

if (!function_exists('create_pdf'))
{

    /**
     * This is helper for creating the pdf files related to the perticular
     * claims.
     * This method just creates the pdf file with the name of claim_id.pdf and
     * saves
     * it to the directory "assets/claims_pdf_files"
     * 
     * @param int $claim_id
     *            is the id of claim for which we want to generate the pdf
     * @return void
     */
    function create_pdf ( $claim_id )
    {
        $ci = & get_instance();
        $ci->load->model('claims_model');
        $claims_model = new claims_model();
        $claims = $claims_model->get_where(
                array('claim_id' => $claim_id
                ));
        $claim = $claims[0];
        // load the helper for get the libaray files
        $ci->load->helper('pdf/generate_pdf');
        // it include the lib needed to generate the PDF files
        include_lib_pdf();
        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(0, 7, 'Information About The Claim', 0, 1, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial', '', 14);
        $pdf->Cell(70, 7, ' ' . ' Answers for', 0, 0, 'L');
        $pdf->Ln();
        $pdf->Cell(70, 7, ' ' . ' Claim for policy number:', 0, 0, 'L');
        $pdf->Cell(0, 7, '' . strip_tags($claim->policy_number), 0, 1, 'L');
        // $pdf->Ln();
        /*
         * this medthod makes the cell for table @param1 : width of cell @param2
         * : height of cell @param3 : content of cell @param4 : border of cell
         * (1 = YES | 0 = NO) @param5 : move to next line (1 = YES | 0 = NO)
         * @param6 : aligment of text (L= Left | C = CENTER | R = RIGHT)
         */
        $pdf->SetFont('Arial', 'B', 14);
        $pdf->Cell(70, 20, ' ' . 'Questions', 0, 0, 'L');
        $pdf->Cell(70, 20, 'Answers', 0, 1, 'L');
        $pdf->SetFont('Arial', '', 14);
        $answers = $claim->get_answers();
        if (is_array($answers))
        {
            foreach ($answers as $answer)
            {
                $question = $answer->get_question();
                $pdf->Cell(70, 8, '' . strip_tags($question->question), 0, 0, 
                        'L');
                
                if ($answer->answer_type == 'text' ||
                         $answer->answer_type == 'textarea')
                {
                    $pdf->Cell(0, 8, '' . strip_tags($answer->answer_text), 0, 
                            1, 'L');
                }
                else
                {
                    $choices = $answer->get_answer_choices();
                    
                    $choice_count = 0;
                    if (is_array($choices) && count($choices) > 0)
                    {
                        foreach ($choices as $choice)
                        {
                            $choice_count++;
                            $pdf->Cell(70, 8, 
                                    ' ' .
                                             strip_tags(
                                                    $choice->get_selected_item_details()->answer), 
                                            0, 1, 'L');
                            
                            if (count($choices) != $choice_count)
                            {
                                $pdf->Cell(70, 8, '', 0, 0, 'L');
                            }
                        }
                    }
                    else
                    {
                        $pdf->Cell(70, 8, '', 0, 1, 'L');
                    }
                }
            }
        }
        $path = realpath('assets/claims_pdf_files');
        $pdf->Output($path . '/' . $claim_id . '.pdf', 'F');
    
    }
}
