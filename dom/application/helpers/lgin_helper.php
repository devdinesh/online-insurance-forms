<?php

if (!function_exists('is_logged_in')) {

    /**
     *
     * @return type Boolean true if user is logged in
     */
    function is_logged_in() {
        $ci = & get_instance();
        return $ci->session->userdata('admin') || $ci->session->userdata('super_admin');
    }

}

if (!function_exists('admin_get_client_id')) {

    /**
     *
     * @return int returns the client object associated with currunt admin
     */
    function admin_get_client_id() {
        $ci = & get_instance();
        $admin_array = $ci->session->userdata('admin');
        if (!empty($admin_array)) {
            $ci->load->model('users_model');
            $user_id = $admin_array['user_id'];
            $users = $ci->users_model->get_where(array('user_id' => $user_id));
            $user = $users[0];
            $client = $user->get_client();
        } else {
            $ci->load->model('clients_model');
            $policy_holder_info = $ci->session->userdata('policy_holder');
            $client_id = $policy_holder_info->client_id;
            $clients = $ci->clients_model->get_where(array('client_id' => $client_id));
            $client = $clients[0];
        }
        return $client;
    }

    /**
     * returns the object of policy_holder_model from the session
     * thas works only if the user is logged in
     *
     * @return policy_holder_model
     */
    function user_get_policy_holder() {
        $ci = & get_instance();
        $ci->load->model('policy_holder_model');
        $users = $ci->session->userdata('user');
        return $users;
    }

}

if (!function_exists('get_logo_to_be_shown')) {

    /**
     * This method returns the absolute path to the logo
     * image if it is already set in the session.
     * Returns path to
     * deafult logo if the theme is not already set.
     * 
     * @return string absolute path to the url of the logo
     */
    function get_logo_to_be_shown() {
        $ci = & get_instance();

        // get the logo url from session
        $logo = $ci->session->userdata('logo_url');

        // if logo is not set the set the logo to default logo
        if (!$logo) {
            $logo = base_url() . "assets/img/logo.jpg";
        }
        return $logo;
    }

}
