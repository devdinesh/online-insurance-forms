<?php

if (!function_exists('admin_info')) {

    /**
     * method for get the information about the perticular user
     *
     * @return array of information related to the login client
     *        
     */
    function admin_info() {
        $ci = & get_instance();

        $user = $ci->session->userdata('policy_holder');

        $policy_holder_id = $user->policy_holder_id;
        if ($policy_holder_id != '') {
            $ci->load->model('client_setting_model');
            $client_setting_model = new client_setting_model();
            $settings = $client_setting_model->get_where(
                    array('client_id' => $user->client_id
                    ));
            $result = $settings[0];
            return $result;
        } else {
            return false;
        }
    }

}

if (!function_exists('get_client_logo')) {

    /**
     * Method for get the admin logo url which can be shown
     * in the policy holder side directly.
     *
     * If the logo is already set in the session then
     * this method returns the logo directly from the session.
     *
     * If the logo is not already set in the session then
     * this method fethces the logo from the database and sets the
     * path in session.
     *
     * @return string of the logo url
     */
    function get_client_logo() {

        $ci = & get_instance();
        $logo = $ci->session->userdata('logo_url');
        $user = $ci->session->userdata('policy_holder');

        if ($logo != false) {
            return $logo;
        } elseif (isset($user) && $user != false) {


            $policy_holder_id = $user->policy_holder_id;
            if ($policy_holder_id != '') {

                $client_id = $user->client_id;
                $ci->load->model('client_setting_model');
                $client_setting_model = new client_setting_model();
                $settings = $client_setting_model->get_where(
                        array('client_id' => $client_id
                        ));

                $admin_logo = $settings[0]->admin_logo;

                if ($admin_logo != '') {
                    $logo_url = base_url() . 'assets/upload/client_logo' . "/" .
                            $admin_logo;

                    $data = array('logo_url' => $logo_url
                    );
                    $ci->session->set_userdata($data);

                    return $logo_url;
                } else {
                    return get_super_admin_logo();
                }
            } else {

                return false;
            }
        } else {
            $ci->load->helper('lgin');
            $client = admin_get_client_id();
            $ci->load->model('client_setting_model');
            $client_id = $client->client_id;
            $client_setting_model = new client_setting_model();
            $settings = $client_setting_model->get_where(
                    array('client_id' => $client_id
                    ));
            $admin_logo = $settings[0]->admin_logo;

            if ($admin_logo != '') {
                $logo_url = base_url() . 'assets/upload/client_logo' . "/" . $admin_logo;
                return $logo_url;
            } else {
                return get_super_admin_logo();
            }
        }
    }

    if (!function_exists('get_super_admin_logo')) {

        /**
         * Method for get the super admin logo url when the admin is not log in.
         *
         * this method fethces the logo from the database and sets the
         * path of the logo
         *
         * @return string of the logo url
         */
        function get_super_admin_logo() {
            $ci = & get_instance();
            $logo = $ci->session->userdata('logo_url1');
            if ($logo != false) {
                return $logo;
            } else {
                $ci->load->model('super_admin_setting_model');
                $super_admin_setting_model = new super_admin_setting_model();
                $super_admin_info = $super_admin_setting_model->get_all();
                $logo = $super_admin_info[0]->logo;
                if ($logo != '') {
                    $logo_url = base_url() . 'assets/upload/client_logo' . "/" .
                            $logo;
                    $data = array('logo_url1' => $logo_url
                    );
                    $ci->session->set_userdata($data);
                    return $logo_url;
                } else {
                    $logo_url = base_url() . 'assets/upload/client_logo/default_logo.jpg';
                    return $logo_url;
                }
            }
        }

    }
}