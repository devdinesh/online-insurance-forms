<?php

if (!function_exists('get_page_name')) {

    function get_page_name($page_name) {
        return $page_name;
    }

}

if (!function_exists('get_default_page_template_name')) {

    function get_default_page_template_name() {
        return "template";
    }

}

if (!function_exists('get_super_admin_template_name')) {

    function get_super_admin_template_name() {
        return "super_admin/super_admin_template";
    }

}

if (!function_exists('get_admin_template_name')) {

    function get_admin_template_name() {
        return "admin/admin_template";
    }

}

if (!function_exists('get_admin_template_for_not_logged_in')) {

    function get_admin_template_for_not_logged_in() {
        return "admin/login_template";
    }

}

if (!function_exists('get_admin_user_template_logged_in')) {

    function get_admin_user_template_logged_in() {
        return "admin/template";
    }

}

if (!function_exists('get_policy_holder_template_logged_in')) {

    function get_policy_holder_template_logged_in() {
        return "user/template";
    }

}
if (!function_exists('get_mat_super_admin_template_name')) {

    function get_mat_super_admin_template_name() {
        return "super_admin/super_admin_template_mat";
    }

}
?>