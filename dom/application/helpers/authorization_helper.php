<?php

/**
 * this method returns false if the claim was not found or 
 * the claim does not belongs to the given polivy holder id.
 * it returns true otherwise.
 * @param int $claim_id is the primary key from the claim table
 * @param int $policy_holder_id is the id of policy holder
 * @return boolean true if the claim passed belongs to the policy holder whos id is passed. false otherwise
 * 
 */
function claim_id_belongs_to_policy_holder ( $claim_id, $policy_holder_id )
{
    $ci = & get_instance();
    $ci->load->model('claims_model');
    $array = array('claim_id' => $claim_id,
            'policy_holder_id' => $policy_holder_id
    );
    $result = $ci->claims_model->get_where($array);
    
    // we get a record only if the claim belongs to the given policy holder
    if (count($result) > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}