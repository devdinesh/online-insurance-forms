<?php



if (! function_exists('get_current_user_subscription'))
{

    /**
     *
     * @return int returns the values of the current subscription.
     */
    function get_current_user_subscription()
    {
        $ci = & get_instance();
        //$ci->load->model('users_model');
        $ci->load->helper('lgin');
        $ci->load->model('client_subscription_model');
        // get the client for which we want to get the list
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        // get the information about the current subscription.
        $obj_sub = new client_subscription_model();
        $sub_record = $obj_sub->getuserSubscription($client_id);
        return count($sub_record);
    }

   
}


