<?php

if (! function_exists('my_file_upload'))
{

    function my_file_upload ( $form_field_name, $settings, $ci )
    {
        // echo $form_field_name;
        // $ci = get_instance();
        $arr = array();
        $valid = true;
        if ($_FILES[$form_field_name]['name'] != '')
        {
            $ci->load->library('upload');
            $ci->upload->initialize($settings);
            if ($ci->upload->do_upload($form_field_name))
            {
                $upload_data = $ci->upload->data();
                $valid = 1;
                
                $arr['status'] = $valid;
                $arr['data'] = $upload_data['file_name'];
                return $arr;
            }
            else
            {
                $valid = 0;
                $arr['status'] = $valid;
                $arr['data'] = $ci->upload->display_errors('', '');
                return $arr;
            }
        }
        else
        {
            $valid = 1;
            $arr['status'] = $valid;
            return $arr;
        }
    }
}
?>
