<?php

// echo 'only for test';
if (!function_exists('dynamic_send_mail')) {

    function dynamic_send_mail($tomailid, $subject, $message, $cc = '', $attachement = '', $smtp_user, $smtp_pass, $smtp_server, $smtp_port,$from=NULL) {
        $ci = get_instance();

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = $smtp_server;
        if ($smtp_port != 0) {
            $config['smtp_port'] = $smtp_port;
        }
        $config['smtp_user'] = $smtp_user;
        $config['smtp_pass'] = $smtp_pass;
        $config['mailtype'] = 'html';

        $ci->load->library('email', $config);
        $ci->email->clear(TRUE);
        $ci->email->set_newline("\r\n");
        if (isset($from) && $from != NULL) {
            $ci->email->from(trim($from));
        } else {
            $ci->email->from($smtp_user);
        }
        $ci->email->to($tomailid);
        $ci->email->subject($subject);
        $ci->email->message($message);

        if ($cc != '') {
            $ci->email->bcc($cc);
        }

        if ($attachement != '') {
            if (is_array($attachement)) {
                foreach ($attachement as $att) {
                    $ci->email->attach($att);
                }
            } else {
                $ci->email->attach($attachement);
            }
        }

        if (!$ci->email->send()) {
            return FALSE;
        } else {
            $ci->email->clear();
            return TRUE;
        }
    }

}
?>
