<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class is for dashboard of the policy holder (policy holder = user )
 */
class dashboard extends CI_Controller
{

    var $default_template_name;

    function __construct ()
    {
        parent::__construct();
        
        $this->default_template_name = get_policy_holder_template_logged_in();
    
    }

    /**
     * this page is now disabled.
     * now redirecting to the
     * list insurance pages
     */
    public function index ()
    {
        redirect(base_url() . "user/list_forms");
    
    }

}

?>
