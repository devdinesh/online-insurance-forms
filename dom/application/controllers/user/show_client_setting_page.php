<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class show_client_setting_page extends CI_Controller
{

    var $default_template_name;

    function __construct ()
    {
        parent::__construct();
        
        $this->default_template_name = get_policy_holder_template_logged_in();
        $this->load->helper('admin_setting_info');
    
    }

    public function about_us ()
    {
        $admin_info = admin_info();
        $content = $admin_info->about_us;
        $data['page_name'] = 'user/show_content';
        $data['title'] = 'About Us';
        $data['content'] = $content;
        $data['page_title'] = 'Over ons'; // showd in the <h1> at the top of the page
        $this->load->view($this->default_template_name, $data);
    
    }

    public function contact ()
    {
        $admin_info = admin_info();
        $content = $admin_info->contact;
        $data['page_name'] = 'user/show_content';
        $data['title'] = 'Contact';
        $data['content'] = $content;
        $data['page_title'] = 'Contact'; // showd in the <h1> at the top of the page
        $this->load->view($this->default_template_name, $data);
    
    }

}