<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class policy_holder_info extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('policy_holder_model');
        $this->load->model('client_setting_model');
        $this->default_template_name = get_policy_holder_template_logged_in();
    }

    public function index($randid) {
        $obj = new policy_holder_model();
        $res = $this->policy_holder_model->get_where(
                array('password_reset_random_string' => $randid
                ));

        if (count($res) == 1) {
            $admin_setting_info = $this->client_setting_model->get_where(
                    array('client_id' => $res[0]->client_id
                    ));
            $data['registratin_text'] = $admin_setting_info[0]->registration_text;
            $data['page_name'] = 'user/policy_holder/add_policy_holder_info';
            $data['policy_holder'] = $res[0];
            $data['title'] = 'policy holder';
            $data['randid'] = $randid;
            $this->load->view($this->default_template_name, $data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    /**
     * this will handel all the data post by the add method form.
     */
    function editListener($randid) {
        $obj = new policy_holder_model();
        $res = $this->policy_holder_model->get_where(
                array('password_reset_random_string' => $randid
                ));
        if (count($res) == 1) {
            $validation = $obj->validation_rules;

            $this->form_validation->set_rules($validation);
            $this->form_validation->set_rules('passward', 'Wachtwoord', 'trim|required');
            $this->form_validation->set_rules('c_passward', 'Wachtwoord (bevestiging)', 'trim|required|matches[passward]');

            if ($this->form_validation->run() == FALSE) {

                $this->form_validation->set_value('f_name');
                $this->form_validation->set_value('m_name');
                $this->form_validation->set_value('l_name');
                $this->form_validation->set_value('address');
                $this->form_validation->set_value('zipcode');
                $this->form_validation->set_value('p_number');
                $this->form_validation->set_value('b_number');
                $this->form_validation->set_value('m_number');
                $this->form_validation->set_value('email');
                $this->index($randid);
            } else {
                $obj->client_id = $res[0]->client_id;
                $obj->first_name = $this->input->post('f_name');
                $obj->middle_name = $this->input->post('m_name');
                $obj->last_name = $this->input->post('l_name');
                $obj->email = $res[0]->email;
                $obj->address = $this->input->post('address');
                $obj->zipcode = $this->input->post('zipcode');
                $obj->private_number = $this->input->post('p_number');
                $obj->business_number = $this->input->post('b_number');
                $obj->mobile_number = $this->input->post('m_number');
                $obj->email = $this->input->post('email');
                $obj->password_reset_random_string = NULL;
                $obj->policy_holder_id = $res[0]->policy_holder_id;

                $obj->status = 'A';
                $obj->register_date = date("Y-m-d");

                if (trim($this->input->post('passward')) != '') {
                    $obj->password = md5($this->input->post('passward'));
                }

                $obj->update();
                $this->session->set_flashdata('success', "Relatie Updated sucessfully");
                $this->session->unset_userdata('admin');
                $this->session->unset_userdata('super_admin');
                $policy_holder = $this->policy_holder_model->get_where(
                        array('policy_holder_id' => $res[0]->policy_holder_id
                        ));
                $session = array('policy_holder' => $policy_holder[0],
                    'user_type' => 'policy_holder', 'logged_in' => true
                );
                $this->session->set_userdata($session);
                redirect(base_url() . "user");

                // redirect(base_url().'user/list_forms', 'refresh');
            }
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    function edit_policy_holder() {
        $policy_holder_info = $this->session->userdata('policy_holder');
        $policy_holder_id = $policy_holder_info->policy_holder_id;
        $obj = new policy_holder_model();
        $res = $this->policy_holder_model->get_where(
                array('policy_holder_id' => $policy_holder_id
                ));

        if (count($res) == 1) {
            $data['page_name'] = 'user/policy_holder/edit_policy_holder';
            $data['policy_holder'] = $res[0];
            $data['title'] = 'Relatie';
            $this->load->view($this->default_template_name, $data);
        } else {
            redirect(base_url(), 'refresh');
        }
    }

    function editpolicyListener($policy_holder_id) {
        $obj = new policy_holder_model();
        $res = $this->policy_holder_model->get_where(
                array('policy_holder_id' => $policy_holder_id
                ));
        if (count($res) == 1) {
            $validation = $obj->validation_rules;
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules($validation);
            if ($this->form_validation->run() == FALSE) {

                $this->form_validation->set_value('f_name');
                $this->form_validation->set_value('m_name');
                $this->form_validation->set_value('l_name');
                $this->form_validation->set_value('address');
                $this->form_validation->set_value('email');
                $this->form_validation->set_value('zipcode');
                $this->form_validation->set_value('p_number');
                $this->form_validation->set_value('b_number');
                $this->form_validation->set_value('m_number');

                $this->edit_policy_holder();
            } else {
                $first_name = $this->input->post('f_name');
                $middle_name = $this->input->post('m_name');
                $last_name = $this->input->post('l_name');
                $email = $this->input->post('email');
                $address = $this->input->post('address');
                $zipcode = $this->input->post('zipcode');
                $obj->client_id = $res[0]->client_id;
                $obj->first_name = $first_name;
                $obj->middle_name = $middle_name;
                $obj->last_name = $last_name;
                $obj->email = $email;
                $obj->address = $address;
                $obj->zipcode = $zipcode;
                $obj->private_number = $this->input->post('p_number');
                $obj->business_number = $this->input->post('b_number');
                $obj->mobile_number = $this->input->post('m_number');
                $obj->password_reset_random_string = $res[0]->password_reset_random_string;
                $obj->policy_holder_id = $res[0]->policy_holder_id;
                if ($this->input->post('status') == 'A') {
                    $obj->status = 'A';
                } else {
                    $obj->status = 'D';
                }

                $obj->register_date = date("Y-m-d", $res[0]->register_date);

                if (trim($this->input->post('passward')) != '') {
                    $obj->password = md5($this->input->post('passward'));
                }
                if ($res[0]->first_name != $first_name) {
                    $obj->old_first_name = $res[0]->first_name;
                }
                if ($res[0]->middle_name != $middle_name) {
                    $obj->old_middle_name = $res[0]->middle_name;
                }
                if ($res[0]->last_name != $last_name) {
                    $obj->old_last_name = $res[0]->last_name;
                }
                if ($res[0]->email != $email) {
                    $obj->old_email = $res[0]->email;
                }
                if ($res[0]->address != $address) {
                    $obj->old_address = $res[0]->address;
                }
                if ($res[0]->zipcode != $zipcode) {
                    $obj->old_zipcode = $res[0]->zipcode;
                }
                $obj->update();
                // echo "record updated sucessfully";

                $this->session->set_flashdata('success', "Profile Updated sucessfully");
                redirect(base_url() . "user/list_forms");
            }
        } else {
            redirect(base_url() . "user/list_forms");
        }
    }

}

?>
