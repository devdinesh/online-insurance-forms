<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * this class gets the answers for question from the policy holder one by one
 */
class answer extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('forms_model');
        $this->load->model('form_field_values_model');
        $this->load->model('form_field_model');
        $this->load->model('claims_model');
        $this->load->model('clients_model');
        $this->load->helper('time_worked');
        $this->load->model('client_setting_model');
        $this->load->model('forms_categories_model');
        $this->load->model('forms_categories_question_model');
        $this->load->model('forms_categories_question_answer_model');
        $this->load->helper('create_pdf_for_claim_helper');
        $this->load->model('form_field_values_model');
        $this->load->helper('daynamic_sending_mail');
        $this->load->library('image_lib');
        $this->load->model('forms_answers_model');
        $this->load->model('claims_files_model');
        $this->load->helper('email');
        $this->load->model('forms_answers_details_model');
        $this->default_template_name = get_policy_holder_template_logged_in();
    }

    public function view_insurance_index($claim_id, $form_id) {
        $data['page_name'] = 'user/answer/damage__form_info';
        $data['claim_id'] = $claim_id;
        // get the information about the form
        $form_info = $this->forms_model->get_where(array('form_id' => $form_id));
        $data['form_value'] = $form_info[0];
        // get the information about the forms fields
        $field_info = $this->form_field_values_model->get_where(array('claim_id' => $claim_id));
        $data['field_value'] = $field_info;
        $data['title'] = 'Damage form';
        $this->load->view($this->default_template_name, $data);
    }

    public function edit_details($claim_id, $temp = false) {
        $claim_info = $this->claims_model->get_where(array('claim_id' => $claim_id));

        if (count($claim_info) <= 0) {
            $this->session->set_flashdata('error', 'Claim Does not exists');
        } else {
            // update the information of the form field value
            $field_info = $this->form_field_values_model->get_where(array('claim_id' => $claim_id));

            // get the email field name.
            $form_id = $claim_info[0]->form_id;
            $email_field = $this->form_field_model->getMailFiledFromID($form_id);

            foreach ($field_info as $info) {
                if ($info->filed_value != $this->input->post(str_replace(' ', '_', $info->name_at_form))) {
                    if ($info->name == $email_field->field_name) {
                        $this->form_field_values_model->update_Claim_Values($info->name_at_form, $this->input->post(str_replace(' ', '_', $info->name_at_form)), $claim_id, $info->filed_value, "1");
                    }
                }
            }
            $this->session->set_flashdata('success', 'Updated The Data Sucessfully');

            redirect(base_url() . "user/list_forms", 'refresh');
        }
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                                                                         * 
     *                                                                         *
     *                          START OF PREVIEW FORMS                         *
     *                                                                         *
     *                                                                         *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    function view_form($claim_id, $form_id) {

        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $check = $this->claims_model->get_where(array('form_id' => $form_id, 'claim_id' => $claim_id));
        if (count($check) == 0) {
            $this->session->set_flashdata('error', "URL is Altered Somehow");
            redirect(base_url() . "user/list_forms");
        }

        $this->session->unset_userdata('prev_url');
        $this->session->keep_flashdata('success');
        $this->session->unset_userdata('previous_url');

        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);

        if (count($forms) == 1) {
            $this->session->unset_userdata('prev_cat');
            $this->session->unset_userdata('prev_seq');
            if ($forms[0]->show_all_question_at_once == 0) {
                redirect(base_url() . 'user/answer/preview_once_start/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/null/null', 'refresh');
            } elseif ($forms[0]->show_all_question_at_once == 1) {
                redirect(base_url() . 'user/answer/preview_question_start/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
            } else {
                redirect(base_url() . 'user/answer/preview_category_start/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', "URL is Altered Somehow");
            redirect(base_url() . 'user/list_forms', 'refresh');
        }
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                      START SHOW ALL QUESTION AT ONCE                    * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    function view_form_fields($claim_id, $form_id) {

        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $check = $this->claims_model->get_where(array('form_id' => $form_id, 'claim_id' => $claim_id));
        if (count($check) == 0) {
            $this->session->set_flashdata('error', "URL is Altered Somehow");
            redirect(base_url() . "user/list_forms");
        }

        $data['title'] = 'Form Fields Details';
        $data['claim_id'] = $claim_id;
        $data['email_field'] = $this->form_field_model->getMailFiledFromID($form_id);

        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);

        $data['form'] = $forms[0];
        $field_info = $this->form_field_values_model->get_where(array('claim_id' => $claim_id));
        $data['field_value'] = $field_info;
        $data['page_name'] = 'user/answer/view_form_fields';
        $this->load->view($this->default_template_name, $data);
    }

    function preview_once_start($claim_id, $form_id, $category_id = null, $sequence = null) {

        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $check = $this->claims_model->get_where(array('form_id' => $form_id, 'claim_id' => $claim_id));
        if (count($check) == 0) {
            $this->session->set_flashdata('error', "URL is Altered Somehow");
            redirect(base_url() . "user/list_forms");
        }

        $data['title'] = 'Preview Insurance Form';
        $data['claim_id'] = $claim_id;

        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
        $data['form'] = $forms[0];

        $form_info = $this->forms_model->get_where(array('form_id' => $form_id));
        $field_info = $this->form_field_values_model->get_where(array('claim_id' => $claim_id));
        $data['form_value'] = $form_info[0];
        $data['field_value'] = $field_info;
        $data['email_field'] = $this->form_field_model->getMailFiledFromID($form_id);

        $data['category_id'] = $category_id;
        $data['sequence'] = $sequence;

        $new_ques = array();
        if ($category_id == 'null' && $sequence == 'null') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                    $last_question = $obj_ques->anyQuestionLeft_allAtOnce($get_question[0]->cat_id, $get_question[0]->question_id);
                }
            }

            $next = $this->getFirstCategoryFirstQuestion($form_id);
            if (isset($next) && !empty($next)) {
                $obj_ques = new forms_categories_question_model();
                $new_questions = $obj_ques->getAllQuestionForFormAtOnce($next['next_cat']->cat_id, $next['next_question']->sequence);
                foreach ($new_questions as $new) {
                    if (!in_array($new->question_id, $questionids)) {
                        $obj_ques = new forms_categories_question_model();
                        $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                        $new_ques[] = $get_question[0];
                        $last_question = $obj_ques->anyQuestionLeft_allAtOnce($get_question[0]->cat_id, $get_question[0]->question_id);
                    }
                }
            }

            if (!empty($last_question) && $last_question == TRUE) {
                $data['show_end_data'] = TRUE;
            } else {
                $data['show_end_data'] = FALSE;
            }
        } else if ($category_id == 'end' && $sequence == 'end') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (is_array($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }
            $data['show_end_data'] = TRUE;
        } else {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                    $last_question = $obj_ques->anyQuestionLeft_allAtOnce($get_question[0]->cat_id, $get_question[0]->question_id);
                }
            }
            $obj_ques = new forms_categories_question_model();
            $new_questions = $obj_ques->getAllQuestionForFormAtOnce($category_id, $sequence);
            foreach ($new_questions as $new) {
                if (!in_array($new->question_id, $questionids)) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                    $new_ques[] = $get_question[0];
                    $last_question = $obj_ques->anyQuestionLeft_allAtOnce($get_question[0]->cat_id, $get_question[0]->question_id);
                }
            }

            if (!empty($last_question) && $last_question == TRUE) {
                $data['show_end_data'] = TRUE;
            } else {
                $data['show_end_data'] = FALSE;
            }
        }

        $all_cat = array();
        foreach ($new_ques as $ques) {
            if (!in_array($ques->cat_id, $all_cat)) {
                $all_cat[] = $ques->cat_id;
            }
        }

        foreach ($all_cat as $cat) {
            $obj_cat = new forms_categories_model();
            $get_cat_details = $obj_cat->get_where(array('cat_id' => $cat));
            if (!empty($get_cat_details))
                $category_info[] = $get_cat_details[0];
        }

        $data['category_info'] = $category_info;
        $data['questions'] = $new_ques;
        $data['page_name'] = 'user/answer/show_all_at_once';
        $this->load->view($this->default_template_name, $data);
    }

    function get_smart_questions($claim_id, $form_id, $category_id, $sequence) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $check = $this->claims_model->get_where(array('form_id' => $form_id, 'claim_id' => $claim_id));
        if (count($check) == 0) {
            $this->session->set_flashdata('error', "URL is Altered Somehow");
            redirect(base_url() . "user/list_forms");
        }

        $sucess = $this->validateQuestionForShowAllAtOnce($claim_id, $form_id, $category_id, $sequence);
        if ($sucess == TRUE) {
            $this->saveShowallAtOnce($claim_id, $form_id, $category_id, $sequence);
            $answer_id = $this->input->post('last_answer_id');
            if (!empty($answer_id)) {
                $check = $this->forms_categories_question_answer_model->get_where(array('answer_id' => $answer_id));
                if ($check[0]->skip_to_questions != null && $check[0]->skip_to_questions != '') {
                    if ($check[0]->skip_to_questions != 0) {
                        $obj_ques = new forms_categories_question_model();
                        $get_question = $obj_ques->get_where(array('question_id' => $check[0]->skip_to_questions));
                        redirect(base_url() . 'user/answer/preview_once_start/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $get_question[0]->cat_id . '/' . $get_question[0]->sequence . '#view_' . $check[0]->skip_to_questions, 'refresh');
                    } else {
                        redirect(base_url() . 'user/answer/preview_once_start/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/end/end#end_form', 'refresh');
                    }
                } else {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $check[0]->question_id));
                    $next = $this->getNextQuestion($get_question[0]->cat_id, $get_question[0]->question_id, $get_question[0]->sequence);
                    redirect(base_url() . 'user/answer/preview_once_start/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $next['next_cat']->cat_id . '/' . $next['next_question']->sequence . '#view_' . $next['next_question']->question_id, 'refresh');
                }
            } else {
                $this->preview_once_start($this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**'), $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**'), $category_id, $sequence);
            }
        } else {
            $this->preview_once_start($this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**'), $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**'), $category_id, $sequence);
        }
    }

    function saveShowallAtOnce($claim_id, $form_id, $category_id, $sequence) {
        $new_ques = array();
        if ($category_id == 'null' && $sequence == 'null') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }
            $next = $this->getFirstCategoryFirstQuestion($form_id);
            $obj_ques = new forms_categories_question_model();
            $new_questions = $obj_ques->getAllQuestionForFormAtOnce($next['next_cat']->cat_id, $next['next_question']->sequence);
            foreach ($new_questions as $new) {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                $new_ques[] = $get_question[0];
            }
        } else if ($category_id == 'end' && $sequence == 'end') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }
        } else {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }

            $obj_ques = new forms_categories_question_model();
            $new_questions = $obj_ques->getAllQuestionForFormAtOnce($category_id, $sequence);
            foreach ($new_questions as $new) {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                $new_ques[] = $get_question[0];
            }
        }

        foreach ($new_ques as $question) {
            $this->saveFomrValues($claim_id, $form_id, $question->cat_id, $question->question_id);
        }

        return true;
    }

    function wholeFormSaveActionAtOnce($claim_id, $form_id, $category_id, $sequence) {
        $action = $this->input->post('action');
        if ($action == 'Opslaan in concept') {
            $this->endPreviewSaveAllAtonce($claim_id, $form_id, $category_id, $sequence);
        } else if ($action == 'confirm') {
            $this->endPreviewConfirmAndSendAtOnce($claim_id, $form_id, $category_id, $sequence);
        }
    }

    function endPreviewSaveAllAtonce($claim_id, $form_id, $category_id, $sequence) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $sucess = $this->validateQuestionForShowAllAtOnce($claim_id, $form_id, $category_id, $sequence);
        if ($sucess == TRUE) {
            $this->saveShowallAtOnce($claim_id, $form_id, $category_id, $sequence);
            $claims_model = new claims_model();
            $claims = $claims_model->get_where(array('claim_id' => $claim_id));
            $claim = $claims[0];
            $claim->status = "open";
            $claim->update();
            $this->session->set_flashdata('success', "Form status is now open");
            redirect(base_url() . "user/list_forms");
        } else {
            $this->preview_once_start($this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**'), $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**'), $category_id, $sequence);
        }
    }

    function endPreviewConfirmAndSendAtOnce($claim_id, $form_id, $category_id, $sequence) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $sucess = $this->validateQuestionForShowAllAtOnce($claim_id, $form_id, $category_id, $sequence);
        if ($sucess == TRUE) {
            $this->saveShowallAtOnce($claim_id, $form_id, $category_id, $sequence);
            $claims_model = new claims_model();
            $claims = $claims_model->get_where(array('claim_id' => $claim_id));
            $claim = $claims[0];
            $claim->conform_date = get_current_time()->get_date_for_db();
            $claim->status = "Afgerond";
            if (!valid_email($claim->handler_email)) {
                $get_hendler_email_field = $this->form_field_model->get_where(array('form_id' => $claim->form_id, 'field_type' => 'BE'));
                if (!empty($get_hendler_email_field)) {
                    $policy_field_value = $this->form_field_values_model->get_where(
                            array('name' => $get_hendler_email_field[0]->field_name,
                                'claim_id' => $claim->claim_id
                            ));
                    if (!empty($policy_field_value)) {
                        if (valid_email(trim($policy_field_value[0]->filed_value))) {
                            $claim->handler_email = trim($policy_field_value[0]->filed_value);
                        }
                    }
                }
            }
            $claim->update();
            $client_id = $claim->get_form_object()->client_id;
            $policy_holder_object = $claim->get_policy_holder();
            $this->_send_email_to_policy_holder($client_id, $claim, $policy_holder_object);
            // $this->_send_email_to_intermediary($client_id, $claim, $policy_holder_object);
            if (isset($claim->handler_email) && $claim->handler_email != NULL) {
                $this->_send_email_to_hendler($client_id, $claim, $policy_holder_object);
            }
            $this->session->set_flashdata('success', "Formulier " . $claim->get_form_number() . " is afgerond");
            redirect(base_url() . "user/list_forms");
        } else {
            $this->preview_once_start($this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**'), $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**'), $category_id, $sequence);
        }
    }

    function getPreviousQuestionDetails($claim_id) {
        $obj_ans = new forms_answers_model();
        $get_answer = $obj_ans->get_where(array('claim_id' => $claim_id));
        $return = array();
        foreach ($get_answer as $ans) {
            $return[] = $ans->question_id;
        }
        return $return;
    }

    function validateQuestionForShowAllAtOnce($claim_id, $form_id, $category_id, $sequence) {
        $new_ques = array();
        $validatin_success = true;
        if ($category_id == 'null' && $sequence == 'null') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }
            $next = $this->getFirstCategoryFirstQuestion($form_id);
            $obj_ques = new forms_categories_question_model();
            $new_questions = $obj_ques->getAllQuestionForFormAtOnce($next['next_cat']->cat_id, $next['next_question']->sequence);
            foreach ($new_questions as $new) {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                $new_ques[] = $get_question[0];
            }
        } else if ($category_id == 'end' && $sequence == 'end') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }
        } else {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }

            $obj_ques = new forms_categories_question_model();
            $new_questions = $obj_ques->getAllQuestionForFormAtOnce($category_id, $sequence);
            foreach ($new_questions as $new) {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                $new_ques[] = $get_question[0];
            }
        }

        if (!empty($new_ques)) {
            foreach ($new_ques as $question) {
                if ($question->answer_kind == 'checkbox') {
                    if ($question->required == 1) {
                        $checkbox_validated = $this->_validate_checkbox_group_required($question);
                        if ($checkbox_validated != true) {
                            $this->form_validation->set_rules($question->question_id . "_", ' answer of this ', 'required');
                            if ($this->form_validation->run() == FALSE) {
                                $validatin_success = false;
                                $this->form_validation->set_value($question->question_id . "_");
                            }
                        }
                    }
                } else if ($question->answer_kind == 'date') {

                    if ($question->required == 1) {
                        $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                    } else {
                        $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                    }
                    if ($this->form_validation->run() == FALSE) {
                        $validatin_success = false;
                        $this->form_validation->set_value($question->question_id . "_");
                    }
                } else if ($question->answer_kind == 'number') {
                    //||regex_match[/^[0-9]+\.[0-9]{2}$/]
                    if ($question->required == 1) {
                        $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^[0-9]\d*(\,\d{0,2})?$/]');
                    } else {
                        $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^[0-9]\d*(\,\d{0,2})?$/]');
                    }
                    if ($this->form_validation->run() == FALSE) {
                        $validatin_success = false;
                        $this->form_validation->set_value($question->question_id . "_");
                    }
                } else {
                    if ($question->required == 1) {
                        $this->form_validation->set_rules($question->question_id . "_", ' answer of this ', 'required');
                        if ($this->form_validation->run() == FALSE) {
                            $validatin_success = false;
                            $this->form_validation->set_value($question->question_id . "_");
                        }
                    }
                }
            }
        }

        return $validatin_success;
    }

    function uploadAttachmentOnce($claim_id, $form_id, $category_id, $sequence) {

        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $form_field_name = 'attachment';
        $this->load->model('claims_files_model');
        $upload_config = claims_files_model::get_upload_config();

        $files = claims_files_model::get_claims_for_claim_id($claim_id);
        $current_image_size = $_FILES[$form_field_name]['size'];
        $size = 0;
        foreach ($files as $file) {
            $path = FCPATH . 'assets/claim_files/' . $file->file_name;
            $size += filesize($path);
        }
        $size1 = $current_image_size + $size;
        $total_size = number_format($size1 / 1048576, 1);
        if ($total_size > 10) {
            $success = false;
            $this->session->set_flashdata('file_errors', 'You can not upload more than 10MB data.');
            redirect(base_url() . "user/answer/preview_once_start/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/end/end#end_form");
            exit;
        }

        $logo_upload_status = my_file_upload($form_field_name, $upload_config, $this);
        $success = true;
        if ($logo_upload_status['status'] == 0) {
            $success = false;
            $this->session->set_flashdata('file_errors', $logo_upload_status['data']);
            echo '<script type="text/javascript">alert(\'' . htmlentities($logo_upload_status['data']) . '\'); window.location.href=\'' . base_url() . "user/answer/preview_once_start/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/end/end#end_form" . '\'</script>';
        }

        if (!isset($logo_upload_status['data'])) {
            $success = false;
            $this->session->set_flashdata('file_errors', "Selecteer eerst het bestand dat u wilt toevoegen.");
            $this->load->model('forms_model');
            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $form = $forms[0];
            echo '<script type="text/javascript">alert(\'Selecteer eerst het bestand dat u wilt toevoegen.\'); window.location.href=\'' . base_url() . "user/answer/preview_once_start/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/end/end#end_form" . '\'</script>';
            flush();
        }

        if ($success == true) {
            $file_name = $logo_upload_status['data'];
            $claims_files_model = new claims_files_model();
            $claims_files_model->file_name = $file_name;
            $claims_files_model->claim_id = $claim_id;
            $claims_files_model->save();
            $this->load->model('forms_model');
            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $form = $forms[0];
            $this->session->set_flashdata('flash_prev_url', $this->input->post('prev_url'));
            redirect(base_url() . "user/answer/preview_once_start/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/end/end#end_form");
        }
    }

    function deleteAttachmentOnce($claim_id, $form_id, $category_id, $sequence, $attachment_id) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $attachment_id = $this->encrypt->decode($attachment_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $claims_files_model = new claims_files_model();
        $files = $claims_files_model->get_where(array('file_id' => $attachment_id));
        if (count($files) > 0) {
            $file = $files[0];
            $file->delete();
            $this->session->set_flashdata('flash_prev_url', $this->session->flashdata('flash_prev_url'));
        }
        redirect(base_url() . 'user/answer/preview_once_start/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/end/end#end_form', 'refresh');
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                        END SHOW ALL QUESTION AT ONCE                    * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *              START SHOW ALL QUESTION CATEGORY BY CATEGORY               * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    public function preview_category_start($claim_id, $form_id) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $check = $this->claims_model->get_where(array('form_id' => $form_id, 'claim_id' => $claim_id));
//        if (count($check) == 0) {
//            $this->session->set_flashdata('error', "URL is Altered Somehow");
//            redirect(base_url() . "user/list_forms");
//        }

        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
        $form = $forms[0];

        $form_info = $this->forms_model->get_where(array('form_id' => $form_id));
        $data['form_value'] = $form_info[0];

        $field_info = $this->form_field_values_model->get_where(array('claim_id' => $claim_id));
        $data['field_value'] = $field_info;

        $email_field = $this->form_field_model->getMailFiledFromID($form_id);

        $data['email_field_name'] = @$email_field->field_name;

        $data['total_form_completed'] = $this->getFromProgress($claim_id, $form_id);
        $data['title'] = 'Preview Insurance Form';
        $data['form_id'] = $form_id;
        $data['form'] = $form;
        $data['claim_id'] = $claim_id;
        $data['page_name'] = 'user/answer/show_category_header';

        $first_category = $this->getFirstCategory($form_id);

        if ($first_category !== false) {
            $data['next_category'] = $first_category;
        }

        $this->load->view($this->default_template_name, $data);
    }

    function preview_category($claim_id, $form_id, $category_id, $sequence = null) {

        session_start();
        $temp = @$_SESSION['temp'];
        // var_dump($temp);
        //exit;
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $category_id = $this->encrypt->decode($category_id, '**DOM_CLAIM_DEVREPUBLIC**');

        if ($category_id != 'end') {

            $obj_cat = new forms_categories_model();
            $get_cat = $obj_cat->get_where(array('form_id' => $form_id, 'cat_id' => $category_id));

            if (count($get_cat) == 1) {
                $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
                $data['form'] = $forms[0];
                $data['category'] = $get_cat[0];
                $data['claim_id'] = $claim_id;
                $PURL = $temp['prev_url'];
                if (isset($PURL) && $PURL != false) {
                    $data['prev_url_smart'] = $PURL;
                } else {
                    $data['prev_url_smart'] = '';
                }

                $data['prev_cat'] = $this->get_previous_category($category_id);

                $data['next_cat'] = $this->get_next_category($category_id);
                $data['smart_sequence'] = $sequence;

                $data['total_form_completed'] = $this->getFromProgress($claim_id, $form_id);
                $data['title'] = 'Forms';
                $data['page_name'] = 'user/answer/show_single_category';
                $this->load->view($this->default_template_name, $data);
            } else {
                //$this->session->set_flashdata('error', 'Data Not Exits 2');
                redirect(base_url() . 'user/list_forms', 'refresh');
            }
        } else {
            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $data['form'] = $forms[0];
            $PURL = $this->session->userdata('prev_url');
            $count = count($PURL);
            if (isset($PURL) && $PURL != false) {
                $data['prev_url_smart'] = $PURL;
            } else {
                $data['prev_url_smart'] = '';
            }
            $data['prev_cat'] = $this->getLastCategory($form_id);
            $data['claim_id'] = $claim_id;
            $data['title'] = 'Forms';
            $data['total_form_completed'] = $this->getFromProgress($claim_id, $form_id);
            $data['page_name'] = 'user/answer/show_category_footer';
            $this->load->view($this->default_template_name, $data);
        }
    }

    function PreviewCategoryListener($claim_id, $form_id, $category_id, $sequence = null) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $category_id = $this->encrypt->decode($category_id, '**DOM_CLAIM_DEVREPUBLIC**');

        if ($category_id != null) {

            if (isset($sequence) && $sequence != null) {
                $save_success = $this->_preview_category_validate_form_post($form_id, $category_id, $sequence);

                if ($save_success == false) {
                    $this->preview_category($this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**'), $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**'), $this->encrypt->encode($category_id, '**DOM_CLAIM_DEVREPUBLIC**'), $sequence);
                } else {
                    $sucess = true;
                }
            } else {
                $save_success = $this->_preview_category_validate_form_post($form_id, $category_id, $sequence);


                if ($save_success == false) {
                    $this->preview_category($this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**'), $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**'), $this->encrypt->encode($category_id, '**DOM_CLAIM_DEVREPUBLIC**'));
                } else {
                    $sucess = true;
                }
            }

            if (@$sucess == true) {
                if (isset($sequence) && $sequence != null) {
                    $this->savecategoryValues($claim_id, $form_id, $category_id, $sequence);
                } else {
                    $this->savecategoryValues($claim_id, $form_id, $category_id);
                }

                $prev_url = array();
                session_start();
                $temp = @$_SESSION['temp'];

                $PURL = $temp['prev_url'];

                $temp = array();
                if ($PURL != FALSE && $PURL != '') {
                    foreach ($PURL as $p) {
                        $temp['prev_url'][] = $p;
                    }
                }

                if (isset($sequence) && $sequence != null) {
                    $temp_url = base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($category_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $sequence;
                    if (!empty($temp['prev_url'])) {
                        if (!in_array($temp_url, $temp['prev_url'])) {
                            $temp['prev_url'][] = $temp_url;
                        }
                    } else {
                        $temp['prev_url'][] = base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($category_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $sequence;
                    }
                } else {

                    $temp_url = base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($category_id, '**DOM_CLAIM_DEVREPUBLIC**');

                    if (!empty($temp['prev_url'])) {
                        if (!in_array($temp_url, @$temp['prev_url'])) {
                            $temp['prev_url'][] = $temp_url;
                        }
                    } else {
                        $temp['prev_url'][] = base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($category_id, '**DOM_CLAIM_DEVREPUBLIC**');
                    }
                }
                // session_start();
                $_SESSION['temp'] = $temp;
                // $this->session->set_userdata($temp);


                $obj_cat = new forms_categories_model();
                $get_cat = $obj_cat->get_where(array('form_id' => $form_id, 'cat_id' => $category_id));
                if (isset($sequence) && $sequence != null) {
                    $this->load->model('forms_categories_question_model');
                    $obj_ques = new forms_categories_question_model();
                    $get_ques = $obj_ques->get_where(array('cat_id' => $get_cat[0]->cat_id, 'sequence >' => $sequence));

                    if (count($get_ques) > 0) {
                        $skip_questions = $get_cat[0]->get_question_if_smart($sequence, false);
                    } else {
                        $skip_questions = $get_cat[0]->get_question_if_smart($sequence, true);
                    }
                } else {
                    $skip_questions = $get_cat[0]->get_question_smart();
                }

                $last_question_detail = $skip_questions[count($skip_questions) - 1];

                if ($last_question_detail->answer_kind == 'radio') {
                    $answer_given = $this->input->post($last_question_detail->question_id . '_');
                    $answers = $this->forms_categories_question_answer_model->get_where(array('answer_id' => $answer_given));
                    $check = $this->forms_categories_question_answer_model->get_where(array('question_id' => $last_question_detail->question_id, 'answer_id' => $answer_given));

                    if (isset($check[0]->skip_to_questions) && $check[0]->skip_to_questions != null && $check[0]->skip_to_questions != '') {
                        if ($check[0]->skip_to_questions == 0) {
                            redirect(base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
                        } else {
                            $obj_ques = new forms_categories_question_model();
                            $get_question = $obj_ques->get_where(array('question_id' => $check[0]->skip_to_questions));

                            redirect(base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($get_question[0]->cat_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $get_question[0]->sequence, 'refresh');
                        }
                    } else {
                        $is_question_exit = $this->isAnyQuestionLeft($form_id, $category_id, $last_question_detail->sequence);

                        if ($is_question_exit != false) {
                            redirect(base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($category_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $is_question_exit, 'refresh');
                        } else {
                            $next_cat_id = $this->get_next_category($category_id);

                            if (isset($next_cat_id) && $next_cat_id != null) {
                                redirect(base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($next_cat_id, '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
                            } else {
                                redirect(base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
                            }
                        }
                    }
                } else {
                    $is_question_exit = $this->isAnyQuestionLeft($form_id, $category_id, $last_question_detail->sequence);

                    if ($is_question_exit != false) {
                        redirect(base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($category_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $is_question_exit, 'refresh');
                    } else {
                        $next_cat_id = $this->get_next_category($category_id);

                        if (isset($next_cat_id) && $next_cat_id != null) {
                            redirect(base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($next_cat_id, '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
                        } else {
                            redirect(base_url() . 'user/answer/preview_category/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
                        }
                    }
                }
            }
        }
    }

    function isAnyQuestionLeft($form_id, $cat_id, $seq) {
        $obj_quest = new forms_categories_question_model();
        $get_ques = $obj_quest->get_where_sort(array('cat_id' => $cat_id, 'sequence >' => $seq), 'asc');

        if (count($get_ques) > 0) {
            return $get_ques[0]->sequence;
        } else {
            return false;
        }
    }

    function savecategoryValues($claim_id, $form_id, $category_id, $sequesnce = null) {
        $obj_cat = new forms_categories_model();
        $get_cat = $obj_cat->get_where(array('form_id' => $form_id, 'cat_id' => $category_id));
        $category = $get_cat[0];
        // $questions = $get_cat[0]->get_questions();
        if (isset($sequesnce) && $sequesnce != null) {
            $this->load->model('forms_categories_question_model');
            $obj_ques = new forms_categories_question_model();
            $get_ques = $obj_ques->get_where(array('cat_id' => $category->cat_id, 'sequence >' => $sequesnce));

            if (count($get_ques) > 0) {
                $questions = $category->get_question_if_smart($sequesnce, false);
            } else {
                $questions = $category->get_question_if_smart($sequesnce, true);
            }
        } else {
            $questions = $category->get_question_smart();
        }
        foreach ($questions as $question) {
            $question_id = $question->question_id;

            $obj_ques = new forms_categories_question_model();
            $get_question = $obj_ques->get_where(array('question_id' => $question_id));

            $answer = new forms_answers_model();
            $prev_id = $answer->getLastRecordInserted($claim_id);
            if (!empty($prev_id)) {
                $prev_id = $prev_id[0]->question_id;
            } else {
                $prev_id = 0;
            }
            $answers = $answer->get_where(array('question_id' => $question_id, 'claim_id' => $claim_id));
            if (count($answers) > 0) {
                $action = "update";
                $answer = $answers[0];
            } else {
                $action = "save";
            }

            if ($get_question[0]->answer_kind == 'checkbox') {
                $answers_choice_selected_ids = $this->input->post($question_id . "_");
                if ($action != "update") {
                    $answer = new forms_answers_model();
                }

                $answer->claim_id = $claim_id;
                $answer->cat_id = $category_id;
                $answer->answer_type = "checkbox";
                $answer->question_id = $question_id;
                $answer_id = -1;
                if ($action == "update") {
                    $answer->update();
                    $answer_id = $answer->answer_id;
                    $answers_choices = $answer->get_answer_choices();
                    foreach ($answers_choices as $choice) {
                        $choice->delete();
                    }
                } else {
                    $answer->prev_question = $prev_id;
                    $answer_id = $answer->save();
                }
                if ($answers_choice_selected_ids != false) {
                    foreach ($answers_choice_selected_ids as $answer_choice_id) {

                        $forms_answers_details_model = new forms_answers_details_model();
                        $forms_answers_details_model->user_answer_id = $answer_id;
                        $forms_answers_details_model->answer_id = $answer_choice_id;
                        $forms_answers_details_model->save();
                    }
                }
            } else {
                if ($action != "update") {
                    $answer = new forms_answers_model();
                }
                $answer->claim_id = $claim_id;
                $answer->cat_id = $category_id;
                $answer->answer_type = "";
                $answer->question_id = $question_id;
                $answer->answer_text = $this->input->post($question_id . "_");
                $answer->answer_type = $get_question[0]->answer_kind;
                if ($action == "update") {
                    $answer->update();
                } else {
                    $answer->prev_question = $prev_id;
                    $answer->save();
                }
            }
        }
    }

    function get_previous_category($cat_id) {
        $obj = new forms_categories_model();
        $get_current_records = $obj->get_where(array('cat_id' => $cat_id));
        $get_all_records = $obj->get_where_sort(array('form_id' => $get_current_records[0]->form_id), 'asc');

        if (count($get_current_records) == 1) {
            $temp = array();
            foreach ($get_all_records as $all) {
                $x = array();
                $x['sequence'] = $all->sequence;
                $x['cat_id'] = $all->cat_id;
                $temp[] = $x;
            }

            $current_key = 0;
            foreach ($temp as $key => $value) {
                if ($value['sequence'] == $get_current_records[0]->sequence) {
                    $current_key = $key;
                }
            }
            $prev_cat = @$temp[$current_key - 1]['cat_id'];
            return $prev_cat;
        } else {
            return false;
        }
    }

    function get_next_category($cat_id) {
        $obj = new forms_categories_model();
        $get_current_records = $obj->get_where(array('cat_id' => $cat_id));
        $get_all_records = $obj->get_where_sort(array('form_id' => $get_current_records[0]->form_id), 'asc');

        if (count($get_current_records) == 1) {
            $temp = array();
            foreach ($get_all_records as $all) {
                $x = array();
                $x['sequence'] = $all->sequence;
                $x['cat_id'] = $all->cat_id;
                $temp[] = $x;
            }

            $current_key = 0;
            foreach ($temp as $key => $value) {
                if ($value['sequence'] == $get_current_records[0]->sequence) {
                    $current_key = $key;
                }
            }
            // Update next to up sequence
            $next_cat = @$temp[$current_key + 1]['cat_id'];
            return $next_cat;
        } else {
            return false;
        }
    }

    function getFirstCategory($form_id) {
        $return = array();
        $obj_cat = new forms_categories_model();
        $get_cat = $obj_cat->get_where_sort(array('form_id' => $form_id), 'asc');

        if (count($get_cat) > 0) {
            $obj_ques = new forms_categories_question_model();
            $get_question = $obj_ques->get_where_sort(array('cat_id' => $get_cat[0]->cat_id), 'asc');

            if (count($get_question) > 0) {
                $return['next_cat'] = $get_cat[0];
                return $return;
                exit();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function getLastCategory($form_id) {
        $return = array();
        $obj_cat = new forms_categories_model();
        $get_cat = $obj_cat->get_where_sort(array('form_id' => $form_id), 'desc');

        if (count($get_cat) > 0) {
            $obj_ques = new forms_categories_question_model();
            $get_question = $obj_ques->get_where_sort(array('cat_id' => $get_cat[0]->cat_id), 'desc');

            if (count($get_question) > 0) {
                $return['next_cat'] = $get_cat[0];
                return $return;
                exit();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function _preview_category_validate_form_post($form_id, $category_id, $sequence = null) {
        $validatin_success = true;
        $this->load->model('forms_categories_model');
        $get_all_cat_info = $this->forms_categories_model->get_where(array('form_id' => $form_id, 'cat_id' => $category_id));
        if (!empty($get_all_cat_info)) {
            foreach ($get_all_cat_info as $cat_info) {
                $obj_cat = new forms_categories_model();
                $get_cat = $obj_cat->get_where(array('form_id' => $form_id, 'cat_id' => $category_id));
                if (isset($sequence) && $sequence != null) {
                    $this->load->model('forms_categories_question_model');
                    $obj_ques = new forms_categories_question_model();
                    $get_ques = $obj_ques->get_where(array('cat_id' => $get_cat[0]->cat_id, 'sequence >' => $sequence));
                    if (count($get_ques) > 0) {
                        $questions = $get_cat[0]->get_question_if_smart($sequence, false);
                    } else {
                        $questions = $get_cat[0]->get_question_if_smart($sequence, true);
                    }
                } else {
                    $questions = $get_cat[0]->get_question_smart();
                }

                if (!empty($questions)) {
                    foreach ($questions as $question) {
                        if ($question->answer_kind == 'checkbox') {
                            if ($question->required == 1) {
                                $checkbox_validated = $this->_validate_checkbox_group_required($question);
                                if ($checkbox_validated != true) {
                                    $this->form_validation->set_rules($question->question_id . "_", ' answer of this ', 'required');
                                    if ($this->form_validation->run() == FALSE) {
                                        $validatin_success = false;
                                        $this->form_validation->set_value($question->question_id . "_");
                                    }
                                }
                            }
                        } else if ($question->answer_kind == 'date') {
                            if ($question->required == 1) {
                                $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                            } else {
                                $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                            }
                            if ($this->form_validation->run() == FALSE) {
                                $validatin_success = false;
                                $this->form_validation->set_value($question->question_id . "_");
                            }
                        } else if ($question->answer_kind == 'number') {
                            if ($question->required == 1) {
                                $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^[0-9]\d*(\,\d{0,2})?$/]');
                            } else {
                                $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^[0-9]\d*(\.\d{0,2})?$/]');
                            }
                            if ($this->form_validation->run() == FALSE) {
                                $validatin_success = false;
                                $this->form_validation->set_value($question->question_id . "_");
                            }
                        } else {
                            if ($question->required == 1) {
                                $this->form_validation->set_rules($question->question_id . "_", ' answer of this ', 'required');
                                if ($this->form_validation->run() == FALSE) {
                                    $validatin_success = false;
                                    $this->form_validation->set_value($question->question_id . "_");
                                } else {
                                    
                                }
                            }

                            if ($question->answer_kind == 'radio') {
                                $answer_selected_id = $this->input->post($question->question_id . "_");
                                $forms_categories_question_answer_model = new forms_categories_question_answer_model();
                                $answers = $forms_categories_question_answer_model->get_where(array('answer_id' => $this->input->post($question->question_id . "_")));
                                if (count($answers) > 0) {
                                    $answer = $answers[0];
                                    if (isset($answer->skip_to_category)) {
                                        $this->skip_to_category = $answer->skip_to_category;
                                    } else if (isset($answer->skip_to_questions)) {
                                        $this->skip_to_questions = $answer->skip_to_questions;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $validatin_success;
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *               END SHOW ALL QUESTION CATEGORY BY CATEGORY                * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                    START SHOW ALL QUESTION ONE BY ONE                   * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    function preview_question_start($claim_id, $form_id) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $check = $this->claims_model->get_where(array('form_id' => $form_id, 'claim_id' => $claim_id));
        if (count($check) == 0) {
            $this->session->set_flashdata('error', "URL is Altered Somehow");
            redirect(base_url() . "user/list_forms");
        }

        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
        $form = $forms[0];

        $form_info = $this->forms_model->get_where(array('form_id' => $form_id));
        $data['form_value'] = $form_info[0];

        $field_info = $this->form_field_values_model->get_where(array('claim_id' => $claim_id));
        if ($field_info)
            $data['field_value'] = $field_info;

        $email_field = $this->form_field_model->getMailFiledFromID($form_id);
        if ($email_field)
            $data['email_field_name'] = $email_field->field_name;

        $data['total_form_completed'] = $this->getFromProgress($claim_id, $form_id);
        $data['title'] = 'Preview Insurance Form';
        $data['form_id'] = $form_id;
        $data['form'] = $form;
        $data['claim_id'] = $claim_id;
        $data['page_name'] = 'user/answer/show_form_headder';

        $first_question = $this->getFirstCategoryFirstQuestion($form_id);

        if ($first_question !== false) {
            $data['next_question'] = $first_question;
        }

        $this->load->view($this->default_template_name, $data);
    }

    function getFirstCategoryFirstQuestion($form_id) {
        $return = array();
        $obj_cat = new forms_categories_model();
        $get_cat = $obj_cat->get_where_sort(array('form_id' => $form_id), 'asc');

        if (count($get_cat) > 0) {
            $obj_ques = new forms_categories_question_model();
            $get_question = $obj_ques->get_where_sort(array('cat_id' => $get_cat[0]->cat_id), 'asc');

            if (count($get_question) > 0) {
                $return['next_cat'] = $get_cat[0];
                $return['next_question'] = $get_question[0];
                return $return;
                exit();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function getCategoryQuestion($form_id, $obj_cat_id) {
        $return = array();
        $obj_ques = new forms_categories_question_model();
        $get_question = $obj_ques->get_where_sort(array('cat_id' => $obj_cat_id->cat_id), 'asc');

        if (count($get_question) > 0) {
            $return['next_cat'] = $obj_cat_id;
            $return['next_question'] = $get_question[0];
            return $return;
        } else {
            $object = new stdClass();
            $object->cat_id = 'end';
            $object->question_id = 'end';
            $return['next_cat'] = $object;
            $return['next_question'] = $object;
            return $return;
        }
    }

    function getPREVCategoryQuestion($form_id, $obj_cat_id) {
        $return = array();
        $obj_ques = new forms_categories_question_model();
        $get_question = $obj_ques->get_where_sort(array('cat_id' => $obj_cat_id->cat_id), 'desc');
        if (count($get_question) > 0) {
            $return['prev_cat'] = $obj_cat_id;
            $return['prev_question'] = $get_question[0];
            return $return;
        } else {
            return false;
        }
    }

    function ValidateQuestion($question_id) {
        $obj_ques = new forms_categories_question_model();
        $get_question = $obj_ques->get_where(array('question_id' => $question_id));
        $validatin_success = true;
        if (count($get_question) == 1) {
            $question = $get_question[0];
            if ($question->answer_kind == 'checkbox') {
                if ($question->required == 1) {
                    $checkbox_validated = $this->_validate_checkbox_group_required($question);
                    if ($checkbox_validated != true) {
                        $this->form_validation->set_rules($question->question_id . "_", ' answer of this ', 'required');
                        if ($this->form_validation->run() == FALSE) {
                            $validatin_success = false;
                            $this->form_validation->set_value($question->question_id . "_");
                        }
                    }
                }
            } else if ($question->answer_kind == 'date') {
                if ($question->required == 1) {
                    $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                } else {
                    $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                }
                if ($this->form_validation->run() == FALSE) {
                    $validatin_success = false;
                    $this->form_validation->set_value($question->question_id . "_");
                }
            } else if ($question->answer_kind == 'number') {
                if ($question->required == 1) {
                    $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^[0-9]\d*(\,\d{0,2})?$/]');
                } else {
                    $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^[0-9]\d*(\,\d{0,2})?$/]');
                }
                if ($this->form_validation->run() == FALSE) {
                    $validatin_success = false;
                    $this->form_validation->set_value($question->question_id . "_");
                }
            } else {
                if ($question->required == 1) {
                    $this->form_validation->set_rules($question->question_id . "_", ' answer of this ', 'required');
                    if ($this->form_validation->run() == FALSE) {
                        $validatin_success = false;
                        $this->form_validation->set_value($question->question_id . "_");
                    }
                }
            }
        } else {
            $validatin_success = false;
        }
        return $validatin_success;
    }

    function PreviewQuestionListener($claim_id, $form_id, $category_id, $question_id) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $category_id = $this->encrypt->decode($category_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $question_id = $this->encrypt->decode($question_id, '**DOM_CLAIM_DEVREPUBLIC**');


        $check = $this->claims_model->get_where(array('form_id' => $form_id, 'claim_id' => $claim_id));
        if (count($check) == 0) {
            $this->session->set_flashdata('error', "URL is Altered Somehow");
            redirect(base_url() . "user/list_forms");
        }

        $old_cat_id = $this->input->post('old_category_id');

        if ($old_cat_id != '') {
            $validation_check_result = $this->ValidateQuestion($question_id);
            if ($validation_check_result == false) {
                $this->preview_question($claim_id, $form_id, $category_id, $question_id);
            } else {
                $this->saveFomrValues($claim_id, $form_id, $category_id, $question_id);
//                $url = base_url() . 'user/answer/preview_question/' . $claim_id . '/' . $form_id . '/' . $category_id . '/' . $question_id;
//                $url = base_url() . 'user/answer/preview_question/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($category_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($question_id, '**DOM_CLAIM_DEVREPUBLIC**');

                $session = array('claim_id' => $claim_id, 'form_id' => $form_id, 'category_id' => $category_id, 'question_id' => $question_id, 'previous_url' => TRUE);
                $this->session->set_userdata($session);

                //var_dump($this->session->userdata('previous_url'));
                //exit;
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $question_id));

                if ($get_question[0]->answer_kind == 'radio') {
                    $obj_answer = new forms_categories_question_answer_model();
                    $get_answer = $obj_answer->get_where(array('question_id' => $question_id, 'answer_id' => $this->input->post($question_id . '_')));
                    if (count($get_answer) > 0 && ($get_answer[0]->skip_to_questions != NULL || $get_answer[0]->skip_to_questions != '')) {
                        if ($get_answer[0]->skip_to_questions !== '0') {
                            $obj_ques = new forms_categories_question_model();
                            $get_question = $obj_ques->get_where(array('question_id' => $get_answer[0]->skip_to_questions));
                            redirect(base_url() . 'user/answer/preview_question/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($get_question[0]->cat_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($get_answer[0]->skip_to_questions, '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
                        } else {
                            redirect(base_url() . 'user/answer/preview_question/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
                        }
                    } else {
                        $next_question = $this->getNextQuestion($category_id, $question_id, $get_question[0]->sequence);
                        redirect(base_url() . 'user/answer/preview_question/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($next_question['next_cat']->cat_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($next_question['next_question']->question_id, '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
                    }
                } else {
                    $next_question = $this->getNextQuestion($category_id, $question_id, $get_question[0]->sequence);
                    redirect(base_url() . 'user/answer/preview_question/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($next_question['next_cat']->cat_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($next_question['next_question']->question_id, '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
                }
            }
        }
    }

    function saveFomrValues($claim_id, $form_id, $category_id, $question_id) {
        $obj_ques = new forms_categories_question_model();
        $get_question = $obj_ques->get_where(array('question_id' => $question_id));

        $answer = new forms_answers_model();
        $prev_id = $answer->getLastRecordInserted($claim_id);
        if (!empty($prev_id)) {
            $prev_id = $prev_id[0]->question_id;
        } else {
            $prev_id = 0;
        }
        $answers = $answer->get_where(array('question_id' => $question_id, 'claim_id' => $claim_id));
        if (count($answers) > 0) {
            $action = "update";
            $answer = $answers[0];
        } else {
            $action = "save";
        }

        if ($get_question[0]->answer_kind == 'checkbox') {
            $answers_choice_selected_ids = $this->input->post($question_id . "_");
            if ($action != "update") {
                $answer = new forms_answers_model();
            }

            $answer->claim_id = $claim_id;
            $answer->cat_id = $category_id;
            $answer->answer_type = "checkbox";
            $answer->question_id = $question_id;
            $answer_id = -1;
            if ($action == "update") {
                $answer->update();
                $answer_id = $answer->answer_id;
                $answers_choices = $answer->get_answer_choices();
                foreach ($answers_choices as $choice) {
                    $choice->delete();
                }
            } else {
                $answer->prev_question = $prev_id;
                $answer_id = $answer->save();
            }
            if ($answers_choice_selected_ids != false) {
                foreach ($answers_choice_selected_ids as $answer_choice_id) {
                    $forms_answers_details_model = new forms_answers_details_model();
                    $forms_answers_details_model->user_answer_id = $answer_id;
                    $forms_answers_details_model->answer_id = $answer_choice_id;
                    $forms_answers_details_model->save();
                }
            }
        } else {
            if ($action != "update") {
                $answer = new forms_answers_model();
            }
            $answer->claim_id = $claim_id;
            $answer->cat_id = $category_id;
            $answer->answer_type = "";
            $answer->question_id = $question_id;
            $answer->answer_text = $this->input->post($question_id . "_");
            $answer->answer_type = $get_question[0]->answer_kind;
            if ($action == "update") {
                $answer->update();
            } else {
                $answer->prev_question = $prev_id;
                $answer->save();
            }
        }
    }

    function preview_question($claim_id, $form_id, $category_id, $question_id) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $category_id = $this->encrypt->decode($category_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $question_id = $this->encrypt->decode($question_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $check = $this->claims_model->get_where(array('form_id' => $form_id, 'claim_id' => $claim_id));
        if (count($check) == 0) {
            $this->session->set_flashdata('error', "URL is Altered Somehow");
            redirect(base_url() . "user/list_forms");
        }

        if ($category_id != 'end' && $question_id != 'end') {
            $obj_claim = new claims_model();
            $session = $this->session->userdata('policy_holder');
            $get_claim = $obj_claim->get_where(array('claim_id' => $claim_id, 'policy_holder_id' => $session->policy_holder_id, 'form_id' => $form_id));
            if (count($get_claim) == 1) {
                $obj_cat = new forms_categories_model();
                $get_cat = $obj_cat->get_where(array('form_id' => $form_id, 'cat_id' => $category_id));
                if (count($get_cat) == 1) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('cat_id' => $category_id, 'question_id' => $question_id));
                    if (count($get_question) == 1) {
                        if ($get_question[0]->answer_kind == 'radio') {
                            $obj_ans = new forms_categories_question_answer_model();
                            $get_ans = $obj_ans->get_where(array('question_id' => $question_id));
                            $data['answer_radio'] = $get_ans;
                        }

                        if ($get_question[0]->answer_kind == 'checkbox') {
                            $obj_ans = new forms_categories_question_answer_model();
                            $get_ans = $obj_ans->get_where(array('question_id' => $question_id));
                            $data['answer_check_box'] = $get_ans;
                        }

                        if ($get_question[0]->answer_kind == 'text') {
                            $data['answer_text'] = TRUE;
                        }

                        if ($get_question[0]->answer_kind == 'textarea') {
                            $data['answer_textarea'] = TRUE;
                        }

                        if ($get_question[0]->answer_kind == 'date') {
                            $data['answer_date'] = TRUE;
                        }

                        if ($get_question[0]->answer_kind == 'number') {
                            $data['answer_number'] = TRUE;
                        }
                        if ($get_question[0]->answer_kind == 'financieel') {
                            $data['answer_financieel'] = TRUE;
                        }
                        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
                        $data['form'] = $forms[0];
                        $data['category_details'] = $get_cat[0];
                        $data['question_details'] = $get_question[0];
                        $data['claim_id'] = $claim_id;
                        $next_question = $this->getNextQuestion($category_id, $question_id, $get_question[0]->sequence);

                        if ($next_question !== false)
                            $data['next_question'] = $next_question;

                        $check_answer_given = $this->isAnswerGiven($claim_id, $form_id, $question_id);
                        if ($check_answer_given != false) {
                            $data['prev_url'] = $check_answer_given;
                        } else if ($check_answer_given === 0) {
                            $data['prev_url'] = base_url() . 'user/answer/preview_question_start/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');
                        } else {

                            $claim_id1 = $this->session->userdata('claim_id');
                            $form_id1 = $this->session->userdata('form_id');
                            $category_id1 = $this->session->userdata('category_id');
                            $question_id1 = $this->session->userdata('question_id');
                            $url = base_url() . 'user/answer/preview_question/' . $this->encrypt->encode($claim_id1, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id1, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($category_id1, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($question_id1, '**DOM_CLAIM_DEVREPUBLIC**');

                            $is_value = $this->session->userdata('previous_url');

                            if ($is_value == TRUE) {
                                $data['prev_url'] = $url;
                            } else {
                                $prev_question = $this->getPreviousQuestion($category_id, $question_id, $get_question[0]->sequence);
                                if ($prev_question !== false)
                                    $data['prev_question'] = $prev_question;
                            }
                        }

                        $data['total_form_completed'] = $this->getFromProgress($claim_id, $form_id);
                        $data['title'] = 'Forms';
                        $data['page_name'] = 'user/answer/single_question';
                        $this->load->view($this->default_template_name, $data);
                    }
                    else {
                        $this->session->set_flashdata('error', 'Data Not Exits 1');
                        redirect(base_url() . "user/list_forms");
                    }
                } else {
                    $this->session->set_flashdata('error', 'Data Not Exits 2');
                    redirect(base_url() . "user/list_forms");
                }
            } else {
                $this->session->set_flashdata('error', 'Temper with URL');
                redirect(base_url() . "user/list_forms");
            }
        } else {

            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $data['form'] = $forms[0];
            $data['claim_id'] = $claim_id;
            $data['title'] = 'Forms';
            $claim_id1 = $this->session->userdata('claim_id');
            $form_id1 = $this->session->userdata('form_id');
            $category_id1 = $this->session->userdata('category_id');
            $question_id1 = $this->session->userdata('question_id');
            $url = base_url() . 'user/answer/preview_question/' . $this->encrypt->encode($claim_id1, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id1, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($category_id1, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($question_id1, '**DOM_CLAIM_DEVREPUBLIC**');

            $is_value = $this->session->userdata('previous_url');
            if ($is_value == TRUE) {
                $data['prev_url'] = $url;
            } else {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('cat_id' => $category_id, 'question_id' => $question_id));
                $prev_question = $this->getPreviousQuestion($category_id, $question_id, $get_question[0]->sequence);
                if ($prev_question !== false)
                    $data['prev_question'] = $prev_question;
            }

            $data['total_form_completed'] = $this->getFromProgress($claim_id, $form_id);
            $data['page_name'] = 'user/answer/show_form_footer';
            $this->load->view($this->default_template_name, $data);
        }
    }

    function isAnswerGiven($claim_id, $form_id, $question_id) {
        $answer = new forms_answers_model();
        $answers = $answer->get_where(array('question_id' => $question_id, 'claim_id' => $claim_id));

        if (!empty($answers)) {
            if ($answers[0]->prev_question != '0' && $answers[0]->prev_question != NULL) {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $answers[0]->prev_question));
                return base_url() . 'user/answer/preview_question/' . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($get_question[0]->cat_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($get_question[0]->question_id, '**DOM_CLAIM_DEVREPUBLIC**');
            } else {
                return 0;
            }
        } else {
            return false;
        }
    }

    function getNextQuestion($category_id, $question_id, $sequence) {
        $obj_ques = new forms_categories_question_model();
        $get_sequence = $obj_ques->get_where_sort(array('cat_id' => $category_id, 'sequence >' => $sequence), 'asc');
        if (count($get_sequence) > 0) {
            $obj_cat = new forms_categories_model();
            $get_cat = $obj_cat->get_where_sort(array('cat_id' => $category_id), 'asc');
            $return['next_cat'] = $get_cat[0];
            $return['next_question'] = $get_sequence[0];
            return $return;
        } else {
            $obj_cat = new forms_categories_model();
            $get_cat = $obj_cat->get_where(array('cat_id' => $category_id));
            $get_next_cat = $obj_cat->get_where_sort(array('form_id' => $get_cat[0]->form_id, 'sequence >' => $get_cat[0]->sequence), 'asc');
            if (!empty($get_next_cat)) {
                $get = $this->getCategoryQuestion($get_cat[0]->form_id, $get_next_cat[0]);
                $return['next_cat'] = $get['next_cat'];
                $return['next_question'] = $get['next_question'];
                return $return;
            } else {
                $object = new stdClass();
                $object->cat_id = 'end';
                $object->question_id = 'end';
                $return['next_cat'] = $object;
                $return['next_question'] = $object;
                return $return;
            }
        }
    }

    function getPreviousQuestion($category_id, $question_id, $sequence) {
        $obj_ques = new forms_categories_question_model();
        $get_sequence = $obj_ques->get_where_sort(array('cat_id' => $category_id, 'sequence <' => $sequence), 'asc');
        if (count($get_sequence) > 0) {
            $obj_cat = new forms_categories_model();
            $get_cat = $obj_cat->get_where_sort(array('cat_id' => $category_id), 'asc');
            $return['prev_cat'] = $get_cat[0];
            $return['prev_question'] = $get_sequence[count($get_sequence) - 1];
            return $return;
        } else {
            $obj_cat = new forms_categories_model();
            $get_cat = $obj_cat->get_where_sort(array('cat_id' => $category_id), 'asc');
            $get_next_cat = $obj_cat->get_where_sort(array('form_id' => $get_cat[0]->form_id, 'sequence <' => $get_cat[0]->sequence), 'asc');
            if (!empty($get_next_cat)) {
                $get = $this->getPREVCategoryQuestion($get_cat[0]->form_id, $get_next_cat[count($get_next_cat) - 1]);
                $return['prev_cat'] = $get['prev_cat'];
                $return['prev_question'] = $get['prev_question'];
                return $return;
            } else {
                $object = new stdClass();
                $object->cat_id = 'end';
                $object->question_id = 'end';
                $return['prev_cat'] = $object;
                $return['prev_question'] = $object;
                return $return;
            }
        }
    }

    function whole_form_save_action_at_once($claim_id, $form_id) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $action = $this->input->post('action');
        if ($action == 'Opslaan in concept') {
            $this->end_preview_save_all_at_once($claim_id);
        } else if ($action == 'confirm') {
            $this->end_preview_confirm_and_send_at_once($claim_id);
        }
    }

    public function end_preview_confirm_and_send_at_once($claim_id) {
        $save_success = true;
        $claims_model = new claims_model();
        $claims = $claims_model->get_where(array('claim_id' => $claim_id));
        $claim = $claims[0];
        $form = $claim->get_form_object();
        if ($form->show_all_question_at_once == 1) {
            $categories = $form->get_categories();
            foreach ($categories as $category) {
                $save_success = $this->_preview_form_validate_form_post(
                        $claim_id, $claim->form_id, $category->cat_id);
                if ($save_success == false) {
                    break;
                } else {
                    $save_success = $this->_save_form_values($claim_id, $claim->form_id, $category->cat_id);
                }
            }
            if ($save_success == false) {
                $this->view_form($claim_id, $claim->form_id);
            }
        }

        if ($save_success == true) {
            $claim->conform_date = get_current_time()->get_date_for_db();
            $claim->status = "Afgerond";
            $claim->update();
            $client_id = $claim->get_form_object()->client_id;
            $policy_holder_object = $claim->get_policy_holder();
            $this->_send_email_to_policy_holder($client_id, $claim, $policy_holder_object);
            $this->_send_email_to_intermediary($client_id, $claim, $policy_holder_object);
            $this->session->set_flashdata('success', "Formulier " . $claim->get_form_number() . " is afgerond");
            redirect(base_url() . "user/list_forms");
        }
    }

    public function end_preview_save_all_at_once($claim_id) {
        $save_success = true;
        $claims_model = new claims_model();
        $claims = $claims_model->get_where(array('claim_id' => $claim_id));
        $claim = $claims[0];
        $form = $claim->get_form_object();
        if ($form->show_all_question_at_once == 1) {
            $categories = $form->get_categories();
            foreach ($categories as $category) {
                $save_success = $this->_preview_form_validate_form_post($claim_id, $claim->form_id, $category->cat_id);
                if ($save_success == false) {
                    break;
                } else {
                    $save_success = $this->_save_form_values($claim_id, $claim->form_id, $category->cat_id);
                }
            }
            if ($save_success == false) {
                $this->view_form($claim_id, $claim->form_id);
            }
        }

        if ($save_success == true) {
            $claim->status = "open";
            $claim->update();
            $this->session->set_flashdata('success', "Form status is now open");
            redirect(base_url() . "user");
        }
    }

    public function end_preview_one_by_one($claim_id) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');


        $claims_model = new claims_model();
        $claims = $claims_model->get_where(array('claim_id' => $claim_id));
        $claim = $claims[0];

        if (count($claims) == 1) {
            $claim->status = "open";
            $claim->update();
            $this->session->set_flashdata('success', "Form status is now open");
        } else {
            $this->session->set_flashdata('error', "Temper With URL");
        }

        redirect(base_url() . "user/list_forms", 'refresh');
    }

    public function end_preview_confirm_and_send_one_by_one($claim_id) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $claims_model = new claims_model();
        $claims = $claims_model->get_where(array('claim_id' => $claim_id));
        $claim = $claims[0];
        $claim->conform_date = get_current_time()->get_date_for_db();
        $claim->status = "Afgerond";
        if (!valid_email($claim->handler_email)) {
            $get_hendler_email_field = $this->form_field_model->get_where(array('form_id' => $claim->form_id, 'field_type' => 'BE'));
            if (!empty($get_hendler_email_field)) {
                $policy_field_value = $this->form_field_values_model->get_where(
                        array('name' => $get_hendler_email_field[0]->field_name,
                            'claim_id' => $claim->claim_id
                        ));
                if (!empty($policy_field_value)) {
                    if (valid_email(trim($policy_field_value[0]->filed_value))) {
                        $claim->handler_email = trim($policy_field_value[0]->filed_value);
                    }
                }
            }
        }
        $claim->update();
        $client_id = $claim->get_form_object()->client_id;
        $policy_holder_object = $claim->get_policy_holder();
        $this->_send_email_to_policy_holder($client_id, $claim, $policy_holder_object);
        // $this->_send_email_to_intermediary($client_id, $claim, $policy_holder_object);
        if (isset($claim->handler_email) && $claim->handler_email != NULL) {
            $this->_send_email_to_hendler($client_id, $claim, $policy_holder_object);
        }
        $this->session->set_flashdata('success', "Formulier " . $claim->get_form_number() . " is afgerond");
        redirect(base_url() . "user/list_forms");
    }

    public function getFromProgress($claim_id, $form_id) {
        $obj_cat = new forms_categories_model();
        $get_cat = $obj_cat->get_where_sort(array('form_id' => $form_id), 'asc');
        $temp = array();
        $i = 1;
        foreach ($get_cat as $cat) {
            $questions = $this->forms_categories_question_model->get_where_sort(array('cat_id' => $cat->cat_id), 'asc');
            foreach ($questions as $question) {
                $temp[$i] = $question->question_id;
                $i++;
            }
        }
        $answer = new forms_answers_model();
        $prev_id = $answer->getLastRecordInserted($claim_id);
        $id = NULL;
        if (!empty($prev_id)) {
            $cat_ans = new forms_categories_question_answer_model();
            $cat_answers = $cat_ans->get_where(array('question_id' => $prev_id[0]->question_id));
            if (count($cat_answers) > 0) {
                foreach ($cat_answers as $ans) {
                    if ($ans->skip_to_questions !== '0') {
                        $id = $prev_id[0]->question_id;
                    } else {
                        $id = $temp[count($temp)];
                    }
                }
            } else {
                $id = $prev_id[0]->question_id;
            }
        } else {
            $id = 0;
        }

        if ($id != 0) {
            $answer_given = array_keys($temp, $id);
            return round(( $answer_given[0] / count($temp) ) * 100);
        } else {
            return 0;
        }
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                     END SHOW ALL QUESTION ONE BY ONE                    * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                                                                         * 
     *                                                                         *
     *                           END OF PREVIEW FORMS                          *
     *                                                                         *
     *                                                                         *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    function preview_form($claim_id, $form_id, $categoty_id = '') {
        if ($this->input->post('action') == 'Terug') {
            $sequence = $this->input->post('sequence');
            if ($categoty_id != '' && $sequence == '') {
                redirect(base_url('user/answer/preview_form/' . $form_id));
            }
            $go_to = $this->_get_last_category_or_question($sequence);
            if (isset($go_to->go_to_type)) {
                if ($go_to->go_to_type == "C") {
                    $sequence = $this->_remove_last_go_to_item($sequence);
                    $this->sequence = $sequence;
                    $_POST['sequence'] = $sequence;
                    $go_to = $this->_get_last_category_or_question($sequence);
                    if (isset($go_to->go_to_value)) {
                        $this->previous_category = $go_to->go_to_value;
                        $this->sequence = $sequence;
                        $this->_render_preview_form($claim_id, $form_id, $go_to->go_to_value);
                    } else {
                        redirect(base_url('user/answer/preview_form/' . $form_id));
                    }
                } else if ($go_to->go_to_type == 'Q') {
                    /*
                     * $sequence = $this->_remove_last_go_to_item($sequence);
                     * $this->sequence = $sequence; $_POST['sequence'] =
                     * $sequence;
                     */
                    $this->sequence = $sequence;
                    $_POST['sequence'] = $sequence;
                    $go_to = $this->_get_last_category_or_question($sequence);
                    $this->go_to_question($claim_id, $form_id, $go_to->go_to_value, 0);
                    // $this-> ($form_id, $go_to->go_to_value);
                } else {
                    echo "tempering with form is done. invalid go to value";
                }

                /*
                 * if ($go_to->go_to_value == 'end') { $sequence =
                 * $this->_remove_last_go_to_item($sequence); $this->sequence =
                 * $sequence; }
                 */
            }
        } else {
            if ($categoty_id != '') {

                // validate the form
                // get the category id of previous page
                // if it was not start page then the category_id would be there
                $old_category_id = $this->input->post('old_categoty_id');
                // var_dump($old_category_id) ; // . ' |' .$old_category_id .
                // '|' ;
                if ($old_category_id != false) {
                    $validation_check_result = $this->_preview_form_validate_form_post(
                            $claim_id, $form_id, $old_category_id);

                    // validate the form
                    if ($validation_check_result == false) {
                        $sequence = $this->input->post('sequence');
                        $this->sequence = $sequence;
                        $this->go_to_value = '';
                        // dont let the user go to next page
                        $this->_render_preview_form($claim_id, $form_id, $old_category_id);
                    } else {
                        $this->_save_form_values($claim_id, $form_id, $old_category_id);
                        // validation runs
                        if (isset($this->skip_to_category)) {

                            $sequence = $this->_add_category_to_sequence(
                                    $this->skip_to_category);
                            $this->sequence = $sequence;

                            $this->_render_preview_form($claim_id, $form_id, $this->skip_to_category);
                        } else if (isset($this->skip_to_questions)) {

                            $sequence = $this->_add_question_to_sequence(
                                    $this->skip_to_questions);
                            $this->sequence = $sequence;

                            $this->go_to_question($claim_id, $form_id, $this->skip_to_questions, $categoty_id);
                        } else {

                            $sequence = $this->_add_category_to_sequence(
                                    $categoty_id);
                            $this->sequence = $sequence;

                            $this->_render_preview_form($claim_id, $form_id, $categoty_id);
                        }
                    }
                } else {
                    $sequence = $this->_add_category_to_sequence($categoty_id);
                    $this->sequence = $sequence;
                    $this->_render_preview_form($claim_id, $form_id, $categoty_id);
                }
            } else {
                $sequence = $this->input->post('sequence');
                $this->sequence = $sequence;
                $this->_render_preview_form($claim_id, $form_id, $categoty_id);
            }
        }
    }

    private function _remove_last_go_to_item($sequence) {
        if ($sequence != '') {
            $values = explode(",", $sequence);
        }
        unset($values[count($values) - 1]);
        $return_value = implode(',', $values);
        return $return_value;
    }

    private function _get_last_category_or_question($sequence) {
        $return_value = '';
        if ($sequence != '') {
            $values = explode(",", $sequence);
            $return_value = new stdClass();
            foreach ($values as $value) {

                $go_to = explode(":", $value);
                $return_value->go_to_type = $go_to[0];
                $return_value->go_to_value = $go_to[1];
            }
        }
        return $return_value;
    }

    private function _add_category_to_sequence($category_id) {
        $sequence = '';
        if (isset($_POST['sequence'])) {
            $sequence = trim($_POST['sequence']);
        }

        if ($sequence == '') {
            $sequence = 'C:' . $category_id;
        } else {
            $sequence .= ',C:' . $category_id;
        }
        return $sequence;
    }

    private function _add_question_to_sequence($question_id) {
        $sequence = '';
        if (isset($_POST['sequence'])) {
            $sequence = trim($_POST['sequence']);
        }
        if ($sequence == '') {
            $sequence = 'Q:' . $question_id;
        } else {
            $sequence .= ',Q:' . $question_id;
        }
        return $sequence;
    }

    public function go_to_question($claim_id, $form_id, $question_id, $previous_category_id) {
        $forms_categories_question_model = new forms_categories_question_model();
        $questions = $forms_categories_question_model->get_where(
                array('question_id' => $question_id
                ));

        // check if form exists with given id
        $forms_model = new forms_model();
        $forms = $forms_model->get_where(
                array('form_id' => $form_id
                ));

        if (count($forms) == 0) {
            $this->session->set_flashdata('error', 'Form not found');
            redirect('user/answer', 'refresh');
        }

        // check if question exists with given id
        if (count($questions) == 0) {
            $this->session->set_flashdata('error', 'Question not found');
            redirect('user/answer', 'refresh');
        }

        $this->_add_question_to_sequence($question_id);
        $data['sequence'] = '';
        if (isset($this->sequence)) {
            $data['sequence'] = $this->sequence;
        }

        $data['claim_id'] = $claim_id;
        $data['categoty_id'] = $questions[0]->cat_id;
        $data['question'] = $questions[0];
        $data['form'] = $forms[0];
        $data['page_name'] = 'user/answer/preview_question';
        $data['title'] = 'Preview Form';
        $data['previous_category_id'] = $previous_category_id;
        $this->load->view($this->default_template_name, $data);
    }

    public function go_to_question_lietener($claim_id, $form_id, $question_id, $previous_category_id) {
        if ($this->input->post('action') == 'Terug') {
            $sequence = $this->input->post('sequence');

            $go_to = $this->_get_last_category_or_question($sequence);

            if ($go_to->go_to_type == 'Q' && $go_to->go_to_value == $question_id) {
                $sequence = $this->_remove_last_go_to_item($sequence);
                $this->sequence = $sequence;
                $go_to = $this->_get_last_category_or_question($sequence);
            }

            if (isset($go_to->go_to_type)) {
                if ($go_to->go_to_type == 'C') {

                    // $sequence = $this->_remove_last_go_to_item($sequence);
                    // $sequence = $this->_remove_last_go_to_item($sequence);
                    $this->sequence = $sequence;

                    if (isset($go_to->go_to_value)) {
                        $this->previous_category = $go_to->go_to_value;
                        $this->sequence = $sequence;
                        $this->_render_preview_form($claim_id, $form_id, $go_to->go_to_value);
                    } else {
                        $this->_render_preview_form($claim_id, $form_id, '');
                    }
                } else if ($go_to->go_to_type == 'Q') {
                    $sequence = $this->_remove_last_go_to_item($sequence);
                    $this->sequence = $sequence;
                    $go_to = $this->_get_last_category_or_question($sequence);
                    $this->go_to_question($claim_id, $form_id, $go_to->go_to_value, 0);
                } else {
                    echo "tempering with form is done. invalid go to value";
                }
            }
        } else {
            // check if form exists with given id
            $forms_model = new forms_model();
            $forms = $forms_model->get_where(
                    array('form_id' => $form_id
                    ));

            if (count($forms) == 0) {
                $this->session->set_flashdata('error', 'Form not found');
                redirect('user/answer', 'refresh');
            }

            $form = $forms[0];

            $forms_categories_question_model = new forms_categories_question_model();
            $questions = $forms_categories_question_model->get_where(
                    array('question_id' => $question_id
                    ));

            $this->_save_form_values($claim_id, $form_id, $questions[0]->cat_id);
            // check if question exists with given id
            if (count($questions) == 0) {
                $this->session->set_flashdata('error', 'Question not found');
                redirect('user/answer', 'refresh');
            }

            $question = $questions[0];

            // if it is checkbox

            $validatin_success = $this->_validate_question($question);
            // if validations fail then we need to
            // show the same page again
            if ($validatin_success == false) {
                $this->sequence = $this->input->post('sequence');
                $this->go_to_question($claim_id, $form_id, $question_id, $previous_category_id);
            } else {
                $this->sequence = $this->input->post('sequence');

                // if skip to categor is there
                if (isset($answer->skip_to_category)) {
                    $this->_render_preview_form($claim_id, $form_id, $answer->skip_to_category);
                } else if (isset($answer->skip_to_questions)) {
                    $this->go_to_question($claim_id, $form_id, $answer->skip_to_questions, $previous_category_id);
                } else {
                    // go to the next categry of previous category
                    $category_obj = new forms_categories_model();

                    $next_category_id = $category_obj->get_next_category(
                            $form_id, $question->cat_id);
                    if (count($next_category_id) > 0) {
                        $cat_id = $next_category_id[0]->cat_id;
                        $this->_render_preview_form($claim_id, $form_id, $cat_id);
                    } else {
                        $this->_render_preview_form($claim_id, $form_id, 'end');
                    }
                }
            }
        }
    }

    private function _validate_question($question) {
        $validatin_success = TRUE;
        if ($question->answer_kind == 'checkbox') {
            if ($question->required == 1) {
                $checkbox_validated = $this->_validate_checkbox_group_required(
                        $question);
                // if none of the checkboxes were checked
                if ($checkbox_validated != true) {

                    $this->form_validation->set_rules(
                            $question->question_id . "_", ' answer of this ', 'required');

                    if ($this->form_validation->run() == FALSE) {

                        $validatin_success = false;
                        $this->form_validation->set_value(
                                $question->question_id . "_");
                    }
                }
            }
        } else {
            if ($question->required == 1) {
                $this->form_validation->set_rules($question->question_id . "_", ' answer of this ', 'required');
                if ($this->form_validation->run() == FALSE) {

                    $validatin_success = false;
                    $this->form_validation->set_value(
                            $question->question_id . "_");
                } else {
                    
                }
            }

            // if the form validation was successful
            // then check if the question no to go to needs to be
            // set

            if ($question->answer_kind == 'radio') {
                $answer_selected_id = $this->input->post(
                        $question->question_id . "_");
                $forms_categories_question_answer_model = new forms_categories_question_answer_model();
                $answers = $forms_categories_question_answer_model->get_where(
                        array(
                            'answer_id' => $this->input->post(
                                    $question->question_id . "_")
                        ));
                if (!empty($answers)) {
                    $answer = $answers[0];
                }
            }
        }

        return $validatin_success;
    }

    function _preview_form_validate_form_post($claim_id, $form_id, $category_id) {
        /**
         * $validatin_success Variable that maintains whether form was
         * validated or not
         */
        $validatin_success = true;

        // get all the questions in the given category
        $this->load->model('forms_categories_question_model');
        $questions = $this->forms_categories_question_model->selectMoreRecord(
                'cat_id', $category_id);
        if (isset($questions) && count($questions) > 0 && $questions != false) {
            // iterate thru all of them and validate them
            foreach ($questions as $question) {
                // if it is checkbox
                if ($question->answer_kind == 'checkbox') {
                    if ($question->required == 1) {
                        $validatin_success = true;
                        $this->load->model('forms_categories_question_model');
                        $questions = $this->forms_categories_question_model->selectMoreRecord(
                                'cat_id', $category_id);
                        foreach ($questions as $question) {
                            if ($question->answer_kind == 'checkbox') {
                                if ($question->required == 1) {
                                    $checkbox_validated = $this->_validate_checkbox_group_required(
                                            $question);
                                    if ($checkbox_validated != true) {
                                        $this->form_validation->set_rules(
                                                $question->question_id . "_", 'answer of this', 'required');
                                        if ($this->form_validation->run() ==
                                                FALSE) {
                                            $validatin_success = false;
                                            $this->form_validation->set_value(
                                                    $question->question_id . "_");
                                        }
                                    }
                                }
                            } else {
                                if ($question->required == 1) {
                                    $this->form_validation->set_rules(
                                            $question->question_id . "_", ' answer of this ', 'required');
                                    if ($this->form_validation->run() == FALSE) {
                                        $validatin_success = false;
                                        $this->form_validation->set_value(
                                                $question->question_id . "_");
                                    }
                                }
                            }
                        }
                        return $validatin_success;
                        $checkbox_validated = $this->_validate_checkbox_group_required(
                                $question);
                        if ($checkbox_validated != true) {
                            $this->form_validation->set_rules(
                                    $question->question_id . "_", ' answer of this ', 'required');
                            if ($this->form_validation->run() == FALSE) {
                                $validatin_success = false;
                                $this->form_validation->set_value(
                                        $question->question_id . "_");
                            }
                        }
                    }
                } else {
                    if ($question->required == 1) {
                        $this->form_validation->set_rules(
                                $question->question_id . "_", ' answer of this ', 'required');
                        if ($this->form_validation->run() == FALSE) {
                            $validatin_success = false;
                            $this->form_validation->set_value(
                                    $question->question_id . "_");
                        }
                    }

                    if ($question->answer_kind == 'radio') {
                        $answer_selected_id = $this->input->post(
                                $question->question_id . "_");
                        $forms_categories_question_answer_model = new forms_categories_question_answer_model();
                        $answers = $forms_categories_question_answer_model->get_where(
                                array(
                                    'answer_id' => $this->input->post(
                                            $question->question_id . "_")
                                ));
                        if (count($answers) > 0) {
                            $answer = $answers[0];
                            if (isset($answer->skip_to_category)) {
                                $this->skip_to_category = $answer->skip_to_category;
                            } else if (isset($answer->skip_to_questions)) {
                                $this->skip_to_questions = $answer->skip_to_questions;
                            }
                        }
                    }
                }
            }
        } else {
            $validatin_success = true;
        }
        return $validatin_success;
    }

    function _validate_checkbox_group_required($question) {
        $checkbox_group_validation_passed = false;
        $checkbox_count = (int) $this->input->post(
                        $question->question_id . '_count');
        // check if its required
        if ($question->required == 1) {

            $checkbox_group_validation_passed = false;
            // iterate thru all the checkboxes
            for ($i = 1; $i <= $checkbox_count; $i++) {
                $checkbox_name = $question->question_id . "_" . $i;

                // $this->form_validation->set_rules('77_[]', '',
                // 'required|xss_clean');
                // if checked
                if ($this->input->post($checkbox_name)) {
                    $checkbox_group_validation_passed = true;
                }
            }
        }

        return $checkbox_group_validation_passed;
    }

    function _render_preview_form($claim_id, $form_id, $categoty_id) {
        $this->load->model('forms_categories_model');
        $this->load->model('forms_categories_question_model');

        // $obj = new forms_model();
        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
        $form = $forms[0];

        $category_obj = new forms_categories_model();
        $next_category_id = '';
        if ($categoty_id == '' || $categoty_id == 'end') {
            $next_category_id = $category_obj->get_next_category($form_id);
        } else {
            $next_category_id = $category_obj->get_next_category($form_id, $categoty_id);
        }

        $categories = $category_obj->selectMoreRecord('cat_id', $categoty_id);
        $category = $categories[0];

        $data['title'] = 'Preview Insurance Form';
        $data['form_id'] = $form_id;
        $data['form'] = $form;

        // get the category if category id was set
        if ($categoty_id != 'end') {
            
        }

        // / if it had next category to be shown
        if (count($next_category_id) > 0) {
            $data['next_category_id'] = $next_category_id[0]->cat_id;
        } else {
            $data['next_category_id'] = '#';
        }

        if (isset($category)) {

            $data['categoryies'][] = $category;

            $questions = $this->forms_categories_question_model->selectMoreRecord(
                    'cat_id', $categoty_id);
            $data['questions'] = $questions;
        }

        // if category is empty then we are on first page
        if (!$categoty_id && trim($categoty_id) == '') {
            // if the show all is enabled
            if ($form->show_all_question_at_once == 1) {
                $questions = array();
                $data['categoryies'] = array();
                $data['categoryies'] = $form->get_categories();
            }
        }

        $data['categoty_id'] = $categoty_id;
        $data['sequence'] = '';
        if (isset($this->sequence)) {
            $data['sequence'] = $this->sequence;
        }
        // get the information about the form
        $form_info = $this->forms_model->get_where(
                array('form_id' => $form_id
                ));
        $data['form_value'] = $form_info[0];
        // get the information about the forms fields
        $field_info = $this->form_field_values_model->get_where(
                array('claim_id' => $claim_id
                ));
        $data['field_value'] = $field_info;
        // get the email field name.
        $email_field = $this->form_field_model->getMailFiledFromID($form_id);
        $data['email_field_name'] = $email_field->field_name;
        // $data['header_info'] = $this->makeHeaderText($claim_id);
        $data['claim_id'] = $claim_id;
        $data['page_name'] = 'user/answer/index';
        $this->load->view($this->default_template_name, $data);
    }

    function _save_form_values($claim_id, $form_id, $category_id) {
        $validatin_success = true;

        $this->load->model('forms_answers_model');
        $this->load->model('forms_answers_details_model');

        // get all the questions in the given category
        $this->load->model('forms_categories_question_model');

        $questions = $this->forms_categories_question_model->selectMoreRecord(
                'cat_id', $category_id);
        if ($questions) {
            // iterate thru all of them and validate them
            foreach ($questions as $question) {
                $action = "save";

                // check if ans answer already exists
                $answer = new forms_answers_model();
                $answers = $answer->get_where(
                        array('question_id' => $question->question_id,
                            'claim_id' => $claim_id
                        )); // get the result
                if (count($answers) > 0) {
                    $action = "update";
                    $answer = $answers[0];
                }

                // echo $question->question_id;
                // $answer->claim_id= $claim_id;
                // $answer->cat_id = $category_id;
                // if it is checkbox
                if ($question->answer_kind == 'checkbox') {
                    $answers_choice_selected_ids = $this->input->post(
                            $question->question_id . "_");
                    if ($action != "update") {
                        $answer = new forms_answers_model();
                    }
                    $answer->claim_id = $claim_id;
                    $answer->cat_id = $category_id;
                    $answer->answer_type = "checkbox";
                    $answer->question_id = $question->question_id;
                    $answer_id = -1;
                    if ($action == "update") {

                        $answer->update(); // update the record
                        // and get the answer id
                        $answer_id = $answer->answer_id;
                        // we need to delete all the selected choices for the
                        // given
                        // answer
                        // as they already exists in the database
                        $answers_choices = $answer->get_answer_choices();

                        // iterate thru all the choices user has selected and
                        // delete
                        // them
                        foreach ($answers_choices as $choice) {
                            $choice->delete();
                        }
                    } else {
                        $answer_id = $answer->save();
                    }
                    if ($answers_choice_selected_ids != false) {
                        foreach ($answers_choice_selected_ids as $answer_choice_id) {
                            $forms_answers_details_model = new forms_answers_details_model();
                            $forms_answers_details_model->user_answer_id = $answer_id;
                            // id of answer entry for policy hoder

                            $forms_answers_details_model->answer_id = $answer_choice_id;
                            // id of choice selected by the user

                            $forms_answers_details_model->save();
                        }
                    }
                } else {
                    if ($action != "update") {
                        $answer = new forms_answers_model();
                    }
                    $answer->claim_id = $claim_id;
                    $answer->cat_id = $category_id;
                    $answer->answer_type = "";
                    $answer->question_id = $question->question_id;
                    $answer->answer_text = $this->input->post(
                            $question->question_id . "_");

                    $answer->answer_type = $question->answer_kind;
                    if ($action == "update") {
                        $answer->update();
                    } else {
                        $answer->save();
                    }
                }
            }
        } else {
            $validatin_success = true;
        }
        return $validatin_success;
    }

    /*
     * ================ Method Start for not show all at once  ===================
     */

    function whole_form_save_action($claim_id, $form_id) {
        $action = $this->input->post('action');

        if ($action == 'Opslaan in concept') {
            $this->end_preview_save($claim_id);
        } else if ($action == 'confirm') {
            $this->end_preview_confirm_and_send($claim_id);
        }
    }

    public function end_preview_save($claim_id) {
        $save_success = true;
        $claims_model = new claims_model();
        $claims = $claims_model->get_where(
                array('claim_id' => $claim_id
                ));
        $claim = $claims[0];
        $form = $claim->get_form_object();
        if ($form->show_all_question_at_once == 1) {
            $categories = $form->get_categories();
            foreach ($categories as $category) {
                $save_success = $this->_preview_form_validate_form_post(
                        $claim_id, $claim->form_id, $category->cat_id);
                if ($save_success == false) {
                    break;
                } else {
                    $save_success = $this->_save_form_values($claim_id, $claim->form_id, $category->cat_id);
                }
            }
            if ($save_success == false) {
                $this->preview_form($claim_id, $claim->form_id);
            }
        }

        if ($save_success == true) {
            $claim->status = "open";
            $claim->update();
            $this->session->set_flashdata('success', "Form status is now open");
            redirect(base_url() . "user/");
        }
    }

    public function end_preview_confirm_and_send($claim_id) {
        $save_success = true;

        // get the claim for given id
        $claims_model = new claims_model();
        $claims = $claims_model->get_where(
                array('claim_id' => $claim_id
                ));

        // / get the first claim from array
        $claim = $claims[0];

        // if show all questions at once is true then
        // save/update the values entered by the user
        $form = $claim->get_form_object();
        if ($form->show_all_question_at_once == 1) {

            $categories = $form->get_categories();
            foreach ($categories as $category) {
                $save_success = $this->_preview_form_validate_form_post(
                        $claim_id, $claim->form_id, $category->cat_id);
                if ($save_success == false) {
                    break;
                } else {
                    $save_success = $this->_save_form_values($claim_id, $claim->form_id, $category->cat_id);
                }
            }

            if ($save_success == false) {
                $this->preview_form($claim_id, $claim->form_id);
            }
        }

        if ($save_success == true) {

            // set the status to completed

            $claim->conform_date = get_current_time()->get_date_for_db();
            $claim->status = "Afgerond";

            // update the value in database

            $claim->update();

            // send email to the policy holder as well as the intermediary
            // populate client id and policy holder object for use in sending
            // email
            $client_id = $claim->get_form_object()->client_id;
            $policy_holder_object = $claim->get_policy_holder();

            $this->_send_email_to_policy_holder($client_id, $claim, $policy_holder_object);
            $this->_send_email_to_intermediary($client_id, $claim, $policy_holder_object);

            // set the success message and redirect
            $this->session->set_flashdata('success', "Formulier " . $claim->get_form_number() . " is afgerond");
            redirect(base_url() . "user/list_forms");
        }
    }

    /*
     * ================ Method end for not show all at once ===================
     */

    private function _send_email_to_policy_holder($client_id, $claim, $policy_holder_object) {

        $admin_setting_info = $this->client_setting_model->get_where(
                array('client_id' => $client_id
                ));

        $smtp_user = (string) $admin_setting_info[0]->email;
        $smtp_pass = (string) $admin_setting_info[0]->password;
        $smtp_server = (string) $admin_setting_info[0]->smtp_server;
        $smtp_port = $admin_setting_info[0]->smtp_port;
        //get client logo
        $client_info = $this->clients_model->get_where(array('client_id' => $client_id));
        $client_logo = $admin_setting_info[0]->mail_logo;
        $logo_path = base_url() . 'assets/upload/mail_logo/' . $client_logo;
        $logo_exist = 'assets/upload/mail_logo/' . $client_logo;
        if (isset($client_logo) && $client_logo !== '') {
            if (file_exists($logo_exist)) {
                $type = pathinfo($logo_exist, PATHINFO_EXTENSION);
                $data = file_get_contents($logo_exist);
                // $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $base64 = base_url() . 'assets/upload/mail_logo/' . $client_logo;
                $logo_url = '<img src="' . $base64 . '" border="0" style="display: block; max-width: 200px;">';
            } else {
                $logo_url = '';
            }
        } else {
            $logo_url = '';
        }
        //end client logo
        // get the template
        $this->load->model('client_email_template_model');
        $client_email_templates = new client_email_template_model();
        $client_email_templates = $client_email_templates->get_where(
                array('client_id' => $client_id,
                    'email_type' => 'Form Completed (policy)'
                ));

        // if the template was not found
        if (count($client_email_templates) == 0) {
            echo 'The mail template with name "Form Completed (policy)" was not found for sending email.';
            exit();
            $this->session->set_flashdata('error', 'The mail template with name "Form Completed (policy)" was not found for sending email.');
            redirect(base_url() . "user/list_forms/index");
        }

        $template = $client_email_templates[0];

        // get the message get the policy number in the message
        $message = $template->email_message;
        // get the policy number from the ins_form_field_value.
        $get_policy_num_field = $this->form_field_model->get_where(
                array('form_id' => $claim->form_id, 'field_type' => 'P'
                ));
        $get_hendler_email_field = $this->form_field_model->get_where(
                array('form_id' => $claim->form_id, 'field_type' => 'BE'
                ));
        if (!empty($get_policy_num_field)) {
            $policy_field_value = $this->form_field_values_model->get_where(
                    array('name' => $get_policy_num_field[0]->field_name,
                        'claim_id' => $claim->claim_id
                    ));
            $policy_number = @$policy_field_value[0]->filed_value;
        } else {
            $policy_number = "";
        }
        if (!empty($get_hendler_email_field)) {
            $policy_field_value = $this->form_field_values_model->get_where(
                    array('name' => $get_hendler_email_field[0]->field_name,
                        'claim_id' => $claim->claim_id
                    ));
            $hendler_email = @$policy_field_value[0]->filed_value;
        } else {
            $hendler_email = "";
        }
        $send_mail = $policy_holder_object->email;
        $message = str_replace('&lt;logo&gt;', $logo_url, $message);
        $message = str_replace('&lt;policy_number&gt;', $policy_number, $message);
        // set the form number in message
        $message = str_replace('&lt;form_number&gt;', $claim->get_form_number(), $message);

        // load the hlper that generates the pdf of the form that
        // has been filled by the policy holder
        $this->create_pdf_for_claim($claim->claim_id);

        $attechment = array();

        // construct the path to the pdf
        $this->load->helper('path');
        $link_to_claim_pdf = set_realpath("assets/claims_pdf_files/");
        $attechment[] = $link_to_claim_pdf . $claim->get_form_number() .
                '_Digitaal_op_Maat.pdf';
        // get all the atteched file.

        $link_to_claim_attechement = set_realpath("assets/claim_files/");
        $claim_attechment_info = $this->claims_files_model->get_where(
                array('claim_id' => $claim->claim_id
                ));
        foreach ($claim_attechment_info as $claim_attechment) {
            if (file_exists(
                            "./assets/claim_files/" . $claim_attechment->file_name)) {
                $attechment[] = $link_to_claim_attechement .
                        $claim_attechment->file_name;
            }
        }

        $sent_msg = dynamic_send_mail($send_mail, $template->email_subject, $message, '', $attechment, $smtp_user, $smtp_pass, $smtp_server, $smtp_port);
    }

    private function _send_email_to_intermediary($client_id, $claim, $policy_holder_object) {

        // get the template
        $admin_setting_info = $this->client_setting_model->get_where(
                array('client_id' => $client_id
                ));

        $smtp_user = (string) $admin_setting_info[0]->email;
        $smtp_pass = (string) $admin_setting_info[0]->password;
        $smtp_server = (string) $admin_setting_info[0]->smtp_server;
        $smtp_port = $admin_setting_info[0]->smtp_port;

        //get client logo
        $client_info = $this->clients_model->get_where(array('client_id' => $client_id));
        $client_logo = $admin_setting_info[0]->mail_logo;
        $logo_path = base_url() . 'assets/upload/mail_logo/' . $client_logo;
        $logo_exist = 'assets/upload/mail_logo/' . $client_logo;
        if (isset($client_logo) && $client_logo !== '') {
            if (file_exists($logo_exist)) {
                $type = pathinfo($logo_exist, PATHINFO_EXTENSION);
                $data = file_get_contents($logo_exist);
                // $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $base64 = base_url() . 'assets/upload/mail_logo/' . $client_logo;
                $logo_path = '<a herf="#"><img src="' . $base64 . '" style="max-width: 300px;"></a>';
                $logo_url = '<img src="' . $base64 . '" border="0" style="display: block; max-width: 200px;">';
            } else {
                $logo_url = '';
            }
        } else {
            $logo_url = '';
        }
        //end client logo

        $this->load->model('client_email_template_model');

        // load settings tab to get the email address
        $this->load->model('client_setting_model');
        $client_setting_model = new client_setting_model();
        $client_settings = $client_setting_model->get_where(
                array('client_id' => $client_id
                ));
        $client_setting = $client_settings[0];

        $this->load->model('clients_model');
        $clients_model = new clients_model();
        $clients = $clients_model->get_where(
                array('client_id' => $client_id
                ));
        $client = $clients[0];

        $client_email_templates = new client_email_template_model();
        $client_email_templates = $client_email_templates->get_where(
                array('client_id' => $client_id,
                    'email_type' => 'Form Completed (insurance)'
                ));

        // if the template was not found
        if (count($client_email_templates) == 0) {
            echo 'The mail template with name "Form Completed (insurance)" was not found for sending email.';
            exit();
            $this->session->set_flashdata('error', 'The mail template with name "Form Completed (insurance)" was not found for sending email.');
            redirect(base_url() . "user/list_forms/index");
        }

        $template = $client_email_templates[0];

        $message = $template->email_message; // get the message
        // get the policy number in the
        // message
        // get the policy number from the
        // ins_form_field_value.
        $get_policy_num_field = $this->form_field_model->get_where(
                array('form_id' => $claim->form_id, 'field_type' => 'P'
                ));
        if (!empty($get_policy_num_field)) {
            $policy_field_value = $this->form_field_values_model->get_where(
                    array('name' => $get_policy_num_field[0]->field_name,
                        'claim_id' => $claim->claim_id
                    ));
            $policy_number = $policy_field_value[0]->filed_value;
        } else {
            $policy_number = "";
        }

        $message = str_replace('&lt;policy_number&gt;', $policy_number, $message);
        $message = str_replace('&lt;logo&gt;', $logo_url, $message);

        // set the form number in message
        $message = str_replace('&lt;form_number&gt;', $claim->get_form_number(), $message);
        // $this->create_pdf_for_claim($claim->claim_id);
        // load the hlper that generates the pdf of the form that
        // has been filled by the policy holder
        // create_pdf($claim->claim_id);
        // construct the path to the pdf
        $attechment1 = array();
        $this->load->helper('path');
        $link_to_claim_pdf2 = set_realpath("assets/claims_pdf_files/");
        $attechment1[] = $link_to_claim_pdf2 . $claim->get_form_number() .
                '_Digitaal_op_Maat.pdf';

        $link_to_claim_attechement = set_realpath("assets/claim_files/");
        $claim_attechment_info = $this->claims_files_model->get_where(
                array('claim_id' => $claim->claim_id
                ));
        foreach ($claim_attechment_info as $claim_attechment) {
            if (file_exists(
                            "./assets/claim_files/" . $claim_attechment->file_name)) {
                $attechment1[] = $link_to_claim_attechement .
                        $claim_attechment->file_name;
            }
        }

        $sent_msg = dynamic_send_mail($client_setting->email, $template->email_subject, $message, '', $attechment1, $smtp_user, $smtp_pass, $smtp_server, $smtp_port);
    }

//================send Email to the hendler start=========================
    private function _send_email_to_hendler($client_id, $claim, $policy_holder_object) {
        // get the template
        $admin_setting_info = $this->client_setting_model->get_where(
                array('client_id' => $client_id
                ));

        $smtp_user = (string) $admin_setting_info[0]->email;
        $smtp_pass = (string) $admin_setting_info[0]->password;
        $smtp_server = (string) $admin_setting_info[0]->smtp_server;
        $smtp_port = $admin_setting_info[0]->smtp_port;

        //get client logo
        $client_info = $this->clients_model->get_where(array('client_id' => $client_id));
        $client_logo = $admin_setting_info[0]->mail_logo;
        $logo_path = base_url() . 'assets/upload/mail_logo/' . $client_logo;
        $logo_exist = 'assets/upload/mail_logo/' . $client_logo;
        if (isset($client_logo) && $client_logo !== '') {
            if (file_exists($logo_exist)) {
                $type = pathinfo($logo_exist, PATHINFO_EXTENSION);
                $data = file_get_contents($logo_exist);
                //$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $base64 = base_url() . 'assets/upload/mail_logo/' . $client_logo;
                $logo_path = '<a herf="#"><img src="' . $base64 . '" style="max-width: 300px;"></a>';
                $logo_url = '<img src="' . $base64 . '" border="0" style="display: block; max-width: 200px;">';
            } else {
                $logo_url = '';
            }
        } else {
            $logo_url = '';
        }
        //end client logo

        $this->load->model('client_email_template_model');

        // load settings tab to get the email address
        $this->load->model('client_setting_model');
        $client_setting_model = new client_setting_model();
        $client_settings = $client_setting_model->get_where(
                array('client_id' => $client_id
                ));
        $client_setting = $client_settings[0];

        $this->load->model('clients_model');
        $clients_model = new clients_model();
        $clients = $clients_model->get_where(
                array('client_id' => $client_id
                ));
        $client = $clients[0];

        $client_email_templates = new client_email_template_model();
        $client_email_templates = $client_email_templates->get_where(
                array('client_id' => $client_id,
                    'email_type' => 'Form Completed (insurance)'
                ));

        // if the template was not found
        if (count($client_email_templates) == 0) {
            echo 'The mail template with name "Form Completed (insurance)" was not found for sending email.';
            exit();
            $this->session->set_flashdata('error', 'The mail template with name "Form Completed (insurance)" was not found for sending email.');
            redirect(base_url() . "user/list_forms/index");
        }

        $template = $client_email_templates[0];

        $message = $template->email_message; // get the message
        // get the policy number in the
        // message
        // get the policy number from the
        // ins_form_field_value.
        $get_policy_num_field = $this->form_field_model->get_where(
                array('form_id' => $claim->form_id, 'field_type' => 'P'
                ));
        if (!empty($get_policy_num_field)) {
            $policy_field_value = $this->form_field_values_model->get_where(
                    array('name' => $get_policy_num_field[0]->field_name,
                        'claim_id' => $claim->claim_id
                    ));
            $policy_number = $policy_field_value[0]->filed_value;
        } else {
            $policy_number = "";
        }

        $message = str_replace('&lt;policy_number&gt;', $policy_number, $message);
        $message = str_replace('&lt;logo&gt;', $logo_url, $message);

        // set the form number in message
        $message = str_replace('&lt;form_number&gt;', $claim->get_form_number(), $message);
        // $this->create_pdf_for_claim($claim->claim_id);
        // load the hlper that generates the pdf of the form that
        // has been filled by the policy holder
        // create_pdf($claim->claim_id);
        // construct the path to the pdf
        $attechment1 = array();
        $this->load->helper('path');
        $link_to_claim_pdf2 = set_realpath("assets/claims_pdf_files/");
        $attechment1[] = $link_to_claim_pdf2 . $claim->get_form_number() .
                '_Digitaal_op_Maat.pdf';

        $link_to_claim_attechement = set_realpath("assets/claim_files/");
        $claim_attechment_info = $this->claims_files_model->get_where(
                array('claim_id' => $claim->claim_id
                ));
        foreach ($claim_attechment_info as $claim_attechment) {
            if (file_exists(
                            "./assets/claim_files/" . $claim_attechment->file_name)) {
                $attechment1[] = $link_to_claim_attechement .
                        $claim_attechment->file_name;
            }
        }

        $sent_msg = dynamic_send_mail($claim->handler_email, $template->email_subject, $message, '', $attechment1, $smtp_user, $smtp_pass, $smtp_server, $smtp_port);
    }

    //=========================send Email to hendler end ===============


    function generate_html($claim_id) {
        define('EURO', chr(128));
        $claims_model = new claims_model();
        $claims = $claims_model->get_where(
                array('claim_id' => $claim_id
                ));
        $claim = $claims[0];
        $form_id = $claim->form_id;
        $form_info = $this->forms_model->get_where(
                array('form_id' => $form_id
                ));
        $intro_text = @$form_info[0]->introduction_text;
        $intro_text = str_replace('<pre>', '', $intro_text);
        $intro_text = str_replace('</pre>', '', $intro_text);
        $closer_text = @$form_info[0]->closure_text;
        $closer_text = str_replace('<pre>', '', $closer_text);
        $closer_text = str_replace('</pre>', '', $closer_text);
        $records = $this->clients_model->get_where(
                array('client_id' => $form_info[0]->client_id
                ));
        $admin_setting_info = $this->client_setting_model->get_where(array('client_id' => $claim->client_id));
        $fonttype = NULL;
        if ($admin_setting_info[0]->letter_type) {
            $fonttype = $admin_setting_info[0]->letter_type;
        } else {
            $fonttype = 'sans-serif';
        }
        $category_info = $this->forms_categories_model->get_where_sort(
                array('form_id' => $form_id
                ), 'asc');
        $logo = 'assets/upload/pdf_logo/' . $admin_setting_info[0]->pdf_logo;

        $config_image['image_library'] = 'gd2';
        $config['source_image'] = $logo;
        // $config['new_image'] ='assets/upload/client_logo/thumb_'.
        // $records[0]->logo;
        $config['maintain_ratio'] = TRUE;
        $config['create_thumb'] = TRUE;
        $config['width'] = 150;
        $config['height'] = 150;
        $this->image_lib->initialize($config);
        $this->load->library('image_lib', $config);
        // $resize= $this->image_lib->resize();

        $return = NULL;
        $return = '<html><head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> ';
        $style = NULL;
        $style.=' <style>
    #footer { bottom: 0px; margin-top:-20px; position:fixed; right: 10px;  margin: 0; padding: 0; text-align:right; font-size:12px}
 </style>';


        $style.='<style type="text/css">';

        if (!empty($admin_setting_info[0]->pdf_template)) {
            $pdf_template_fcpath = FCPATH . 'assets/upload/pdf_template/' . $admin_setting_info[0]->pdf_template;
            if (file_exists($pdf_template_fcpath) == true) {
                $pdf_template = base_url() . 'assets/upload/pdf_template/' . $admin_setting_info[0]->pdf_template;
            }
        }

        if (isset($pdf_template)) {
            $style.='body{background-image: url(' . $pdf_template . ');}';
        }

        $style.='#main_table_contain{';
        if (!empty($admin_setting_info[0]->top_margin)) {
            $style.='margin-top:' . $admin_setting_info[0]->top_margin . 'px;';
        }
        if (!empty($admin_setting_info[0]->bottom_margin)) {
            $style.='margin-bottom:' . $admin_setting_info[0]->bottom_margin . 'px;';
        }
        if (!empty($admin_setting_info[0]->left_margin)) {
            $style.='margin-left:' . $admin_setting_info[0]->left_margin . 'px;';
        }
        if (!empty($admin_setting_info[0]->right_margin)) {
            $style.='margin-right:' . $admin_setting_info[0]->right_margin . 'px;';
        }

        $style.='}</style>';

        $return.=$style;
        $return.='</head><body>';
        $return.='<div id="main_table_contain" class="page">';
        $return.= '<table style="font-family: ' . $fonttype . '; font-size: 14.2667px; width: 100%;" border="0" cellspacing="6" cellspadding="3">';
        if (!$this->image_lib->resize()) {
            $return .= '<tr><th width="1%">&nbsp;</th><th colspan="2">' .
                    $this->image_lib->display_errors() . '</th></tr>';
        } else {
            $client_logo = substr($admin_setting_info[0]->pdf_logo, 0, strlen($admin_setting_info[0]->pdf_logo) - 4) . '_thumb' .
                    substr($admin_setting_info[0]->pdf_logo, -4);
            $client_logo_url = FCPATH . 'assets/upload/pdf_logo/' .
                    $client_logo;
            if (file_exists($client_logo_url) == true) {
                $client_logo_url_true = base_url() . 'assets/upload/pdf_logo/' .
                        $client_logo;
            } else {
                $return .= '<tr><th width="1%">&nbsp;</th><th colspan="2">IMAGE PATH NOT FOUND</th></tr>';
            }
        }

        if (isset($client_logo_url_true)) {
            $return .= '<tr><th width="1%" style="text-align:left"><img  src="' .
                    $client_logo_url_true . '"></th><th colspan="2">&nbsp;</th></tr>';
        } else {
            $return .= '<tr><th width="1%">&nbsp;</th><th colspan="2">No LOGO FOUND</th></tr>';
        }

        $return .= '<tr><th colspan="3"><h2>' . ucfirst(
                        $form_info[0]->form_name) . '<h2></th></tr>
        <tr>
        <td colspan="3">' .
                $this->makeHeaderText_pdf($claim_id) .
                '</td>
        </tr>
        <tr>
        <td colspan="3"><hr>' .
                @$intro_text . '</td>
        </tr>
        <tr>
        <td colspan="3">&nbsp;</td>
        </tr>';

        foreach ($category_info as $cat_info) {
            $answers = $claim->get_answers_by_cat($cat_info->cat_id, $claim_id);
            if (is_array($answers) && !empty($answers)) {
                $return .= '<tr><td colspan="3" style="font-weight: bold;"><hr>' .
                        ucfirst(@$cat_info->cat_name) . '</td></tr>';
                foreach ($answers as $answer) {
                    $question = $answer->get_question();
                    $return .= '<tr><td valign="top" collspan="2" style="padding-right:10px; width:50%">' .
                            strip_tags($question->question) . '</td>';

                    if ($answer->answer_type == 'text' ||
                            $answer->answer_type == 'textarea' || $answer->answer_type == 'date' || $answer->answer_type == 'number') {
                        $return .= '<td valign="top" style="width:50%">' . $answer->answer_text .
                                '</td></tr>';
                    } else if ($answer->answer_type == 'financieel') {
                        $return .= '<td valign="top" style="width:50%">' . $answer->answer_text . '</td></tr>';
                    } else if ($answer->answer_type == 'radio') {
                        $ans_id = $answer->answer_text;
                        if (isset($ans_id) && ( $ans_id !== 0 )) {
                            $radio_ans = $this->forms_categories_question_answer_model->get_where(
                                    array('answer_id' => $ans_id
                                    ));
                            $return .= '<td valign="top" style="width:50%">' . @$radio_ans[0]->answer .
                                    '</td></tr>';
                        }
                    } else {
                        $choices = $answer->get_answer_choices();
                        $checkbox_answer = null;
                        if (is_array($choices) && count($choices) > 0) {

                            foreach ($choices as $choice) {
                                $checkbox_answer .= $choice->get_selected_item_details()->answer .
                                        '<br />';
                            }

                            $return .= '<td valign="top" style="width:50%">' . @$checkbox_answer .
                                    '</td></tr>';
                            $checkbox_answer = null;
                        } else {
                            $return .= '<td>&nbsp;</td></tr>';
                        }
                    }
                }
            }
        }
        $page_footer_date = date("Y-m-d H:i:s") . ' - ' . $form_id;
        $return .= '<tr><td colspan="3">&nbsp;</td></tr>
    <tr>
    <td colspan="3">' . @$closer_text . '</td>
    </tr>

    </table>';
        $return.='</div>
       </div><div id="footer">
    <p class="page">' . $page_footer_date . ' </p>
  </div>
        </body>
        </html>';
        return $return;
    }

    function create_pdf_for_claim($claim_id) {
        $claims_model = new claims_model();
        $claims = $claims_model->get_where(array('claim_id' => $claim_id
                ));
        $claim = $claims[0];
        $html = $this->generate_html($claim_id);
        $this->load->helper('dompdf');
        $this->load->helper('dompdf/file');
        $this->load->helper('dompdf/dompdf_lib_include');
        dompdf_lib_include();

        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();

        $path = realpath('assets/claims_pdf_files');
        $file_to_save = $path . '/' . $claim->get_form_number() . '_Digitaal_op_Maat.pdf';
// save the pdf file on the server
        file_put_contents($file_to_save, $dompdf->output());
    }

    public function upload_attachment($claim_id) {

        $form_id = $this->input->post('form_id');

        $form_field_name = 'attachment';

        $this->load->model('claims_files_model');
        $upload_config = claims_files_model::get_upload_config();

// try uploadng the file
// if file is selected then only the upload will be done
// it would be success if no file was selested
        $logo_upload_status = my_file_upload($form_field_name, $upload_config, $this);

        $success = true;

        if ($logo_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $success = false;

            $this->session->set_flashdata('file_errors', $logo_upload_status['data']);
            echo '<script type = "text/javascript">alert(\'' .
            htmlentities($logo_upload_status['data']) .
            '\'); window.location.href=\'' . base_url() .
            "user/answer/preview_form/$claim_id/$form_id/end" . '\'</script>';
            // redirect(base_url() .
            // "user/answer/preview_form/$claim_id/$form_id/end");
        }

        if (!isset($logo_upload_status['data'])) {
            $success = false;
            $this->session->set_flashdata('file_errors', "Selecteer eerst het bestand dat u wilt toevoegen.");

            $this->load->model('forms_model');
            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $form = $forms[0];

            if ($form->show_all_question_at_once == 1) {
                echo '<script type="text/javascript">alert(\'Selecteer eerst het bestand dat u wilt toevoegen.\'); window.location.href=\'' .
                base_url() . "user/answer/view_form/$claim_id/$form_id" .
                '\'</script>';
            } else {
                echo '<script type="text/javascript">alert(\'Selecteer eerst het bestand dat u wilt toevoegen.\'); window.location.href=\'' .
                base_url() . "user/answer/preview_form/$claim_id/$form_id/end" .
                '\'</script>';
            }

            flush();
        }

        if ($success == true) {
            $file_name = $logo_upload_status['data'];
            $claims_files_model = new claims_files_model();
            $claims_files_model->file_name = $file_name;
            $claims_files_model->claim_id = $claim_id;
            $claims_files_model->save();

            $this->load->model('forms_model');
            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $form = $forms[0];

            if ($form->show_all_question_at_once == 1) {
                echo '<script type="text/javascript"> window.location.href=\'' .
                base_url() . "user/answer/view_form/$claim_id/$form_id" .
                '\'</script>';
            } else {
                echo '<script type="text/javascript"> window.location.href=\'' .
                base_url() . "user/answer/preview_form/$claim_id/$form_id/end" .
                '\'</script>';
            }
        }
    }

    public function upload_attachment_category($claim_id, $category = false) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->input->post('form_id');

        $form_field_name = 'attachment';

        $this->load->model('claims_files_model');
        $upload_config = claims_files_model::get_upload_config();
        $files = claims_files_model::get_claims_for_claim_id($claim_id);

        $current_image_size = $_FILES[$form_field_name]['size'];
        $size = 0;
        foreach ($files as $file) {
            $path = FCPATH . 'assets/claim_files/' . $file->file_name;
            $size += filesize($path);
        }
        $size1 = $current_image_size + $size;
        $total_size = number_format($size1 / 1048576, 1);
        if ($total_size > 10) {
            $success = false;
            $this->session->set_flashdata('file_errors', 'You can not upload more than 10MB data.');
            redirect(base_url() . "user/answer/preview_category/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**'));
            exit;
        }
// try uploadng the file
// if file is selected then only the upload will be done
// it would be success if no file was selested
        $logo_upload_status = my_file_upload($form_field_name, $upload_config, $this);
        $success = true;
        if ($logo_upload_status['status'] == 0) {
            $success = false;
            $this->session->set_flashdata('file_errors', $logo_upload_status['data']);
            echo '<script type="text/javascript">alert(\'' . htmlentities($logo_upload_status['data']) . '\'); window.location.href=\'' . base_url() . "user/answer/preview_question/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, 'DOM_CLAIM_DEVREPUBLIC') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**') . '\'</script>';
        }

        if (!isset($logo_upload_status['data'])) {
            $success = false;
            $this->session->set_flashdata('file_errors', "Selecteer eerst het bestand dat u wilt toevoegen.");
            $this->load->model('forms_model');
            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $form = $forms[0];
            echo '<script type="text/javascript">alert(\'Selecteer eerst het bestand dat u wilt toevoegen.\'); window.location.href=\'' . base_url() . "user/answer/preview_category/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, 'DOM_CLAIM_DEVREPUBLIC') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**') . '\'</script>';
            flush();
        }

        if ($success == true) {
            $file_name = $logo_upload_status['data'];
            $claims_files_model = new claims_files_model();
            $claims_files_model->file_name = $file_name;
            $claims_files_model->claim_id = $claim_id;
            $claims_files_model->save();
            $this->load->model('forms_model');
            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $form = $forms[0];
            $this->session->set_flashdata('flash_prev_url', $this->input->post('prev_url'));
            redirect(base_url() . "user/answer/preview_category/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form->form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**'));
        }
    }

    public function upload_attachment_once($claim_id) {
        $claim_id = $this->encrypt->decode($claim_id, 'DOM_CLAIM_DEVREPUBLIC');

        $form_id = $this->input->post('form_id');
        $form_field_name = 'attachment';
        $this->load->model('claims_files_model');
        $upload_config = claims_files_model::get_upload_config();
        $files = claims_files_model::get_claims_for_claim_id($claim_id);
        $current_image_size = $_FILES[$form_field_name]['size'];
        $size = 0;
        foreach ($files as $file) {
            $path = FCPATH . 'assets/claim_files/' . $file->file_name;
            $size += filesize($path);
        }
        $size1 = $current_image_size + $size;
        $total_size = number_format($size1 / 1048576, 1);

        if ($total_size > 10) {
            $success = false;
            $this->session->set_flashdata('file_errors', 'You can not upload more than 10MB data.');
            redirect(base_url() . "user/answer/preview_question/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**'));
            exit;
        }
        $logo_upload_status = my_file_upload($form_field_name, $upload_config, $this);
        $success = true;

        if ($logo_upload_status['status'] == 0) {
            $success = false;
            $this->session->set_flashdata('file_errors', $logo_upload_status['data']);
            echo '<script type="text/javascript">alert(\'' . htmlentities($logo_upload_status['data']) . '\'); window.location.href=\'' . base_url() . "user/answer/preview_question/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**') . '\'</script>';
        }

        if (!isset($logo_upload_status['data'])) {
            $success = false;
            $this->session->set_flashdata('file_errors', "Selecteer eerst het bestand dat u wilt toevoegen.");
            $this->load->model('forms_model');
            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $form = $forms[0];
            echo '<script type="text/javascript">alert(\'Selecteer eerst het bestand dat u wilt toevoegen.\'); window.location.href=\'' . base_url() . "user/answer/preview_question/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**') . '\'</script>';
            flush();
        }

        if ($success == true) {
            $file_name = $logo_upload_status['data'];
            $claims_files_model = new claims_files_model();
            $claims_files_model->file_name = $file_name;
            $claims_files_model->claim_id = $claim_id;
            $claims_files_model->save();
            $this->load->model('forms_model');
            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $form = $forms[0];
            $this->session->set_flashdata('flash_prev_url', $this->input->post('prev_url'));
            redirect(base_url() . "user/answer/preview_question/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**'));
        }
    }

    function delete_attachment($claim_id, $form_id, $attachment_id) {
        $claim_id = $this->encrypt->decode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $form_id = $this->encrypt->decode($form_id, '**DOM_CLAIM_DEVREPUBLIC**');
        $attachment_id = $this->encrypt->decode($attachment_id, '**DOM_CLAIM_DEVREPUBLIC**');

        $this->load->model('claims_files_model');

        $this->load->model('forms_model');
        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
        $form = $forms[0];

        $claims_files_model = new claims_files_model();
        $files = $claims_files_model->get_where(array('file_id' => $attachment_id));
        $file = $files[0];
        $file->delete();
        $this->session->set_flashdata('flash_prev_url', $this->session->flashdata('flash_prev_url'));
        if ($form->show_all_question_at_once == 0) {
            redirect(base_url() . "user/answer/preview_question/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**'));
        } elseif ($form->show_all_question_at_once == 1) {
            redirect(base_url() . "user/answer/preview_question/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**'));
        } else {
            redirect(base_url() . "user/answer/preview_category/" . $this->encrypt->encode($claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**') . "/" . $this->encrypt->encode('end', '**DOM_CLAIM_DEVREPUBLIC**'));
        }
    }

    function _extract_form_tag($content) {
// echo $content;
        $matches = array();
        $regx = '/\&lt;(.*?)\&gt;/';
        preg_match_all($regx, $content, $matches);
        return $matches;
    }

    function makeHeaderText($claim_id) {
        $form_info = $this->claims_model->get_where(
                array('claim_id' => $claim_id
                ));

        $form_id = $form_info[0]->form_id;
        $form_info = $this->forms_model->get_where(
                array('form_id' => $form_id
                ));
        $new_text = $form_info[0]->header_text;

        $field_info = $this->form_field_values_model->get_where(
                array('claim_id' => $claim_id
                ));

        $get_filed = $this->_extract_form_tag($new_text);

        $name = array();

        foreach ($field_info as $filed) {
            foreach ($get_filed[0] as $x) {
                $get_field_name = str_replace('&gt;', '', str_replace('&lt;', '', $x));
                if (strtolower($filed->name) == strtolower($get_field_name)) {
                    $make_input_box = ' <input type="text" name="' .
                            str_replace(' ', '_', $filed->name_at_form) . '"
                value="' . $filed->filed_value . '">';
                    $new_text = str_replace($x, $make_input_box, $new_text);
                }
            }
        }

        return str_replace('&gt;', '', str_replace('&lt;', '', $new_text));
    }

    function makeHeaderText_pdf($claim_id) {
        $form_info = $this->claims_model->get_where(array('claim_id' => $claim_id));

        $form_id = $form_info[0]->form_id;
        $form_info = $this->forms_model->get_where(array('form_id' => $form_id));
        $new_text = $form_info[0]->header_text;

        $field_info = $this->form_field_values_model->get_where(array('claim_id' => $claim_id));
        $get_filed = $this->_extract_form_tag($new_text);

        $name = array();

        foreach ($field_info as $filed) {
            foreach ($get_filed[0] as $x) {
                $get_field_name = str_replace('&gt;', '', str_replace('&lt;', '', $x));
                if (strtolower($filed->name) == strtolower($get_field_name)) {
                    $new_text = str_replace($x, $filed->filed_value, $new_text);
                }
            }
        }
        return str_replace('&gt;', '', str_replace('&lt;', '', $new_text));
    }

    public function edit_detail($claim_id, $form_id) {
        $field_info = $this->form_field_values_model->get_where(array('claim_id' => $claim_id));
        foreach ($field_info as $info) {
            $this->form_field_values_model->update_Claim_Values($info->name_at_form, $this->input->post(str_replace(' ', '_', $info->name_at_form)), $claim_id);
        }

        $this->session->set_flashdata('success', 'Updated The Data Sucessfully');
        redirect(base_url() . "user/answer/preview_question_start/$claim_id/$form_id");
    }

}