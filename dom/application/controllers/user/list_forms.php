<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This controller is responsible for
 * listing the forms for the policy holder (policy holder = user )
 */
class list_forms extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('forms_model');
        $this->load->model('claims_model');
        $this->load->helper('time_worked');
        $this->default_template_name = get_policy_holder_template_logged_in();
    }

    /**
     * this will render the list page
     */
    public function index() {

        $data['page_name'] = 'user/list_insurance';
        $data['title'] = 'Formulieren';

        $this->load->helper('admin_setting_info');
        $admin_info = admin_info();

        if ($admin_info != false) {

            $content = $admin_info->welcome_text;
            $data['content'] = $content;
            //get news
            $this->load->helper('admin_setting_info');
            $admin_info = admin_info();

            $this->load->model('news_model');
            $news_model = new news_model();
            $current_date_for_database = date('Y-m-d', time());

            // get the news whos publication date and expiration date
            // are valid and which belongs to the given client
            $news = $news_model->get_where(
                    array('publication_date <= ' => $current_date_for_database,
                        'expire_date <= ' => $current_date_for_database,
                        'client_id' => $admin_info->client_id
                    ));
            $data['news'] = $news;
            //
            // get the news items here
            $client_id = $admin_info->client_id;
            $all_form_info = $this->forms_model->get_where(array('client_id' => $client_id, 'select_for_policyholder' => '1'));
            if (!empty($all_form_info)) {
                $data['form_info'] = $all_form_info;
            } else {
                $data['form_info'] = '';
            }

            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', 'your link isn\'t valid anymore. Contact your insurance company.');

            redirect(base_url() . 'polishouders', 'refresh');
        }
    }

    /**
     * renders json for the list page
     */
    public function get_json() {
        // get the user
        $user = $this->session->userdata('policy_holder');
        $policy_holder_id = $user->policy_holder_id;

        $this->load->model('claims_model');
        $claim = new claims_model();
        $where = "`policy_holder_id` = " . $policy_holder_id . " and `status` != 'Archief'";
        //$claims = $claim->get_where(array('policy_holder_id' => $policy_holder_id)); 
        $claims = $claim->get_where($where);
        $arr = $this->_get_array_for_json($claims);
        $data['aaData'] = $arr;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    /**
     * returns array that can be used for json
     *
     * @param array[claims_model] $objects            
     * @return array[]
     */
    function _get_array_for_json($objects) {
        $arra = array();

        foreach ($objects as $value) {
            $temp_arr = array();

            $temp_arr[] = $value->import_date;

            $temp_arr[] = $value->get_form_number();
            $form_info = $this->forms_model->get_where(
                    array('form_id' => $value->form_id
                    ));
            if (!empty($form_info)) {
                $temp_arr[] = $form_info[0]->form_name;
            } else {
                $temp_arr[] = null;
            }

            $policy_holders = new policy_holder_model();
            $policy_holders = $policy_holders->get_where(
                    array('policy_holder_id' => $value->policy_holder_id
                    ));
            if (count($policy_holders) > 0) {
                $policy_holder = $policy_holders[0];
            }

            if (isset($policy_holder) && $policy_holder->policy_number != NULL) {
                $temp_arr[] = @$policy_holder->policy_number;
            } else {
                $temp_arr[] = null;
            }

            $temp_arr[] = ucwords($value->handler);


            if ($value->status == "Nieuw") {
                $temp_arr[] = '<img style="height: 35px;" src="' . assets_url_img .
                        'nieuw.png' . '" alt="Nieuw" title="Nieuw"/>';
            } elseif ($value->status == "open") {
                $temp_arr[] = '<img style="height: 35px;" src="' . assets_url_img .
                        'open.png' . '" alt="open" title="open"/>';
            } elseif ($value->status == "Afgerond") {
                $temp_arr[] = '<img style="height: 35px;" src="' . assets_url_img .
                        'afgerond.png' . '" alt="Afgerond" title="Afgerond"/>';
            } elseif ($value->status == "Archief") {
                $temp_arr[] = '<img style="height: 35px;" src="' . assets_url_img .
                        'archief.png' . '" alt="Archief" title="Archief"/>';
            } else {
                $temp_arr[] = '';
            }
            // $temp_arr[] = '<a href="' . base_url() . 'user/answer/view_form_fields/' . $value->claim_id . '/' . $value->form_id . '">Details</a>';
            if ($value->status == "Afgerond" || $value->status == "Archief") {
                $temp_arr[] = anchor(
                        base_url() . 'assets/claims_pdf_files/' .
                        $value->get_form_number() . '_Digitaal_op_Maat.pdf', '<img style="height: 35px;" src="' .
                        assets_url_img . 'PDF-Document-icon.png' .
                        '" alt="alt"/>', 'target="_blank"');
            } else {
                $temp_arr[] = '<a href="' . base_url() . 'user/answer/view_form/' . $this->encrypt->encode($value->claim_id, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($value->form_id, '**DOM_CLAIM_DEVREPUBLIC**') . '"><img style="height: 35px;" src="' . assets_url_img . 'edit_claim.png' . '" alt="Bewerken" title="Bewerken"/></a>';
            }

            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function add_claim() {
        $this->form_validation->set_rules('form', 'Form', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('form');
            $this->index();
        } else {
            $user = $this->session->userdata('policy_holder');
            $date = get_current_time()->get_date_for_db();
            $obj = new claims_model();
            $form_id = $this->input->post('form');
            $obj->import_date = $date;
            $obj->kind = '';
            $obj->handler = '';
            $obj->schadenummer = $obj->get_form_number();
            $obj->userid = '';
            $obj->form_id = $form_id;
            $obj->mail_send_date = $date;
            $obj->status = 'open';
            $obj->policy_holder_id = $user->policy_holder_id;
            $obj->client_id = $user->client_id;
            $obj->claim_sequence_number = $obj->generateNewClaimSequenceNumber($user->client_id);
            $obj->conform_date = '';
            $obj->is_delete = '0';
            $res = $obj->save();

            if (isset($res) && $res != '') {
                $this->session->set_flashdata('success', 'Form added sucessfully.');
                redirect(base_url() . 'user/answer/view_form/' . $this->encrypt->encode($res, '**DOM_CLAIM_DEVREPUBLIC**') . '/' . $this->encrypt->encode($form_id, '**DOM_CLAIM_DEVREPUBLIC**'), 'refresh');
            } else {
                $this->session->set_flashdata('error', ' Error ! Please try again.');
                redirect(base_url() . 'user/list_forms', 'refresh');
            }
            ///working progress...
        }
    }

}

// file at : application/controllers/user/list_forms.php
?>
