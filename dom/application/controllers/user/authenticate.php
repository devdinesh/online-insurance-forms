<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class authenticate extends CI_Controller {

    var $default_template_name;
    var $default_template_name1;

    /**
     * this is the default constructor
     */
    function __construct() {
        parent::__construct();
        $this->load->model('policy_holder_model');
        $this->load->model('client_setting_model');
        $this->load->model('client_email_template_model');
        $this->default_template_name = get_policy_holder_template_logged_in();
    }

    /**
     * this will render the super admin login form
     */
    public function index($msg = '') {
        
        $session = $this->session->userdata('user');
        if ($session == false) {
            $data['title'] = "Policy Holder Login";
            $this->load->view('user/login_template', $data);
        } else {
            redirect('user/list_forms', 'refresh');
        }
    }

    function policy_index() {
        $data['page_name'] = 'admin/policy_holder/login_proc';
        $data['title'] = "Policy Holder";
        $this->default_template_name1 = get_admin_template_for_not_logged_in();
        $this->load->view($this->default_template_name1, $data);
    }

    function check_policy_holder_Login() {
        $this->form_validation->set_rules('p_email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('p_password', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('p_email');
            $this->form_validation->set_value('p_password');
            $this->index();
        } else {
            $email = $this->input->post('p_email');
            $passward = md5($this->input->post('p_password'));

            $res = $this->policy_holder_model->get_where(array('email' => $email, 'password' => $passward));
            if (count($res) == 1) {
                $user_array = $res[0];
                unset($user_array->validation_rules);
                unset($user_array->table_name);
                unset($user_array->password_reset_random_string);
                unset($user_array->random_str);
                unset($user_array->password);
                $session = array('policy_holder' => $user_array, 'user_type' => 'policy_holder', 'logged_in' => true);
                $this->session->set_userdata($session);
                redirect('user/list_forms', 'refresh');
            } else {
                $this->session->set_flashdata('error', 'Please Enter Valid Email Address Or Passward');

                redirect(base_url() . 'polishouders', 'refresh');
            }
        }
    }

    function logout() {
        $this->session->sess_destroy();
        session_start();
        session_destroy();
        redirect(base_url() . "user/authenticate");
    }

    function set_session_for_user($user_object) {
        // set the session and redirect the user
        // $user_object = $user_object->to_associative_array();
        $session = array('user' => $user_object, 'user_type' => 'user',
            'logged_in' => true
        );
        $this->session->set_userdata($session);
    }

    /**
     * this method when invoked sets the session for the policy holder and
     * allows to do the tasks without loggin in-to the system
     * @param string $random_str is the random string associated with the policy holder
     */
    function login_with_link($pass_random_str) {
        $policy_holder = new policy_holder_model();
        $policy_holders = $policy_holder->get_where(
                array('password_reset_random_string' => $pass_random_str
                ));

        // if the random id was invalid then show the error
        // do not go ahead
        if (count($policy_holders) == 0) {
            echo "Invalid link has been entered. Please check the link.";
            exit();
        }
        // if policy holder was there
        $this->session->unset_userdata('admin');
        $this->session->unset_userdata('super_admin');

        $policy_holder = $policy_holders[0];
        $session = array('policy_holder' => $policy_holder,
            'user_type' => 'policy_holder', 'logged_in' => true
        );
        $this->session->set_userdata($session);
        redirect(base_url() . "user");
    }

    function login_without_link($random_string) {

        /*
         * $policy_holder = new policy_holder_model(); $policy_holders =
         * $policy_holder->get_where( array('password_reset_random_string' =>
         * $random_string )); // if the random id was invalid then show the
         * error // do not go ahead if (count($policy_holders) == 0) { echo
         * "Invalid link has been entered. Please check the link."; exit(); } //
         * if policy holder was there $policy_holder = $policy_holders[0];
         * $this->set_session_for_user($policy_holder);
         */

        $res = $this->policy_holder_model->get_where(
                array('password_reset_random_string' => $random_string
                ));

        if (count($res) == 1) {

            $this->session->unset_userdata('admin');
            $this->session->unset_userdata('super_admin');

            $session = array('policy_holder' => $res[0],
                'user_type' => 'policy_holder', 'logged_in' => true
            );
            $this->session->set_userdata($session);

            redirect("user/policy_holder_info/index/" . $random_string, 'refresh');
        } else {
            $this->session->set_flashdata('error', 'Please Enter Valid Email Address Or Passward');

            redirect('user/authenticate/policy_index', 'refresh');
        }
    }

    function forgot_pasword() {
        $session = $this->session->userdata('user');
        if ($session == false) {
            $data['title'] = "Polishouders Wachtwoord Vergeten";
            $this->load->view('user/forgot_password_template', $data);
        } else {
            redirect('user/list_forms', 'refresh');
        }
    }

    function forgotPasswordListener() {
        $this->form_validation->set_rules('p_email', 'Email', 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('p_email');
            $this->forgot_pasword();
        } else {
            $randid = random_string('alnum', 30);
            $email = $this->input->post('p_email');
            $check = $this->policy_holder_model->checkMail($email, $randid);

            if ($check === 1) {
                $this->session->set_flashdata('error', 'Account Not Active');
                redirect(base_url() . 'polishouders/wachtwoord_vergeten', 'refresh');
            } else if ($check === 2) {
                $this->session->set_flashdata('error', 'Mail Address is Not Registered with Us');
                redirect(base_url() . 'polishouders/wachtwoord_vergeten', 'refresh');
            } else if ($check === 3) {
                $this->session->set_flashdata('error', 'Mail Address is Not Registered with Us');
                redirect(base_url() . 'polishouders/wachtwoord_vergeten', 'refresh');
            } else {
                $obj_email = new client_email_template_model();
                $email_template = $obj_email->get_where(array('client_id' => $check->client_id, 'email_type' => 'forgot_password'));
                if (count($email_template) == 0) {
                    $this->session->set_flashdata('error', 'The mail template with name "forgot_password" was not found.');
                    redirect(base_url() . 'polishouders/wachtwoord_vergeten', 'refresh');
                } else {
                    $admin_setting_info = $this->client_setting_model->get_where(array('client_id' => $check->client_id));
                    $smtp_user = (string) $admin_setting_info[0]->email;
                    $smtp_pass = (string) $admin_setting_info[0]->password;
                    $smtp_server = (string) $admin_setting_info[0]->smtp_server;
                    $smtp_port = $admin_setting_info[0]->smtp_port;

                    $message = $email_template[0]->email_message; // get the message
                    $link = '<a href="' . base_url() . 'polishouders/wachtwoord_reset/' . $randid . '">Link</a> ';
                    $message = str_replace('&lt;link&gt;', $link, $message);

                    $this->load->helper('daynamic_sending_mail');
                    $sent_msg = dynamic_send_mail($email, $email_template[0]->email_subject, $message, '', $email_template[0]->email_attachment, $smtp_user, $smtp_pass, $smtp_server, $smtp_port);
                    if ($sent_msg == TRUE) {
                        $this->session->set_flashdata('success', 'Email is verzonden!');
                        redirect(base_url() . 'polishouders/wachtwoord_vergeten', 'refresh');
                    } else {
                        $this->session->set_flashdata('error', 'Error in Sending the mail. Please try again later.');
                        redirect(base_url() . 'polishouders/wachtwoord_vergeten', 'refresh');
                    }
                }
            }
        }
    }

    function reset_pasword($rand_str) {
        $session = $this->session->userdata('user');
        if ($session == false) {
            $data['title'] = "Polishouders Reset Wachtwoord";
            $data['rand_str'] = $rand_str;
            $this->load->view('user/reset_password_template', $data);
        } else {
            redirect('user/list_forms', 'refresh');
        }
    }

    function resetPasswordListener() {
        $this->form_validation->set_rules('p_password', 'Wachtwoord', 'trim|required|');
        $this->form_validation->set_rules('c_password', 'Bevestig Wachtwoord', 'trim|required|matches[p_password]');

        $randid = $this->input->post('rand_str');
        //check validation
        if ($this->form_validation->run() == FALSE) {
            $this->reset_pasword($randid);
        } else {
            $obj = new policy_holder_model();
            $obj->password = md5($this->input->post('p_password'));
            $check = $obj->updatePassword($randid);
            if ($check === true) {
                $this->session->set_flashdata('success', 'Uw wachtwoord is gewijzigd. Login met uw nieuwe wachtwoord.');
                redirect(base_url() . 'polishouders/wachtwoord_reset/' . $randid, 'refresh');
            } else {
                $this->session->set_flashdata('error', 'Fout in rustende wachtwoord');
                redirect(base_url() . 'polishouders/wachtwoord_reset/' . $randid, 'refresh');
            }
        }
    }

}

/* End of file authenticate.php */
/* Location: ./application/controllers/authenticate.php */
