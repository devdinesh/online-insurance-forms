<?php

/*
 *
 * Author : DINESH
 * 
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class auto_import extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('path');
        $this->load->helper('sending_mail');
        $this->load->model('admin_model');
        $this->load->model('forms_model');
        $this->load->model('claims_model');
        $this->load->model('client_setting_model');
        $this->load->model('policy_holder_model');
        $this->load->model('users_model');
        $this->load->model('mail_template_model');
        $this->load->model('form_field_model');
        $this->load->model('form_field_values_model');
        $this->load->model('ins_import_log_model');
        $this->load->model('client_email_template_model');


        $this->load->model('newletter_model');
        $this->load->model('newsletter_template_model');
        $this->load->model('newsletter_article_model');
        $this->load->model('article_model');
        $this->load->model('send_newletter_model');
    }

    /**
     * The function use to make automatically import template which is imported by the client .
     */
    public function index() {
        $clients = $this->clients_model->get_auto_import_client();
        if (!empty($clients)) {
            $success = 0;
            $failure = 0;
            $failur_template = array();
            foreach ($clients as $client) {
                if ($client->path_to_import) {


                    $files = $this->_get_file_list($client->path_to_import);
                    if (!empty($files)) {
                        // go thru all the files
//                        var_dump($files);
//                        exit;
                        foreach ($files as $file) {
                            $import_data = array();
                            if (substr($file, 0, 8) && substr($file, 0, 8) != 'IMPORTED') {
                                $full_path = './assets/admin_assets/import_files/' .
                                        $client->path_to_import . "/" . $file;
                                $path = set_realpath($full_path);
                                $path = substr($path, 0, strlen($path) - 1);
                                $ext = pathinfo($file, PATHINFO_EXTENSION);

                                if ($ext == 'docx') {
                                    $content = $this->read_file_docx($path);
                                } else {
                                    $content = $this->parseWord($path);
                                }

                                $import_result = $this->import_listener($content, $ext, $client->client_id);
                                if ($import_result) {
                                    $original = FCPATH . '/assets/admin_assets/import_files/' .
                                            $client->path_to_import . '/' . $file;
                                    // $original = str_replace("/", "\\",
                                    // set_realpath($original) . $file);
                                    // @chmod($original, 0777);
                                    $delete_status = unlink($original);
                                    $import_data['claim_id'] = $import_result;
                                    $send_link = $this->send_link_for_without_login_fillup($import_result);
//                                    exit;
                                    $success++;
                                } else {
                                    $failur_template[$failure] = $file;
                                    $import_data['claim_id'] = NULL;
                                    $delete_status = unlink($full_path);
                                    $failure++;
                                }

                                //ADD THE IMPORRT LOG FILE DETAILS . 
                                $import_data['client_id'] = $client->client_id;
                                $import_data['file_name'] = $file;
                                $import_data['file_location'] = $full_path;
                                $import_data['date'] = date('Y-m-d');
                                $import_data['time'] = date('H:i:s');
                                $import_log_status = $this->ins_import_log_model->insert($import_data);
                            }
                        }
                    }
                    //Update the last imported date and time  'in the ins_client_setting "last_impor" field.'

                    $last_import_status = $this->clients_model->update_last_imported_time($client->client_id);
                }
            }
            echo "<br/>";
            echo "Sucessfully Imported Templates : " . $success;
            echo "<br/>";
            echo "<br/>";
            echo "Een of meerdere documenten zijn niet correct en worden niet geimporteerd : ";
            echo "<br/>";
            echo "<br/>";
            echo "Rejected Templates : " . $failure;
            echo "<br/>";

            if ($failur_template) {
                for ($k = 0; $k < count($failur_template); $k++) {
                    echo "<br/>";
                    echo $failur_template[$k];
                }
            }
        } else {
            echo "there is no client to allow to automatic import templates";
        }
        exit;
    }

    /**
     * This method is called when we call the import method to Import the templates.
     */
    function import_listener($content, $ext, $client_id) {
        $email = null;
        $policy_number = null;
        $behandler = null;
        $schadenummer = null;
        $client_detail = $this->clients_model->get_where(array('client_id' => $client_id));
        if ($ext == 'doc') {
            $content = preg_replace('/[\x00-\x1F\x80-\xFF]/', '<br />', $content);
        }

        if ($ext == 'doc') {
            $val = explode('<br />', $content);
            $form_tag = $val[0];
            if ($this->forms_model->form_with_tag_exists($form_tag, $client_id)) {

                $obj_forms_fiels = new form_field_model();
                $email_filed = $obj_forms_fiels->getMailFiledFromTag($form_tag, $client_id);

                $email_in_template = $this->searchArray($email_filed->field_name . ': ', $val);
                $email_in_template = $this->searchArray($email_filed->field_name . ': ', $val);

                $policy_number = $obj_forms_fiels->getPolicyNumberFiledFromTag($form_tag, $client_id);
                $policy_number_in_template = $this->searchArray($policy_number->field_name . ': ', $val);

                $behandler = $obj_forms_fiels->getBehandlerFiledFromTag($form_tag, $client_id);
                $behandler_in_template = $this->searchArray($behandler->field_name . ': ', $val);
                $schadenummer = $obj_forms_fiels->getschadenummerFiledFromTag($form_tag, $client_id);

                if (isset($schadenummer) && $schadenummer != false) {
                    $schadenummer_in_template = $this->searchArray($schadenummer->field_name . ': ', $val);
                } else {
                    $schadenummer_in_template = '';
                }

                //behindler Emial start
                $behandler_email = $obj_forms_fiels->getBehandlerEmailFromTag($form_tag, $client_id);
                $behandler_email_in_template = $this->_extract_field($content, $behandler_email->field_name . ': ', '');

                if (preg_match("/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/", $behandler_email_in_template)) {
                    $behandler_email = $behandler_email_in_template;
                    //  echo $email;
                } else {
                    $temp1 = explode(" ", $behandler_email_in_template);
                    //var_dump($temp);
                    $behandler_email = $temp1[0];
                }

                //behindler Emial end
                //var_dump($policy_number);
                // first extract the email address ffrom the template and check it
                // is exist in the table or not.

                if (preg_match("/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/", $email_in_template)) {
                    $email = preg_replace('/[^(\x20-\x7F)]*/', "", $email_in_template);
                } else {
                    $email = preg_replace('/\s+/', '', $email_in_template);
//                $temp = explode(" ", $email_in_template);
//                $email = preg_replace('/[^(\x20-\x7F)]*/', "", $temp[3]);
                }
            }
        } else {
            $form_tag = trim($this->_extract_form_tag($content));
            if ($this->forms_model->form_with_tag_exists($form_tag, $client_id)) {
                $obj_forms_fiels = new form_field_model();
                $email_filed = $obj_forms_fiels->getMailFiledFromTag($form_tag, $client_id);
                $email_in_template = $this->_extract_field($content, $email_filed->field_name . ': ', '');

                // var_dump($email_in_template);
                if (preg_match("/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/", $email_in_template)) {
                    $email = $email_in_template;
                    //  echo $email;
                } else {
                    $temp = explode(" ", $email_in_template);
                    //var_dump($temp);
                    $email = @$temp[2];
                }

                $policy_number = $obj_forms_fiels->getPolicyNumberFiledFromTag($form_tag, $client_id);

                $policy_number_in_template = $this->_extract_field($content, $policy_number->field_name . ': ', '');

                $behandler = $obj_forms_fiels->getBehandlerFiledFromTag($form_tag, $client_id);
                $behandler_in_template = $this->_extract_field($content, $behandler->field_name . ': ', '');
                //behindler Emial start
                $behandler_email = $obj_forms_fiels->getBehandlerEmailFromTag($form_tag, $client_id);
                $behandler_email_in_template = $this->_extract_field($content, $behandler_email->field_name . ': ', '');

                if (preg_match("/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/", $behandler_email_in_template)) {
                    $behandler_email = $behandler_email_in_template;
                    //  echo $email;
                } else {
                    $temp1 = explode(" ", $behandler_email_in_template);
                    //var_dump($temp);
                    $behandler_email = @$temp1[2];
                }
                //behindler Emial end

                $schadenummer = $obj_forms_fiels->getschadenummerFiledFromTag($form_tag, $client_id);

                if (isset($schadenummer) && $schadenummer != false) {
                    $schadenummer_in_template = $this->_extract_field($content, $schadenummer->field_name . ': ', '');
                } else {
                    $schadenummer_in_template = '';
                }
            }
        }
        $admin_setting_info = $this->client_setting_model->get_where(array('client_id' => $client_id));

//          $smtp_user = (string) $admin_setting_info[0]->email;
//          $smtp_pass = (string) $admin_setting_info[0]->password;
//          $smtp_server = (string) $admin_setting_info[0]->smtp_server;
//          $smtp_port = $admin_setting_info[0]->smtp_port; 


        if ($this->forms_model->form_with_tag_exists($form_tag, $client_id) && isset($admin_setting_info[0]->main_user) && $admin_setting_info[0]->main_user != NULL) {

            $obj_forms = new forms_model();
            $get_form_details = $obj_forms->get_where(array('form_tag' => $form_tag));

            $obj_fileds = new form_field_model();
            $get_fields = $obj_fileds->get_where(array('form_id' => $get_form_details[0]->form_id));

            $poliy_holders = new policy_holder_model();
            $get_poliy_holders = $poliy_holders->get_where(array('email' => $email, 'client_id' => $client_id));
            if ((is_array($get_poliy_holders) && count($get_poliy_holders) == 1)) {

                if ($policy_number_in_template != '') {
                    $poliy_holders->policy_number = $policy_number_in_template;
                    $poliy_holders->policy_holder_id = $get_poliy_holders[0]->policy_holder_id;
                    $poliy_holders->updatePolicyNumber();
                }

                // $users = $this->users_model->get_where(array('client_id' => $client_id));

                $this->claims_model->import_date = date('d-m-Y', time()); // strtotime(date("d-m-Y"));
                $this->claims_model->kind = NULL;
                $this->claims_model->userid = $admin_setting_info[0]->main_user;

                if ($behandler_in_template != '')
                    $this->claims_model->handler = $behandler_in_template;

                if ($schadenummer_in_template != '')
                    $this->claims_model->schadenummer = $schadenummer_in_template;
                if (isset($behandler_email) && $behandler_email != '')
                    $this->claims_model->handler_email = $behandler_email;
                $this->claims_model->form_id = $get_form_details[0]->form_id;
                $this->claims_model->mail_send_date = NULL;
                $this->claims_model->status = 'Nieuw';
                $this->claims_model->policy_holder_id = $get_poliy_holders[0]->policy_holder_id;
                $this->claims_model->client_id = $client_id;
                $this->claims_model->is_delete = 0;
                $this->claims_model->claim_sequence_number = $this->claims_model->generateNewClaimSequenceNumber($client_id);
                $claim_id = $this->claims_model->save();

                foreach ($get_fields as $filed) {
                    $obj_filed_value = new form_field_values_model();
                    $obj_filed_value->name = $filed->field_name;
                    $obj_filed_value->name_at_form = $filed->name_on_form;
                    if ($ext == 'doc') {
                        $val = explode('<br />', $content);
                        $obj_filed_value->filed_value = $this->searchArray($filed->field_name . ': ', $val);
                    } else {
                        //$obj_filed_value->filed_value = $this->_extract_field($content, $filed->field_name . ': ', '');
                        $extract_contain = $this->_extract_field($content, $filed->field_name . ': ', '');
                        if (strpos($extract_contain, 'TIME \@ "d MMMM yyyy"') !== false) {
                            $obj_filed_value->filed_value = str_replace('TIME \@ "d MMMM yyyy"', '', $this->_extract_field($content, $filed->field_name . ': ', ''));
                        } else {
                            $obj_filed_value->filed_value = $extract_contain;
                        }
                    }
                    $obj_filed_value->claim_id = $claim_id;
                    $check = $obj_filed_value->save();
                }
                if (isset($client_detail[0]->enable_newsletters) && $client_detail[0]->enable_newsletters == 1) {
                    $NB_form_tag = substr($form_tag, 0, 2);
                    if ($NB_form_tag == 'NB') {
                        $this->sendNewsletter($form_tag, $get_poliy_holders[0]->policy_holder_id, $client_id);
                    }
                }
//                if ($check != '') {
//                    $this->session->set_flashdata('success', " Items were imported");
//                } else {
//                    $this->session->set_flashdata('error', "Error in Importing Items");
//                }
                return $claim_id;
            } else {

                if ($email != '' && $email != NULL) {
                    $randid = $this->generateRandStr(10);

                    $this->policy_holder_model->email = $email;
                    $this->policy_holder_model->client_id = $client_id;
                    $this->policy_holder_model->status = "A";
                    $this->policy_holder_model->password_reset_random_string = $randid;

                    if ($policy_number_in_template != '')
                        $this->policy_holder_model->policy_number = $policy_number_in_template;



                    $policy_holder_id = $this->policy_holder_model->save();

                    // $users = $this->users_model->get_where(array('client_id' => $client_id));
                    $this->claims_model->import_date = date('d-m-Y', time()); // strtotime(date("d-m-Y"));
                    $this->claims_model->kind = NULL;

                    if ($behandler_in_template != '')
                        $this->claims_model->handler = $behandler_in_template;

                    if ($schadenummer_in_template != '')
                        $this->claims_model->schadenummer = $schadenummer_in_template;

                    if (isset($behandler_email) && $behandler_email != '')
                        $this->claims_model->handler_email = $behandler_email;

                    $this->claims_model->userid = $admin_setting_info[0]->main_user;
                    $this->claims_model->form_id = $get_form_details[0]->form_id;
                    $this->claims_model->mail_send_date = NULL;
                    $this->claims_model->status = 'Nieuw';
                    $this->claims_model->policy_holder_id = $policy_holder_id;
                    $this->claims_model->client_id = $client_id;
                    $this->claims_model->is_delete = 0;
                    $this->claims_model->claim_sequence_number = $this->claims_model->generateNewClaimSequenceNumber($client_id);
                    $claim_id = $this->claims_model->save();
                    foreach ($get_fields as $filed) {
                        $obj_filed_value = new form_field_values_model();
                        $obj_filed_value->name = $filed->field_name;
                        $obj_filed_value->name_at_form = $filed->name_on_form;
                        if ($ext == 'doc') {
                            $val = explode('<br />', $content);
                            $obj_filed_value->filed_value = $this->searchArray($filed->field_name . ': ', $val);
                        } else {
                            $obj_filed_value->filed_value = $this->_extract_field($content, $filed->field_name . ': ', '');
                        }
                        $obj_filed_value->claim_id = $claim_id;
                        $check = $obj_filed_value->save();
                    }
                    if (isset($client_detail[0]->enable_newsletters) && $client_detail[0]->enable_newsletters == 1) {
                        $NB_form_tag = substr($form_tag, 0, 2);
                        if ($NB_form_tag == 'NB') {
                            $this->sendNewsletter($form_tag, $policy_holder_id, $client_id);
                        }
                    }
                    /*  $policy_holder_email = $email;

                      $message = "Your Policy Agent has send you a link for entering your personal details. Please click the below link to enter the details.";
                      $message .= '<br /><br /><br /><a href="' . base_url() .
                      'user/authenticate/login_without_link/' . $randid .
                      '">Link</a> ';

                      $this->load->helper('daynamic_sending_mail');

                      $sent_msg = dynamic_send_mail($policy_holder_email, 'policy holder', $message, '', '', $smtp_user, $smtp_pass, $smtp_server, $smtp_port); */

//                    if ($check != '') {
//                        $this->session->set_flashdata('success', " Items were imported");
//                    } else {
//                        $this->session->set_flashdata('error', "Error in Importing Items");
//                    }
                    return $claim_id;
                } else {

//                    //$users = $this->users_model->get_where(array('client_id' => $client_id));
//                    $this->claims_model->import_date = date('d-m-Y', time()); // strtotime(date("d-m-Y"));
//                    $this->claims_model->kind = NULL;
//                    $this->claims_model->userid = $admin_setting_info[0]->main_user;
//                    ;
//
//                    if ($behandler_in_template != '')
//                        $this->claims_model->handler = $behandler_in_template;
//
//                    if ($schadenummer_in_template != '')
//                        $this->claims_model->schadenummer = $schadenummer_in_template;
//
//                    if (isset($behandler_email) && $behandler_email != '')
//                        $this->claims_model->handler_email = $behandler_email;
//
//                    $this->claims_model->form_id = $get_form_details[0]->form_id;
//                    $this->claims_model->mail_send_date = NULL;
//                    $this->claims_model->status = 'Nieuw';
//                    $this->claims_model->policy_holder_id = NULL;
//                    $this->claims_model->client_id = $client_id;
//                    $this->claims_model->is_delete = 0;
//                    $this->claims_model->claim_sequence_number = $this->claims_model->generateNewClaimSequenceNumber($client_id);
//                    $claim_id = $this->claims_model->save();
//
//                    foreach ($get_fields as $filed) {
//                        $obj_filed_value = new form_field_values_model();
//                        $obj_filed_value->name = $filed->field_name;
//                        $obj_filed_value->name_at_form = $filed->name_on_form;
//                        if ($ext == 'doc') {
//                            $val = explode('<br />', $content);
//                            $obj_filed_value->filed_value = $this->searchArray($filed->field_name . ': ', $val);
//                        } else {
//                            $obj_filed_value->filed_value = $this->_extract_field($content, $filed->field_name . ': ', '');
//                        }
//                        $obj_filed_value->claim_id = $claim_id;
//                        $check = $obj_filed_value->save();
////                        if ($check != '') {
////                            $this->session->set_flashdata('success', " Items were imported");
////                        } else {
////                            $this->session->set_flashdata('error', "Error in Importing Items");
////                        }
//                    }
                    return FALSE;
                }
            }
        } else {
            return FALSE;
        }
    }

    /**
     * This method extracts the form tag from the word file's content.
     *
     * @param type $content
     *            the word file content from which we want to extract
     *            the form tag
     * @return string value of the form tag if the tag was found
     */
    function _extract_form_tag($content) {
        $matches = array();
        $regx = "/.*#.+/";
        preg_match($regx, $content, $matches);
        if (count($matches) > 0) {
            return $matches[0];
        }
        return;
    }

    /**
     *
     * @param type $content
     *            is the content from which we want to find out the details
     * @param type $field_name
     *            is the name of field for which we want to extract the value
     * @param type $next_field_name
     *            is the name of the next field, not used at the moment
     * @param type $string_length
     *            is the position which is used in subsstring. This parameter is
     *            optional
     * @return type nothing is returned if the field was not found.
     *         The value of the field is returned if the value was found
     */
    function _extract_field($content, $field_name, $next_field_name, $string_length = 0) {
        $matches = array();
        $regx = "/" . $field_name . ".+" . "/";
        preg_match($regx, $content, $matches);

        if (count($matches) > 0) {
            // /echo "ans: " . substr($matches[0], strlen($field_name)) .
            // "<br>";
            if ($string_length != 0) {

                $return_val = substr($matches[0], $string_length);
                $return_val = trim($return_val);
                if ($return_val[0] == ':') {
                    $return_val = substr($return_val, 1);
                }

                return $return_val;
            } else {
                $return_val = substr($matches[0], strlen($field_name));
                $return_val = trim($return_val);
                if (@$return_val[0] == ':') {
                    $return_val = substr($return_val, 1);
                }

                return $return_val;
            }
        }
        return;
    }

    function searchArray($search, $array) {
        foreach ($array as $key => $value) {
            if (stristr($value, $search)) {
                return substr($value, strlen($search));
            }
        }
        return false;
    }

    /**
     * The function use to get the list of templates which is imported from the client in seperate directory .
     */
    function _get_file_list($location) {
        $this->load->helper('file');
        $location = 'assets/admin_assets/import_files/' . $location;
        $files = get_filenames($location);
        return $files;
    }

    /**
     * read word (.docx) file content
     *
     * @param type $filename
     *            is the name of word file from which we want
     *            to get content
     * @return boolean boolean false is returned if file read failed because of
     *         some reason, files content is returned if the read was successful
     */
    function read_file_docx($filename) {
        $striped_content = '';
        $content = '';

        if (!$filename || !file_exists($filename))
            return false;

        $zip = zip_open($filename);

        if (!$zip || is_numeric($zip))
            return false;

        while (( $zip_entry = zip_read($zip) ) != false) {

            if (zip_entry_open($zip, $zip_entry) == FALSE)
                continue;

            if (zip_entry_name($zip_entry) != "word/document.xml")
                continue;

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        } // end while

        zip_close($zip);

        // echo "aa: ", $content;
        // echo "<hr>";
        // file_put_contents('1.xml', $content);
        $content = str_replace("<w:br/>", "\r\n", $content);
        $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        $content = str_replace('</w:r></w:p>', "\r\n", $content);
        $striped_content = strip_tags($content);

        return $striped_content;
    }

    // read the .doc file templates
    function parseWord($filename) {
        if (file_exists($filename)) {

            if (( $fh = fopen($filename, 'r') ) !== false) {

                $headers = fread($fh, 0xA00);

                // 1 = (ord(n)*1) ; Document has from 0 to 255 characters
                $n1 = ( ord($headers[0x21C]) - 1 );

                // 1 = ((ord(n)-8)*256) ; Document has from 256 to 63743
                // characters
                $n2 = ( ( ord($headers[0x21D]) - 8 ) * 256 );

                // 1 = ((ord(n)*256)*256) ; Document has from 63744 to 16775423
                // characters
                $n3 = ( ( ord($headers[0x21E]) * 256 ) * 256 );

                // (((ord(n)*256)*256)*256) ; Document has from 16775424 to
                // 4294965504 characters
                $n4 = ( ( ( ord($headers[0x21F]) * 256 ) * 256 ) * 256 );

                // Total length of text in the document
                $textLength = ( $n1 + $n2 + $n3 + $n4 );

                $extracted_plaintext = fread($fh, $textLength);

                // if you want the plain text with no formatting, do this
                // $content = $extracted_plaintext;

                /*  $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", 
                  $content);
                  $content = str_replace('</w:r></w:p>', "\r\n", $content);
                  $striped_content = strip_tags($content); */
                return nl2br($extracted_plaintext);
                // if you want to see your paragraphs in a web page, do this
                // echo nl2br($extracted_plaintext);
            }
        }
    }

    function generateRandStr($length) {
        $randstr = "";
        for ($i = 0; $i < $length; $i++) {
            $randnum = mt_rand(0, 61);
            if ($randnum < 10) {
                $randstr .= chr($randnum + 48);
            } else if ($randnum < 36) {
                $randstr .= chr($randnum + 55);
            } else {
                $randstr .= chr($randnum + 61);
            }
        }
        return $randstr;
    }

    function send_link_for_without_login_fillup($claim_id) {
        // get the claim for claim id
        $claims = $this->claims_model->get_where(array('claim_id' => $claim_id));
        $claim_info = $claims[0];
        $form_id = $claim_info->form_id;

        // get the information of the forms related to the claim.
        $form_info = $this->forms_model->selectSingleRecord('form_id', $form_id);

        $fill_in_needed = $form_info[0]->fill_in_needed;
        // $first_remind = $form_info[0]->first_remind;
        // $second_remind = $form_info[0]->second_remind;
        // first check the reminder if reminder not set then change the status
        // and create the pdf of the form and send the link.

        if ($fill_in_needed == "0") {

            $this->create_pdf_for_forms($form_id, $claim_id);

            $claims_model = new claims_model();

            $claim_info->status = "Afgerond";

            $claim_info->update();
            return TRUE;
        }

        // if the claim was not found then set flash with error
        // and redirect to the base url
        if (count($claims) == 0) {
            $this->session->set_flashdata('error', 'The claim was not found. Could not send the link.');
            // redirect(base_url() . "admin");
            // only writing base_url will redirect to admin so flash would be
            // lost so apped admin at the end
            return FALSE;
        }

        // get the email for the policy holder
        $claim = $claims[0];

        $policy_holder = $claim->get_policy_holder();
        $policy_holder_email = $policy_holder->email;
        $policy_holder_email = strtolower($policy_holder_email); // lower case
        // the email if
        // reuquired
        // get the
        // client id so
        // that we can
        // get the
        // email
        // template
        // associated
        // with it

        $client_id = $claim->client_id;
        $admin_setting_info = $this->client_setting_model->get_where(
                array('client_id' => $client_id
                ));

        $smtp_user = (string) $admin_setting_info[0]->email;
        $smtp_pass = (string) $admin_setting_info[0]->password;
        $smtp_server = (string) $admin_setting_info[0]->smtp_server;
        $smtp_port = $admin_setting_info[0]->smtp_port;
        //get client logo
        $client_info = $this->clients_model->get_where(array('client_id' => $client_id));
        $client_logo = $admin_setting_info[0]->mail_logo;
        $logo_path = base_url() . 'assets/upload/mail_logo/' . $client_logo;
        $logo_exist = 'assets/upload/mail_logo/' . $client_logo;
        if (isset($client_logo) && $client_logo !== '') {
            if (file_exists($logo_exist)) {
                $type = pathinfo($logo_exist, PATHINFO_EXTENSION);
                $data = file_get_contents($logo_exist);
                $base64 = base_url() . 'assets/upload/mail_logo/' . $client_logo;
                //$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $logo_url = '<img src="' . $base64 . '" border="0" style="display: block; max-width: 200px;">';
            } else {
                $logo_url = '';
            }
        } else {
            $logo_url = '';
        }
        //end client logo

        $this->load->model('client_email_template_model');
        $client_email_templates = new client_email_template_model();

        $obj_policy_holder = new policy_holder_model();
        $policy_holder_record = $obj_policy_holder->get_where(
                array('policy_holder_id' => $claim_info->policy_holder_id
                ));

        $randid = $this->generateRandStr(10);
        $obj_policy_holder->password_reset_random_string = $randid;
        $obj_policy_holder->policy_holder_id = $policy_holder_record[0]->policy_holder_id;
        $obj_policy_holder->updatePasswordRandomString();
//         if ($policy_holder_record[0]->first_name != '') {}
        $policy_first_name = 'test name';

        if ($policy_first_name != '') {
            if (isset($form_info[0]->mail_new_form) && $form_info[0]->mail_new_form != NULL) {
                $client_email_templates = $client_email_templates->get_where(
                        array('client_id' => $client_id, 'email_id' => $form_info[0]->mail_new_form));
            } else {
                $client_email_templates = $client_email_templates->get_where(
                        array('client_id' => $client_id, 'email_type' => 'form link'
                        ));
            }
            // if the template was not found
            if (count($client_email_templates) == 0) {
                $this->session->set_flashdata('error', 'The mail template with name "form link" was not found for sending email.');
                return FALSE;
                //redirect(base_url() . "admin");
            }
            $template = $client_email_templates[0];
            $message = $template->email_message; // get the message
            $message = str_replace('&lt;name&gt;', $policy_holder->first_name, $message);
            $message = str_replace('&lt;logo&gt;', $logo_url, $message);
            $message = str_replace('../../../../', base_url(), $message);
            // get the policy holder name in the message
            $link = '<a href="' . base_url() .
                    'user/authenticate/login_with_link/' . $randid .
                    '">Link</a> ';
            // constructing the link with random string
            $message = str_replace('&lt;link&gt;', $link, $message);
            if (isset($claim_info->userid) && $claim_info->userid != NULL && $claim_info->userid != '') {
                $user_info = $this->users_model->get_where(array('user_id' => $claim_info->userid));
                $message = str_replace('&lt;voornaam&gt;', $user_info[0]->first_name, $message);
                $message = str_replace('&lt;tussenvoegsel&gt;', $user_info[0]->middle_name, $message);
                $message = str_replace('&lt;achternaam&gt;', $user_info[0]->last_name, $message);
                $message = str_replace('&lt;bedrijf&gt;', $user_info[0]->company, $message);
            }
            // set the link in the message string load helper and send email
        } else {
            $client_email_templates = $client_email_templates->get_where(
                    array('client_id' => $client_id,
                        'email_type' => 'new_registration'
                    ));
            // if the template was not found
            if (count($client_email_templates) == 0) {
                $this->session->set_flashdata('error', 'The mail template with name "new_registration" was not found for sending email.');
                return FALSE;
                //redirect(base_url() . "admin");
            }

            $template = $client_email_templates[0];
            $message = $template->email_message; // get the message
            // get the policy holder name
            // in the message
            $link = '<a href="' . base_url() .
                    'user/authenticate/login_without_link/' . $randid .
                    '">Link</a> ';
            $message = str_replace('&lt;logo&gt;', $logo_url, $message);
            $message = str_replace('&lt;link&gt;', $link, $message);
            // set the link in the message string load helper and send email
        }

        $email_attachment = NULL;
        if (isset($template->email_attachment) && $template->email_attachment != '') {
            $attached_file = FCPATH . "assets/upload/client_email_attachments/" . $template->email_attachment;
            if (file_exists($attached_file)) {
                $email_attachment = $attached_file;
            }
        }
        $this->load->helper('daynamic_sending_mail');
        $sent_msg = dynamic_send_mail($policy_holder_email, $template->email_subject, $message, '', $email_attachment, $smtp_user, $smtp_pass, $smtp_server, $smtp_port);
        // if email sending was successful then set the success message and then
        // redirect to admin page
        if ($sent_msg == TRUE) {
            $this->claims_model->claim_id = $claim->claim_id;
            $this->claims_model->status = 'open';
            $this->claims_model->mail_send_date = get_current_time()->get_date_for_db();
            $this->claims_model->update_mail_send_date();

            ////add the mail log file 
            $this->load->model('log_mails_model');
            $log_data['date_time'] = date("Y-m-d H:i:s");
            $log_data['subject'] = $template->email_subject;
            $log_data['text'] = $message;
            $log_data['from'] = $smtp_user;
            $log_data['to'] = $policy_holder_email;
            $log_data['from_id'] = $form_id;
            $check_log = $this->log_mails_model->insert_data($log_data);
            $this->session->set_flashdata('success', 'Email is verzonden!');
            return TRUE;
            //redirect(base_url() . "admin");
        } else {
            // show_error($this->email->print_debugger());
            $this->session->set_flashdata('error', 'Check Email Setting!');
            return FALSE;
            // redirect(base_url() . "admin");
        }
    }

    function sendNewsletter($form_tag, $policy_holder_id, $client_id) {
        $newletter_model = new newletter_model();
        $article_model = new article_model();
        $newletters = $newletter_model->get_where(array('client_id' => $client_id, 'form_code' => $form_tag));
        if (!empty($newletters) && $newletters[0]->template != null && $newletters[0]->template != '') {
            $newletter_template = $this->newsletter_template_model->get_where(array('email_id' => $newletters[0]->template));
            if (!empty($newletter_template)) {
                $policy_holder = $this->policy_holder_model->get_where(array('policy_holder_id' => $policy_holder_id));
                $message = $newletters[0]->title . '<br>';
                $message .= $newletter_template[0]->email_message;
                $articles_message = '';
                $message = str_replace('&lt;introduction_text&gt;', $newletters[0]->introduction_text, $message);

                $news_articles_ids = $this->newsletter_article_model->getNewsArticle($newletters[0]->id);
                if (!empty($news_articles_ids)) {
                    foreach ($news_articles_ids as $news_articles_id) {
                        $artile = $article_model->get_where(array('id' => $news_articles_id));
                        if (!empty($artile)) {
                            $articles_message .=$artile[0]->title . '<br>';
                            $articles_message .=$artile[0]->content . '<br>';
                            if (isset($artile[0]->image) && $artile[0]->image != NULL && $artile[0]->image != '') {
                                $image_path = base_url() . 'assets/upload/article_image/' . $artile[0]->image;
                                $image_exist = 'assets/upload/article_image/' . $artile[0]->image;
                                if (file_exists($image_exist)) {
                                    $articles_message .= '<img src="' . $image_path . '" border="0" style="display: block; max-width: 500px;"> <br>';
                                }
                            }
                        }
                    }
                }
                $message = str_replace('&lt;articles&gt;', $articles_message, $message);
                $message = str_replace('&lt;closure_text&gt;', $newletters[0]->closure_text, $message);

                $admin_setting_info = $this->client_setting_model->get_where(array('client_id' => $client_id));

                $smtp_user = (string) $admin_setting_info[0]->email;
                $smtp_pass = (string) $admin_setting_info[0]->password;
                $smtp_server = (string) $admin_setting_info[0]->smtp_server;
                $smtp_port = $admin_setting_info[0]->smtp_port;
                $email_attachment = NULL;
                $this->load->helper('daynamic_sending_mail');
                $sent_msg = dynamic_send_mail($policy_holder[0]->email, $newletter_template[0]->email_subject, $message, '', $email_attachment, $smtp_user, $smtp_pass, $smtp_server, $smtp_port);
                if ($sent_msg == TRUE) {
                    $sent_newletter = new send_newletter_model();
                    $sent_newletter->newsletter_id = $newletters[0]->id;
                    $sent_newletter->date = date("Y-m-d");
                    $sent_newletter->time = date("H:i:s");
                    $sent_newletter->mail_address = $policy_holder[0]->email;
                    $sent_newletter->name = @$policy_holder[0]->first_name . ' ' . @$policy_holder[0]->last_name;
                    $sent_newletter->save();
                    return TRUE;
                } else {
                    // show_error($this->email->print_debugger());
//                    $this->session->set_flashdata('error', 'Check Email Setting!');
                    return FALSE;
                    // redirect(base_url() . "admin");
                }
            }
        } else {
            return FALSE;
        }
    }

}

