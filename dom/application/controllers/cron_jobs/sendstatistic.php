<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class sendstatistic extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('path');
        $this->load->helper('sending_mail');
        $this->load->model('claims_model');
        $this->load->model('client_setting_model');
        $this->load->model('client_email_template_model');
        $this->load->helper('daynamic_sending_mail');

        $this->load->model('users_model');
    }

    /**
     * The function use to send the statistic mail to the users.
     */
    public function index() {
        //$clients = $this->clients_model->get_all();
        $clients = $this->clients_model->get_where(array('client_id' => '1'));
        foreach ($clients as $client) {
            $send_mail = FALSE;
            $type_statistics = '';
            $statisticdata = '';
            $last_statistics = 0;
            $current_date = get_current_time()->get_date_for_db();
            $client_setting = $this->client_setting_model->get_where(array('client_id' => $client->client_id));
            if (isset($client_setting[0]->mail_type) && $client_setting[0]->mail_type != NULL) {
                if ($client_setting[0]->mail_type == 'Daily') {
                    $type_statistics = 'Vorige dag';
                    $send_mail = TRUE;
                } elseif ($client_setting[0]->mail_type == 'Monthly' && date('d', strtotime($current_date)) == '01') {
                    $type_statistics = 'Vorige maand';
                    $send_mail = TRUE;
                } elseif ($client_setting[0]->mail_type == 'Weekly' && date('N', strtotime($current_date)) == 7) {
                    $type_statistics = 'Vorige week';
                    $send_mail = TRUE;
                } else {
                    $send_mail = FALSE;
                }
            }

            if ($send_mail) {
                $users = $this->users_model->get_where(array('client_id' => $client->client_id, 'administrator' => 1));
                if (!empty($users)) {
                    $to = '';
                    $i = 0;
                    foreach ($users as $user) {
                        if ($i == 0) {
                            $to .= $user->mail_address;
                        } else {
                            $to .= ',' . $user->mail_address;
                        }
                        $i++;
                    }
                    $last_statistics = $this->claims_model->getlastFormImportStatistic($client->client_id, $client_setting[0]->mail_type);
                    //get statistic data start
                    $start_date = date('Y-m-d', strtotime($client->registration_date));
                    $end_date = get_current_time()->get_date_for_db();
                    $months = $this->get_months($start_date, $end_date);
                    $month_name = array('01' => 'januari', '02' => 'februari', '03' => 'maart',
                        '04' => 'april', '05' => 'mei', '06' => 'juni', '07' => 'juli',
                        '08' => 'augustus', '09' => 'september', '10' => 'oktober',
                        '11' => 'november', '12' => 'december'
                    );
                    $statisticdata .='<div id="statisticdata"><table class="table table-striped" style="width:40%;" >';
                    $statisticdata .='<tr>
                                      <th style="text-align: left;">Maand</th>
                                      <th style="text-align: left;">Verzonden formulieren</th>
                                  </tr>';
                    foreach ($months as $month) {
                        $statisticdata .='<tr>
                                        <td>' . ucwords($month_name[date('m', strtotime($month))]) . '-' . date('Y', strtotime($month)) . '</td>
                                        <td class="text-center">' . $this->claims_model->getFormImportInMonth($client->client_id, date('m', strtotime($month)), date('Y', strtotime($month))) . '</td>
                                    </tr>';
                    }
                    $statisticdata .='</table></div>';


                    //get smtp info 
                    $admin_setting_info = $this->client_setting_model->get_where(array('client_id' => $client->client_id));

                    $smtp_user = (string) $admin_setting_info[0]->email;
                    $smtp_pass = (string) $admin_setting_info[0]->password;
                    $smtp_server = (string) $admin_setting_info[0]->smtp_server;
                    $smtp_port = $admin_setting_info[0]->smtp_port;

                    $client_email_templates = new client_email_template_model();
                    $client_email_templates = $client_email_templates->get_where(array('client_id' => $client->client_id, 'email_type' => 'statistic mail'));
                    $template = $client_email_templates[0];
                    $message = $template->email_message; // get the message
                    $message = str_replace('&lt;type&gt;', $type_statistics, $message);
                    $message = str_replace('&lt;last_statistics&gt;', $last_statistics, $message);
                    $message = str_replace('&lt;full_statistics&gt;', $statisticdata, $message);
                    $this->load->helper('daynamic_sending_mail');
                    if ($to != '') {
                        $sent_msg = dynamic_send_mail($to, $template->email_subject, $message, '', $template->email_attachment, $smtp_user, $smtp_pass, $smtp_server, $smtp_port);
                    }
                }
            }
        }
    }

    function get_months($date1, $date2) {
        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        $my = date('m-Y', $time2);
        $mesi = array('Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni', 'Juli',
            'Augustus', 'September', 'Oktober', 'November', 'December'
        );

        // $months = array(date('F', $time1));
        $months = array();
        $f = '';

        while ($time1 < $time2) {
            if (date('m-Y', $time1) != $f) {
                $f = date('m-Y', $time1);
                if (date('m-Y', $time1) != $my && ( $time1 < $time2 )) {
                    $str_mese = ( date('m', $time1) ); // $mesi[(date('m',
                    // $time1)
                    // - 1)];
                    $months[] = '01' . '-' . $str_mese . '-' . date('Y', $time1);
                }
            }
            $time1 = strtotime(( date('Y-m-d', $time1) . ' +15days'));
        }

        $str_mese = ( date('m-Y', $time2) ); // $mesi[(date('m', $time2) - 1)];
        $months[] = '01' . '-' . $str_mese . '-' . date('Y', $time2);
        return $months;
    }

}