<?php

/*
 *
 * Author : DINESH
 * 
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class reminder extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('path');
        $this->load->helper('sending_mail');
        $this->load->model('claims_model');
        $this->load->model('forms_model');
        $this->load->model('policy_holder_model');
        $this->load->model('client_setting_model');
        $this->load->model('client_email_template_model');
        $this->load->helper('daynamic_sending_mail');
    }

    /**
     * The function use to send the reminder mail for the forms.
     */
    public function index() {
        $policy_holders = $this->policy_holder_model->get_where(array('status' => 'A'));
        if ($policy_holders) {
            foreach ($policy_holders as $policy_holder) {
//                if ($policy_holder->email == 'dinesh@devrepublic.nl') {
                    $claims = $this->claims_model->get_where(array('status' => 'open', 'policy_holder_id' => $policy_holder->policy_holder_id, 'is_delete' => 0));
                    if ($claims) {
                       
                        //mail send setting start ..
                        $policy_holder_email = $policy_holder->email;
                        $policy_holder_email = strtolower($policy_holder_email);
                        $client_id = $policy_holder->client_id;
                        $admin_setting_info = $this->client_setting_model->get_where(
                                array('client_id' => $client_id
                                ));
                        $smtp_user = (string) $admin_setting_info[0]->email;
                        $smtp_pass = (string) $admin_setting_info[0]->password;
                        $smtp_server = (string) $admin_setting_info[0]->smtp_server;
                        $smtp_port = $admin_setting_info[0]->smtp_port;
                        //get client logo
                        $client_info = $this->clients_model->get_where(array('client_id' => $client_id));
                        $client_logo = $admin_setting_info[0]->mail_logo;
                        $logo_path = base_url() . 'assets/upload/mail_logo/' . $client_logo;
                        $logo_exist = 'assets/upload/mail_logo/' . $client_logo;
                        if (isset($client_logo) && $client_logo !== '') {
                            if (file_exists($logo_exist)) {
                                $type = pathinfo($logo_exist, PATHINFO_EXTENSION);
                                $data = file_get_contents($logo_exist);
                                $base64 = base_url() . 'assets/upload/mail_logo/' . $client_logo;
                                //$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                $logo_url = '<img src="' . $base64 . '" border="0" style="display: block; max-width: 200px;">';
                            } else {
                                $logo_url = '';
                            }
                        } else {
                            $logo_url = '';
                        }
                        //end client logo
                        //client mail template 

                        $client_email_templates = $this->client_email_template_model->get_where(array('client_id' => $client_id, 'email_type' => 'reminder mail'));

                        $template = $client_email_templates[0];
                        $message = $template->email_message; // get the message
                        $message = str_replace('&lt;name&gt;', $policy_holder->first_name, $message);
                        $message = str_replace('&lt;logo&gt;', $logo_url, $message);
                        $message = str_replace('../../../../', base_url(), $message);
                        //mail send setting end ..
                        foreach ($claims as $claim) {
                            $form_id = $claim->form_id;
                            $mail_send_date = $claim->mail_send_date;
                            $reminder_info = get_reminder_mail_info($form_id);

                            $dStart = new DateTime(date('Y-m-d', strtotime($mail_send_date)));
                            $dEnd = new DateTime(date("Y-m-d"));
                            $dDiff = $dStart->diff($dEnd);
                            $diff = $dDiff->days;
                            if (isset($reminder_info['second_remind']) && $reminder_info['second_remind'] != 0 && $diff > $reminder_info['second_remind'] && $claim->second_remind_date == NULL) {
                                $expire_check_days = $reminder_info['second_remind'] + 30;
                                if ($diff > $expire_check_days) {

                                    $randid = $this->generateRandStr(10);
                                    $this->policy_holder_model->password_reset_random_string = $randid;
                                    $this->policy_holder_model->policy_holder_id = $policy_holder->policy_holder_id;
                                    $this->policy_holder_model->updatePasswordRandomString();
                                } else {

                                    $randid = $this->generateRandStr(10);
                                    $this->policy_holder_model->password_reset_random_string = $randid;
                                    $this->policy_holder_model->policy_holder_id = $policy_holder->policy_holder_id;
                                    $this->policy_holder_model->updatePasswordRandomString();
                                    // get the policy holder name in the message
                                    $link = '<a href="' . base_url() .
                                            'user/authenticate/login_with_link/' . $randid .
                                            '">Link</a> ';
                                    // constructing the link with random string
                                    $message = str_replace('&lt;link&gt;', $link, $message);
                                    $message = str_replace('&lt;reminder_text&gt;', @$reminder_info['s_reminder_text'], $message);

                                    $sent_msg = dynamic_send_mail($policy_holder_email, $template->email_subject, $message, '', $template->email_attachment, $smtp_user, $smtp_pass, $smtp_server, $smtp_port);


                                    if ($sent_msg == TRUE) {
                                        $this->claims_model->update_reminder_date($claim->claim_id, 'second_remind');
//                                    ////add the mail log file 
//                                    $this->load->model('log_mails_model');
//                                    $log_data['date_time'] = date("Y-m-d H:i:s");
//                                    $log_data['subject'] = $template->email_subject;
//                                    $log_data['text'] = $message;
//                                    $log_data['from'] = $smtp_user;
//                                    $log_data['to'] = $policy_holder_email;
//                                    $log_data['from_id'] = $form_id;
//                                    $check_log = $this->log_mails_model->insert_data($log_data);
                                    }
                                    break;
                                }
                            } elseif (isset($reminder_info['first_remind']) && $reminder_info['first_remind'] != 0 && $diff > $reminder_info['first_remind'] && $claim->first_remind_date == NULL && $claim->second_remind_date == NULL) {
                                $randid = $this->generateRandStr(10);
                                $this->policy_holder_model->password_reset_random_string = $randid;
                                $this->policy_holder_model->policy_holder_id = $policy_holder->policy_holder_id;
                                $this->policy_holder_model->updatePasswordRandomString();
                                // get the policy holder name in the message
                                $link = '<a href="' . base_url() .
                                        'user/authenticate/login_with_link/' . $randid .
                                        '">Link</a> ';
                                // constructing the link with random string
                                $message = str_replace('&lt;link&gt;', $link, $message);
                                $message = str_replace('&lt;reminder_text&gt;', @$reminder_info['f_reminder_text'], $message);

                                $sent_msg = dynamic_send_mail($policy_holder_email, $template->email_subject, $message, '', $template->email_attachment, $smtp_user, $smtp_pass, $smtp_server, $smtp_port);

                                if ($sent_msg == TRUE) {
                                    //save thre reminder mail send date
                                    $this->claims_model->update_reminder_date($claim->claim_id, 'first_remind');

                                    ////add the mail log file 
//                                    $this->load->model('log_mails_model');
//                                    $log_data['date_time'] = date("Y-m-d H:i:s");
//                                    $log_data['subject'] = $template->email_subject;
//                                    $log_data['text'] = $message;
//                                    $log_data['from'] = $smtp_user;
//                                    $log_data['to'] = $policy_holder_email;
//                                    $log_data['from_id'] = $form_id;
//                                    $check_log = $this->log_mails_model->insert_data($log_data);
                                }
                                break;
                            } else {
                                
                            }
                        }
                    }
//                }
            }
        }
    }

    function generateRandStr($length) {
        $randstr = "";
        for ($i = 0; $i < $length; $i++) {
            $randnum = mt_rand(0, 61);
            if ($randnum < 10) {
                $randstr .= chr($randnum + 48);
            } else if ($randnum < 36) {
                $randstr .= chr($randnum + 55);
            } else {
                $randstr .= chr($randnum + 61);
            }
        }
        return $randstr;
    }

}