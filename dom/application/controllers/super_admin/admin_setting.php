<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class admin_setting extends CI_Controller
{

    var $default_template_name;

    var $id;

    function __construct ()
    {
        parent::__construct();
        $this->load->model('super_admin_setting_model');
        $this->default_template_name = get_super_admin_template_name();
    
    }

    public function index ()
    {
        $data['page_name'] = 'super_admin/admin/setting/update_setting';
        $data['title'] = 'Change Mail';
        
        $result = $this->super_admin_setting_model->get_all();
        $data['super_admin'] = $result[0];
        $this->load->view($this->default_template_name, $data);
    
    }

    public function update_setting( $id )
    {
        // get the object and validations rules
        $super_admin = new super_admin_setting_model();
        $rules = $super_admin->validation_rules;
        // file errors if any
        $error = array();
        // try uploadng the file
        // if file is selected then only the upload will be done
        // it would be success if no file was selested
        $logo_upload_status = my_file_upload('logo', 
                $super_admin->logo_upload_config(), $this);
        
        // if file was uploaded
        if ($logo_upload_status['status'] == 0)
        {
            // set the error message for fors in the flash
            $success = false;
            
            $this->session->set_flashdata('file_errors', 
                    $logo_upload_status['data']);
            redirect(base_url() . "super_admin/admin/setting");
        }
        else
        {
            $success = true;
        }
        
        // if upload was ok
        if ($success == true)
        {
            $this->form_validation->set_rules($rules);
            
            if ($this->form_validation->run() == FALSE)
            {
                $this->index();
            }
            else
            {
                // get the data to be updated
                $super_admin = new super_admin_setting_model();
                $super_admin = $super_admin->get_where(
                        array('id' => $id
                        ));
                
                // get the first result as where return multiple things
                $super_admin = $super_admin[0];
                
                // remove the previous file if exists
                if (isset($logo_upload_status['data']))
                {
                    if (isset($super_admin->logo) && $super_admin->logo != '')
                    {
                        $path_to_be_removed = substr(
                                $super_admin->logo_upload_path . "/" .
                                         $super_admin->logo, 2);
                        @unlink($path_to_be_removed);
                    }
                }
                
                // get the path to new fiel uploded
                
                if (!isset($logo_upload_status['data']))
                {
                    // if no file was chosen then the path would not be changed
                    $logo_upload_status['data'] = $super_admin->logo;
                }
                $new_file_path = $logo_upload_status['data'];
                
                $super_admin->logo = $new_file_path; // set the value to newly
                                                     // uploaded file's path
                
                $super_admin->email = $this->input->post('super_admin_email');
                $super_admin->ftp_host = $this->input->post('ftp_host');
                $super_admin->ftp_username = $this->input->post('ftp_username');
                $super_admin->ftp_password = $this->input->post('ftp_password');
                $super_admin->ftp_path = $this->input->post('ftp_path');
                // update the value to database
                $val = $super_admin->update();
                $this->session->set_flashdata('success', 
                        'Data Updated Successfully');
                redirect('super_admin/admin/setting');
            }
        }
        else
        {
            $this->form_validation->set_value('super_admin_email');
            $this->session->set_flashdata('error', 'Data Not Updated');
            $this->index();
        }
    
    }

}

?>
