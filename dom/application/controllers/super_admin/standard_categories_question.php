<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class standard_categories_question extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('standard_forms_categories_question_model');
        $this->load->model('standard_forms_categories_model');
        $this->load->model('standard_forms_categories_question_answer_model');
        $this->load->model('standard_forms_model');
        $this->default_template_name = get_mat_super_admin_template_name();
        language_change::setLanguage();
    }

    public function index($id) {

        $result_category = $this->standard_forms_categories_model->selectSingleRecord(
                'cat_id', $id);
        if ($result_category !== FALSE) {
            $data['page_name'] = 'super_admin/standard_forms/questions/view_question';
            $data['title'] = $this->lang->line('categories_questions');
            $result_category = $this->standard_forms_categories_model->selectSingleRecord(
                    'cat_id', $id);
            $data['category_details'] = $result_category[0];

            $result_form = $this->standard_forms_model->selectSingleRecord('form_id', $result_category[0]->form_id);
            $data['forms_details'] = $result_form[0];
            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', $this->lang->line('question_not_found'));
            redirect('super_admin/standard_forms', 'refresh');
        }
    }

    function getJson($id) {
        $records = $this->standard_forms_categories_question_model->get_where_sort(array('cat_id' => $id), 'asc');

        if ($records !== FALSE) {
            $array = $this->get_array_for_json($records);
        } else {
            $array = array();
        }
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function get_array_for_json($objects) {
        $arra = array();
        $i = 1;
        $count = count($objects);
        foreach ($objects as $value) {
            // deleted strip_slashes and deleted relative path
            $que_val = str_replace('../../../../../', base_url(), $value->question);
            $que_val = str_replace('../../../', base_url(), $value->question);
            $re_que_value = strip_slashes($que_val);

            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() .
                    'super_admin/standard_categories_question/edit/' . $value->question_id .
                    '">' . $value->question_id . '</a>';
            $temp_arr[] = '<a href="' . base_url() .
                    'super_admin/standard_categories_question/edit/' . $value->question_id .
                    '">' . $re_que_value . '</a>';

            if ($value->answer_kind == 'textarea') {
                $temp_arr[] = '<span title="Answer can not be added for question where answer type is multiline">Klik Hier</span>';
            } else if ($value->answer_kind == 'text') {
                $temp_arr[] = '<span title="Answer can not be added where answer type is single line">Klik Hier</span>';
            } else if ($value->answer_kind == 'date') {
                $temp_arr[] = '<span title="Answer can not be added where answer type is date">Klik Hier</span>';
            } else if ($value->answer_kind == 'number') {
                $temp_arr[] = '<span title="Answer can not be added where answer type is number line">Klik Hier</span>';
            } else {
                $temp_arr[] = "<a href=\"" . base_url() . "super_admin/standard_categories_question_answer/" . $value->question_id . "\">" . "Klik Hier </a>";
            }
            $temp_arr[] = " <a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $value->question_id .
                    "' id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title='" . $this->lang->line('delete') . "'><i class='md md-delete'></i></a>";
            $temp_arr[] = 'ques_id_' . $value->question_id;
            $arra[] = $temp_arr;
            $i++;
        }
        return $arra;
    }

    function add($id) {
        $result_category = $this->standard_forms_categories_model->selectSingleRecord('cat_id', $id);
        if ($result_category !== FALSE) {
            $data['page_name'] = 'super_admin/standard_forms/questions/add_question';
            $data['title'] = $this->lang->line('add_question');
            $data['question_id'] = $id;
            $data['category_details'] = $result_category[0];
            $result_form = $this->standard_forms_model->selectSingleRecord('form_id', $result_category[0]->form_id);
            $data['forms_details'] = $result_form[0];
            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', $this->lang->line('category_not_found'));
            redirect('super_admin/standard_forms', 'refresh');
        }
    }

    /**
     * This is action listener for the add form.
     *
     * This will be invoked when user clicks on save button on add form.
     * This will handel all the data posted by add form.
     *
     * @param int $id
     *            is the category id.
     */
    function addListener($id) {
        $result_category = $this->standard_forms_categories_model->selectSingleRecord('cat_id', $id);
        if ($result_category !== FALSE) {
            $obj = new standard_forms_categories_question_model($id);
            $this->form_validation->set_rules($obj->validation_rules);
            $check_total_runtimebox = $this->input->post(
                    'total_runtime_checkbox');
            if ($this->input->post('answer_kind') == 'radio' ||
                    $this->input->post('answer_kind') == 'checkbox') {
                if ($check_total_runtimebox != false) {
                    for ($i = 1; $i <= $check_total_runtimebox; $i++) {
                        $this->form_validation->set_rules('answers' . $i, '', 'trim|required');
                    }
                } else {
                    $this->form_validation->set_rules('answers1', '', 'trim|required');
                    $this->form_validation->set_rules('answers2', '', 'trim|required');
                }
            }

            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('question');
                $this->form_validation->set_value('help_text');
                $this->form_validation->set_value('answer_kind');
                $this->form_validation->set_value('required');
                $this->form_validation->set_value('answer_kind');

                $this->add($id);
            } else {
                $obj->cat_id = $id;
                $obj->question = $this->input->post('question');
                $obj->sequence = $obj->getNewSequenceId($id);
                $obj->help_text = $this->input->post('help_text');
                $obj->answer_kind = $this->input->post('answer_kind');
                $obj->required = $this->input->post('required') == true ? 1 : 0;

                $check = $obj->dataUpdateSave();
                if ($this->input->post('answer_kind') == 'radio' ||
                        $this->input->post('answer_kind') == 'checkbox') {
                    if ($check_total_runtimebox != false) {
                        for ($i = 1; $i <= $check_total_runtimebox; $i++) {
                            $obj_ans = new standard_forms_categories_question_answer_model();
                            $obj_ans->question_id = $check;
                            $obj_ans->answer = $this->input->post(
                                    'answers' . $i);
                            $obj_ans->save();
                        }
                    }
                }
                if ($check == TRUE) {
                    $this->session->set_flashdata('success', $this->lang->line('add_success'));
                } else {
                    $this->session->set_flashdata('error', $this->lang->line('add_error'));
                }
                redirect('super_admin/standard_categories_question/' . $id, 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', 'Error !! Please Try Again from Strach by click on categoires "Click Here" then Add New Category.');
            redirect('super_admin/standard_forms', 'refresh');
        }
    }

    function edit($id) {

        $result_question = $this->standard_forms_categories_question_model->selectSingleRecord('question_id', $id);
        if ($result_question !== FALSE) {
            $data['page_name'] = 'super_admin/standard_forms/questions/edit_question';
            $data['title'] = $this->lang->line('edit_question');
            $data['question_details'] = $result_question[0];

            $result_category = $this->standard_forms_categories_model->selectSingleRecord('cat_id', $result_question[0]->cat_id);
            $data['category_details'] = $result_category[0];

            $result_form = $this->standard_forms_model->selectSingleRecord('form_id', $result_category[0]->form_id);
            $data['forms_details'] = $result_form[0];

            $result_answers = $this->standard_forms_categories_question_model->getAnswers($id);
            $data['answer_details'] = $result_answers;

            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', $this->lang->line('question_not_found'));
            redirect('super_admin/standard_forms', 'refresh');
        }
    }

    /**
     * This method will be invoked when user clicks on save button on
     * edit page.
     * This will handel all the data posted by edit form.
     *
     * @param int $id
     *            is id of question
     */
    function editListener($id) {
        $result_question = $this->standard_forms_categories_question_model->selectSingleRecord('question_id', $id);
        if ($result_question !== FALSE) {
            $obj = new standard_forms_categories_question_model();
            $rules = $obj->validation_rules;
            unset($rules[2]);
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('question');
                $this->form_validation->set_value('sequence');
                $this->form_validation->set_value('help_text');
                $this->form_validation->set_value('answer_kind');
                $this->form_validation->set_value('required');
                $this->edit($id);
            } else {
                $obj->question_id = $id;
                $obj->cat_id = $result_question[0]->cat_id;
                $obj->question = $this->input->post('question');
                $obj->sequence = $result_question[0]->sequence;
                $obj->help_text = $this->input->post('help_text');
                $obj->required = $this->input->post('required') == true ? 1 : 0;
                $obj->answer_kind = $result_question[0]->answer_kind;

                $check = $obj->dataUpdateSave();
                ////start edit
                $check_total_runtimebox = $this->input->post('total_runtime_checkbox');

                if ($result_question[0]->answer_kind == 'radio' || $result_question[0]->answer_kind == 'checkbox') {
                    $obj_ans = new standard_forms_categories_question_answer_model();
                    $get_ans = $obj_ans->get_where(array('question_id' => $id));

                    foreach ($get_ans as $ans) {

                        $obj_ans = new standard_forms_categories_question_answer_model();
                        $get_ans = $obj_ans->get_where(array('answer_id' => $ans->answer_id));
                        $obj_ans->question_id = $get_ans[0]->question_id;
                        $obj_ans->answer = $this->input->post('given_answer_' . $ans->answer_id);
                        $obj_ans->skip_to_questions = $ans->skip_to_questions;
                        $obj_ans->answer_id = $ans->answer_id;
                        $obj_ans->update();
                    }

                    if ($check_total_runtimebox != false) {
                        for ($i = 1; $i <= $check_total_runtimebox; $i++) {
                            if ($this->input->post('answers' . $i) != '') {
                                $obj_ans = new standard_forms_categories_question_answer_model();
                                $obj_ans->question_id = $id;
                                $obj_ans->answer = $this->input->post('answers' . $i);
                                $obj_ans->save();
                            }
                        }
                    }
                }
                ///end edit
                $result_category = $this->standard_forms_categories_model->selectSingleRecord('cat_id', $result_question[0]->cat_id);
                $this->session->set_flashdata('success', 'record updated Sucessufully');
                redirect('super_admin/standard_categories_question/' . $result_category[0]->cat_id, 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', 'Error !! Please Try Again from Strach by click on categoires "Click Here" then Add New Category.');
            redirect('super_admin/standard_forms', 'refresh');
        }
    }

    function deleteQuestion($cat_id, $question_id) {
        $get_skip_question = $this->standard_forms_categories_question_answer_model->get_where(array('skip_to_questions' => $question_id));
        if (empty($get_skip_question)) {
            $check = $this->standard_forms_categories_question_model->deleteData(
                    $question_id);


            if ($check == TRUE) {
                $this->session->set_flashdata('success', $this->lang->line('delete_success'));
            } else {
                $this->session->set_flashdata('error',  $this->lang->line('delete_error'));
            }
            redirect('super_admin/standard_categories_question/' . $cat_id, 'refresh');
        } else {
            $this->session->set_flashdata('error', $this->lang->line('skip_question_error'));
            redirect('super_admin/standard_categories_question/' . $cat_id, 'refresh');
        }
    }

    function check_sequence_is_correct($org_ques_array, $ques_array, $question) {
        $obj1 = new standard_forms_categories_question_answer_model();
        $obj2 = new standard_forms_categories_question_model();
        $sucess = 1;
        $main_question_info = $obj2->get_where(array('question_id' => $question));
        if ($main_question_info[0]->answer_kind == 'radio') {
            $answers = $obj1->get_where(array('question_id' => $question));
            foreach ($answers as $ans) {
                if ($ans->skip_to_questions != '' && $ans->skip_to_questions != 0) {
                    $diff = array_search($ans->skip_to_questions, $org_ques_array) - array_search($ans->skip_to_questions, $ques_array);
                    if ($diff > 0) {
                        if (array_search($question, $org_ques_array) > array_search($question, $ques_array)) {
                            $sucess = 1;
                        } else {
                            $sucess = 0;
                            break;
                        }
                    } else if ($diff < 0) {
                        if (array_search($question, $org_ques_array) < array_search($question, $ques_array)) {
                            $sucess = 1;
                        } else {
                            $sucess = 0;
                            break;
                        }
                    }
                } else {
                    $sucess = 1;
                }
            }
        } else {
            $sucess = 1;
        }
        return $sucess;
    }

    function sortable() {
        $obj = new standard_forms_categories_question_model();
        $ques_array = $_POST['ques_id'];
        $get_question_info = $obj->get_where(array('question_id' => $ques_array[0]));
        $get_all_question = $obj->get_where_sort(array('cat_id' => $get_question_info[0]->cat_id), 'asc');
        $temp = 1;
        $org_ques_array = array();
        foreach ($get_all_question as $questions) {
            $org_ques_array[] = $questions->question_id;
        }
        for ($i = 0; $i < count($org_ques_array); $i++) {
            if ($org_ques_array[$i] == $ques_array[$i]) {
                continue;
            } else {
                $res = $this->check_sequence_is_correct($org_ques_array, $ques_array, $ques_array[$i]);
                if ($res === 0) {
                    $temp = 0;
                    break;
                } else {
                    $temp = 1;
                }
            }
        }
        if ($temp == 1) {
            $obj->updateAllSequence($_POST['ques_id']);
            $this->session->set_flashdata('success', $this->lang->line('sequence_change_success'));
            return true;
        } else {
            $this->session->set_flashdata('error', $this->lang->line('sequence_change_error'));
            return false;
        }
    }

    public

    function deleteAnswer($answer_id) {
        $obj = new standard_forms_categories_question_answer_model();
        $get_records = $obj->get_where(array('answer_id' => $answer_id));
        if (count($get_records) == 1) {
            $obj->answer_id = $answer_id;
            $check = $obj->delete();
            if ($check == TRUE) {
                $this->session->set_flashdata('success', 'Deleted Sucessufully');
            } else {
                $this->session->set_flashdata('error', 'Error in Delete');
            }
            redirect(base_url() . 'super_admin/standard_categories_question/edit/' . $get_records[0]->question_id, 'refresh');
        } else {
            redirect(base_url() . 'super_admin/standard_forms', 'refresh');
        }
    }

    function add_answer() {
        $obj = new standard_forms_categories_question_model(
                        $this->input->post('category_id'));
        $this->form_validation->set_rules($obj->validation_rules);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('question');
            $this->form_validation->set_value('sequence');
            $this->form_validation->set_value('help_text');
            $this->form_validation->set_value('answer_kind');
            $this->form_validation->set_value('required');
            $this->form_validation->set_value('answer_kind');
            $this->session->set_flashdata('error-text', '* Dit veld is verplicht');
            redirect(
                    base_url() . 'super_admin/standard_categories_question/addListener/' .
                    $this->input->post('category_id'));
        }
        $ans_to_be_inserted = array();

        $validations = array();

        $total_answers = 0;

        // echo "yet to be done ";
        $question = $this->input->post('question');
        $help_text = $this->input->post('help_text');
        $required = $this->input->post('required');
        $sequence = $this->input->post('sequence');
        $answer_kind = $this->input->post('answer_kind');
        $category_id = $this->input->post('category_id');

        $data['question'] = $question;
        $data['help_text'] = $help_text;
        $data['required'] = $required;
        $data['sequence'] = $sequence;
        $data['answer_kind'] = $answer_kind;
        $data['category_id'] = $category_id;

        /*
         * if ($this->input->post('element_to_be_removed')) { echo
         * $this->input->post('element_to_be_removed'); }
         */

        //
        if ($this->input->post('answer_count')) {
            $data['answer_count'] = $this->input->post('answer_count');
        } else {
            $data['answer_count'] = 0;
        }

        $answer_count = $data['answer_count'];

        if ($answer_count > 0) {
            for ($i = 1; $i <= $answer_count; $i++) {
                if ($this->input->post('ans_' . $i)) {

                    // if there is an element that is to be removed
                    if ($this->input->post('element_to_be_removed')) {
                        // if the element is the one that should not be removed
                        if ($this->input->post('element_to_be_removed') !=
                                ( 'ans_' . $i )) {
                            $data['ans_' . $i] = $this->input->post('ans_' . $i);

                            $ans_to_be_inserted[] = $this->input->post(
                                    'ans_' . $i);
                            $validations[] = array('field' => 'ans_' . $i,
                                'label' => 'Answer',
                                'rules' => 'required|trim|min_length[2]|max_length[255]'
                            );

                            ++$total_answers;
                        }
                    } else {
                        // there is not element that is to be removed
                        $data['ans_' . $i] = $this->input->post('ans_' . $i);
                        $ans_to_be_inserted[] = $this->input->post('ans_' . $i);
                        $validations[] = array('field' => 'ans_' . $i,
                            'label' => 'Answer',
                            'rules' => 'required|trim|min_length[2]|max_length[100]'
                        );
                        ++$total_answers;
                    }
                }
            }
        }

        if ($this->input->post('action')) {
            $action = $this->input->post('action');
            if ($action == 'Add Answer') {
                $data['add_row'] = true;
            }
        }

        if ($this->input->post('action')) {
            $action = $this->input->post('action');

            // if user has clicked on save
            if ($action == 'Save') {

                $this->form_validation->set_rules($validations);

                // if validations fail
                if ($this->form_validation->run() == FALSE) {
                    if ($total_answers < 2) {

                        $this->session->set_flashdata('message_error', 'There should atleast be two answers');
                    }
                    $data['page_name'] = 'admin/forms/questions/add_answer';
                    $data['title'] = 'Add Answers To Question';
                    $this->load->view($this->default_template_name, $data);
                } else {
                    if ($total_answers < 2) {
                        $this->session->set_flashdata('message_error', 'There should atleast be two answers');
                        $data['page_name'] = 'admin/forms/questions/add_answer';
                        $data['title'] = 'Add Answers To Question';
                        $this->load->view($this->default_template_name, $data);
                    } else {
                        // save the details ;

                        $obj = new standard_forms_categories_question_model();
                        $obj->cat_id = $category_id;
                        $obj->question = $question;
                        $obj->sequence = $sequence;
                        $obj->help_text = $help_text;
                        $obj->answer_kind = $answer_kind;
                        $obj->required = $required ? 1 : 0;
                        $check = $obj->dataUpdateSave();

                        $insert_data = $obj->saveAnswers($check, $ans_to_be_inserted);
                        if ($insert_data == TRUE) {
                            $this->session->set_flashdata('success', 'Question Added Sucessufully');
                        } else {
                            $this->session->set_flashdata('error', 'Error in Adding The Question');
                        }
                        redirect('super_admin/standard_categories_question/' . $category_id, 'refresh');
                    }
                }
            } else {
                // otherwise
                $data['page_name'] = 'admin/forms/questions/add_answer';
                $data['title'] = 'Add Answers To Question';

                $this->load->view($this->default_template_name, $data);
            }
        } else {
            $data['page_name'] = 'admin/forms/questions/add_answer';
            $data['title'] = 'Add Answers To Question';

            $this->load->view($this->default_template_name, $data);
        }

        // otherwise
        // print_r($data);
    }

}

?>
