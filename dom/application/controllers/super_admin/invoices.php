<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * list all client invoices
 */
class invoices extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('clients_model');
        $this->default_template_name = get_mat_super_admin_template_name();
        $this->load->model('invoice_model');
        $this->load->model('client_setting_model');
        $this->load->helper('time_worked');
        $this->load->helper('sending_mail');
        $this->load->model('admin_subscription_kinds_model');
        language_change::setLanguage();
    }

    /**
     * renders list page
     */
    public function index() {
        $data['page_name'] = 'super_admin/invoices/index';
        $data['title'] = $this->lang->line('invoices');
        // get the all clients.
        $data['client_info'] = $this->clients_model->get_all();
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * json for data table
     */
    public function get_json_for_all_clients() {
        $invoice_model = new invoice_model();
        $invoices = $invoice_model->get_where(array());

        $arra = $this->_get_array_for_json($invoices);

        $data['aaData'] = $arra;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    private function _get_array_for_json($invoices) {
        $arra = array();

        foreach ($invoices as $invoice) {

            $temp_arr = array();
            $invoice_amount = number_format($invoice->invoice_amount, 2, ',', '');

            $objinvoice = new invoice_model();
            $objinvoice->client_id = $invoice->client_id;
            $client = $objinvoice->get_client();

            if ($client != null && !empty($client)) {

                $temp_arr[] = $client->client_name;
            } else {
                $temp_arr[] = '';
            }
            $temp_arr[] = $invoice->invoice_number;
            $temp_arr[] = $invoice->invoice_subject;
            $temp_arr[] = '&euro; ' . $invoice_amount;
            $temp_arr[] = $invoice->invoice_date;
            $temp_arr[] = anchor(
                    base_url(
                            'super_admin/invoices/create_pdf/' .
                            $invoice->invoice_number), '<img src="' . base_url('assets/img/pdf-icon.png') . '">', array("target" => "_blank"
                    ));
            $temp_arr[] = '<a href="' . base_url() .
                    "super_admin/invoices/send_mail/" . $invoice->invoice_number .
                    '">' . "<img src=\"" . assets_url_img . 'mail.png' .
                    '"   alt="E-Mail" title="'.$this->lang->line('email').'"> ' . '</a>';
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    // create pdf files for invoices
    public function create_pdf($invo_number_with_year) {
        define('EURO', chr(128));
        $no_with = substr($invo_number_with_year, 4);
        $invo_number = (int) $no_with;

        $invoice_model = new invoice_model();
        $invoices = $invoice_model->get_where(
                array('invoice_number' => $invo_number
                ));

        $obj_client = new clients_model();
        $client_info = $obj_client->get_where(
                array('client_id' => $invoices[0]->client_id
                ));

        // load the helper for get the libaray files
        $this->load->helper('admin_setting_info');

        $this->load->helper('pdf/generate_pdf');

        // it include the lib needed to generate the PDF files

        include_lib_pdf();

        $pdf = new FPDF();

        $pdf->AddPage();

        $this->load->helper('admin_setting_info');
        $logo_url = get_super_admin_logo();
        $pdf->Image($logo_url, 65, 10, 100, 25);
        $pdf->Ln(35);

        /*
         * this medthod makes the cell for table @param1 : width of cell @param2
         * : height of cell @param3 : content of cell @param4 : border of cell
         * (1 = YES | 0 = NO) @param5 : move to next line (1 = YES | 0 = NO)
         * @param6 : aligment of text (L= Left | C = CENTER | R = RIGHT)
         */

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(25, 6, 'Factuuradres', 0, 0, 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(5, 6, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 6, ucwords($client_info[0]->title_application), 0, 1, 'L');
        $pdf->Cell(30, 6, NULL, 0, 0, 'L');
        $pdf->Cell(Null, 6, ucwords($client_info[0]->address), 0, 1, 'L');
        $pdf->Cell(30, 6, NULL, 0, 0, 'L');
        $pdf->Cell(Null, 6, ucwords($client_info[0]->city), 0, 1, 'L');
        $pdf->Cell(30, 6, NULL, 0, 0, 'L');
        $pdf->Cell(Null, 6, ucwords($client_info[0]->zip_code), 0, 1, 'L');

        $pdf->Ln();

        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(25, 8, 'Datum', 0, 0, 'L');
        $pdf->Cell(5, 8, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 8, date('d-m-Y', strtotime($invoices[0]->invoice_date)), 0, 1, 'L');

        $pdf->Cell(25, 8, 'Factuurnummer', 0, 0, 'L');
        $pdf->Cell(5, 8, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 8, $invoices[0]->invoice_number, 0, 1, 'L');

        $pdf->Cell(25, 8, 'Onderwerp', 0, 0, 'L');
        $pdf->Cell(5, 8, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 8, $invoices[0]->invoice_subject, 0, 1, 'L');

        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(95, 7, ' Specificaties Diensten', 'LT', 0, 'L');
        $pdf->Cell(95, 7, 'Totaal ', 'RT', 1, 'R');
        $pdf->SetFont('Arial', '', 10);
        // $pdf->Cell(95, 7, ' Abonnement : NULL', 'LB', 0, 'L');
        $pdf->Cell(95, 7, '', 'LB', 0, 'L');
        $pdf->SetFont('', '', 10);
        $pdf->Cell(95, 7, EURO . ' ' . $invoices[0]->invoice_amount . ' ', 'RB', 1, 'R');

        $pdf->Ln();
        $pdf->Cell(168, 7, NULL, 'LTB', 0, 'L');
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(10, 7, 'Totaal : ', 'TB', 0, 'R');
        $pdf->SetFont('Arial', '', 10);

        $pdf->Cell(12, 7, EURO . ' ' . $invoices[0]->invoice_amount . ' ', 'RTB', 1, 'R');

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(Null, 10, 'Wij danken u hartelijk voor het gebruik maken van onze dienstverlening.', 0, 1, 'C');
        $pdf->Output();
    }

    public function send_mail($invoice_number) {
        $no_with0 = substr($invoice_number, 4);
        $invo_number = (int) $no_with0;

        $invoice_model = new invoice_model();
        $invoices = $invoice_model->get_where(
                array('invoice_number' => $invo_number
                ));
        $client = $invoices[0]->get_client();
        // get the client email id.
        $client_info = $this->client_setting_model->get_where(
                array('client_id' => $client->client_id
                ));

        $this->load->model('client_email_template_model');
        $client_email_templates = new client_email_template_model();
        $client_email_templates = $client_email_templates->get_where(
                array('client_id' => $client->client_id,
                    'email_type' => 'invoice'
                ));
        // if the template was not found
        if (count($client_email_templates) == 0) {
            $this->session->set_flashdata('error', 'The mail template with name "invoice" was not found for sending 
        email .
                 ');
            redirect(base_url() . "super_admin/invoices");
        }

        $template = $client_email_templates[0];
        $message = $template->email_message; // get the message
        $message = str_replace(' &
                 lt;
        name & gt;', $client->client_name, $message);
        $tomail = $client_info[0]->email;
        $subject = $template->email_subject;

        $this->createPDFForEmail($invoice_number);

        $this->load->helper('path');
        $link_to_claim_pdf2 = set_realpath("assets/invoice_pdf_files/");
        $link_to_claim_pdf2 .= $invoice_number . '.pdf';

        $sent_msg = send_mail($tomail, $subject, $message, '', $link_to_claim_pdf2);
        if ($sent_msg == TRUE) {

            $this->invoice_model->invoice_number = $invo_number;
            $this->invoice_model->mail_sent = get_current_time()->get_date_for_db();
            $this->invoice_model->update_mail_sent_date();
            unlink($link_to_claim_pdf2);
            $this->session->set_flashdata('success', 'An E-Mail has been send');
            redirect(base_url() . "super_admin/invoices");
        } else {
            $this->session->set_flashdata('error', 'An E-Mail not send.');
            redirect(base_url() . "super_admin/invoices");
        }
    }

    // this method is for filtering the data by the client name
    public function getclient_info($client_id = null) {

        // get the client for which we want to get the list
        $array = '';
        $selected = $this->invoice_model->get_filter_Data($client_id);
        if (is_array($selected) && count($selected) > 0) {
            $array = $this->_get_array_for_json($selected);
        }
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    public function createPDFForEmail($invo_number_with_year) {
        define('EURO', chr(128));
        $no_with = substr($invo_number_with_year, 4);
        $invo_number = (int) $no_with;

        $invoice_model = new invoice_model();
        $invoices = $invoice_model->get_where(
                array('invoice_number' => $invo_number
                ));

        $obj_client = new clients_model();
        $client_info = $obj_client->get_where(
                array('client_id' => $invoices[0]->client_id
                ));
        // load the helper for get the libaray files

        $this->load->helper('pdf/generate_pdf');

        // it include the lib needed to generate the PDF files

        include_lib_pdf();

        $pdf = new FPDF();

        $pdf->AddPage();

        $this->load->helper('admin_setting_info');
        $logo_url = get_super_admin_logo();

        $pdf->Image($logo_url, 65, 10, 100, 25);
        $pdf->Ln(35);

        /*
         * this medthod makes the cell for table @param1 : width of cell @param2
         * : height of cell @param3 : content of cell @param4 : border of cell
         * (1 = YES | 0 = NO) @param5 : move to next line (1 = YES | 0 = NO)
         * @param6 : aligment of text (L= Left | C = CENTER | R = RIGHT)
         */

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(25, 10, 'Factuuradres', 0, 0, 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(5, 10, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 10, ucwords($client_info[0]->title_application), 0, 1, 'L');
        $pdf->Cell(30, 10, NULL, 0, 0, 'L');
        $pdf->Cell(Null, 10, ucwords($client_info[0]->address), 0, 1, 'L');
        $pdf->Cell(30, 10, NULL, 0, 0, 'L');
        $pdf->Cell(Null, 10, ucwords($client_info[0]->city), 0, 1, 'L');
        $pdf->Cell(30, 10, NULL, 0, 0, 'L');
        $pdf->Cell(Null, 10, ucwords($client_info[0]->zip_code), 0, 1, 'L');

        $pdf->Ln();

        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(25, 10, 'Datum', 0, 0, 'L');
        $pdf->Cell(5, 10, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 10, date('d-m-Y', strtotime($invoices[0]->invoice_date)), 0, 1, 'L');

        $pdf->Cell(25, 10, 'Factuurnummer', 0, 0, 'L');
        $pdf->Cell(5, 10, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 10, $invoices[0]->invoice_number, 0, 1, 'L');

        $pdf->Cell(25, 10, 'Onderwerp', 0, 0, 'L');
        $pdf->Cell(5, 10, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 10, $invoices[0]->invoice_subject, 0, 1, 'L');

        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(95, 7, ' Specificaties Diensten', 'LT', 0, 'L');
        $pdf->Cell(95, 7, 'Totaal ', 'RT', 1, 'R');
        $pdf->SetFont('Arial', '', 10);

        $pdf->Cell(95, 7, ' ', 'LB', 0, 'L');
        $pdf->SetFont('', '', 10);
        $pdf->Cell(95, 7, EURO . ' ' . $invoices[0]->invoice_amount . ' ', 'RB', 1, 'R');

        $pdf->Ln();
        $pdf->Cell(168, 7, NULL, 'LTB', 0, 'L');
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(10, 7, '
        Totaal : ', 'TB', 0, 'R');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(12, 7, EURO . ' ' .
                $invoices[0]->invoice_amount . ' ', 'RTB', 1, 'R');

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(Null, 10, 'Wij danken u hartelijk voor het gebruik maken van onze dienstverlening.', 0, 1, 'C');

        $path = realpath('assets/invoice_pdf_files');
        $pdf->Output($path . '/' . $invo_number_with_year . '.pdf', 'F')
        ;
    }

}

?>
