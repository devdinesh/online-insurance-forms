<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Details with Forms>Form_fields.
 */
class form_fields extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('form_field_model');
        $this->default_template_name = get_super_admin_template_name();
    }

    public function index($id) {

        $data['page_name'] = 'super_admin/forms/fields/view_field';
        $data['title'] = 'field list';
        $data['form_id'] = $id;
        $this->load->view($this->default_template_name, $data);
        // $this->add();
    }

    function getJson($form_id) {
        $records = $this->form_field_model->get_where(
                array('form_id' => $form_id
                ));
        $array = $this->getArrayForJson($records);

        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function getArrayForJson($objects) {
        $arra = array();
        foreach ($objects as $value) {

            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() . 'super_admin/form_fields/edit/' .
                    $value->field_id . '">' . $value->field_name . '</a>';
            $temp_arr[] = $value->name_on_form;
            $temp_arr[] = "<div class = \"btn-group pull-right\"><a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $value->field_id .
                    "' id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"><img src='" . assets_url_img . "delete.png' alt='Delete' title='Delete'></a></div>";

            $arra[] = $temp_arr;
        }
        return $arra;
    }

    /**
     * this funcrtion will render the Add Form
     */
    function add($form_id) {
        $data['page_name'] = 'super_admin/forms/fields/add_field';
        $data['title'] = 'Forms Fields';
        $data['form_id'] = $form_id;
        $this->load->view($this->default_template_name, $data);
    }

    function addListener($id) {
        $obj = new form_field_model();

        $validation = $obj->validation_rules;

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == FALSE) {

            $this->form_validation->set_value('field_name');
            $this->form_validation->set_value('name_on_form');

            $this->add($id);
        } else {
            $obj->field_name = $this->input->post('field_name');
            $obj->name_on_form = $this->input->post('name_on_form');
            $obj->form_id = $id;

            $obj->save();

            $this->session->set_flashdata('success', "Field added sucessfully");
            redirect('super_admin/super_form_fields/' . $id, 'refresh');
        }
    }

    function edit($id) {
        $data['page_name'] = 'super_admin/forms/fields/edit_field';
        $data['title'] = 'Edit field';
        $field_info = $this->form_field_model->get_where(array('field_id' => $id
                ));

        $data['field'] = $field_info[0];

        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
        $obj = new form_field_model();
        $res = $obj->get_where(array('field_id' => $id));

        $validation = $obj->validation_rules;

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == FALSE) {

            $this->form_validation->set_value('field_name');
            $this->form_validation->set_value('name_on_form');
            $this->edit($id);
        } else {
            $obj->field_name = $this->input->post('field_name');
            $obj->name_on_form = $this->input->post('name_on_form');
            $obj->form_id = $res[0]->form_id;
            $obj->field_id = $id;
            $obj->update($id);
            $this->session->set_flashdata('success', "Field edited sucessfully");
            redirect('super_admin/super_form_fields/' . $res[0]->form_id, 'refresh');
        }
    }

    function delete_field($id) {
        $obj = new form_field_model();
        $res = $obj->get_where(array('field_id' => $id));

        $this->form_field_model->field_id = $id;
        $this->form_field_model->delete();
        redirect('super_admin/super_form_fields/' . $res[0]->form_id, 'refresh');
    }

}