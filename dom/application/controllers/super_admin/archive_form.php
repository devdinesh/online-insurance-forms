<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class archive_form extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->library('image_lib');
        $this->load->model('claim_archive_model');
        $this->default_template_name = get_super_admin_template_name();
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *        START COMMON FUNCTION WHICH CAN BE USED IN ANY FORM SHOW         * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * this will render the super admin login form
     */
    function index() {
        $data['page_name'] = 'super_admin/forms/archive_form/view_form';
        $data['title'] = 'Archive Forms';
// get the all clients.
        $data['client_info'] = $this->clients_model->get_all();
        $this->load->view($this->default_template_name, $data);
    }

    public function getJson($client_id) {
        ini_set('memory_limit', '512M');
        if (isset($client_id) && $client_id != 'nl') {
            $were = '';
            $this->load->library('datatable');
            $this->datatable->aColumns = array('c.claim_id', 'c.claim_sequence_number', 'c.import_date', 'c.schadenummer', 'c.handler', 'c.status', 'c.policy_holder_name', 'c.policy_holder_mail', 'f.form_name', 'c.archive_date', 'c.text_form');
            $this->datatable->eColumns = array('c.claim_id');
            $this->datatable->sIndexColumn = "c.claim_id";
            $this->datatable->sTable = " ins_claim_archive c,ins_forms f";
            $this->datatable->myWhere = "WHERE c.client_id=" . $client_id . " AND c.status='Archief'
AND f.form_id = c.form_id AND c.is_delete = 0" . $were;
            $this->datatable->datatable_process();
            foreach ($this->datatable->rResult->result_array() as $aRow) {
                $temp_arr = array();
                $temp_arr[] = $aRow['form_name'] != '' ? $aRow['form_name'] : null;
                $temp_arr[] = date("d-m-Y", strtotime($aRow['import_date']));
                $temp_arr[] = $aRow['claim_sequence_number'];
                $temp_arr[] = $aRow['policy_holder_name'] != '' ? $aRow['policy_holder_name'] : null;
                $temp_arr[] = $aRow['policy_holder_mail'] != '' ? $aRow['policy_holder_mail'] : null;
                $temp_arr[] = $aRow['handler'] != '' ? $aRow['handler'] : null;
                $temp_arr[] = $aRow['archive_date'] != '' ? date('d-m-Y', strtotime($aRow['archive_date'])) : null;

                if ($aRow['status'] == "Archief") {
                    $temp_arr[] = '<img style="height: 35px;" src="' . assets_url_img . 'archief.png' . '" alt="Archief" title="' . date('d-m-Y', strtotime($aRow['archive_date'])) . '"/>';
                } else {
                    $temp_arr[] = null;
                }

                if (trim($aRow['status']) == 'Archief') {
                    $temp_arr[] = '<a href="#" onclick="open_detail_model(' . $aRow['claim_id'] . ');" class="btn btn-large btn-primary gray-button">Details</a>';
                } else {
                    $temp_arr[] = null;
                }
                $this->datatable->output['aaData'][] = $temp_arr;
            }
        } else {
            $this->datatable->output['aaData'] = array();
            $this->datatable->output['iTotalDisplayRecords'] = 0;
            $this->datatable->output['iTotalRecords'] = 0;
            $this->datatable->output['sEcho'] = 1;
        }
        echo json_encode($this->datatable->output);
        exit();
    }

    public function get_filled_detail($claim_id) {
        $claim_info = $this->claim_archive_model->where(array('claim_id' => $claim_id));
        if ($claim_info) {
            $data = $claim_info[0]->text_form;
        } else {
            $data = "Details not Found.";
        }
        echo json_encode($data);
    }

    public function export($client_id) {
        $ftp = get_ftp_info();
        $this->load->library('ftp');
        $config['hostname'] = $ftp['ftp_host'];
        $config['username'] = $ftp['ftp_username'];
        $config['password'] = $ftp['ftp_password'];
        //$config['debug'] = TRUE; 
        $connect = $this->ftp->connect($config);
        $is_uploaded = TRUE;
        if ($connect) {
            $filename = date('YmdHis') . '_archive.sql';

            if ($client_id != 'nl') {
                $where = "WHERE client_id = {$client_id}";
            } else {
                $where = "WHERE 1";
            }
            $sql = "INSERT INTO ins_temp_claim_archive (SELECT * FROM ins_claim_archive {$where});";
            $result = $this->db->query($sql);
            $this->load->dbutil();
            $prefs = array(
                'tables' => array('ins_temp_claim_archive'), // Array of tables to backup.
                'ignore' => array(), // List of tables to omit from the backup
                'format' => 'txt', // gzip, zip, txt
                'filename' => $filename, // File name - NEEDED ONLY WITH ZIP FILES
                'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
                'add_insert' => TRUE, // Whether to add INSERT data to backup file
                'newline' => "\n"               // Newline character used in backup file
            );
// Load the DB utility class
// Backup your entire database and assign it to a variable
            $backup = $this->dbutil->backup($prefs);
            $backup = str_replace('ins_temp_claim_archive', 'ins_claim_archive', $backup);
            $this->db->empty_table('ins_temp_claim_archive');
// Load the file helper and write the file to your server
            $this->load->helper('file');
            $check = write_file('./assets/db_backup/' . $filename, $backup);
            //upload the file to the given FTP details .. 
            $upload_file = $this->ftp->upload('./assets/db_backup/' . $filename, $ftp['ftp_path'] . $filename, 'ascii', 0775);
            if ($upload_file) {
                unlink('./assets/db_backup/' . $filename);
                $is_uploaded = TRUE;
            } else {
                unlink('./assets/db_backup/' . $filename);
                $is_uploaded = FALSE;
                 
            }
        } else {
            $is_uploaded = FALSE;
        }
        $this->ftp->close();
        if ($is_uploaded) {
            $message = '<tr><td><div class = "alert alert-success"><a class="close" data-dismiss = "alert" href="#">x</a>Archived Claims exported sucessfully and Uploaded to FTP host.</div></td><tr>';
        } else {
            $message = '<tr><td><div class = "alert alert-error"><a class="close" data-dismiss = "alert" href="#">x</a>Could not connect to FTP server. Please check FTP setting.</div></td><tr>';
        }
        echo json_encode(array('success' => TRUE, 'message' => $message));
        exit;
    }
}
