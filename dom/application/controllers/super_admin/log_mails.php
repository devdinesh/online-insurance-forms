<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class log_mails extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('log_mails_model');
        $this->load->model('forms_model');
        $this->default_template_name = get_super_admin_template_name();
    }

    public function index() {
        $data['page_name'] = 'super_admin/admin/log_mails/view';
        $data['title'] = 'Log Mails';
        $this->load->view($this->default_template_name, $data);
    }

    public function get_json() {
        $log_mails_model = new log_mails_model();
        $logs = $log_mails_model->getData();
        $main_arr = array();
        foreach ($logs as $log) {
            $form_info = $this->forms_model->get_where(array('form_id' => $log->from_id));
            $temp = array();
            $temp[] = date('d-m-Y H:i:s', strtotime($log->date_time));
            $temp[] = $log->subject;
           //$temp[] = $log->text;
            $temp[] = $log->from;
            $temp[] = $log->to;
            $temp[] = $form_info[0]->form_name;
            $main_arr[] = $temp;
        }
        $data['aaData'] = $main_arr;
        echo json_encode($data);
    }
}