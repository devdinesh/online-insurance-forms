<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class copy_client_form extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('clients_model');
        $this->load->model('forms_model');
        $this->load->model('standard_forms_model');
        $this->load->model('forms_categories_model');
        $this->load->model('standard_forms_categories_model');
        $this->load->model('forms_categories_question_model');
        $this->load->model('standard_forms_categories_question_model');
        $this->load->model('standard_forms_categories_question_answer_model');
        $this->load->model('form_field_model');
        $this->load->model('standard_form_field_model');


        $this->default_template_name = get_super_admin_template_name();
    }

    public function index() {
        $data['clients'] = $this->clients_model->get_all();
        $data['page_name'] = 'super_admin/copy_form/add';
        $data['title'] = 'Copy Client';
        $this->load->view($this->default_template_name, $data);
    }

    public function copy_form() {
        $client_id = $this->input->post('client_id');
        $forms = $this->forms_model->get_where(array('client_id' => $client_id));
        foreach ($forms as $form) {
            $form_id = $form->form_id;
            /////trying to copy all forms ... 
            $obj = new forms_model();
            $res = $obj->selectSingleRecord('form_id', $form_id);
            if (!empty($res)) {
                ///save form data start
                $std_form_obj = new standard_forms_model();
                $std_form_exist = $std_form_obj->is_form_exist($res[0]->form_name, $client_id);
                if ($std_form_exist == 0) {
                    $std_form_obj->form_name = $res[0]->form_name;
                    $std_form_obj->form_tag = $res[0]->form_tag;
                } else {
                    $std_form_obj->form_name = $res[0]->form_name . '_' . ($std_form_exist);
                    $std_form_obj->form_tag = $res[0]->form_tag . '_' . ($std_form_exist);
                }
                $rand_code = random_string('alpha', 5);
                $std_form_obj->form_code = $rand_code;
                $std_form_obj->fill_in_needed = $res[0]->fill_in_needed;
                $std_form_obj->header_text = $res[0]->header_text;
                $std_form_obj->introduction_text = $res[0]->introduction_text;
                $std_form_obj->show_all_question_at_once = $res[0]->show_all_question_at_once;
                $std_form_obj->closure_text = $res[0]->closure_text;
                $std_form_obj->current_date = get_current_time()->get_date_for_db();
                $last_nserted_id = $std_form_obj->dataUpdateSave();


                //save form data end
                //save category start
                $categories = $this->forms_categories_model->selectMoreRecord('form_id', $form_id);
                if (!empty($categories)) {
                    foreach ($categories as $category) {
                        $old_cat_id = $category->cat_id;
                        $category_obj = new standard_forms_categories_model();
                        $category_obj->form_id = $last_nserted_id;
                        $category_obj->cat_name = $category->cat_name;
                        $category_obj->sequence = $category->sequence;
                        $category_obj->introduction_text = $category->introduction_text;
                        $category_obj->dataUpdateSave();
                        // insert a new record for category
                        $last_inserted_category_id = mysql_insert_id();

                        //save question start ...         
                        $questions = $this->forms_categories_question_model->get_where(array('cat_id' => $old_cat_id));
                        if (count($questions) > 0) {
                            foreach ($questions as $question) {
                                $old_question_id = $question->question_id;
                                $question_obj = new standard_forms_categories_question_model();
                                $question_obj->cat_id = $last_inserted_category_id;
                                $question_obj->question = $question->question;
                                $question_obj->help_text = $question->help_text;
                                $question_obj->sequence = $question->sequence;
                                $question_obj->answer_kind = $question->answer_kind;
                                $question_obj->required = $question->required;
                                $question_obj->dataUpdateSave();
                                $last_inserted_question_id = mysql_insert_id();
                                /* get all the answers for original question */
                                $answers = $this->forms_categories_question_model->getAnswers($old_question_id);
                                // if there wre answers
                                if (is_array($answers) && count($answers) > 0) {

                                    // get an array containing answers
                                    $new_answers = array();
                                    foreach ($answers as $answer) {
                                        $new_answers[] = $answer->answer;
                                    }
                                    // add the answers to the newly inserted
                                    $question_obj->saveAnswers($last_inserted_question_id, $new_answers);
                                }
                            }
                        }
                        //save question end... 
                    }
                }

                //save the form fielsd for that form ..start ..
                //save the form field values.... end
                $form_fields = $this->form_field_model->get_where(array('form_id' => $form_id));
                if (!empty($form_fields)) {
                    foreach ($form_fields as $form_field) {
                        $obj_std_field = new standard_form_field_model();
                        $obj_std_field->field_name = $form_field->field_name;
                        $obj_std_field->name_on_form = $form_field->name_on_form;
                        $obj_std_field->field_type = $form_field->field_type;
                        $obj_std_field->form_id = $last_nserted_id;
                        $obj_std_field->save();
                    }
                }
                //save category end .
                //trying to copy all  forms...
            }
        }
        if ($last_nserted_id) {
            $this->session->set_flashdata('success', 'The Form is Copied Sucessufully');
        } else {
            $this->session->set_flashdata('error', 'Error in Copying The Form');
        }
        redirect('super_admin/standard_forms', 'refresh');
    }

}