<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Details with Forms>Form_fields.
 */
class standard_form_fields extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('standard_form_field_model');
        $this->default_template_name = get_mat_super_admin_template_name();
        language_change::setLanguage();
    }

    public function index($id) {
        $data['page_name'] = 'super_admin/standard_forms/fields/view_field';
        $data['title'] = $this->lang->line('list_import_fields');
        $data['form_id'] = $id;
        $this->load->view($this->default_template_name, $data);
        // $this->add();
    }

    function getJson($form_id) {
        $obj = new standard_form_field_model();
        $records = $obj->get_where(array('form_id' => $form_id));
        $array = $this->getArrayForJson($records);
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function getArrayForJson($objects) {
        $arra = array();
        foreach ($objects as $value) {

            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() . 'super_admin/standard_form_fields/edit/' . $value->field_id . '">' . $value->field_name . '</a>';
            $temp_arr[] = $value->name_on_form;
            if ($value->field_type == 'E') {
                $temp_arr[] = '<button type="button" class="btn btn-block ink-reaction btn-raised btn-xs btn-primary btn-warning">E-Mail</button>';
            } else if ($value->field_type == 'P') {
                $temp_arr[] = '<button type="button" class="btn btn-block ink-reaction btn-raised btn-xs btn-primary btn-success">Polisnummer</button>';
            } else if ($value->field_type == 'B') {
                $temp_arr[] = '<button type="button" class="btn btn-block ink-reaction btn-raised btn-xs btn-primary btn-info">Behandelaar</button>';
            } else if ($value->field_type == 'S') {
                $temp_arr[] = '<button type="button" class="btn btn-block ink-reaction btn-raised btn-xs btn-primary btn-default-dark">Schadenummer</button>';
            } else if ($value->field_type == 'N') {
                $temp_arr[] = '<button type="button" class="btn btn-block ink-reaction btn-default btn-xs">Nomaal</button>';
            } else if ($value->field_type == 'BE') {
                $temp_arr[] = '<button type="button" class="btn btn-block ink-reaction btn-raised btn-xs btn-primary btn-info">Behandelaar E-mail</button>';
            }

            $temp_arr[] = " <a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" . $value->field_id . "' data-toggle=\"tooltip\" data-placement=\"top\" title='" . $this->lang->line('delete') . "'><i class='md md-delete'></i></a>";

            $arra[] = $temp_arr;
        }
        return $arra;
    }

    /**
     * this funcrtion will render the Add Form
     */
    function add($form_id) {
        $data['page_name'] = 'super_admin/standard_forms/fields/add_field';
        $data['title'] =$this->lang->line('add_new_field');
        $data['form_id'] = $form_id;
        $data['show_mail'] = $this->standard_form_field_model->isMailFiledExit($form_id);
        $data['show_ploicy_number'] = $this->standard_form_field_model->isPolicyNumberFiledExit($form_id);
        $data['show_behandler'] = $this->standard_form_field_model->isBehandlerFiledExit($form_id);
        $data['show_schadenummer'] = $this->standard_form_field_model->isschadenummerFiledExit($form_id);
        $data['show_behandler_emial'] = $this->standard_form_field_model->isBehandlerEmailExit($form_id);
        $this->load->view($this->default_template_name, $data);
    }

    function addListener($id) {
        $obj = new standard_form_field_model();
        $validation = $obj->validation_rules;

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == FALSE) {

            $this->form_validation->set_value('field_name');
            $this->form_validation->set_value('name_on_form');
            $this->form_validation->set_value('field_type');

            $this->add($id);
        } else {
            $obj->field_name = $this->input->post('field_name');
            $obj->name_on_form = $this->input->post('name_on_form');

            if ($this->input->post('field_type') != '') {
                $obj->field_type = $this->input->post('field_type');
            } else {
                $obj->field_type = 'N';
            }
            $obj->form_id = $id;
            $obj->save();
            $this->session->set_flashdata('success', $this->lang->line('add_success'));
            redirect('super_admin/standard_form_fields/' . $id, 'refresh');
        }
    }

    function edit($id) {

        $data['page_name'] = 'super_admin/standard_forms/fields/edit_field';
        $data['title'] = $this->lang->line('edit_new_field');
        $field_info = $this->standard_form_field_model->get_where(array('field_id' => $id));
        $data['show_mail'] = $this->standard_form_field_model->isMailFiledExit($field_info[0]->form_id);
        $data['show_ploicy_number'] = $this->standard_form_field_model->isPolicyNumberFiledExit($field_info[0]->form_id);
        $data['show_behandler'] = $this->standard_form_field_model->isBehandlerFiledExit($field_info[0]->form_id);
        $data['show_schadenummer'] = $this->standard_form_field_model->isschadenummerFiledExit($field_info[0]->form_id);
        $data['show_behandler_emial'] = $this->standard_form_field_model->isBehandlerEmailExit($field_info[0]->form_id);
        $data['field'] = $field_info[0];

        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
        $obj = new standard_form_field_model();
        $res = $obj->get_where(array('field_id' => $id));

        $validation = $obj->validation_rules;

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == FALSE) {

            $this->form_validation->set_value('field_name');
            $this->form_validation->set_value('name_on_form');
            $this->edit($id);
        } else {
            $obj->field_name = $this->input->post('field_name');
            $obj->name_on_form = $this->input->post('name_on_form');
            $obj->form_id = $res[0]->form_id;

            if ($this->input->post('field_type') != '') {
                $obj->field_type = $this->input->post('field_type');
            } else {
                $obj->field_type = 'N';
            }

            $obj->field_id = $id;
            $obj->update($id);
            $this->session->set_flashdata('success', $this->lang->line('edit_success'));
            redirect('super_admin/standard_form_fields/' . $res[0]->form_id, 'refresh');
        }
    }

    function delete_field($id) {
        $obj = new standard_form_field_model();
        $res = $obj->get_where(array('field_id' => $id));

        $this->standard_form_field_model->field_id = $id;
        $this->standard_form_field_model->delete();
        $this->session->set_flashdata('success', $this->lang->line('delete_success'));
        redirect('super_admin/standard_form_fields/' . $res[0]->form_id, 'refresh');
    }

}