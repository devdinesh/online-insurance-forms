<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the admin and other
 * methods.
 */
class standard_group_forms extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('form_group_forms_model');
        $this->load->model('standard_forms_model');
        $this->load->model('ins_form_groups_model');
        $this->default_template_name = get_mat_super_admin_template_name();
        language_change::setLanguage();
    }

    /**
     * this will render the admin categoris form
     */
    public function index($id) {
        $data['page_name'] = 'super_admin/form_group_forms/view_group_form';
        $data['title'] = $this->lang->line('form_groups');
        $res = $this->ins_form_groups_model->getWhere(array('form_group_id' => $id));
        if (empty($res)) {
            $this->session->set_flashdata('error', $this->lang->line('group_not_found'));
            redirect('super_admin/form_groups', 'refresh');
        }
        $data['group_info'] = $res[0];
        $data['group_id'] = $id;
        $this->load->view($this->default_template_name, $data);
    }

    function getJson($id) {
        $records = $this->form_group_forms_model->getWhere(array('form_group_id' => $id));
        if ($records !== FALSE) {
            $array = $this->get_array_for_json($records);
        } else {
            $array = array();
        }
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function get_array_for_json($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $standarad_form_info = $this->standard_forms_model->selectSingleRecord('form_id', $value->standard_from_id);
            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() . 'super_admin/standard_group_forms/edit/' . $value->id . '">' . $standarad_form_info[0]->form_name . '</a>';
            $temp_arr[] = "<div class = \"btn-group pull-right\"><a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" . $value->id . "' id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=" . $this->lang->line('delete') . "><i class='md md-delete'></i></a></div>";
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function add($id) {
        $res = $this->ins_form_groups_model->getWhere(array('form_group_id' => $id));
        $added_form = $this->form_group_forms_model->getWhere(array('form_group_id' => $id));
        if ($res !== FALSE) {
            $data['page_name'] = 'super_admin/form_group_forms/add_group_form';
            $data['title'] = $this->lang->line('add_form');
            $all_form = $this->standard_forms_model->get_all();
            $data['standard_forms'] = $all_form;
            $data['group_info'] = $res[0];
            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', $this->lang->line('record_not_found'));
            redirect('super_admin/form_groups', 'refresh');
        }
    }

    function addListener() {
        $id = $this->input->post('form_group_id');
        $res = $this->ins_form_groups_model->getWhere(array('form_group_id' => $id));
        if ($res !== FALSE) {
            $obj = new form_group_forms_model();
            $validation_rules = $obj->validationRules();
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('standard_from_id');
                $this->add($id);
            } else {
                $obj->form_group_id = $id;
                $obj->standard_from_id = $this->input->post('standard_from_id');
                $check_data = $obj->getWhere(array('form_group_id' => $id, 'standard_from_id' => $this->input->post('standard_from_id')));
                if (!empty($check_data)) {
                    $this->session->set_flashdata('error', $this->lang->line('already_added_form'));
                } else {
                    $check = $obj->insertData();
                    if ($check == TRUE) {
                        $this->session->set_flashdata('success', $this->lang->line('add_success'));
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('add_error'));
                    }
                }
                redirect('super_admin/standard_group_forms/' . $id, 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('record_not_found'));
            redirect('super_admin/form_groups', 'refresh');
        }
    }

    function edit($id) {
        $res = $this->form_group_forms_model->getWhere(array('id' => $id));
        if ($res !== FALSE) {
            $data['page_name'] = 'super_admin/form_group_forms/edit_group_form';
            $data['title'] = $this->lang->line('edit_form');
            $all_form = $this->standard_forms_model->get_all();
            $group_info = $this->ins_form_groups_model->getWhere(array('form_group_id' => $res[0]->form_group_id));
            $data['standard_forms'] = $all_form;
            $data['group_info'] = $group_info[0];
            $data['group_form'] = $res[0];
            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', $this->lang->line('record_not_found'));
            redirect('super_admin/form_groups', 'refresh');
        }
    }

    function editListener($id) {
        $res = $this->form_group_forms_model->getWhere(array('id' => $id));
        if ($res !== FALSE) {
            $obj = new form_group_forms_model();
            $validation_rules = $obj->validationRules();
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('standard_from_id');
                $this->edit($id);
            } else {
                $group_id = $this->input->post('form_group_id');
                $obj->id = $id;
                $obj->form_group_id = $group_id;
                $obj->standard_from_id = $this->input->post('standard_from_id');

                $check_data = $obj->getWhere(array('form_group_id' => $group_id, 'standard_from_id' => $this->input->post('standard_from_id')));
                if (!empty($check_data)) {
                    $this->session->set_flashdata('error', $this->lang->line('already_added_form'));
                } else {
                    $check = $obj->updateData();
                    if ($check == TRUE) {
                        $this->session->set_flashdata('success', $this->lang->line('edit_success'));
                    } else {
                        $this->session->set_flashdata('error', $this->lang->line('edit_error'));
                    }
                }
                redirect('super_admin/standard_group_forms/' . $group_id, 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('record_not_found'));
            redirect('super_admin/form_groups', 'refresh');
        }
    }

    function delete($id) {

        $records = $this->form_group_forms_model->getWhere(array('id' => $id));
        if (!empty($records)) {
            $this->form_group_forms_model->id = $id;
            $check = $this->form_group_forms_model->deleteData();
            if ($check == TRUE) {
                $this->session->set_flashdata('success', $this->lang->line('delete_success'));
            } else {
                $this->session->set_flashdata('error', $this->lang->line('delete_error'));
            }
            redirect('super_admin/standard_group_forms/' . $records[0]->form_group_id, 'refresh');
        } else {
            $this->session->set_flashdata('error', $this->lang->line('record_not_found'));
            redirect('super_admin/form_groups', 'refresh');
        }
    }

}

?>
