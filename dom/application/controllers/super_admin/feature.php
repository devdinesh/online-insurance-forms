<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class feature extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('feature_model');
        $this->load->model('my_file_upload');

        $this->default_template_name = get_super_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
// $random_reference=$this->feature_model->get_rendom_reference();
// var_dump($random_reference->feature_image);
// exit;
        $data['page_name'] = 'super_admin/admin/feature/view_feature';
        $data['title'] = 'feature';
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * a method that renders the json containing list of all the clients
     * that can be understood by the jquery data table
     */
    function getJsonBanner() {
        $records = $this->feature_model->get_all();
        $array = $this->_get_array_for_json($records);

        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    /**
     * takes array objects and converts it to an array of array
     * that is appropriate for rendering in json
     *
     * @param unknown_type $objects            
     * @return multitype:multitype:string NULL
     */
    function _get_array_for_json($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() .
                    'super_admin/feature/edit/' . $value->feature_id . '">' .
                    $value->feature_label . '</a>';
            $file = file_exists(
                    './assets/upload/feature_image/' . $value->feature_image);
            if ($file && $value->feature_image != null)
                $temp_arr[] = '<img src="' . base_url() .
                        'assets/upload/feature_image/' .
                        $value->feature_image .
                        '" class="image-thumb-slider" width="135px;"/>';
            else
                $temp_arr[] = '<img src="' . base_url() .
                        'assets/upload/feature_image/no_image.png" class="image-thumb-slider"/>';
            $temp_arr[] = $value->feature_text;
            $temp_arr[] = '<a href="javascript:;" onclick="deleteRow(' .
                    $value->feature_id .
                    ')" class="deletepage"  data-toggle="tooltip" data-placement="top" title="Delete"><img src="' . assets_url_img . 'delete.png" alt="Delete" title="Delete"></a>';
            $temp_arr[] = 'feature_id_' . $value->feature_id;
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function add($image_error = null) {

        $data['page_name'] = 'super_admin/admin/feature/add_feature';
        $data['title'] = 'Add feature';
        $data['image_error'] = $image_error;
        $this->load->view($this->default_template_name, $data);
    }

    function add_listener() {
        $feature = new feature_model();

        $rules = $feature->validation_rules;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('feature_image');
            $this->form_validation->set_value('feature_text');
            $this->form_validation->set_value('feature_label');
            $this->add();
        } else {
            if ($_FILES['feature_image']['name'] != '') {
                $upload_status = $this->do_upload('feature_image');
                if (isset($upload_status['upload_data'])) {
                    if ($upload_status['upload_data']['file_name'] != '') {
                        $feature->feature_image = str_replace(' ', '_', $upload_status['upload_data']['file_name']);
                    }
                    $check = true;
                } elseif (isset($upload_status['error'])) {
                    $this->form_validation->set_value('feature_image');
                    $this->form_validation->set_value('feature_text');
                    $this->form_validation->set_value('feature_label');
                    $check = $upload_status['error'];
                }
            } else {
                $check = true;
            }
            if ($check === true) {
                $feature->feature_label = $this->input->post('feature_label');
                $feature->feature_text = $this->input->post('feature_text');
                $feature->sequence = $feature->getNewSequenceId();
                $feature->save();
                $this->session->set_flashdata('success', "feature was added successfully");
                redirect(base_url() . 'super_admin/admin/feature', 'refresh');
            } else {
                $this->add($check);
            }
        }
    }

    function edit($id) {
        $data['page_name'] = 'super_admin/admin/feature/edit_feature';
        $data['title'] = 'Edit feature';
        $data['feature_id'] = $id;
        $res = $this->feature_model->get_where(
                array('feature_id' => $id
                ));

        if (empty($res)) {
            $this->session->set_flashdata('error', "Error !! May Be feature not found");
            redirect(base_url() . "super_admin/admin/feature");
        } else {
            $data['feature'] = $res[0];
        }
        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
// get the object and validations rules
        $feature = new feature_model();

        $error = array();

// try uploadng the file
// if file is selected then only the upload will be done
// it would be success if no file was selested
        $logo_upload_status = my_file_upload('feature_image', $feature->logo_upload_config(), $this);

// if file was uploaded
        if ($logo_upload_status['status'] == 0) {
// set the error message for fors in the flash
            $error = array();
            $error['feature_image'] = $logo_upload_status['data'];
            $success = false;
            $this->session->set_flashdata('file_errors', $error);
        } else {
            $success = true;
        }
        $rules = $feature->validation_rules;

// if upload was ok
        if ($success == true) {
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == FALSE) {
                $this->edit($id);
            } else {
                $feature = new $feature();
                $feature = $feature->get_where(
                        array('feature_id' => $id
                        ));

// get the first result as where return multiple things
                $feature = $feature[0];

// remove the previous file if exists
                if (isset($logo_upload_status['data'])) {
                    if (isset($feature->feature_image) &&
                            $feature->feature_image != '') {
                        $path_to_be_removed = substr(
                                $feature->logo_upload_path . "/" .
                                $feature->feature_image, 2);
                        unlink($path_to_be_removed);
                    }
                }

// get the path to new fiel uploded
                if (!isset($logo_upload_status['data'])) {
// if no file was chosen then the path would not be changed
                    $logo_upload_status['data'] = $feature->feature_image;
                }
                $new_file_path = $logo_upload_status['data'];
// get all the fields from form
                $feature->feature_label = $this->input->post(
                        'feature_label');
                $feature->feature_text = $this->input->post(
                        'feature_text');
                $feature->feature_image = $new_file_path; // set the value
// to
// newly
// uploaded file's
// path
// update the
// value to
// database
                $feature->update();

                $this->session->set_flashdata('success', "feature was updated successfully");
                redirect(base_url() . "super_admin/admin/feature");
// print_r($client);
            }
        } else {

// set the values that user entered
            $this->form_validation->set_value('feature_text');
            $this->form_validation->set_value('feature_image');

// / go to edit if it fails
            $this->edit($id);
        }
    }

    function sortable() {
        $feature_array = $_POST['feature_id'];
        $obj = new feature_model();
        $temp = 1;
        $get_feature_info = $obj->get_where(array('feature_id' => $feature_array[0]));
        $get_all_feature = $obj->get_all();
        $org_feature_array = array();
        foreach ($get_all_feature as $feature) {
            $org_feature_array[] = $feature->feature_id;
        }
        for ($i = 0; $i < count($org_feature_array); $i++) {
            if ($org_feature_array[$i] == $feature_array[$i]) {
                continue;
            } else {
                // $res = $this->check_sequence_is_correct($cat_array[$i]);
                $res = 1;
                if ($res === 0) {
                    $temp = 0;
                    break;
                } else {
                    $temp = 1;
                }
            }
        }
        if ($temp == 1) {
            $obj->updateAllSequence($_POST['feature_id']);
            $this->session->set_flashdata('success', 'Sequence of the Feature are changed Sucessufully.');
            return true;
        } else {
            $this->session->set_flashdata('error', 'You can not change the sequence.');
            return false;
        }
    }

    function do_upload($field) {
        $config['upload_path'] = './assets/upload/feature_image';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($field)) {
            $data = array('error' => $this->upload->display_errors()
            );
        } else {
            $data = array('upload_data' => $this->upload->data($field)
            );

// uploading successfull, now do your further actions
            $image = $data['upload_data']['file_name'];
            $this->load->helper('image_manipulation/image_manipulation');
            include_lib_image_manipulation();
            $magicianObj = new imageLib(
                            './assets/upload/feature_image/' . $image);
            $magicianObj->resizeImage(250, 250, 'landscape');
            $magicianObj->saveImage('./assets/upload/feature_image/' . $image, 100);
        }

        return $data;
    }

    function delete($id) {
        $res = $this->feature_model->get_where(
                array('feature_id' => $id
                ));
        $feature = $res[0];
        $path_to_be_removed = substr(
                $feature->logo_upload_path . "/" . $feature->feature_image, 2);
        unlink($path_to_be_removed);
        $feature->delete();
        $this->session->set_flashdata('success', "feature was deleted successfully");
        redirect(base_url() . "super_admin/admin/feature");
    }

}