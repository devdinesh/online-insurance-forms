<?php

//AUTHOR : DINESH GORANIYA

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class rates_user extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('rates_user_model');
        $this->default_template_name = get_super_admin_template_name();
    }

//THIS FUNCTION IS USED TO GET THE MAIN PAGE OF THE USER RATES
    public function index() {
        $data['page_name'] = 'super_admin/rate/user_index';
        $data['title'] = 'User Rates';
        $this->load->view($this->default_template_name, $data);
    }

//THIS FUNCTION IS USED TO GET JSON RECORDS FOR THE USER RATES
    public function get_json() {
        $rate_model = new rates_user_model();
        $rates = $rate_model->getAllrates();
        $main_arr = array();
        foreach ($rates as $rate) {

            $temp = array();
            $temp[] = $rate->from;
            $temp[] = $rate->to;
            //$temp[] = $log->text;
            $temp[] = convert_eng_dutch($rate->rate);
            $temp[] = "<a  href=\"" . base_url() . "super_admin/rates_user/manage/" . $rate->id . "\" id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"><img src='" . assets_url_img . "edit.png' alt='edit rate' title='edit rate'></a>
                <a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $rate->id . "'><img src='" . assets_url_img .
                    "delete.png' alt='Delete' title='Delete'></a>";

            $main_arr[] = $temp;
        }
        $data['aaData'] = $main_arr;
        echo json_encode($data);
    }

//THIS FUNCTION IS USED TO LOAD THE ADD AND EDIT PAGE OF THE USER RATES
    function manage($rate_id = NULL) {
        if (isset($rate_id)) {
            $where = array('id' => $rate_id);
            $result = $this->rates_user_model->getWhere($where);
            $data['rate'] = $result[0];
        }
        $data['page_name'] = 'super_admin/rate/user_manage';
        $data['title'] = 'User Rates';
        $this->load->view($this->default_template_name, $data);
    }

    function save() {
        $this->form_validation->set_rules('from', 'Van', 'trim|numeric|required');
        $this->form_validation->set_rules('to', 'Tot', 'trim|numeric|required');
        $this->form_validation->set_rules('rate', 'Tarief', 'trim|required');
        $data['manage'] = array(
            'from' => $this->input->post('from'),
            'to' => $this->input->post('to'),
            'rate' => convert_dutch_eng($this->input->post('rate'))
        );
        if ($this->form_validation->run() == false) {
            if ($this->input->post("rate_id")) {
                $this->manage($this->input->post("rate_id"));
            } else {
                $this->manage();
            }
        } else {
            if ($this->input->post("rate_id")) {
                //update the data
                $data = $data['manage'];
                $where = array('id' => $this->input->post("rate_id"));
                $this->rates_user_model->update($data, $where);
                $this->session->set_flashdata('success', 'Rate Updated Sucessfully.');
            } else {
                //insert the data
                $data = $data['manage'];
                $this->rates_user_model->save($data);
                $this->session->set_flashdata('success', 'Rate Added Sucessfully.');
            }

            redirect(base_url() . 'super_admin/rates_user', 'refresh');
        }
    }

//THIS FUNCTION IS USED TO DELETE THE SINGLE RECORD OF THE USER RATE
    function delete($id) {
        $this->rates_user_model->delete($id);
        redirect(base_url() . 'super_admin/rates_user', 'refresh');
    }

}