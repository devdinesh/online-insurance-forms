<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class manages the Admin > text Menu.
 */
class admin_texts extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();

        $this->load->model('admin_texts_model');
        $this->load->model('clients_model');
        $this->default_template_name = get_super_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
        $data['page_name'] = 'super_admin/admin/texts/viewall';
        $data['title'] = "Pagina's";

        $this->load->view($this->default_template_name, $data);
    }

    /**
     * this function will edit the text and render the form.
     *
     * @param type $id            
     */
    function edit($id) {
        $data['page_name'] = 'super_admin/admin/texts/edit_texts';
        $data['title'] = "Bewerken Pagina's";
        $data['texts_id'] = $id;
        $data['client_details'] = $this->clients_model->get_all();
        $res = $this->admin_texts_model->selectSingleRecord('text_id', $id);
        if (empty($res)) {
            $this->session->set_flashdata('error', "Error!! Please Try Again. May Be Texts Not Found");
            redirect('super_admin/admin/texts', 'refresh');
        }
        $data['texts_details'] = $res[0];
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * this function will process the data passed by the edit function.
     *
     * @param type $id            
     */
    function editListener($id) {
        $obj = new admin_texts_model();
        $this->form_validation->set_rules($obj->validation_rules);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('text_content');
            $this->edit($id);
        } else {
            $obj->text_id = $id;
            $obj->text_content = $this->input->post('text_content');
            $obj->page_title = $this->input->post('page_title');
            $obj->meta_tags = $this->input->post('meta_tags');
            $obj->meta_desc = $this->input->post('meta_desc');
            if ($this->input->post('is_no_index') == "1") {
                $obj->is_no_index = $this->input->post('is_no_index');
            } else {
                $obj->is_no_index = '0';
            }
            if ($this->input->post('is_no_follow') == "1") {
                $obj->is_no_follow = $this->input->post('is_no_follow');
            } else {
                $obj->is_no_follow = '0';
            }
            if ($this->input->post('include_sitemap') == "1") {
                $obj->include_sitemap = $this->input->post('include_sitemap');
            } else {
                $obj->include_sitemap = '0';
            }
            $obj->update();
            redirect('super_admin/admin/texts', 'refresh');
        }
    }

    /**
     * this function load the datatable in view.
     */
    function getJson() {
        $records = $this->admin_texts_model->getAll();

        $array = $this->getArrayForJson($records);

        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    /**
     * this function will make an array when object is passed to it which is ned
     * by getJson method.
     *
     * @param type $objects            
     * @return type
     */
    function getArrayForJson($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $temp_arr = array();
            $temp_arr[] = '<a href="texts/edit/' . $value->text_id . '">' .
                    $value->text_title . '</a>';
            $arra[] = $temp_arr;
        }
        return $arra;
    }

}

?>
