<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class banner extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('banner_model');
        $this->load->model('my_file_upload');

        $this->default_template_name = get_super_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
        $data['page_name'] = 'super_admin/admin/banner/view_banner';
        $data['title'] = 'banner';
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * a method that renders the json containing list of all the clients
     * that can be understood by the jquery data table
     */
    function getJsonBanner() {
        $records = $this->banner_model->get_all();

        $array = $this->_get_array_for_json($records);

        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    /**
     * takes array objects and converts it to an array of array
     * that is appropriate for rendering in json
     *
     * @param unknown_type $objects            
     * @return multitype:multitype:string NULL
     */
    function _get_array_for_json($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() . 'super_admin/banner/edit/' .
                    $value->banner_id . '">' . $value->banner_sequence . '</a>';
            $temp_arr[] = '<a href="' . base_url() . 'super_admin/banner/edit/' .
                    $value->banner_id . '">' . $value->banner_name . '</a>';
            $file = file_exists(
                    './assets/upload/banner_image/' . $value->banner_image);
            if ($file && $value->banner_image != null)
                $temp_arr[] = '<img src="' . base_url() .
                        'assets/upload/banner_image/' . $value->banner_image .
                        '" class="image-thumb-slider"/>';
            else
                $temp_arr[] = '<img src="' . base_url() .
                        'assets/upload/banner_image/no_image.png" class="image-thumb-slider"/>';

            $temp_arr[] = '<a href="javascript:;" onclick="deleteRow(' .
                    $value->banner_id .
                    ')" class="deletepage"  data-toggle="tooltip" data-placement="top" title="Delete"><img src="' . assets_url_img . 'delete.png" alt="Delete" title="Delete"></a>';
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function add() {
        $data['page_name'] = 'super_admin/admin/banner/add_banner';
        $data['title'] = 'Add Banner';
        $this->load->view($this->default_template_name, $data);
    }

    function add_listener() {
        $banner = new banner_model();
        $rules = $banner->validation_rules;

        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            // set the values that user entered
            $this->form_validation->set_value('banner_name');
            $this->form_validation->set_value('banner_url');
            $this->form_validation->set_value('banner_sequence');
            $this->add();
        } else {
            $upload_status = $this->do_upload('banner_image');
            if (isset($upload_status['upload_data'])) {
                if ($upload_status['upload_data']['file_name'] != '') {
                    $banner->banner_name = $this->input->post('banner_name');
                    $banner->banner_url = $this->input->post('banner_url');
                    $banner->banner_sequence = $this->input->post(
                            'banner_sequence');
                    $banner->banner_image = str_replace(' ', '_', $upload_status['upload_data']['file_name']);
                    $new_id = $banner->save();
                    $this->session->set_flashdata('success', "Banner was added successfully");
                }
                redirect(base_url() . "super_admin/admin/banner");
            } else if (isset($upload_status['error'])) {
                $this->session->set_flashdata('file_errors', $upload_status['error']);
                redirect(base_url() . 'super_admin/admin/banner', 'refresh');
            }
        }
    }

    function edit($id) {
        $data['page_name'] = 'super_admin/admin/banner/edit_banner';
        $data['title'] = 'Edit Banner';
        $data['banner_id'] = $id;
        $res = $this->banner_model->get_where(
                array('banner_id' => $id
                ));

        if (empty($res)) {
            $this->session->set_flashdata('error', "Error !! May Be form not found");
            redirect(base_url() . "super_admin/admin/banner");
        } else {
            $data['banner'] = $res[0];
        }
        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
        // get the object and validations rules
        $banner = new banner_model();

        $error = array();

        // try uploadng the file
        // if file is selected then only the upload will be done
        // it would be success if no file was selested
        $logo_upload_status = my_file_upload('banner_image', $banner->logo_upload_config(), $this);

        // if file was uploaded
        if ($logo_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $error = array();
            $error['banner_image'] = $logo_upload_status['data'];
            $success = false;
            $this->session->set_flashdata('file_errors', $error);
        } else {
            $success = true;
        }
        $rules = $banner->validation_rules;
        unset($rules[3]);
        // if upload was ok
        if ($success == true) {
            $this->form_validation->set_rules($rules);
            $sequence = $id . ',banner_id,banner_sequence,banner_model';
            $this->form_validation->set_rules('banner_sequence', 'Banner sequence', 'trim|required|edit_isDataExitSingTable_validator[' .
                    $sequence . ']');
            if ($this->form_validation->run() == FALSE) {
                $this->edit($id);
            } else {
                $banner = new banner_model();
                $banner = $banner->get_where(
                        array('banner_id' => $id
                        ));

                // get the first result as where return multiple things
                $banner = $banner[0];

                // remove the previous file if exists
                if (isset($logo_upload_status['data'])) {
                    if (isset($banner->banner_image) &&
                            $banner->banner_image != '') {
                        $path_to_be_removed = substr(
                                $banner->logo_upload_path . "/" .
                                $banner->banner_image, 2);
                        unlink($path_to_be_removed);
                    }
                }

                // get the path to new fiel uploded
                if (!isset($logo_upload_status['data'])) {
                    // if no file was chosen then the path would not be changed
                    $logo_upload_status['data'] = $banner->banner_image;
                }
                $new_file_path = $logo_upload_status['data'];
                // get all the fields from form
                $banner->banner_name = $this->input->post('banner_name');
                $banner->banner_url = $this->input->post('banner_url');
                $banner->banner_sequence = $this->input->post('banner_sequence');

                $banner->banner_image = $new_file_path; // set the value to
                // newly
                // uploaded file's path
                // update the value to
                // database
                $banner->update();

                $this->session->set_flashdata('success', "Banner was updated successfully");
                redirect(base_url() . "super_admin/admin/banner");
                // print_r($client);
            }
        } else {

            // set the values that user entered
            $this->form_validation->set_value('banner_name');
            $this->form_validation->set_value('banner_url');
            $this->form_validation->set_value('banner_sequence');
            // / go to edit if it fails
            $this->edit($id);
        }
    }

    function do_upload($field) {
        list($width, $height, $type, $attr) = getimagesize($_FILES[$field]['tmp_name']);
        $config['upload_path'] = './assets/upload/banner_image';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($field)) {
            $data = array('error' => $this->upload->display_errors()
            );
        } else {
            $data = array('upload_data' => $this->upload->data($field)
            );

            // uploading successfull, now do your further actions
            $image = $data['upload_data']['file_name'];

            $this->load->helper('image_manipulation/image_manipulation');
            include_lib_image_manipulation();
            $magicianObj = new imageLib('./assets/upload/banner_image/' . $image);
            $magicianObj->resizeImage($width, $height, 'auto');
            $magicianObj->saveImage('./assets/upload/banner_image/' . $image, 100);

            $config['image_library'] = 'GD2';
            $config['source_image'] = './assets/upload/banner_image/' . $image;
            $config['new_image'] = './assets/upload/banner_image/';
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = false;
            $config['master_dim'] = 'width';
            $config['width'] = $width;
            $config['height'] = $height;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->image_lib->clear();
        }

        return $data;
    }

    function delete($id) {
        $res = $this->banner_model->get_where(
                array('banner_id' => $id
                ));
        $banner = $res[0];
        $path_to_be_removed = substr(
                $banner->logo_upload_path . "/" . $banner->banner_image, 2);
        unlink($path_to_be_removed);
        $banner->delete();
        $this->session->set_flashdata('success', "banner was deleted successfully");
        redirect(base_url() . "super_admin/admin/banner");
    }

}