<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class contact_form extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->library('image_lib');
        $this->load->model('contact_form_model');
        $this->default_template_name = get_super_admin_template_name();
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *        START COMMON FUNCTION WHICH CAN BE USED IN ANY FORM SHOW         * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * this will render the super admin login form
     */
    function index() {
        $data['page_name'] = 'super_admin/forms/contact_form/view_form';
        $data['title'] = 'Contact Forms';
        $this->load->view($this->default_template_name, $data);
    }

    public function getJson() {
        ini_set('memory_limit', '512M');
        $this->load->library('datatable');
        $this->datatable->aColumns = array('c.id', 'c.name', 'c.company_name', 'c.email_address', 'c.phone_number');
        $this->datatable->eColumns = array('c.id');
        $this->datatable->sIndexColumn = "c.id";
        $this->datatable->sTable = " ins_contact_form c";

        $this->datatable->datatable_process();
        foreach ($this->datatable->rResult->result_array() as $aRow) {
            $temp_arr = array();
            $temp_arr[] = $aRow['name'];
            $temp_arr[] = $aRow['company_name'] != '' ? $aRow['company_name'] : null;
            $temp_arr[] = $aRow['email_address'];
            $temp_arr[] = $aRow['phone_number'] != '' ? $aRow['phone_number'] : null;
            $temp_arr[] = '<a href="javascript:;" onclick="deleteRow(' .
                    $aRow['id'] .
                    ')" class="deletepage"  data-toggle="tooltip" data-placement="top" title="Delete"><img src="' . assets_url_img . 'delete.png" alt="Delete" title="Delete"></a>';

            $this->datatable->output['aaData'][] = $temp_arr;
        }
        echo json_encode($this->datatable->output);
        exit();
    }

    function delete($id) {
        $res = $this->contact_form_model->get_where(array('id' => $id));
        $contact = $res[0];
        $contact->delete();
        $this->session->set_flashdata('success', "Record was deleted successfully");
        redirect(base_url() . "super_admin/contact_form");
    }

}
