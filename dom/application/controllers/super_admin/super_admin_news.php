<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class deals with nwez
 */
class super_admin_news extends CI_Controller
{

    /**
     *
     * @var String template name whis is rendered.
     */
    var $default_template_name;

    /**
     * Default constructor loads the models and set ups template name.
     */
    function __construct ()
    {
        parent::__construct();
        $this->load->model('super_admin_news_model');
        $this->default_template_name = get_super_admin_template_name();
    
    }

    /**
     * renders page for lsit all newz that belongs to the
     * given client
     */
    public function index ()
    {
        $data['page_name'] = 'super_admin/admin/news/index';
        $data['title'] = 'Nieuws';
        
        $this->load->view($this->default_template_name, $data);
    
    }
    
    /*
     * renders json for the list newz page
     */
    public function get_json ()
    {
        $super_admin_news_model = new super_admin_news_model();
        $newzs = $super_admin_news_model->get_all();
        $main_arr = array();
        foreach ($newzs as $newz)
        {
            $temp = array();
            $temp[] = anchor(
                    base_url() . 'super_admin/super_admin_news/edit/' .
                             $newz->newz_id, $newz->title);
            $temp[] = $newz->publication_date;
            $temp[] = $newz->expire_date;
            $temp[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                     $newz->newz_id . "'><img src='" . assets_url_img .
                     "delete.png' alt='Delete' title='Delete'></a>";
            $main_arr[] = $temp;
        }
        $data['aaData'] = $main_arr;
        echo json_encode($data);
    
    }

    public function add ()
    {
        $data['page_name'] = 'super_admin/admin/news/add';
        $data['title'] = 'Add News';
        $this->load->view($this->default_template_name, $data);
    
    }

    public function add_listener ()
    {
        $super_admin_news_model = new super_admin_news_model();
        $rules = $super_admin_news_model->validation_rules;
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->add();
        }
        else
        {
            $super_admin_news_model->date = $this->input->post('date');
            $super_admin_news_model->title = $this->input->post('title');
            $super_admin_news_model->newz_text = $this->input->post('newz_text');
            $super_admin_news_model->publication_date = $this->input->post(
                    'publication_date');
            $super_admin_news_model->expire_date = $this->input->post(
                    'expire_date');
            $super_admin_news_model->save();
            $this->session->set_flashdata('success', 
                    "Nieuwsbericht is toegevoegd");
            redirect(base_url('super_admin/admin/news'));
        }
    
    }

    public function edit ( $newz_id )
    {
        $super_admin_news_model = new super_admin_news_model();
        $newzs = $super_admin_news_model->get_where(
                array('newz_id' => $newz_id
                ));
        
        if (count($newzs) == 0)
        {
            $this->session->set_flashdata('error', "No such news exists");
            redirect(base_url('super_admin/admin/news'));
        }
        
        $news = $newzs[0];
        $data['page_name'] = 'super_admin/admin/news/edit';
        $data['title'] = 'Edit News';
        $data['news'] = $news;
        
        $this->load->view($this->default_template_name, $data);
    
    }

    public function edit_listener ( $newz_id )
    {
        $super_admin_news_model = new super_admin_news_model();
        $newzs = $super_admin_news_model->get_where(
                array('newz_id' => $newz_id
                ));
        
        if (count($newzs) == 0)
        {
            $this->session->set_flashdata('error', "No such news exists");
            redirect(base_url('super_admin/admin/news'));
        }
        
        $news = $newzs[0];
        $rules = $super_admin_news_model->validation_rules;
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->edit($newz_id);
        }
        else
        {
            $news->date = $this->input->post('date');
            $news->title = $this->input->post('title');
            $news->newz_text = $this->input->post('newz_text');
            $news->publication_date = $this->input->post('publication_date');
            $news->expire_date = $this->input->post('expire_date');
            $news->update();
            $this->session->set_flashdata('success', "Newz Edited successfully");
            redirect(base_url('super_admin/admin/news'));
        }
    
    }

    function delete ( $id )
    {
        $this->super_admin_news_model->newz_id = $id;
        $this->super_admin_news_model->delete();
        $this->session->set_flashdata('success', "news deleted sucessfully");
        redirect('super_admin/admin/news', 'refresh');
    
    }

}