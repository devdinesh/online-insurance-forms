<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class admin_subscription_kinds extends CI_Controller
{

    var $default_template_name;

    function __construct ()
    {
        parent::__construct();
        
        $this->load->model('admin_subscription_kinds_model');
        $this->default_template_name = get_super_admin_template_name();
    
    }

    /**
     * this will render the view of subscrption plans
     */
    public function index ()
    { 
        $data['page_name'] = 'super_admin/admin/subscription_kinds/view_all';
        $data['title'] = 'Subscription Kinds';
        
        $this->load->view($this->default_template_name, $data);
    
    }

    /**
     * this will render the add form of plans
     */
    function add ( $plan_image_error = '' )
    {
        $data['page_name'] = 'super_admin/admin/subscription_kinds/add_subscription';
        $data['title'] = 'Add Subscription Plan';
        $data['plan_image_error'] = $plan_image_error;
        $this->load->view($this->default_template_name, $data);
    
    }

    /**
     * when user will post the data this will handel all data
     */
    function addListener ()
    {
        $plan_image_error = FALSE;
        
        $obj = new admin_subscription_kinds_model();
        $this->form_validation->set_rules($obj->validation_rules);
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->form_validation->set_value('subscription_title');
            $this->form_validation->set_value('maximum_forms');
            $this->form_validation->set_value('subscription_fee');
            $this->form_validation->set_value('rate_per_extra_form');
            $this->form_validation->set_value('help_text');
            $this->form_validation->set_value('plan_image');
            $this->add();
        }
        else
        {
            $is_file = empty($_FILES['plan_image']['name']);
            
            $ans = $obj->upload_file('plan_image');
            if ($ans == TRUE)
            {
                $obj->plan_image = $this->upload->file_name;
            }
            
            if ($is_file == FALSE and $ans == FALSE)
            {
                $this->form_validation->set_value('subscription_title');
                $this->form_validation->set_value('maximum_forms');
                $this->form_validation->set_value('subscription_fee');
                $this->form_validation->set_value('rate_per_extra_form');
                $this->form_validation->set_value('help_text');
                $this->form_validation->set_value('plan_image');
                $plan_image_error = TRUE;
                $this->add($this->upload->display_errors());
            }
        }
        if ($plan_image_error == FALSE && $this->form_validation->run() == TRUE)
        {
            
            $obj->subscription_title = $this->input->post('subscription_title');
            $obj->maximum_forms = $this->input->post('maximum_forms');
            $obj->subscription_fee = $this->input->post('subscription_fee');
            $obj->rate_per_extra_form = $this->input->post(
                    'rate_per_extra_form');
            $obj->help_text = $this->input->post('help_text');
            
            if ($this->input->post('status') != null)
            {
                $obj->status = 'A';
            }
            else
            {
                $obj->status = 'D';
            }
            
            $obj->dataUpdateSave();
            redirect('super_admin/admin/subscription', 'refresh');
        }
    
    }

    /**
     * this will render the edit plan form with old data whose id is passed
     *
     * @param type $id
     *            : which id data we want to edit
     */
    function edit ( $id, $plan_image_error = '' )
    {
        $data['page_name'] = 'super_admin/admin/subscription_kinds/edit_subscription';
        $data['title'] = 'Edit Subscription Plan';
        $data['subscription_id'] = $id;
        $data['plan_image_error'] = $plan_image_error;
        $res = $this->admin_subscription_kinds_model->selectSingleRecord(
                'subscription_kinds_id', $id);
        if (empty($res))
        {
            $this->session->set_flashdata('error', 
                    "Error !! Please Try Again.May Be Subscription Not Found.");
            redirect('super_admin/admin/subscription', 'refresh');
        }
        $data['subscription'] = $res[0];
        $this->load->view($this->default_template_name, $data);
    
    }

    /**
     * when user update the data this will handel all the data posted by edit
     * form
     *
     * @param type $id
     *            : which id data we want to update
     */
    function editListener ( $id )
    {
        $plan_image_error = FALSE;
        
        $obj = new admin_subscription_kinds_model();
        $this->form_validation->set_rules($obj->validation_rules);
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->form_validation->set_value('subscription_title');
            $this->form_validation->set_value('maximum_forms');
            $this->form_validation->set_value('subscription_fee');
            $this->form_validation->set_value('rate_per_extra_form');
            $this->form_validation->set_value('help_text');
            $this->form_validation->set_value('plan_image');
            $this->edit($id);
        }
        else
        {
            $is_file = empty($_FILES['plan_image']['name']);
            
            if ($_FILES['plan_image']['name'] != '')
            {
                $this->removeImageById($id);
                $ans = $obj->upload_file('plan_image');
                if ($ans == TRUE)
                {
                    $obj->plan_image = $this->upload->file_name;
                }
            }
            else
            {
                $res = $this->admin_subscription_kinds_model->selectSingleRecord(
                        'subscription_kinds_id', $id);
                $obj->plan_image = $res[0]->plan_image;
            }
            
            if ($is_file == FALSE and $ans == FALSE)
            {
                $this->form_validation->set_value('subscription_title');
                $this->form_validation->set_value('maximum_forms');
                $this->form_validation->set_value('subscription_fee');
                $this->form_validation->set_value('rate_per_extra_form');
                $this->form_validation->set_value('help_text');
                $this->form_validation->set_value('plan_image');
                $plan_image_error = TRUE;
                $this->edit($id, $this->upload->display_errors());
            }
        }
        if ($plan_image_error == FALSE && $this->form_validation->run() == TRUE)
        {
            $obj->subscription_kinds_id = $id;
            $obj->subscription_title = $this->input->post('subscription_title');
            $obj->maximum_forms = $this->input->post('maximum_forms');
            $obj->subscription_fee = $this->input->post('subscription_fee');
            $obj->rate_per_extra_form = $this->input->post(
                    'rate_per_extra_form');
            $obj->help_text = $this->input->post('help_text');
            
            if ($this->input->post('status') != null)
            {
                $obj->status = 'A';
            }
            else
            {
                $obj->status = 'D';
            }
            
            $obj->dataUpdateSave();
            redirect('super_admin/admin/subscription', 'refresh');
        }
    
    }

    /**
     * this function will removw the image whose id is passed.
     * basically it will call the another function which actually delete the
     * image and update the table
     *
     * @param type $id            
     */
    function removeImage ( $id )
    {
        if ($this->removeImageById($id))
        {
            $url = base_url('super_admin/admin/subscription/edit/' . $id);
            redirect($url);
        }
    
    }

    /**
     * This actually delete the image and update the table and remove from
     * folder.
     *
     * @param type $id            
     * @return boolean
     */
    function removeImageById ( $id )
    {
        $return = FALSE;
        $obj = new admin_subscription_kinds_model();
        $obj = $obj->selectSingleRecord('subscription_kinds_id', $id);
        
        if (isset($obj[0]->plan_image) && $obj[0]->plan_image != 'NULL')
        {
            unlink('assets/plan_images/' . $obj[0]->plan_image);
            $obj[0]->plan_image = null;
            $obj[0]->dataUpdateSave();
            $return = TRUE;
        }
        
        return $return;
    
    }

    /**
     * this will delete the data whose id is passed
     *
     * @param type $id            
     */
    function deleteListener ( $id )
    {
        $this->admin_subscription_kinds_model->deleteData($id);
        redirect('super_admin/admin/subscription', 'refresh');
    
    }

    /**
     * this function load the datatable in view.
     */
    function getJson ()
    {
        $records = $this->admin_subscription_kinds_model->get_all();
        
        $array = $this->get_array_for_json($records);
        
        $data['aaData'] = $array;
        if (is_array($data))
        {
            echo json_encode($data);
        }
    
    }

    /**
     * on passing the objects it will make an array which will be used by the
     * getJson method.
     *
     * @param type $objects            
     * @return string
     */
    function get_array_for_json ( $objects )
    {
        $arra = array();
        foreach ($objects as $value)
        {
            $temp_arr = array();
            $temp_arr[] = '<a href="subscription/edit/' .
                     $value->subscription_kinds_id . '">' .
                     $value->subscription_title . '</a>';
            $temp_arr[] = $value->maximum_forms;
            $temp_arr[] = $value->subscription_fee;
            $temp_arr[] = $value->rate_per_extra_form;
            
            if ($value->status == 'A')
            {
                $temp_arr[] = '<span class="label label-success">Active</span>';
            }
            elseif ($value->status == 'D')
            {
                $temp_arr[] = '<span class="label label-important">Deactive</span>';
            }
            
            $check = $this->admin_subscription_kinds_model->isPlanSubscribed(
                    $value->subscription_kinds_id);
            if ($check === FALSE)
            {
                $temp_arr[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                         $value->subscription_kinds_id .
                         "'><span class=\"label label-warning\">Delete</span></a>";
            }
            else
            {
                $temp_arr[] = "<span class=\"label\">Delete</span>";
            }
            
            $arra[] = $temp_arr;
        }
        return $arra;
    
    }

}

?>
