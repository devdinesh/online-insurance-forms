<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class dashboard extends CI_Controller
{

    var $default_template_name;

    function __construct ()
    {
        parent::__construct();
        
        $this->default_template_name = get_mat_super_admin_template_name();
    
    }

    public function index ()
    {
        $data['page_name'] = 'super_admin/dashboard';
        $data['title'] = 'Admin Dashboard';
        
        $this->load->view($this->default_template_name, $data);
    
    }

}
?>
