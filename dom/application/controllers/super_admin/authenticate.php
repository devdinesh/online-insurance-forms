<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class authenticate extends CI_Controller {

    /**
     * this is the default constructor
     */
    function __construct() {
        parent::__construct();

        $this->load->model('super_admin_authenticate');
    }

    /**
     * this will render the super admin login form
     */
    public function index($msg = '') {

        // redirect to hashbard if already logged in
        if (is_logged_in()) {
            $type = $this->session->userdata('super_admin');
            if ($type) {
                redirect(base_url() . "super_admin/dashboard");
            }
        }
        $redirect_to = 'super_admin';
        if (isset($_GET['redirect_to'])) {
            $redirect_to = $_GET['redirect_to'];
        }

        // render the page otherwise
        $data['title'] = 'Super Admin';
        $data['login_error'] = $msg;
        $data['redirect_to'] = $redirect_to;
        $this->load->view('super_admin/login', $data);
    }

    /**
     * this will process the data passed by login form
     */
    function checkLogin() {
        $authenticate_model = new super_admin_authenticate();
        $this->form_validation->set_rules($authenticate_model->validation_rules);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('super_admin_email');
            $this->form_validation->set_value('super_admin_password');
            $this->index();
        } else {
            $this->session->unset_userdata('admin');
            $this->session->unset_userdata('super_admin');
            $this->session->unset_userdata('policy_holder');


            // get the fields posted by the user
            $email = $this->input->post('super_admin_email');
            $password = $this->input->post('super_admin_password');

            // check the login status
            $login_status = $authenticate_model->check_login($email, $password);

            // login should be succesesfult as the username/email and password
            // matche
            if ($login_status === true) {
                $email = $this->super_admin_authenticate->get_by_email_id(
                        $email);

                $this->set_session_for_user_by_email($email->super_admin_email);
                $redirect_to = 'super_admin/dashboard';
                if (isset($_GET['redirect_to'])) {
                    $redirect_to = $_GET['redirect_to'];
                }
                // echo "redirecting to " . base_url() .$redirect_to;
                redirect(base_url() . $redirect_to);
            } else {
                // render the same form again
                $this->form_validation->set_value('super_admin_email');
                $msg = $login_status;
                $this->index($msg);
            }
        }
    }

    /**
     * this will render the forgot password form
     */
    function forgotPassword($msg = '') {
        $data['title'] = 'Forgot Password';
        $data['login_error'] = $msg;
        $this->load->view('super_admin/forgot_password', $data);
    }

    /**
     * this will process the data passed by forgot password form
     */
    function forgotpassword_listener() {
        $authenticate_model = new super_admin_authenticate();
        $this->form_validation->set_rules(
                $authenticate_model->validation_rules[0]);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('super_admin_email');
            $this->forgotPassword();
        } else {
            $randid = random_string('alnum', 30);
            $check = $this->super_admin_authenticate->checkMail($randid);

            if ($check === 1) {
                $this->forgotPassword('Mail Address Not Found');
            } else if ($check === 2) {
                $this->forgotPassword('Mail Address Not Found');
            } else {

                // $this->load->model('email_template');
                $this->load->helper('sending_mail');

                $bussiness_email_adress = $this->input->post(
                        'super_admin_email');

                // reset link on which user click and reset the password
                $reset_link = anchor(base_url() . "super_admin/Reset/$randid", 'Click Here');

                $mail_subject = 'Forgot Password';
                $mail_message = $reset_link;

                $check = send_mail($bussiness_email_adress, $mail_subject, $mail_message);

                if ($check == TRUE) {
                    $this->forgotPassword(
                            'Please Check Mail Address To Reset Password');
                } else {
                    $this->forgotPassword('Sorry Mail Not Sent Try Again');
                }
            }
        }
    }

    /**
     * this will render the reset password form with random id (randid) check
     * whose password is to reset.
     * if any unknow person comes and put any email id so for security purpose a
     * rand id will de taken which is updated at time send mail
     * to super admin.
     *
     * @param type $randid
     *            : alpha-numeric string.
     */
    function resetPassword($randid) {
        $data['title'] = "Reset Password";
        $data['randid'] = $randid;
        $this->load->view('super_admin/reset_password', $data);
    }

    /**
     * this will process the reset password form and the data send by end user
     * i.e.
     * super_admin
     *
     * @param type $randid
     *            : alpha-numeric string
     */
    function resetPasswordListener($randid) {
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[4]|matches[new_password]');

        if ($this->form_validation->run() == FALSE) {
            $this->resetPassword($randid);
        } else {
            if ($this->super_admin_authenticate->selectSingleRecord(
                            'super_admin_password', $randid) !== FALSE) {
                $check = $this->super_admin_authenticate->resetNewPassword(
                        $randid);
                if ($check == True) {
                    $this->index("Login with new Password");
                }
            } else {
                $this->index("Reset link is changed Somehow");
            }
        }
    }

    function logout() {
        $type = $this->session->userdata('admin');

        // if admin is not logged in
        if (!$type) {
            $this->session->sess_destroy();
        } else {
            $this->session->unset_userdata('super_admin');
        }

        redirect(base_url() . "super_admin");
    }

    function set_session_for_user_by_email($email) {
        // set the session and redirect the user
        $super_admin = $this->super_admin_authenticate->get_by_email_id($email);
        $super_admin_arr = $super_admin->to_array();
        $session = array('super_admin' => $super_admin_arr,
            'user_type' => 'super_admin', 'logged_in' => true
        );
        $this->session->set_userdata($session);
    }

}

/* End of file authenticate.php */
/* Location: ./application/controllers/authenticate.php */
