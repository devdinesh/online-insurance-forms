<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class form_groups extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('ins_form_groups_model');
        $this->load->model('form_group_forms_model');
        $this->load->model('standard_forms_model');
        $this->load->model('clients_model');
        $this->load->model('forms_model');
        $this->load->model('forms_categories_model');
        $this->load->model('forms_categories_question_model');
        $this->load->model('ins_form_groups_model');
        $this->default_template_name = get_mat_super_admin_template_name();
        language_change::setLanguage();
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
        $data['page_name'] = 'super_admin/form_group/view_form_group';
        $data['title'] = $this->lang->line('form_groups');
        $this->load->view($this->default_template_name, $data);
    }

    function getJson() {
        $records = $this->ins_form_groups_model->getAll();

        if ($records !== FALSE) {
            $array = $this->get_array_for_json($records);
        } else {
            $array = array();
        }
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function get_array_for_json($objects) {
        $arra = array();

        foreach ($objects as $value) {
            $group_info = $this->form_group_forms_model->getWhere(array('form_group_id' => $value->form_group_id));
            $records = $this->form_group_forms_model->getWhere(array('form_group_id' => $value->form_group_id));
            $form_name = '';
            $i = 0;
            foreach ($group_info as $group) {
                $standarad_form_info = $this->standard_forms_model->selectSingleRecord('form_id', $group->standard_from_id);
                if ($i != 0) {
                    $form_name.=' , ' . @$standarad_form_info[0]->form_name;
                } else {
                    $form_name.=@$standarad_form_info[0]->form_name;
                }
                $i++;
            }
            $temp_arr = array();
            $temp_str = "<div class = \"btn-group pull-right\">";
            $temp_arr[] = '<a href="form_groups/edit/' . $value->form_group_id . '">' . $value->form_group_name . '</a>';
            $temp_arr[] = $form_name;
            $temp_arr[] = '<a href="' . base_url() . 'super_admin/standard_group_forms/' . $value->form_group_id . '">' . $this->lang->line('forms') . '</a>';
            if (!empty($records)) {
                $temp_str = "<a href='javascript:;' onclick='openModal(" . $value->form_group_id . ")'  title=" . $this->lang->line('copy') . "><i class='glyphicon glyphicon-random link-button-with-icon'></i></a>";
            } else {
                $temp_str = "&nbsp;";
            }
            $temp_str.= "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $value->form_group_id . "' id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=" . $this->lang->line('remove_form') . "><i class='md md-delete'></i></a></div>";
            $temp_arr[] = $temp_str;
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function add() {
        $data['page_name'] = 'super_admin/form_group/add_form_group';
        $data['title'] = $this->lang->line('add_from_group');
        $this->load->view($this->default_template_name, $data);
    }

    function addListener() {
        $obj = new ins_form_groups_model();
        $validation_rules = $obj->validationRules();
        $this->form_validation->set_rules($validation_rules);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('form_group_name');
            $this->add();
        } else {

            $obj->form_group_name = $this->input->post('form_group_name');
            $obj->created_date = get_current_time()->get_date_for_db();

            $check = $obj->insertData();
            if ($check == TRUE) {
                $this->session->set_flashdata('success', $this->lang->line('add_success'));
            } else {
                $this->session->set_flashdata('error', $this->lang->line('add_error'));
            }
            redirect('super_admin/form_groups', 'refresh');
        }
    }

    function edit($id) {
        $res = $this->ins_form_groups_model->getWhere(array('form_group_id' => $id));
        if ($res !== FALSE) {
            $data['page_name'] = 'super_admin/form_group/edit_form_group';
            $data['title'] = $this->lang->line('edit_from_group');
            $data['group_info'] = $res[0];
            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', $this->lang->line('group_not_found'));
            redirect('super_admin/form_groups', 'refresh');
        }
    }

    function editListener($id) {
        $obj = new ins_form_groups_model();
        $res = $obj->getWhere(array('form_group_id' => $id));
        if ($res !== FALSE) {

            $validation_rules = $obj->validationRules();
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('form_group_name');
                $this->edit($id);
            } else {
                $obj->form_group_id = $id;
                $obj->form_group_name = $this->input->post('form_group_name');
                $obj->updated_date = get_current_time()->get_date_for_db();
                $check = $obj->updateData();
                if ($check == TRUE) {
                    $this->session->set_flashdata('success', $this->lang->line('edit_success'));
                } else {
                    $this->session->set_flashdata('error', $this->lang->line('edit_error'));
                }
                redirect('super_admin/form_groups', 'refresh');
            }
        } else {
            $this->session->set_flashdata('error',$this->lang->line('group_not_found'));
            redirect('super_admin/form_groups', 'refresh');
        }
    }

    function delete($form_group_id) {
        $obj = new ins_form_groups_model();
        $obj->form_group_id = $form_group_id;
        $check = $obj->deleteData();
        if ($check == TRUE) {
            $this->session->set_flashdata('success', $this->lang->line('delete_success'));
        } else {
            $this->session->set_flashdata('error', $this->lang->line('delete_error'));
        }
        redirect('super_admin/form_groups', 'refresh');
    }

    function view_copy_form($group_id) {
        $data['client_info'] = $this->clients_model->get_all();
        $data['group_id'] = $group_id;
        $data['title'] = $this->lang->line('form_groups');
        $this->load->view('super_admin/form_group/copy_form', $data);
    }

    function copy_all_form($group_id) {
        $client_id = $this->input->post('client_id');
        $last_nserted_id = '';
        $get_group_forms = $this->form_group_forms_model->getWhere(array('form_group_id' => $group_id));
//        var_dump($get_group_forms);
//                exit;
        if (!empty($get_group_forms)) {
            foreach ($get_group_forms as $standar_form) {
                $obj = new standard_forms_model();

                $res = $this->standard_forms_model->selectSingleRecord('form_id', $standar_form->standard_from_id);

                if ($res !== FALSE && !empty($res)) {
                    $form_obj = new forms_model();
                    $form_exist = $form_obj->is_form_exist($res[0]->form_name, $client_id);
                    if ($form_exist == 0) {
                        $form_obj->form_name = $res[0]->form_name;
                        $form_obj->form_tag = $res[0]->form_tag;
                    } else {
                        $form_obj->form_name = $res[0]->form_name . '_' . ($form_exist );
                        $form_obj->form_tag = $res[0]->form_tag . '_' . ($form_exist );
                    }
                    $rand_code = random_string('alpha', 5);
                    // save the new form
                    $form_obj->client_id = $client_id;
                    $form_obj->form_code = $rand_code;
                    $form_obj->fill_in_needed = $res[0]->fill_in_needed;
                    $form_obj->header_text = $res[0]->header_text;
                    $form_obj->introduction_text = $res[0]->introduction_text;
                    $form_obj->show_all_question_at_once = $res[0]->show_all_question_at_once;
                    $form_obj->closure_text = $res[0]->closure_text;
                    $form_obj->current_date = get_current_time()->get_date_for_db();
                    $last_nserted_id = $form_obj->dataUpdateSave();
                    $this->load->model('standard_forms_categories_model');
                    $this->load->model('standard_forms_categories_question_model');

                    // get the categories that belog to the origina l form
                    $categories = $this->standard_forms_categories_model->selectMoreRecord('form_id', $standar_form->standard_from_id);
                    if (!empty($categories)) {
                        foreach ($categories as $category) {
                            $old_cat_id = $category->cat_id;
                            $category_obj = new forms_categories_model();
                            $category_obj->form_id = $last_nserted_id;
                            $category_obj->cat_name = $category->cat_name;
                            $category_obj->sequence = $category->sequence;
                            $category_obj->introduction_text = $category->introduction_text;
                            $category_obj->dataUpdateSave();
                            // insert a new record for category
                            $last_inserted_category_id = $this->db->insert_id();
                            // get the questions
                            $questions = $this->standard_forms_categories_question_model->get_where(array('cat_id' => $old_cat_id));

                            // iterate thru the questions and add them as well

                            if (count($questions) > 0) { // if there were answers
                                foreach ($questions as $question) {
                                    $old_question_id = $question->question_id;
                                    $question_obj = new forms_categories_question_model();

                                    $question_obj->cat_id = $last_inserted_category_id;
                                    $question_obj->question = $question->question;
                                    $question_obj->help_text = $question->help_text;
                                    $question_obj->sequence = $question->sequence;
                                    $question_obj->answer_kind = $question->answer_kind;
                                    $question_obj->required = $question->required;
                                    $question_obj->dataUpdateSave();
                                    $last_inserted_question_id = $this->db->insert_id();

                                    /* get all the answers for original question */
                                    $answers = $this->standard_forms_categories_question_model->getAnswers($old_question_id);

                                    // if there wre answers
                                    if (is_array($answers) && count($answers) > 0) {

                                        // get an array containing answers
                                        $new_answers = array();
                                        foreach ($answers as $answer) {
                                            $new_answers[] = $answer->answer;
                                        }
                                        // add the answers to the newly inserted
                                        // question
                                        $question_obj->saveAnswers($last_inserted_question_id, $new_answers);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if ($last_nserted_id != '') {
                $this->session->set_flashdata('success', $this->lang->line('copy_sucess'));
            } else {
                $this->session->set_flashdata('error', $this->lang->line('copy_error'));
            }
            redirect('super_admin/form_groups', 'refresh');
        } else {
            $this->session->set_flashdata('error', $this->lang->line('error_no_form_group'));
            redirect('super_admin/form_groups', 'refresh');
        }
    }

}

?>
