<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class deals with nwez
 */
class info_icons extends CI_Controller {

    /**
     *
     * @var String template name whis is rendered.
     */
    var $default_template_name;

    /**
     * Default constructor loads the models and set ups template name.
     */
    function __construct() {
        parent::__construct();
        $this->load->model('ins_info_icons_model');
        $this->default_template_name = get_super_admin_template_name();
    }

    /**
     * renders page for lsit all newz that belongs to the
     * given client
     */
    public function index() {
        $data['page_name'] = 'super_admin/admin/info_icons/index';
        $data['title'] = 'Info Icons';
        $this->load->view($this->default_template_name, $data);
    }

    /*
     * renders json for the list newz page
     */

    public function get_json() {
        $info_icon = new ins_info_icons_model();
        $info_icons_details = $info_icon->getAll();
        $main_arr = array();
        foreach ($info_icons_details as $icons) {
            $temp = array();
            $temp[] = anchor(base_url() . 'super_admin/info_icons/edit/' . $icons->info_icon_id, $icons->info_icon_name);
            $temp[] = $icons->info_icon_text;
            $temp[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $icons->info_icon_id . "'><img src='" . assets_url_img .
                    "delete.png' alt='Delete' title='Delete'></a>";
            $main_arr[] = $temp;
        }
        $data['aaData'] = $main_arr;
        echo json_encode($data);
    }

    public function add() {
        $data['page_name'] = 'super_admin/admin/info_icons/add';
        $data['title'] = 'Add Info icons';
        $this->load->view($this->default_template_name, $data);
    }

    public function add_listener() {
        $info_icon = new ins_info_icons_model();
        $rules = $info_icon->validationRules();
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('info_icon_name');
            $this->form_validation->set_value('info_icon_text');
            $this->add();
        } else {
            $info_icon->info_icon_name = $this->input->post('info_icon_name');
            $info_icon->info_icon_text = $this->input->post('info_icon_text');
            $info_icon->insertData();
            $this->session->set_flashdata('success', "Info icons added sucessfully.");
            redirect(base_url('super_admin/info_icons'));
        }
    }

    public function edit($info_icon_id) {
        $info_icon = new ins_info_icons_model();
        $info_icons_details = $info_icon->getWhere(array('info_icon_id' => $info_icon_id));

        if (count($info_icons_details) == 0) {
            $this->session->set_flashdata('error', "No such Info Icons exists");
            redirect(base_url('super_admin/info_icons'));
        }
        $icons = $info_icons_details[0];
        $data['page_name'] = 'super_admin/admin/info_icons/edit';
        $data['title'] = 'Edit Info Icons';
        $data['icons'] = $icons;
        $this->load->view($this->default_template_name, $data);
    }

    public function edit_listener($info_icon_id) {
        $info_icon = new ins_info_icons_model();
        $info_icons_details = $info_icon->getWhere(array('info_icon_id' => $info_icon_id));

        if (count($info_icons_details) == 0) {
            $this->session->set_flashdata('error', "No such Info icons exists");
            redirect(base_url('super_admin/info_icons'));
        }
        $rules = $info_icon->validationRules();
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('info_icon_name');
            $this->form_validation->set_value('info_icon_text');
            $this->edit($info_icon_id);
        } else {
            $info_icon->info_icon_id = $info_icon_id;
            $info_icon->info_icon_name = $this->input->post('info_icon_name');
            $info_icon->info_icon_text = $this->input->post('info_icon_text');
            $info_icon->updateData();
            $this->session->set_flashdata('success', "Info icons Edited successfully");
            redirect(base_url('super_admin/info_icons'));
        }
    }

    function delete($id) {
        $this->ins_info_icons_model->info_icon_id = $id;
        $this->ins_info_icons_model->deleteData();
        $this->session->set_flashdata('success', "Info icons deleted sucessfully");
        redirect('super_admin/info_icons', 'refresh');
    }

}