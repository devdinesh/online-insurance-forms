<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class deals with categories > question and answers
 */
class forms_categories_question extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('forms_categories_question_model');
        $this->load->model('forms_categories_model');
        $this->load->model('forms_categories_question_answer_model');
        $this->load->model('forms_model');
        $this->default_template_name = get_super_admin_template_name();
    }

    /**
     * this will render all the question whose cat_id(Category Id) is passed.
     *
     * @param type $id
     *            : cat_id
     */
    public function index($id) {
        $result_category = $this->forms_categories_model->selectSingleRecord(
                'cat_id', $id);
        if ($result_category !== FALSE) {
            $data['page_name'] = 'super_admin/forms/questions/view_question';
            $data['title'] = 'Categories Questions';
            $result_category = $this->forms_categories_model->selectSingleRecord(
                    'cat_id', $id);
            $data['category_details'] = $result_category[0];

            $result_form = $this->forms_model->selectSingleRecord('form_id', $result_category[0]->form_id);
            $data['forms_details'] = $result_form[0];
            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', 'Error !! Please Try Again.May Be Question Not Found.');
            redirect('super_admin/forms', 'refresh');
        }
    }

    /**
     * this will generate the datatable with fill data of question whose cat_id
     * is passed
     *
     * @param type $id            
     */
    function getJson($id) {
        $records = $this->forms_categories_question_model->get_where_sort(
                array('cat_id' => $id
                ), 'asc');

        if ($records !== FALSE) {
            $array = $this->get_array_for_json($records);
        } else {
            $array = array();
        }
        $data['aaData'] = $array;

        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    /**
     * this function make an Array from an object for getJson() function.
     *
     * @param type $objects            
     * @return string
     */
    function get_array_for_json($objects) {
        $arra = array();
        $i = 1;
        $count = count($objects);
        foreach ($objects as $value) {
            // deleted strip_slashes and deleted relative path
            $que_val = str_replace('../../../../../', base_url(), $value->question);
            $re_que_value = strip_slashes($que_val);
            $temp_arr = array();
            $temp_arr[] = '<a href="edit/' . $value->question_id . '">' .
                    $value->question_id . '</a>';
            $temp_arr[] = '<a href="edit/' . $value->question_id . '">' .
                    $re_que_value . '</a>';
            if ($value->answer_kind == 'textarea') {
                $temp_arr[] = '<span title="Answer can not be added for question where answer type is multiline">Klik Hier</span>';
            } else if ($value->answer_kind == 'text') {
                $temp_arr[] = '<span title="Answer can not be added where answer type is single line">Klik Hier</span>';
            } else if ($value->answer_kind == 'date') {
                $temp_arr[] = '<span title="Answer can not be added where answer type is date">Klik Hier</span>';
            } else if ($value->answer_kind == 'number') {
                $temp_arr[] = '<span title="Answer can not be added where answer type is number line">Klik Hier</span>';
            } else {
                $temp_arr[] = "<a href=\"" . base_url() . "super_admin/answers/" .
                        $value->question_id . "\">" . "Klik Hier </a>";
            }
            //
            if ($i == 1) {
                $temp_arr[] = '<a href="' . base_url() .
                        'super_admin/categories_question/movedown/' .
                        $value->question_id . '"><img src="' . base_url() .
                        'assets/img/' .
                        'icon_down.png" width="14" height="15" alt="Delete" /></a>';
            } else if ($i == $count) {
                $temp_arr[] = '<a href="' . base_url() .
                        'super_admin/categories_question/moveup/' .
                        $value->question_id . '"><img src="' . base_url() .
                        'assets/img/' .
                        'icon_up.png" width="14" height="15" alt="Delete" /></a>';
            } else {
                $temp_arr[] = '<a href="' . base_url() .
                        'super_admin/categories_question/moveup/' .
                        $value->question_id . '"><img src="' . base_url() .
                        'assets/img/' .
                        'icon_up.png" width="14" height="15" alt="Delete" /></a> &nbsp;  <a href="' .
                        base_url() . 'super_admin/categories_question/movedown/' .
                        $value->question_id . '"><img src="' . base_url() .
                        'assets/img/' .
                        'icon_down.png" width="14" height="15" alt="Delete" /></a>';
            }
            //

            $temp_arr[] = "<div class = \"btn-group pull-right\"><a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $value->question_id .
                    "' id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"><img src='" . assets_url_img . "delete.png' alt='Delete' title='Delete'></a></div>";

            $arra[] = $temp_arr;
            $i++;
        }
        return $arra;
    }

    /**
     * this function will render the add form
     *
     * @param type $id
     *            : which category question it is.
     */
    function add($id) {
        $result_category = $this->forms_categories_model->selectSingleRecord(
                'cat_id', $id);
        if ($result_category !== FALSE) {
            $data['page_name'] = 'super_admin/forms/questions/add_question';
            $data['title'] = 'Toevoegen vraag';
            $data['question_id'] = $id;

            $data['category_details'] = $result_category[0];

            $result_form = $this->forms_model->selectSingleRecord('form_id', $result_category[0]->form_id);
            $data['forms_details'] = $result_form[0];
            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', 'Error !! Please Try Again.May Be Category Not Found.');
            redirect('super_admin/forms', 'refresh');
        }
    }

    /**
     * this will handel all the data posted by add form.
     *
     * @param type $id
     *            :
     */
    function addListener($id) {
        $result_category = $this->forms_categories_model->selectSingleRecord(
                'cat_id', $id);
        if ($result_category !== FALSE) {
            $obj = new forms_categories_question_model();
            $this->form_validation->set_rules($obj->validation_rules);
            $check_total_runtimebox = $this->input->post(
                    'total_runtime_checkbox');
            if ($this->input->post('answer_kind') == 'radio' ||
                    $this->input->post('answer_kind') == 'checkbox') {
                if ($check_total_runtimebox != false) {
                    for ($i = 1; $i <= $check_total_runtimebox; $i++) {
                        $this->form_validation->set_rules('answers' . $i, '', 'trim|required');
                    }
                } else {
                    $this->form_validation->set_rules('answers1', '', 'trim|required');
                    $this->form_validation->set_rules('answers2', '', 'trim|required');
                }
            }

            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('question');
                $this->form_validation->set_value('sequence');
                $this->form_validation->set_value('help_text');
                $this->form_validation->set_value('answer_kind');
                $this->form_validation->set_value('required');
                $this->add($id);
            } else {
                $obj->cat_id = $id;
                $obj->question = strip_slashes($this->input->post('question'));
                $obj->sequence = $this->input->post('sequence');
                $obj->help_text = $this->input->post('help_text');
                $obj->answer_kind = $this->input->post('answer_kind');
                $obj->required = $this->input->post('required') == true ? 1 : 0;

                $check = $obj->dataUpdateSave();
                if ($this->input->post('answer_kind') == 'radio' ||
                        $this->input->post('answer_kind') == 'checkbox') {
                    if ($check_total_runtimebox != false) {
                        for ($i = 1; $i <= $check_total_runtimebox; $i++) {
                            $obj_ans = new forms_categories_question_answer_model();
                            $obj_ans->question_id = $check;
                            $obj_ans->answer = $this->input->post(
                                    'answers' . $i);
                            $obj_ans->save();
                        }
                    }
                }

                if ($check == TRUE) {
                    $this->session->set_flashdata('success', 'Question Added Sucessufully');
                } else {
                    $this->session->set_flashdata('error', 'Error in Adding The Question');
                }
                redirect('super_admin/forms/categories/question/' . $id, 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', 'Error !! Please Try Again from Strach by click on categoires "Click Here" then Add New Category.');
            redirect('super_admin/forms', 'refresh');
        }
    }

    /**
     * this render the edit from.
     *
     * @param int $id            
     */
    function edit($id) {
        $result_question = $this->forms_categories_question_model->selectSingleRecord(
                'question_id', $id);
        if ($result_question !== FALSE) {
            $data['page_name'] = 'super_admin/forms/questions/edit_question';
            $data['title'] = 'Details varag';
            $data['question_details'] = $result_question[0];

            $result_category = $this->forms_categories_model->selectSingleRecord(
                    'cat_id', $result_question[0]->cat_id);
            $data['category_details'] = $result_category[0];

            $result_form = $this->forms_model->selectSingleRecord('form_id', $result_category[0]->form_id);
            $data['forms_details'] = $result_form[0];

            $result_answers = $this->forms_categories_question_model->getAnswers(
                    $id);
            $data['answer_details'] = $result_answers;

            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', 'Error !! Please Try Again.May Be Question Not Found.');
            redirect('super_admin/forms', 'refresh');
        }
    }

    /**
     * this will handel all the data posted by edit form.
     *
     * @param type $id            
     */
    function editListener($id) {
        $result_question = $this->forms_categories_question_model->selectSingleRecord(
                'question_id', $id);
        if ($result_question !== FALSE) {
            $obj = new forms_categories_question_model();
            $rules = $obj->validation_rules;
            unset($rules[3]);
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('question');
                $this->form_validation->set_value('sequence');
                $this->form_validation->set_value('help_text');
                $this->form_validation->set_value('answer_kind');
                $this->form_validation->set_value('required');
                $this->edit($id);
            } else {
                $obj->question_id = $id;
                $obj->cat_id = $result_question[0]->cat_id;
                $obj->question = $this->input->post('question');
                $obj->sequence = $this->input->post('sequence');
                $obj->help_text = $this->input->post('help_text');
                $obj->required = $this->input->post('required') == true ? 1 : 0;
                $obj->answer_kind = $result_question[0]->answer_kind;

                $check = $obj->dataUpdateSave();
                ////start edit
                $check_total_runtimebox = $this->input->post('total_runtime_checkbox');

                if ($result_question[0]->answer_kind == 'radio' || $result_question[0]->answer_kind == 'checkbox') {
                    $obj_ans = new forms_categories_question_answer_model();
                    $get_ans = $obj_ans->get_where(array('question_id' => $id));

                    foreach ($get_ans as $ans) {

                        $obj_ans = new forms_categories_question_answer_model();
                        $get_ans = $obj_ans->get_where(array('answer_id' => $ans->answer_id));
                        $obj_ans->question_id = $get_ans[0]->question_id;
                        $obj_ans->answer = $this->input->post('given_answer_' . $ans->answer_id);
                        $obj_ans->answer_id = $ans->answer_id;
                        $obj_ans->update();
                    }

                    if ($check_total_runtimebox != false) {
                        for ($i = 1; $i <= $check_total_runtimebox; $i++) {
                            if ($this->input->post('answers' . $i) != '') {
                                $obj_ans = new forms_categories_question_answer_model();
                                $obj_ans->question_id = $id;
                                $obj_ans->answer = $this->input->post('answers' . $i);
                                $obj_ans->save();
                            }
                        }
                    }
                }
                ///end edits
                $result_category = $this->forms_categories_model->selectSingleRecord(
                        'cat_id', $result_question[0]->cat_id);
                redirect(
                        'super_admin/forms/categories/question/' .
                        $result_category[0]->cat_id, 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', 'Error !! Please Try Again from Strach by click on categoires "Click Here" then Add New Category.');
            redirect('super_admin/forms', 'refresh');
        }
    }

    /**
     * this will delete all the data whose question id is passed and cat_id is
     * for redirect purpose.
     *
     * @param type $cat_id            
     * @param type $question_id            
     */
    function deleteQuestion($cat_id, $question_id) {

        $check = $this->forms_categories_question_model->deleteData(
                $question_id);

        if ($check == TRUE) {
            $this->session->set_flashdata('success', 'Question Deleted Sucessufully');
        } else {
            $this->session->set_flashdata('error', 'Error in Deleting Question.');
        }

        redirect(base_url('super_admin/forms/categories/question/' . $cat_id), 'refresh');
    }

    /**
     * this function will delete the answer
     *
     * @param type $question_id            
     * @param type $answer            
     */
    function deleteAnswers($question_id, $answer) {
        $check = $this->forms_categories_question_model->deleteAnswers(
                $question_id, $answer);
        redirect('super_admin/forms/categories/question/edit/' . $question_id, 'refresh');
    }

    public function deleteAnswer_check($answer_id) {
        $obj = new forms_categories_question_answer_model();
        $get_records = $obj->get_where(array('answer_id' => $answer_id));
        if (count($get_records) == 1) {
            $obj->answer_id = $answer_id;
            $check = $obj->delete();
            if ($check == TRUE) {
                $this->session->set_flashdata('success', 'Deleted Sucessufully');
            } else {
                $this->session->set_flashdata('error', 'Error in Delete');
            }
            redirect(base_url() . 'super_admin/forms/categories/question/edit/' . $get_records[0]->question_id, 'refresh');
        } else {
            redirect(base_url() . 'super_admin/forms', 'refresh');
        }
    }

    public function move_sequence_up($question_id) {
        $obj = new forms_categories_question_model();
        $get_current_records = $obj->get_where(
                array('question_id' => $question_id
                ));
        $get_all_records = $obj->get_where_sort(
                array('cat_id' => $get_current_records[0]->cat_id
                ), 'asc');

        if (count($get_current_records) == 1) {
            $temp = array();
            foreach ($get_all_records as $all) {
                $x = array();
                $x['sequence'] = $all->sequence;
                $x['question_id'] = $all->question_id;
                $temp[] = $x;
            }

            $current_key = 0;
            foreach ($temp as $key => $value) {
                if ($value['sequence'] == $get_current_records[0]->sequence) {
                    $current_key = $key;
                }
            }

            // Update Current to up sequence
            $obj->question_id = $temp[$current_key]['question_id'];
            $obj->sequence = $temp[$current_key - 1]['sequence'];
            $obj->updateSequence();

            // Update Previous to up sequence
            $obj->question_id = $temp[$current_key - 1]['question_id'];
            $obj->sequence = $temp[$current_key]['sequence'];
            $obj->updateSequence();
            redirect(
                    'super_admin/forms/categories/question/' .
                    $get_current_records[0]->cat_id, 'refresh');
        } else {
            redirect(base_url() . 'super_admin/forms', 'refresh');
        }
    }

    public function move_sequence_down($question_id) {
        $obj = new forms_categories_question_model();
        $get_current_records = $obj->get_where(
                array('question_id' => $question_id
                ));
        $get_all_records = $obj->get_where_sort(
                array('cat_id' => $get_current_records[0]->cat_id
                ), 'asc');

        if (count($get_current_records) == 1) {
            $temp = array();
            foreach ($get_all_records as $all) {
                $x = array();
                $x['sequence'] = $all->sequence;
                $x['question_id'] = $all->question_id;
                $temp[] = $x;
            }

            $current_key = 0;
            foreach ($temp as $key => $value) {
                if ($value['sequence'] == $get_current_records[0]->sequence) {
                    $current_key = $key;
                }
            }

            // Update Current to up sequence
            $obj->question_id = $temp[$current_key]['question_id'];
            $obj->sequence = $temp[$current_key + 1]['sequence'];
            $obj->updateSequence();

            // Update Previous to up sequence
            $obj->question_id = $temp[$current_key + 1]['question_id'];
            $obj->sequence = $temp[$current_key]['sequence'];
            $obj->updateSequence();

            redirect(
                    'super_admin/forms/categories/question/' .
                    $get_current_records[0]->cat_id, 'refresh');
        } else {
            redirect(base_url() . 'super_admin/forms', 'refresh');
        }
    }

    function add_answer() {
        $ans_to_be_inserted = array();

        $validations = array();

        $total_answers = 0;

        // echo "yet to be done ";
        $question = $this->input->post('question');
        $help_text = $this->input->post('help_text');
        $required = $this->input->post('required');
        $sequence = $this->input->post('sequence');
        $answer_kind = $this->input->post('answer_kind');
        $category_id = $this->input->post('category_id');

        $data['question'] = $question;
        $data['help_text'] = $help_text;
        $data['required'] = $required;
        $data['sequence'] = $sequence;
        $data['answer_kind'] = $answer_kind;
        $data['category_id'] = $category_id;

        /*
         * if ($this->input->post('element_to_be_removed')) { echo
         * $this->input->post('element_to_be_removed'); }
         */

        //
        if ($this->input->post('answer_count')) {
            $data['answer_count'] = $this->input->post('answer_count');
        } else {
            $data['answer_count'] = 0;
        }

        $answer_count = $data['answer_count'];

        if ($answer_count > 0) {
            for ($i = 1; $i <= $answer_count; $i++) {
                if ($this->input->post('ans_' . $i)) {

                    // if there is an element that is to be removed
                    if ($this->input->post('element_to_be_removed')) {
                        // if the element is the one that should not be removed
                        if ($this->input->post('element_to_be_removed') !=
                                ( 'ans_' . $i )) {
                            $data['ans_' . $i] = $this->input->post('ans_' . $i);

                            $ans_to_be_inserted[] = $this->input->post(
                                    'ans_' . $i);
                            $validations[] = array('field' => 'ans_' . $i,
                                'label' => 'Answer',
                                'rules' => 'required|trim|min_length[2]|max_length[255]'
                            );

                            ++$total_answers;
                        }
                    } else {
                        // there is not element that is to be removed
                        $data['ans_' . $i] = $this->input->post('ans_' . $i);
                        $ans_to_be_inserted[] = $this->input->post('ans_' . $i);
                        $validations[] = array('field' => 'ans_' . $i,
                            'label' => 'Answer',
                            'rules' => 'required|trim|min_length[2]|max_length[100]'
                        );
                        ++$total_answers;
                    }
                }
            }
        }

        if ($this->input->post('action')) {
            $action = $this->input->post('action');
            if ($action == 'Add Answer') {
                $data['add_row'] = true;
            }
        }

        if ($this->input->post('action')) {
            $action = $this->input->post('action');

            // if user has clicked on save
            if ($action == 'Save') {

                $this->form_validation->set_rules($validations);

                // if validations fail
                if ($this->form_validation->run() == FALSE) {
                    if ($total_answers < 2) {

                        $this->session->set_flashdata('message_error', 'There should atleast be two answers');
                    }
                    $data['page_name'] = 'super_admin/forms/questions/add_answer';
                    $data['title'] = 'Add Answers To Question';
                    $this->load->view($this->default_template_name, $data);
                } else {
                    if ($total_answers < 2) {
                        $this->session->set_flashdata('message_error', 'There should atleast be two answers');
                        $data['page_name'] = 'super_admin/forms/questions/add_answer';
                        $data['title'] = 'Add Answers To Question';
                        $this->load->view($this->default_template_name, $data);
                    } else {
                        // save the details ;

                        $obj = new forms_categories_question_model();
                        $obj->cat_id = $category_id;
                        $obj->question = $question;
                        $obj->sequence = $sequence;
                        $obj->help_text = $help_text;
                        $obj->answer_kind = $answer_kind;
                        $obj->required = $required ? 1 : 0;
                        $check = $obj->dataUpdateSave();

                        $insert_data = $obj->saveAnswers($check, $ans_to_be_inserted);
                        if ($insert_data == TRUE) {
                            $this->session->set_flashdata('success', 'Question Added Sucessufully');
                        } else {
                            $this->session->set_flashdata('error', 'Error in Adding The Question');
                        }
                        redirect(
                                'super_admin/forms/categories/question/' .
                                $category_id, 'refresh');
                    }
                }
            } else {
                // otherwise
                $data['page_name'] = 'super_admin/forms/questions/add_answer';
                $data['title'] = 'Add Answers To Question';

                $this->load->view($this->default_template_name, $data);
            }
        } else {
            $data['page_name'] = 'super_admin/forms/questions/add_answer';
            $data['title'] = 'Add Answers To Question';

            $this->load->view($this->default_template_name, $data);
        }

        // otherwise
        // print_r($data);
    }

}

?>
