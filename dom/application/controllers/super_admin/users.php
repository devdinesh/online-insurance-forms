<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Controller to help maintain user details
 */
class users extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('clients_model');
        $this->default_template_name = get_mat_super_admin_template_name();
    }

    /**
     * renders list page
     */
    public function index() {
        $data['page_name'] = 'super_admin/users/view_all';
        $data['title'] = 'Users';
        // get the all clients.
        $data['client_info'] = $this->clients_model->get_all();
        $this->load->view($this->default_template_name, $data);
    }

    function edit($id) {
        $data['page_name'] = 'super_admin/users/edit_user';
        $data['title'] = 'Edit User';
        $data['user_id'] = $id;

        $data['client_details'] = $this->clients_model->get_all();
        $res = $this->users_model->selectSingleRecord('user_id', $id);
        if (empty($res)) {
            $this->session->set_flashdata('error', "Error!! May Be User Not Found");
            redirect(base_url() . "super_admin/users");
        }
        $data['user'] = $res[0];

        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
        $obj = new users_model();
        $validation = $obj->validation_rules;
        unset($validation[5]);
        unset($validation[6]);
        $this->form_validation->set_rules($validation);
        if ($this->input->post('password') != '') {
            $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|matches[password]');
        }

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('client_id');
            $this->form_validation->set_value('first_name');
            $this->form_validation->set_value('middle_name');
            $this->form_validation->set_value('last_name');
            $this->form_validation->set_value('mail_address');
            $this->form_validation->set_value('role');
            $this->form_validation->set_value('status');
            $this->form_validation->set_value('administrator');
            $this->form_validation->set_value('password');
            $this->edit($id);
        } else {
            $obj->user_id = $id;
            $obj->client_id = $this->input->post('client_id');
            $obj->first_name = $this->input->post('first_name');
            $obj->middle_name = $this->input->post('middle_name');
            $obj->last_name = $this->input->post('last_name');
            $obj->mail_address = $this->input->post('mail_address');
            $obj->register_date = $this->input->post('register_date');
            $obj->role = $this->input->post('role');

            if ($this->input->post('status') != null) {
                $obj->status = 'A';
            } else {
                $obj->status = 'D';
            }
            if ($this->input->post('administrator')) {
                $obj->administrator = 1;
            } else {
                $obj->administrator = 0;
            }
            if (trim($this->input->post('password')) != '') {
                $obj->password = $this->input->post('password');
            }

            $obj->dataUpdate($id);
            $this->session->set_flashdata('success', "User edited sucessfully");
            redirect('super_admin/users', 'refresh');
        }
    }

    function getJson() {
        $records = $this->users_model->getAll();

        $array = $this->getArrayForJson($records);

        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function getArrayForJson($objects) {
        $arra = array();
        foreach ($objects as $value) {

            $temp_arr = array();

            $temp_arr[] = '<a href="users/edit/' . $value->user_id . '">' .
                    ucwords($value->first_name . ' ' . $value->last_name) .
                    '</a>';
            $temp_arr[] = $value->client_name;
            $temp_arr[] = date('d-m-Y', strtotime($value->register_date));
            if ($value->role == 'admin') {
                $temp_arr[] = '<button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary btn-info">Administrator</button>';
            } else {
                $temp_arr[] = '<button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary btn-warning">Viewer</button>';
            }

            if ($value->status == 'A') {
                $temp_arr[] = '<button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary btn-success">Active</button>';
            } elseif ($value->status == 'D') {
                $temp_arr[] = '<button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary btn-danger">Deactive</button>';
            }
            $temp_arr[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $value->user_id . "'><i class='md md-delete'></i></a>";
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function delete($id) {
        $this->users_model->dataDelete($id);
        $this->session->set_flashdata('success', "Data Deleted sucessfully");
        redirect('super_admin/users', 'refresh');
    }

    // this method is for filtering the data by the client name
    public function getclient_info($client_id = null) {
        // get the client for which we want to get the list
        $array = '';
        $selected = $this->users_model->get_filter_Data($client_id);
        if (is_array($selected) && count($selected) > 0) {
            $array = $this->getArrayForJson($selected);
        }
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    /**
     * this funcrtion will render the Add Form
     */
    function add() {
        $data['client_details'] = $this->clients_model->get_all();

        $data['page_name'] = 'super_admin/users/add_user';
        $data['title'] = 'Add User';
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * this will handel all the data post by the add method form.
     */
    function addListener() {
        $obj = new users_model();

        $validation = $obj->validation_rules;

        $this->form_validation->set_rules($validation);

        if ($this->input->post('password') != '') {
            $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|trim|matches[password]');
        }

        if ($this->form_validation->run() == FALSE) {

            $this->form_validation->set_value('client_id');
            $this->form_validation->set_value('first_name');
            $this->form_validation->set_value('middle_name');
            $this->form_validation->set_value('last_name');
            $this->form_validation->set_value('mail_address');
            $this->form_validation->set_value('status');
            $this->form_validation->set_value('administrator');
            $this->form_validation->set_value('password');
            $this->form_validation->set_value('password_confirmation');
            $this->form_validation->set_value('role');
            $this->add();
        } else {
            $obj->client_id = $this->input->post('client_id');
            $obj->first_name = $this->input->post('first_name');
            $obj->middle_name = $this->input->post('middle_name');
            $obj->last_name = $this->input->post('last_name');
            $obj->mail_address = $this->input->post('mail_address');
            $obj->role = $this->input->post('role');

            if ($this->input->post('status') != null) {
                $obj->status = 'A';
            } else {
                $obj->status = 'D';
            }
            if ($this->input->post('administrator')) {
                $obj->administrator = 1;
            } else {
                $obj->administrator = 0;
            }
            if ($this->input->post('is_handler') != null) {
                $obj->is_handler = '1';
            } else {
                $obj->is_handler = '0';
            }
            if (trim($this->input->post('password')) != '') {
                $obj->password = md5($this->input->post('password'));
            }

            $obj->save();
            $this->session->set_flashdata('success', "User added sucessfully");
            redirect('super_admin/users', 'refresh');
        }
    }

}

