<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class subscriptions extends CI_Controller
{

    var $default_template_name;

    function __construct ()
    {
        parent::__construct();
        
        $this->load->model('clients_model');
        $this->load->model('forms_model');
        $this->load->model('client_subscription_model');
        $this->load->model('admin_subscription_kinds_model');
        $this->default_template_name = get_super_admin_template_name();
    
    }

    /**
     * this will render the super admin login form
     */
    public function index ()
    {
        $client_info = $this->client_subscription_model->get_disctinct_client_name();
        
        $data['client_info'] = $client_info;
        $subscription_type_info = $this->client_subscription_model->get_disctinct_kind_of_subscription();
        $data['subscription_type_info'] = $subscription_type_info;
        $data['page_name'] = 'super_admin/subscriptions/index';
        $data['title'] = 'Subscriptions';
        
        $this->load->view($this->default_template_name, $data);
    
    }

    function client_subscription ( $clinet_id )
    {
        $client = new clients_model();
        $client = $client->get_where(
                array('client_id' => $clinet_id
                ));
        
        $data['page_name'] = 'super_admin/subscriptions/clinet_specific_subscription_index';
        $data['title'] = 'Subscriptions';
        $data['client'] = $client[0];
        
        $this->load->view($this->default_template_name, $data);
    
    }

    /**
     * this is the method for get the subscription with end date is null and
     * filter by the check box.
     */
    function get_subscription ( $status = null, $client_id = null, 
            $subscription_type = null )
    {
        $status = urldecode($status);
        $client_id = urldecode($client_id);
        $subscription_type = urldecode($subscription_type);
        $array = '';
        $selected = $this->client_subscription_model->get_filter_Data($status, 
                $client_id, $subscription_type);
        if (is_array($selected) && count($selected) > 0)
        {
            $array = $this->get_json($selected);
        }
        $data['aaData'] = $array;
        
        if (is_array($data))
        {
            echo json_encode($data);
        }
    
    }

    function client_subscriptions_json ( $client_id )
    {
        
        // get subscriptiosn for given client id
        $client_subscription = new client_subscription_model();
        $client_subscriptions = $client_subscription->get_where(
                array('client_id' => $client_id
                ));
        $result = array();
        // iterate thru each of them
        foreach ($client_subscriptions as $subscription)
        {
            // get subscription type
            $subscription_info = $this->admin_subscription_kinds_model->selectSingleRecord(
                    'subscription_kinds_id', 
                    $subscription->subscription_kinds_id);
            
            $subscription_type = $subscription_info[0]->subscription_title;
            
            $record = array();
            // count forms
            $no_of_forms = $this->forms_model->created_forms_between_from_and_to_date(
                    $client_id, 
                    date('Y-m-d', strtotime($subscription->from_date)), 
                    date('Y-m-d', strtotime($subscription->end_date)));
            
            // construct array
            
            $record[] = $subscription_type;
            $record[] = '<a href="' . base_url() .
                     'super_admin/subscriptions/edit/' .
                     $subscription->client_subscription_id . '">' .
                     $subscription->from_date . "</a>";
            $record[] = $subscription->end_date;
            $record[] = $subscription_info[0]->subscription_fee;
            $record[] = $no_of_forms;
            
            $result[] = $record;
        }
        
        $ans['aaData'] = $result;
        echo json_encode($ans);
    
    }

    function get_json ( $client_subscriptions )
    {
        $result = array();
        // iterate thru each of them
        foreach ($client_subscriptions as $subscription)
        {
            // get subscription type
            $subscription_type = $this->admin_subscription_kinds_model->selectSingleRecord(
                    'subscription_kinds_id', 
                    $subscription->subscription_kinds_id);
            $subscription_type = $subscription_type[0]->subscription_title;
            $record = array();
            
            // construct array
            
            $this->load->model('clients_model');
            $clients_model = new clients_model();
            $clients = $clients_model->get_where(
                    array('client_id' => $subscription->client_id
                    ));
            
            $record[] = $clients[0]->client_name;
            $record[] = $subscription_type;
            $record[] = $subscription->from_date; // '<a href="' . base_url() .
                                                  // 'super_admin/subscriptions/edit/'
                                                  // .
                                                  // $subscription->client_subscription_id
                                                  // . '">' . 'link title' .
                                                  // "</a>"
            $record[] = $subscription->end_date;
            $result[] = $record;
        }
        return $result;
    
    }

    function client_invoice_json ( $client_id )
    {
        $this->load->model('invoice_model');
        $invoice_model = new invoice_model();
        $invoices = $invoice_model->get_where(
                array('client_id' => $client_id
                ));
        $arra = array();
        
        foreach ($invoices as $invoice)
        {
            $temp_arr = array();
            
            $temp_arr[] = $invoice->invoice_number;
            $temp_arr[] = '&euro;' . $invoice->invoice_amount;
            $temp_arr[] = $invoice->invoice_date;
            $temp_arr[] = '<a target="_blank" href="' . base_url() .
                     'super_admin/invoices/create_pdf/' .
                     $invoice->invoice_number . '">' .
                     ' <img style="height: 35px;"  src="' . assets_url_img .
                     'PDF-Document-icon.png' . '" alt="alt"/>' . '</a>';
            
            $arra[] = $temp_arr;
        }
        
        $data['aaData'] = $arra;
        if (is_array($data))
        {
            echo json_encode($data);
        }
    
    }

    /**
     * this method renders the view to let the super admin add the
     * subscription whis id is passed as parameter
     *
     * @param type $client_id
     *            is the id of client that you want
     *            to add new subscription for that client.
     */
    function add ( $client_id )
    {
        $data['page_name'] = 'super_admin/subscriptions/add_subscription';
        $data['title'] = 'add Subscriptions';
        $data['client_id'] = $client_id;
        $admin_subscription_kinds = new admin_subscription_kinds_model();
        
        $data['subcription_kinds'] = $admin_subscription_kinds->get_all();
        $this->load->view($this->default_template_name, $data);
    
    }

    function add_listener ( $client_id )
    {
        $obj = new client_subscription_model();
        // add a rule for date comparison
        $obj->form_validations[1]['rules'] = $obj->form_validations[1]['rules'] .
                 "|date_greater[" . $this->input->post('from_date') .
                 "]|not_same_as[" . $this->input->post('from_date') . "]";
        // add a rule for for date coliding
        // see the comments in the validator if you wish
        // application->librabry->MY_Form_validation.php->
        // client_subscription_coliding method
        // new rules for subscription_kinds_id is required
        $obj->form_validations[1]['rules'] = $obj->form_validations[1]['rules'] .
                 "|client_subscription_coliding[" .
                 str_replace("-", "-", $this->input->post('from_date')) . "," .
                 $obj->client_id . ',' . $client_id . "]";
        $this->form_validation->set_rules('subscription_kinds_id', 
                'subscription_kinds', 'required');
        $this->form_validation->set_rules($obj->form_validations);
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->form_validation->set_value('from_date');
            $this->form_validation->set_value('end_date');
            $this->form_validation->set_value('subscription_kinds_id');
            // validation failed, render the form again with error messages
            $this->add($client_id);
        }
        else
        {
            // get the subscription object in order to get the price
            $this->load->model('admin_subscription_kinds_model');
            $admin_subscription_kinds_model = new admin_subscription_kinds_model();
            $kinds = $admin_subscription_kinds_model->get_wher(
                    array(
                            'subscription_kinds_id' => $this->input->post(
                                    'subscription_kinds_id')
                    ));
            $kind = $kinds[0];
            
            // set the values back
            $obj->client_id = $client_id;
            $obj->from_date = $this->input->post('from_date');
            $obj->end_date = $this->input->post('end_date');
            $obj->subscription_kinds_id = $this->input->post(
                    'subscription_kinds_id');
            $obj->monthly_fee = $kind->subscription_fee;
            $obj->save();
            
            // get the subscription king in order to get the
            // amount etc
            $admin_subscription_kinds_model = new admin_subscription_kinds_model();
            $subscription_kinds = $admin_subscription_kinds_model->get_wher(
                    array(
                            'subscription_kinds_id' => $this->input->post(
                                    'subscription_kinds_id')
                    ));
            
            // make an entry in invoices table
            $this->load->model('invoice_model');
            $invoice_model = new invoice_model();
            $invoice_model->invoice_amount = $subscription_kinds[0]->subscription_fee;
            $invoice_model->invoice_period_start = $this->input->post(
                    'from_date');
            $invoice_model->invoiece_period_end = $this->_add_one_month_to_date(
                    $this->input->post('from_date'));
            $invoice_model->invoice_date = date('d-m-Y'); // set the current
                                                          // date
            $invoice_model->client_id = $client_id;
            $invoice_model->invoice_subject = "Invoice for Online Insurance Forms";
            $invoice_model->save();
            
            // send to view all page
            redirect(
                    base_url() . "super_admin/client_subscriptions/" .
                             $obj->client_id);
        }
    
    }

    /**
     * this method renders the view to let the super admin edit the
     * subscription whis id is passed as parameter
     *
     * @param type $client_subscription_id
     *            is the id of subscription that you want
     *            to edit.
     */
    function edit ( $client_subscription_id )
    {
        
        // get the client subscription whos id is passed
        $client_subscription = new client_subscription_model();
        $subscriptions = $client_subscription->get_where(
                array('client_subscription_id' => $client_subscription_id
                ));
        $subscription = $subscriptions[0];
        
        // construct the parameters to be passed
        $data['page_name'] = 'super_admin/subscriptions/edit_subscription';
        $data['title'] = 'Edit Subscriptions';
        $data['subscription'] = $subscription;
        $admin_subscription_kinds = new admin_subscription_kinds_model();
        
        $data['subcription_kinds'] = $admin_subscription_kinds->get_all();
        
        // $subscription_kinds = $subscription->
        // render the view
        $this->load->view($this->default_template_name, $data);
    
    }

    function edit_listener ( $client_subscription_id )
    {
        // echo $client_subscription_id;
        $obj = new client_subscription_model();
        $obj = $obj->get_where(
                array('client_subscription_id' => $client_subscription_id
                ));
        $obj = $obj[0];
        
        // add a rule for date comparison
        $obj->form_validations[1]['rules'] = $obj->form_validations[1]['rules'] .
                 "|date_greater[" . $this->input->post('from_date') .
                 "]|not_same_as[" . $this->input->post('from_date') . "]";
        // add a rule for for date coliding
        // see the comments in the validator if you wish
        // application->librabry->MY_Form_validation.php->
        // client_subscription_coliding method
        /*
         * $obj->form_validations[1]['rules'] =
         * $obj->form_validations[1]['rules'] . "|client_subscription_coliding["
         * . str_replace("-", "-", $this->input->post('from_date')) . "," .
         * $obj->client_id . ',' . $client_subscription_id . "]";
         */
        $this->form_validation->set_rules($obj->form_validations);
        if ($this->form_validation->run() == FALSE)
        {
            $this->form_validation->set_value('from_date');
            $this->form_validation->set_value('to_date');
            // validation failed, render the form again with error messages
            $this->edit($client_subscription_id);
        }
        else
        {
            // set the values back
            $obj->from_date = $this->input->post('from_date');
            $obj->end_date = $this->input->post('end_date');
            $obj->subscription_kinds_id = $this->input->post(
                    'subscription_kinds_id');
            $obj->update();
            // / send to view all page
            redirect(
                    base_url() . "super_admin/client_subscriptions/" .
                             $obj->client_id);
        }
    
    }

    public function get_json_for_all_clients ()
    {
        $this->load->model('admin_subscription_kinds_model');
        // get subscriptiosn for given client id
        $client_subscription = new client_subscription_model();
        $client_subscriptions = $client_subscription->get_where(
                array('end_date' => Null
                ));
        $result = array();
        
        // iterate thru each of them
        foreach ($client_subscriptions as $subscription)
        {
            // get subscription type
            $subscription_type = $this->admin_subscription_kinds_model->selectSingleRecord(
                    'subscription_kinds_id', 
                    $subscription->subscription_kinds_id);
            $subscription_type = $subscription_type[0]->subscription_title;
            $record = array();
            
            // construct array
            
            $this->load->model('clients_model');
            $clients_model = new clients_model();
            $clients = $clients_model->get_where(
                    array('client_id' => $subscription->client_id
                    ));
            
            $record[] = $clients[0]->client_name;
            $record[] = $subscription_type;
            $record[] = $subscription->from_date; // '<a href="' . base_url() .
                                                  // 'super_admin/subscriptions/edit/'
                                                  // .
                                                  // $subscription->client_subscription_id
                                                  // . '">' . 'link title' .
                                                  // "</a>"
            $record[] = $subscription->end_date;
            $result[] = $record;
        }
        
        $ans = array();
        $ans['aaData'] = $result;
        echo json_encode($ans);
    
    }

    private function _add_one_month_to_date ( $date )
    {
        $new_date = date("d-m-Y", strtotime($date));
        $newdate = strtotime('+1 month -1day', strtotime($new_date));
        return date("d-m-Y", $newdate);
    
    }

    public function get_disctinct_client_name ()
    {
        $objects = array();
        $this->db->select('status');
        $this->db->distinct();
        $this->db->where(array('client_id' => $client_id
        ));
        $query = $this->db->get($this->table_name);
        foreach ($query->result() as $row)
        {
            $objects[] = $row->status;
        }
        return $objects;
    
    }

}

?>
