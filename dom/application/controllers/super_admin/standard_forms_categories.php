<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the admin and other
 * methods.
 */
class standard_forms_categories extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('standard_forms_categories_model');
        $this->load->model('forms_categories_question_answer_model');
        $this->load->model('forms_categories_question_model');
        $this->load->model('standard_forms_model');
        $this->default_template_name = get_mat_super_admin_template_name();
        language_change::setLanguage();
    }

    /**
     * this will render the admin categoris form
     */
    public function index($id) {
        $data['page_name'] = 'super_admin/standard_forms/cetegories/view_category';
        $data['title'] = $this->lang->line('overview_categories');
        $res = $this->standard_forms_model->selectSingleRecord('form_id', $id);
        if (empty($res)) {
            $this->session->set_flashdata('error', "Error !! May Be Category not found");
            redirect('super_admin/standard_forms', 'refresh');
        }
        $data['form_details'] = $res[0];
        $this->load->view($this->default_template_name, $data);
    }

    function getJson($id) {
        $records = $this->standard_forms_categories_model->get_where_sort(
                array('form_id' => $id
                ), 'asc');

        if ($records !== FALSE) {
            $array = $this->get_array_for_json($records);
        } else {
            $array = array();
        }
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function get_array_for_json($objects) {
        $arra = array();
        $i = 1;
        $count = count($objects);
        foreach ($objects as $value) {

            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() . 'super_admin/standard_categories/edit/' .
                    $value->cat_id . '">' . $value->cat_name . '</a>';
            $temp_arr[] = '<a href="' . base_url() . 'super_admin/standard_categories_question/' . $value->cat_id . '">'.$this->lang->line('click_here').'</a>';
            $temp_arr[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" . $value->cat_id . "' data-toggle=\"tooltip\" data-placement=\"top\" title='".$this->lang->line('delete')."'><i class='md md-delete'></i></a>";
            $temp_arr[] = 'cat_id_' . $value->cat_id;
            $arra[] = $temp_arr;
            $i++;
        }
        return $arra;
    }

    function add($id) {
        $res = $this->standard_forms_model->selectSingleRecord('form_id', $id);
        if ($res !== FALSE) {
            $data['page_name'] = 'super_admin/standard_forms/cetegories/add_category';
            $data['title'] = $this->lang->line('add_category');
            $res = $this->standard_forms_model->selectSingleRecord('form_id', $id);
            $data['form_details'] = $res[0];
            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', $this->lang->line('category_click_error'));
            redirect('super_admin/standard_forms', 'refresh');
        }
    }

    function addListener($id) {
        $res = $this->standard_forms_model->selectSingleRecord('form_id', $id);
        if ($res !== FALSE) {
            $obj = new standard_forms_categories_model($id);
            $this->form_validation->set_rules($obj->validation_rules);

            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('cat_name');
                $this->form_validation->set_value('introduction_text');
                $this->add($id);
            } else {
                $obj->form_id = $id;
                $obj->cat_name = $this->input->post('cat_name');
                $obj->sequence = $obj->getNewSequenceId($id);
                $obj->introduction_text = $this->input->post('introduction_text');

                $check = $obj->dataUpdateSave();
                if ($check == TRUE) {
                    $this->session->set_flashdata('success', $this->lang->line('add_success'));
                } else {
                    $this->session->set_flashdata('error', $this->lang->line('add_error'));
                }
                redirect('super_admin/standard_categories/' . $id, 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('category_click_error'));
            redirect('super_admin/standard_forms', 'refresh');
        }
    }

    function edit($id) {
        $res = $this->standard_forms_categories_model->selectSingleRecord('cat_id', $id);

        if ($res !== FALSE) {
            $data['page_name'] = 'super_admin/standard_forms/cetegories/edit_category';
            $data['title'] = $this->lang->line('edit_category');
            $data['cat_details'] = $res[0];
            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', $this->lang->line('category_click_error'));
            redirect('super_admin/standard_forms', 'refresh');
        }
    }

    function editListener($id) {
        $res = $this->standard_forms_categories_model->selectSingleRecord('cat_id', $id);
        if ($res !== FALSE) {
            $obj = new standard_forms_categories_model();
            $validation = $obj->validation_rules;
            unset($validation[0]);
            $this->form_validation->set_rules($validation);
            $cat_name = $id . ",cat_id," . $res[0]->form_id .
                    ',cat_name,standard_forms_categories_model';
            $this->form_validation->set_rules('cat_name', 'Categorienaam', 'trim|required|edit_isDataExit_validator[' . $cat_name . ']');
            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('cat_name');
                $this->form_validation->set_value('introduction_text');
                $this->edit($id);
            } else {
                $obj->cat_id = $id;
                $obj->form_id = $res[0]->form_id;
                $obj->cat_name = $this->input->post('cat_name');
                $obj->sequence = $res[0]->sequence;
                $obj->introduction_text = $this->input->post(
                        'introduction_text');

                $check = $obj->dataUpdateSave();
                if ($check == TRUE) {
                    $this->session->set_flashdata('success', $this->lang->line('edit_success'));
                } else {
                    $this->session->set_flashdata('error', $this->lang->line('edit_error'));
                }

                redirect('super_admin/standard_categories/' . $res[0]->form_id, 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', $this->lang->line('category_click_error'));
            redirect('super_admin/standard_forms', 'refresh');
        }
    }

    function delete($form_id, $cat_id) {
        $check = $this->standard_forms_categories_model->deleteData($cat_id);
        if ($check == TRUE) {
            $this->session->set_flashdata('success', $this->lang->line('delete_success'));
        } else {
            $this->session->set_flashdata('error', $this->lang->line('delete_error'));
        }
        redirect('super_admin/standard_categories/' . $form_id, 'refresh');
    }

    function check_sequence_is_correct($categories) {
        $obj = new standard_forms_categories_model();
        $obj1 = new forms_categories_question_answer_model();
        $obj2 = new forms_categories_question_model();
        $main_cat_info = $obj->get_where(array('cat_id' => $categories));
        $smart_question = $obj->get_question_smart_for_category($categories);
        $sucess = 1;
        if (!empty($smart_question) && $smart_question != false) {
            foreach ($smart_question as $smatr_ques) {
                $answers = $obj1->get_where(array('question_id' => $smatr_ques->question_id));
                foreach ($answers as $ans) {
                    if ($ans->skip_to_questions != '' || $ans->skip_to_questions != 0) {
                        $question_info = $obj2->get_where(array('question_id' => $ans->skip_to_questions));
                        $cat_info = $obj->get_where(array('cat_id' => $question_info[0]->cat_id));
                        if ($main_cat_info[0]->sequence >= $cat_info[0]->sequence) {
                            $sucess = 1;
                            //echo 'true-' . $main_cat_info[0]->sequence . '--' . $cat_info[0]->sequence . '========';
                        } else {
                            $sucess = 0;
                            break;

                            // echo 'false-' . $main_cat_info[0]->sequence . '--' . $cat_info[0]->sequence . '========';
                        }
                    } else {
                        $sucess = 1;
                        // echo "3true-";
                    }
                }
            }
        } else {
            $sucess = 1;
            // echo "4true-";
        }
        return $sucess;
    }

    function sortable() {
        $obj = new standard_forms_categories_model();
        $cat_array = $_POST['cat_id'];
        $temp = 1;
        $get_cat_info = $obj->get_where(array('cat_id' => $cat_array[0]));
        $get_all_cat = $obj->get_where_sort(array('form_id' => $get_cat_info[0]->form_id), 'asc');
        $org_cat_array = array();
        foreach ($get_all_cat as $cat) {
            $org_cat_array[] = $cat->cat_id;
        }
        for ($i = 0; $i < count($org_cat_array); $i++) {
            if ($org_cat_array[$i] == $cat_array[$i]) {
                continue;
            } else {
                $res = $this->check_sequence_is_correct($cat_array[$i]);
                if ($res === 0) {
                    $temp = 0;
                    break;
                } else {
                    $temp = 1;
                }
            }
        }
        if ($temp == 1) {
            $obj->updateAllSequence($_POST['cat_id']);
            $this->session->set_flashdata('success', $this->lang->line('sequence_change_success'));
            return true;
        } else {
            $this->session->set_flashdata('error',$this->lang->line('sequence_change_error'));
            return false;
        }
    }

}

?>
