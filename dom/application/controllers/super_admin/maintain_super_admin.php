<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class maintain_super_admin extends CI_Controller
{

    var $default_template_name;

    var $id;

    function __construct ()
    {
        parent::__construct();
        $this->load->model('super_admin_authenticate');
        
        $super_admin = $this->session->userdata('super_admin');
        $this->id = $super_admin['super_admin_id'];
        
        $this->default_template_name = get_super_admin_template_name();
    
    }

    public function index ()
    {
        $data['page_name'] = 'super_admin/maintain_super_admin/index';
        $data['title'] = 'Maitain Super Admins';
        $this->load->view($this->default_template_name, $data);
    
    }

    /**
     * returns the json for list super admins
     */
    public function getJson ()
    {
        $records = $this->super_admin_authenticate->get_all();
        
        $array = $this->get_array_for_json($records);
        
        $data['aaData'] = $array;
        if (is_array($data))
        {
            echo json_encode($data);
        }
    
    }

    function get_array_for_json ( $objects )
    {
        $arra = array();
        foreach ($objects as $value)
        {
            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() .
                     'super_admin/maintain_super_admin/edit/' .
                     $value->super_admin_id . '">' .
                     $value->super_admin_first_name . '</a>';
            $temp_arr[] = $value->super_admin_middle_name;
            $temp_arr[] = $value->super_admin_last_name;
            $temp_arr[] = $value->super_admin_email;
            $arra[] = $temp_arr;
        }
        return $arra;
    
    }

    public function add ()
    {
        $data['page_name'] = 'super_admin/maintain_super_admin/add';
        $data['title'] = 'Maitain Super Admins';
        $this->load->view($this->default_template_name, $data);
    
    }

    public function add_listener ()
    {
        $this->form_validation->set_rules('super_admin_first_name', 
                'First Name', 'required|trim|min_length[2]|max_length[45]');
        $this->form_validation->set_rules('super_admin_middle_name', 
                'Middle Name', 'required|trim|min_length[2]|max_length[45]');
        $this->form_validation->set_rules('super_admin_last_name', 'Last Name', 
                'required|trim|min_length[2]|max_length[45]');
        $this->form_validation->set_rules('super_admin_email', 'Email', 
                'required|valid_email|trim|min_length[2]|max_length[45]');
        $this->form_validation->set_rules('super_admin_password', 'Password', 
                'required|trim|min_length[2]|max_length[45]');
        $this->form_validation->set_rules('super_admin_password_repeat', 
                'Repeat Password', 
                'required|trim|matches[super_admin_password]|min_length[2]|max_length[45]');
        
        if ($this->form_validation->run() == FALSE)
        {
            
            $this->form_validation->set_value('super_admin_first_name');
            $this->form_validation->set_value('super_admin_middle_name');
            $this->form_validation->set_value('super_admin_last_name');
            $this->form_validation->set_value('super_admin_email');
            $this->form_validation->set_value('super_admin_password');
            $this->form_validation->set_value('super_admin_password_repeat');
            $this->add();
        }
        else
        {
            $this->super_admin_authenticate->super_admin_first_name = $this->input->post(
                    'super_admin_first_name');
            $this->super_admin_authenticate->super_admin_middle_name = $this->input->post(
                    'super_admin_middle_name');
            
            $this->super_admin_authenticate->super_admin_last_name = $this->input->post(
                    'super_admin_last_name');
            $this->super_admin_authenticate->super_admin_email = $this->input->post(
                    'super_admin_email');
            
            $this->super_admin_authenticate->super_admin_username = $this->input->post(
                    'super_admin_email');
            
            $this->super_admin_authenticate->super_admin_password = md5(
                    $this->input->post('super_admin_password'));
            $status = $this->super_admin_authenticate->save();
            
            if ($status)
            {
                $this->session->set_flashdata('success', 
                        "Super admins was added successfully");
            }
            else
            {
                $this->session->set_flashdata('error', 
                        "Super admins could not be added successfully. Something went wrong.");
            }
            redirect(base_url() . "super_admin/maintain_super_admin/index");
        }
    
    }

    public function edit ( $super_admin_id )
    {
        $result = $this->super_admin_authenticate->get_where(
                array('super_admin_id' => $super_admin_id
                ));
        if (empty($result))
        {
            $this->session->set_flashdata('error', 
                    "Error !! Please Try Again ! May Be User Not Found");
            redirect(base_url() . "super_admin/maintain_super_admin");
        }
        $super_admin = $result[0];
        $data['super_admin'] = $super_admin;
        
        $data['page_name'] = 'super_admin/maintain_super_admin/edit';
        $data['title'] = 'Maitain Super Admins';
        $this->load->view($this->default_template_name, $data);
    
    }

    public function edit_details_listener ( $super_admin_id )
    {
        $this->form_validation->set_rules('super_admin_first_name', 
                'First Name', 'required|trim|min_length[2]|max_length[45]');
        $this->form_validation->set_rules('super_admin_middle_name', 
                'Middle Name', 'required|trim|min_length[2]|max_length[45]');
        $this->form_validation->set_rules('super_admin_last_name', 'Last Name', 
                'required|trim|min_length[2]|max_length[45]');
        $this->form_validation->set_rules('super_admin_email', 'Email', 
                'required|valid_email|trim|min_length[2]|max_length[45]');
        
        if ($this->form_validation->run() == FALSE)
        {
            
            $this->form_validation->set_value('super_admin_first_name');
            $this->form_validation->set_value('super_admin_middle_name');
            $this->form_validation->set_value('super_admin_last_name');
            $this->form_validation->set_value('super_admin_email');
            $this->edit($super_admin_id);
        }
        else
        {
            
            $result = $this->super_admin_authenticate->get_where(
                    array('super_admin_id' => $super_admin_id
                    ));
            $super_admin = $result[0];
            $super_admin->super_admin_first_name = $this->input->post(
                    'super_admin_first_name');
            $super_admin->super_admin_middle_name = $this->input->post(
                    'super_admin_middle_name');
            
            $super_admin->super_admin_last_name = $this->input->post(
                    'super_admin_last_name');
            $super_admin->super_admin_email = $this->input->post(
                    'super_admin_email');
            
            $super_admin->super_admin_username = $this->input->post(
                    'super_admin_email');
            $super_admin->update();
            $this->session->set_flashdata('success', 
                    "Super admins was updated successfully");
            redirect(base_url() . "super_admin/maintain_super_admin/index");
        }
    
    }

    function chage_password_listener ( $super_admin_id )
    {
        $this->form_validation->set_rules('super_admin_password', 'Password', 
                'required|trim|min_length[2]|max_length[45]');
        $this->form_validation->set_rules('super_admin_password_repeat', 
                'Repeat Password', 
                'required|trim|matches[super_admin_password]|min_length[2]|max_length[45]');
        
        if ($this->form_validation->run() == FALSE)
        {
            
            $this->form_validation->set_value('super_admin_first_name');
            $this->form_validation->set_value('super_admin_middle_name');
            $this->edit($super_admin_id);
        }
        else
        {
            $result = $this->super_admin_authenticate->get_where(
                    array('super_admin_id' => $super_admin_id
                    ));
            $super_admin = $result[0];
            
            $super_admin->super_admin_password = md5(
                    $this->input->post('super_admin_password'));
            $super_admin->update();
            $this->session->set_flashdata('success', 
                    "Super admins password was updated successfully");
            redirect(base_url() . "super_admin/maintain_super_admin/index");
        }
    
    }

}

?>
