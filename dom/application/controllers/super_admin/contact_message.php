<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class contact_message extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('contact_message_model');
        $this->load->model('users_model');
        $this->default_template_name = get_super_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
        $data['page_name'] = 'super_admin/admin/contact_message/view_contact_message';
        $data['title'] = 'Contact Message';
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * a method that renders the json containing list of all the clients
     * that can be understood by the jquery data table
     */
    function getJsonmessage() {
        $records = $this->contact_message_model->get_all();
        $array = $this->_get_array_for_json($records);
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function _get_array_for_json($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $user_info = $this->users_model->selectSingleRecord('user_id', $value->user_id);
            $temp_arr = array();
            $temp_arr[] = $value->date;
            $temp_arr[] = $user_info[0]->first_name . ' ' . $user_info[0]->last_name;
            $temp_arr[] = $user_info[0]->mail_address;
            $temp_arr[] = $value->subject;
            $temp_arr[] = $value->message;
            $temp_arr[] = '<a href="javascript:;" onclick="deleteRow(' . $value->id . ')" class="deletepage"  data-toggle="tooltip" data-placement="top" title="Delete"><img src="' . assets_url_img . 'delete.png" alt="Delete" title="Delete"></a>';
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function delete($id) {
        $this->contact_message_model->id=$id;
        $check = $this->contact_message_model->delete();
        if ($check == TRUE) {
            $this->session->set_flashdata('success', "Contact Messsage was deleted successfully");
        } else {
            $this->session->set_flashdata('error', "Error While Delete The Contact Message");
        }
        redirect(base_url() . "super_admin/contact_message", 'refresh');
    }

}