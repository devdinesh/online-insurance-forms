<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class feedback_details extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('feedback_category_model');
        $this->load->model('feedback_record_model');
        $this->load->model('users_model');
        $this->default_template_name = get_super_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
        $data['page_name'] = 'feedback/view_feedback';
        $data['title'] = 'Subscriptions';
        $this->load->view($this->default_template_name, $data);
    }

    function getJsonfeedback() {
        $records = $this->feedback_record_model->get_all();
        $array = $this->_get_array_for_json($records);
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function _get_array_for_json($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $cat_info = $this->feedback_category_model->get_where(array('feed_cat_id' => $value->feed_cat_id));
            $temp_arr = array();
            $temp_arr[] = $value->feed_created;
            $temp_arr[] = $value->feed_rating;
            $temp_arr[] = $cat_info[0]->feed_cat_name;
            $temp_arr[] = $value->feed_username;
            $temp_arr[] = $value->feed_email;
            $temp_arr[] = $value->feed_remark;
            $temp_arr[] = '<a href="javascript:;" onclick="deleteRow(' . $value->feed_id . ')" class="deletepage"  data-toggle="tooltip" data-placement="top" title="Delete"><img src="' . assets_url_img . 'delete.png" alt="Delete" title="Delete"></a>';
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function delete($id) {
        $this->feedback_record_model->feed_id = $id;
        $check = $this->feedback_record_model->delete();
        if ($check == TRUE) {
            $this->session->set_flashdata('success', "Feedback Detail was deleted successfully");
        } else {
            $this->session->set_flashdata('error', "Error While Delete The Feedback Details");
        }
        redirect(base_url() . "super_admin/feedback", 'refresh');
    }

}

?>
