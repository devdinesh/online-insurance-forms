<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class forms_categories extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('forms_categories_model');
        $this->load->model('forms_model');
        $this->default_template_name = get_super_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function index($id) {
        $data['page_name'] = 'super_admin/forms/categories/view_category';
        $data['title'] = 'Forms Categories';
        $res = $this->forms_model->selectSingleRecord('form_id', $id);
        if (empty($res)) {
            $this->session->set_flashdata('error', "Error !!Please Try Again ! May Be Category Not Found");
            redirect('super_admin/forms', 'refresh');
        }
        $data['form_details'] = $res[0];
        $this->load->view($this->default_template_name, $data);
    }

    function getJson($id) {
        $records = $this->forms_categories_model->selectMoreRecord('form_id', $id);

        if ($records !== FALSE) {
            $array = $this->get_array_for_json($records);
        } else {
            $array = array();
        }
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function get_array_for_json($objects) {
        $arra = array();
        $i = 1;
        $count = count($objects);
        foreach ($objects as $value) {
            $temp_arr = array();
            $temp_arr[] = '<a href="edit/' . $value->cat_id . '">' .
                    $value->cat_name . '</a>';
            $temp_arr[] = '<a href="question/' . $value->cat_id .
                    '">Click Here</a>';

            $temp_arr[] = "<div class = \"btn-group pull-right\"><a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $value->cat_id .
                    "' id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"><img src='" . assets_url_img . "delete.png' alt='Delete' title='Delete'></a></div>";
            $temp_arr[] = 'cat_id_' . $value->cat_id;
            $arra[] = $temp_arr;
            $i++;
        }
        return $arra;
    }

    function add($id) {
        $res = $this->forms_model->selectSingleRecord('form_id', $id);
        if ($res !== FALSE) {
            $data['page_name'] = 'super_admin/forms/categories/add_category';
            $data['title'] = 'Add Forms Categories';
            $res = $this->forms_model->selectSingleRecord('form_id', $id);
            $data['form_details'] = $res[0];
            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', 'Error !! Please Try Again .May Be Forms Not Found.');
            redirect('super_admin/forms', 'refresh');
        }
    }

    function addListener($id) {
        $res = $this->forms_model->selectSingleRecord('form_id', $id);
        if ($res !== FALSE) {
            $obj = new forms_categories_model($id);
            $this->form_validation->set_rules($obj->validation_rules);

            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('cat_name');
                $this->form_validation->set_value('introduction_text');
                $this->add($id);
            } else {
                $obj->form_id = $id;
                $obj->cat_name = $this->input->post('cat_name');
                $obj->sequence = $obj->getNewSequenceId($id);
                $obj->introduction_text = $this->input->post(
                        'introduction_text');

                $check = $obj->dataUpdateSave();
                if ($check == TRUE) {
                    $this->session->set_flashdata('success', 'Category Added Sucessufully');
                } else {
                    $this->session->set_flashdata('error', 'Error in Adding The Category');
                }
                redirect('super_admin/forms/categories/' . $id, 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', 'Error !! Please Try Again from Strach by click on categoires "Click Here" then Add New Category.');
            redirect('super_admin/forms', 'refresh');
        }
    }

    function edit($id) {
        $res = $this->forms_categories_model->selectSingleRecord('cat_id', $id);
        if ($res !== FALSE) {
            $data['page_name'] = 'super_admin/forms/categories/edit_category';
            $data['title'] = 'Edit Forms Categories';
            $data['cat_details'] = $res[0];
            $this->load->view($this->default_template_name, $data);
        } else {
            $this->session->set_flashdata('error', 'Error !! Please Try Again.May Be Category Not Found.');
            redirect('super_admin/forms', 'refresh');
        }
    }

    function editListener($id) {
        $res = $this->forms_categories_model->selectSingleRecord('cat_id', $id);
        if ($res !== FALSE) {
            $obj = new forms_categories_model();
            $validation = $obj->validation_rules;
            unset($validation[0]);
            $rules = $this->form_validation->set_rules($validation);
            $cat_name = $id . ",cat_id," . $res[0]->form_id .
                    ',cat_name,forms_categories_model';
            $this->form_validation->set_rules('cat_name', 'Category Name', 'trim|required|edit_isDataExit_validator[' . $cat_name . ']');

            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('cat_name');
                $this->form_validation->set_value('introduction_text');
                $this->edit($id);
            } else {
                $obj->cat_id = $id;
                $obj->form_id = $res[0]->form_id;
                $obj->cat_name = $this->input->post('cat_name');
                $obj->sequence = $res[0]->sequence;
                $obj->introduction_text = $this->input->post(
                        'introduction_text');

                $check = $obj->dataUpdateSave();
                if ($check == TRUE) {
                    $this->session->set_flashdata('success', 'Category Edited Sucessufully');
                } else {
                    $this->session->set_flashdata('error', 'Error in Editing The Category');
                }
                redirect('super_admin/forms/categories/' . $res[0]->form_id, 'refresh');
            }
        } else {
            $this->session->set_flashdata('error', 'Error !! Please Try Again from Strach by click on categoires "Click Here" then Add New Category.');
            redirect('super_admin/forms', 'refresh');
        }
    }

    function delete($form_id, $cat_id) {
        $check = $this->forms_categories_model->deleteData($cat_id);
        if ($check == TRUE) {
            $this->session->set_flashdata('success', 'Deleted Sucessufully');
        } else {
            $this->session->set_flashdata('error', 'Error in Delete');
        }
        redirect('super_admin/forms/categories/' . $form_id, 'refresh');
    }

    function sortable() {
        $obj = new forms_categories_model();
        $obj->updateAllSequence($_POST['cat_id']);
        return true;
    }

}

?>
