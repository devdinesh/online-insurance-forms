<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class standard_question_answer extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('standard_forms_categories_question_model');
        $this->load->model('standard_forms_categories_model');
        $this->load->model('standard_forms_model');
        $this->load->model('standard_forms_categories_question_answer_model');
        $this->default_template_name = get_mat_super_admin_template_name();
        language_change::setLanguage();
    }

    public function index($question_id) {

        $data['page_name'] = 'super_admin/standard_forms/answer/view_answer';
        $data['title'] = $this->lang->line('list_answer');
        $cat_result = $this->standard_forms_categories_question_model->selectSingleRecord('question_id', $question_id);
        if (empty($cat_result)) {
            $this->session->set_flashdata('error', $this->lang->line('answer_not_found'));
            redirect(base_url() . "super_admin/standard_forms");
        }
        // $frm_id,$cat_id needs for the forms and categories link which is in
        // the view_answer.php page.
        $cat_id = $cat_result[0]->cat_id;

        // getting form id
        // not using get where here as this code would reduce the round trips to
        // database
        $this->standard_forms_categories_question_model->cat_id = $cat_id;
        $frm_result = $this->standard_forms_categories_question_model->get_cartegory();
        $frm_id = $frm_result->form_id;
        $data['frm_id'] = $frm_id;
        // get the question.
        $this->standard_forms_categories_question_answer_model->question_id = $question_id;
        $question = $this->standard_forms_categories_question_answer_model->get_question();
        $data['question'] = $question->question;
        $data['cat_id'] = $cat_id;
        $data['question_id'] = $question_id;
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * Renders json so for answers so that it can be shown
     * in the jquery data table on the list page.
     *
     * @param int $question_id
     *            is the id of question for which we need to render
     *            the question in json format so that it can be shown in the
     *            jquery data table
     */
    public function get_json($question_id) {
        $records = new standard_forms_categories_question_answer_model();
        $records = $records->get_where(array('question_id' => $question_id));
        $array = $this->get_array_for_json($records);

        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    /**
     * converts array of objects to array of array so
     * that it can be used in the get_json method
     *
     * @param unknown_type $objects            
     * @return multitype:multitype:string
     */
    function get_array_for_json($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() .
                    'super_admin/standard_categories_question_answer/edit/' . $value->answer_id .
                    '">' . $value->answer . "</a>";

            if ($value->skip_to_questions !== NULL) {
                if ($value->skip_to_questions !== '0') {
                    $ques_info = $this->standard_forms_categories_question_model->get_where(array('question_id' => $value->skip_to_questions));
                    $que_val = strip_tags($ques_info[0]->question) . ' (' . $value->skip_to_questions . ' )';
                } else {
                    $que_val = $this->lang->line('end_form');
                }
            } else {
                $que_val = '';
            }

            $temp_arr[] = $que_val;

            $arra[] = $temp_arr;
        }
        return $arra;
    }

    /**
     * Renders the edit page.
     *
     * @param int $answer_id
     *            answer id is the primary key
     *            of the record that we want to edit.
     */
    function edit($answer_id) {

        $records = new standard_forms_categories_question_answer_model();
        $records = $records->get_where(array('answer_id' => $answer_id));
        if (empty($records)) {
            $this->session->set_flashdata('error', "'Error !! Please Try Again.May Be Answer Not Found'");
            redirect('super_admin/standard_forms', 'refresh');
        }
        $answer = $records[0];

        // $frm_id,$cat_id and $question_id needs for the forms and
        // categories,questions link which is in
        // the edit_answer.php page.
        $question_id = $answer->question_id;

        $skip_to_question_id = $answer->skip_to_questions;
        $standard_forms_categories_question_model = new standard_forms_categories_question_model();
        $skiped_question = $standard_forms_categories_question_model->get_where(
                array('question_id' => $skip_to_question_id
                ));
        $skiped_cat_question_id = @$skiped_question[0]->cat_id;

        if (isset($skiped_cat_question_id)) {
            $data['skip_cat_id'] = $skiped_cat_question_id;
        }

        $data['question_id'] = $question_id;

        // gettting category id
        // not using get where here as this code would reduce the round trips to
        // database
        $this->standard_forms_categories_question_answer_model->question_id = $question_id;
        $cat_result = $this->standard_forms_categories_question_answer_model->get_question();

        if ($cat_result->answer_kind == 'radio') {
            $data['display_smat_form'] = true;
        } else {
            $data['display_smat_form'] = false;
        }
        $cat_id = $cat_result->cat_id;
        $data['cat_id'] = $cat_id;

        // getting form id
        // not using get where here as this code would reduce the round trips to
        // database
        $this->standard_forms_categories_question_model->cat_id = $cat_id;
        $frm_result = $this->standard_forms_categories_question_model->get_cartegory();
        $frm_id = $frm_result->form_id;
        $data['frm_id'] = $frm_id;

        $data['question_id'] = $question_id;
        $data['answer'] = $answer;
        $data['page_name'] = 'super_admin/standard_forms/answer/edit_answer';
        $data['title'] = $this->lang->line('edit_answer');

        $question = $answer->get_question();

        // question list for the list box "skip to question"
        $data['questions'] = $this->standard_forms_categories_question_model->get_where(
                array('cat_id' => $question->cat_id,
                    'question_id <>' => $answer->question_id
                ));

        $this->load->view($this->default_template_name, $data);
    }

    /**
     * This method is called when user clicks on save button
     * on the edit page.
     */
    function edit_listener() {
        $answer_id = $this->input->post('answer_id');
        $question_id = $this->input->post('question_id');
        $answer = $this->input->post('answer');
        if ($this->input->post('go_to_type') == 'to_end') {
            $skip_to_questions = '0';
        }
        if ($this->input->post('go_to_type') == 'to_question') {
            $skip_to_questions = $this->input->post('question_no');
        }
        if ($this->input->post('go_to_type') == 'to_none') {
            $skip_to_questions = NULL;
        }

        $answers = new standard_forms_categories_question_answer_model();
        $answers = $answers->get_where(array('answer_id' => $answer_id
                ));
        $answer_obj = $answers[0];

        $rules = $answer_obj->validation_rules;
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('answer');
            $this->form_validation->set_value('questions');
            $this->edit($answer_id);
        } else {
            $answer_obj->answer = $answer;
            $answer_obj->skip_to_questions = $skip_to_questions;
            $answer_obj->update();
            $this->session->set_flashdata('success', $this->lang->line('edit_success'));
            redirect(
                    base_url() . "super_admin/standard_categories_question_answer/" .
                    $question_id);
        }
    }

    /**
     * Renders the add answer form.
     *
     * @param int $question_id
     *            is the id of question
     *            to which we want to add the answers
     */
    function add($question_id) {
        $data['page_name'] = 'super_admin/standard_forms/answer/add_answer';
        $data['title'] = $this->lang->line('add_answer');
        $question_category_id = $this->standard_forms_categories_question_model->get_where(array('question_id' => $question_id));

        if (empty($question_category_id)) {
            $this->session->set_flashdata('error', "Error !! Please Try Again.May Be Question Not Found");
            redirect('super_admin/standard_forms', 'refresh');
        }

        if ($question_category_id[0]->answer_kind == 'radio') {
            $data['display_smat_form'] = true;
        } else {
            $data['display_smat_form'] = false;
        }
        $data['questions'] = $this->standard_forms_categories_question_model->get_where(
                array('cat_id' => $question_category_id[0]->cat_id
                ));
        // $frm_id,$cat_id and $question_id needs for the forms and
        // categories,questions and answers link which is in
        // the add_answer.php page.
        // getting category id
        $cat_id = $question_category_id[0]->cat_id;
        $data['cat_id'] = $cat_id;

        // / getting form id
        // not using get where here as this code would reduce the round trips to
        // database
        $this->standard_forms_categories_question_model->cat_id = $cat_id;
        $frm_result = $this->standard_forms_categories_question_model->get_cartegory();
        $frm_id = $frm_result->form_id;
        $data['frm_id'] = $frm_id;

        $data['question_id'] = $question_id;
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * This method is invoked when the user clicks on
     * save button from the add answer page.
     */
    function add_listener() {
        $answer_id = $this->input->post('answer_id');
        $question_id = $this->input->post('question_id');
        $answer = $this->input->post('answer');
        $skip_to_questions = $this->input->post('question_no');
        $answer_obj = new standard_forms_categories_question_answer_model();

        if ($this->input->post('go_to_type') == 'to_end') {
            $skip_to_questions = '0';
        }
        if ($this->input->post('go_to_type') == 'to_question') {
            $skip_to_questions = $this->input->post('question_no');
        }
        if ($this->input->post('go_to_type') == 'to_none') {
            $skip_to_questions = NULL;
        }

        $rules = $answer_obj->validation_rules;
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('answer');
            $this->form_validation->set_value('questions');
            $this->add($question_id);
        } else {
            $answer_obj->answer = $answer;
            $answer_obj->skip_to_questions = $skip_to_questions;
            $answer_obj->question_id = $question_id;
            $answer_obj->save();
            $this->session->set_flashdata('success', $this->lang->line('add_success'));
            redirect(
                    base_url() . "super_admin/standard_categories_question_answer/" .
                    $question_id);
        }
    }

    public function get_edit_question_answer_cat_list_for_edit_page($type) {
        if ($type == 'none') {
            echo "";
        } else if ($type == 'to_question') {
            echo $this->lang->line('select_category');
        } else {
            echo "Invalid Option Entered";
        }
    }

    public function get_edit_question_answer_cat_list_for_edit_page_values(
    $type, $answer_id) {
        if ($type == 'none') {
            echo "";
        } else {
            $this->load->model('standard_forms_categories_question_answer_model');
            $this->load->model('standard_forms_categories_question_model');
            $standard_forms_categories_question_answer_model = new standard_forms_categories_question_answer_model();
            $answers = $standard_forms_categories_question_answer_model->get_where(
                    array('answer_id' => $answer_id
                    ));
            if (count($answers) == 0) {
                $this->session->set_flashdata('error', 'The question was not found');
                redirect(base_url() . "super_admin/answer/edit/" . $answer_id);
            }
            $answer = $answers[0];
            $question = $answer->get_question(); // /question where we wants to
            // add the answer
            $cat_id = $question->cat_id;

            $skip_to_question_id = @$answer->skip_to_questions;
            if (isset($skip_to_question_id)) {
                $standard_forms_categories_question_model = new standard_forms_categories_question_model();
                $skiped_question = $standard_forms_categories_question_model->get_where(
                        array('question_id' => $skip_to_question_id
                        ));
                $skiped_cat_question_id = $skiped_question[0]->cat_id;
            }
            $que_seq = $question->sequence;

            $this->load->model('standard_forms_categories_model');
            $standard_forms_categories_model = new standard_forms_categories_model();
            $categories = $standard_forms_categories_model->get_where(
                    array('cat_id' => $cat_id
                    ));
            $category = $categories[0];
            $cat_seq = $category->sequence;
            $form_id = $category->form_id;

            if ($type == 'to_question') {
                $categories = $standard_forms_categories_model->get_where(
                        array('form_id' => $form_id
                        ));
                $questions_array = array();
                $this->load->model('standard_forms_categories_question_model');
                ?>
                <select id="category"  onchange="getcategoryquestion(this.value);" class="form-control">
                    <option value="nl"><?php echo  $this->lang->line('select_category');?></option>
                    <?php
                    foreach ($categories as $category) {
                        if ($category->cat_id == @$skiped_cat_question_id) {
                            ?>
                            <option
                                value="<?php echo $category->cat_id; ?>" selected="selected"><?php echo $category->cat_name; ?></option>
                                <?php
                            } elseif ($cat_id != $category->cat_id) {
                                if ($cat_seq < $category->sequence) {
                                    ?>
                                <option
                                    value="<?php echo $category->cat_id; ?>"><?php echo $category->cat_name; ?></option>

                                <?php
                            }
                        } else {
                            ?>
                            <option
                                value="<?php echo $category->cat_id; ?>"><?php echo $category->cat_name; ?></option>

                            <?php
                        }
                    }
                    ?>
                </select>
                <?php
            } else {
                echo "Invlid Choice";
            }
        }
    }

    public function get_category_question($cat_id, $answer_id) {
        $this->load->model('standard_forms_categories_question_answer_model');
        $standard_forms_categories_question_answer_model = new standard_forms_categories_question_answer_model();
        $answers = $standard_forms_categories_question_answer_model->get_where(
                array('answer_id' => $answer_id
                ));
        if (count($answers) == 0) {
            $this->session->set_flashdata('error', 'The question was not found');
            redirect(base_url() . "super_admin/answer/edit/" . $answer_id);
        }
        $answer = $answers[0];

        $skip_to_question = $answer->skip_to_questions;

        $this->load->model('standard_forms_categories_question_model');
        $standard_forms_categories_question_model = new standard_forms_categories_question_model();
        $questions = $standard_forms_categories_question_model->get_where(
                array('question_id' => $answer->question_id
                ));

        $question = $questions[0];
        $que_seq = $question->sequence;

        $question_cat_id = $question->cat_id;

        $questions_array = array();
        if ($cat_id == 'nl') {
            $standard_forms_categories_model = new standard_forms_categories_model();
            $categories = $standard_forms_categories_model->get_where(
                    array('cat_id' => $question_cat_id
                    ));
            $category = $categories[0];
            $cat_seq = $category->sequence;
            $form_id = $category->form_id;
            $all_categories = $standard_forms_categories_model->get_where(
                    array('form_id' => $form_id
                    ));
            $questions_array = array();
            foreach ($all_categories as $category) {
                if ($category->sequence >= $cat_seq) {

                    $array = array('cat_id' => $category->cat_id
                    );
                    $standard_forms_categories_question_model = new standard_forms_categories_question_model();
                    $questions = $standard_forms_categories_question_model->get_where(
                            $array);

                    foreach ($questions as $question) {
                        if ($question->cat_id == $question_cat_id) {
                            if ($question->sequence > $que_seq) {
                                $questions_array[$question->question_id] = $question->question;
                            }
                        } else {
                            $questions_array[$question->question_id] = $question->question;
                        }
                    }
                }
            }

            echo form_dropdown('question_no', $questions_array, '', 'class="form-control"', $skip_to_question) . "<label>".$this->lang->line('select_question')."</<label>";
        } else {
            $array = array('cat_id' => $cat_id
            );
            $standard_forms_categories_question_model = new standard_forms_categories_question_model();
            $questions = $standard_forms_categories_question_model->get_where($array);

            $questions_array = array();
            foreach ($questions as $question) {
                if ($question->cat_id == $question_cat_id) {
                    if ($question->sequence > $que_seq) {
                        $questions_array[$question->question_id] = $question->question;
                    }
                } else {
                    $questions_array[$question->question_id] = $question->question;
                }
            }
            echo  form_dropdown('question_no', $questions_array, '', 'class="form-control"', $skip_to_question) . "<label>".$this->lang->line('select_question')."</<label>";
        }
    }

    public function get_category_question_for_add($cat_id, $question_id) {
        $this->load->model('standard_forms_categories_question_model');
        $standard_forms_categories_question_model = new standard_forms_categories_question_model();
        $questions = $standard_forms_categories_question_model->get_where(
                array('question_id' => $question_id
                ));
        $question = $questions[0];
        $que_seq = $question->sequence;
        $question_cat_id = $question->cat_id;
        $questions_array = array();
        if ($cat_id == 'nl') {
            $standard_forms_categories_model = new standard_forms_categories_model();
            $categories = $standard_forms_categories_model->get_where(
                    array('cat_id' => $question_cat_id
                    ));
            $category = $categories[0];
            $cat_seq = $category->sequence;
            $form_id = $category->form_id;
            $all_categories = $standard_forms_categories_model->get_where(
                    array('form_id' => $form_id
                    ));
            $questions_array = array();
            foreach ($all_categories as $category) {
                if ($category->sequence >= $cat_seq) {

                    $array = array('cat_id' => $category->cat_id
                    );
                    $standard_forms_categories_question_model = new standard_forms_categories_question_model();
                    $questions = $standard_forms_categories_question_model->get_where(
                            $array);

                    foreach ($questions as $question) {
                        if ($question->cat_id == $question_cat_id) {
                            if ($question->sequence > $que_seq) {
                                $questions_array[$question->question_id] = $question->question;
                            }
                        } else {
                            $questions_array[$question->question_id] = $question->question;
                        }
                    }
                }
            }

            echo form_dropdown('question_no', $questions_array, '', 'class="form-control"', @$skip_to_question) .
            "<label>".$this->lang->line('select_question')."</<label>";
        } else {
            $array = array('cat_id' => $cat_id
            );
            $standard_forms_categories_question_model = new standard_forms_categories_question_model();
            $questions = $standard_forms_categories_question_model->get_where($array);

            foreach ($questions as $question) {
                if ($question->cat_id == $question_cat_id) {
                    if ($question->sequence > $que_seq) {
                        $temp_arr = array();
                        $questions_array[$question->question_id] = $question->question;
                    }
                } else {
                    $temp_arr = array();
                    $questions_array[$question->question_id] = $question->question;
                }
            }
            echo form_dropdown('question_no', $questions_array, '', 'class="form-control"') . "<label>".$this->lang->line('select_question')."</<label>";
        }
    }

    public function get_edit_question_answer_cat_list_for_add_page_values(
    $type, $question_id) {
        if ($type == 'none') {
            echo "";
        } else {
            // get question object fomr question_id
            // $question_id
            $this->load->model('standard_forms_categories_question_model');
            $standard_forms_categories_question_model = new standard_forms_categories_question_model();
            $questions = $standard_forms_categories_question_model->get_where(
                    array('question_id' => $question_id
                    ));
            $question = $questions[0];
            $que_seq = $question->sequence;
            $cat_id = $question->cat_id;

            $this->load->model('standard_forms_categories_model');
            $standard_forms_categories_model = new standard_forms_categories_model();
            $categories = $standard_forms_categories_model->get_where(
                    array('cat_id' => $cat_id
                    ));
            $category = $categories[0];
            $cat_seq = $category->sequence;
            $form_id = $category->form_id;

            if ($type == 'to_question') {
                $categories = $standard_forms_categories_model->get_where(
                        array('form_id' => $form_id
                        ));
                $questions_array = array();
                $this->load->model('standard_forms_categories_question_model');
                ?>
                <select id="category" class="form-control" onchange="getcategoryquestion(this.value);">
                    <option value="nl"><?php echo $this->lang->line('select_category');?></option>
                    <?php
                    foreach ($categories as $category) {

                        if ($cat_seq <= $category->sequence) {
                            ?>
                            <option
                                value="<?php echo $category->cat_id; ?>"><?php echo $category->cat_name; ?></option>

                            <?php
                        }
                    }
                    ?>
                </select>
                <?php
            } else {
                echo "Invlid Choice";
            }
        }
    }

}

/**
 * Location: application/controllers/admin/standard_question_answer.php
 */