<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class clients extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('clients_model');
        $this->load->model('my_file_upload');
        $this->load->model('client_setting_model');
        $this->load->model('users_model');
        $this->default_template_name = get_mat_super_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
        $data['page_name'] = 'super_admin/clients/index';
        $data['title'] = 'Clients';

        $this->load->view($this->default_template_name, $data);
    }

    /**
     * renders the edit page for the client whos id is passed
     *
     * @param int $id
     *            is the id of client in the database
     */
    function edit($id) {
        $data['page_name'] = 'super_admin/clients/edit_client';
        $data['title'] = 'Edit Client';
        $data['client_id'] = $id;
        $res = $this->clients_model->get_where(
                array('client_id' => $id
                ));
        $res_admin = $this->client_setting_model->get_where(
                array('client_id' => $id
                ));

        if (isset($res_admin[0])) {
            $data['admin'] = $res_admin[0];
        }
        $data['user_records'] = $this->users_model->get_where(array('client_id' => $id, 'role' => 'admin'));
        if (empty($res)) {
            $this->session->set_flashdata('error', "Error !! May Be Client not found");
            redirect(base_url() . "super_admin/clients");
        }
        $data['client'] = $res[0];

        $this->load->view($this->default_template_name, $data);
    }

    /*
     * action listener for edit page
     */

    function editListener($id) {

        // get the object and validations rules
        $client = new clients_model();
        $admin = new client_setting_model();
        $new_rules = array(
            array('field' => 'email', 'label' => 'Email',
                'rules' => 'required|trim|valid_email'
            ),
            array('field' => 'password', 'label' => 'Password',
                'rules' => 'trim'
            ),
            array('field' => 'password_confirmation',
                'label' => 'Password Confiramtion',
                'rules' => 'trim|matches[password]'
            ),
            array('field' => 'smtp_server', 'label' => 'SMTP Server',
                'rules' => 'required|trim|max_length[256]|xss_clean|valid_url_format|url_exists|callback_duplicate_URL_check'
            ),
            array('field' => 'smtp_port', 'label' => 'SMTP Port',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'top_margin', 'label' => 'Top margin',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'bottom_margin', 'label' => 'Bottom margin',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'left_margin', 'label' => 'Left margin',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'right_margin', 'label' => 'Right margin',
                'rules' => 'trim|numeric'
            )
        );
        $rules = array_merge($client->validation_rules, $new_rules);

        // move the validations rule for joining date as it is not there in the
        // table
        unset($rules[4]);
        // file errors if any
        $error = array();

        // try uploadng the file
        // if file is selected then only the upload will be done
        // it would be success if no file was selested
        $logo_upload_status = my_file_upload('logo', $client->logo_upload_config(), $this);

        // if file was uploaded
        if ($logo_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $error = array();
            $error['logo'] = $logo_upload_status['data'];
            $success = false;
            $this->session->set_flashdata('file_errors', $error);
        } else {
            $success = true;
        }
        //pdf template start
        $pdf_template_upload_status = my_file_upload('pdf_template', $admin->pdf_template_upload_config(), $this);

        // if file was uploaded
        if ($pdf_template_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $error = array();
            $error['pdf_template'] = $pdf_template_upload_status['data'];
            $success1 = false;
            $this->session->set_flashdata('file_errors', $error);
        } else {
            $success1 = true;
        }
        //pdf template close
        // if upload was ok
        if ($success == true && $success1 == true) {
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == FALSE) {
                $this->edit($id);
            } else {

                // get the data to be updated
                $client = new clients_model();
                $client = $client->get_where(
                        array('client_id' => $id
                        ));

                // get the first result as where return multiple things
                $client = $client[0];

                // remove the previous file if exists
                if (isset($logo_upload_status['data'])) {
                    if (isset($client->logo) && $client->logo != '') {
                        $path_to_be_removed = substr(
                                $client->logo_upload_path . "/" . $client->logo, 2);
                        unlink($path_to_be_removed);
                    }
                }
                $admin = $admin->get_where(
                        array('client_id' => $id
                        ));

                if (isset($admin[0])) {
                    // get the first result as where return multiple things
                    $admin = $admin[0];
                } else {
                    $admin = new client_setting_model();
                }
                // get the path to new fiel uploded
                if (!isset($logo_upload_status['data'])) {
                    // if no file was chosen then the path would not be changed
                    $logo_upload_status['data'] = $client->logo;
                }
                // remove the previous file if exists
                if (isset($pdf_template_upload_status['data'])) {
                    if (isset($admin->pdf_template) && $admin->pdf_template != '') {
                        $path_to_be_removed = substr(
                                $admin->pdf_template_upload_path . "/" .
                                $admin->pdf_template, 2);
                        @unlink($path_to_be_removed);
                    }
                }

                // get the path to new fiel uploded
                if (!isset($pdf_template_upload_status['data'])) {
                    // if no file was chosen then the path would not be changed
                    $pdf_template_upload_status['data'] = $admin->pdf_template;
                }
                $pdf_template = $pdf_template_upload_status['data'];
                $new_file_path = $logo_upload_status['data'];

                // get all the fields from form
                $client->client_name = $this->input->post('client_name');
                $client->address = $this->input->post('address');
                $client->zip_code = $this->input->post('zip_code');
                $client->city = $this->input->post('city');
                $client->logo = $new_file_path; // set the value to newly
                // uploaded file's path
                $client->title_application = $this->input->post(
                        'title_application');
                $client->path_to_import = $this->input->post('path_to_import');
                $client->current_url = $this->input->post('current_url');
                $client->first_remind_mail = $this->input->post(
                        'first_remind_mail');
                $client->second_remind_mail = $this->input->post(
                        'second_remind_mail');
                $client->status = $this->input->post('status');
                $client->monthly_fee = $this->input->post('monthly_fee');
                $client->amount_forms_month = $this->input->post('amount_forms_month');
                $client->additional_forms_fee = $this->input->post('additional_forms_fee');
                $client->fewer_forms_fee = $this->input->post('fewer_forms_fee');

                if ($this->input->post('enable_newsletters')) {
                    $client->enable_newsletters = $this->input->post('enable_newsletters');
                } else {
                    $client->enable_newsletters = 0;
                }
                $client->monthly_fee_newsletters = $this->input->post('monthly_fee_newsletters');
                $client->fee_per_newsletter = $this->input->post('fee_per_newsletter');
                // update the value to database
                $client->update();
                $admin->client_id = $id;
                $admin->smtp_server = $this->input->post('smtp_server');
                $admin->smtp_port = $this->input->post('smtp_port');
                $admin->email = $this->input->post('email');
                $admin->pdf_template = $pdf_template;
                $admin->top_margin = $this->input->post('top_margin');
                $admin->bottom_margin = $this->input->post('bottom_margin');
                $admin->left_margin = $this->input->post('left_margin');
                $admin->right_margin = $this->input->post('right_margin');
                $admin->automatic_import = $this->input->post('automatic_import');
                if ($this->input->post('main_user')) {
                    $admin->main_user = $this->input->post('main_user');
                } else {
                    $admin->main_user = NULL;
                }
                if (trim($this->input->post('password')) != '') {
                    $admin->password = $this->input->post('password');
                }
                $admin->client_update();

                $this->session->set_flashdata('success', "client was updated successfully");
                redirect(base_url() . "super_admin/clients");
                // print_r($client);
            }
        } else {

            // set tjhe values that user entered
            $this->form_validation->set_value('client_name');
            $this->form_validation->set_value('address');
            $this->form_validation->set_value('zip_code');
            $this->form_validation->set_value('city');
            $this->form_validation->set_value('title_application');
            $this->form_validation->set_value('path_to_import');
            $this->form_validation->set_value('current_url');
            $this->form_validation->set_value('first_remind_mail');
            $this->form_validation->set_value('second_remind_mail');
            $this->form_validation->set_value('smtp_server');
            $this->form_validation->set_value('smtp_port');
            $this->form_validation->set_value('email');
            $this->form_validation->set_value('top_margin');
            $this->form_validation->set_value('bottom_margin');
            $this->form_validation->set_value('left_margin');
            $this->form_validation->set_value('right_margin');
            $this->form_validation->set_value('status');
            $this->form_validation->set_value('monthly_fee');
            $this->form_validation->set_value('amount_forms_month');
            $this->form_validation->set_value('additional_forms_fee');
            $this->form_validation->set_value('fewer_forms_fee');
            $this->form_validation->set_value('enable_newsletters');
            $this->form_validation->set_value('monthly_fee_newsletters');
            $this->form_validation->set_value('fee_per_newsletter');

            // / go to edit if it fails
            $this->edit($id);
        }
    }

    /**
     * a method that renders the json containing list of all the clients
     * that can be understood by the jquery data table
     */
    function getJsonClients() {
        $records = $this->clients_model->get_all();

        $array = $this->_get_array_for_json($records);

        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    /**
     * takes array objects and converts it to an array of array
     * that is appropriate for rendering in json
     *
     * @param unknown_type $objects            
     * @return multitype:multitype:string NULL
     */
    function _get_array_for_json($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $temp_arr = array();
            $temp_arr[] = '<a href="clients/edit/' . $value->client_id . '">' .
                    $value->client_name . '</a>';
            $temp_arr[] = $value->address;
            $temp_arr[] = $value->zip_code;
            $temp_arr[] = $value->city;
            if (isset($value->status) && $value->status != NULL) {
                $temp_arr[] = $value->status;
            } else {
                $temp_arr[] = '-';
            }

//            $temp_arr[] = '<a href="' . base_url() .
//                    'super_admin/client_subscriptions/' . $value->client_id .
//                    '">Subscriptions</a>';

            $temp_arr[] = '<a href="javascript:;"  onclick="deleteRow(this)" id="' . $value->client_id . '"  class="deletepage"  data-toggle="tooltip" data-placement="top" title="Delete"><i class="md md-delete"></i></a>';

            $arra[] = $temp_arr;
        }
        return $arra;
    }

    /**
     * renders the add form
     */
    function add($message = '') {
        $data['page_name'] = 'super_admin/clients/add';
        $data['title'] = 'Add Client';
        $data['message'] = $message;
        $this->load->view($this->default_template_name, $data);
    }

    function add_listener() {

        // get the object and validations rules
        $admin = new client_setting_model();
        $client = new clients_model();
        $new_rules = array(
            array('field' => 'email', 'label' => 'Email',
                'rules' => 'required|trim|valid_email'
            ),
            array('field' => 'password', 'label' => 'Password',
                'rules' => 'trim'
            ),
            array('field' => 'password_confirmation',
                'label' => 'Password Confiramtion',
                'rules' => 'trim|matches[password]'
            ),
            array('field' => 'smtp_server', 'label' => 'SMTP Server',
                'rules' => 'required|trim|max_length[256]|xss_clean|valid_url_format|url_exists|callback_duplicate_URL_check'
            ),
            array('field' => 'smtp_port', 'label' => 'SMTP Port',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'top_margin', 'label' => 'Top margin',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'bottom_margin', 'label' => 'Bottom margin',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'left_margin', 'label' => 'Left margin',
                'rules' => 'trim|numeric'
            ),
            array('field' => 'right_margin', 'label' => 'Right margin',
                'rules' => 'trim|numeric'
            )
        );
        $rules = array_merge($client->validation_rules, $new_rules);

        // move the validations rule for joining date as it is not there in the
        // table
        unset($rules[4]);

        // file errors if any
        $error = array();

        // try uploadng the file
        // if file is selected then only the upload will be done
        // it would be success if no file was selested
        $logo_upload_status = my_file_upload('logo', $client->logo_upload_config(), $this);

        // if file was uploaded
        if ($logo_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $error = array();
            $error['logo'] = $logo_upload_status['data'];
            $success = false;
            $this->session->set_flashdata('file_errors', $error);
        } else {
            $success = true;
        }
        //pdf template start
        $pdf_template_upload_status = my_file_upload('pdf_template', $admin->pdf_template_upload_config(), $this);

        // if file was uploaded
        if ($pdf_template_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $success1 = false;

            $this->session->set_flashdata('pdf_template_error', $pdf_template_upload_status['data']);
            //redirect(base_url() . "admin/admin_settings");
        } else {
            $success1 = true;
        }
        //pdf template close
        // if upload was ok
        if ($success == true && $success1 == true) {
            $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == FALSE) {
                // set tjhe values that user entered
                $this->form_validation->set_value('client_name');
                $this->form_validation->set_value('address');
                $this->form_validation->set_value('zip_code');
                $this->form_validation->set_value('city');
                $this->form_validation->set_value('title_application');
                $this->form_validation->set_value('path_to_import');
                $this->form_validation->set_value('current_url');
                $this->form_validation->set_value('first_remind_mail');
                $this->form_validation->set_value('second_remind_mail');
                $this->form_validation->set_value('smtp_server');
                $this->form_validation->set_value('smtp_port');
                $this->form_validation->set_value('email');
                $this->form_validation->set_value('top_margin');
                $this->form_validation->set_value('bottom_margin');
                $this->form_validation->set_value('left_margin');
                $this->form_validation->set_value('right_margin');
                $this->form_validation->set_value('status');
                $this->form_validation->set_value('monthly_fee');
                $this->form_validation->set_value('amount_forms_month');
                $this->form_validation->set_value('additional_forms_fee');
                $this->form_validation->set_value('fewer_forms_fee');
                $this->form_validation->set_value('enable_newsletters');
                $this->form_validation->set_value('monthly_fee_newsletters');
                $this->form_validation->set_value('fee_per_newsletter');

                $this->add($logo_upload_status);
            } else {

                // get the data to be updated
                $client = new clients_model();

                // get the path to new fiel uploded
                if (!isset($logo_upload_status['data'])) {
                    // if no file was chosen then the path would not be changed
                    $logo_upload_status['data'] = $client->logo;
                }
                $new_file_path = $logo_upload_status['data'];
                if (!isset($logo_upload_status['data'])) {
                    // if no file was chosen then the path would not be changed
                    $logo_upload_status['data'] = $client->logo;
                }
                if (!isset($pdf_template_upload_status['data'])) {
                    // if no file was chosen then the path would not be changed
                    $pdf_template_upload_status['data'] = $admin->pdf_template;
                }

                $pdf_template = $pdf_template_upload_status['data'];

                $new_file_path = $logo_upload_status['data'];

                // get all the fields from form
                $client->client_name = $this->input->post('client_name');
                $client->address = $this->input->post('address');
                $client->zip_code = $this->input->post('zip_code');
                $client->city = $this->input->post('city');
                $client->logo = $new_file_path; // set the value to newly
                // uploaded file's path
                $client->title_application = $this->input->post(
                        'title_application');
                $client->path_to_import = $this->input->post('path_to_import');
                $client->current_url = $this->input->post('current_url');
                $client->first_remind_mail = $this->input->post(
                        'first_remind_mail');
                $client->second_remind_mail = $this->input->post(
                        'second_remind_mail');
                $client->status = $this->input->post('status');
                $client->monthly_fee = $this->input->post('monthly_fee');
                $client->amount_forms_month = $this->input->post('amount_forms_month');
                $client->additional_forms_fee = $this->input->post('additional_forms_fee');
                $client->fewer_forms_fee = $this->input->post('fewer_forms_fee');
                if ($this->input->post('enable_newsletters')) {
                    $client->enable_newsletters = $this->input->post('enable_newsletters');
                } else {
                    $client->enable_newsletters = 0;
                }
                $client->monthly_fee_newsletters = $this->input->post('monthly_fee_newsletters');
                $client->fee_per_newsletter = $this->input->post('fee_per_newsletter');

                // update the value to database
                $new_id = $client->save();
                if (isset($new_id) && $new_id != '' && $new_id != 0) {
                    $admin = new client_setting_model();
                    $admin->client_id = $new_id;
                    $admin->smtp_server = $this->input->post('smtp_server');
                    $admin->smtp_port = $this->input->post('smtp_port');
                    $admin->email = $this->input->post('email');
                    $admin->pdf_template = $pdf_template;
                    $admin->top_margin = $this->input->post('top_margin');
                    $admin->bottom_margin = $this->input->post('bottom_margin');
                    $admin->left_margin = $this->input->post('left_margin');
                    $admin->right_margin = $this->input->post('right_margin');
                    $admin->automatic_import = 0;
                    if (trim($this->input->post('password')) != '') {
                        $admin->password = $this->input->post('password');
                    }
                    $admin->save();
                } $this->session->set_flashdata('success', "Client was added successfully");
                redirect(base_url() . "super_admin/clients");
                // print_r($client);
            }
        } else {
            $this->form_validation->set_value('client_name');
            $this->form_validation->set_value('address');
            $this->form_validation->set_value('zip_code');
            $this->form_validation->set_value('city');
            $this->form_validation->set_value('title_application');
            $this->form_validation->set_value('path_to_import');
            $this->form_validation->set_value('current_url');
            $this->form_validation->set_value('first_remind_mail');
            $this->form_validation->set_value('second_remind_mail');
            $this->form_validation->set_value('smtp_server');
            $this->form_validation->set_value('smtp_port');
            $this->form_validation->set_value('email');
            $this->form_validation->set_value('top_margin');
            $this->form_validation->set_value('bottom_margin');
            $this->form_validation->set_value('left_margin');
            $this->form_validation->set_value('right_margin');
            $this->form_validation->set_value('status');
            $this->form_validation->set_value('monthly_fee');
            $this->form_validation->set_value('amount_forms_month');
            $this->form_validation->set_value('additional_forms_fee');
            $this->form_validation->set_value('fewer_forms_fee');
            $this->form_validation->set_value('enable_newsletters');
            $this->form_validation->set_value('monthly_fee_newsletters');
            $this->form_validation->set_value('fee_per_newsletter');
            $this->add($logo_upload_status);
        }
    }

    /**
     * deletes the client whos id is passed.
     * also unlinks the
     * logo associated with it
     *
     * @param int $id
     *            is the id from database which you want to delete
     */
    function delete($id) {
        $res = $this->clients_model->get_where(
                array('client_id' => $id
                ));
        $client = $res[0];
        $path_to_be_removed = substr($client->logo_upload_path . "/" . $client->logo, 2);
        unlink($path_to_be_removed);
        $client->delete();
        $this->session->set_flashdata('success', "Client was deleted successfully");
        redirect(base_url() . "super_admin/clients");
    }

}

?>
