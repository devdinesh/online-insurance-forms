<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Controller to help maintain user details
 */
class menus extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('menu_model');
        $this->load->model('admin_texts_model');
        $this->default_template_name = get_super_admin_template_name();
    }

    /**
     * renders list page
     */
    public function index() {
        $data['page_name'] = 'super_admin/admin/menu/view';
        $data['title'] = 'Menus';
        $this->load->view($this->default_template_name, $data);
    }

    function add() {
        $data['menus'] = $this->menu_model->get_all();
        $data['pages'] = $this->admin_texts_model->getAll();
        $data['page_name'] = 'super_admin/admin/menu/add';
        $data['title'] = 'Add Menu';
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * this will handel all the data post by the add method form.
     */
    function addListener() {
        $obj = new menu_model();
        $validation = $obj->validation_rules;
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('title');
            $this->form_validation->set_value('parent');
            $this->form_validation->set_value('page');
            $this->form_validation->set_value('special_feature');
            $this->add();
        } else {
            $obj->title = $this->input->post('title');
            if ($this->input->post('parent') && $this->input->post('parent') != '') {
                $obj->parent = $this->input->post('parent');
            } else {
                $obj->parent = NULL;
            }
            $obj->page = $this->input->post('page');
            if ($this->input->post('special_feature') && $this->input->post('special_feature') != '') {
                $obj->special_feature = $this->input->post('special_feature');
            } else {
                $obj->special_feature = NULL;
            }
            $obj->save();
            $this->session->set_flashdata('success', "Menu added sucessfully");
            redirect('super_admin/admin/menus', 'refresh');
        }
    }

    function edit($id) {
        $data['menus'] = $this->menu_model->get_where(array('id != ' => $id));
        $data['pages'] = $this->admin_texts_model->getAll();
        $data['page_name'] = 'super_admin/admin/menu/edit';
        $data['title'] = 'Edit Menu';
        $data['id'] = $id;
        $res = $this->menu_model->get_where(array('id' => $id));
        if (empty($res)) {
            $this->session->set_flashdata('error', "Error!! May Be menu Not Found");
            redirect(base_url() . "super_admin/admin/menus");
        }
        $data['menu_detail'] = $res[0];
        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
        $obj = new menu_model();
        $validation = $obj->validation_rules;
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('title');
            $this->form_validation->set_value('parent');
            $this->form_validation->set_value('page');
            $this->form_validation->set_value('special_feature');
            $this->edit($id);
        } else {
            $obj->id = $id;
            $obj->title = $this->input->post('title');
            if ($this->input->post('parent') && $this->input->post('parent') != '') {
                $obj->parent = $this->input->post('parent');
            } else {
                $obj->parent = NULL;
            }
            $obj->page = $this->input->post('page');
            if ($this->input->post('special_feature') && $this->input->post('special_feature') != '') {
                $obj->special_feature = $this->input->post('special_feature');
            } else {
                $obj->special_feature = NULL;
            }
            $obj->update();
            $this->session->set_flashdata('success', "Menu edited sucessfully");
            redirect('super_admin/admin/menus', 'refresh');
        }
    }

    function getJson() {
        $records = $this->menu_model->get_all();
        $array = $this->getArrayForJson($records);
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function getArrayForJson($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $page = $this->admin_texts_model->get_wher(array('text_id' => $value->page));
            if (isset($value->parent) && $value->parent != NULL) {
                $parent = $this->menu_model->get_where(array('id' => $value->parent));
                $parent_title = $parent[0]->title;
            } else {
                $parent_title = '';
            }
            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() . 'super_admin/menus/edit/' . $value->id . '">' .
                    $value->title . '</a>';
            $temp_arr[] = $page[0]->text_title;
            $temp_arr[] = $parent_title;
            $temp_arr[] = $value->special_feature;
            $temp_arr[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $value->id . "'><img src='" . base_url() .
                    "assets/images/icon_delete.png' alt='Delete' title='Delete'></a>";
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function deleteListener($id) {
        $this->menu_model->id = $id;
        $this->menu_model->delete();
        $this->session->set_flashdata('success', "Menu deleted sucessfully");
        redirect('super_admin/admin/menus', 'refresh');
    }

}

