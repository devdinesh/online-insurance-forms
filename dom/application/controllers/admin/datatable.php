<?php

/*
 * 
 * Author: DINESH
 * 19-12-2014
 * 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Datatable extends CI_Controller {

    var $aColumns;
    var $eColumns;
    var $sIndexColumn;
    var $sTable;
    var $rResult;
    var $output;
    var $myWhere;
    var $groupBy;
    var $sOrder;
    var $sEcho;

    public function __construct() {
        parent::__construct();
        $this->myWhere = "";
    }

    /*
     * 
     * Edit Button : '.$this->lang->line("Edit").'
     * Delete Button : '.$this->lang->line("Delete").'
     * Delete Link : <a href="javascript:;" class="btn btn-xs red" onclick="delete_data(this)" id="' . $aRow['id'] . '"><i class="fa fa-trash-o"></i> ' . $this->lang->line("Delete") . '</a>'
     * 
     */

    // GET THE LIST OF IMPORTED TEMPLATES DETAILS .

    function get_import_log() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $this->aColumns = array('ins_import_log.file_name,ins_import_log.file_location,ins_import_log.date,ins_import_log.time,ins_import_log.claim_id,ins_clients.client_name');
        $this->eColumns = array('ins_import_log.import_log_id,ins_import_log.client_id');
        $this->sIndexColumn = "ins_import_log.import_log_id";
        $this->sTable = "ins_import_log
             LEFT JOIN ins_clients ON ins_clients.client_id = ins_import_log.client_id
               ";
        $this->myWhere = " Where ins_import_log.client_id = '" . $client_id . "' ORDER BY ins_import_log.import_log_id desc";

        $this->datatable_process();
        foreach ($this->rResult->result_array() as $aRow) {
            $row = array();
            $row[] = $aRow['client_name'];
            $row[] = $aRow['file_name'];
            $row[] = $aRow['file_location'];
            $row[] = date('d-m-Y',  strtotime($aRow['date']));
            $row[] = $aRow['time'];
            if ($aRow['claim_id'] != NULL) {
                $status = '<span class="label label-success">Imported</span>';
            } else {
                $status = '<span class="label label-important">Rejected</span>';
            }
            $row[] = $status;
            $row[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $aRow['import_log_id'] . "'><img src='" . base_url() .
                    "assets/images/icon_delete.png' alt='Delete' title='Delete'></a>";

            $this->output['aaData'][] = $row;
        }
        echo json_encode($this->output);
        exit();
    }

    /*
     * Datastable core process
     */

    public function datatable_process() {
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
        }

        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {



                    if (stripos($this->aColumns[intval($_GET['iSortCol_' . $i])], "AS") > 0) {
                        $fiel_explode = explode(" AS ", $this->aColumns[intval($_GET['iSortCol_' . $i])]);
                        $sOrder .= $fiel_explode[0] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
                    } else
                        $sOrder .= $this->aColumns[intval($_GET['iSortCol_' . $i])] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
                }
            }
            $sOrder = substr_replace($sOrder, "", - 2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        } else {
            if ($this->sOrder != '') {
                $sOrder = $this->sOrder;
            }
        }

        $sWhere = $this->myWhere;

        if (isset($_GET['sSearch'])) {

            if ($_GET['sSearch'] != "") {

                $sWhere .= ($sWhere == "") ? " WHERE (" : " AND (";

                for ($i = 0; $i < count($this->aColumns); $i++) {

                    if ($_GET['bSearchable_' . $i] == "true") {

                        if (stripos($this->aColumns[$i], "AS") > 0) {

                            $fiel_explode = explode(" AS ", $this->aColumns[$i]);

                            $sWhere .= $fiel_explode[0] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR ";
                            $sWhere .= $fiel_explode[0] . " LIKE '%" . mysql_real_escape_string(strtolower($_GET['sSearch'])) . "%' OR ";
                            $sWhere .= $fiel_explode[0] . " LIKE '%" . mysql_real_escape_string(strtoUpper($_GET['sSearch'])) . "%' OR ";
                            $sWhere .= $fiel_explode[0] . " LIKE '%" . mysql_real_escape_string(ucfirst($_GET['sSearch'])) . "%' OR ";
                            $sWhere .= $fiel_explode[0] . " LIKE '%" . mysql_real_escape_string(ucwords($_GET['sSearch'])) . "%' OR ";
                        } else {

                            $sWhere .= $this->aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR ";
                            $sWhere .= $this->aColumns[$i] . " LIKE '%" . mysql_real_escape_string(strtolower($_GET['sSearch'])) . "%' OR ";
                            $sWhere .= $this->aColumns[$i] . " LIKE '%" . mysql_real_escape_string(strtoUpper($_GET['sSearch'])) . "%' OR ";
                            $sWhere .= $this->aColumns[$i] . " LIKE '%" . mysql_real_escape_string(ucfirst($_GET['sSearch'])) . "%' OR ";
                            $sWhere .= $this->aColumns[$i] . " LIKE '%" . mysql_real_escape_string(ucwords($_GET['sSearch'])) . "%' OR ";
                        }
                    }
                }

                $sWhere = substr_replace($sWhere, "", - 3);

                $sWhere .= ')';
            }
        }


        /*
         * Group By
         */
        $sGroupBy = $this->groupBy;

        /*
         * Individual column filtering
         */
        /* for($i = 0;$i < count($this->aColumns);$i++){

          if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" ){

          if($sWhere == ""){
          $sWhere = "WHERE ";
          }else{
          $sWhere .= " AND ";
          }
          $sWhere .= $this->aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
          }
          } */

        $sQuery = "
		SELECT SQL_CALC_FOUND_ROWS " . str_replace(" , ", " ", implode(", ", $this->aColumns)) . "," . str_replace(" , ", " ", implode(", ", $this->eColumns)) . "
		FROM   $this->sTable
		$sWhere
		$sGroupBy
		$sOrder
		$sLimit
		";

        $this->rResult = $this->db->query($sQuery);
        //echo $this->db->last_query();

        /*
         * Data set length after filtering
         */
        $iFilteredTotal = $this->rResult->num_rows();

        /*
         * Total data set length
         */
        $sQuery = "
		SELECT COUNT(" . $this->sIndexColumn . ") AS count
		FROM   $this->sTable
		$sWhere
		$sGroupBy
		";
        if ($sGroupBy != null) {
            $rResultTotal = $this->db->query($sQuery);
            $iTotal = $rResultTotal->num_rows();
        } else {
            $rResultTotal = $this->db->query($sQuery);
            $aResultTotal = $rResultTotal->row();
            $iTotal = $aResultTotal->count;
        }
        /*
         * Output
         */
        $this->output = array("iTotalRecords" => $iTotal, "iTotalDisplayRecords" => $iTotal, // $iFilteredTotal,
            "aaData" => array());
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */