<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class manages import file in admin
 */
class import_menually extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->helper('path');
        $this->load->helper('sending_mail');
        $this->load->model('admin_model');
        $this->load->model('forms_model');
        $this->load->model('claims_model');
        $this->load->model('client_setting_model');
        $this->load->model('policy_holder_model');
        $this->load->model('users_model');
        $this->load->model('mail_template_model');
        $this->load->model('form_field_model');
        $this->load->model('form_field_values_model');
        $this->load->model('ins_import_log_model');
        $this->load->model('client_email_template_model');
        $this->default_template_name = get_admin_template_name();
    }

    public function index() {
        
    }

    function searchArray($search, $array) {
        foreach ($array as $key => $value) {
            if (stristr($value, $search)) {
                return substr($value, strlen($search));
            }
        }
        return false;
    }

    /**
     * this method is called when we call the menually import forms
     */
    function import_listener() {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $form_id = $this->input->post('manual_form');
        $email = $this->input->post('manual_mail_address');

        $policy_number = null;
        $behandler = null;
        $schadenummer = null;
        $claim_id = NULL;
        $policy_holder_id = NULL;

//----get email field for the form -- ----
        $obj_forms_fiels = new form_field_model();
        $email_filed = $obj_forms_fiels->getMailFiledFromID($form_id);

        $behandler = $obj_forms_fiels->getBehandlerFiledFromID($form_id);

        //behindler Emial start
        $behandler_email = $obj_forms_fiels->getBehandlerEmailFromID($form_id);


//----get form details for the form -- ----
        $obj_forms = new forms_model();
        $get_form_details = $obj_forms->get_where(array('form_id' => $form_id));

        if (!empty($get_form_details)) {
            $poliy_holders = new policy_holder_model();
            $get_poliy_holders = $poliy_holders->get_where(array('email' => $email, 'client_id' => $client_id));
            if (( is_array($get_poliy_holders) && count($get_poliy_holders) == 1)) {
                $policy_holder_id = $get_poliy_holders[0]->policy_holder_id;
            } else {
                $randid = $this->generateRandStr(10);
                $this->policy_holder_model->email = $email;
                $this->policy_holder_model->client_id = $client_id;
                $this->policy_holder_model->status = "A";
                $this->policy_holder_model->password_reset_random_string = $randid;
                $policy_holder_id = $this->policy_holder_model->save();
            }
            $claim_sequence_number = $this->claims_model->generateNewClaimSequenceNumber($client->client_id);
            $admin_session_detials = $this->session->userdata('admin');
            $this->claims_model->import_date = date('d-m-Y', time()); // strtotime(date("d-m-Y"));
            $this->claims_model->kind = NULL;
            $this->claims_model->userid = $admin_session_detials['user_id'];
            $this->claims_model->handler = $admin_session_detials['first_name'] . ' ' . $admin_session_detials['last_name'];
            $this->claims_model->schadenummer = NULL;
            $this->claims_model->handler_email = $admin_session_detials['mail_address'];
            $this->claims_model->form_id = $form_id;
            $this->claims_model->mail_send_date = NULL;
            $this->claims_model->status = 'Nieuw';
            $this->claims_model->policy_holder_id = $policy_holder_id;
            $this->claims_model->client_id = $client->client_id;
            $this->claims_model->is_delete = 0;
            $this->claims_model->claim_sequence_number = $claim_sequence_number;
            $claim_id = $this->claims_model->save();

            //----add form field value .....          
            $obj_filed_value = new form_field_values_model();
            $obj_filed_value->name = $email_filed->field_name;
            $obj_filed_value->name_at_form = $email_filed->name_on_form;
            $obj_filed_value->filed_value = $email;
            $obj_filed_value->claim_id = $claim_id;
            $check = $obj_filed_value->save();

            if ($behandler) {
                $obj_filed_value = new form_field_values_model();
                $obj_filed_value->name = $behandler->field_name;
                $obj_filed_value->name_at_form = $behandler->name_on_form;
                $obj_filed_value->filed_value = $admin_session_detials['first_name'] . ' ' . $admin_session_detials['last_name'];
                $obj_filed_value->claim_id = $claim_id;
                $check = $obj_filed_value->save();
            }
            if ($behandler_email) {
                $obj_filed_value = new form_field_values_model();
                $obj_filed_value->name = $behandler_email->field_name;
                $obj_filed_value->name_at_form = $behandler_email->name_on_form;
                $obj_filed_value->filed_value = $admin_session_detials['mail_address'];
                $obj_filed_value->claim_id = $claim_id;
                $check = $obj_filed_value->save();
            }
            $send_link = $this->send_link_for_without_login_fillup($claim_id);
            $this->session->set_flashdata('success', ' Formulier ' . $claim_sequence_number . ' is verstuurd aan.');
        } else {
            $this->session->set_flashdata('error', 'Selected Form is not exist.');
        }
        redirect(base_url() . "admin");
    }

    function generateRandStr($length) {
        $randstr = "";
        for ($i = 0; $i < $length; $i++) {
            $randnum = mt_rand(0, 61);
            if ($randnum < 10) {
                $randstr .= chr($randnum + 48);
            } else if ($randnum < 36) {
                $randstr .= chr($randnum + 55);
            } else {
                $randstr .= chr($randnum + 61);
            }
        }
        return $randstr;
    }

    function send_link_for_without_login_fillup($claim_id) {
        // get the claim for claim id
        $admin_session_detials = $this->session->userdata('admin');
        $claims = $this->claims_model->get_where(array('claim_id' => $claim_id));
        $claim_info = $claims[0];
        $form_id = $claim_info->form_id;

        // get the information of the forms related to the claim.
        $form_info = $this->forms_model->selectSingleRecord('form_id', $form_id);

        $fill_in_needed = $form_info[0]->fill_in_needed;
        // $first_remind = $form_info[0]->first_remind;
        // $second_remind = $form_info[0]->second_remind;
        // first check the reminder if reminder not set then change the status
        // and create the pdf of the form and send the link.

        if ($fill_in_needed == "0") {

            $this->create_pdf_for_forms($form_id, $claim_id);

            $claims_model = new claims_model();

            $claim_info->status = "Afgerond";

            $claim_info->update();
            return TRUE;
        }

        // if the claim was not found then set flash with error
        // and redirect to the base url
        if (count($claims) == 0) {
            $this->session->set_flashdata('error', 'The claim was not found. Could not send the link.');
            // redirect(base_url() . "admin");
            // only writing base_url will redirect to admin so flash would be
            // lost so apped admin at the end
            return FALSE;
        }

        // get the email for the policy holder
        $claim = $claims[0];

        $policy_holder = $claim->get_policy_holder();
        $policy_holder_email = $policy_holder->email;
        $policy_holder_email = strtolower($policy_holder_email); // lower case
        // the email if
        // reuquired
        // get the
        // client id so
        // that we can
        // get the
        // email
        // template
        // associated
        // with it

        $client_id = $claim->client_id;
        $admin_setting_info = $this->client_setting_model->get_where(
                array('client_id' => $client_id
                ));

        $smtp_user = (string) $admin_setting_info[0]->email;
        $smtp_pass = (string) $admin_setting_info[0]->password;
        $smtp_server = (string) $admin_setting_info[0]->smtp_server;
        $smtp_port = $admin_setting_info[0]->smtp_port;
        //get client logo
        $client_info = $this->clients_model->get_where(array('client_id' => $client_id));
        $client_logo = $admin_setting_info[0]->mail_logo;
        $logo_path = base_url() . 'assets/upload/mail_logo/' . $client_logo;
        $logo_exist = 'assets/upload/mail_logo/' . $client_logo;
        if (isset($client_logo) && $client_logo !== '') {
            if (file_exists($logo_exist)) {
                $type = pathinfo($logo_exist, PATHINFO_EXTENSION);
                $data = file_get_contents($logo_exist);
                $base64 = base_url() . 'assets/upload/mail_logo/' . $client_logo;
                //$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                $logo_url = '<img src="' . $base64 . '" border="0" style="display: block; max-width: 200px;">';
            } else {
                $logo_url = '';
            }
        } else {
            $logo_url = '';
        }
        //end client logo

        $this->load->model('client_email_template_model');
        $client_email_templates = new client_email_template_model();

        $obj_policy_holder = new policy_holder_model();
        $policy_holder_record = $obj_policy_holder->get_where(
                array('policy_holder_id' => $claim_info->policy_holder_id
                ));

        $randid = $this->generateRandStr(10);
        $obj_policy_holder->password_reset_random_string = $randid;
        $obj_policy_holder->policy_holder_id = $policy_holder_record[0]->policy_holder_id;
        $obj_policy_holder->updatePasswordRandomString();
//         if ($policy_holder_record[0]->first_name != '') {}
        $policy_first_name = 'test name';
        if ($policy_first_name != '') {
            if (isset($form_info[0]->mail_new_form) && $form_info[0]->mail_new_form != NULL) {
                $client_email_templates = $client_email_templates->get_where(
                        array('client_id' => $client_id, 'email_id' => $form_info[0]->mail_new_form));
            } else {
                $client_email_templates = $client_email_templates->get_where(
                        array('client_id' => $client_id, 'email_type' => 'form link'
                        ));
            }
            // if the template was not found
            if (count($client_email_templates) == 0) {
                //$this->session->set_flashdata('error', 'The mail template with name "form link" was not found for sending email.');
                return FALSE;
                //redirect(base_url() . "admin");
            }
            $template = $client_email_templates[0];
            $message = $template->email_message; // get the message
            $message = str_replace('&lt;name&gt;', $policy_holder->first_name, $message);
            $message = str_replace('&lt;logo&gt;', $logo_url, $message);
            $message = str_replace('../../../../', base_url(), $message);
            // get the policy holder name in the message
            $link = '<a href="' . base_url() .
                    'user/authenticate/login_with_link/' . $randid .
                    '">Link</a> ';
            // constructing the link with random string
            $message = str_replace('&lt;link&gt;', $link, $message);
            $message = str_replace('&lt;voornaam&gt;', $admin_session_detials['first_name'], $message);
            $message = str_replace('&lt;tussenvoegsel&gt;', $admin_session_detials['middle_name'], $message);
            $message = str_replace('&lt;achternaam&gt;', $admin_session_detials['last_name'], $message);
            $message = str_replace('&lt;bedrijf&gt;', $admin_session_detials['company'], $message);
            // set the link in the message string load helper and send email
        }

        $email_attachment = NULL;
        if (isset($template->email_attachment) && $template->email_attachment != '') {
            $attached_file = FCPATH . "assets/upload/client_email_attachments/" . $template->email_attachment;
            if (file_exists($attached_file)) {
                $email_attachment = $attached_file;
            }
        }
        $this->load->helper('daynamic_sending_mail');
        $sent_msg = dynamic_send_mail($policy_holder_email, $template->email_subject, $message, '', $email_attachment, $smtp_user, $smtp_pass, $smtp_server, $smtp_port, $admin_session_detials['mail_address']);
        // if email sending was successful then set the success message and then
        // redirect to admin page
        if ($sent_msg == TRUE) {
            $this->claims_model->claim_id = $claim->claim_id;
            $this->claims_model->status = 'open';
            $this->claims_model->mail_send_date = get_current_time()->get_date_for_db();
            $this->claims_model->update_mail_send_date();
            return TRUE;
            //redirect(base_url() . "admin");
        } else {
            // show_error($this->email->print_debugger());
            $this->session->set_flashdata('error', 'Check Email Setting!');
            return FALSE;
            // redirect(base_url() . "admin");
        }
    }

}

?>
