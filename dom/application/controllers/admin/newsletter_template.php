<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the admin settings and other
 * methods.
 */
class newsletter_template extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();

        $this->load->model('newsletter_template_model');
        $this->load->helper('lgin');
        $this->load->model('client_setting_model');
        $this->default_template_name = get_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $res = $this->client_setting_model->get_where(
                array('client_id' => $client_id
                ));
        $data['default_rows'] = $res[0]->default_rows;

        $data['page_name'] = 'admin/newsletter_template/index';
        $data['title'] = 'Nieuwsbrief sjablonen';

        $this->load->view($this->default_template_name, $data);
    }

    function edit($id) {
        $result = $this->newsletter_template_model->get_where(array('email_id' => $id));
        if (empty($result)) {
            $this->session->set_flashdata('error', "Error Please Try Again.Mail Template not found");
            redirect(base_url() . 'admin/newsletter_template');
        }
        $template = $result[0];
        $data['page_name'] = 'admin/newsletter_template/edit_email';
        $data['title'] = 'Nieuwsbrief bewerken templates';
        $data['email_id'] = $id;
        $data['email_template'] = $template;
        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
        $result = $this->newsletter_template_model->get_where(
                array('email_id' => $id
                ));
        $template = $result[0];
        // get the validations
        $validation[] = $template->validation_rules[2];
        $validation[] = $template->validation_rules[3];
        $validation[] = $template->validation_rules[4];

        $this->form_validation->set_rules($validation);

        // try to run the validations
        // if validations run safely then save otherwise set success to
        // false
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('email_name');
            $this->form_validation->set_value('email_subject');
            $this->form_validation->set_value('email_message');
            $this->edit($id);
        } else {
            // now get the values from the form and set in the form
            // get all the fields from form
            $template->email_name = $this->input->post('email_name');
            $template->email_subject = $this->input->post('email_subject');
            $template->email_message = $this->input->post('email_message');
            $template->update(); // save the values
            $this->session->set_flashdata('success', 'Template updated Sucessufully');
            redirect(base_url() . 'admin/newsletter_template');
        }
    }

    function getJson() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $json_data = $this->_transform_for_json(
                $this->newsletter_template_model->get_where(
                        array('client_id' => $client_id
                )));

        $data['aaData'] = $json_data;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function _transform_for_json($data) {
        $return_val = array();
        foreach ($data as $row) {

            $row_array = array();
            $client = $row->get_client();

            $client_name = $client->client_name;
            $template_id = $row->email_id;
            $email_type = $row->email_type;
            $email_subject = $row->email_subject;

            $row_array[] = "<a href=\"" . base_url() .
                    "admin/newsletter_template/edit/" . $row->email_id . "\" >" .
                    $row->email_name . "</a>";
            $row_array[] = $email_subject;

            $row_array[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $row->email_id . "'><img src='" . base_url() . "assets/images/icon_delete.png' alt='Delete' title='Delete'></a>";

            $return_val[] = $row_array;
        }

        return $return_val;
    }

    function add() {
        $data['page_name'] = 'admin/newsletter_template/add_email';
        $data['title'] = 'Voeg nieuwsbrief templates';
        $this->load->view($this->default_template_name, $data);
    }

    function addListener() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $template = new newsletter_template_model();
        // get the validations
        $validation[] = $template->validation_rules[2];
        $validation[] = $template->validation_rules[3];
        $validation[] = $template->validation_rules[4];
        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('email_name');
            $this->form_validation->set_value('email_subject');
            $this->form_validation->set_value('email_message');
            $this->add();
        } else {
            $template->email_type = $this->generateRandomString();
            $template->email_name = $this->input->post('email_name');
            // now get the values from the form and set in the form
            // get all the fields from form
            $template->email_subject = $this->input->post('email_subject');
            $template->email_message = $this->input->post('email_message');
            $template->client_id = $client_id;
            $template->email_dynamic_fields = '<introduction_text> <articles> <closure_text>';
            $template->save(); // save the values
            $this->session->set_flashdata('success', 'Template added Sucessufully');
            redirect(base_url() . 'admin/newsletter_template');
        }
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function delete($id) {
        $this->newsletter_template_model->email_id = $id;
        $this->newsletter_template_model->delete($id);
        $this->session->set_flashdata('success', 'Record deleted Sucessufully.');
        redirect('admin/newsletter_template', 'refresh');
    }

}

?>
