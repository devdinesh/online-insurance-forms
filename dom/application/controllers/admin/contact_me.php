<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class contact_me extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('mail_template_model');
        $this->load->model('super_admin_setting_model');

        $this->default_template_name = get_admin_template_for_not_logged_in();
    }

    // load the view here
    public function index() {
        $data['page_name'] = 'admin/contact_me_form';
        $data['title'] = 'Contact Page';
        $data['redirect_to'] = 'admin/';

        $this->load->model('admin_texts_model');
        $admin_texts_model = new admin_texts_model();
        $texts = $admin_texts_model->get_wher(
                array('text_title' => 'Contact'
                ));
        if (count($texts) != 0) {
            $text = $texts[0];
        } else {
            $text = "Default text needs to be set, session not set.";
        }

        $data['text'] = $text;

        $this->load->view($this->default_template_name, $data);
    }

    public function validation() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('company_name', 'Voornaam', 'trim|required');
        $this->form_validation->set_rules('name', 'Naam', 'required|trim');
        $this->form_validation->set_rules('person_email', 'E-mailadres', 'trim|required|valid_email');

        $this->form_validation->set_rules('telefoon', 'telefoon', 'required|trim|numeric');
        //$this->form_validation->set_rules('zipcode', 'Postcode', 'required|trim');
        $this->form_validation->set_rules('comment', 'Opmerkingen', 'required|trim');
        //$this->form_validation->set_rules('E-mailadres', 'email', 'trim|valid_email');
        // $this->form_validation->set_rules('website', 'Website', 'trim|max_length[256]|xss_clean|prep_url|valid_url_format|url_exists|callback_duplicate_URL_check');

        if ($this->form_validation->run() == false) {
            
        } else {
            return TRUE;
        }
    }

    // when the user click on the contact me on the form
    // / this function should be called
    // string replace function for email sending message..
    public function replace_all($string) {
        // $string = str_replace('&lt;name&gt;', $this->input->post('f_name'), $string);
        $string = str_replace('&lt;name&gt;', $this->input->post('name'), $string);
        $string = str_replace('&lt;address&gt;', $this->input->post('person_email'), $string);
        $string = str_replace('&lt;company name&gt;', $this->input->post('company_name'), $string);
        $string = str_replace('&lt;telephone&gt;', $this->input->post('telefoon'), $string);
        $string = str_replace('&lt;comment&gt;', $this->input->post('comment'), $string);
        // $string = str_replace('&lt;city&gt;', $this->input->post('city'), $string);
        /* $string = str_replace('&lt;mail address&gt;', 
          $this->input->post('company_email'), $string);
          $string = str_replace('&lt;website&gt;', $this->input->post('website'),
          $string); */
        return $string;
    }

    public function contact_me_function() {
        // /echo phpinfo();
        if ($this->validation() == TRUE) {

            // send email to the super admin

            $msg_template = $this->mail_template_model->get_where(
                    array('email_id' => 1
                    )); // 1
            // is
            // the
            // id
            // of
            // the
            // template
            // with
            // name
            // contact
            // (super
            // admin)
            $msg_template['mail_msg'] = $msg_template;
            $mail_message = $msg_template['mail_msg'][0]->email_message;
            $subject = $msg_template['mail_msg'][0]->email_subject;
            $attechment = './assets/admin_assets/email_attachments/' .
                    $msg_template['mail_msg'][0]->email_attachment;

            $org = $this->replace_all($mail_message);
            $super_admin_setting_model = new super_admin_setting_model();
            $settings = $super_admin_setting_model->get_where(array());

            $super_admin_email = $settings[0]->email;

            $this->load->helper('sending_mail');
            $sent_msg = send_mail($super_admin_email, $subject, $org, '', $attechment);
            if ($sent_msg == TRUE) {
                // echo"your data is sucessfully send";
                $this->session->set_flashdata('success', 'The contact form is send. We will get in touch with you.');
            } else {
                show_error($this->email->print_debugger());
            }

            // send email to the person who wants to contact the admin

            $msg_template = $this->mail_template_model->get_where(
                    array('email_id' => 4
                    )); // 4
            // is
            // the
            // id
            // of
            // the
            // form
            // with
            // name
            // Contact
            // (client)
            $msg_template['mail_msg'] = $msg_template;
            $mail_message = $msg_template['mail_msg'][0]->email_message;
            $subject = $msg_template['mail_msg'][0]->email_subject;
            $attechment = './assets/admin_assets/email_attachments/' .
                    $msg_template['mail_msg'][0]->email_attachment;
            $org = $this->replace_all($mail_message);
            $admin_settings = $this->super_admin_setting_model->get_where(
                    array());
            $tomailid = $admin_settings[0]->email;

            $this->load->helper('sending_mail');
            $sent_msg = send_mail($this->input->post('person_email'), $subject, $org, '', $attechment); // semd
            // the
            // same
            // email
            // to
            // the
            // person
            // also
            if ($sent_msg == TRUE) {
                // echo"your data is sucessfully send";
                $this->session->set_flashdata('success', 'The contact form is send. We will get in touch with you.');
                redirect(base_url());
            } else {
                show_error($this->email->print_debugger());
            }
        } else {
            redirect(base_url());
        }
    }

    public function add_contact_details($string) {
        $string = str_replace('&lt;name&gt;', $this->input->post('name'), $string);
        $string = str_replace('&lt;address&gt;', $this->input->post('email_address'), $string);
        $string = str_replace('&lt;company name&gt;', $this->input->post('company_name'), $string);
        $string = str_replace('&lt;telephone&gt;', $this->input->post('phone_number'), $string);
        $string = str_replace('&lt;comment&gt;', $this->input->post('remark'), $string);
        return $string;
    }

    function save_contact() {

        $this->load->model('contact_form_model');
        $contact = new contact_form_model();
        $rules = $contact->validation_rules;
        $this->form_validation->set_rules($rules);
        $data['success'] = FALSE;
        $data['message'] = NULL;
        if ($this->form_validation->run() == FALSE) {
            $data['success'] = FALSE;
            $data['message'] = '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a> Please enter the valid Values.</div>';
        } else {
            $contact->name = $this->input->post('name');
            $contact->company_name = $this->input->post('company_name');
            $contact->email_address = $this->input->post('email_address');
            $contact->phone_number = $this->input->post('phone_number');
            $contact->remark = $this->input->post('remark');
            $contact->save();
            // send mail to the super admin  ..

            $msg_template = $this->mail_template_model->get_where(
                    array('email_id' => 1
                    ));


            $msg_template['mail_msg'] = $msg_template;
            $mail_message = $msg_template['mail_msg'][0]->email_message;
            $subject = $msg_template['mail_msg'][0]->email_subject;
            if (isset($msg_template['mail_msg'][0]->email_attachment) && $msg_template['mail_msg'][0]->email_attachment != NULL) {
                $attechment = './assets/admin_assets/email_attachments/' .
                        $msg_template['mail_msg'][0]->email_attachment;
            } else {
                $attechment = NULL;
            }

            $org = $this->add_contact_details($mail_message);

            $super_admin_setting_model = new super_admin_setting_model();
            $settings = $super_admin_setting_model->get_where(array());

            $super_admin_email = $settings[0]->email;
            $success = TRUE;
            $this->load->helper('sending_mail');
            
            $sent_msg = send_mail($super_admin_email, $subject, $org, '', $attechment);
            if ($sent_msg == TRUE) {
                // echo"your data is sucessfully send";
                $success = TRUE;
            } else {
                $success = FALSE;
                show_error($this->email->print_debugger());
            }

            ///
            if ($success) {
                $data['success'] = TRUE;
                $data['message'] = '<div class="alert alert-success"><a class="close" data-dismiss="alert" href="#" >x</a> Contact added sucesfully.</div>';
            } else {
                $data['error'] = TRUE;
                $data['message'] = '<div class="alert alert-danger"><a class="close" data-dismiss="alert" href="#" >x</a> Error ! Please try again later.</div>';
            }
        }
        echo json_encode($data);
        exit;
    }

}

?>
