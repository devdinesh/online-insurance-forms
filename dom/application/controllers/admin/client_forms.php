<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the admin and process the login and other
 * methods.
 */
class client_forms extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('forms_model');
        $this->load->helper('time_worked');
        $this->load->model('client_setting_model');
        $this->load->model('claims_model');
        $this->load->model('form_field_values_model');
        $this->load->model('form_field_model');
        $this->load->model('forms_answers_model');
        $this->load->model('forms_categories_question_model');
        $this->load->model('forms_categories_question_answer_model');
        $this->load->library('image_lib');
        $this->load->library('fpdf/fpdf');
        $this->load->helper('lgin');
        // /temp models.
        $this->load->model('temp_forms_answers_model');
        $this->load->model('temp_forms_answers_details_model');

        $this->default_template_name = get_admin_template_name();
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *        START COMMON FUNCTION WHICH CAN BE USED IN ANY FORM SHOW         * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    function index() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $res = $this->client_setting_model->get_where(
                array('client_id' => $client_id
                ));
        $data['default_rows'] = $res[0]->default_rows;
        $data['page_name'] = 'admin/forms/list_forms_view';
        $data['title'] = 'Overzicht formulieren';
        $this->load->view($this->default_template_name, $data);
    }

    function getJson() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $records = $this->forms_model->get_where(
                array('client_id' => $client_id
                ));
        $array = $this->get_array_for_json($records);

        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function get_array_for_json($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $temp_arr = array();
            $temp_arr[] = '<a href="client_forms/edit/' . $value->form_id . '">' .
                    $value->form_name . '</a>';
            $temp_arr[] = $value->form_tag;
            $temp_arr[] = "<a href=\"" . base_url() . "admin/categories/" .
                    $value->form_id . "\">Klik Hier </a>";
            $temp_arr[] = "<a href=\"" . base_url() . "admin/form_fields/" .
                    $value->form_id . "\">Klik Hier </a>";
            $temp_str = " <div class = \"btn-group pull-right\">
                        <a href=\"client_forms/copyForm/" . $value->form_id .
                    "\" id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Kopieer formulier\"><i class=\"icon-big-random link-button-with-icon \"></i></a>";

            $temp_str .= " <a target=\"_blank\" href=\"" . base_url() .
                    "admin/form_pdf/" . $value->form_id . "\" id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Maak een PDF bestand\"><i class=\"icon-big-pdf link-button-with-icon\"></i></a>
                             ";
            $temp_str .= "<a href=\"" . base_url() .
                    "admin/client_forms/view_form/" . $value->form_id .
                    "\" id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Toon preview\" ><i class=\"icon-big-eye link-button-with-icon \"></i></a>
                        <a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $value->form_id . "' id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Verwijder formulier\"><i class=\"icon-big-bin link-button-with-icon \"></i></a>
                    </div>";
            $temp_arr[] = $temp_str;

            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function delete($id) {

        $this->forms_model->form_id = $id;
        $check = $this->forms_model->Delete_all_forms_details();
        if ($check == TRUE) {
            $this->session->set_flashdata('success', 'Formulier is verwijderd');
        } else {
            $this->session->set_flashdata('error', 'Error in Delete');
        }
        redirect('admin/client_forms', 'refresh');
    }

    function add() {
        $data['page_name'] = 'admin/forms/add_form';
        $data['title'] = 'Toevoegen formulier';

        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $data['client_details'] = $this->clients_model->get_where(
                array('client_id' => $client_id
                ));

        $this->load->view($this->default_template_name, $data);
    }

    function addListener() {
        $obj = new forms_model();
        $validation = $obj->validation_rules;
        unset($validation[0]);
        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == FALSE) {
            // $this->form_validation->set_value ( 'client_id' );
            $this->form_validation->set_value('form_name');
            $this->form_validation->set_value('form_tag');
            $this->form_validation->set_value('header_text');
            $this->form_validation->set_value('introduction_text');
            $this->form_validation->set_value('closure_text');
            $this->form_validation->set_value('fill_in_needed');
            $this->form_validation->set_value('f_reminder');
            $this->form_validation->set_value('s_reminder');
            $this->form_validation->set_value('f_reminder_text');
            $this->form_validation->set_value('s_reminder_text');
            $this->form_validation->set_value('show_all_question_at_once');
            $this->form_validation->set_value('select_for_policyholder');
            $this->form_validation->set_value('mail_new_form');

            $this->add();
        } else {
            $rand_code = random_string('alpha', 5);
            $get_client_id = admin_get_client_id();
            $client_id = $get_client_id->client_id;
            $obj->client_id = $client_id;
            $obj->form_code = $rand_code;
            $obj->fill_in_needed = $this->input->post('fill_in_needed');
            $obj->first_remind = $this->input->post('f_reminder');
            $obj->second_remind = $this->input->post('s_reminder');
            $obj->f_reminder_text = $this->input->post('f_reminder_text');
            $obj->s_reminder_text = $this->input->post('s_reminder_text');
            if ($this->input->post('select_for_policyholder') == '1') {
                $obj->select_for_policyholder = '1';
            } else {
                $obj->select_for_policyholder = '0';
            }

            $obj->form_name = $this->input->post('form_name');
            $obj->form_tag = $this->input->post('form_tag');
            $obj->header_text = $this->input->post('header_text');
            $obj->introduction_text = $this->input->post('introduction_text');
            $obj->closure_text = $this->input->post('closure_text');
            $obj->show_all_question_at_once = $this->input->post(
                    'show_all_question_at_once');
            $obj->current_date = get_current_time()->get_date_for_db();
            $mail_new_form = $this->input->post('mail_new_form');
            if (isset($mail_new_form) && $mail_new_form != '') {
                $obj->mail_new_form = $mail_new_form;
            } else {
                $obj->mail_new_form = NULL;
            }
            $check = $obj->dataUpdateSave();
            if ($check == TRUE) {
                $this->session->set_flashdata('success', 'Form Added Sucessufully');
            } else {
                $this->session->set_flashdata('error', 'Error in Adding The Form');
            }
            redirect('admin/client_forms', 'refresh');
        }
    }

    function edit($id) {
        $data['page_name'] = 'admin/forms/edit_form';
        $data['title'] = 'Details formulier';
        $data['form_id'] = $id;

        $data['client_details'] = $this->clients_model->get_all();
        $res = $this->forms_model->selectSingleRecord('form_id', $id);
        if (empty($res)) {
            //$this->session->set_flashdata('error', "Fout!  Probeer het nog eens. Geen corresponderend formulier gevonden");
            redirect('admin/client_forms', 'refresh');
        }
        $data['form_details'] = $res[0];

        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
        $obj = new forms_model();
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $validation = $obj->validation_rules;
        unset($validation[0]);
        unset($validation[1]);
        unset($validation[2]);
        $rules = $this->form_validation->set_rules($validation);
        $form_name = $id . ",form_id," . $client_id . ",form_name,forms_model";
        $form_tag = $id . ",form_id," . $client_id . ",form_tag,forms_model";
        $this->form_validation->set_rules('form_name', 'Form Name', 'trim|required|edit_isDataExit_validator[' . $form_name . ']');
        $this->form_validation->set_rules('form_tag', 'Form Tag', 'trim|required|edit_isDataExit_validator[' . $form_tag . ']');

        if ($this->form_validation->run() == FALSE) {
            // $this->form_validation->set_value ( 'client_id' );
            $this->form_validation->set_value('form_name');
            $this->form_validation->set_value('form_tag');
            $this->form_validation->set_value('header_text');
            $this->form_validation->set_value('introduction_text');
            $this->form_validation->set_value('closure_text');
            $this->form_validation->set_value('fill_in_needed');
            $this->form_validation->set_value('f_reminder');
            $this->form_validation->set_value('s_reminder');
            $this->form_validation->set_value('f_reminder_text');
            $this->form_validation->set_value('s_reminder_text');
            $this->form_validation->set_value('show_all_question_at_once');
            $this->form_validation->set_value('select_for_policyholder');
            $this->form_validation->set_value('mail_new_form');
            $this->edit($id);
        } else {
            $obj->form_id = $id;
            $obj->client_id = $client_id;
            $obj->form_name = $this->input->post('form_name');
            $obj->form_tag = $this->input->post('form_tag');
            $obj->fill_in_needed = $this->input->post('fill_in_needed');
            $obj->first_remind = $this->input->post('f_reminder');
            $obj->second_remind = $this->input->post('s_reminder');
            $obj->f_reminder_text = $this->input->post('f_reminder_text');
            $obj->s_reminder_text = $this->input->post('s_reminder_text');
            if ($this->input->post('select_for_policyholder') == '1') {
                $obj->select_for_policyholder = '1';
            } else {
                $obj->select_for_policyholder = '0';
            }
            $obj->header_text = $this->input->post('header_text');
            $obj->introduction_text = $this->input->post('introduction_text');
            $obj->closure_text = $this->input->post('closure_text');
            $obj->show_all_question_at_once = $this->input->post(
                    'show_all_question_at_once');
            $mail_new_form = $this->input->post('mail_new_form');
            if (isset($mail_new_form) && $mail_new_form != '') {
                $obj->mail_new_form = $mail_new_form;
            } else {
                $obj->mail_new_form = NULL;
            }

            $check = $obj->dataUpdateSave();
            if ($check && $check == 1) {
                $this->session->set_flashdata('success', 'Edited the Form Sucessufully');
            }
            redirect('admin/client_forms', 'refresh');
        }
    }

    function copyForm($id) {
        $obj = new forms_model();
        $res = $this->forms_model->selectSingleRecord('form_id', $id);

        if ($res !== FALSE) {
            $last_form_name = $this->forms_model->getLastCopiedFormName(
                    $res[0]->form_name);

            $rand_code = random_string('alpha', 5);

            // save the new form
            $obj->client_id = $res[0]->client_id;
            $obj->form_code = $rand_code;
            $obj->fill_in_needed = $res[0]->fill_in_needed;
            $obj->form_name = $res[0]->form_name . '_Copy_' . ( $last_form_name );
            $obj->form_tag = $res[0]->form_tag . '_Copy_' . ( $last_form_name );
            $obj->header_text = $res[0]->header_text;
            $obj->introduction_text = $res[0]->introduction_text;
            $obj->show_all_question_at_once = $res[0]->show_all_question_at_once;
            $obj->closure_text = $res[0]->closure_text;
            $obj->current_date = get_current_time()->get_date_for_db();

            $last_nserted_id = $obj->dataUpdateSave();

            $this->load->model('forms_categories_model');
            $this->load->model('forms_categories_question_model');

            // get the categories that belog to the origina l form
            $categories = $this->forms_categories_model->selectMoreRecord('form_id', $id);
            if (!empty($categories)) {
                foreach ($categories as $category) {

                    $old_cat_id = $category->cat_id;

                    /* insert a new category */
                    unset($category->cat_id); // unset the value so that we can add with new form id
                    $category->form_id = $last_nserted_id; // set the form id to last inserted id
                    $category->dataUpdateSave(); // insert a new record for category
                    $last_inserted_category_id = $this->db->insert_id();
                    // get the questions
                    $questions = $this->forms_categories_question_model->get_where(
                            array('cat_id' => $old_cat_id
                            ));

                    // iterate thru the questions and add them as well

                    if (count($questions) > 0) { // if there were answers
                        foreach ($questions as $question) {
                            $old_question_id = $question->question_id;
                            unset($question->question_id);
                            $question->cat_id = $last_inserted_category_id;
                            $question->dataUpdateSave();
                            $last_inserted_question_id = $this->db->insert_id();

                            /* get all the answers for original question */
                            $answers = $this->forms_categories_question_model->getAnswers(
                                    $old_question_id);

                            // if there wre answers
                            if (is_array($answers) && count($answers) > 0) {

                                // get an array containing answers
                                $new_answers = array();
                                foreach ($answers as $answer) {
                                    $new_answers[] = $answer->answer;
                                }
                                // add the answers to the newly inserted
                                // question
                                $this->forms_categories_question_model->saveAnswers(
                                        $last_inserted_question_id, $new_answers);
                            }
                        }
                    }
                }
            }

            if ($last_nserted_id) {
                $this->session->set_flashdata('success', 'Copied The Form Sucessufully');
            } else {
                $this->session->set_flashdata('error', 'Error in Copying The Form');
            }
            redirect('admin/client_forms', 'refresh');
        }
    }

    function _preview_form_validate_form_post($form_id, $category_id) {

        /**
         * $validatin_success Variable that maintains whether form was
         * validated or not
         */
        $validatin_success = true;
        $this->load->model('forms_categories_model');
        $get_all_cat_info = $this->forms_categories_model->get_where(
                array('form_id' => $form_id
                ));
        if (!empty($get_all_cat_info)) {
            foreach ($get_all_cat_info as $cat_info) {

                // get all the questions in the given category
                $this->load->model('forms_categories_question_model');
                $questions = $this->forms_categories_question_model->selectMoreRecord(
                        'cat_id', $cat_info->cat_id);
                if (!empty($questions)) {
                    // iterate thru all of them and validate them
                    foreach ($questions as $question) {
                        // if it is checkbox
                        if ($question->answer_kind == 'checkbox') {
                            if ($question->required == 1) {
                                $checkbox_validated = $this->_validate_checkbox_group_required(
                                        $question);
                                // if none of the checkboxes were checked
                                if ($checkbox_validated != true) {

                                    $this->form_validation->set_rules(
                                            $question->question_id . "_", ' answer of this ', 'required');

                                    if ($this->form_validation->run() == FALSE) {

                                        $validatin_success = false;
                                        $this->form_validation->set_value(
                                                $question->question_id . "_");
                                    }
                                }
                            }
                        } else {
                            if ($question->required == 1) {
                                $this->form_validation->set_rules(
                                        $question->question_id . "_", ' answer of this ', 'required');
                                if ($this->form_validation->run() == FALSE) {

                                    $validatin_success = false;
                                    $this->form_validation->set_value(
                                            $question->question_id . "_");
                                } else {
                                    
                                }
                            }

                            // if the form validation was successful
                            // then check if the question no to go to needs to
                            // be
                            // set

                            if ($question->answer_kind == 'radio') {
                                $answer_selected_id = $this->input->post(
                                        $question->question_id . "_");
                                $forms_categories_question_answer_model = new forms_categories_question_answer_model();
                                $answers = $forms_categories_question_answer_model->get_where(
                                        array(
                                            'answer_id' => $this->input->post(
                                                    $question->question_id .
                                                    "_")
                                        ));
                                if (count($answers) > 0) {
                                    $answer = $answers[0];
                                    // if skip to categor is there

                                    if (isset($answer->skip_to_category)) {
                                        $this->skip_to_category = $answer->skip_to_category;
                                    } else if (isset($answer->skip_to_questions)) {
                                        $this->skip_to_questions = $answer->skip_to_questions;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $validatin_success;
    }

    function _preview_category_validate_form_post($form_id, $category_id, $sequence = null) {

        /**
         * $validatin_success Variable that maintains whether form was
         * validated or not
         */
        $validatin_success = true;
        $this->load->model('forms_categories_model');
        $get_all_cat_info = $this->forms_categories_model->get_where(
                array('form_id' => $form_id, 'cat_id' => $category_id
                ));
        if (!empty($get_all_cat_info)) {
            foreach ($get_all_cat_info as $cat_info) {

                // get all the questions in the given category
                $obj_cat = new forms_categories_model();
                $get_cat = $obj_cat->get_where(
                        array('form_id' => $form_id, 'cat_id' => $category_id
                        ));
                if (isset($sequence) && $sequence != null) {
                    $this->load->model('forms_categories_question_model');
                    $obj_ques = new forms_categories_question_model();
                    $get_ques = $obj_ques->get_where(
                            array('cat_id' => $get_cat[0]->cat_id,
                                'sequence >' => $sequence
                            ));

                    if (count($get_ques) > 0) {
                        $questions = $get_cat[0]->get_question_if_smart(
                                $sequence, false);
                    } else {
                        $questions = $get_cat[0]->get_question_if_smart(
                                $sequence, true);
                    }
                } else {
                    $questions = $get_cat[0]->get_question_smart();
                }

                if (!empty($questions)) {
                    // iterate thru all of them and validate them
                    foreach ($questions as $question) {
                        // if it is checkbox
                        if ($question->answer_kind == 'checkbox') {
                            if ($question->required == 1) {
                                $checkbox_validated = $this->_validate_checkbox_group_required(
                                        $question);
                                // if none of the checkboxes were checked
                                if ($checkbox_validated != true) {

                                    $this->form_validation->set_rules(
                                            $question->question_id . "_", ' answer of this ', 'required');

                                    if ($this->form_validation->run() == FALSE) {

                                        $validatin_success = false;
                                        $this->form_validation->set_value(
                                                $question->question_id . "_");
                                    }
                                }
                            }
                        } else if ($question->answer_kind == 'date') {

                            if ($question->required == 1) {
                                $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                            } else {
                                $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                            }
                            if ($this->form_validation->run() == FALSE) {
                                $validatin_success = false;
                                $this->form_validation->set_value($question->question_id . "_");
                            }
                        } else if ($question->answer_kind == 'number') {
                            //regex_match[/^[0-9]+[\.,][0-9]{0,2}$/]
                            if ($question->required == 1) {
                                $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^[0-9]\d*(\,\d{0,2})?$/]');
                            } else {
                                $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^[0-9]\d*(\.\d{0,2})?$/]');
                            }
                            if ($this->form_validation->run() == FALSE) {
                                $validatin_success = false;
                                $this->form_validation->set_value($question->question_id . "_");
                            }
                        } else {
                            if ($question->required == 1) {
                                $this->form_validation->set_rules(
                                        $question->question_id . "_", ' answer of this ', 'required');
                                if ($this->form_validation->run() == FALSE) {

                                    $validatin_success = false;
                                    $this->form_validation->set_value(
                                            $question->question_id . "_");
                                } else {
                                    
                                }
                            }

                            // if the form validation was successful
                            // then check if the question no to go to needs to
                            // be
                            // set

                            if ($question->answer_kind == 'radio') {
                                $answer_selected_id = $this->input->post(
                                        $question->question_id . "_");
                                $forms_categories_question_answer_model = new forms_categories_question_answer_model();
                                $answers = $forms_categories_question_answer_model->get_where(
                                        array(
                                            'answer_id' => $this->input->post(
                                                    $question->question_id .
                                                    "_")
                                        ));
                                if (count($answers) > 0) {
                                    $answer = $answers[0];
                                    // if skip to categor is there

                                    if (isset($answer->skip_to_category)) {
                                        $this->skip_to_category = $answer->skip_to_category;
                                    } else if (isset($answer->skip_to_questions)) {
                                        $this->skip_to_questions = $answer->skip_to_questions;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $validatin_success;
    }

    function _validate_checkbox_group_required($question) {
        $checkbox_group_validation_passed = false;
        $checkbox_count = (int) $this->input->post(
                        $question->question_id . '_count');
        // check if its required
        if ($question->required == 1) {

            $checkbox_group_validation_passed = false;
            // iterate thru all the checkboxes
            for ($i = 1; $i <= $checkbox_count; $i++) {
                $checkbox_name = $question->question_id . "_" . $i;

                // $this->form_validation->set_rules('77_[]', '',
                // 'required|xss_clean');
                // if checked
                if ($this->input->post($checkbox_name)) {
                    $checkbox_group_validation_passed = true;
                }
            }
        }

        return $checkbox_group_validation_passed;
    }

    function go_to_question($form_id, $question_id, $previous_category_id) {
        $forms_categories_question_model = new forms_categories_question_model();
        $questions = $forms_categories_question_model->get_where(
                array('question_id' => $question_id
                ));

        // check if form exists with given id
        $forms_model = new forms_model();
        $forms = $forms_model->get_where(array('form_id' => $form_id
                ));

        if (count($forms) == 0) {
            $this->session->set_flashdata('error', 'Form not found');
            redirect('admin/client_forms', 'refresh');
        }

        // check if question exists with given id
        if (count($questions) == 0) {
            $this->session->set_flashdata('error', 'Question not found');
            redirect('admin/client_forms', 'refresh');
        }

        $this->_add_question_to_sequence($question_id);
        $data['sequence'] = '';
        if (isset($this->sequence)) {
            $data['sequence'] = $this->sequence;
        }

        $data['question'] = $questions[0];
        $data['form'] = $forms[0];
        $data['page_name'] = 'admin/forms/preview_question';
        $data['title'] = 'Preview Form';
        $data['previous_category_id'] = $previous_category_id;
        $this->load->view($this->default_template_name, $data);
    }

    function go_to_question_lietener($form_id, $question_id, $previous_category_id) {
        if ($this->input->post('action') == 'Terug') {
            $sequence = $this->input->post('sequence');

            $go_to = $this->_get_last_category_or_question($sequence);

            if ($go_to->go_to_type == 'Q' && $go_to->go_to_value == $question_id) {
                $sequence = $this->_remove_last_go_to_item($sequence);
                $this->sequence = $sequence;
                $go_to = $this->_get_last_category_or_question($sequence);
            }

            if (isset($go_to->go_to_type)) {
                if ($go_to->go_to_type == 'C') {

                    // $sequence = $this->_remove_last_go_to_item($sequence);
                    // $sequence = $this->_remove_last_go_to_item($sequence);
                    $this->sequence = $sequence;

                    if (isset($go_to->go_to_value)) {
                        $this->previous_category = $go_to->go_to_value;
                        $this->sequence = $sequence;
                        $this->_render_preview_form($form_id, $go_to->go_to_value);
                    } else {
                        $this->_render_preview_form($form_id, '');
                    }
                } else if ($go_to->go_to_type == 'Q') {
                    $sequence = $this->_remove_last_go_to_item($sequence);
                    $this->sequence = $sequence;
                    $go_to = $this->_get_last_category_or_question($sequence);
                    $this->go_to_question($form_id, $go_to->go_to_value, 0);
                } else {
                    echo "tempering with form is done. invalid go to value";
                }
            }
        } else {
            // check if form exists with given id
            $forms_model = new forms_model();
            $forms = $forms_model->get_where(
                    array('form_id' => $form_id
                    ));

            if (count($forms) == 0) {
                $this->session->set_flashdata('error', 'Form not found');
                redirect('admin/client_forms', 'refresh');
            }

            $form = $forms[0];

            $forms_categories_question_model = new forms_categories_question_model();
            $questions = $forms_categories_question_model->get_where(
                    array('question_id' => $question_id
                    ));

            // check if question exists with given id
            if (count($questions) == 0) {
                $this->session->set_flashdata('error', 'Question not found');
                redirect('admin/client_forms', 'refresh');
            }

            $question = $questions[0];

            // if it is checkbox

            $validatin_success = $this->_validate_question($question);
            // if validations fail then we need to
            // show the same page again
            if ($validatin_success == false) {
                $this->sequence = $this->input->post('sequence');
                $this->go_to_question($form_id, $question_id, $previous_category_id);
            } else {
                $this->sequence = $this->input->post('sequence');

                // if skip to categor is there
                if (isset($answer->skip_to_category)) {
                    $this->_render_preview_form($form_id, $answer->skip_to_category);
                } else if (isset($answer->skip_to_questions)) {
                    $this->go_to_question($form_id, $answer->skip_to_questions, $previous_category_id);
                } else {
                    // go to the next categry of previous category
                    $category_obj = new forms_categories_model();

                    $next_category_id = $category_obj->get_next_category(
                            $form_id, $question->cat_id);
                    if (count($next_category_id) > 0) {
                        $cat_id = $next_category_id[0]->cat_id;
                        $this->_render_preview_form($form_id, $cat_id);
                    } else {
                        $this->_render_preview_form($form_id, 'end');
                    }
                }
            }
        }
    }

    function _validate_question($question) {
        $validatin_success = TRUE;
        if ($question->answer_kind == 'checkbox') {
            if ($question->required == 1) {
                $checkbox_validated = $this->_validate_checkbox_group_required(
                        $question);
                // if none of the checkboxes were checked
                if ($checkbox_validated != true) {

                    $this->form_validation->set_rules(
                            $question->question_id . "_", ' answer of this ', 'required');

                    if ($this->form_validation->run() == FALSE) {

                        $validatin_success = false;
                        $this->form_validation->set_value(
                                $question->question_id . "_");
                    }
                }
            }
        } else {
            if ($question->required == 1) {
                $this->form_validation->set_rules($question->question_id . "_", ' answer of this ', 'required');
                if ($this->form_validation->run() == FALSE) {

                    $validatin_success = false;
                    $this->form_validation->set_value(
                            $question->question_id . "_");
                } else {
                    
                }
            }

            // if the form validation was successful
            // then check if the question no to go to needs to be
            // set

            if ($question->answer_kind == 'radio') {
                $answer_selected_id = $this->input->post(
                        $question->question_id . "_");
                $forms_categories_question_answer_model = new forms_categories_question_answer_model();
                $answers = $forms_categories_question_answer_model->get_where(
                        array(
                            'answer_id' => $this->input->post(
                                    $question->question_id . "_")
                        ));
                $answer = $answers[0];
            }
        }

        return $validatin_success;
    }

    function create_pdf_for_forms1($form_id) {
        // require(APPPATH.'libraries/fpdf/fpdf.php');
//The forms and client start 
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $form_info = $this->forms_model->get_where(
                array('form_id' => $form_id, 'client_id' => $client_id
                ));
        $intro_text = @$form_info[0]->introduction_text;
        $intro_text = str_replace('<pre>', '', $intro_text);
        $intro_text = str_replace('</pre>', '', $intro_text);
        $closer_text = @$form_info[0]->closure_text;
        $closer_text = str_replace('<pre>', '', $closer_text);
        $closer_text = str_replace('</pre>', '', $closer_text);
        if (empty($form_info)) {
            $this->session->set_flashdata('error', 'Form not found');
            redirect('admin/client_forms', 'refresh');
        }
        $records = $this->clients_model->get_where(
                array('client_id' => $form_info[0]->client_id
                ));
//The forms and clieni info end.  
//PDF START : 
        $pdf = new FPDF();
        $pdf->AddPage();
//The Admin setting info related to PDF start.
        $admin_setting_info = $this->client_setting_model->get_where(array('client_id' => $client_id));
        if (!empty($admin_setting_info[0]->pdf_template)) {
            $pdf_template_fcpath = FCPATH . 'assets/upload/pdf_template/' . $admin_setting_info[0]->pdf_template;
            if (file_exists($pdf_template_fcpath) == true) {
                $pdf_template = base_url() . 'assets/upload/pdf_template/' . $admin_setting_info[0]->pdf_template;
            }
        }
        if (!empty($admin_setting_info[0]->top_margin)) {
            
        }
        if (!empty($admin_setting_info[0]->bottom_margin)) {
            
        }
        if (!empty($admin_setting_info[0]->left_margin)) {
            
        }
        if (!empty($admin_setting_info[0]->right_margin)) {
            
        }
        $logo_error = NULL;
        if (!empty($admin_setting_info[0]->pdf_logo)) {
            $logo = 'assets/upload/pdf_logo/' . $admin_setting_info[0]->pdf_logo;

            $config_image['image_library'] = 'gd2';
            $config['source_image'] = $logo;
            // $config['new_image'] ='assets/upload/client_logo/thumb_'.
            // $records[0]->logo;
            $config['maintain_ratio'] = TRUE;
            $config['create_thumb'] = TRUE;
            $config['width'] = 150;
            $config['height'] = 150;
            $this->image_lib->initialize($config);
            $this->load->library('image_lib', $config);
            // $resize= $this->image_lib->resize();

            if (!$this->image_lib->resize()) {
                $logo_error = $this->image_lib->display_errors();
            } else {

                $client_logo = substr($admin_setting_info[0]->pdf_logo, 0, strlen($admin_setting_info[0]->pdf_logo) - 4) . '_thumb' .
                        substr($admin_setting_info[0]->pdf_logo, -4);
                $client_logo_url = FCPATH . 'assets/upload/pdf_logo/' .
                        $client_logo;
                if (file_exists($client_logo_url) == true) {
                    $client_logo_url_true = base_url() .
                            'assets/upload/pdf_logo/' . $client_logo;
                } else {
                    $logo_error = 'IMAGE PATH NOT FOUND';
                }
            }

            if (isset($client_logo_url_true)) {
                $logo_url = $client_logo_url_true;
            }
        } else {
            $logo_error = 'No LOGO FOUND';
        }
//Admin setting info related to PDF end.
//Category Info Start.
        // $logo_error="sfuoasjdfidsjfiasdjf";
        $category_info = $this->forms_categories_model->get_where_sort(array('form_id' => $form_id), 'asc');
//Category Info end.
        if ($logo_error == NULL) {
            $pdf->Image($logo_url, 10, 6, 30);
        } else {
            $pdf->SetFont('Arial', '', 16);
            $pdf->Cell(0, 6, $logo_error);
        }
        $pdf->Output();
    }

    function create_pdf_for_forms($form_id) {
        $html = $this->get_pdf_content_ofform($form_id);
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $form_info = $this->forms_model->get_where(
                array('form_id' => $form_id, 'client_id' => $client_id
                ));
        $form_name = $form_info[0]->form_name;
        $this->load->helper('dompdf');
        $this->load->helper('dompdf/file');
        $this->load->helper('dompdf/dompdf_lib_include');
        dompdf_lib_include();

        $dompdf = new DOMPDF;
        // define("DOMPDF_DPI", 62.230);
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream("test.pdf", array("Attachment" => 0
        ));
    }

    function get_pdf_content_ofform($form_id) {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $form_info = $this->forms_model->get_where(
                array('form_id' => $form_id, 'client_id' => $client_id
                ));
        $intro_text = @$form_info[0]->introduction_text;
        $intro_text = str_replace('<pre>', '', $intro_text);
        $intro_text = str_replace('</pre>', '', $intro_text);
        $closer_text = @$form_info[0]->closure_text;
        $closer_text = str_replace('<pre>', '', $closer_text);
        $closer_text = str_replace('</pre>', '', $closer_text);
        if (empty($form_info)) {
            $this->session->set_flashdata('error', 'Form not found');
            redirect('admin/client_forms', 'refresh');
        }
        $records = $this->clients_model->get_where(
                array('client_id' => $form_info[0]->client_id
                ));
        /**
         * get the information about the categories related to the
         * forms
         */
        $category_info = $this->forms_categories_model->get_where_sort(
                array('form_id' => $form_id
                ), 'asc');

        $return = NULL;
        $return = '<html><head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> ';
        $style = NULL;
        $style.='<style type="text/css">';
        $admin_setting_info = $this->client_setting_model->get_where(array('client_id' => $client_id));
        $fonttype = NULL;
        if ($admin_setting_info[0]->letter_type) {
            $fonttype = $admin_setting_info[0]->letter_type;
        } else {
            $fonttype = 'sans-serif';
        }
        if (!empty($admin_setting_info[0]->pdf_template)) {
            /* $pdf_template_info = 'assets/upload/pdf_template/' . $admin_setting_info[0]->pdf_template;

              $config_image['image_library'] = 'gd2';
              $config['source_image'] = $pdf_template_info;
              $config['maintain_ratio'] = TRUE;
              $config['create_thumb'] = TRUE;
              $config['width'] = 792;
              $config['height'] = 792;
              $this->image_lib->initialize($config);
              $this->load->library('image_lib', $config);
              // $resize= $this->image_lib->resize();

              if (!$this->image_lib->resize()) {
              $return .= '<div>' .
              $this->image_lib->display_errors() . '</div>';
              } else {

              $pdf_template_exist = substr($admin_setting_info[0]->pdf_template, 0, strlen($admin_setting_info[0]->pdf_template) - 4) . '_thumb' .
              substr($admin_setting_info[0]->pdf_template, -4);

             */
            $pdf_template_fcpath = FCPATH . 'assets/upload/pdf_template/' . $admin_setting_info[0]->pdf_template;
            if (file_exists($pdf_template_fcpath) == true) {
                $pdf_template = base_url() . 'assets/upload/pdf_template/' . $admin_setting_info[0]->pdf_template;
            }
        }

        if (isset($pdf_template)) {
            $style.='body{background-image: url(' . $pdf_template . ');}';
        }

        $style.='#main_table_contain{';
        if (!empty($admin_setting_info[0]->top_margin)) {
            $style.='margin-top:' . $admin_setting_info[0]->top_margin . 'px;';
        }
        if (!empty($admin_setting_info[0]->bottom_margin)) {
            $style.='margin-bottom:' . $admin_setting_info[0]->bottom_margin . 'px;';
        }
        if (!empty($admin_setting_info[0]->left_margin)) {
            $style.='margin-left:' . $admin_setting_info[0]->left_margin . 'px;';
        }
        if (!empty($admin_setting_info[0]->right_margin)) {
            $style.='margin-right:' . $admin_setting_info[0]->right_margin . 'px;';
        }

        $style.='}</style>';
        $return.=$style;
        $return.='</head><body>';
        $return.='<div id="main_table_contain">';
        $return.= '<table style="font-family: ' . $fonttype . '; font-size: 14.2667px; width: 100%;" border="0" cellspacing="6" cellspadding="3">';
        if (!empty($admin_setting_info[0]->pdf_logo)) {
            $logo = 'assets/upload/pdf_logo/' . $admin_setting_info[0]->pdf_logo;

            $config_image['image_library'] = 'gd2';
            $config['source_image'] = $logo;
            // $config['new_image'] ='assets/upload/client_logo/thumb_'.
            // $records[0]->logo;
            $config['maintain_ratio'] = TRUE;
            $config['create_thumb'] = TRUE;
            $config['width'] = 150;
            $config['height'] = 150;
            $this->image_lib->initialize($config);
            $this->load->library('image_lib', $config);
            // $resize= $this->image_lib->resize();

            if (!$this->image_lib->resize()) {
                $return .= '<tr><th width="1%">&nbsp;</th><th colspan="2">' .
                        $this->image_lib->display_errors() . '</th></tr>';
            } else {

                $client_logo = substr($admin_setting_info[0]->pdf_logo, 0, strlen($admin_setting_info[0]->pdf_logo) - 4) . '_thumb' .
                        substr($admin_setting_info[0]->pdf_logo, -4);
                $client_logo_url = FCPATH . 'assets/upload/pdf_logo/' .
                        $client_logo;
                if (file_exists($client_logo_url) == true) {
                    $client_logo_url_true = base_url() .
                            'assets/upload/pdf_logo/' . $client_logo;
                } else {
                    $return .= '<tr><th width="1%">&nbsp;</th><th colspan="2">IMAGE PATH NOT FOUND</th></tr>';
                }
            }

            if (isset($client_logo_url_true)) {
                $return .= '<tr><th width="1%" style="text-align:left"><img  src="' .
                        $client_logo_url_true . '"></th><th colspan="2">&nbsp;</th></tr>';
            }
        } else {
            $return .= '<tr><th width="1%">&nbsp;</th><th colspan="2">No LOGO FOUND</th></tr>';
        }
        $return .= '<tr><th colspan="3"><h2>' . ucfirst(
                        $form_info[0]->form_name) . '</h2></th></tr>
             
            <tr>
            	<td colspan="3" style="text-align: justify;"><hr>' .
                @$intro_text . '</td>
            </tr>
            <tr>
            	<td colspan="3"></td>
            </tr>';

        foreach ($category_info as $cat_info) {
            $return .= '<tr><td colspan="3" style="font-weight: bold;"><hr>' .
                    ucfirst(@$cat_info->cat_name) . '</td></tr>';
            $this->forms_categories_model->cat_id = $cat_info->cat_id;
            $question = $this->forms_categories_model->get_questions();
            if (!empty($question)) {
                foreach ($question as $que) {
                    $return .= '<tr><td  width="50%" collspan="2" valign="top" style="padding-right:15px">' .
                            strip_tags($que->question) . '</td>';

                    // $pdf->Cell(15, 8, ' Ans->', 0, 0, 'L');
                    if ($que->answer_kind == 'checkbox') {
                        $answers = $this->forms_categories_question_answer_model->get_where(
                                array('question_id' => $que->question_id
                                ));
                        $return .= '<td valign="top">';
                        foreach ($answers as $ans) {
                            $return .= '<img src="' . base_url() .
                                    'assets/upload/client_logo/1369153410_checkbox_no.png' .
                                    '">' . strip_tags($ans->answer) . '<br />';
                        }
                        $return .= '</td></tr>';
                    } elseif ($que->answer_kind == 'radio') {
                        $answers = $this->forms_categories_question_answer_model->get_where(
                                array('question_id' => $que->question_id
                                ));
                        $return .= '<td valign="top">';
                        foreach ($answers as $ans) {

                            if (isset($ans->skip_to_questions) &&
                                    ( $ans->skip_to_questions == 0 )) {
                                $return .= '<img src="' . base_url() .
                                        'assets/upload/client_logo/Radio_button_off.png' .
                                        '">' . strip_tags($ans->answer) . '<br />';
                                // '(Einde Formulier)' . end form
                            } elseif (isset($ans->skip_to_questions) &&
                                    ( $ans->skip_to_questions != null )) {
                                $return .= '<img src="' . base_url() .
                                        'assets/upload/client_logo/Radio_button_off.png' . '">' .
                                        strip_tags($ans->answer) . '<br />';
                                //'(' .strip_tags($ans->skip_to_questions) . ')' .smart questions .. 
                            } else {
                                $return .= ' <img src="' . base_url() .
                                        'assets/upload/client_logo/Radio_button_off.png' . '">' .
                                        strip_tags($ans->answer) . '<br />';
                            }
                        }
                        $return .= '</td></tr>';
                    } else {
                        $return .= '<td></td></tr>';
                    }
                    // $pdf->Cell(70, 8, strip_tags($que->answer_kind), 0, 1,
                    // 'L');
                    $return .= '<tr><td colspan="3"></td></tr>';
                }
            } else {
                $return .= '<tr><td colspan="3">Er zijn geen vragen gevonden</td></tr>';
            }
        }
        $return .= '<tr><td colspan="3"></td></tr>
<tr>
	<td colspan="3" style="text-align: justify;"><hr>' .
                @$closer_text . '</td>
</tr>
        
</table>';
        $return.='</div></body></html>';
        return $return;
    }

    function _add_question_to_sequence($question_id) {
        $sequence = '';
        if (isset($_POST['sequence'])) {
            $sequence = trim($_POST['sequence']);
        }
        if ($sequence == '') {
            $sequence = 'Q:' . $question_id;
        } else {
            $sequence .= ',Q:' . $question_id;
        }
        return $sequence;
    }

    function _add_category_to_sequence($category_id) {
        $sequence = '';
        if (isset($_POST['sequence'])) {
            $sequence = trim($_POST['sequence']);
        }

        if ($sequence == '') {
            $sequence = 'C:' . $category_id;
        } else {
            $sequence .= ',C:' . $category_id;
        }
        return $sequence;
    }

    function _get_last_category_or_question($sequence) {
        $return_value = '';
        if ($sequence != '') {
            $values = explode(",", $sequence);
            $return_value = new stdClass();
            foreach ($values as $value) {

                $go_to = explode(":", $value);
                $return_value->go_to_type = $go_to[0];
                $return_value->go_to_value = $go_to[1];
            }
        }
        return $return_value;
    }

    function _remove_last_go_to_item($sequence) {
        if ($sequence != '') {
            $values = explode(",", $sequence);
        }
        unset($values[count($values) - 1]);
        $return_value = implode(',', $values);
        return $return_value;
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *        END COMMON FUNCTION WHICH CAN BE USED IN ANY FORM SHOW         * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                                                                         * 
     *                                                                         *
     *                          START OF PREVIEW FORMS                         *
     *                                                                         *
     *                                                                         *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    function view_form($form_id) {

        $admin_array = $this->session->userdata('admin');
        $user_id = $admin_array['user_id'];
        $this->temp_forms_answers_model->claim_id = $user_id;
        $this->temp_forms_answers_model->delete_temp_data();
        $this->session->unset_userdata('prev_url');
        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);

        if (count($forms) == 1) {
            $this->session->unset_userdata('prev_cat');
            $this->session->unset_userdata('prev_seq');
            if ($forms[0]->show_all_question_at_once == 0) {
                redirect(
                        base_url() . 'admin/client_forms/preview_once_start/' .
                        $user_id . '/' . $form_id . '/null/null', 'refresh');
            } elseif ($forms[0]->show_all_question_at_once == 1) {
                redirect(
                        base_url() . 'admin/client_forms/preview_question_start/' .
                        $user_id . '/' . $form_id, 'refresh');
            } else {
                redirect(
                        base_url() . 'admin/client_forms/preview_category_start/' .
                        $user_id . '/' . $form_id, 'refresh');
            }
        } else {
            redirect(base_url() . 'admin/client_forms', 'refresh');
        }
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                      START SHOW ALL QUESTION AT ONCE                    * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    function view_form_fields($claim_id, $form_id) {
        $data['title'] = 'Form Fields Details';
        $data['claim_id'] = $claim_id;
        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
        $data['form'] = $forms[0];
        $field_info = $this->form_field_values_model->get_where(array('claim_id' => $claim_id));
        $data['field_value'] = $field_info;
        // $data['email_field'] = $this->form_field_model->getMailFiledFromID($form_id);
        $data['page_name'] = 'admin/forms/preview_form/view_form_fields';
        $this->load->view($this->default_template_name, $data);
    }

    function preview_once_start($claim_id, $form_id, $category_id = null, $sequence = null, $success = true) {

        $admin_array = $this->session->userdata('admin');
        $user_id = $admin_array['user_id'];
        $data['title'] = 'Preview Insurance Form';
        $data['claim_id'] = $user_id;

        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
        $data['form'] = $forms[0];

        $form_info = $this->forms_model->get_where(array('form_id' => $form_id));
        $field_info = $this->form_field_values_model->get_where(array('claim_id' => $user_id));

        $data['form_value'] = $form_info[0];
        // $data['field_value'] = $field_info;
        $data['email_field'] = $this->form_field_model->getMailFiledFromID($form_id);

        $data['category_id'] = $category_id;
        $data['sequence'] = $sequence;

        $new_ques = array();
        if ($category_id == 'null' && $sequence == 'null') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    //  $new_ques[] = $get_question[0];
                }
            }

            $next = $this->getFirstCategoryFirstQuestion($form_id);

            $obj_ques = new forms_categories_question_model();
            if ($next) {
                $new_questions = $obj_ques->getAllQuestionForFormAtOnce($next['next_cat']->cat_id, $next['next_question']->sequence);
                if (!empty($new_questions)) {
                    foreach ($new_questions as $new) {
                        if (!in_array($new->question_id, $questionids)) {
                            $obj_ques = new forms_categories_question_model();
                            $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                            $new_ques[] = $get_question[0];
                            $last_question = $obj_ques->anyQuestionLeft_allAtOnce($get_question[0]->cat_id, $get_question[0]->question_id);
                        }
                    }
                }
            }
            if (!empty($last_question) && $last_question == TRUE) {
                $data['show_end_data'] = TRUE;
            } else {
                $data['show_end_data'] = FALSE;
            }
        } else if ($category_id == 'end' && $sequence == 'end') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (is_array($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }
            $data['show_end_data'] = TRUE;
        } else {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    //$new_ques[] = $get_question[0];
                }
            }
            $obj_ques = new forms_categories_question_model();
            $new_questions = $obj_ques->getAllQuestionForFormAtOnce($category_id, $sequence);
            foreach ($new_questions as $new) {
                if (!in_array($new->question_id, $questionids)) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                    $new_ques[] = $get_question[0];
                    $last_question = $obj_ques->anyQuestionLeft_allAtOnce($get_question[0]->cat_id, $get_question[0]->question_id);
                }
            }


            if (!empty($last_question) && $last_question == TRUE) {
                $data['show_end_data'] = TRUE;
            } else {
                $data['show_end_data'] = FALSE;
            }
        }

        $temp_count = count($questionids);
        if ($temp_count > 0) {
            $prev_questions = $questionids[count($questionids) - 1];
            if (isset($prev_questions) && $prev_questions != '') {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $prev_questions));
                if (!empty($get_question)) {
                    $data['prev_cat_id'] = $get_question[0]->cat_id;
                } else {
                    $data['prev_cat_id'] = '';
                }
            }
        }

        $all_cat = array();
        foreach ($new_ques as $ques) {
            if (!in_array($ques->cat_id, $all_cat)) {
                $all_cat[] = $ques->cat_id;
            }
        }

        $category_info = array();
        foreach ($all_cat as $cat) {
            $obj_cat = new forms_categories_model();
            $get_cat_details = $obj_cat->get_where(array('cat_id' => $cat));
            if (!empty($get_cat_details))
                $category_info[] = $get_cat_details[0];
        }
//        if (empty($category_info)) {
//            $this->session->set_flashdata('error', 'There is no Categories and Questions.');
//            redirect(base_url() . 'admin/client_forms', 'refresh');
//            exit;
//        }
        $data['category_info'] = $category_info;
        $data['questions'] = $new_ques;
        //$data['cond_var'] = $new[count($new_ques) - 1];
        //echo "tests";
        //exit;

        if ($category_id == 'null' && $sequence == 'null') {
            $data['show_data'] = 1;
            $data['page_name'] = 'admin/forms/preview_form/show_all_at_once';
            $this->load->view($this->default_template_name, $data);
        } else {
            $data['show_data'] = 0;
            $this->load->view('admin/forms/preview_form/show_all_at_once', $data);
        }
    }

    function get_smart_questions($claim_id, $form_id, $category_id, $sequence) {
        $sucess = $this->validateQuestionForShowAllAtOnce($claim_id, $form_id, $category_id, $sequence);
        if ($sucess == TRUE) {
            $this->saveShowallAtOnce($claim_id, $form_id, $category_id, $sequence);

            $answer_id = $this->input->post('last_answer_id');
            $check = $this->forms_categories_question_answer_model->get_where(array('answer_id' => $answer_id));

            if ($check[0]->skip_to_questions != null && $check[0]->skip_to_questions != '') {

                if ($check[0]->skip_to_questions != 0) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $check[0]->skip_to_questions));
                    /* $url=base_url() . 'admin/client_forms/preview_once_start/' . $claim_id . '/' . $form_id . '/' . $get_question[0]->cat_id . '/' . $get_question[0]->sequence . '#view_' . $check[0]->skip_to_questions;
                      var_dump($url);
                      exit;
                      redirect(base_url() . 'admin/client_forms/preview_once_start/' . $claim_id . '/' . $form_id . '/' . $get_question[0]->cat_id . '/' . $get_question[0]->sequence . '#view_' . $check[0]->skip_to_questions, 'refresh'); */

                    $this->preview_once_start($claim_id, $form_id, $get_question[0]->cat_id, $get_question[0]->sequence, $sucess);
                } else {

                    redirect(base_url() . 'admin/client_forms/preview_once_start/' . $claim_id . '/' . $form_id . '/end/end#end_form', 'refresh');
                    // $this->preview_once_start($claim_id, $form_id, 'end', 'end');
                }
            } else {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $check[0]->question_id));
                $next = $this->getNextQuestion($get_question[0]->cat_id, $get_question[0]->question_id, $get_question[0]->sequence);
                //redirect(base_url() . 'admin/client_forms/preview_once_start/' . $claim_id . '/' . $form_id . '/' . $next['next_cat']->cat_id . '/' . $next['next_question']->sequence . '#view_' . $next['next_question']->question_id, 'refresh');

                $this->preview_once_start($claim_id, $form_id, $next['next_cat']->cat_id, $next['next_question']->sequence, $sucess);
            }
        } else {

            $this->preview_once_start($claim_id, $form_id, $category_id, $sequence, $sucess);
        }
    }

    function saveShowallAtOnce($claim_id, $form_id, $category_id, $sequence) {

        $new_ques = array();
        if ($category_id == 'null' && $sequence == 'null') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }
            $next = $this->getFirstCategoryFirstQuestion($form_id);
            $obj_ques = new forms_categories_question_model();
            $new_questions = $obj_ques->getAllQuestionForFormAtOnce($next['next_cat']->cat_id, $next['next_question']->sequence);
            foreach ($new_questions as $new) {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                $new_ques[] = $get_question[0];
            }
        } else if ($category_id == 'end' && $sequence == 'end') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }
        } else {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }

            $obj_ques = new forms_categories_question_model();
            $new_questions = $obj_ques->getAllQuestionForFormAtOnce($category_id, $sequence);
            foreach ($new_questions as $new) {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                $new_ques[] = $get_question[0];
            }
        }

        foreach ($new_ques as $question) {
            $this->saveFomrValues($claim_id, $form_id, $question->cat_id, $question->question_id);
        }

        return true;
    }

    function wholeFormSaveActionAtOnce($claim_id, $form_id, $category_id, $sequence) {
        $action = $this->input->post('action');
        if ($action == 'Opslaan in concept') {
            $this->endPreviewSaveAllAtonce($claim_id, $form_id, $category_id, $sequence);
        } else if ($action == 'confirm') {
            $this->endPreviewConfirmAndSendAtOnce($claim_id, $form_id, $category_id, $sequence);
        }
    }

    function endPreviewSaveAllAtonce($claim_id, $form_id, $category_id, $sequence) {
        $sucess = $this->validateQuestionForShowAllAtOnce($claim_id, $form_id, $category_id, $sequence);
        if ($sucess == TRUE) {
            $this->saveShowallAtOnce($claim_id, $form_id, $category_id, $sequence);
            $this->session->set_flashdata('success', "Form status is now open");
            redirect(base_url() . "admin/client_forms");
        } else {
            $this->preview_once_start($claim_id, $form_id, $category_id, $sequence, $sucess);
        }
    }

    function endPreviewConfirmAndSendAtOnce($claim_id, $form_id, $category_id, $sequence) {
        $sucess = $this->validateQuestionForShowAllAtOnce($claim_id, $form_id, $category_id, $sequence);
        if ($sucess == TRUE) {
            $this->saveShowallAtOnce($claim_id, $form_id, $category_id, $sequence);
            $this->session->set_flashdata('success', "Formulier is afgerond");
            redirect(base_url() . "admin/client_forms");
        } else {
            $this->preview_once_start($claim_id, $form_id, $category_id, $sequence, $sucess);
        }
    }

    function getPreviousQuestionDetails($claim_id) {
        $obj_ans = new temp_forms_answers_model();
        $get_answer = $obj_ans->get_where(array('claim_id' => $claim_id));
        $return = array();
        foreach ($get_answer as $ans) {
            $return[] = $ans->question_id;
        }
        return $return;
    }

    function validateQuestionForShowAllAtOnce($claim_id, $form_id, $category_id, $sequence) {

        $new_ques = array();
        $validatin_success = true;
        if ($category_id == 'null' && $sequence == 'null') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }
            $next = $this->getFirstCategoryFirstQuestion($form_id);
            $obj_ques = new forms_categories_question_model();
            $new_questions = $obj_ques->getAllQuestionForFormAtOnce($next['next_cat']->cat_id, $next['next_question']->sequence);
            foreach ($new_questions as $new) {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                $new_ques[] = $get_question[0];
            }
        } else if ($category_id == 'end' && $sequence == 'end') {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }
        } else {
            $questionids = $this->getPreviousQuestionDetails($claim_id);
            if (!empty($questionids)) {
                foreach ($questionids as $new) {
                    $obj_ques = new forms_categories_question_model();
                    $get_question = $obj_ques->get_where(array('question_id' => $new));
                    $new_ques[] = $get_question[0];
                }
            }

            $obj_ques = new forms_categories_question_model();
            $new_questions = $obj_ques->getAllQuestionForFormAtOnce($category_id, $sequence);
            foreach ($new_questions as $new) {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(array('question_id' => $new->question_id));
                $new_ques[] = $get_question[0];
            }
        }

        if (!empty($new_ques)) {
            foreach ($new_ques as $question) {
                if ($question->answer_kind == 'checkbox') {
                    if ($question->required == 1) {
                        $checkbox_validated = $this->_validate_checkbox_group_required($question);
                        if ($checkbox_validated != true) {
                            $this->form_validation->set_rules($question->question_id . "_", ' answer of this ', 'required');
                            if ($this->form_validation->run() == FALSE) {
                                $validatin_success = false;
                                $this->form_validation->set_value($question->question_id . "_");
                            }
                        }
                    }
                } else if ($question->answer_kind == 'date') {

                    if ($question->required == 1) {
                        $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                    } else {
                        $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                    }
                    if ($this->form_validation->run() == FALSE) {
                        $validatin_success = false;
                        $this->form_validation->set_value($question->question_id . "_");
                    }
                } else if ($question->answer_kind == 'number') {
                    //||regex_match[/^[0-9]+\.[0-9]{2}$/]
                    if ($question->required == 1) {
                        $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^[0-9]\d*(\,\d{0,2})?$/]');
                    } else {
                        $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^[0-9]\d*(\,\d{0,2})?$/]');
                    }
                    if ($this->form_validation->run() == FALSE) {
                        $validatin_success = false;
                        $this->form_validation->set_value($question->question_id . "_");
                    }
                } else {

                    if ($question->required == 1) {
                        $this->form_validation->set_rules($question->question_id . "_", ' answer of this ', 'required');
                        if ($this->form_validation->run() == FALSE) {
                            $validatin_success = false;
                            $this->form_validation->set_value($question->question_id . "_");
                        } else {
                            $validatin_success = true;
                        }
                    }
                }
            }
        }
        return $validatin_success;
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                        END SHOW ALL QUESTION AT ONCE                    * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *              START SHOW ALL QUESTION CATEGORY BY CATEGORY               * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    function preview_category_start($claim_id, $form_id) {
        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
        $form = $forms[0];

        $form_info = $this->forms_model->get_where(
                array('form_id' => $form_id
                ));
        $data['form_value'] = $form_info[0];

        $field_info = $this->form_field_values_model->get_where(
                array('claim_id' => $claim_id
                ));
        $data['field_value'] = $field_info;

        $email_field = $this->form_field_model->getMailFiledFromID($form_id);
        $data['email_field_name'] = @$email_field->field_name;

        $data['total_form_completed'] = $this->getFromProgress($claim_id, $form_id);
        $data['title'] = 'Preview Insurance Form';
        $data['form_id'] = $form_id;
        $data['form'] = $form;
        $data['claim_id'] = $claim_id;
        $data['page_name'] = 'admin/forms/preview_form/show_category_header';

        $first_category = $this->getFirstCategory($form_id);

        if ($first_category !== false) {
            $data['next_category'] = $first_category;
        }

        $this->load->view($this->default_template_name, $data);
    }

    function preview_category($claim_id, $form_id, $category_id, $sequence = null) {
        if ($category_id != 'end') {
            $obj_cat = new forms_categories_model();
            $get_cat = $obj_cat->get_where(
                    array('form_id' => $form_id, 'cat_id' => $category_id
                    ));

            if (count($get_cat) == 1) {

                $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
                $data['form'] = $forms[0];
                $data['category'] = $get_cat[0];
                $data['claim_id'] = $claim_id;
                $PURL = $this->session->userdata('prev_url');
                if (isset($PURL) && $PURL != false) {
                    $data['prev_url_smart'] = $PURL;
                } else {
                    $data['prev_url_smart'] = '';
                }

                $data['prev_cat'] = $this->get_previous_category($category_id);
                $data['next_cat'] = $this->get_next_category($category_id);
                $data['smart_sequence'] = $sequence;

                $data['total_form_completed'] = $this->getFromProgress(
                        $claim_id, $form_id);
                $data['title'] = 'Forms';
                $data['page_name'] = 'admin/forms/preview_form/show_single_category';
                $this->load->view($this->default_template_name, $data);
            } else {
                $this->session->set_flashdata('error', 'Data Not Exits 2');
                redirect(base_url() . "admin/client_forms");
            }
        } else {
            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $data['form'] = $forms[0];
            $PURL = $this->session->userdata('prev_url');
            $count = count($PURL);
            if (isset($PURL) && $PURL != false) {
                $data['prev_url_smart'] = $PURL;
            } else {
                $data['prev_url_smart'] = '';
            }
            $data['prev_cat'] = $this->getLastCategory($form_id);
            $data['claim_id'] = $claim_id;
            $data['title'] = 'Forms';
            $data['total_form_completed'] = $this->getFromProgress($claim_id, $form_id);
            $data['page_name'] = 'admin/forms/preview_form/show_category_footer';
            $this->load->view($this->default_template_name, $data);
        }
    }

    function PreviewCategoryListener($claim_id, $form_id, $category_id, $sequence = null) {
        if ($category_id != null) {
            if (isset($sequence) && $sequence != null) {
                $save_success = $this->_preview_category_validate_form_post(
                        $form_id, $category_id, $sequence);
                if ($save_success == false) {
                    $this->preview_category($claim_id, $form_id, $category_id, $sequence);
                } else {
                    $sucess = true;
                }
            } else {
                $save_success = $this->_preview_category_validate_form_post(
                        $form_id, $category_id, $sequence);
                if ($save_success == false) {
                    $this->preview_category($claim_id, $form_id, $category_id);
                } else {
                    $sucess = true;
                }
            }

            if (@$sucess == true) {
                $this->savecategoryValues($claim_id, $form_id, $category_id);
                $prev_url = array();
                $PURL = $this->session->userdata('prev_url');
                $temp = array();
                if ($PURL !== FALSE) {
                    foreach ($PURL as $p) {
                        $temp['prev_url'][] = $p;
                    }
                }

                if (isset($sequence) && $sequence != null) {

                    $temp_url = base_url() .
                            'admin/client_forms/preview_category/' . $claim_id .
                            '/' . $form_id . '/' . $category_id . '/' .
                            $sequence;
                    if (!empty($temp['prev_url'])) {
                        if (!in_array($temp_url, $temp['prev_url'])) {
                            $temp['prev_url'][] = $temp_url;
                        }
                    } else {
                        $temp['prev_url'][] = base_url() .
                                'admin/client_forms/preview_category/' .
                                $claim_id . '/' . $form_id . '/' . $category_id .
                                '/' . $sequence;
                    }
                } else {

                    $temp_url = base_url() .
                            'admin/client_forms/preview_category/' . $claim_id .
                            '/' . $form_id . '/' . $category_id;
                    if (!empty($temp['prev_url'])) {
                        if (!in_array($temp_url, @$temp['prev_url'])) {
                            $temp['prev_url'][] = $temp_url;
                        }
                    } else {
                        $temp['prev_url'][] = base_url() .
                                'admin/client_forms/preview_category/' .
                                $claim_id . '/' . $form_id . '/' . $category_id;
                    }
                }

                $this->session->set_userdata($temp);
                $obj_cat = new forms_categories_model();
                $get_cat = $obj_cat->get_where(
                        array('form_id' => $form_id, 'cat_id' => $category_id
                        ));
                if (isset($sequence) && $sequence != null) {
                    $this->load->model('forms_categories_question_model');
                    $obj_ques = new forms_categories_question_model();
                    $get_ques = $obj_ques->get_where(
                            array('cat_id' => $get_cat[0]->cat_id,
                                'sequence >' => $sequence
                            ));

                    if (count($get_ques) > 0) {
                        $skip_questions = $get_cat[0]->get_question_if_smart(
                                $sequence, false);
                    } else {
                        $skip_questions = $get_cat[0]->get_question_if_smart(
                                $sequence, true);
                    }
                } else {
                    $skip_questions = $get_cat[0]->get_question_smart();
                }

                $last_question_detail = $skip_questions[count($skip_questions) -
                        1];

                if ($last_question_detail->answer_kind == 'radio') {
                    $answer_given = $this->input->post(
                            $last_question_detail->question_id . '_');

                    $answers = $this->forms_categories_question_answer_model->get_where(
                            array('answer_id' => $answer_given
                            ));

                    $check = $this->forms_categories_question_answer_model->get_where(
                            array(
                                'question_id' => $last_question_detail->question_id,
                                'answer_id' => $answer_given
                            ));

                    if (isset($check[0]->skip_to_questions) &&
                            $check[0]->skip_to_questions != null &&
                            $check[0]->skip_to_questions != '') {
                        if ($check[0]->skip_to_questions == 0) {
                            redirect(
                                    base_url() .
                                    'admin/client_forms/preview_category/' .
                                    $claim_id . '/' . $form_id . '/end', 'refresh');
                        } else {
                            $obj_ques = new forms_categories_question_model();
                            $get_question = $obj_ques->get_where(
                                    array(
                                        'question_id' => $check[0]->skip_to_questions
                                    ));

                            redirect(
                                    base_url() .
                                    'admin/client_forms/preview_category/' .
                                    $claim_id . '/' . $form_id . '/' .
                                    $get_question[0]->cat_id . '/' .
                                    $get_question[0]->sequence, 'refresh');
                        }
                    } else {
                        $is_question_exit = $this->isAnyQuestionLeft($form_id, $category_id, $last_question_detail->sequence);

                        if ($is_question_exit != false) {
                            redirect(
                                    base_url() .
                                    'admin/client_forms/preview_category/' .
                                    $claim_id . '/' . $form_id . '/' .
                                    $category_id . '/' .
                                    $is_question_exit, 'refresh');
                        } else {
                            $next_cat_id = $this->get_next_category(
                                    $category_id);

                            if (isset($next_cat_id) && $next_cat_id != null) {
                                redirect(
                                        base_url() .
                                        'admin/client_forms/preview_category/' .
                                        $claim_id . '/' . $form_id . '/' .
                                        $next_cat_id, 'refresh');
                            } else {
                                redirect(
                                        base_url() .
                                        'admin/client_forms/preview_category/' .
                                        $claim_id . '/' . $form_id .
                                        '/end', 'refresh');
                            }
                        }
                    }
                } else {
                    $is_question_exit = $this->isAnyQuestionLeft($form_id, $category_id, $last_question_detail->sequence);

                    if ($is_question_exit != false) {
                        redirect(
                                base_url() .
                                'admin/client_forms/preview_category/' .
                                $claim_id . '/' . $form_id . '/' .
                                $category_id . '/' . $is_question_exit, 'refresh');
                    } else {
                        $next_cat_id = $this->get_next_category($category_id);

                        if (isset($next_cat_id) && $next_cat_id != null) {
                            redirect(
                                    base_url() .
                                    'admin/client_forms/preview_category/' .
                                    $claim_id . '/' . $form_id . '/' .
                                    $next_cat_id, 'refresh');
                        } else {
                            redirect(
                                    base_url() .
                                    'admin/client_forms/preview_category/' .
                                    $claim_id . '/' . $form_id . '/end', 'refresh');
                        }
                    }
                }
            }
        }
    }

    function isAnyQuestionLeft($form_id, $cat_id, $seq) {
        $obj_quest = new forms_categories_question_model();
        $get_ques = $obj_quest->get_where_sort(
                array('cat_id' => $cat_id, 'sequence >' => $seq
                ), 'asc');

        if (count($get_ques) > 0) {
            return $get_ques[0]->sequence;
        } else {
            return false;
        }
    }

    function savecategoryValues($claim_id, $form_id, $category_id) {
        $obj_cat = new forms_categories_model();
        $get_cat = $obj_cat->get_where(
                array('form_id' => $form_id, 'cat_id' => $category_id
                ));
        $questions = $get_cat[0]->get_questions();
        foreach ($questions as $question) {
            $question_id = $question->question_id;

            $obj_ques = new forms_categories_question_model();
            $get_question = $obj_ques->get_where(
                    array('question_id' => $question_id
                    ));

            $answer = new temp_forms_answers_model();
            $prev_id = $answer->getLastRecordInserted($claim_id);
            if (!empty($prev_id)) {
                $prev_id = $prev_id[0]->question_id;
            } else {
                $prev_id = 0;
            }
            $answers = $answer->get_where(array('question_id' => $question_id, 'claim_id' => $claim_id)); // get
            // the
            // result
            if (count($answers) > 0) {
                $action = "update";
                $answer = $answers[0];
            } else {
                $action = "save";
            }

            if ($get_question[0]->answer_kind == 'checkbox') {
                $answers_choice_selected_ids = $this->input->post(
                        $question_id . "_");
                if ($action != "update") {
                    $answer = new temp_forms_answers_model();
                }

                $answer->claim_id = $claim_id;
                $answer->cat_id = $category_id;
                $answer->answer_type = "checkbox";
                $answer->question_id = $question_id;
                $answer_id = -1;
                if ($action == "update") {
                    $answer->update();
                    $answer_id = $answer->answer_id;
                    $answers_choices = $answer->get_answer_choices();
                    foreach ($answers_choices as $choice) {
                        $choice->delete();
                    }
                } else {
                    $answer->prev_question = $prev_id;
                    $answer_id = $answer->save();
                }
                if ($answers_choice_selected_ids != false) {
                    foreach ($answers_choice_selected_ids as $answer_choice_id) {

                        $forms_answers_details_model = new temp_forms_answers_details_model();
                        $forms_answers_details_model->user_answer_id = $answer_id;
                        $forms_answers_details_model->answer_id = $answer_choice_id;
                        $forms_answers_details_model->save();
                    }
                }
            } else {
                if ($action != "update") {
                    $answer = new temp_forms_answers_model();
                }
                $answer->claim_id = $claim_id;
                $answer->cat_id = $category_id;
                $answer->answer_type = "";
                $answer->question_id = $question_id;
                $answer->answer_text = $this->input->post($question_id . "_");
                $answer->answer_type = $get_question[0]->answer_kind;
                if ($action == "update") {
                    $answer->update();
                } else {
                    $answer->prev_question = $prev_id;
                    $answer->save();
                }
            }
        }
    }

    function get_previous_category($cat_id) {
        $obj = new forms_categories_model();
        $get_current_records = $obj->get_where(
                array('cat_id' => $cat_id
                ));
        $get_all_records = $obj->get_where_sort(
                array('form_id' => $get_current_records[0]->form_id
                ), 'asc');

        if (count($get_current_records) == 1) {
            $temp = array();
            foreach ($get_all_records as $all) {
                $x = array();
                $x['sequence'] = $all->sequence;
                $x['cat_id'] = $all->cat_id;
                $temp[] = $x;
            }

            $current_key = 0;
            foreach ($temp as $key => $value) {
                if ($value['sequence'] == $get_current_records[0]->sequence) {
                    $current_key = $key;
                }
            }
            $prev_cat = @$temp[$current_key - 1]['cat_id'];
            return $prev_cat;
        } else {
            return false;
        }
    }

    function get_next_category($cat_id) {
        $obj = new forms_categories_model();
        $get_current_records = $obj->get_where(
                array('cat_id' => $cat_id
                ));
        $get_all_records = $obj->get_where_sort(
                array('form_id' => $get_current_records[0]->form_id
                ), 'asc');

        if (count($get_current_records) == 1) {
            $temp = array();
            foreach ($get_all_records as $all) {
                $x = array();
                $x['sequence'] = $all->sequence;
                $x['cat_id'] = $all->cat_id;
                $temp[] = $x;
            }

            $current_key = 0;
            foreach ($temp as $key => $value) {
                if ($value['sequence'] == $get_current_records[0]->sequence) {
                    $current_key = $key;
                }
            }
            // Update next to up sequence
            $next_cat = @$temp[$current_key + 1]['cat_id'];
            return $next_cat;
        } else {
            return false;
        }
    }

    function getFirstCategory($form_id) {
        $return = array();
        $obj_cat = new forms_categories_model();
        $get_cat = $obj_cat->get_where_sort(
                array('form_id' => $form_id
                ), 'asc');

        if (count($get_cat) > 0) {
            $obj_ques = new forms_categories_question_model();
            $get_question = $obj_ques->get_where_sort(
                    array('cat_id' => $get_cat[0]->cat_id
                    ), 'asc');

            if (count($get_question) > 0) {
                $return['next_cat'] = $get_cat[0];
                return $return;
                exit();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function getLastCategory($form_id) {
        $return = array();
        $obj_cat = new forms_categories_model();
        $get_cat = $obj_cat->get_where_sort(
                array('form_id' => $form_id
                ), 'desc');

        if (count($get_cat) > 0) {
            $obj_ques = new forms_categories_question_model();
            $get_question = $obj_ques->get_where_sort(
                    array('cat_id' => $get_cat[0]->cat_id
                    ), 'desc');

            if (count($get_question) > 0) {
                $return['next_cat'] = $get_cat[0];
                return $return;
                exit();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *               END SHOW ALL QUESTION CATEGORY BY CATEGORY                * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                    START SHOW ALL QUESTION ONE BY ONE                   * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    function preview_question($claim_id, $form_id, $category_id, $question_id) {
        if ($category_id != 'end' && $question_id != 'end') {

            $obj_cat = new forms_categories_model();
            $get_cat = $obj_cat->get_where(
                    array('form_id' => $form_id, 'cat_id' => $category_id
                    ));
            if (count($get_cat) == 1) {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(
                        array('cat_id' => $category_id,
                            'question_id' => $question_id
                        ));
                if (count($get_question) == 1) {
                    if ($get_question[0]->answer_kind == 'radio') {
                        $obj_ans = new forms_categories_question_answer_model();
                        $get_ans = $obj_ans->get_where(
                                array('question_id' => $question_id
                                ));
                        $data['answer_radio'] = $get_ans;
                    }

                    if ($get_question[0]->answer_kind == 'checkbox') {
                        $obj_ans = new forms_categories_question_answer_model();
                        $get_ans = $obj_ans->get_where(
                                array('question_id' => $question_id
                                ));
                        $data['answer_check_box'] = $get_ans;
                    }

                    if ($get_question[0]->answer_kind == 'text') {
                        $data['answer_text'] = TRUE;
                    }

                    if ($get_question[0]->answer_kind == 'textarea') {
                        $data['answer_textarea'] = TRUE;
                    }
                    if ($get_question[0]->answer_kind == 'date') {
                        $data['answer_date'] = TRUE;
                    }
                    if ($get_question[0]->answer_kind == 'number') {
                        $data['answer_number'] = TRUE;
                    }
                    if ($get_question[0]->answer_kind == 'financieel') {
                        $data['answer_financieel'] = TRUE;
                    }

                    $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
                    $data['form'] = $forms[0];

                    $data['category_details'] = $get_cat[0];
                    $data['question_details'] = $get_question[0];
                    $data['claim_id'] = $claim_id;
                    $next_question = $this->getNextQuestion($category_id, $question_id, $get_question[0]->sequence);

                    if ($next_question !== false)
                        $data['next_question'] = $next_question;

                    $check_answer_given = $this->isAnswerGiven($claim_id, $form_id, $question_id);
                    if ($check_answer_given != false) {
                        $data['prev_url'] = $check_answer_given;
                    } else if ($check_answer_given === 0) {
                        $data['prev_url'] = base_url() .
                                'admin/client_forms/preview_question_start/' .
                                $claim_id . '/' . $form_id;
                    } else {
                        $is_value = $this->session->userdata('previous_url');
                        if ($is_value != '') {
                            $data['prev_url'] = $is_value;
                        } else {
                            $prev_question = $this->getPreviousQuestion(
                                    $category_id, $question_id, $get_question[0]->sequence);
                            if ($prev_question !== false)
                                $data['prev_question'] = $prev_question;
                        }
                    }

                    $session = array('previous_url' => ''
                    );
                    $this->session->unset_userdata($session);

                    $data['total_form_completed'] = $this->getFromProgress(
                            $claim_id, $form_id);
                    $data['title'] = 'Forms';
                    $data['page_name'] = 'admin/forms/preview_form/single_question';
                    $this->load->view($this->default_template_name, $data);
                }
                else {
                    $this->session->set_flashdata('error', 'Data Not Exits 1');
                    redirect(base_url() . "admin/client_forms");
                }
            } else {
                $this->session->set_flashdata('error', 'Data Not Exits 2');
                redirect(base_url() . "admin/client_forms");
            }
        } else {
            $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
            $data['form'] = $forms[0];
            $data['claim_id'] = $claim_id;
            $data['title'] = 'Forms';
            $is_value = $this->session->userdata('previous_url');
            if ($is_value != '') {
                $data['prev_url'] = $is_value;
            } else {
                $prev_question = $this->getPreviousQuestion($category_id, $question_id, $get_question[0]->sequence);
                if ($prev_question !== false)
                    $data['prev_question'] = $prev_question;
            }

            $data['total_form_completed'] = $this->getFromProgress($claim_id, $form_id);
            $data['page_name'] = 'admin/forms/preview_form/show_form_footer';
            $this->load->view($this->default_template_name, $data);
        }
    }

    function PreviewQuestionListener($claim_id, $form_id, $category_id, $question_id) {
        $old_cat_id = $this->input->post('old_category_id');

        if ($old_cat_id != '') {
            $validation_check_result = $this->ValidateQuestion($question_id);
            if ($validation_check_result == false) {
                $this->preview_question($claim_id, $form_id, $category_id, $question_id);
            } else {
                $this->saveFomrValues($claim_id, $form_id, $category_id, $question_id);

                $url = base_url() . 'admin/client_forms/preview_question/' .
                        $claim_id . '/' . $form_id . '/' . $category_id . '/' .
                        $question_id;
                $session = array('previous_url' => $url
                );
                $this->session->set_userdata($session);
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(
                        array('question_id' => $question_id
                        ));

                if ($get_question[0]->answer_kind == 'radio') {
                    $obj_answer = new forms_categories_question_answer_model();
                    $get_answer = $obj_answer->get_where(
                            array('question_id' => $question_id,
                                'answer_id' => $this->input->post(
                                        $question_id . '_')
                            ));
                    if (@$get_answer[0]->skip_to_questions != NULL ||
                            @$get_answer[0]->skip_to_questions != '') {
                        if ($get_answer[0]->skip_to_questions !== '0') {
                            $obj_ques = new forms_categories_question_model();
                            $get_question = $obj_ques->get_where(
                                    array(
                                        'question_id' => $get_answer[0]->skip_to_questions
                                    ));
                            redirect(
                                    base_url() .
                                    'admin/client_forms/preview_question/' .
                                    $claim_id . '/' . $form_id . '/' .
                                    $get_question[0]->cat_id . '/' .
                                    $get_answer[0]->skip_to_questions, 'refresh');
                        } else {
                            redirect(
                                    base_url() .
                                    'admin/client_forms/preview_question/' .
                                    $claim_id . '/' . $form_id .
                                    '/end/end', 'refresh');
                        }
                    } else {
                        $next_question = $this->getNextQuestion($category_id, $question_id, $get_question[0]->sequence);
                        redirect(
                                base_url() .
                                'admin/client_forms/preview_question/' .
                                $claim_id . '/' . $form_id . '/' .
                                $next_question['next_cat']->cat_id . '/' .
                                $next_question['next_question']->question_id, 'refresh');
                    }
                } else {
                    $next_question = $this->getNextQuestion($category_id, $question_id, $get_question[0]->sequence);
                    redirect(
                            base_url() . 'admin/client_forms/preview_question/' .
                            $claim_id . '/' . $form_id . '/' .
                            $next_question['next_cat']->cat_id . '/' .
                            $next_question['next_question']->question_id, 'refresh');
                }
            }
        }
    }

    function saveFomrValues($claim_id, $form_id, $category_id, $question_id) {
        $obj_ques = new forms_categories_question_model();
        $get_question = $obj_ques->get_where(
                array('question_id' => $question_id
                ));

        $answer = new temp_forms_answers_model();
        $prev_id = $answer->getLastRecordInserted($claim_id);
        if (!empty($prev_id)) {
            $prev_id = $prev_id[0]->question_id;
        } else {
            $prev_id = 0;
        }
        $answers = $answer->get_where(
                array('question_id' => $question_id, 'claim_id' => $claim_id
                )); // get
        // the
        // result
        if (count($answers) > 0) {
            $action = "update";
            $answer = $answers[0];
        } else {
            $action = "save";
        }

        if ($get_question[0]->answer_kind == 'checkbox') {
            $answers_choice_selected_ids = $this->input->post(
                    $question_id . "_");
            if ($action != "update") {
                $answer = new temp_forms_answers_model();
            }

            $answer->claim_id = $claim_id;
            $answer->cat_id = $category_id;
            $answer->answer_type = "checkbox";
            $answer->question_id = $question_id;
            $answer_id = -1;
            if ($action == "update") {
                $answer->update();
                $answer_id = $answer->answer_id;
                $answers_choices = $answer->get_answer_choices();
                foreach ($answers_choices as $choice) {
                    $choice->delete();
                }
            } else {
                $answer->prev_question = $prev_id;
                $answer_id = $answer->save();
            }
            if ($answers_choice_selected_ids != false) {
                foreach ($answers_choice_selected_ids as $answer_choice_id) {

                    $forms_answers_details_model = new temp_forms_answers_details_model();
                    $forms_answers_details_model->user_answer_id = $answer_id;
                    $forms_answers_details_model->answer_id = $answer_choice_id;
                    $forms_answers_details_model->save();
                }
            }
        } else {
            if ($action != "update") {
                $answer = new temp_forms_answers_model();
            }
            $answer->claim_id = $claim_id;
            $answer->cat_id = $category_id;
            $answer->answer_type = "";
            $answer->question_id = $question_id;
            $answer->answer_text = $this->input->post($question_id . "_");
            $answer->answer_type = $get_question[0]->answer_kind;
            if ($action == "update") {
                $answer->update();
            } else {
                $answer->prev_question = $prev_id;
                $answer->save();
            }
        }
    }

    function ValidateQuestion($question_id) {
        $obj_ques = new forms_categories_question_model();
        $get_question = $obj_ques->get_where(
                array('question_id' => $question_id
                ));
        $validatin_success = true;
        if (count($get_question) == 1) {
            $question = $get_question[0];
            if ($question->answer_kind == 'checkbox') {
                if ($question->required == 1) {
                    $checkbox_validated = $this->_validate_checkbox_group_required(
                            $question);
                    // if none of the checkboxes were checked
                    if ($checkbox_validated != true) {

                        $this->form_validation->set_rules(
                                $question->question_id . "_", ' answer of this ', 'required');

                        if ($this->form_validation->run() == FALSE) {

                            $validatin_success = false;
                            $this->form_validation->set_value(
                                    $question->question_id . "_");
                        }
                    }
                }
            } else if ($question->answer_kind == 'date') {

                if ($question->required == 1) {
                    $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                } else {
                    $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/]');
                }
                if ($this->form_validation->run() == FALSE) {
                    $validatin_success = false;
                    $this->form_validation->set_value($question->question_id . "_");
                }
            } else if ($question->answer_kind == 'number') {
                //||regex_match[/^[0-9]+\.[0-9]{2}$/]
                if ($question->required == 1) {
                    $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'required||regex_match[/^[0-9]\d*(\,\d{0,2})?$/]');
                } else {
                    $this->form_validation->set_rules($question->question_id . "_", 'Answer of this', 'regex_match[/^[0-9]\d*(\,\d{0,2})?$/]');
                }
                if ($this->form_validation->run() == FALSE) {
                    $validatin_success = false;
                    $this->form_validation->set_value($question->question_id . "_");
                }
            } else {
                if ($question->required == 1) {
                    $this->form_validation->set_rules(
                            $question->question_id . "_", ' answer of this ', 'required');
                    if ($this->form_validation->run() == FALSE) {
                        $validatin_success = false;
                        $this->form_validation->set_value(
                                $question->question_id . "_");
                    }
                }
            }
        } else {
            $validatin_success = false;
        }
        return $validatin_success;
    }

    function getCategoryQuestion($form_id, $obj_cat_id) {
        $return = array();
        $obj_ques = new forms_categories_question_model();
        $get_question = $obj_ques->get_where_sort(
                array('cat_id' => $obj_cat_id->cat_id
                ), 'asc');

        if (count($get_question) > 0) {
            $return['next_cat'] = $obj_cat_id;
            $return['next_question'] = $get_question[0];
            return $return;
        } else {
            $object = new stdClass();
            $object->cat_id = 'end';
            $object->question_id = 'end';
            $return['next_cat'] = $object;
            $return['next_question'] = $object;
            return $return;
        }
    }

    function getPreviousQuestion($category_id, $question_id, $sequence) {
        $obj_ques = new forms_categories_question_model();
        $get_sequence = $obj_ques->get_where_sort(
                array('cat_id' => $category_id, 'sequence <' => $sequence
                ), 'asc');
        if (count($get_sequence) > 0) {
            $obj_cat = new forms_categories_model();
            $get_cat = $obj_cat->get_where_sort(
                    array('cat_id' => $category_id
                    ), 'asc');
            $return['prev_cat'] = $get_cat[0];
            $return['prev_question'] = $get_sequence[count($get_sequence) - 1];
            return $return;
        } else {
            $obj_cat = new forms_categories_model();
            $get_cat = $obj_cat->get_where_sort(
                    array('cat_id' => $category_id
                    ), 'asc');
            $get_next_cat = $obj_cat->get_where_sort(
                    array('form_id' => $get_cat[0]->form_id,
                'sequence <' => $get_cat[0]->sequence
                    ), 'asc');
            if (!empty($get_next_cat)) {
                $get = $this->getPREVCategoryQuestion($get_cat[0]->form_id, $get_next_cat[count($get_next_cat) - 1]);
                $return['prev_cat'] = $get['prev_cat'];
                $return['prev_question'] = $get['prev_question'];
                return $return;
            } else {
                $object = new stdClass();
                $object->cat_id = 'end';
                $object->question_id = 'end';
                $return['prev_cat'] = $object;
                $return['prev_question'] = $object;
                return $return;
            }
        }
    }

    function getPREVCategoryQuestion($form_id, $obj_cat_id) {
        $return = array();
        $obj_ques = new forms_categories_question_model();
        $get_question = $obj_ques->get_where_sort(
                array('cat_id' => $obj_cat_id->cat_id
                ), 'desc');
        if (count($get_question) > 0) {
            $return['prev_cat'] = $obj_cat_id;
            $return['prev_question'] = $get_question[0];
            return $return;
        } else {
            return false;
        }
    }

    function getNextQuestion($category_id, $question_id, $sequence) {
        $obj_ques = new forms_categories_question_model();
        $get_sequence = $obj_ques->get_where_sort(
                array('cat_id' => $category_id, 'sequence >' => $sequence
                ), 'asc');
        if (count($get_sequence) > 0) {
            $obj_cat = new forms_categories_model();
            $get_cat = $obj_cat->get_where_sort(
                    array('cat_id' => $category_id
                    ), 'asc');
            $return['next_cat'] = $get_cat[0];
            $return['next_question'] = $get_sequence[0];

            return $return;
        } else {
            $obj_cat = new forms_categories_model();
            $get_cat = $obj_cat->get_where(
                    array('cat_id' => $category_id
                    ));
            $get_next_cat = $obj_cat->get_where_sort(
                    array('form_id' => $get_cat[0]->form_id,
                'sequence >' => $get_cat[0]->sequence
                    ), 'asc');
            if (!empty($get_next_cat)) {
                $get = $this->getCategoryQuestion($get_cat[0]->form_id, $get_next_cat[0]);
                $return['next_cat'] = $get['next_cat'];
                $return['next_question'] = $get['next_question'];
                return $return;
            } else {
                $object = new stdClass();
                $object->cat_id = 'end';
                $object->question_id = 'end';
                $return['next_cat'] = $object;
                $return['next_question'] = $object;
                return $return;
            }
        }
    }

    function isAnswerGiven($claim_id, $form_id, $question_id) {
        $answer = new temp_forms_answers_model();
        $answers = $answer->get_where(
                array('question_id' => $question_id, 'claim_id' => $claim_id
                ));

        if (!empty($answers)) {
            if ($answers[0]->prev_question != '0' &&
                    $answers[0]->prev_question != NULL) {
                $obj_ques = new forms_categories_question_model();
                $get_question = $obj_ques->get_where(
                        array('question_id' => $answers[0]->prev_question
                        ));
                return base_url() . 'admin/client_forms/preview_question/' .
                        $claim_id . '/' . $form_id . '/' .
                        $get_question[0]->cat_id . '/' .
                        $get_question[0]->question_id;
            } else {
                return 0;
            }
        } else {
            return false;
        }
    }

    function preview_question_start($claim_id, $form_id) {

        $forms = $this->forms_model->selectSingleRecord('form_id', $form_id);
        $form = $forms[0];

        $form_info = $this->forms_model->get_where(
                array('form_id' => $form_id
                ));
        $data['form_value'] = $form_info[0];

        $field_info = $this->form_field_values_model->get_where(
                array('claim_id' => $claim_id
                ));
        $data['field_value'] = $field_info;

        $email_field = $this->form_field_model->getMailFiledFromID($form_id);
        $data['email_field_name'] = @$email_field->field_name;

        $data['total_form_completed'] = $this->getFromProgress($claim_id, $form_id);
        $data['title'] = 'Preview Insurance Form';
        $data['form_id'] = $form_id;
        $data['form'] = $form;
        $data['claim_id'] = $claim_id;
        $data['page_name'] = 'admin/forms/preview_form/show_form_headder';

        $first_question = $this->getFirstCategoryFirstQuestion($form_id);

        if ($first_question !== false) {
            $data['next_question'] = $first_question;
        }

        $this->load->view($this->default_template_name, $data);
    }

    function getFirstCategoryFirstQuestion($form_id) {
        $return = array();
        $obj_cat = new forms_categories_model();
        $get_cat = $obj_cat->get_where_sort(
                array('form_id' => $form_id
                ), 'asc');

        if (count($get_cat) > 0) {
            $obj_ques = new forms_categories_question_model();
            $get_question = $obj_ques->get_where_sort(
                    array('cat_id' => $get_cat[0]->cat_id
                    ), 'asc');

            if (count($get_question) > 0) {
                $return['next_cat'] = $get_cat[0];
                $return['next_question'] = $get_question[0];
                return $return;
                exit();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function getFromProgress($claim_id, $form_id) {
        $obj_cat = new forms_categories_model();
        $get_cat = $obj_cat->get_where_sort(
                array('form_id' => $form_id
                ), 'asc');
        $temp = array();
        $i = 1;
        foreach ($get_cat as $cat) {
            $questions = $this->forms_categories_question_model->get_where_sort(
                    array('cat_id' => $cat->cat_id
                    ), 'asc');
            foreach ($questions as $question) {
                $temp[$i] = $question->question_id;
                $i++;
            }
        }
        $answer = new temp_forms_answers_model();
        $prev_id = $answer->getLastRecordInserted($claim_id);
        $id = NULL;
        if (!empty($prev_id)) {
            $cat_ans = new forms_categories_question_answer_model();
            $cat_answers = $cat_ans->get_where(
                    array('question_id' => $prev_id[0]->question_id
                    ));
            if (count($cat_answers) > 0) {
                foreach ($cat_answers as $ans) {
                    if ($ans->skip_to_questions !== '0') {
                        $id = $prev_id[0]->question_id;
                    } else {
                        $id = $temp[count($temp)];
                    }
                }
            } else {
                $id = $prev_id[0]->question_id;
            }
        } else {
            $id = 0;
        }

        if ($id != 0) {
            $answer_given = array_keys($temp, $id);
            return @round(( $answer_given[0] / count($temp) ) * 100);
        } else {
            return 0;
        }
    }

    function end_preview_one_by_one($claim_id) {
        $this->session->set_flashdata('success', "Form status is now open");

        redirect(base_url() . "admin/client_forms", 'refresh');
    }

    function end_preview_confirm_and_send_one_by_one($claim_id) {
        $this->session->set_flashdata('success', "Formulier is afgerond");

        // $data['html_data'] = $this->generate_html($claim_id);
        // $this->load->view('user/answer/show_html', $data);
        redirect(base_url() . "admin/client_forms");
    }

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                     END SHOW ALL QUESTION ONE BY ONE                    * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /*     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *                                                                         * 
     *                                                                         *
     *                           END OF PREVIEW FORMS                          *
     *                                                                         *
     *                                                                         *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
}
