<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the admin settings and other
 * methods.
 */
class admin_mail_template extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();

        $this->load->model('client_email_template_model');
        $this->load->helper('lgin');
        $this->load->model('client_setting_model');
        $this->default_template_name = get_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $res = $this->client_setting_model->get_where(
                array('client_id' => $client_id
                ));
        $data['default_rows'] = $res[0]->default_rows;

        $data['page_name'] = 'admin/mail_templates/index';
        $data['title'] = 'E-mailsjabloon';

        $this->load->view($this->default_template_name, $data);
    }

    function edit($id) {
        $result = $this->client_email_template_model->get_where(
                array('email_id' => $id
                ));
        if (empty($result)) {
            $this->session->set_flashdata('error', "Error Please Try Again.Mail Template not found");
            redirect(base_url() . 'admin/admin_mail_template');
        }
        $template = $result[0];
        $data['page_name'] = 'admin/mail_templates/edit_email';
        $data['title'] = 'E-Mailsjabloon';
        $data['email_id'] = $id;
        $data['email_template'] = $template;
        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
        $result = $this->client_email_template_model->get_where(
                array('email_id' => $id
                ));
        $template = $result[0];
        // print_r($template);
        // try uploadng the file
        // if file is selected then only the upload will be done
        // it would be success if no file was selested
        $config['upload_path'] = './assets/upload/client_email_attachments';
        $config['allowed_types'] = '*';
        $logo_upload_status = my_file_upload('email_attachment', $config, $this);

        $success = true;

        // if upload was ok, if file was selected, it is uploaded safely ,and if
        // not selected, it is no uploaded and it is fine
        if ($logo_upload_status['status'] != 1) {

            // set the error flash message
            $error['logo'] = $logo_upload_status['data'];
            $this->session->set_flashdata('file_errors', $error);

            $success = false;
            redirect(base_url() . 'admin/admin_mail_template/edit/' . $id);
        }

        // if file upload was failed then we will render the edit form again
        // if file was ok then we will run the form validations
        if ($success == true) {

            // get the validations
            $validation[] = $template->validation_rules[2];
            $validation[] = $template->validation_rules[3];
            $validation[] = $template->validation_rules[4];

            $this->form_validation->set_rules($validation);

            // try to run the validations
            // if validations run safely then save otherwise set success to
            // false
            if ($this->form_validation->run() == FALSE) {
                $success = false;
            } else {

                if (isset($logo_upload_status['data'])) {
                    // if upload was there then unlink the old image
                    // and set the location to new image link

                    $old_location = $template->email_attachment; // the name of
                    // file in
                    // folder

                    $full_path_of_old = './assets/upload/client_email_attachments' .
                            "/" . $old_location; // absolute
                    // path
                    // to
                    // be
                    // removed
                    $path_to_be_removed = substr($full_path_of_old, 2); // remove
                    // ./
                    // from
                    // the
                    // absolute
                    // location
                    unlink($path_to_be_removed);
                    $template->email_attachment = $logo_upload_status['data']; // set
                    // the
                    // new
                    // fiel
                    // location
                }

                // now get the values from the form and set in the form
                // get all the fields from form
                $template->email_name = $this->input->post('email_name');
                $template->email_subject = $this->input->post('email_subject');
                $template->email_message = $this->input->post('email_message');
                $template->email_bcc_admin = $this->input->post(
                                'email_bcc_admin') == 1 ? true : false;
                $template->update(); // save the values
            }
        }

        // if not successful then render the form again
        if ($success == false) {
            // set the values and render the form agian
            $this->form_validation->set_value('email_subject');
            $this->form_validation->set_value('email_message');
            $this->form_validation->set_value('email_bcc_admin');
            $this->edit($id);
        } else {
            // if success then redirect to new page
            $this->session->set_flashdata('success', 'Email Template updated Sucessufully');
            redirect(base_url() . 'admin/admin_mail_template');
        }
        // echo $id;
    }

    function getJson() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $json_data = $this->_transform_for_json(
                $this->client_email_template_model->get_where(
                        array('client_id' => $client_id
                )));

        $data['aaData'] = $json_data;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function _transform_for_json($data) {
        $return_val = array();
        foreach ($data as $row) {

            $row_array = array();
            $client = $row->get_client();

            $client_name = $client->client_name;
            $template_id = $row->email_id;
            $email_type = $row->email_type;
            $email_subject = $row->email_subject;

            $row_array[] = "<a href=\"" . base_url() .
                    "admin/admin_mail_template/edit/" . $row->email_id . "\" >" .
                    $row->email_name . "</a>";
            $row_array[] = $email_subject;
            if ($row->manageable == 1) {
                $row_array[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                        $row->email_id . "'><img src='" . base_url() . "assets/images/icon_delete.png' alt='Delete' title='Delete'></a>";
            } else {
                $row_array[] = NULL;
            }
            $return_val[] = $row_array;
        }

        return $return_val;
    }

    function remove_attachment($id) {

        // get the object from database
        $result = $this->client_email_template_model->get_where(
                array('email_id' => $id
                ));
        $template = $result[0];

        // unlink the image / delete the image
        $old_location = $template->email_attachment;
        $full_path_of_old = './assets/upload/client_email_attachments' . "/" .
                $old_location; // absolute
        // path
        // to
        // be
        // removed
        $path_to_be_removed = substr($full_path_of_old, 2); // remove ./ from
        // the absolute
        // location
        $status = unlink($path_to_be_removed);

        // set the attachment to null and save to database
        $template->email_attachment = null;
        $template->update();
        // / redirect to attachmets page
        redirect(base_url() . 'admin/admin_mail_template');
    }

    function add() {
        $data['page_name'] = 'admin/mail_templates/add_email';
        $data['title'] = 'E-Mailsjabloon';
        $this->load->view($this->default_template_name, $data);
    }

    function addListener() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $template = new client_email_template_model();
        $config['upload_path'] = './assets/upload/client_email_attachments';
        $config['allowed_types'] = '*';
        $logo_upload_status = my_file_upload('email_attachment', $config, $this);

        $success = true;

        // if upload was ok, if file was selected, it is uploaded safely ,and if
        // not selected, it is no uploaded and it is fine
        if ($logo_upload_status['status'] != 1) {

            // set the error flash message
            $error['logo'] = $logo_upload_status['data'];
            $this->session->set_flashdata('file_errors', $error);

            $success = false;
            redirect(base_url() . 'admin/admin_mail_template/add');
        }

        // if file upload was failed then we will render the edit form again
        // if file was ok then we will run the form validations
        if ($success == true) {

            // get the validations
            $validation[] = $template->validation_rules[2];
            $validation[] = $template->validation_rules[3];
            $validation[] = $template->validation_rules[4];
            $this->form_validation->set_rules($validation);

            if ($this->form_validation->run() == FALSE) {
                $success = false;
            } else {

                if (isset($logo_upload_status['data'])) {

                    $template->email_attachment = $logo_upload_status['data']; // set
                }
                $template->email_type = $this->generateRandomString();
                $template->email_name = $this->input->post('email_name');
                // now get the values from the form and set in the form
                // get all the fields from form
                $template->email_subject = $this->input->post('email_subject');
                $template->email_message = $this->input->post('email_message');
                $template->email_bcc_admin = $this->input->post(
                                'email_bcc_admin') == 1 ? true : false;
                $template->client_id = $client_id;
                $template->manageable = 1;
                $template->email_dynamic_fields = '<link>,<logo>,<voornaam>,<tussenvoegsel>,<achternaam>,<bedrijf>';
                $template->save(); // save the values
            }
        }

        // if not successful then render the form again
        if ($success == false) {
            // set the values and render the form agian
            $this->form_validation->set_value('email_name');
            $this->form_validation->set_value('email_subject');
            $this->form_validation->set_value('email_message');
            $this->form_validation->set_value('email_bcc_admin');
            $this->add();
        } else {
            // if success then redirect to new page
            $this->session->set_flashdata('success', 'Email Template added Sucessufully');
            redirect(base_url() . 'admin/admin_mail_template');
        }
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function delete($id) {
        $this->client_email_template_model->email_id = $id;
        $this->client_email_template_model->delete($id);
        $this->session->set_flashdata('success', 'Record deleted Sucessufully.');
        redirect('admin/admin_mail_template', 'refresh');
    }

}

?>
