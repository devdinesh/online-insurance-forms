<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class deals with nwez
 */
class newletter extends CI_Controller {

    /**
     *
     * @var String template name whis is rendered.
     */
    var $default_template_name;

    /**
     * Default constructor loads the models and set ups template name.
     */
    function __construct() {
        parent::__construct();
        $this->load->model('newletter_model');
        $this->load->model('article_model');
        $this->load->model('newsletter_article_model');
        $this->load->model('newsletter_template_model');
        $this->load->model('send_newletter_model');

        $this->default_template_name = get_admin_template_name();
    }

    public function index() {
        $data['page_name'] = 'admin/newletter/index';
        $data['title'] = 'Nieuwsbrieven';
        $this->load->view($this->default_template_name, $data);
    }

    /*
     * renders json for the list newz page
     */

    public function get_json() {
        $admin = $this->session->userdata('admin');
        $client_id = $admin['client_id'];

        $newletter_model = new newletter_model();
        $newletters = $newletter_model->get_where(array('client_id' => $client_id));
        $main_arr = array();
        foreach ($newletters as $newletter) {
            $temp = array();
            $temp[] = anchor(base_url() . 'admin/newletter/edit/' . $newletter->id, $newletter->title);
            $temp[] = $newletter->form_code;
            $action = "<a  href=\"" . base_url() . "admin/newletter/manage/" . $newletter->id . "\" id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"><img src='" . assets_url_img . "edit.png' alt='Manage Articles' title='Manage Articles'></a> &nbsp;";
            $action .= " <a  href=\"" . base_url() . "admin/send_newletters/" . $newletter->id . "\" id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Stuur nieuwsbrieven\"><img src='" . assets_url_img . "open.png' alt='Stuur nieuwsbrieven' title='Stuur nieuwsbrieven'></a>  &nbsp;";
            $action .= "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $newletter->id . "'><img src='" . assets_url_img .
                    "delete.png' alt='Delete' title='Delete'></a>";
            $temp[] = $action;
            $main_arr[] = $temp;
        }
        $data['aaData'] = $main_arr;
        echo json_encode($data);
    }

    public function add($error = NULL) {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $data['templates'] = $this->newsletter_template_model->get_where(array('client_id' => $client_id));
        $data['page_name'] = 'admin/newletter/add';
        $data['error'] = $error;
        $data['title'] = 'Toevoegen Nieuwsbrief ';

        $this->load->view($this->default_template_name, $data);
    }

    public function add_listener() {
        $newletter = new newletter_model();
        $rules = $newletter->validation_rules;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            $this->add();
        } else {
            $nb = $result = substr($this->input->post('form_code'), 0, 2);
            if ($nb == 'NB') {
                $admin = $this->session->userdata('admin');
                $client_id = $admin['client_id'];
                $newletter->client_id = $client_id;
                $newletter->title = $this->input->post('title');
                $newletter->form_code = trim($this->input->post('form_code'));
                if ($this->input->post('template')) {
                    $newletter->template = $this->input->post('template');
                } else {
                    $newletter->template = NULL;
                }

                $newletter->introduction_text = $this->input->post('introduction_text');
                $newletter->closure_text = $this->input->post('closure_text');
                $newletter->save();
                $this->session->set_flashdata('success', "Nieuwsbrief is toegevoegd");
                redirect(base_url('admin/newletter'));
            } else {
                $this->add("De code moet beginnen met 'NB'.");
            }
        }
    }

    public function edit($id, $error = NULL) {

        $admin = $this->session->userdata('admin');
        $client_id = $admin['client_id'];

        $newletter = new newletter_model();
        $newzs = $newletter->get_where(array('id' => $id));

        if (count($newzs) == 0) {
            $this->session->set_flashdata('error', "No such Newsletter exists");
            redirect(base_url('admin/newletter'));
        }

        $news = $newzs[0];
        if ($news->client_id != $client_id) {
            $this->session->set_flashdata('error', "Can not access that");
            redirect(base_url('admin/newletter'));
        }
        $data['error'] = $error;
        $data['templates'] = $this->newsletter_template_model->get_where(array('client_id' => $client_id));
        $data['page_name'] = 'admin/newletter/edit';
        $data['title'] = 'Bewerk Nieuwsbrief';
        $data['newletter'] = $news;

        $this->load->view($this->default_template_name, $data);
    }

    public function edit_listener($id) {
        $admin = $this->session->userdata('admin');

        $client_id = $admin['client_id'];

        $newletter = new newletter_model();
        $newzs = $newletter->get_where(array('id' => $id));

        if (count($newzs) == 0) {
            $this->session->set_flashdata('error', "No such newletters exists");
            redirect(base_url('admin/newletter'));
        }

        $news = $newzs[0];
        if ($news->client_id != $client_id) {
            $this->session->set_flashdata('error', "Can not access that");
            redirect(base_url('admin/newletter'));
        }

        $rules = $newletter->validation_rules;
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            $this->edit($id);
        } else {
            $nb = $result = substr($this->input->post('form_code'), 0, 2);
            if ($nb == 'NB') {
                $admin = $this->session->userdata('admin');
                $client_id = $admin['client_id'];
                $newletter->id = $id;
                $newletter->client_id = $client_id;
                $newletter->title = $this->input->post('title');
                $newletter->form_code = trim($this->input->post('form_code'));
                if ($this->input->post('template')) {
                    $newletter->template = $this->input->post('template');
                } else {
                    $newletter->template = NULL;
                }
                $newletter->introduction_text = $this->input->post('introduction_text');
                $newletter->closure_text = $this->input->post('closure_text');
                $newletter->update();
                $this->session->set_flashdata('success', "Nieuwsbrief Bijgewerkt");
                redirect(base_url('admin/newletter'));
            } else {
                $this->edit($id, "De code moet beginnen met 'NB'.");
            }
        }
    }

    function delete($id) {
        $this->newletter_model->id = $id;
        $this->newletter_model->delete();
        $this->session->set_flashdata('success', "Newsletter deleted sucessfully");
        redirect('admin/newletter', 'refresh');
    }

    function manage($id) {
        $data['page_name'] = 'admin/newletter/manage_article';
        $data['title'] = 'Beheer Nieuwsbrief Artikelen';
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $data['articles'] = $this->article_model->get_where(array('client_id' => $client_id));
        $data['newsletter_article'] = $this->newsletter_article_model->getNewsArticle($id);
        $data['id'] = $id;
        $this->load->view($this->default_template_name, $data);
    }

    function getJsonManage($id) {
        $this->load->library('datatable');
        $this->datatable->aColumns = array('na.id', 'na.newsletter_id', 'na.article_id', 'na.sequence', 'n.title AS letter_title', 'a.title AS article_title');
        $this->datatable->eColumns = array('na.id');
        $this->datatable->sIndexColumn = "na.id";
        $this->datatable->sTable = " ins_newsletters_articles na, ins_newsletters n,  ins_articles a";
        $this->datatable->myWhere = "WHERE n.id=" . $id . " AND n.client_id=" . admin_get_client_id()->client_id . " AND a.id = na.article_id AND n.id = na.newsletter_id ORDER BY na.sequence ASC";
        $this->datatable->datatable_process();
//        echo "<br>";
//        var_dump($this->datatable->rResult->result_array());
//        exit;

        foreach ($this->datatable->rResult->result_array() as $aRow) {
            $temp_arr = array();
            $temp_arr[] = $aRow['letter_title'];
            $temp_arr[] = $aRow['article_title'];
            $temp_arr[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $aRow['id'] . "'><img src='" . assets_url_img .
                    "delete.png' alt='Delete' title='Delete'></a>";
            $temp_arr[] = 'new_articles_' . $aRow['id'];
            $this->datatable->output['aaData'][] = $temp_arr;
        }
        echo json_encode($this->datatable->output);
        exit();
    }

    function addArticle() {
        $articles = $this->input->post('selected_article');
        $newsletter_id = $this->input->post('newsletter_id');
        $old_article = $data['newsletter_article'] = $this->newsletter_article_model->getNewsArticle($newsletter_id);
        $article_delete = array_diff($old_article, $articles);
        $article_add = array_diff($articles, $old_article);
        if (!empty($article_delete)) {
            $this->newsletter_article_model->deleteMultiplearticle($article_delete, $newsletter_id);
        }
        if (!empty($article_add)) {
            $insert_data = array();
            $newseq_id = $this->newsletter_article_model->getNewSequenceId($newsletter_id);
            foreach ($article_add as $key => $value) {
                $insert_data[$key] = array(
                    'newsletter_id' => $newsletter_id,
                    'article_id' => $value,
                    'sequence' => $newseq_id,
                );
                $newseq_id++;
            }
            $this->newsletter_article_model->addMultipleArticle($insert_data);
        }
        $this->session->set_flashdata('success', "Newsletter articles Updated sucessfully.");
        redirect('admin/newletter/manage/' . $newsletter_id, 'refresh');
    }

    function delete_article($id) {
        $newsletter_article = $this->newsletter_article_model->get_where(array('id' => $id));
        $newletter_id = $newsletter_article[0]->newsletter_id;
        $this->newsletter_article_model->id = $id;
        $this->newsletter_article_model->delete();
        $this->session->set_flashdata('success', "Article deleted sucessfully");
        redirect('admin/newletter/manage/' . $newletter_id, 'refresh');
    }

    function sortable() {
        $obj = new newsletter_article_model();
        $new_articles = $_POST['new_articles'];
        $temp = 1;
        $get_news_article_info = $obj->get_where(array('id' => $new_articles[0]));
        $get_all_articles = $obj->get_where_sort(array('newsletter_id' => $get_news_article_info[0]->newsletter_id), 'asc');
        $org_article_array = array();
        foreach ($get_all_articles as $article) {
            $org_article_array[] = $article->id;
        }
        for ($i = 0; $i < count($org_article_array); $i++) {
            if ($org_article_array[$i] == $new_articles[$i]) {
                continue;
            } else {
                $temp = 1;
            }
        }
        if ($temp == 1) {
            $obj->updateAllSequence($new_articles);
            $this->session->set_flashdata('success', 'Sequence of the Newsletter articles are changed Sucessufully.');
            return true;
        } else {
            $this->session->set_flashdata('error', 'Error..Please try again.');
            return false;
        }
    }

    function send_newletters($id) {
        $data['page_name'] = 'admin/newletter/send_newsletter';
        $data['title'] = 'Verstuurde nieuwsbrieven';
        $data['id'] = $id;
        $this->load->view($this->default_template_name, $data);
    }

    public function getSendNewletterJson($id) {
        $this->load->library('datatable');
        $this->datatable->aColumns = array('sn.id', 'sn.newsletter_id', 'sn.date', 'sn.time', 'sn.mail_address', 'sn.name', 'n.title');
        $this->datatable->eColumns = array('sn.id');
        $this->datatable->sIndexColumn = "sn.id";
        $this->datatable->sTable = " ins_send_newsletters sn, ins_newsletters n";
        $this->datatable->myWhere = "WHERE sn.newsletter_id=" . $id . " AND sn.newsletter_id = n.id";
        $this->datatable->datatable_process();
        foreach ($this->datatable->rResult->result_array() as $aRow) {
            $temp_arr = array();
            $temp_arr[] = $aRow['title'];
            $temp_arr[] = $aRow['name'];
            $temp_arr[] = $aRow['mail_address'];
            $temp_arr[] = date('d-m-Y', strtotime($aRow['date']));
            $temp_arr[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $aRow['id'] . "'><img src='" . assets_url_img .
                    "delete.png' alt='Delete' title='Delete'></a>";
            $this->datatable->output['aaData'][] = $temp_arr;
        }
        echo json_encode($this->datatable->output);
        exit();
    }

    function delete_sendnewletter($id) {
        $send_newsletter = $this->send_newletter_model->get_where(array('id' => $id));
        $newletter_id = $send_newsletter[0]->newsletter_id;
        $this->send_newletter_model->id = $id;
        $this->send_newletter_model->delete();
        $this->session->set_flashdata('success', "Record deleted sucessfully");
        redirect('admin/send_newletters/' . $newletter_id, 'refresh');
    }

}