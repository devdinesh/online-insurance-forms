<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class manages the Admin > text Menu.
 */
class feedback extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('feedback_category_model');
        $this->load->model('feedback_record_model');
        $this->load->helper('time_worked');
        $this->default_template_name = get_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function show_panel() {
        $data = array();
        $data['feedback_cat_details'] = $this->feedback_category_model->get_all();
        $this->load->view('feedback/panel_1', $data);
    }

    public function save_feedback_1() {
        $post = $this->input->post();
        $this->load->view('feedback/panel_2', $post);
    }

    public function save_feedback_2() {
        $obj_feedback = new feedback_record_model();
        $admin_array = $this->session->userdata('admin');
        $user_id = $admin_array['user_id'];
        if ($user_id == NULL) {
            $feed_user_id = 0;
            $feed_username = "Guest";
        } else {
            $feed_user_id = $user_id;
            $feed_username = $admin_array['first_name'] . ' ' . $admin_array['last_name'];
        }
        $obj_feedback->feed_user_id = $feed_user_id;
        $obj_feedback->feed_username = $feed_username;
        $obj_feedback->feed_email = $this->input->post('feed_email');
        $obj_feedback->feed_cat_id = $this->input->post('feed_cat_id');
        $obj_feedback->feed_rating = $this->input->post('feed_rating');
        $obj_feedback->feed_remark = $this->input->post('feed_remark');
        $obj_feedback->feed_created = get_current_time()->get_date_for_db();
        $check = $obj_feedback->save();
        if (isset($check) && $check != '') {
            //send mail to the email address
        }
        $this->load->view('feedback/panel_3');
    }

}

?>
