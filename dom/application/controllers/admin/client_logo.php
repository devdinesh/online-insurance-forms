<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the admin and process the login and other
 * methods.
 */
class client_logo extends CI_Controller
{

    var $default_template_name;

    function __construct ()
    {
        parent::__construct();
        $this->load->helper('lgin');
        $this->load->model('clients_model');
        $this->load->model('my_file_upload');
        $this->default_template_name = get_admin_template_name();
    
    }

    public function index ()
    {
        $this->edit();
    
    }

    /**
     * renders the edit page for the client whos id is passed
     *
     * @param int $id
     *            is the id of client in the database
     */
    function edit ()
    {
        $data['page_name'] = 'admin/settings/edit_admin';
        $data['title'] = 'Edit admin';
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $data['client_id'] = $client_id;
        $res = $this->clients_model->get_where(
                array('client_id' => $client_id
                ));
        $data['client'] = $res[0];
        $this->load->view($this->default_template_name, $data);
    
    }
    
    /*
     * action listener for edit page
     */
    function editListener ( $id )
    {
        // get the object and validations rules
        $client = new clients_model();
        $rules_old = $client->validation_rules;
        $rules = $rules_old[5];
        // try uploadng the file
        // if file is selected then only the upload will be done
        // it would be success if no file was selested
        $logo_upload_status = my_file_upload('logo', 
                $client->logo_upload_config(), $this);
        
        // if file was uploaded
        if ($logo_upload_status['status'] == 0)
        {
            // set the error message for fors in the flash
            // $error = array ();
            // $error ['logo'] = $logo_upload_status ['data'];
            $success = false;
            $this->session->set_flashdata('error', 
                    "The filetype you are attempting to upload is not allowed.");
            redirect(base_url() . "admin/client_logo");
        }
        else
        {
            $success = true;
        }
        
        // if upload was ok
        if ($success == true)
        {
            // get the data to be updated
            $client = new clients_model();
            $client = $client->get_where(
                    array('client_id' => $id
                    ));
            
            // get the first result as where return multiple things
            $client = $client[0];
            
            // remove the previous file if exists
            if (isset($logo_upload_status['data']))
            {
                if (isset($client->logo) && $client->logo != '')
                {
                    $path_to_be_removed = substr(
                            $client->logo_upload_path . "/" . $client->logo, 2);
                    unlink($path_to_be_removed);
                }
            }
            
            // get the path to new fiel uploded
            if (!isset($logo_upload_status['data']))
            {
                // if no file was chosen then the path would not be changed
                $logo_upload_status['data'] = $client->logo;
                $this->session->set_flashdata('error', 
                        "pleaase upload the new logo");
                redirect(base_url() . "admin/client_logo");
            }
            $new_file_path = $logo_upload_status['data'];
            
            $client->logo = $new_file_path; // set the value to newly
                                            // uploaded file's path
                                            
            // update the value to database
            $val = $client->update();
            $this->session->set_flashdata('success', "Logo Edited Sucessufully");
            redirect(base_url() . "admin");
        }
    
    }

}

?>
