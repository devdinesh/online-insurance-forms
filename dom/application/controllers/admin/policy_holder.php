<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class policy_holder extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('policy_holder_model');
        $this->load->helper('lgin');
        $this->load->model('clients_model');
        $this->load->model('client_setting_model');
        $this->load->model('claims_model');
        $this->default_template_name = get_admin_template_name();
        //$this->output->enable_profiler(TRUE);
    }

    public function index() {
        $data['page_name'] = 'admin/policy_holder/view_policy_holder';
        $data['title'] = 'Polishouders';
        $data['setting'] = $this->client_setting_model->get_where(array('client_id' => admin_get_client_id()->client_id));
        $this->load->view($this->default_template_name, $data);
    }

    function edit($id) {
        $data['page_name'] = 'admin/policy_holder/edit_policy_holder';
        $data['title'] = 'Details polishouderr';
        $data['policy_holder_id'] = $id;
        $res = $this->policy_holder_model->get_where(
                array('policy_holder_id' => $id
                ));

        if (empty($res)) {
            $this->session->set_flashdata('error', "Error!! May Be relatie Not Found");
            redirect(base_url() . "admin/policy_holoder");
        }
        $data['policy_holder'] = $res[0];

        $this->load->view($this->default_template_name, $data);
    }

    /**
     * this will handel all the data post by the add method form.
     */
    function editListener($policy_holder_id) {
        $obj = new policy_holder_model();
        $res = $this->policy_holder_model->get_where(
                array('policy_holder_id' => $policy_holder_id
                ));
        if (count($res) == 1) {
            $validation = $obj->validation_rules;
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules($validation);
            if ($this->form_validation->run() == FALSE) {

                $this->form_validation->set_value('f_name');
                $this->form_validation->set_value('m_name');
                $this->form_validation->set_value('l_name');
                $this->form_validation->set_value('address');
                $this->form_validation->set_value('email');
                $this->form_validation->set_value('zipcode');
                $this->form_validation->set_value('p_number');
                $this->form_validation->set_value('b_number');
                $this->form_validation->set_value('m_number');

                $this->edit($policy_holder_id);
            } else {
                $obj->client_id = $res[0]->client_id;
                $obj->first_name = $this->input->post('f_name');
                $obj->middle_name = $this->input->post('m_name');
                $obj->last_name = $this->input->post('l_name');
                $obj->email = $this->input->post('email');
                $obj->address = $this->input->post('address');
                $obj->zipcode = $this->input->post('zipcode');
                $obj->private_number = $this->input->post('p_number');
                $obj->business_number = $this->input->post('b_number');
                $obj->mobile_number = $this->input->post('m_number');
                $obj->password_reset_random_string = NULL;
                $obj->policy_holder_id = $res[0]->policy_holder_id;
                if ($this->input->post('status') == 'A') {
                    $obj->status = 'A';
                } else {
                    $obj->status = 'D';
                }

                $obj->register_date = date("Y-m-d", $res[0]->register_date);
                if (trim($this->input->post('passward')) != '') {
                    $obj->password = md5($this->input->post('passward'));
                }

                $obj->update();
                // echo "record updated sucessfully";

                $this->session->set_flashdata('success', "Relatie Updated sucessfully");
                redirect(base_url() . "admin/policy_holder");
            }
        } else {
            redirect(base_url() . "admin/policy_holder");
        }
    }

    function getJson() {
        $this->load->library('datatable');
        $this->datatable->aColumns = array('p.first_name', 'p.last_name', 'p.email');
        $this->datatable->eColumns = array('p.policy_holder_id', 'CASE WHEN (cm.claim_id IS NULL) THEN 0 ELSE count(p.policy_holder_id) END AS total');
        $this->datatable->sIndexColumn = "p.policy_holder_id";
        $this->datatable->sTable = "ins_policy_holder p left join ins_claims cm on p.policy_holder_id=cm.policy_holder_id";
        $this->datatable->myWhere = "WHERE p.client_id=" . admin_get_client_id()->client_id . " AND cm.is_delete=0";
        $this->datatable->groupBy = 'group by p.policy_holder_id';
        $this->datatable->datatable_process();
        foreach ($this->datatable->rResult->result_array() as $aRow) {
            $row = array();

            $row[] = $aRow['first_name'] . ' ' . $aRow['last_name'];
            $row[] = $aRow['email'];
            if ($aRow['total'] > 0) {
                $row[] = " <a  href=\"" . base_url() . "admin/list_insurance/index/" . $aRow['policy_holder_id'] . "\" id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"><img src='" . assets_url_img . "icon_forms.png' alt='Toon formulieren' title='Toon formulieren'></a>";
            } else {
                $row[] = '';
            }

            $row[] = "<a  href=\"" . base_url() . "admin/policy_holder/edit/" . $aRow['policy_holder_id'] . "\" id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"><img src='" . assets_url_img . "edit.png' alt='Wijzig gegevens polishouder' title='Wijzig gegevens polishouder'></a>";

            $row[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" . $aRow['policy_holder_id'] . "'><img src='" . assets_url_img . "delete.png' alt='Verwijder polishouder' title='Verwijder polishouder'></a>";

            $this->datatable->output['aaData'][] = $row;
        }
        echo json_encode($this->datatable->output);
        exit();
    }

    function getArrayForJson($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $temp_arr = array();
            $temp_arr[] = $value->first_name . ' ' . $value->last_name;
            $temp_arr[] = $value->email;
            $res = $this->claims_model->get_where(array('policy_holder_id' => $value->policy_holder_id, 'is_delete' => '0'));
            if (!empty($res)) {
                $temp_arr[] = " <a  href=\"" . base_url() .
                        "admin/list_insurance/index/" . $value->policy_holder_id . "\" id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"><img src='" . assets_url_img . "icon_forms.png' alt='Toon formulieren' title='Toon formulieren'></a>";
            } else {
                $temp_arr[] = '';
            }
            $temp_arr[] = " <a  href=\"" . base_url() . "admin/policy_holder/edit/" . $value->policy_holder_id . "\" id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"><img src='" . assets_url_img . "edit.png' alt='Wijzig gegevens polishouder' title='Wijzig gegevens polishouder'></a>
                             ";
            $temp_arr[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $value->policy_holder_id . "'><img src='" . assets_url_img .
                    "delete.png' alt='Verwijder polishouder' title='Verwijder polishouder'></a>";
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function delete($id) {
        $this->policy_holder_model->policy_holder_id = $id;
        $this->policy_holder_model->delete();
        $this->session->set_flashdata('success', "policy Holder deleted sucessfully");
        redirect('admin/policy_holder', 'refresh');
    }

    function view_policy_holder($policy_holder_id) {
        // $policy_holder_info=$this->policy_holder_model(array('policy_holder_id'=>$policy_holder_id));
        // echo $policy_holder_id;
        //exit;\
        $data['policy_holder_id'] = $policy_holder_id;
        $data['page_name'] = 'admin/policy_holder/list_insurance';
        $data['title'] = 'Polishouders';
        $this->load->view($this->default_template_name, $data);
    }

    function getJson_policy($policy_holder_id) {
        $this->load->model('claims_model');
        $claim = new claims_model();
        $claims = $claim->get_where(
                array('policy_holder_id' => $policy_holder_id
                ));
        $arr = $this->_get_array_for_json($claims);
        $data['aaData'] = $arr;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function _get_array_for_json($objects) {
        $arra = array();

        foreach ($objects as $value) {
            $temp_arr = array();

            $temp_arr[] = $value->import_date;

            $temp_arr[] = $value->get_form_number();
            $form_info = $this->forms_model->get_where(
                    array('form_id' => $value->form_id
                    ));
            if (!empty($form_info)) {
                $temp_arr[] = $form_info[0]->form_name;
            } else {
                $temp_arr[] = null;
            }

            $policy_holders = new policy_holder_model();
            $policy_holders = $policy_holders->get_where(
                    array('policy_holder_id' => $value->policy_holder_id
                    ));
            if (count($policy_holders) > 0) {
                $policy_holder = $policy_holders[0];
            }

            if (isset($policy_holder) && $policy_holder->policy_number != NULL) {
                $temp_arr[] = @$policy_holder->policy_number;
            } else {
                $temp_arr[] = null;
            }

            $temp_arr[] = ucwords($value->handler);


            if ($value->status == "Nieuw") {
                $temp_arr[] = '<img style="height: 35px;" src="' . assets_url_img .
                        'nieuw.png' . '" alt="Nieuw" title="Nieuw"/>';
            } elseif ($value->status == "open") {
                $temp_arr[] = '<img style="height: 35px;" src="' . assets_url_img .
                        'open.png' . '" alt="open" title="open"/>';
            } elseif ($value->status == "Afgerond") {
                $temp_arr[] = '<img style="height: 35px;" src="' . assets_url_img .
                        'afgerond.png' . '" alt="Afgerond" title="Afgerond"/>';
            } else {
                $temp_arr[] = '';
            }
            $temp_arr[] = '<a href="#">Details</a>';
            if ($value->status == "Afgerond") {
                $temp_arr[] = anchor(
                        base_url() . 'assets/claims_pdf_files/' .
                        $value->get_form_number() . '_Digitaal_op_Maat.pdf', '<img style="height: 35px;" src="' .
                        assets_url_img . 'PDF-Document-icon.png' .
                        '" alt="alt"/>', 'target="_blank"');
            } else {
                $temp_arr[] = '<a href="#"><img style="height: 35px;" src="' . assets_url_img . 'edit_claim.png' . '" alt="Edit" title="Edit"/></a>';
            }

            $arra[] = $temp_arr;
        }
        return $arra;
    }

}

?>
