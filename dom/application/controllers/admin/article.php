<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class article extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('article_model');
        $this->load->model('my_file_upload');

        $this->default_template_name = get_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
        $data['page_name'] = 'admin/article/view_article';
        $data['title'] = 'Artikelen';
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * a method that renders the json containing list of all the clients
     * that can be understood by the jquery data table
     */
    function getJsonArticle() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $records = $this->article_model->get_where(array('client_id' => $client_id));
        $array = $this->_get_array_for_json($records);
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    /**
     * takes array objects and converts it to an array of array
     * that is appropriate for rendering in json
     *
     * @param unknown_type $objects            
     * @return multitype:multitype:string NULL
     */
    function _get_array_for_json($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() . 'admin/article/edit/' .
                    $value->id . '">' . $value->title . '</a>';

            $temp_arr[] = '<a href="javascript:;" onclick="deleteRow(' .
                    $value->id .
                    ')" class="deletepage"  data-toggle="tooltip" data-placement="top" title="Delete"><img src="' . assets_url_img . 'delete.png" alt="Delete" title="Delete"></a>';
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function add($image_error = null) {
        $data['page_name'] = 'admin/article/add_article';
        $data['title'] = 'Toevoegen Artikel';
        $data['image_error'] = $image_error;
        $this->load->view($this->default_template_name, $data);
    }

    function addListener() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $article = new article_model();

        $rules = $article->validation_rules;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('title');
            $this->form_validation->set_value('content');
            $this->add();
        } else {
            if ($_FILES['image']['name'] != '') {
                $upload_status = $this->do_upload('image');
                if (isset($upload_status['upload_data'])) {
                    if ($upload_status['upload_data']['file_name'] != '') {
                        $article->image = str_replace(' ', '_', $upload_status['upload_data']['file_name']);
                    }
                    $check = true;
                } elseif (isset($upload_status['error'])) {
                    $this->form_validation->set_value('title');
                    $this->form_validation->set_value('content');
                    $check = $upload_status['error'];
                }
            } else {
                $check = true;
            }
            if ($check === true) {
                $article->title = $this->input->post('title');
                $article->content = $this->input->post('content');
                $article->client_id = $client_id;

                $article->save();
                $this->session->set_flashdata('success', "Article was added successfully");
                redirect(base_url() . 'admin/article', 'refresh');
            } else {
                $this->add($check);
            }
        }
    }

    function edit($id) {
        $data['page_name'] = 'admin/article/edit_article';
        $data['title'] = 'Details Artikel';
        $data['id'] = $id;
        $res = $this->article_model->get_where(
                array('id' => $id
                ));

        if (empty($res)) {
            $this->session->set_flashdata('error', "Error !! May Be Article not found");
            redirect(base_url() . "admin/article");
        } else {
            $data['article'] = $res[0];
        }
        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $article = new article_model();

        $error = array();

        // try uploadng the file
        // if file is selected then only the upload will be done
        // it would be success if no file was selested
        $logo_upload_status = my_file_upload('image', $article->logo_upload_config(), $this);

        // if file was uploaded
        if ($logo_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $error = array();
            $error['image'] = $logo_upload_status['data'];
            $success = false;
            $this->session->set_flashdata('file_errors', $error);
        } else {
            $success = true;
        }
        $rules = $article->validation_rules;

        // if upload was ok
        if ($success == true) {
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE) {
                $this->edit($id);
            } else {
                $article = $article->get_where(array('id' => $id));

                // get the first result as where return multiple things
                $article = $article[0];

                // remove the previous file if exists
                if (isset($logo_upload_status['data'])) {
                    if (isset($article->image) &&
                            $article->image != '') {
                        $path_to_be_removed = substr(
                                $article->logo_upload_path . "/" .
                                $article->image, 2);
                        unlink($path_to_be_removed);
                    }
                }

                // get the path to new fiel uploded
                if (!isset($logo_upload_status['data'])) {
                    // if no file was chosen then the path would not be changed
                    $logo_upload_status['data'] = $article->image;
                }
                $new_file_path = $logo_upload_status['data'];
                // get all the fields from form
                $article->title = $this->input->post('title');
                $article->content = $this->input->post('content');

                $article->image = $new_file_path; // set the value to
                // newly
                // uploaded file's path
                // update the value to
                // database
                $article->update();

                $this->session->set_flashdata('success', "Article was updated successfully");
                redirect(base_url() . "admin/article");
                // print_r($client);
            }
        } else {

            $this->form_validation->set_value('title');
            $this->form_validation->set_value('content');
            $this->edit($id);
        }
    }

    function do_upload($field) {
        list($width, $height, $type, $attr) = getimagesize($_FILES[$field]['tmp_name']);
        $config['upload_path'] = './assets/upload/article_image';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['overwrite'] = FALSE;
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($field)) {
            $data = array('error' => $this->upload->display_errors()
            );
        } else {
            $data = array('upload_data' => $this->upload->data($field)
            );
            $image = $data['upload_data']['file_name'];

            $this->load->helper('image_manipulation/image_manipulation');
            include_lib_image_manipulation();
            $magicianObj = new imageLib('./assets/upload/article_image/' . $image);
            $magicianObj->resizeImage($width, $height, 'auto');
            $magicianObj->saveImage('./assets/upload/article_image/' . $image, 100);

            $config['image_library'] = 'GD2';
            $config['source_image'] = './assets/upload/article_image/' . $image;
            $config['new_image'] = './assets/upload/article_image/';
            $config['create_thumb'] = false;
            $config['maintain_ratio'] = false;
            $config['master_dim'] = 'width';
            $config['width'] = $width;
            $config['height'] = $height;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->image_lib->clear();
        }

        return $data;
    }

    function delete($id) {
        $res = $this->article_model->get_where(  array('id' => $id));
        $article = $res[0];
        $path_to_be_removed = substr(
                $article->logo_upload_path . "/" . $article->image, 2);
        unlink($path_to_be_removed);
        $article->delete();
        $this->session->set_flashdata('success', "Article was deleted successfully");
        redirect(base_url() . "admin/article");
    }

}