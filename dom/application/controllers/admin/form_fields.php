<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Details with Forms>Form_fields.
 */
class form_fields extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('form_field_model');
        $this->default_template_name = get_admin_template_name();
    }

    public function index($id) {
        $data['page_name'] = 'admin/forms/fields/view_field';
        $data['title'] = 'field list';
        $data['form_id'] = $id;
        $this->load->view($this->default_template_name, $data);
        // $this->add();
    }

    function getJson($form_id) {
        $obj = new form_field_model();
        $records = $obj->get_where(array('form_id' => $form_id));
        $array = $this->getArrayForJson($records);
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function getArrayForJson($objects) {
        $arra = array();
        foreach ($objects as $value) {

            $temp_arr = array();
            $temp_arr[] = '<a href="' . base_url() . 'admin/form_fields/edit/' . $value->field_id . '">' . $value->field_name . '</a>';
            $temp_arr[] = $value->name_on_form;
//            if ($value->field_type == 'E') {
//                $temp_arr[] = '<span class="label span2 label-warning text-center">E-Mail</span>';
//            } else if ($value->field_type == 'P') {
//                $temp_arr[] = '<span class="label span2 label-success text-center">Polisnummer</span>';
//            } else if ($value->field_type == 'B') {
//                $temp_arr[] = '<span class="label span2 label-info text-center">Behandelaar</span>';
//            } else if ($value->field_type == 'S') {
//                $temp_arr[] = '<span class="label span2 label-inverse text-center">Schadenummer</span>';
//            } else if ($value->field_type == 'N') {
//                $temp_arr[] = '<span class="label span2 text-center">Nomaal</span>';
//            } else if ($value->field_type == 'BE') {
//                $temp_arr[] = '<span class="label span2 label-info text-center">Behandelaar E-mail</span>';
//            }
            if ($value->field_type == 'E') {
                $temp_arr[] = '<span class="span2">E-Mail</span>';
            } else if ($value->field_type == 'P') {
                $temp_arr[] = '<span class="span2">Polisnummer</span>';
            } else if ($value->field_type == 'B') {
                $temp_arr[] = '<span class="span2">Behandelaar</span>';
            } else if ($value->field_type == 'S') {
                $temp_arr[] = '<span class="span2">Schadenummer</span>';
            } else if ($value->field_type == 'N') {
                $temp_arr[] = '<span class="span2">Nomaal</span>';
            } else if ($value->field_type == 'BE') {
                $temp_arr[] = '<span class="span2">Behandelaar E-mail</span>';
            } else {
                $temp_arr[] = '';
            }

            $temp_arr[] = "<div class = \"btn-group pull-right\"><a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" . $value->field_id . "' id=\"tool\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"><button data-original-title=\"Cancel\"><i class=\"icon-trash\"></i></button></a></div>";

            $arra[] = $temp_arr;
        }
        return $arra;
    }

    /**
     * this funcrtion will render the Add Form
     */
    function add($form_id) {
        $data['page_name'] = 'admin/forms/fields/add_field';
        $data['title'] = 'Forms Fields';
        $data['form_id'] = $form_id;
        $data['show_mail'] = $this->form_field_model->isMailFiledExit($form_id);
        $data['show_ploicy_number'] = $this->form_field_model->isPolicyNumberFiledExit($form_id);
        $data['show_behandler'] = $this->form_field_model->isBehandlerFiledExit($form_id);
        $data['show_schadenummer'] = $this->form_field_model->isschadenummerFiledExit($form_id);
        $data['show_behandler_emial'] = $this->form_field_model->isBehandlerEmailExit($form_id);
        $this->load->view($this->default_template_name, $data);
    }

    function addListener($id) {
        $obj = new form_field_model();
        $validation = $obj->validation_rules;

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == FALSE) {

            $this->form_validation->set_value('field_name');
            $this->form_validation->set_value('name_on_form');
            $this->form_validation->set_value('field_type');

            $this->add($id);
        } else {
            $obj->field_name = $this->input->post('field_name');
            $obj->name_on_form = $this->input->post('name_on_form');

            if ($this->input->post('field_type') != '') {
                $obj->field_type = $this->input->post('field_type');
            } else {
                $obj->field_type = 'N';
            }
            $obj->form_id = $id;
            $obj->save();
            $this->session->set_flashdata('success', "Field added sucessfully");
            redirect('admin/form_fields/' . $id, 'refresh');
        }
    }

    function edit($id) {
        $data['page_name'] = 'admin/forms/fields/edit_field';
        $data['title'] = 'Edit field';
        $field_info = $this->form_field_model->get_where(array('field_id' => $id));
        $data['show_mail'] = $this->form_field_model->isMailFiledExit($field_info[0]->form_id);
        $data['show_ploicy_number'] = $this->form_field_model->isPolicyNumberFiledExit($field_info[0]->form_id);
        $data['show_behandler'] = $this->form_field_model->isBehandlerFiledExit($field_info[0]->form_id);
        $data['show_schadenummer'] = $this->form_field_model->isschadenummerFiledExit($field_info[0]->form_id);
        $data['show_behandler_emial'] = $this->form_field_model->isBehandlerEmailExit($field_info[0]->form_id);
        $data['field'] = $field_info[0];

        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
        $obj = new form_field_model();
        $res = $obj->get_where(array('field_id' => $id));

        $validation = $obj->validation_rules;

        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == FALSE) {

            $this->form_validation->set_value('field_name');
            $this->form_validation->set_value('name_on_form');
            $this->edit($id);
        } else {
            $obj->field_name = $this->input->post('field_name');
            $obj->name_on_form = $this->input->post('name_on_form');
            $obj->form_id = $res[0]->form_id;

            if ($this->input->post('field_type') != '') {
                $obj->field_type = $this->input->post('field_type');
            } else {
                $obj->field_type = 'N';
            }

            $obj->field_id = $id;
            $obj->update($id);
            $this->session->set_flashdata('success', "Field edited sucessfully");
            redirect('admin/form_fields/' . $res[0]->form_id, 'refresh');
        }
    }

    function delete_field($id) {
        $obj = new form_field_model();
        $res = $obj->get_where(array('field_id' => $id));

        $this->form_field_model->field_id = $id;
        $this->form_field_model->delete();
        redirect('admin/form_fields/' . $res[0]->form_id, 'refresh');
    }

}