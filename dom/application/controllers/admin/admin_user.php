<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate client and the user whos role is admin process the
 * login and other
 * methods.
 */
class admin_user extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('user_roles_model');
        $this->load->model('clients_model');
        $this->load->model('ins_departments_model');
        $this->load->model('client_setting_model');
        $this->load->helper('lgin');
        $this->default_template_name = get_admin_template_name();
    }

    /**
     * this will render the user whos role is admin
     */
    public function index() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $res = $this->client_setting_model->get_where(
                array('client_id' => $client_id
                ));
        $data['default_rows'] = $res[0]->default_rows;

        $data['page_name'] = 'admin/user/view_user';
        $data['title'] = 'Gebruikers';

        $this->load->view($this->default_template_name, $data);
    }

    /**
     * this funcrtion will render the Add Form
     */
    function add() {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $res = $this->ins_departments_model->getWhere(
                array('client_id' => $client_id
                ));
        $data['userroles'] = $this->user_roles_model->get_where(array('client_id' => $client_id));
        $data['departments'] = $res;
        $data['page_name'] = 'admin/user/add_user';
        $data['title'] = 'Toevoegen Gebruike';
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * this will handel all the data post by the add method form.
     */
    function addListener() {
        $obj = new users_model();

        $validation = $obj->validation_rules;
        unset($validation[0]);
        unset($validation[7]);
        $client = admin_get_client_id();
        $client_id = $client->client_id;

        $this->form_validation->set_rules($validation);

        if ($this->input->post('password') != '') {
            $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|trim|matches[password]');
        }

        if ($this->form_validation->run() == FALSE) {

            $this->form_validation->set_value('first_name');
            $this->form_validation->set_value('middle_name');
            $this->form_validation->set_value('last_name');
            $this->form_validation->set_value('mail_address');
            $this->form_validation->set_value('status');
            $this->form_validation->set_value('administrator');
            $this->form_validation->set_value('department');
            $this->form_validation->set_value('password');
            $this->form_validation->set_value('password_confirmation');
            $this->form_validation->set_value('role_menu');
            $this->form_validation->set_value('company');
            $this->add();
        } else {
            $obj->client_id = $client_id;
            $obj->first_name = $this->input->post('first_name');
            $obj->middle_name = $this->input->post('middle_name');
            $obj->last_name = $this->input->post('last_name');
            $obj->department = $this->input->post('department');
            $obj->mail_address = $this->input->post('mail_address');
            $obj->role = 'admin';
            if ($this->input->post('administrator')) {
                $obj->administrator = 1;
            } else {
                $obj->administrator = 0;
            }
            if ($this->input->post('status') != null) {
                $obj->status = 'A';
            } else {
                $obj->status = 'D';
            }
            if (trim($this->input->post('password')) != '') {
                $obj->password = md5($this->input->post('password'));
            }
            if ($this->input->post('role_menu')) {
                $obj->role_menu = $this->input->post('role_menu');
            } else {
                $obj->role_menu = NULL;
            }
            if ($this->input->post('company')) {
                $obj->company = $this->input->post('company');
            } else {
                $obj->company = NULL;
            }
            $obj->save();
            $this->session->set_flashdata('success', "Gebruiker toegevoegd.");
            redirect('admin/admin_user', 'refresh');
        }
    }

    function edit($id) {
        $data['page_name'] = 'admin/user/edit_user';
        $data['title'] = 'Edit Users';
        $data['user_id'] = $id;
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $res = $this->ins_departments_model->getWhere(
                array('client_id' => $client_id
                ));
        $data['departments'] = $res;
        $data['userroles'] = $this->user_roles_model->get_where(array('client_id' => $client_id));
        $data['client_details'] = $this->clients_model->get_all();
        $res = $this->users_model->selectSingleRecord('user_id', $id);
        if (empty($res)) {
            $this->session->set_flashdata('error', "Error!! May Be User Not Found");
            redirect(base_url() . "admin/admin_user");
        }
        $data['user'] = $res[0];

        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {

        $obj = new users_model();

        $validation = $obj->validation_rules;
        /**
         * unset the client id,role ,passward and the conforms passward when we
         * edit the user
         */
        unset($validation[0]);
        unset($validation[5]);
        unset($validation[6]);
        unset($validation[7]);
        $client = admin_get_client_id();
        $client_id = $client->client_id;

        $this->form_validation->set_rules($validation);

        if ($this->input->post('password') != '') {
            $this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required|matches[password]');
        }

        if ($this->form_validation->run() == FALSE) {

            $this->form_validation->set_value('first_name');
            $this->form_validation->set_value('middle_name');
            $this->form_validation->set_value('last_name');
            $this->form_validation->set_value('mail_address');
            $this->form_validation->set_value('department');
            $this->form_validation->set_value('status');
            $this->form_validation->set_value('administrator');
            $this->form_validation->set_value('password');
            $this->form_validation->set_value('role_menu');
            $this->form_validation->set_value('company');
            $this->edit($id);
        } else {
            $obj->user_id = $id;
            $obj->client_id = $client_id;
            $obj->first_name = $this->input->post('first_name');
            $obj->middle_name = $this->input->post('middle_name');
            $obj->last_name = $this->input->post('last_name');
            $obj->department = $this->input->post('department');
            $obj->mail_address = $this->input->post('mail_address');
            $obj->role = 'admin';
            if ($this->input->post('administrator')) {
                $obj->administrator = 1;
            } else {
                $obj->administrator = 0;
            }
            if ($this->input->post('status') != null) {
                $obj->status = 'A';
            } else {
                $obj->status = 'D';
            }

            if (trim($this->input->post('password')) != '') {
                $obj->password = $this->input->post('password');
            }
            if ($this->input->post('role_menu')) {
                $obj->role_menu = $this->input->post('role_menu');
            } else {
                $obj->role_menu = "NULL";
            }
            if ($this->input->post('company')) {
                $obj->company = $this->input->post('company');
            } else {
                $obj->company = NULL;
            }
           
            $obj->dataUpdate($id);
           
            $this->session->set_flashdata('success', "Bewerkt gebruiker.");
            redirect('admin/admin_user', 'refresh');
        }
    }

    function getJson() {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $array = array('client_id' => $client_id, 'role' => 'admin'
        );
        $records = $this->users_model->get_where($array);

        $array = $this->getArrayForJson($records);

        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function getArrayForJson($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $temp_arr = array();
            $temp_arr[] = '<a href="admin_user/edit/' . $value->user_id . '">' .
                    ucwords($value->first_name . ' ' . $value->last_name) .
                    '</a>';
            $temp_arr[] = date('d-m-Y', strtotime($value->register_date));
            if (isset($value->role_menu) && $value->role_menu != NULL) {
                $userrole = $this->user_roles_model->get_where(array('id' => $value->role_menu));
                $temp_arr[] = $userrole[0]->name;
            } else {
                $temp_arr[] = '-';
            }
            if ($value->status == 'A') {
                $temp_arr[] = '<span class="label label-success">Active</span>';
            } elseif ($value->status == 'D') {
                $temp_arr[] = '<span class="label label-important">Deactive</span>';
            }
            $temp_arr[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $value->user_id . "'><img src='" . base_url() .
                    "assets/images/icon_delete.png' alt='Delete' title='Delete'></a>";
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function delete($id) {
        $this->users_model->dataDelete($id);
        redirect('admin/admin_user', 'refresh');
    }

    function import_excel() {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $tmp_file_name = $_FILES['Filedata']['tmp_name'];
        $ext = explode(".", $_FILES['Filedata']['name']);
        if ($ext[1] == "xls" || $ext[1] == "xlsx") {
            $ok = move_uploaded_file($tmp_file_name, FCPATH . '/assets/upload/user_excel-file/' . $_FILES['Filedata']['name']);
            $filePath = FCPATH . '/assets/upload/user_excel-file/' . $_FILES['Filedata']['name'];
            require_once( APPPATH . 'libraries/PHPExcel/Classes/PHPExcel.php' );
            $objPHPExcel = PHPExcel_IOFactory::load($filePath);
            $data = $objPHPExcel->getActiveSheet()->toArray();
            unset($data[0]);
            $check = FALSE;
            if (!empty($data)) {
                foreach ($data as $values) {
                    $user = new users_model();
                    if ($values[0] != '' && $values[2] != '' && $values[3] != '' && $values[4] != '' && $values[5] != '') {
                        $user->client_id = $client_id;
                        $user->first_name = trim($values[0]);
                        $user->middle_name = trim($values[1]);
                        $user->last_name = trim($values[2]);
                        $user->mail_address = trim($values[3]);
                        $user->password = md5(trim($values[4]));
                        $user->role = 'admin';
                        $user->register_date = date('Y-m-d');
                        $user->status = trim($values[5]);
                        $user->administrator = 0;
                        if (trim($values[6]) != '') {
                            $userrole = $this->user_roles_model->get_where(array('client_id' => $client_id, 'name' => trim($values[6])));
                            if (!empty($userrole)) {
                                $user->role_menu = $userrole[0]->id;
                            }
                        }
                        if (trim($values[7]) != '') {
                            $user->company =trim($values[7]);
                        }
                        $check = $user->save();
                        //  $check = TRUE;
                    }
                }
                if ($check) {
                    $this->session->set_flashdata('success', "Excel imported successfully !!");
                } else {
                    $this->session->set_flashdata('error', "Error!! There is no record in Excel..");
                }
            } else {
                $this->session->set_flashdata('error', "Error!! There is no record in Excel..");
            }
            unlink('assets/upload/user_excel-file/' . $_FILES['Filedata']['name']);
            echo $ok ? "OK" : "FAIL";
        } else {
            echo "FAIL";
        }
        exit;
    }

}

?>
