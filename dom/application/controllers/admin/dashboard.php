<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class manages the Admin > text Menu.
 */
class dashboard extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();

        $this->default_template_name = get_admin_template_name();

        $this->load->model('contact_message_model');
        $this->load->model('admin_texts_model');
        $this->load->helper('time_worked');
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
        $data['page_name'] = 'admin/dashboard/dashboard';
        $data['title'] = 'Texts';

        $this->load->view($this->default_template_name, $data);
    }

    function viewDynamicText($name) {
        if ($name == 'Contact') {
            $data['title'] = ucwords($name);
            $data['page_name'] = 'admin/content/view_contact';
            $this->load->view($this->default_template_name, $data);
        } else {

            $obj = new admin_texts_model();
            $record = $obj->get_wher(
                    array('text_title' => str_replace('_', ' ', $name)
                    ));

            if (count($record) == 1) {
                $data['text_title'] = $record[0]->text_title;
                $data['text_content'] = $record[0]->text_content;

                $data['page_name'] = 'admin/content/view';
                $data['title'] = ucwords($name);

                $this->load->view($this->default_template_name, $data);
            } else {
                redirect(base_url(), 'refresh');
            }
        }
    }

    function add_content() {
        $obj = new contact_message_model();
        $validation = $obj->validation_rules;
        $this->form_validation->set_rules($validation);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('subject');
            $this->form_validation->set_value('message');
            $this->viewDynamicText('Contact');
        } else {
            $admin_array = $this->session->userdata('admin');
            $user_id = $admin_array['user_id'];
            $obj->subject = $this->input->post('subject');
            $obj->message = $this->input->post('message');
            $obj->date = get_current_time()->get_date_for_db();
            $obj->user_id = $user_id;
            $check = $obj->save();
            if ($check != '') {
                $this->session->set_flashdata('success', 'Contact Message Added Sucessufully');
            } else {
                $this->session->set_flashdata('error', 'Error in Adding The Contact Message');
            }
            redirect('admin/dashboard', 'refresh');
        }
    }

    function addPolicyholder($statr, $end) {
        for ($i = $statr; $i <= $end; $i++) {
            $sql = "INSERT INTO ins_policy_holder (policy_holder_id, client_id, policy_number, first_name, middle_name, last_name, email, address, zipcode, private_number, business_number, mobile_number, password, register_date, status, random_str, password_reset_random_string) VALUES (NULL, '1', '2000$i', 'Soyab_$i', NULL, 'Rana_$i', 'soyab$i@devrepublic.nl', NULL, NULL, NULL, NULL, NULL, '202cb962ac59075b964b07152d234b70', NULL, 'A', NULL, 'Mm4rBFKxaW');";
            $this->db->query($sql);
        }
    }

    function addClaim($statr, $end) {
        for ($i = $statr; $i <= $end; $i++) {
            $sql = "INSERT INTO ins_claims (claim_id, import_date, kind, userid, handler, schadenummer, form_id, mail_send_date, status, policy_holder_id, client_id, conform_date, is_delete) VALUES (NULL, '2014-01-21', NULL, '1', NULL, NULL, '13', '2014-01-21', 'open', '$i', '1', '2014-01-17', '0');";
            $this->db->query($sql);
        }
    }

}

?>
