<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate client and the user whos role is admin process the
 * login and other
 * methods.
 */
class admin_department extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('ins_departments_model');
        $this->load->model('client_setting_model');
        $this->load->helper('lgin');
        $this->default_template_name = get_admin_template_name();
    }

    /**
     * this will render the user whos role is admin
     */
    public function index() {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $res = $this->client_setting_model->get_where(
                array('client_id' => $client_id
                ));
        $data['default_rows'] = $res[0]->default_rows;

        $data['page_name'] = 'admin/department/view_department';
        $data['title'] = 'Afdelingen';
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * this funcrtion will render the Add Form
     */
    function add() {
        $data['page_name'] = 'admin/department/add_department';
        $data['title'] = 'Toevoegen Afdeling';
        $this->load->view($this->default_template_name, $data);
    }

    /**
     * this will handel all the data post by the add method form.
     */
    function addListener() {
        $obj = new ins_departments_model();
        $validation = $obj->validationRules();
        $client = admin_get_client_id();
        $client_id = $client->client_id;

        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('name');
            $this->add();
        } else {
            $obj->client_id = $client_id;
            $obj->name = $this->input->post('name');
            $obj->insertData();
            $this->session->set_flashdata('success', "Afdeling toegevoegd.");
            redirect('admin/departments', 'refresh');
        }
    }

    function edit($id) {
        $data['page_name'] = 'admin/department/edit_department';
        $data['title'] = 'Edit department';
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $array = array('account_id' => $id, 'client_id' => $client_id);
        $res = $this->ins_departments_model->getWhere($array);
        if (empty($res)) {
            $this->session->set_flashdata('error', "Error!! May Be User Not Found");
            redirect(base_url() . "admin/departments");
        }
        $data['department'] = $res[0];
        $this->load->view($this->default_template_name, $data);
    }

    function editListener($id) {
        $obj = new ins_departments_model();
        $validation = $obj->validationRules();
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $this->form_validation->set_rules($validation);
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('name');
            $this->edit($id);
        } else {
            $obj->account_id = $id;
            $obj->client_id = $client_id;
            $obj->name = $this->input->post('name');
            $obj->updateData();
            $this->session->set_flashdata('success', "Bewerkt afdeling.");
            redirect('admin/departments', 'refresh');
        }
    }

    function getJson() {

        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $array = array('client_id' => $client_id);
        $records = $this->ins_departments_model->getWhere($array);
        $array = $this->getArrayForJson($records);
        $data['aaData'] = $array;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    function getArrayForJson($objects) {
        $arra = array();
        foreach ($objects as $value) {
            $temp_arr = array();
            $temp_arr[] = '<a href="edit_departments/' . $value->account_id . '">' . ucwords($value->name) . '</a>';
            $temp_arr[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $value->account_id . "'><img src='" . base_url() .
                    "assets/images/icon_delete.png' alt='Delete' title='Delete'></a>";
            $arra[] = $temp_arr;
        }
        return $arra;
    }

    function delete($id) {
        $this->ins_departments_model->account_id = $id;
        $this->ins_departments_model->deleteData();
        $this->session->set_flashdata('success', "Afdeling verwijderd.");
        redirect('admin/departments', 'refresh');
    }

}

?>
