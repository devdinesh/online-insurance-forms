<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class is for admin settings(like edit the authenticated clients log and
 * the email address)
 */
class admin_settings extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->helper('lgin');
        $this->load->library('image_lib');
        $this->load->model('client_setting_model');
        $this->load->model('users_model');
        $this->load->model('my_file_upload');
        $this->default_template_name = get_admin_template_name();
    }

    public function index() {
        //$this->session->unset_userdata('some_name');
        // $this->session->set_userdata('open_tab', "tab_1_2");
        $this->edit();
    }

    /**
     * edit the the authenticated client using this edit method
     */
    function edit() {
        $data['page_name'] = 'admin/settings/edit_admin';
        $data['title'] = 'Edit admin';
        $client_setting_model = new client_setting_model();

        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;

        $res = $client_setting_model->get_where(
                array('client_id' => $client_id
                ));

        $data['user_records'] = $this->users_model->get_where(array('client_id' => $client_id, 'role' => 'admin'));

        if (isset($res[0])) {
            $data['admin'] = $res[0];
        }
        $this->load->view($this->default_template_name, $data);
    }

    /*
     * action listener for edit page
     */

    function editListener($id) {
        $this->session->unset_userdata('open_tab');
        $this->session->set_userdata('open_tab', "general");
        // get the login client id
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        // get the object and validations rules
        $admin = new client_setting_model();
        $rules = $admin->validation_rules;
        // file errors if any
        $error = array();
        // try uploadng the file
        // if file is selected then only the upload will be done
        // it would be success if no file was selested
        $logo_upload_status = my_file_upload('logo', $admin->logo_upload_config(), $this);

        // if file was uploaded
        if ($logo_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $success = false;

            $this->session->set_flashdata('file_errors', $logo_upload_status['data']);
            redirect(base_url() . "admin/admin_settings");
        } else {
            $success = true;
        }


        // //mail_logo start
        $mail_logo_upload_status = my_file_upload('mail_logo', $admin->mail_logo_upload_config(), $this);

        // if file was uploaded
        if ($mail_logo_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $success2 = false;

            $this->session->set_flashdata('mail_logo_error', $mail_logo_upload_status['data']);
            redirect(base_url() . "admin/admin_settings");
        } else {
            $success2 = true;
        }
        //mail_logo close
        //pdf logo start
        $pdf_logo_upload_status = my_file_upload('pdf_logo', $admin->pdf_logo_upload_config(), $this);

        // if file was uploaded
        if ($pdf_logo_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $success3 = false;

            $this->session->set_flashdata('pdf_logo_error', $pdf_logo_upload_status['data']);
            redirect(base_url() . "admin/admin_settings");
        } else {
            $success3 = true;
        }
        //pdf logo close
        // if upload was ok
        if ($success == true && $success2 == true && $success3 == true) {
            $validation = $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('top_margin');
                $this->form_validation->set_value('bottom_margin');
                $this->form_validation->set_value('left_margin');
                $this->form_validation->set_value('right_margin');
                $this->form_validation->set_value('letter_type');
                $this->form_validation->set_value('fontsize_h1');
                $this->form_validation->set_value('fontsize_h2');
                $this->form_validation->set_value('fontsize_h3');
                $this->form_validation->set_value('fontsize_normal');
                $this->edit();
            } else {
                // get the data to be updated
                $admin = new client_setting_model();
                $admin = $admin->get_where(
                        array('setting_id' => $id
                        ));

                if (isset($admin[0])) {
                    // get the first result as where return multiple things
                    $admin = $admin[0];
                } else {
                    $admin = new client_setting_model();
                }

                // remove the previous file if exists
                if (isset($logo_upload_status['data'])) {
                    if (isset($admin->admin_logo) && $admin->admin_logo != '') {
                        $path_to_be_removed = substr(
                                $admin->logo_upload_path . "/" .
                                $admin->admin_logo, 2);
                        @unlink($path_to_be_removed);
                    }
                }

                // get the path to new fiel uploded
                if (!isset($logo_upload_status['data'])) {
                    // if no file was chosen then the path would not be changed
                    $logo_upload_status['data'] = $admin->admin_logo;
                }

                // remove the previous file if exists
                if (isset($pdf_template_upload_status['data'])) {
                    if (isset($admin->pdf_template) && $admin->pdf_template != '') {
                        $path_to_be_removed = substr(
                                $admin->pdf_template_upload_path . "/" .
                                $admin->pdf_template, 2);
                        @unlink($path_to_be_removed);
                    }
                }

                // get the path to new fiel uploded
                if (!isset($pdf_template_upload_status['data'])) {
                    // if no file was chosen then the path would not be changed
                    $pdf_template_upload_status['data'] = $admin->pdf_template;
                }
                ///mail_logo_
                if (isset($mail_logo_upload_status['data'])) {
                    if (isset($admin->mail_logo) && $admin->mail_logo != '') {
                        $path_to_be_removed = substr(
                                $admin->mail_logo_upload_path . "/" .
                                $admin->pdf_template, 2);
                        @unlink($path_to_be_removed);
                    }
                }

                // get the path to new fiel uploded
                if (!isset($mail_logo_upload_status['data'])) {
                    // if no file was chosen then the path would not be changed
                    $mail_logo_upload_status['data'] = $admin->mail_logo;
                }
                //end mail logo
                //pdf_logo_
                if (isset($pdf_logo_upload_status['data'])) {
                    if (isset($admin->pdf_logo) && $admin->pdf_logo != '') {
                        $path_to_be_removed = substr(
                                $admin->pdf_logo_upload_path . "/" .
                                $admin->pdf_logo, 2);
                        @unlink($path_to_be_removed);
                    }
                }

                // get the path to new fiel uploded
                if (!isset($pdf_logo_upload_status['data'])) {
                    // if no file was chosen then the path would not be changed
                    $pdf_logo_upload_status['data'] = $admin->pdf_logo;
                }
                //end pdf logo
                $new_file_path = $logo_upload_status['data'];

                $mail_logo = $mail_logo_upload_status['data'];
                $pdf_logo = $pdf_logo_upload_status['data'];
                $admin->admin_logo = $new_file_path; // set the value to newly
                // uploaded file's path
                $admin->mail_logo = $mail_logo;
                $admin->pdf_logo = $pdf_logo;
                $admin->client_id = $client_id;
                $admin->default_rows = $this->input->post('default_rows');
                if ($this->input->post('letter_type')) {
                    $admin->letter_type = $this->input->post('letter_type');
                } else {
                    $admin->letter_type = NULL;
                }
                if ($this->input->post('fontsize_h1')) {
                    $admin->fontsize_h1 = $this->input->post('fontsize_h1');
                } else {
                    $admin->fontsize_h1 = NULL;
                }
                if ($this->input->post('fontsize_h2')) {
                    $admin->fontsize_h2 = $this->input->post('fontsize_h2');
                } else {
                    $admin->fontsize_h2 = NULL;
                }
                if ($this->input->post('fontsize_h3')) {
                    $admin->fontsize_h3 = $this->input->post('fontsize_h3');
                } else {
                    $admin->fontsize_h3 = NULL;
                }
                if ($this->input->post('fontsize_normal')) {
                    $admin->fontsize_normal = $this->input->post('fontsize_normal');
                } else {
                    $admin->fontsize_normal = NULL;
                }
                $admin->delete_term = $this->input->post('delete_term');
                if ($id == -1) {
                    $val = $admin->save();
                } else {
                    // update the value to database
                    $val = $admin->update();
                }

                $this->session->set_flashdata('success', "Wijzigingen zijn opgeslagen.");
                redirect(base_url() . "admin/admin_settings");
            }
        } else {
            $this->form_validation->set_value('email');
            $this->edit();
        }
    }

    function editTextaListener($id) {
        $this->session->unset_userdata('open_tab');
        $this->session->set_userdata('open_tab', "texts");
        // get the login client id
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        // get the object and validations rules
        $admin = new client_setting_model();
        $rules = $admin->validation_rules;


        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('welcome_text');
            $this->form_validation->set_value('about_us');
            $this->form_validation->set_value('contact');
            $this->form_validation->set_value('registration_text');
            $this->edit();
        } else {
            // get the data to be updated
            $admin = new client_setting_model();
            $admin = $admin->get_where(
                    array('setting_id' => $id
                    ));

            if (isset($admin[0])) {
                // get the first result as where return multiple things
                $admin = $admin[0];
            } else {
                $admin = new client_setting_model();
            }
            $admin->client_id = $client_id;
            $admin->welcome_text = $this->input->post('welcome_text');
            $admin->about_us = $this->input->post('about_us');
            $admin->contact = $this->input->post('contact');
            $admin->registration_text = $this->input->post('registration_text');
            // update the value to database
            $val = $admin->update();

            $this->session->set_flashdata('success', "Wijzigingen zijn opgeslagen.");
            redirect(base_url() . "admin/admin_settings");
        }
    }

    function editImportaListener($id) {

        $this->session->unset_userdata('open_tab');
        $this->session->set_userdata('open_tab', "import");
        $this->form_validation->set_rules('main_user', 'Main User', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->edit();
        } else {

            // get the login client id
            $get_client_id = admin_get_client_id();
            $client_id = $get_client_id->client_id;
            // get the object and validations rules
            $admin = new client_setting_model();


            // get the data to be updated
            $admin = new client_setting_model();
            $admin = $admin->get_where(
                    array('setting_id' => $id
                    ));




            if (isset($admin[0])) {
                // get the first result as where return multiple things
                $admin = $admin[0];
            } else {
                $admin = new client_setting_model();
            }
            $admin->client_id = $client_id;
            $automatic_import = $this->input->post('automatic_import');

            if ($automatic_import == 1) {
                $admin->automatic_import = $automatic_import;
            } else {
                $admin->automatic_import = 0;
            }
            if ($this->input->post('main_user')) {
                $admin->main_user = $this->input->post('main_user');
            }

            // $admin->interval = $this->input->post('interval');
            // update the value to database
            $val = $admin->update();

            $this->session->set_flashdata('success', "Wijzigingen zijn opgeslagen.");
            redirect(base_url() . "admin/admin_settings");
        }
    }

    function editTemplateListener($id) {
        $this->session->unset_userdata('open_tab');
        $this->session->set_userdata('open_tab', "template");
        // get the login client id
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        // get the object and validations rules
        $admin = new client_setting_model();
        //pdf template start
        $pdf_template_upload_status = my_file_upload('pdf_template', $admin->pdf_template_upload_config(), $this);

        // if file was uploaded
        if ($pdf_template_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $success1 = false;

            $this->session->set_flashdata('pdf_template_error', $pdf_template_upload_status['data']);
            redirect(base_url() . "admin/admin_settings");
        } else {
            $success1 = true;
        }
        //pdf template close
        //pdf template start
        $pdf_template_follow_upload_status = my_file_upload('pdf_template_follow', $admin->pdf_template_follow_upload_config(), $this);

        // if file was uploaded
        if ($pdf_template_follow_upload_status['status'] == 0) {
            // set the error message for fors in the flash
            $success2 = false;

            $this->session->set_flashdata('pdf_template_follow_error', $pdf_template_follow_upload_status['data']);
            redirect(base_url() . "admin/admin_settings");
        } else {
            $success2 = true;
        }
        //pdf template close

        $rules = $admin->validation_rules;
        $this->form_validation->set_rules($rules);
        if ($success1 == TRUE && $success2 == TRUE) {
            if ($this->form_validation->run() == FALSE) {
                $this->form_validation->set_value('top_margin');
                $this->form_validation->set_value('bottom_margin');
                $this->form_validation->set_value('left_margin');
                $this->form_validation->set_value('right_margin');
                $this->form_validation->set_value('top_margin_follow');
                $this->form_validation->set_value('bottom_margin_follow');
                $this->form_validation->set_value('left_margin_follow');
                $this->form_validation->set_value('right_margin_follow');
                $this->edit();
            } else {
                // get the data to be updated
                $admin = new client_setting_model();
                $admin = $admin->get_where(
                        array('setting_id' => $id
                        ));

                if (isset($admin[0])) {
                    // get the first result as where return multiple things
                    $admin = $admin[0];
                } else {
                    $admin = new client_setting_model();
                }
                // remove the previous file if exists
                if (isset($pdf_template_upload_status['data'])) {
                    if (isset($admin->pdf_template) && $admin->pdf_template != '') {
                        $path_to_be_removed = substr(
                                $admin->pdf_template_upload_path . "/" .
                                $admin->pdf_template, 2);
                        @unlink($path_to_be_removed);
                    }
                }
                // remove the previous file if exists
                if (isset($pdf_template_follow_upload_status['data'])) {
                    if (isset($admin->pdf_template_folow) && $admin->pdf_template_folow != '') {
                        $path_to_be_removed1 = substr(
                                $admin->pdf_template_follow_upload_path . "/" .
                                $admin->pdf_template_folow, 2);
                        @unlink($path_to_be_removed1);
                    }
                }
                $admin->client_id = $client_id;
                $pdf_template = $pdf_template_upload_status['data'];
                $pdf_template_follow = $pdf_template_follow_upload_status['data'];
                $admin->pdf_template = $pdf_template; // set the value to newly
                $admin->default_rows = $this->input->post('default_rows');
                $admin->top_margin = $this->input->post('top_margin');
                $admin->bottom_margin = $this->input->post('bottom_margin');
                $admin->left_margin = $this->input->post('left_margin');
                $admin->right_margin = $this->input->post('right_margin');

                $admin->pdf_template_follow = $pdf_template_follow; // set the value to newly
                $admin->top_margin_follow = $this->input->post('top_margin_follow');
                $admin->bottom_margin_follow = $this->input->post('bottom_margin_follow');
                $admin->left_margin_follow = $this->input->post('left_margin_follow');
                $admin->right_margin_follow = $this->input->post('right_margin_follow');
                // update the value to database
                $val = $admin->update();

                $this->session->set_flashdata('success', "Wijzigingen zijn opgeslagen.");
                redirect(base_url() . "admin/admin_settings");
            }
        } else {
            $this->edit();
        }
    }

    function delete_logo($setting_id) {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $admin = new client_setting_model();
        $get_data = $admin->get_where(array('setting_id' => $setting_id, 'client_id' => $client_id));
        if (!empty($get_data)) {
            if (isset($get_data[0]->admin_logo) && $get_data[0]->admin_logo != '') {
                $path_to_be_removed = substr(
                        $admin->logo_upload_path . "/" .
                        $get_data[0]->admin_logo, 2);
                @unlink($path_to_be_removed);
                $admin->setting_id = $setting_id;
                $check = $admin->delete_logo();
                $this->session->set_flashdata('success', "Logo Deleted sucessfully.");
                redirect(base_url() . "admin/admin_settings");
            }
        } else {
            $this->session->set_flashdata('error', "Error!,Please Tru again later.");
            redirect(base_url() . "admin/admin_settings");
        }
    }

    function delete_mail_logo($setting_id) {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $admin = new client_setting_model();
        $get_data = $admin->get_where(array('setting_id' => $setting_id, 'client_id' => $client_id));
        if (!empty($get_data)) {
            if (isset($get_data[0]->mail_logo) && $get_data[0]->mail_logo != '') {
                $path_to_be_removed = substr(
                        $admin->mail_logo_upload_path . "/" .
                        $get_data[0]->mail_logo, 2);
                @unlink($path_to_be_removed);
                $admin->setting_id = $setting_id;
                $check = $admin->delete_mail_logo();
                $this->session->set_flashdata('success', "Logo Deleted sucessfully.");
                redirect(base_url() . "admin/admin_settings");
            }
        } else {
            $this->session->set_flashdata('error', "Error!,Please Tru again later.");
            redirect(base_url() . "admin/admin_settings");
        }
    }

    function delete_pdf_logo($setting_id) {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $admin = new client_setting_model();
        $get_data = $admin->get_where(array('setting_id' => $setting_id, 'client_id' => $client_id));
        if (!empty($get_data)) {
            if (isset($get_data[0]->pdf_logo) && $get_data[0]->pdf_logo != '') {
                $path_to_be_removed = substr(
                        $admin->pdf_logo_upload_path . "/" .
                        $get_data[0]->pdf_logo, 2);
                @unlink($path_to_be_removed);
                $admin->setting_id = $setting_id;
                $check = $admin->delete_pdf_logo();
                $this->session->set_flashdata('success', "Logo Deleted sucessfully.");
                redirect(base_url() . "admin/admin_settings");
            }
        } else {
            $this->session->set_flashdata('error', "Error!,Please Tru again later.");
            redirect(base_url() . "admin/admin_settings");
        }
    }

    function delete_pdf_template($setting_id) {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $admin = new client_setting_model();
        $get_data = $admin->get_where(array('setting_id' => $setting_id, 'client_id' => $client_id));
        if (!empty($get_data)) {
            if (isset($get_data[0]->pdf_template) && $get_data[0]->pdf_template != '') {
                $path_to_be_removed = substr(
                        $admin->pdf_template_upload_path . "/" .
                        $get_data[0]->pdf_template, 2);
                @unlink($path_to_be_removed);
                $admin->setting_id = $setting_id;
                $check = $admin->delete_pdf_template();
                $this->session->set_flashdata('success', "Pdf Template Deleted sucessfully.");
                redirect(base_url() . "admin/admin_settings");
            }
        } else {
            $this->session->set_flashdata('error', "Error!,Please Tru again later.");
            redirect(base_url() . "admin/admin_settings");
        }
    }

    function delete_pdf_template_follow($setting_id) {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;
        $admin = new client_setting_model();
        $get_data = $admin->get_where(array('setting_id' => $setting_id, 'client_id' => $client_id));
        if (!empty($get_data)) {
            if (isset($get_data[0]->pdf_template_follow) && $get_data[0]->pdf_template_follow != '') {
                $path_to_be_removed = substr(
                        $admin->pdf_template_follow_upload_path . "/" .
                        $get_data[0]->pdf_template_follow, 2);
                @unlink($path_to_be_removed);
                $admin->setting_id = $setting_id;
                $check = $admin->delete_pdf_template_follow();
                $this->session->set_flashdata('success', "PDF sjabloon vervolgpagina's Deleted sucessfully.");
                redirect(base_url() . "admin/admin_settings");
            }
        } else {
            $this->session->set_flashdata('error', "Error!,Please Tru again later.");
            redirect(base_url() . "admin/admin_settings");
        }
    }

    function create_demo_for_pdf_template($setting_id) {
        $html = $this->get_pdf_content_ofform($setting_id);
        $get_client_id = admin_get_client_id();
        $this->load->helper('dompdf');
        $this->load->helper('dompdf/file');
        $this->load->helper('dompdf/dompdf_lib_include');
        dompdf_lib_include();
        $dompdf = new DOMPDF;
        // define("DOMPDF_DPI", 62.230);
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream("Test_demo.pdf", array("Attachment" => 0
        ));
    }

    function get_pdf_content_ofform($setting_id) {
        $get_client_id = admin_get_client_id();
        $client_id = $get_client_id->client_id;

        $records = $this->clients_model->get_where(array('client_id' => $client_id));
        /**
         * get the information about the categories related to the
         * forms
         */
        $return = NULL;
        $return = '<html><head>';
        $style = NULL;
        $style.='<style type="text/css">';
        $admin_setting_info = $this->client_setting_model->get_where(array('client_id' => $client_id, 'setting_id' => $setting_id));
        if (!empty($admin_setting_info[0]->pdf_template)) {
            /* $pdf_template_info = 'assets/upload/pdf_template/' . $admin_setting_info[0]->pdf_template;

              $config_image['image_library'] = 'gd2';
              $config['source_image'] = $pdf_template_info;
              $config['maintain_ratio'] = TRUE;
              $config['create_thumb'] = TRUE;
              $config['width'] = 792;
              $config['height'] = 792;
              $this->image_lib->initialize($config);
              $this->load->library('image_lib', $config);
              // $resize= $this->image_lib->resize();

              if (!$this->image_lib->resize()) {
              $return .= '<div>' .
              $this->image_lib->display_errors() . '</div>';
              } else {

              $pdf_template_exist = substr($admin_setting_info[0]->pdf_template, 0, strlen($admin_setting_info[0]->pdf_template) - 4) . '_thumb' .
              substr($admin_setting_info[0]->pdf_template, -4);

             */
            $pdf_template_fcpath = FCPATH . 'assets/upload/pdf_template/' . $admin_setting_info[0]->pdf_template;
            if (file_exists($pdf_template_fcpath) == true) {
                $pdf_template = base_url() . 'assets/upload/pdf_template/' . $admin_setting_info[0]->pdf_template;
            }
        }

        if (isset($pdf_template)) {
            $style.='body{background-image: url(' . $pdf_template . ');}';
        }

        $style.='#main_table_contain{';
        if (!empty($admin_setting_info[0]->top_margin)) {
            $style.='margin-top:' . $admin_setting_info[0]->top_margin . 'px;';
        }
        if (!empty($admin_setting_info[0]->bottom_margin)) {
            $style.='margin-bottom:' . $admin_setting_info[0]->bottom_margin . 'px;';
        }
        if (!empty($admin_setting_info[0]->left_margin)) {
            $style.='margin-left:' . $admin_setting_info[0]->left_margin . 'px;';
        }
        if (!empty($admin_setting_info[0]->right_margin)) {
            $style.='margin-right:' . $admin_setting_info[0]->right_margin . 'px;';
        }

        $style.='}</style>';
        $return.=$style;
        $return.='</head><body>';
        $return.='<div id="main_table_contain">';
        $return.= '<table style="font-family: sans-serif; font-size: 14.2667px; width: 100%;" border="0" cellspacing="6" cellspadding="3">';
        if (!empty($records[0]->logo)) {
            $logo = 'assets/upload/client_logo/' . $records[0]->logo;

            $config_image['image_library'] = 'gd2';
            $config['source_image'] = $logo;
            // $config['new_image'] ='assets/upload/client_logo/thumb_'.
            // $records[0]->logo;
            $config['maintain_ratio'] = TRUE;
            $config['create_thumb'] = TRUE;
            $config['width'] = 150;
            $config['height'] = 150;
            $this->image_lib->initialize($config);
            $this->load->library('image_lib', $config);
            // $resize= $this->image_lib->resize();

            if (!$this->image_lib->resize()) {
                $return .= '<tr><th width="1%">&nbsp;</th><th colspan="2">' .
                        $this->image_lib->display_errors() . '</th></tr>';
            } else {

                $client_logo = substr($records[0]->logo, 0, strlen($records[0]->logo) - 4) . '_thumb' .
                        substr($records[0]->logo, -4);
                $client_logo_url = FCPATH . 'assets/upload/client_logo/' .
                        $client_logo;
                if (file_exists($client_logo_url) == true) {
                    $client_logo_url_true = base_url() .
                            'assets/upload/client_logo/' . $client_logo;
                } else {
                    $return .= '<tr><th width="1%">&nbsp;</th><th colspan="2">IMAGE PATH NOT FOUND</th></tr>';
                }
            }

            if (isset($client_logo_url_true)) {
                $return .= '<tr><th width="1%" style="text-align:left"><img  src="' .
                        $client_logo_url_true . '"></th><th colspan="2">&nbsp;</th></tr>';
            }
        } else {
            $return .= '<tr><th width="1%">&nbsp;</th><th colspan="2">No LOGO FOUND</th></tr>';
        }
        $intro_text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

        $return .= '<tr><th colspan="3"><h2>' . ucfirst('bike insurence form') . '</h2></th></tr>
             
            <tr>
            	<td colspan="3" style="text-align: justify;"><hr>' . $intro_text . '</td>
            </tr>
            <tr>
            	<td colspan="3"></td>
            </tr>';
        $content_text = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.";
        $return .= '<tr>
            	<td colspan="3" style="text-align: justify;"><hr>' . $content_text . '</td>
            </tr>
            <tr>
            	<td colspan="3"></td>
            </tr>';
        $closer_text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
        $return .= '<tr><td colspan="3"></td></tr>
<tr>
	<td colspan="3" style="text-align: justify;"><hr>' .
                @$closer_text . '</td>
</tr>
        
</table>';
        $return.='</div></body></html>';
        return $return;
    }

}

/**
 * Location: application/controllers/admin/admin_settings.php
 */
?>

