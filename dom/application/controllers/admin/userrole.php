<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class deals with nwez
 */
class userrole extends CI_Controller {

    /**
     *
     * @var String template name whis is rendered.
     */
    var $default_template_name;

    /**
     * Default constructor loads the models and set ups template name.
     */
    function __construct() {
        parent::__construct();
        $this->load->model('user_roles_model');
        $this->default_template_name = get_admin_template_name();
    }

    public function index() {

        $data['page_name'] = 'admin/userrole/index';
        $data['title'] = 'Rollen';
        $this->load->view($this->default_template_name, $data);
    }

    /*
     * renders json for the list newz page
     */

    public function get_json() {
        $admin = $this->session->userdata('admin');
        $client_id = $admin['client_id'];

        $user_roles_model = new user_roles_model();
        $userroles = $user_roles_model->get_where(array('client_id' => $client_id));
        $main_arr = array();
        foreach ($userroles as $userrole) {
            $temp = array();
            $temp[] = anchor(base_url() . 'admin/userrole/edit/' . $userrole->id, $userrole->name);
            $temp[] = "<a href='javascript:;' onclick='deleteRow(this)' class='deletepage' id='" .
                    $userrole->id . "'><img src='" . assets_url_img .
                    "delete.png' alt='Delete' title='Delete'></a>";
            $main_arr[] = $temp;
        }
        $data['aaData'] = $main_arr;
        echo json_encode($data);
    }

    public function add() {
        $data['page_name'] = 'admin/userrole/add';
        $data['title'] = 'Toevoegen Rollen ';
        $this->load->view($this->default_template_name, $data);
    }

    public function add_listener() {
        $userrole = new user_roles_model();
        $rules = $userrole->validation_rules;
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            $this->add();
        } else {
            $admin = $this->session->userdata('admin');
            $client_id = $admin['client_id'];
            $userrole->client_id = $client_id;
            $userrole->name = $this->input->post('name');
            $userrole->show_dashbord = $this->input->post('show_dashbord') != NULL ? 1 : 0;
            $userrole->show_relations = $this->input->post('show_relations') != NULL ? 1 : 0;
            $userrole->show_forms = $this->input->post('show_forms') != NULL ? 1 : 0;
            $userrole->show_own_forms = $this->input->post('show_own_forms') != NULL ? 1 : 0;
            $userrole->show_news = $this->input->post('show_news') != NULL ? 1 : 0;
            $userrole->show_statistics = $this->input->post('show_statistics') != NULL ? 1 : 0;
            $userrole->show_newsletters = $this->input->post('show_newsletters') != NULL ? 1 : 0;
            $userrole->show_other = $this->input->post('show_other') != NULL ? 1 : 0;
            $userrole->show_user_forms = $this->input->post('show_user_forms') != NULL ? 1 : 0;
            $userrole->save();
            $this->session->set_flashdata('success', "Rollen is toegevoegd");
            redirect(base_url('admin/userrole'));
        }
    }

    public function edit($id) {

        $admin = $this->session->userdata('admin');
        $client_id = $admin['client_id'];

        $userrole = new user_roles_model();
        $roles = $userrole->get_where(array('id' => $id));

        if (count($roles) == 0) {
            $this->session->set_flashdata('error', "No such User role exists");
            redirect(base_url('admin/userrole'));
        }

        $role = $roles[0];

        if ($role->client_id != $client_id) {
            $this->session->set_flashdata('error', "Can not access that");
            redirect(base_url('admin/userrole'));
        }
        $data['page_name'] = 'admin/userrole/edit';
        $data['title'] = 'Bewerk Rollen';
        $data['userrole'] = $role;

        $this->load->view($this->default_template_name, $data);
    }

    public function edit_listener($id) {
        $admin = $this->session->userdata('admin');
        $client_id = $admin['client_id'];

        $userrole = new user_roles_model();
        $roles = $userrole->get_where(array('id' => $id));

        if (count($roles) == 0) {
            $this->session->set_flashdata('error', "No such User role exists");
            redirect(base_url('admin/userrole'));
        }

        $role = $roles[0];
        if ($role->client_id != $client_id) {
            $this->session->set_flashdata('error', "Can not access that");
            redirect(base_url('admin/userrole'));
        }

        $rules = $userrole->validation_rules;
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            $this->edit($id);
        } else {
            $admin = $this->session->userdata('admin');
            $client_id = $admin['client_id'];
            $userrole->id = $id;
            $userrole->client_id = $client_id;
            $userrole->name = $this->input->post('name');
            $userrole->show_dashbord = $this->input->post('show_dashbord') != NULL ? 1 : 0;
            $userrole->show_relations = $this->input->post('show_relations') != NULL ? 1 : 0;
            $userrole->show_forms = $this->input->post('show_forms') != NULL ? 1 : 0;
            $userrole->show_own_forms = $this->input->post('show_own_forms') != NULL ? 1 : 0;
            $userrole->show_news = $this->input->post('show_news') != NULL ? 1 : 0;
            $userrole->show_statistics = $this->input->post('show_statistics') != NULL ? 1 : 0;
            $userrole->show_newsletters = $this->input->post('show_newsletters') != NULL ? 1 : 0;
            $userrole->show_other = $this->input->post('show_other') != NULL ? 1 : 0;
            $userrole->show_user_forms = $this->input->post('show_user_forms') != NULL ? 1 : 0;
            $userrole->update();
            $this->session->set_flashdata('success', "Rollen Bijgewerkt");
            redirect(base_url('admin/userrole'));
        }
    }

    function delete($id) {
        $this->user_roles_model->id = $id;
        $this->user_roles_model->delete();
        $this->session->set_flashdata('success', "User Role deleted sucessfully");
        redirect('admin/userrole', 'refresh');
    }

}