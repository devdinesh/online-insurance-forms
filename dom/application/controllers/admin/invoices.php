<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class invoices extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();

        $this->load->model('clients_model');
        $this->load->model('invoice_model');
        $this->load->model('forms_model');
        $this->load->model('claims_model');
        $this->default_template_name = get_admin_template_name();
    }

    public function index() {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $data['page_name'] = 'admin/invoices/index';
        $data['title'] = 'Facturen';
        $data['client_id'] = $client_id;
        $this->load->view($this->default_template_name, $data);
    }

    function client_invoice_json($client_id) {
        $this->load->model('invoice_model');
        $invoice_model = new invoice_model();
        $invoices = $invoice_model->get_where(
                array('client_id' => $client_id
                ));
        $arra = array();

        foreach ($invoices as $invoice) {
            $temp_arr = array();

            $temp_arr[] = $invoice->invoice_number;
            $temp_arr[] = $invoice->invoice_subject;
            $temp_arr[] = '&euro; ' . $invoice->invoice_amount;
            $temp_arr[] = $invoice->invoice_date;
            $temp_arr[] = '<a target="_blank" href="' . base_url() . 'admin/invoices/create_pdf/' . $invoice->invoice_number . '"> <img style="height: 35px;"  src="' . assets_url_img . 'PDF-Document-icon.png' . '" alt="alt"/> </a>';
            $arra[] = $temp_arr;
        }

        $data['aaData'] = $arra;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    // create pdf files for invoices
    public function create_pdf($invo_number_with_year) {
        define('EURO', chr(128));
        $no_with = substr($invo_number_with_year, 4);
        $invo_number = (int) $no_with;


        $invoice_model = new invoice_model();
        $invoices = $invoice_model->get_where(array('invoice_number' => $invo_number));


        $obj_client = new clients_model();
        $client_info = $obj_client->get_where(array('client_id' => $invoices[0]->client_id));
        // load the helper for get the libaray files


        $this->load->helper('pdf/generate_pdf');

        // it include the lib needed to generate the PDF files

        include_lib_pdf();

        $pdf = new FPDF();

        $pdf->AddPage();


        $this->load->helper('admin_setting_info');
        $logo_url = get_super_admin_logo();

        $pdf->Image($logo_url, 65, 10, 100, 25);
        $pdf->Ln(35);
        /*
         * this medthod makes the cell for table
         * @param1 : width of cell
         * @param2 : height of cell
         * @param3 : content of cell
         * @param4 : border of cell (1 = YES | 0 = NO)
         * @param5 : move to next line (1 = YES | 0 = NO)
         * @param6 : aligment of text (L= Left | C = CENTER | R = RIGHT)
         */

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(25, 6, 'Factuuradres', 0, 0, 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(5, 6, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 6, ucwords($client_info[0]->title_application), 0, 1, 'L');
        $pdf->Cell(30, 6, NULL, 0, 0, 'L');
        $pdf->Cell(Null, 6, ucwords($client_info[0]->address), 0, 1, 'L');
        $pdf->Cell(30, 6, NULL, 0, 0, 'L');
        $pdf->Cell(Null, 6, ucwords($client_info[0]->city), 0, 1, 'L');
        $pdf->Cell(30, 6, NULL, 0, 0, 'L');
        $pdf->Cell(Null, 6, ucwords($client_info[0]->zip_code), 0, 1, 'L');

        $pdf->Ln();

        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(25, 8, 'Datum', 0, 0, 'L');
        $pdf->Cell(5, 8, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 8, date('d-m-Y', strtotime($invoices[0]->invoice_date)), 0, 1, 'L');

        $pdf->Cell(25, 8, 'Factuurnummer', 0, 0, 'L');
        $pdf->Cell(5, 8, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 8, $invoices[0]->invoice_number, 0, 1, 'L');

        $pdf->Cell(25, 8, 'Onderwerp', 0, 0, 'L');
        $pdf->Cell(5, 8, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 8, $invoices[0]->invoice_subject, 0, 1, 'L');

        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(95, 7, ' Specificaties Diensten', 'LT', 0, 'L');
        $pdf->Cell(95, 7, 'Totaal ', 'RT', 1, 'R');
        $pdf->SetFont('Arial', '', 10);
 //$pdf->Cell(95, 7, ' Abonnement : NULL', 'LB', 0, 'L');
        $pdf->Cell(95, 7, '', 'LB', 0, 'L');
        $pdf->SetFont('', '', 10);
        $pdf->Cell(95, 7, EURO . ' ' . $invoices[0]->invoice_amount . ' ', 'RB', 1, 'R');

        $pdf->Ln();
        $pdf->Cell(168, 7, NULL, 'LTB', 0, 'L');
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(10, 7, 'Totaal : ', 'TB', 0, 'R');
        $pdf->SetFont('Arial', '', 10);

        $pdf->Cell(12, 7, EURO . ' ' . $invoices[0]->invoice_amount . ' ', 'RTB', 1, 'R');

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(Null, 10, 'Wij danken u hartelijk voor het gebruik maken van onze dienstverlening.', 0, 1, 'C');
        $pdf->Output();
    }

}

?>
