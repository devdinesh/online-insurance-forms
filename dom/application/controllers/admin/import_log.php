<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate client and the user whos role is admin process the
 * login and other
 * methods.
 */
class import_log extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();
        $this->load->model('ins_import_log_model');
        $this->load->model('clients_model');
        $this->load->helper('lgin');
        $this->default_template_name = get_admin_template_name();
    }

    /**
     * get the view list of the import log table.
     */
    public function index() {
        $data['page_name'] = 'admin/import_log/view';
        $data['title'] = 'Import logboek';
        $this->load->view($this->default_template_name, $data);
    }

    function export_excel() {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $data['client_name'] = $client->client_name;
        $data['import_list'] = $this->ins_import_log_model->get_where_sort(array('client_id' => $client_id));
        $this->load->view('admin/import_log/createxls', $data);
    }

    function delete($id) {
        $this->ins_import_log_model->delete($id);
        redirect('admin/import_log', 'refresh');
    }

    function delete_all() {
        $client = admin_get_client_id();
        $client_id = $client->client_id;

        $import_list = $this->ins_import_log_model->get_where_sort(array('client_id' => $client_id));
        foreach ($import_list as $import) {
            $this->ins_import_log_model->delete($import->import_log_id);
        }
    }

}

?>
