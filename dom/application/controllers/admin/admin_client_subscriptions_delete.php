<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class admin_client_subscriptions extends CI_Controller {

    var $default_template_name;

    function __construct() {
        parent::__construct();

        $this->load->model('clients_model');
        $this->load->model('client_subscription_model');
        $this->load->model('admin_subscription_kinds_model');
        $this->load->model('invoice_model');
        $this->load->model('forms_model');
        $this->load->model('claims_model');
        $this->default_template_name = get_admin_template_name();
    }

    /**
     * this will render the super admin login form
     */
    public function index() {
        $client = admin_get_client_id();
        $client_id = $client->client_id;
        $data['page_name'] = 'admin/subscriptions/clinet_specific_subscription_index';
        $data['title'] = 'Abonnement';
        $data['client_id'] = $client_id;

        $this->load->view($this->default_template_name, $data);
    }

    function client_subscriptions_json($client_id) {
        // get subscriptiosn for given client id
        $client_subscription = new client_subscription_model();
        $client_subscriptions = $client_subscription->get_where(
                array('client_id' => $client_id
                ));
        $result = array();
        // iterate thru each of them
        foreach ($client_subscriptions as $subscription) {
            // get subscription type
            $subscription_info = $this->admin_subscription_kinds_model->selectSingleRecord(
                    'subscription_kinds_id', $subscription->subscription_kinds_id);
         
            $subscription_type = $subscription_info[0]->subscription_title;
            $record = array();

            // construct array
            $record[] = ucwords($subscription_type);
            $record[] = ucwords($subscription_info[0]->maximum_forms);
            $record[] = '<a href="' . base_url() .
                    'admin/admin_client_subscriptions/edit/' .
                    $subscription->client_subscription_id . '">' .
                    $subscription->from_date . "</a>";
            $record[] = $subscription->end_date;
            $record[] = '&euro; ' .$subscription_info[0]->subscription_fee;
            $record[] = '<a href="' . base_url() . 'admin/list_insurance/form_overview/' . $subscription->client_subscription_id . '"><img style="height: 35px;"  src="' . assets_url_img . 'statistics.png' . '" alt="View Graph" title="View Graph"/></a>';
            if(isset($subscription->end_date) && $subscription->end_date!=null){
            $record[] = '';
            }else{
                $record[]='<a href="javascript:;" onclick="deleteRow(this)" class="deletepage" id="' . $subscription->client_subscription_id . '"><img src="' . assets_url_img . 'stop.png" alt="Delete" title="Delete" ></a>';
            }
            $result[] = $record;
        }

        $ans['aaData'] = $result;
        echo json_encode($ans);
    }

    function client_invoice_json($client_id) {
        $this->load->model('invoice_model');
        $invoice_model = new invoice_model();
        $invoices = $invoice_model->get_where(
                array('client_id' => $client_id
                ));
        $arra = array();

        foreach ($invoices as $invoice) {
            $temp_arr = array();

            $temp_arr[] = $invoice->invoice_number;
            $temp_arr[] = $invoice->invoice_subject;
            $temp_arr[] = '&euro; ' . $invoice->invoice_amount;
            $temp_arr[] = $invoice->invoice_date;
            $temp_arr[] = '<a target="_blank" href="' . base_url() . 'admin/admin_client_subscriptions/create_pdf/' . $invoice->invoice_number . '"> <img style="height: 35px;"  src="' . assets_url_img . 'PDF-Document-icon.png' . '" alt="alt"/> </a>';
            $arra[] = $temp_arr;
        }

        $data['aaData'] = $arra;
        if (is_array($data)) {
            echo json_encode($data);
        }
    }

    /**
     * this method renders the view to let the super admin edit the
     * subscription whis id is passed as parameter
     *
     * @param type $client_subscription_id
     *            is the id of subscription that you want
     *            to edit.
     */
    function edit($client_subscription_id) {

        // get the client subscription whos id is passed
        $client_subscription = new client_subscription_model();
        $subscriptions = $client_subscription->get_where(
                array('client_subscription_id' => $client_subscription_id
                ));
        $subscription = $subscriptions[0];

        // construct the parameters to be passed
        $data['page_name'] = 'admin/subscriptions/edit_subscription';
        $data['title'] = 'Abonnementen Bewerken';
        $data['subscription'] = $subscription;
        $admin_subscription_kinds = new admin_subscription_kinds_model();

        $data['subcription_kinds'] = $admin_subscription_kinds->get_all();

        // $subscription_kinds = $subscription->
        // render the view
        $this->load->view($this->default_template_name, $data);
    }

    function edit_listener($client_subscription_id) {
        // echo $client_subscription_id;
        $obj = new client_subscription_model();
        $obj = $obj->get_where(
                array('client_subscription_id' => $client_subscription_id
                ));
        $obj = $obj[0];

        // add a rule for date comparison
        $obj->form_validations[1]['rules'] = $obj->form_validations[1]['rules'] .
                "|date_greater[" . $this->input->post('from_date') .
                "]|not_same_as[" . $this->input->post('from_date') . "]";
        // add a rule for for date coliding
        // see the comments in the validator if you wish
        // application->librabry->MY_Form_validation.php->
        // client_subscription_coliding method
        /*
         * $obj->form_validations[1]['rules'] =
         * $obj->form_validations[1]['rules'] . "|client_subscription_coliding["
         * . str_replace("-", "-", $this->input->post('from_date')) . "," .
         * $obj->client_id . ',' . $client_subscription_id . "]";
         */
        $this->form_validation->set_rules($obj->form_validations);
        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('from_date');
            $this->form_validation->set_value('to_date');
            // validation failed, render the form again with error messages
            $this->edit($client_subscription_id);
        } else {
            // set the values back
            $obj->from_date = $this->input->post('from_date');
            $obj->end_date = $this->input->post('end_date');
            $obj->subscription_kinds_id = $this->input->post(
                    'subscription_kinds_id');
            $obj->update();
            $this->session->set_flashdata('success', 'Subscription updated Sucessufully');
            // / send to view all page
            redirect(base_url() . "admin/admin_client_subscriptions");
        }
    }

    /**
     * this method renders the view to let the super admin add the
     * subscription whis id is passed as parameter
     *
     * @param type $client_id
     *            is the id of client that you want
     *            to add new subscription for that client.
     */
    function add($client_id) {
        $data['page_name'] = 'admin/subscriptions/add_subscription';
        $data['title'] = 'Toevoegen Abonnement';
        $data['client_id'] = $client_id;
        $admin_subscription_kinds = new admin_subscription_kinds_model();

        $data['subcription_kinds'] = $admin_subscription_kinds->get_all();
        $this->load->view($this->default_template_name, $data);
    }

    function add_listener($client_id) {
        $obj = new client_subscription_model();
        // add a rule for date comparison
        $obj->form_validations[1]['rules'] = $obj->form_validations[1]['rules'] .
                "|date_greater[" . $this->input->post('from_date') .
                "]|not_same_as[" . $this->input->post('from_date') . "]";
        // add a rule for for date coliding
        // see the comments in the validator if you wish
        // application->librabry->MY_Form_validation.php->
        // client_subscription_coliding method
        // new rules for subscription_kinds_id is required
        $obj->form_validations[1]['rules'] = $obj->form_validations[1]['rules'] .
                "|client_subscription_coliding[" .
                str_replace("-", "-", $this->input->post('from_date')) . "," .
                $obj->client_id . ',' . $client_id . "]";
        $this->form_validation->set_rules('subscription_kinds_id', 'subscription_kinds', 'required');
        $this->form_validation->set_rules($obj->form_validations);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('from_date');
            $this->form_validation->set_value('end_date');
            $this->form_validation->set_value('subscription_kinds_id');
            // validation failed, render the form again with error messages
            $this->add($client_id);
        } else {
            // get the subscription object in order to get the price
            $this->load->model('admin_subscription_kinds_model');
            $admin_subscription_kinds_model = new admin_subscription_kinds_model();
            $kinds = $admin_subscription_kinds_model->get_wher(
                    array(
                        'subscription_kinds_id' => $this->input->post(
                                'subscription_kinds_id')
                    ));
            $kind = $kinds[0];

            // set the values back
            $obj->client_id = $client_id;
            $obj->from_date = $this->input->post('from_date');
            $obj->end_date = $this->input->post('end_date');
            $obj->subscription_kinds_id = $this->input->post(
                    'subscription_kinds_id');
            $obj->monthly_fee = $kind->subscription_fee;
            $obj->save();

            // get the subscription king in order to get the
            // amount etc
            $admin_subscription_kinds_model = new admin_subscription_kinds_model();
            $subscription_kinds = $admin_subscription_kinds_model->get_wher(
                    array(
                        'subscription_kinds_id' => $this->input->post(
                                'subscription_kinds_id')
                    ));

            // make an entry in invoices table
            $this->load->model('invoice_model');
            $invoice_model = new invoice_model();
            $invoice_model->invoice_amount = $subscription_kinds[0]->subscription_fee;
            $invoice_model->invoice_period_start = $this->input->post(
                    'from_date');
            $invoice_model->invoiece_period_end = $this->_add_one_month_to_date(
                    $this->input->post('from_date'));
            $invoice_model->invoice_date = date('d-m-Y'); // set the current
            // date
            $invoice_model->client_id = $client_id;
            $invoice_model->invoice_subject = "Invoice for Online Insurance Forms";
            $invoice_model->save();

            // send to view all page
            $this->session->set_flashdata('success', 'Abonnement is afgesloten.');
            redirect(base_url() . "admin/admin_client_subscriptions");
        }
    }

    // create pdf files for invoices
    public function create_pdf($invo_number_with_year) {
        $no_with = substr($invo_number_with_year, 4);
        $invo_number = (int) $no_with;

        $invoice_model = new invoice_model();
        $invoices = $invoice_model->get_where(array('invoice_number' => $invo_number));


        $obj_client = new clients_model();
        $client_info = $obj_client->get_where(array('client_id' => $invoices[0]->client_id));
        // load the helper for get the libaray files

        $this->load->helper('pdf/generate_pdf');

        // it include the lib needed to generate the PDF files

        include_lib_pdf();

        $pdf = new FPDF();

        $pdf->AddPage();

        $this->load->helper('admin_setting_info');
        $logo_url = get_super_admin_logo();


        $pdf->Image($logo_url, 65, 10, 100, 25);
        $pdf->Ln(35);
        /*
         * this medthod makes the cell for table
         * @param1 : width of cell
         * @param2 : height of cell
         * @param3 : content of cell
         * @param4 : border of cell (1 = YES | 0 = NO)
         * @param5 : move to next line (1 = YES | 0 = NO)
         * @param6 : aligment of text (L= Left | C = CENTER | R = RIGHT)
         */

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(25, 6, 'Factuuradres', 0, 0, 'L');
        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(5, 6, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 6, ucwords($client_info[0]->title_application), 0, 1, 'L');
        $pdf->Cell(30, 6, NULL, 0, 0, 'L');
        $pdf->Cell(Null,6, ucwords($client_info[0]->address), 0, 1, 'L');
        $pdf->Cell(30, 6, NULL, 0, 0, 'L');
        $pdf->Cell(Null,6, ucwords($client_info[0]->city), 0, 1, 'L');
        $pdf->Cell(30, 6, NULL, 0, 0, 'L');
        $pdf->Cell(Null,6, ucwords($client_info[0]->zip_code), 0, 1, 'L');

        $pdf->Ln();

        $pdf->SetFont('Arial', '', 10);
        $pdf->Cell(25, 8, 'Datum', 0, 0, 'L');
        $pdf->Cell(5, 8, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 8, date('d-m-Y', strtotime($invoices[0]->invoice_date)), 0, 1, 'L');

        $pdf->Cell(25, 8, 'Factuurnummer', 0, 0, 'L');
        $pdf->Cell(5, 8, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 8, $invoices[0]->invoice_number, 0, 1, 'L');

        $pdf->Cell(25, 8, 'Onderwerp', 0, 0, 'L');
        $pdf->Cell(5, 8, '  :', 0, 0, 'L');
        $pdf->Cell(Null, 8, $invoices[0]->invoice_subject, 0, 1, 'L');

        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(95, 7, ' Specificaties Diensten', 'LT', 0, 'L');
        $pdf->Cell(95, 7, 'Totaal ', 'RT', 1, 'R');
        $pdf->SetFont('Arial', '', 10);

        $obj_subscription = new admin_subscription_kinds_model();
        $subscription_record = $obj_subscription->get_wher(array('subscription_kinds_id' => $invoices[0]->subscription_kind_id));
    
        $pdf->Cell(95, 7, ' Abonnement : ' . @ucwords($subscription_record[0]->subscription_title), 'LB', 0, 'L');
        $pdf->SetFont('', '', 10);
        $pdf->Cell(95, 7, '�'.' ' . $invoices[0]->invoice_amount . ' ', 'RB', 1, 'R');

        $pdf->Ln();
        $pdf->Cell(168, 7, NULL, 'LTB', 0, 'L');
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(10, 7, 'Totaal : ', 'TB', 0, 'R');
        $pdf->SetFont('Arial', '', 10);
        
        $pdf->Cell(12, 7, '�'.' '. $invoices[0]->invoice_amount . ' ', 'RTB', 1, 'R');

        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(Null, 10, 'Wij danken u hartelijk voor het gebruik maken van onze dienstverlening.', 0, 1, 'C');
        $pdf->Output();
    }

    private function _add_one_month_to_date($date) {
        $new_date = date("d-m-Y", strtotime($date));
        $newdate = strtotime('+1 month -1day', strtotime($new_date));
        return date("d-m-Y", $newdate);
    }

    function stopSubscription($id) {
        $admin_details = $this->session->userdata('admin');
        $client_id = $admin_details['client_id'];
        $obj_sub = new client_subscription_model();
        $sub_record = $obj_sub->get_where(array('client_subscription_id' => $id, 'client_id' => $client_id));

        if (count($sub_record) == 1) {
            if ($sub_record[0]->end_date === NULL) {
                $start_date = $sub_record[0]->from_date;
                $day = date('d', strtotime($start_date));
                $month = get_current_time()->month + 1;
                $year = date('Y', strtotime($start_date));
                if ($month > 12) {
                    $month = 1;
                    $year = $year + 1;
                }

                $obj_sub->client_subscription_id = $id;
                $obj_sub->client_id = $client_id;
                $obj_sub->subscription_kinds_id = $sub_record[0]->subscription_kinds_id;
                $obj_sub->from_date = $sub_record[0]->from_date;
                $obj_sub->end_date = $year . '-' . $month . '-' . $day;
                $obj_sub->monthly_fee = $sub_record[0]->monthly_fee;
                $obj_sub->update();

                $obj_subscription = new admin_subscription_kinds_model();
                $subscription_record = $obj_subscription->get_wher(array('subscription_kinds_id' => $sub_record[0]->subscription_kinds_id));

                $total_forms_imported = $this->claims_model->getFormImportBetweenDates($client_id, $obj_sub->from_date, $obj_sub->end_date);
                $from_date = new DateTime($obj_sub->from_date);
                $end_date = new DateTime($obj_sub->end_date);
                $total_months_of_subscription = ($from_date->diff($end_date)->m == 0) ? 1 : $from_date->diff($end_date)->m;
                $from_per_month = $total_forms_imported / $total_months_of_subscription;

                if ($from_per_month > $subscription_record[0]->maximum_forms) {

                    $amount = (double) ((int) $total_forms_imported - ((int) $total_months_of_subscription * (int) $subscription_record[0]->maximum_forms)) * (double) $subscription_record[0]->rate_per_extra_form;

                    $obj_invoice = new invoice_model();
                    $obj_invoice->invoice_date = get_current_time()->get_date_for_db();
                    $obj_invoice->subscription_kind_id = $sub_record[0]->subscription_kinds_id;
                    $obj_invoice->invoice_amount = $amount;
                    $obj_invoice->invoice_period_start = $obj_sub->from_date;
                    $obj_invoice->invoiece_period_end = $obj_sub->end_date;
                    $obj_invoice->invoice_subject = 'Verrekening Digitaal op Maat eind abonnement';
                    $obj_invoice->client_id = $client_id;
                    $obj_invoice->save();
                }
                $this->session->set_flashdata('success', 'Subscription Stop Successfully.');
            } else {
                $this->session->set_flashdata('error', 'Subscription already over.');
            }
        } else {
            $this->session->set_flashdata('error', 'The URL is Mofidied');
        }
        redirect(base_url() . 'admin/admin_client_subscriptions', 'refresh');
    }

}

?>
