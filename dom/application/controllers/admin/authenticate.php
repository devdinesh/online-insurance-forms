<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * This class Authenticate the super admin and process the login and other
 * methods.
 */
class authenticate extends CI_Controller {

    /**
     * this is the default constructor
     */
    function __construct() {
        parent::__construct();

        $this->load->model('users_model');
        $this->load->model('banner_model');
        $this->load->model('feature_model');
        $this->load->model('menu_model');
 }

    /**
     * this will render the super admin login form
     */
    public function index($msg = '') {
        //  $this->getPage();
        $this->login();
    }

    function getPage($slug = NULL) {
        // redirect to hashbard if already logged in
        if (is_logged_in()) {
            $type = $this->session->userdata('admin');
            if ($type) {
                redirect(base_url() . "admin/dashboard");
            }
        }
        if ($slug == NULL) {
            $slug = 'home_general';
        }
        $text_content = $this->admin_texts_model->get_wher(array('page_slug' => $slug));
        $data['text_content'] = $text_content[0];

        //default variable value ..
        $data['feature_details'] = '';
        $data['Pakketten'] = FALSE;
        $data['contact_form'] = 0;
        $data['rates'] = array();
        //
        $menu_content = $this->menu_model->get_where(array('page' => $text_content[0]->text_id));
        $data['title'] = $menu_content[0]->title;

        if (isset($menu_content[0]->special_feature) && $menu_content[0]->special_feature != NULL) {
            if ($menu_content[0]->special_feature == 'Features') {
                $feature_info = $this->feature_model->get_all();
                if (!empty($feature_info)) {
                    $data['feature_details'] = $feature_info;
                }
            } elseif ($menu_content[0]->special_feature == 'Calculator') {
                $data['Pakketten'] = TRUE;
                $data['rates'] = $this->rates_model->getAllrates();
            } elseif ($menu_content[0]->special_feature == 'Contact form') {
                $data['contact_form'] = 1;
            } else {
                
            }
        }
        $data['banner_images'] = $this->banner_model->getSliderImages();
        $text_points = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-points'
                ));
        $data['text_points'] = $text_points[0];
        $text_video = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-video'
                ));
        $data['text_video'] = $text_video[0];

        $data['page_name'] = 'admin/un_authenticated/show_guideline';
        $this->load->view('admin/login_template', $data);
    }

    function login($msg = '') {
        $data['login_error'] = $msg;
        $redirect_to = 'admin/dashboard';
        if (isset($_GET['redirect_to'])) {
            $redirect_to = $_GET['redirect_to'];
        }
        $data['redirect_to'] = $redirect_to;
        $data['page_name'] = 'admin/authenticate/login';
        $data['title'] = 'Welcome to Digitaalopmaat';
        $this->load->view('admin/login_template', $data);
    }

    /**
     * this will process the data passed by login form
     */
    function checkLogin() {

        $authenticate_model = new users_model();
        $rule[] = $authenticate_model->validation_rules[4]; // email
        $rule[0]['field'] = 'email';
        $rule[] = $authenticate_model->validation_rules[5]; // password
        $rule[1]['field'] = 'password';
        $this->form_validation->set_rules($rule);

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('email');
            $this->form_validation->set_value('password');
            $this->login();
        } else {

            $this->session->unset_userdata('admin');
            $this->session->unset_userdata('super_admin');
            $this->session->unset_userdata('policy_holder');

            // get the fields posted by the user
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            // form validations were sucessful so we need to
            // check the login

            try {
                // check the login status
                $login_status = $authenticate_model->check_email_and_password(
                        $email, $password);
                $admins = $this->users_model->get_where(
                        array('mail_address' => $email
                        ));
                $admin_object = $admins[0];
                $update_user = $this->users_model->update_user_login_date(
                        $admin_object->user_id);

                // if the user is admin
                if ($admin_object->role != 'admin') {
                    throw new InvalidEmailException();
                }

                $this->set_session_for_admin($admin_object);
//                if (isset($_GET['redirect_to'])) {
//                    $redirect_to = $_GET['redirect_to'];
//                }
                $role_menus = GetMenus();
                $check_redirect = array_search('TRUE', $role_menus);
                if ($check_redirect == 'show_dashbord') {
                    $redirect_to = 'admin/dashboard';
                } elseif ($check_redirect == 'show_relations') {
                    $redirect_to = 'admin/policy_holder';
                } elseif ($check_redirect == 'show_own_forms') {
                    $redirect_to = 'admin/client_forms';
                } elseif ($check_redirect == 'show_forms') {
                    $redirect_to = 'admin/standard_forms';
                } elseif ($check_redirect == 'show_news') {
                    $redirect_to = 'admin/news';
                } elseif ($check_redirect == 'show_statistics') {
                    $redirect_to = 'admin/statistic';
                } elseif ($check_redirect == 'show_newsletters') {
                    $redirect_to = 'admin/newletter';
                } elseif ($check_redirect == 'show_other') {
                    $redirect_to = 'admin/admin_user';
                } else {
                    if (isset($_GET['redirect_to'])) {
                        $redirect_to = $_GET['redirect_to'];
                    } else {
                        $redirect_to = 'admin/dashboard';
                    }
                }
                redirect(base_url() . $redirect_to);
            } catch (InvalidPasswordException $ipe) {
                $this->form_validation->set_value('email');
                $this->login('Gebruikersnaam en/of wachtwoord is niet juist');
            } catch (InvalidEmailException $iee) {
                $this->form_validation->set_value('email');
                $this->login('Gebruikersnaam en/of wachtwoord is niet juist');
            }
        }
    }

    /**
     * this will render the forgot password form
     */
    function forgotPassword($msg = '') {
        $data['title'] = 'Admin';
        $redirect_to = 'admin/dashboard';
        if (isset($_GET['redirect_to'])) {
            $redirect_to = $_GET['redirect_to'];
        }
        $data['page_name'] = 'admin/authenticate/forgot_password';
        $data['login_error'] = $msg;
        $data['redirect_to'] = $redirect_to;
        $this->load->view('admin/login_template', $data);
    }

    /**
     * this will process the data passed by forgot password form
     */
    function forgotpassword_listener() {
        $this->load->model('users_model');

        $users_model = new users_model();

        $this->form_validation->set_rules('email', 'email', 'required|min_length[3]|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $this->form_validation->set_value('email');
            $this->forgotPassword();
        } else {

            $randid = random_string('alnum', 30);

            try {
                $email_address = $this->input->post('email');
                try {

                    $check = $users_model->check_email_and_password(
                            $email_address, 'aa');
                } catch (InvalidPasswordException $iee) {
                    // do nothing if password is wrong
                }
                // we get here if the email is valid
                // set the random string to the admin whos email address
                // is entered in the previous page
                $users_model = new users_model();
                $admins = $users_model->get_where(
                        array('mail_address' => $email_address
                        ));
                $admin = $admins[0];
                $admin->password_reset_random_string = $randid;
                $admin->dataUpdate($admin->user_id);
                // get the email template to be sent

                $this->load->model('mail_template_model');
                $emails = new mail_template_model();
                $emails = $emails->get_where(
                        array('email_type' => 'reset_password'
                        ));
                $email = $emails[0];

                // load the functio
                $this->load->helper('sending_mail');

                // get the email message etc from template
                $subject = $email->email_subject;
                $attachment = $email->email_attachment;
                $message = $email->email_message;

                // construct the reset link that user click and reset the
                // password
                $reset_link = anchor(
                        base_url() . "admin/authenticate/resetPassword/$randid", 'Click Here');

                // set the <link> to actual link etc
                $message = str_replace('&lt;link&gt;', $reset_link, $message);

                $email_sent_success = send_mail($email_address, $subject, $message, '', $attachment);

                if ($email_sent_success == TRUE) {
                    $this->session->set_flashdata('success', 'There is a mail send to your mail address');
                    redirect(base_url());
                } else {
                    // something went wrong, print the debugg message
                    show_error($this->email->print_debugger());
                }
            } catch (InvalidEmailException $iee) {
                $this->form_validation->set_value('email');
                $this->forgotPassword('Mail address doesn\'t exist');
            }
        }
    }

    /**
     * this will render the reset password form with random id (randid) check
     * whose password is to reset.
     * if any unknow person comes and put any email id so for security purpose a
     * rand id will de taken which is updated at time send mail
     * to super admin.
     *
     * @param type $randid
     *            : alpha-numeric string.
     */
    function resetPassword($randid) {
        $data['title'] = 'Reset Password';
        $redirect_to = '/';
        if (isset($_GET['redirect_to'])) {
            $redirect_to = $_GET['redirect_to'];
        }
        $data['page_name'] = 'admin/authenticate/reset_password';
        $data['redirect_to'] = $redirect_to;
        $data['randid'] = $randid;

        // get the admin with given random string
        $this->load->model('users_model');
        $users_model = new users_model();
        $admins = $users_model->get_where(
                array('password_reset_random_string' => $randid
                ));

        // if any admin exists with given id then allow
        if (is_array($admins) && count($admins) > 0) {
            $this->load->view('admin/login_template', $data);
        } else {
            // the admin does not exists so we need to redirect with error
            // message
            $this->session->set_flashdata('error', 'Reset password\'s link is expired. Please click on reset password again to get new link.');
            redirect(base_url());
        }
    }

    /**
     * this will process the reset password form and the data send by end user
     * i.e.
     * admin
     *
     * @param string $randid
     *            : alpha-numeric string
     */
    function resetPasswordListener($randid) {
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[4]|matches[new_password]');

        // if both passwords are same
        if ($this->form_validation->run() == FALSE) {
            // render the same page again
            $this->resetPassword($randid);
        } else {
            // get the admin to update his password
            $users_model = new users_model();
            $admins = $users_model->get_where(
                    array('password_reset_random_string' => $randid
                    ));
            if (is_array($admins) && count($admins) > 0) {
                $admin = $admins[0];
                $admin->password = md5($this->input->post('new_password'));
                echo $this->input->post('new_password');
                // exit;
                $admin->password_reset_random_string = ''; // invalidate the
                // random string
                $admin->dataUpdate($admin->user_id);

                // set the message and redirect
                $this->session->set_flashdata('success', 'The password reset was successful. Please login with new password.');
                redirect(base_url());
            } else {
                // set the message and redirect
                $this->session->set_flashdata('error', 'Reset password\'s link is expired. Please click on reset password again to get new link.');
                redirect(base_url());
            }
        }
    }

    function logout() {
        $type = $this->session->userdata('super_admin');

        // if super_admin is not logged in
        if (!$type) {
            $this->session->sess_destroy();
        } else {
            $this->session->unset_userdata('admin');
        }
        $this->session->sess_destroy();
        // redirect(base_url() . "authenticate");
        redirect(base_url());
    }

    function set_session_for_admin($admin_object) {
        // set the session and redirect the user
        $admin_arr = $admin_object->to_associative_array();
        $session = array('admin' => $admin_arr, 'user_type' => 'admin',
            'logged_in' => true
        );
        $this->session->set_userdata($session);
    }

    function show_news() {
        $this->load->model('super_admin_news_model');
        $this->load->helper('time_worked');
        $date = get_current_time()->get_date_for_db();
        $data['current_date'] = date("d-m-Y", strtotime($date));

        $get_all_news = $this->super_admin_news_model->get_all();

        if (count($get_all_news) > 0) {
            $data['page_name'] = 'admin/un_authenticated/show_news';
            $data['title'] = 'News';
            $redirect_to = 'admin/dashboard';
            if (isset($_GET['redirect_to'])) {
                $redirect_to = $_GET['redirect_to'];
            }
            $data['redirect_to'] = $redirect_to;
        }
        $data['all_news_info'] = $get_all_news;

        $this->load->view('admin/login_template', $data);
    }

    /**
     * this will render the about us forms form
     */
    public function about_us() {

        // redirect to hashbard if already logged in
        if (is_logged_in()) {
            $type = $this->session->userdata('admin');
            if ($type) {
                // redirect(base_url() . "admin/dashboard");
            }
        }
        $redirect_to = 'admin';
        if (isset($_GET['redirect_to'])) {
            $redirect_to = $_GET['redirect_to'];
        }
        /**
         * get the text content which is display at the home page where
         * text_title='Over ons'
         */
        $text_content = $this->admin_texts_model->get_wher(
                array('text_title' => 'Over ons'
                ));
        $data['text_content'] = $text_content;
        // render the page otherwise

        $data['redirect_to'] = $redirect_to;
        $data['title'] = 'Admin';
        $data['page_name'] = 'admin/un_authenticated/about_us';
        $this->load->view('admin/login_template', $data);
    }

    /**
     * sets the image path for logo in the session
     *
     * @param string $url
     *            is the unique url of user
     */
    public function set_logo($url) {
        // get the client whos url is $url's value
        $this->load->model('clients_model');
        $clients_model = new clients_model();
        $clients = $clients_model->get_where(
                array('current_url' => $url
                ));

        // if the url passed is valid and the
        // user with logo exists
        if (count($clients) > 0) {
            // get the image name from the database
            $image_name = $clients[0]->logo;

            // construct absolute path to image/logo that needs to be set
            $image_path = base_url() . "assets/upload/client_logo/" . $image_name;

            // put the image path in session

            $image_path = array('logo_url' => $image_path,
                'client_id' => $clients[0]->client_id
            );

            $this->session->set_userdata($image_path);
        } else {
            echo "The client does not exists";
        }

        // redirect the user to the home in any case, whether the logo was found
        // or not
        redirect(base_url());
    }

    function hoe_werkt_het() {
        $data['banner_images'] = $this->banner_model->getSliderImages();
        $text_points = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-points'
                ));
        $data['text_points'] = $text_points[0];
        $text_video = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-video'
                ));
        $data['text_video'] = $text_video[0];
        $text_content = $this->admin_texts_model->get_wher(array('text_title' => 'Hoe werkt het'));
        $data['text_content'] = $text_content[0];
        $data['title'] = 'Hoe werkt het';
        $data['page_name'] = 'admin/un_authenticated/show_guideline';
        $this->load->view('admin/login_template', $data);
    }

    function pakketten() {
        $data['banner_images'] = $this->banner_model->getSliderImages();
        $data['rates'] = $this->rates_model->getAllrates();
        // $data['user_rates'] = $this->rates_user_model->getAllrates();
        $text_points = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-points'
                ));
        $data['text_points'] = $text_points[0];
        $text_video = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-video'
                ));
        $data['text_video'] = $text_video[0];
        $text_content = $this->admin_texts_model->get_wher(array('text_title' => 'Pakketten'));
        $data['Pakketten'] = TRUE;
        $data['text_content'] = $text_content[0];
        $data['title'] = 'Hoe werkt het';
        $data['page_name'] = 'admin/un_authenticated/show_guideline';
        $this->load->view('admin/login_template', $data);
    }

    function faq() {
        $data['banner_images'] = $this->banner_model->getSliderImages();
        $text_points = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-points'
                ));
        $data['text_points'] = $text_points[0];
        $text_video = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-video'
                ));
        $data['text_video'] = $text_video[0];
        $text_content = $this->admin_texts_model->get_wher(array('text_title' => 'F.A.Q'));
        $data['text_content'] = $text_content[0];
        $data['title'] = 'Hoe werkt het';
        $data['page_name'] = 'admin/un_authenticated/show_guideline';
        $this->load->view('admin/login_template', $data);
    }

    function uitleg() {
        $data['banner_images'] = $this->banner_model->getSliderImages();
        $text_points = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-points'
                ));
        $data['text_points'] = $text_points[0];
        $text_video = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-video'
                ));
        $data['text_video'] = $text_video[0];
        $text_content = $this->admin_texts_model->get_wher(array('text_title' => 'Uitleg'));
        $data['text_content'] = $text_content[0];
        $data['title'] = 'Uitleg';
        $data['page_name'] = 'admin/un_authenticated/show_guideline';
        $this->load->view('admin/login_template', $data);
    }

    function demo() {
        $data['banner_images'] = $this->banner_model->getSliderImages();
        $text_points = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-points'
                ));
        $data['text_points'] = $text_points[0];
        $text_video = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-video'
                ));
        $data['text_video'] = $text_video[0];
        $text_content = $this->admin_texts_model->get_wher(array('text_title' => 'Demo'));
        $data['text_content'] = $text_content[0];
        $data['title'] = 'Demo';
        $data['page_name'] = 'admin/un_authenticated/show_guideline';
        $this->load->view('admin/login_template', $data);
    }

    function over_ons() {
        $data['banner_images'] = $this->banner_model->getSliderImages();
        $text_points = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-points'
                ));
        $data['text_points'] = $text_points[0];
        $text_video = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-video'
                ));
        $data['text_video'] = $text_video[0];
        $text_content = $this->admin_texts_model->get_wher(array('text_title' => 'Over ons'));
        $data['text_content'] = $text_content[0];
        $data['title'] = 'Hoe werkt het';
        $data['page_name'] = 'admin/un_authenticated/show_guideline';
        $this->load->view('admin/login_template', $data);
    }

    function voordelen() {
        $data['banner_images'] = $this->banner_model->getSliderImages();
        $text_points = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-points'
                ));
        $data['text_points'] = $text_points[0];
        $text_video = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-video'
                ));
        $data['text_video'] = $text_video[0];
        $text_content = $this->admin_texts_model->get_wher(array('text_title' => 'Voordelen'));
        $data['text_content'] = $text_content[0];
        $data['title'] = 'Voordelen';
        $data['page_name'] = 'admin/un_authenticated/show_guideline';
        $this->load->view('admin/login_template', $data);
    }

    function contact() {
        $data['banner_images'] = $this->banner_model->getSliderImages();
        $text_points = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-points'
                ));
        $data['text_points'] = $text_points[0];
        $text_video = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-video'
                ));
        $data['text_video'] = $text_video[0];
        $text_content = $this->admin_texts_model->get_wher(array('text_title' => 'Contact'));
        $data['text_content'] = $text_content[0];
        $data['title'] = 'Hoe werkt het';
        $data['contact_form'] = 1;
        $data['page_name'] = 'admin/un_authenticated/show_guideline';
        $this->load->view('admin/login_template', $data);
    }

    function disclaimer() {
        $data['banner_images'] = $this->banner_model->getSliderImages();
        $text_points = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-points'
                ));
        $data['text_points'] = $text_points[0];
        $text_video = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-video'
                ));
        $data['text_video'] = $text_video[0];
        $text_content = $this->admin_texts_model->get_wher(array('text_title' => 'Disclaimer'));
        $data['text_content'] = $text_content[0];
        $data['title'] = 'Disclaimer';
        $data['page_name'] = 'admin/un_authenticated/show_guideline';
        $this->load->view('admin/login_template', $data);
    }

    function privacy_disclaimer() {
        $data['banner_images'] = $this->banner_model->getSliderImages();
        $text_points = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-points'
                ));
        $data['text_points'] = $text_points[0];
        $text_video = $this->admin_texts_model->get_wher(
                array('text_title' => 'Home-video'
                ));
        $data['text_video'] = $text_video[0];
        $text_content = $this->admin_texts_model->get_wher(array('text_title' => 'Privacy disclaimer'));
        $data['text_content'] = $text_content[0];
        $data['title'] = 'Privacy disclaimer';
        $data['page_name'] = 'admin/un_authenticated/show_guideline';
        $this->load->view('admin/login_template', $data);
    }

    /* calulating client benifits */

    public function calculate_benefit($number_of_form, $cost_of_form) {
        $without_dom = $number_of_form * $cost_of_form;
        $rates = $this->rates_model->getAllrates();
        $final = 0;
        //  $cal = 0;
        for ($i = 0; $i < count($rates); $i++) {
            $test = ($rates[$i]->number_to - $rates[$i]->number_from) + 1;
            if ($number_of_form >= $test) {
                $cal = ($test) * $rates[$i]->rate;
                $final = $final + $cal;

                $number_of_form = $number_of_form - $test;
            } else {
                $cal = $number_of_form * $rates[$i]->rate;
                $final = $final + $cal;
                break;
            }
        }
        $with_dom = $final;
        //  $annual_users = 12 * $number_of_user * $rate_of_user;
        $benefits = ($without_dom - $with_dom);

        $array = array('cost_without_dom' => '&euro; ' . number_format($without_dom, 2, ",", "."), 'cost_with_dom' => '&euro; ' . number_format($with_dom, 2, ",", "."), 'benefits' => '&euro; ' . number_format($benefits, 2, ",", "."));
        echo json_encode($array);
        exit;
    }

    /* calulating client benifits */

    public function calculate_benefit_old($number_of_form, $cost_of_form, $number_of_user) {
        $where = array('from' => $number_of_user);
        $user_rates_info = $this->rates_user_model->getrates($number_of_user);
        $rate_of_user = $user_rates_info[0]->rate;
        $without_dom = $number_of_form * $cost_of_form;
        $rates = $this->rates_model->getAllrates();
        $final = 0;
        //  $cal = 0;
        for ($i = 0; $i < count($rates); $i++) {
            $test = ($rates[$i]->number_to - $rates[$i]->number_from) + 1;
            if ($number_of_form >= $test) {
                $cal = ($test) * $rates[$i]->rate;
                $final = $final + $cal;

                $number_of_form = $number_of_form - $test;
            } else {
                $cal = $number_of_form * $rates[$i]->rate;
                $final = $final + $cal;
                break;
            }
        }
        $with_dom = $final;
        $annual_users = 12 * $number_of_user * $rate_of_user;
        $benefits = ($without_dom - $with_dom - $annual_users);

        $array = array('cost_without_dom' => convert_dutch_eng($without_dom) . ' euro', 'cost_with_dom' => convert_dutch_eng($with_dom) . ' euro', 'benefits' => convert_dutch_eng($benefits) . ' euro', 'annual_users' => convert_dutch_eng($annual_users) . ' euro');
        echo json_encode($array);
        exit;
    }

}

/* End of file authenticate.php */
/* Location: ./application/controllers/authenticate.php */
