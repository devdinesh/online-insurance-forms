<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//language for invoice : : : : : : : 
$lang['invoices'] = 'Invoices';
$lang['client'] = 'Client';
$lang['select_client'] = 'Select Client';
$lang['client_name'] = 'Client Name';
$lang['invoice_number'] = 'Invoice Number';
$lang['subject'] = 'Subject';
$lang['invoice_amount'] = 'Invoice Amount';
$lang['invoice_date'] = 'Invoice Date';
$lang['email'] = 'E-Mail';

//language for Standard forms : : : : : : : 
$lang['form_overview'] = 'Overzicht Formulieren';
$lang['add_form'] = 'Toevoegen Formulier';
$lang['form_name'] = 'Formuliernaam';
$lang['code'] = 'Code';
$lang['categories'] = 'Categorieen';
$lang['import_fields'] = 'Import velden';
$lang['click_here'] = 'Klik Hier';
$lang['copy'] = 'Copy';
$lang['copy_form'] = 'Kopieer formulier';
$lang['create_pdf_file'] = 'Maak een PDF bestand';
$lang['show_preview'] = 'Toon preview';
$lang['remove_form'] = 'Verwijder formulier';
$lang['select_client_copy_form'] = 'Select client for copy the forms';
$lang['copy_sucess'] = 'The Form is Copied Sucessufully';
$lang['copy_error'] = 'Error in Copying The Form.';
$lang['forms'] = 'Formulieren';


//language for standard forms ... 

$lang['standard_forms'] = 'Standard forms';
$lang['name'] = 'Name';
$lang['self_administered'] = 'Zelf in te vullen door polishouder';
$lang['policyholder_complete_form'] = 'Polishouder moet formulier invullen';
$lang['yes'] = 'Ja';
$lang['no'] = 'Nee';
$lang['first_reminder'] = 'Eerste Herinnering';
$lang['second_reminder'] = 'Tweede Herinnering';
$lang['days'] = 'Dagen';
$lang['header_text'] = 'Koptekst';
$lang['introduction_text'] = 'Introductietekst';
$lang['closer_text'] = 'Afsluitende Tekst';

//labels for standard categories ...

$lang['overview_categories'] = 'Overzicht categorieen';
$lang['category_not_found'] = 'Error !! May Be Category not found';
$lang['add_category'] = 'Toevoegen Categorie';
$lang['back'] = 'Terug';
$lang['category_name'] = 'Categorienaam';
$lang['ask'] = 'Vragen';
$lang['category_click_error'] = 'Error !! Please Try Again from Strach by click on categoires "Click Here" then Add New Category.';
$lang['edit_category'] = 'Categorie bewerken';

//labels for standard form fields...
$lang['list_import_fields'] = 'Lijst import velden';
$lang['add_new_field'] = 'Nieuw veld toevoegen';
$lang['edit_new_field'] = 'Bewerk nieuw veld';
$lang['name_import_item'] = 'Naam import Item';
$lang['display_as'] = 'Weergeven als';
$lang['type_field'] = 'Type veld';
$lang['mark_field_as'] = 'Markeer veld als';
$lang['add_answer'] = 'Toevoegen antwoord';
$lang['list_answer'] = 'List Answers';


//labels for standard questions ......
$lang['add_question'] = 'Toevoegen vraag';
$lang['overview_question_category'] = 'Overzicht vragen van de categorie';
$lang['categories_questions'] = 'Categories Questions';
$lang['question_number'] = 'Vraagnummer';
$lang['question'] = 'Vraag';
$lang['answer'] = 'Antwoorden';
$lang['categories'] = 'Categories';
$lang['category'] = 'Categorie';
$lang['help_text'] = 'Helptekst';
$lang['is_mandatory'] = 'Is Verplicht';
$lang['answer_type'] = 'Antwoordsoort';
$lang['single_line_date'] = 'Enkele regel (datum)';
$lang['single_line_number'] = 'Enkele regel (getal)';
$lang['single_line_text'] = 'Enkele regel (tekst)';
$lang['multiple_lines'] = 'Meerdere regels';
$lang['some_selection'] = 'Enkele selectie';
$lang['multiple_selections'] = 'Meerdere selecties';
$lang['financial'] = 'Financieel';
$lang['+_add_answer'] = '+ Add Answer';
$lang['edit_question'] = 'Vraag bewerken';
$lang['overview_question_answer'] = 'Overzicht antwoorden voor vraag';
$lang['edit_question'] = 'Vraag bewerken';
$lang['go_to_question'] = 'Ga naar vraag';
$lang['end_form'] = 'Einde Formulier';
$lang['go_to_question_category'] = 'Ga naar vraag / categorie';
$lang['geen'] = ' Geen';
$lang['to_question'] = ' Naar vraag';
$lang['question_not_found'] = 'Error !!Please Try Again !May Be Questions not found.';
$lang['category_not_found'] = 'Error !! Please Try Again.May Be Category Not Found';
$lang['answer_not_found'] = "Error !! Please Try Again ! May Be Answer not found";
$lang['add_answer'] = 'Add Answer';
$lang['select_question'] = 'Selecteer vraag';
$lang['select_category'] = 'Selecteer categorie';
$lang['edit_answer'] = 'Antwoord bewerken'; 

// labels for the form groups .. 
$lang['form_groups'] = 'Form Groups'; 
$lang['add_from_group'] = 'Add Form Group'; 
$lang['form_group_name'] = 'Form Group Name'; 
$lang['edit_from_group'] = 'Edit Form Group'; 
$lang['group_not_found']='Error !! Please Try Again.May Be Group Not Found.'; 
$lang['forms_of_group'] = 'Forms of the group '; 
$lang['add_new_form'] = 'Add New Form'; 
$lang['select_form'] = 'Select Form';
$lang['edit_form'] = 'Formulier bewerken';




//Common Labels.. 

$lang['cancel'] = 'Annuleer';
$lang['save'] = 'Opslaan';
$lang['delete'] = 'Delete';
$lang['requred_text_labels_text'] = 'Velden die gemarkeerd zijn met een <span class="required">*</span> zijn verplicht.';
$lang['add_success'] = 'Record Added Sucessufully.';
$lang['add_error'] = 'Error in Adding Records.';
$lang['edit_success'] = 'Record Edited Sucessufully.';
$lang['edit_error'] = 'Error in Editinging Records.';
$lang['delete_question'] = 'Do you want to delete?';
$lang['delete_success'] = 'Record Deleted Sucessufully. ';
$lang['delete_error'] = 'Error in Deleting Records.. ';
$ang['skip_question_error'] = 'You cant delete because this is the skiped question.';
$lang['sequence_change_success'] = 'Sequence of record are changed Sucessufully.';
$lang['sequence_change_error'] = 'You can not change the sequence.';
$lang['mouse_hover_error'] = 'Ga met de muis op een vraag staan en sleep deze naar de juiste positie om de volgorde te wijzigen.';
$lang['error_no_form_group'] = 'Error !! Please Try Again.May be there is no form in the group.';
$lang['record_not_found'] = 'Error! Record not found.';
$lang['already_added_form'] = 'This Form is Already added.You can not add it second time.';


?>
