/***********************************************
* Simple Marquee (04-October-2012)
* by Vic Phillips - http://www.vicsjavascripts.org.uk/
***********************************************/

var zxcMarquee={

    init:function(o){
        var mde=o.Mode,mde=typeof(mde)=='string'&&mde.charAt(0).toUpperCase()=='H'?['left','offsetWidth','top','width']:['top','offsetHeight','left','height'],id=o.ID,srt=o.StartDelay,ud=o.StartDirection,p=document.getElementById(id),obj=p.getElementsByTagName('DIV')[0],sz=obj[mde[1]],clone,nu=Math.ceil(p[mde[1]]/sz)+1,z0=1;
        p.style.overflow='hidden';
        obj.style.position='absolute';
        obj.style[mde[0]]='0px';
        obj.style[mde[3]]=sz+'px';
        for (;z0<nu;z0++){
            clone=obj.cloneNode(true);
            clone.style[mde[0]]=sz*z0+'px';
            clone.style[mde[2]]='0px';
            obj.appendChild(clone);
        }
        o=this['zxc'+id]={
            obj:obj,
            mde:mde[0],
            sz:sz*(z0-1)
        }
        if (typeof(srt)=='number'){
            o.dly=setTimeout(function(){
                zxcMarquee.scroll(id,typeof(ud)=='number'?ud:-1);
            },srt);
        }
        else {
            this.scroll(id,0)
        }
    },

    scroll:function(id,ud){
        var oop=this,o=this['zxc'+id],p;
        if (o){
            ud=typeof(ud)=='number'?ud:0;
            clearTimeout(o.dly);
            p=parseInt(o.obj.style[o.mde])+ud;
            if ((ud>0&&p>0)||(ud<0&&p<-o.sz)){
                p+=o.sz*(ud>0?-1:1);
            }
            o.obj.style[o.mde]=p+'px';
            o.dly=setTimeout(function(){
                oop.scroll(id,ud);
            },50);
        }
    }

}

function init(){

    zxcMarquee.init({
        ID:'marquee1',     // the unique ID name of the parent DIV.                        (string)
        Mode:'Vertical',   //(optional) the mode of execution, 'Vertical' or 'Horizontal'. (string, default = 'Vertical')
        StartDelay:2000,   //(optional) the auto start delay in milli seconds'.            (number, default = no auto start)
        StartDirection:-1  //(optional) the auto start scroll direction'.                  (number, default = -1)
    });
}
