        setInterval("settime()", 1000); 
        function settime () {
          var curtime = new Date();
          utc = curtime.getTime() + (curtime.getTimezoneOffset() * 60000);
           
          nl = new Date(utc + (3600000*2.0));
          ni = new Date(utc + (3600000*5.5));
          nb = new Date(utc - (3600000*3.0));
//  ============================nl==============================
          var nlhour = nl.getHours();
          var nlmin = nl.getMinutes();
          var nlsec = nl.getSeconds();
          var nltime = "";
        
          if(nlhour == 0) nlhour = 12;
          nltime = set_time(nlhour,nlmin,nlsec)
//  =============================================================

//  ============================ni==============================
          var nihour = ni.getHours();
          var nimin = ni.getMinutes();
          var nisec = ni.getSeconds();
          var nitime = "";
        
          if(nihour == 0) nihour = 12;
          nitime = set_time(nihour,nimin,nisec)
//  ============================================================= 

//  ============================nb==============================
          var nbhour = nb.getHours();
          var nbmin = nb.getMinutes();
          var nbsec = nb.getSeconds();
          var nbtime = "";
        
          if(nbhour == 0) nbhour = 12;
          nbtime = set_time(nbhour,nbmin,nbsec)
//  =============================================================        
				if (document.getElementById('clnl'))
           document.getElementById('clnl').innerHTML =  nltime; 
        if (document.getElementById('clin'))
           document.getElementById('clin').innerHTML =  nitime;
        if (document.getElementById('clbr'))
           document.getElementById('clbr').innerHTML =  nbtime;
      }

function set_time(nlhour,nlmin,nlsec)
{
    time = (nlhour > 12 ? nlhour - 12 : nlhour) + ":" +
    (nlmin < 10 ? "0" : "") + nlmin + ":" +
    (nlsec < 10 ? "0" : "") + nlsec + " " +
    (nlhour > 12 ? "PM" : "AM");
    return time;
}